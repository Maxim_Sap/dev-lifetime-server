-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Час створення: Гру 25 2017 р., 12:30
-- Версія сервера: 5.6.38
-- Версія PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `orgjvpwd_novaya_brachka`
--

-- --------------------------------------------------------

--
-- Структура таблиці `action`
--

CREATE TABLE `action` (
  `id` int(11) NOT NULL,
  `action_type_id` int(11) NOT NULL,
  `skin_id` int(11) NOT NULL,
  `action_creator` int(11) NOT NULL,
  `action_receiver` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `action_credit`
--

CREATE TABLE `action_credit` (
  `action_id` int(11) NOT NULL,
  `credit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `action_type`
--

CREATE TABLE `action_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `activity`
--

CREATE TABLE `activity` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `started_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ended_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cam_duration` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `admin_permissions`
--

CREATE TABLE `admin_permissions` (
  `id` int(11) NOT NULL,
  `permissions` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `affiliates`
--

CREATE TABLE `affiliates` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `referral_link` varchar(255) NOT NULL,
  `m_phone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `agency`
--

CREATE TABLE `agency` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_person` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_number` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `office_address` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `office_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `m_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skype` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `director_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_detail` text COLLATE utf8_unicode_ci NOT NULL,
  `card_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approve_status` smallint(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `agency_revard`
--

CREATE TABLE `agency_revard` (
  `id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `main_photo_id` int(11) DEFAULT NULL,
  `public` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `approve_status`
--

CREATE TABLE `approve_status` (
  `id` int(11) NOT NULL,
  `description` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` enum('story','post') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'story',
  `on_main` int(1) NOT NULL DEFAULT '0',
  `status` int(4) NOT NULL DEFAULT '1',
  `sort_order` int(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `chat_session_id` int(11) NOT NULL,
  `chat_id` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `message` text CHARACTER SET utf8mb4,
  `original_message` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `readed_at` timestamp NULL DEFAULT NULL,
  `edited` smallint(1) DEFAULT '0',
  `active` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `chat_invite`
--

CREATE TABLE `chat_invite` (
  `id` int(11) NOT NULL,
  `invite_sender` int(11) NOT NULL,
  `invite_receiver` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `show` smallint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `chat_session`
--

CREATE TABLE `chat_session` (
  `id` int(11) NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `proxy` int(1) DEFAULT NULL,
  `original` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `resource_type_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_read` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `contact_list`
--

CREATE TABLE `contact_list` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `proxy` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `credit`
--

CREATE TABLE `credit` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `eyes_color`
--

CREATE TABLE `eyes_color` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `featured_letter`
--

CREATE TABLE `featured_letter` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `letter_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `gift`
--

CREATE TABLE `gift` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `price` decimal(10,2) DEFAULT NULL,
  `agency_price` decimal(10,2) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `status` int(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `gift_status`
--

CREATE TABLE `gift_status` (
  `id` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `gift_type`
--

CREATE TABLE `gift_type` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `hair_color`
--

CREATE TABLE `hair_color` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `interpreter`
--

CREATE TABLE `interpreter` (
  `id` int(11) NOT NULL,
  `interpreter_type_id` int(11) DEFAULT NULL,
  `deposit` int(11) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `fee` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `interpreter_revard`
--

CREATE TABLE `interpreter_revard` (
  `id` int(11) NOT NULL,
  `interpreter_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deposit` int(11) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `fee` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `interpreter_type`
--

CREATE TABLE `interpreter_type` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `letter`
--

CREATE TABLE `letter` (
  `id` int(11) NOT NULL,
  `action_id` int(11) DEFAULT NULL,
  `theme_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `readed_at` timestamp NULL DEFAULT NULL,
  `reply_status` int(1) DEFAULT NULL,
  `was_deleted_by_man` int(1) DEFAULT NULL,
  `was_deleted_by_girl` int(1) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `penalty_value` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pay_to_agency` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `letter_theme`
--

CREATE TABLE `letter_theme` (
  `id` int(11) NOT NULL,
  `theme` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `location_type_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `location_type`
--

CREATE TABLE `location_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `log_table`
--

CREATE TABLE `log_table` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `mailing_queue`
--

CREATE TABLE `mailing_queue` (
  `id` bigint(20) NOT NULL,
  `mailing_session_id` bigint(20) NOT NULL,
  `created_for` smallint(6) NOT NULL DEFAULT '0',
  `template_id` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `was_received` smallint(6) DEFAULT '0',
  `was_aborted` smallint(6) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `received_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `mailing_session`
--

CREATE TABLE `mailing_session` (
  `id` bigint(20) NOT NULL,
  `creator` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `created_for` smallint(6) NOT NULL DEFAULT '0',
  `sended_messages` int(11) DEFAULT NULL,
  `dublicated_messages` int(11) DEFAULT NULL,
  `total_messages` int(11) DEFAULT NULL,
  `exclude_blacklist` smallint(6) DEFAULT '0',
  `paused` smallint(6) DEFAULT '0',
  `was_finished` smallint(6) DEFAULT '0',
  `started_at` timestamp NULL DEFAULT NULL,
  `ended_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `mailing_template`
--

CREATE TABLE `mailing_template` (
  `id` int(11) NOT NULL,
  `template_creator` int(11) NOT NULL,
  `agency_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `body` text,
  `created_for` smallint(2) NOT NULL DEFAULT '0',
  `status` smallint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `message_creator` int(11) DEFAULT NULL,
  `message_receiver` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `reply_status` int(1) DEFAULT NULL,
  `was_deleted_by_user` int(1) DEFAULT NULL,
  `was_deleted_by_admin` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `readed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `missing_chat`
--

CREATE TABLE `missing_chat` (
  `id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `chat_session_id` int(11) NOT NULL,
  `chat_creator` int(11) NOT NULL,
  `chat_receiver` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `action_id` int(11) DEFAULT NULL,
  `gift_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `penalty_value` decimal(10,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `order_status_id` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `order_status`
--

CREATE TABLE `order_status` (
  `id` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `other_agency_penalty`
--

CREATE TABLE `other_agency_penalty` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text COLLATE utf8_unicode_ci,
  `penalty_value` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `payments_schedule`
--

CREATE TABLE `payments_schedule` (
  `id` int(11) NOT NULL,
  `agency_id` int(11) NOT NULL,
  `period_date` date NOT NULL,
  `status` int(1) NOT NULL,
  `earnings` double NOT NULL,
  `penalty` double NOT NULL,
  `paid_value` double NOT NULL,
  `transfer` double NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_day` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `photo`
--

CREATE TABLE `photo` (
  `id` int(11) NOT NULL,
  `album_id` int(11) DEFAULT NULL,
  `small_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medium_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `watermark_small` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `watermark_medium` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `watermark_orign` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `premium` int(1) DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(4) DEFAULT NULL,
  `approve_status` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `photo_access`
--

CREATE TABLE `photo_access` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `photo_id` int(11) DEFAULT NULL,
  `started_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ended_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `photo_like`
--

CREATE TABLE `photo_like` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `photo_id` int(11) DEFAULT NULL,
  `likes` int(1) DEFAULT NULL,
  `dislikes` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `physique`
--

CREATE TABLE `physique` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `plan`
--

CREATE TABLE `plan` (
  `id` int(11) NOT NULL,
  `skin_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `pricelist`
--

CREATE TABLE `pricelist` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `skin_id` int(11) DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `action_type_id` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stop_date` timestamp NULL DEFAULT NULL,
  `important` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `pricelist_agency`
--

CREATE TABLE `pricelist_agency` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `skin_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `action_type_id` int(11) NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stop_date` timestamp NULL DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `important` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `pricelist_penalty`
--

CREATE TABLE `pricelist_penalty` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `referral_stats`
--

CREATE TABLE `referral_stats` (
  `id` int(11) NOT NULL,
  `referral_link` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `rule_violation_by_agency`
--

CREATE TABLE `rule_violation_by_agency` (
  `id` int(11) NOT NULL,
  `agency_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `site_page`
--

CREATE TABLE `site_page` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `skin`
--

CREATE TABLE `skin` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status_type_id` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `start_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stop_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `status_type`
--

CREATE TABLE `status_type` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `subscribe`
--

CREATE TABLE `subscribe` (
  `id` int(11) NOT NULL,
  `skin_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `task_creator` int(11) NOT NULL,
  `task_performer` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `comment` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `approved_at` timestamp NULL DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `task_type`
--

CREATE TABLE `task_type` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `transfer_type`
--

CREATE TABLE `transfer_type` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skin_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `agency_id` int(11) NOT NULL,
  `referral_id` int(11) DEFAULT NULL,
  `avatar_photo_id` int(11) DEFAULT NULL,
  `access_token` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token_end_date` int(11) NOT NULL,
  `last_activity` int(11) DEFAULT NULL,
  `cam_online` int(1) NOT NULL DEFAULT '0',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `owner_status` smallint(6) NOT NULL DEFAULT '10',
  `visible` smallint(6) NOT NULL DEFAULT '10',
  `fake` smallint(6) NOT NULL DEFAULT '0',
  `approve_status_id` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `user_auth_facebook`
--

CREATE TABLE `user_auth_facebook` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `facebook_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `user_auth_google`
--

CREATE TABLE `user_auth_google` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `google_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `user_blacklist`
--

CREATE TABLE `user_blacklist` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `blacklist_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `user_favorite_list`
--

CREATE TABLE `user_favorite_list` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `favorite_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `user_interpreter`
--

CREATE TABLE `user_interpreter` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `interpreter_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `user_profile`
--

CREATE TABLE `user_profile` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `m_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `sex` smallint(3) DEFAULT NULL,
  `religion` smallint(4) DEFAULT NULL,
  `marital` int(11) DEFAULT NULL,
  `kids` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `eyes` int(11) DEFAULT NULL,
  `hair` int(11) DEFAULT NULL,
  `smoking` int(11) DEFAULT NULL,
  `alcohol` int(11) DEFAULT NULL,
  `ethnos` int(11) DEFAULT NULL,
  `physique` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `education` int(11) DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `english_level` int(11) DEFAULT NULL,
  `looking_age_from` int(11) DEFAULT '14',
  `looking_age_to` int(11) DEFAULT '70',
  `about_me` text COLLATE utf8_unicode_ci,
  `hobbies` text COLLATE utf8_unicode_ci,
  `my_ideal` text COLLATE utf8_unicode_ci,
  `other_language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deactivation_reason` text COLLATE utf8_unicode_ci,
  `deactivation_time` timestamp NULL DEFAULT NULL,
  `deactivation_by_agency` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `user_transfer`
--

CREATE TABLE `user_transfer` (
  `id` int(11) NOT NULL,
  `transfer_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deposit` int(11) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `fee` decimal(10,2) DEFAULT NULL,
  `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `user_type`
--

CREATE TABLE `user_type` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `user_video_setting`
--

CREATE TABLE `user_video_setting` (
  `id` int(11) NOT NULL,
  `video_creator` int(11) NOT NULL,
  `video_receiver` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `session_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `user_video_temp_session`
--

CREATE TABLE `user_video_temp_session` (
  `id` int(11) NOT NULL,
  `video_creator` int(11) NOT NULL,
  `video_receiver` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `user_visiters`
--

CREATE TABLE `user_visiters` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `visited_user_id` int(11) NOT NULL,
  `visit_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `user_wink`
--

CREATE TABLE `user_wink` (
  `id` int(11) NOT NULL,
  `user_creator` int(11) NOT NULL,
  `user_receiver` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `viewed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `poster` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` smallint(4) DEFAULT '0',
  `public` smallint(1) DEFAULT '0',
  `premium` smallint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `video_access`
--

CREATE TABLE `video_access` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `video_id` int(11) DEFAULT NULL,
  `started_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ended_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `video_chat`
--

CREATE TABLE `video_chat` (
  `id` int(11) NOT NULL,
  `action_id` int(11) DEFAULT NULL,
  `video_chat_id` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-action-skin_id` (`skin_id`),
  ADD KEY `idx-action-action_type_id` (`action_type_id`),
  ADD KEY `idx-action-action_creator` (`action_creator`);

--
-- Індекси таблиці `action_credit`
--
ALTER TABLE `action_credit`
  ADD UNIQUE KEY `action_id` (`action_id`),
  ADD KEY `idx-action-credit_action_id` (`action_id`),
  ADD KEY `idx-action-credit_credit_id` (`credit_id`);

--
-- Індекси таблиці `action_type`
--
ALTER TABLE `action_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Індекси таблиці `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-activity-user_id` (`user_id`);

--
-- Індекси таблиці `admin_permissions`
--
ALTER TABLE `admin_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `affiliates`
--
ALTER TABLE `affiliates`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `agency`
--
ALTER TABLE `agency`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `agency_revard`
--
ALTER TABLE `agency_revard`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-agency_revard-user_id` (`user_id`),
  ADD KEY `idx-agency_revard-action_id` (`action_id`);

--
-- Індекси таблиці `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-album-user_id` (`user_id`);

--
-- Індекси таблиці `approve_status`
--
ALTER TABLE `approve_status`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-chat-action_id` (`action_id`),
  ADD KEY `idx-chat-chat_session_id` (`chat_session_id`);

--
-- Індекси таблиці `chat_invite`
--
ALTER TABLE `chat_invite`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `chat_session`
--
ALTER TABLE `chat_session`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `contact_list`
--
ALTER TABLE `contact_list`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `credit`
--
ALTER TABLE `credit`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `eyes_color`
--
ALTER TABLE `eyes_color`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `featured_letter`
--
ALTER TABLE `featured_letter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-featured_letter-user_id` (`user_id`),
  ADD KEY `idx-featured_letter-letter_id` (`letter_id`);

--
-- Індекси таблиці `gift`
--
ALTER TABLE `gift`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-gift-type_id` (`type_id`),
  ADD KEY `idx-gift-status` (`status`);

--
-- Індекси таблиці `gift_status`
--
ALTER TABLE `gift_status`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `gift_type`
--
ALTER TABLE `gift_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Індекси таблиці `hair_color`
--
ALTER TABLE `hair_color`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `interpreter`
--
ALTER TABLE `interpreter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-interpreter-interpreter_type_id` (`interpreter_type_id`);

--
-- Індекси таблиці `interpreter_revard`
--
ALTER TABLE `interpreter_revard`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-interpreter_revard-user_id` (`user_id`),
  ADD KEY `idx-interpreter_revard-interpreter_type_id` (`interpreter_type_id`);

--
-- Індекси таблиці `interpreter_type`
--
ALTER TABLE `interpreter_type`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `letter`
--
ALTER TABLE `letter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-letter-theme_id` (`theme_id`);

--
-- Індекси таблиці `letter_theme`
--
ALTER TABLE `letter_theme`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-location-user_id` (`user_id`),
  ADD KEY `idx-location-location_type_id` (`location_type_id`),
  ADD KEY `idx-location-country_id` (`country_id`),
  ADD KEY `idx-location-city_id` (`city_id`);

--
-- Індекси таблиці `location_type`
--
ALTER TABLE `location_type`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `log_table`
--
ALTER TABLE `log_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-log_table-user_id` (`user_id`),
  ADD KEY `idx-log_table-user_type` (`user_type`);

--
-- Індекси таблиці `mailing_queue`
--
ALTER TABLE `mailing_queue`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `mailing_session`
--
ALTER TABLE `mailing_session`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `mailing_template`
--
ALTER TABLE `mailing_template`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-message-message_creator` (`message_creator`);

--
-- Індекси таблиці `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Індекси таблиці `missing_chat`
--
ALTER TABLE `missing_chat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-missing_chat-action_id` (`action_id`),
  ADD KEY `idx-missing_chat-chat_session_id` (`chat_session_id`),
  ADD KEY `idx-missing_chat-chat_creator` (`chat_creator`),
  ADD KEY `idx-missing_chat-chat_receiver` (`chat_receiver`);

--
-- Індекси таблиці `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-order-action_id` (`action_id`),
  ADD KEY `idx-order-gift_id` (`gift_id`),
  ADD KEY `idx-order-order_status_id` (`order_status_id`);

--
-- Індекси таблиці `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `other_agency_penalty`
--
ALTER TABLE `other_agency_penalty`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-other_agency_penalty-user_id` (`user_id`);

--
-- Індекси таблиці `payments_schedule`
--
ALTER TABLE `payments_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-photo-album_id` (`album_id`);

--
-- Індекси таблиці `photo_access`
--
ALTER TABLE `photo_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-photo_access-user_id` (`user_id`),
  ADD KEY `idx-photo_access-photo_id` (`photo_id`);

--
-- Індекси таблиці `photo_like`
--
ALTER TABLE `photo_like`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-photo_like-user_id` (`user_id`),
  ADD KEY `idx-photo_like-photo_id` (`photo_id`);

--
-- Індекси таблиці `physique`
--
ALTER TABLE `physique`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-plan-skin_id` (`skin_id`);

--
-- Індекси таблиці `pricelist`
--
ALTER TABLE `pricelist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-pricelist-user_id` (`user_id`),
  ADD KEY `idx-pricelist-plan_id` (`plan_id`),
  ADD KEY `idx-pricelist-action_type_id` (`action_type_id`),
  ADD KEY `fk-pricelist-skin_id` (`skin_id`);

--
-- Індекси таблиці `pricelist_agency`
--
ALTER TABLE `pricelist_agency`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-pricelist_agency-user_id` (`user_id`),
  ADD KEY `idx-pricelist_agency-plan_id` (`plan_id`),
  ADD KEY `idx-pricelist_agency-skin_id` (`skin_id`),
  ADD KEY `idx-pricelist_agency-action_type_id` (`action_type_id`);

--
-- Індекси таблиці `pricelist_penalty`
--
ALTER TABLE `pricelist_penalty`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type_id` (`type_id`);

--
-- Індекси таблиці `referral_stats`
--
ALTER TABLE `referral_stats`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `rule_violation_by_agency`
--
ALTER TABLE `rule_violation_by_agency`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-rule_violation_by_agency-agency_id` (`agency_id`);

--
-- Індекси таблиці `site_page`
--
ALTER TABLE `site_page`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `skin`
--
ALTER TABLE `skin`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-status-user_id` (`user_id`),
  ADD KEY `idx-status-status_type_id` (`status_type_id`);

--
-- Індекси таблиці `status_type`
--
ALTER TABLE `status_type`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `subscribe`
--
ALTER TABLE `subscribe`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-task-task_creator` (`task_creator`),
  ADD KEY `idx-task-task_performer` (`task_performer`),
  ADD KEY `idx-task-status_id` (`status_id`);

--
-- Індекси таблиці `task_type`
--
ALTER TABLE `task_type`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `transfer_type`
--
ALTER TABLE `transfer_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Індекси таблиці `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user-skin_id` (`skin_id`),
  ADD KEY `idx-user-user_type` (`user_type`),
  ADD KEY `idx-user-agency_id` (`agency_id`);

--
-- Індекси таблиці `user_auth_facebook`
--
ALTER TABLE `user_auth_facebook`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD UNIQUE KEY `idx-user_auth_facebook-user_id` (`user_id`);

--
-- Індекси таблиці `user_auth_google`
--
ALTER TABLE `user_auth_google`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD UNIQUE KEY `idx-user_auth_google-user_id` (`user_id`);

--
-- Індекси таблиці `user_blacklist`
--
ALTER TABLE `user_blacklist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user_blacklist_list-user_id` (`user_id`),
  ADD KEY `idx-user_blacklist_list-blacklist_user_id` (`blacklist_user_id`);

--
-- Індекси таблиці `user_favorite_list`
--
ALTER TABLE `user_favorite_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user_favorite_list-user_id` (`user_id`),
  ADD KEY `idx-user_favorite_list-favorite_user_id` (`favorite_user_id`);

--
-- Індекси таблиці `user_interpreter`
--
ALTER TABLE `user_interpreter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user_interpreter-user_id` (`user_id`),
  ADD KEY `idx-user_interpreter-interpreter_id` (`interpreter_id`);

--
-- Індекси таблиці `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user_profile-eyes` (`eyes`),
  ADD KEY `idx-user_profile-hair` (`hair`),
  ADD KEY `idx-user_profile-physique` (`physique`);

--
-- Індекси таблиці `user_transfer`
--
ALTER TABLE `user_transfer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user_tranfser-user_id` (`user_id`),
  ADD KEY `idx-user_tranfser-transfer_type_id` (`transfer_type_id`);

--
-- Індекси таблиці `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Індекси таблиці `user_video_setting`
--
ALTER TABLE `user_video_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user_video_setting-video_creator` (`video_creator`),
  ADD KEY `idx-user_video_setting-video_receiver` (`video_receiver`);

--
-- Індекси таблиці `user_video_temp_session`
--
ALTER TABLE `user_video_temp_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user_video_temp_session-video_creator` (`video_creator`),
  ADD KEY `idx-user_video_temp_session-video_receiver` (`video_receiver`);

--
-- Індекси таблиці `user_visiters`
--
ALTER TABLE `user_visiters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user_visiters-user_id` (`user_id`);

--
-- Індекси таблиці `user_wink`
--
ALTER TABLE `user_wink`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user_wink-user_creator` (`user_creator`),
  ADD KEY `idx-user_wink-user_receiver` (`user_receiver`);

--
-- Індекси таблиці `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-video-user_id` (`user_id`),
  ADD KEY `idx-video-poster` (`poster`);

--
-- Індекси таблиці `video_access`
--
ALTER TABLE `video_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-video_access-user_id` (`user_id`),
  ADD KEY `idx-video_access-video_id` (`video_id`);

--
-- Індекси таблиці `video_chat`
--
ALTER TABLE `video_chat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-video_chat-action_id` (`action_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `action`
--
ALTER TABLE `action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2100;
--
-- AUTO_INCREMENT для таблиці `action_type`
--
ALTER TABLE `action_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=817;
--
-- AUTO_INCREMENT для таблиці `admin_permissions`
--
ALTER TABLE `admin_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;
--
-- AUTO_INCREMENT для таблиці `affiliates`
--
ALTER TABLE `affiliates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT для таблиці `agency`
--
ALTER TABLE `agency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT для таблиці `agency_revard`
--
ALTER TABLE `agency_revard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=777;
--
-- AUTO_INCREMENT для таблиці `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;
--
-- AUTO_INCREMENT для таблиці `approve_status`
--
ALTER TABLE `approve_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблиці `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=893;
--
-- AUTO_INCREMENT для таблиці `chat_invite`
--
ALTER TABLE `chat_invite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT для таблиці `chat_session`
--
ALTER TABLE `chat_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=326;
--
-- AUTO_INCREMENT для таблиці `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT для таблиці `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `contact_list`
--
ALTER TABLE `contact_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблиці `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;
--
-- AUTO_INCREMENT для таблиці `credit`
--
ALTER TABLE `credit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=794;
--
-- AUTO_INCREMENT для таблиці `eyes_color`
--
ALTER TABLE `eyes_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `featured_letter`
--
ALTER TABLE `featured_letter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблиці `gift`
--
ALTER TABLE `gift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT для таблиці `gift_type`
--
ALTER TABLE `gift_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблиці `hair_color`
--
ALTER TABLE `hair_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `interpreter`
--
ALTER TABLE `interpreter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;
--
-- AUTO_INCREMENT для таблиці `interpreter_revard`
--
ALTER TABLE `interpreter_revard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `interpreter_type`
--
ALTER TABLE `interpreter_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `letter`
--
ALTER TABLE `letter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT для таблиці `letter_theme`
--
ALTER TABLE `letter_theme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT для таблиці `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT для таблиці `location_type`
--
ALTER TABLE `location_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `log_table`
--
ALTER TABLE `log_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3363;
--
-- AUTO_INCREMENT для таблиці `mailing_queue`
--
ALTER TABLE `mailing_queue`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=321;
--
-- AUTO_INCREMENT для таблиці `mailing_session`
--
ALTER TABLE `mailing_session`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблиці `mailing_template`
--
ALTER TABLE `mailing_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблиці `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблиці `missing_chat`
--
ALTER TABLE `missing_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;
--
-- AUTO_INCREMENT для таблиці `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблиці `order_status`
--
ALTER TABLE `order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблиці `other_agency_penalty`
--
ALTER TABLE `other_agency_penalty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `payments_schedule`
--
ALTER TABLE `payments_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT для таблиці `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1113;
--
-- AUTO_INCREMENT для таблиці `photo_access`
--
ALTER TABLE `photo_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблиці `photo_like`
--
ALTER TABLE `photo_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблиці `physique`
--
ALTER TABLE `physique`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `plan`
--
ALTER TABLE `plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `pricelist`
--
ALTER TABLE `pricelist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `pricelist_agency`
--
ALTER TABLE `pricelist_agency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблиці `pricelist_penalty`
--
ALTER TABLE `pricelist_penalty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `referral_stats`
--
ALTER TABLE `referral_stats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `rule_violation_by_agency`
--
ALTER TABLE `rule_violation_by_agency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблиці `site_page`
--
ALTER TABLE `site_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблиці `skin`
--
ALTER TABLE `skin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблиці `status_type`
--
ALTER TABLE `status_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблиці `subscribe`
--
ALTER TABLE `subscribe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `task`
--
ALTER TABLE `task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблиці `task_type`
--
ALTER TABLE `task_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `transfer_type`
--
ALTER TABLE `transfer_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5000037;
--
-- AUTO_INCREMENT для таблиці `user_auth_facebook`
--
ALTER TABLE `user_auth_facebook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `user_auth_google`
--
ALTER TABLE `user_auth_google`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблиці `user_blacklist`
--
ALTER TABLE `user_blacklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблиці `user_favorite_list`
--
ALTER TABLE `user_favorite_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT для таблиці `user_interpreter`
--
ALTER TABLE `user_interpreter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблиці `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5000037;
--
-- AUTO_INCREMENT для таблиці `user_transfer`
--
ALTER TABLE `user_transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблиці `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблиці `user_video_setting`
--
ALTER TABLE `user_video_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `user_video_temp_session`
--
ALTER TABLE `user_video_temp_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `user_visiters`
--
ALTER TABLE `user_visiters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=230;
--
-- AUTO_INCREMENT для таблиці `user_wink`
--
ALTER TABLE `user_wink`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT для таблиці `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблиці `video_access`
--
ALTER TABLE `video_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `video_chat`
--
ALTER TABLE `video_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=706;
--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `interpreter_revard`
--
ALTER TABLE `interpreter_revard`
  ADD CONSTRAINT `fk-interpreter_revard-interpreter_type_id` FOREIGN KEY (`interpreter_type_id`) REFERENCES `interpreter_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk-interpreter_revard-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `other_agency_penalty`
--
ALTER TABLE `other_agency_penalty`
  ADD CONSTRAINT `fk-other_agency_penalty-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `pricelist`
--
ALTER TABLE `pricelist`
  ADD CONSTRAINT `fk-pricelist-skin_id` FOREIGN KEY (`skin_id`) REFERENCES `skin` (`id`);

--
-- Обмеження зовнішнього ключа таблиці `rule_violation_by_agency`
--
ALTER TABLE `rule_violation_by_agency`
  ADD CONSTRAINT `fk-rule_violation_by_agency-agency_id` FOREIGN KEY (`agency_id`) REFERENCES `agency` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
