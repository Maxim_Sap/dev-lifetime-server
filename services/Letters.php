<?php
namespace app\services;

use app\models\AgencyBonus;
use Yii;
use app\models\Letter;
use app\models\Letterstheme;
use app\models\ActionTypes;
use app\models\Action;
use app\models\User;
use app\models\UserProfile;
use app\models\Photo as PhotosModel;
use app\models\FeaturedLetters;
use app\models\UserFavoriteList;
use app\models\Womans;
use yii\db\Query;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\rest\UpdateAction;
use yii\web\BadRequestHttpException;


class Letters
{
    private $_userId    = null;
    private $_user_type = null;

    function __construct($userID = null)
    {
        if ($userID !== null) {
            self::setUser($userID);
        }
    }

    public function setUser($userID)
    {
        $user = User::findOne(['id' => $userID]);
        if (empty($user)) {
            return [
                'success' => false,
                'code' => 1, 
                'message' => 'User_id error'
            ];
        }

        $this->_userId    = $user->id;
        $this->_user_type = $user->user_type;
    }

    public function send($action_type, $skin_id, $user_to, $caption, $theme_id, $desc, $previous_letters_id)
    {
        if ($action_type === null || $skin_id === null || !is_numeric($skin_id) || !is_numeric($user_to) || ($caption === null && $theme_id === null)) {
            return ['success' => false, 'code' => 1, 'message' => 'Missing params'];
        }

        if ($this->_userId === null || $this->_user_type === null) {
            return ['success' => false, 'code' => 1, 'message' => 'User error'];
        }

        (is_numeric($action_type)) ?
            $action_types_model = ActionTypes::findOne(['action_type_id' => $action_type]) :
            $action_types_model = ActionTypes::findOne(['code' => $action_type]);

        if (empty($action_types_model)) {
            return ['success' => false, 'code' => 1, 'message' => 'Action type not found'];
        }

        $user = new User($this->_userId);

        $billing_model = new Billing();
        //проверяем если мужик
        if ($this->_user_type == 1) {
            $user_balance = $user->payments->GetBalance();

            $letters_price = $billing_model->getActionPrice($skin_id, $plan_id = 1, $action_types_model->action_type_id, $this->_userId);

            if (!is_numeric($letters_price)) {
                return ['success' => false, 'code' => 1, 'message' => 'Letters price error'];
            }

            if ($user_balance < $letters_price) {
                return ['success' => false, 'message' => 'Not enough money'];
            }
        }

        if ($theme_id === null) {
            $letterstheme_model          = new Letterstheme();
            $letterstheme_model->caption = $caption;
            if (!$letterstheme_model->save()) {
                return ['success' => false, 'message' => 'Letters theme error save'];
            }
        } else {
            $letterstheme_model = Letterstheme::findOne($theme_id);
            if (empty($letterstheme_model)) {
                return ['success' => false, 'message' => 'Letters theme not found'];
            }
        }

        $action_model                 = new Actions();
        $action_model->action_type_id = $action_types_model->action_type_id;
        $action_model->skin_id        = $skin_id;
        $action_model->user_from      = $this->_userId;
        $action_model->user_to        = $user_to;

        if (!$action_model->save()) {
            return ['success' => false, 'message' => 'Action not save'];
        }

        $letters              = new LettersModel();
        $letters->action_id   = $action_model->action_id;
        $letters->theme_id    = $letterstheme_model->id;
        $letters->description = $desc;


        //if action_type_id !== null answer_status = 1;
        if ($previous_letters_id !== null) {
            $previous_letters = LettersModel::findOne($previous_letters_id);
            if (empty($previous_letters)) {
                return ['success' => false, 'message' => 'Previous letters not found'];
            }
            $previous_letters->answer_status = 1;

            //есди мужик отвечает на письмо девочки то агенству сразу заходит премия
            if ($this->_user_type == 1) {
                $women_model = Womans::findOne(['user_id'=>$user_to]);
                if(!empty($women_model)){
                    //даем бонус агенству
                    $agency_id = $women_model->agency_id;
                    $agency_letters_price = $billing_model->getAgencyPrice($skin_id, $plan_id = 1, $action_types_model->action_type_id, $agency_id);
                    if(!empty($agency_letters_price)){
                        $agency_bonus = new AgencyBonus();
                        $agency_bonus->action_id = $action_model->action_id;
                        $agency_bonus->user_id = $agency_id;
                        $agency_bonus->amount = $agency_letters_price;
                        if (!$agency_bonus->save()) {
                            return ['success' => false, 'message' => 'Agency bonus not save'];
                        }
                        $letters->pay_to_agency = 1;
                    }
                }
            }elseif($this->_user_type == 2){
                //девочка отвечает на письмо мужчины
                //если по письму на которое отвечает не было бонуса агенству, то выплатить этот бонус
                //и внести соответствующте данные в письмо на которое отвечает девочка
                //так как по ТЗ агенство получает бонус как только мужчина отвечаает на письмо девочки
                //то есть по одному писму не может быт сразу два бонуса агенству
                if($previous_letters->pay_to_agency == 0){
                    $women_model = Womans::findOne(['user_id'=>$this->_userId]);
                    if(!empty($women_model)){
                        //даем бонус агенству
                        $agency_id = $women_model->agency_id;
                        $agency_letters_price = $billing_model->getAgencyPrice($skin_id, $plan_id = 1, $action_types_model->action_type_id, $agency_id);
                        if(!empty($agency_letters_price)){
                            $agency_bonus = new AgencyBonus();
                            $agency_bonus->action_id = $previous_letters->action_id;
                            $agency_bonus->user_id = $agency_id;
                            $agency_bonus->amount = $agency_letters_price;
                            if (!$agency_bonus->save()) {
                                //return ['success' => false, 'message' => $agency_bonus->errors];
                                return ['success' => false, 'message' => 'Agency bonus not save'];
                            }
                            $previous_letters->pay_to_agency = 1;
                        }
                    }
                }

            }
            if (!$previous_letters->save()) {
                return ['success' => false, 'message' => 'Error save message status'];
            }
        }

        if (!$letters->save()) {
            return ['success' => false, 'message' => 'Letters not save'];
        }




        //с мужиков взымаем плату
        if ($this->_user_type == 1) {
            $user->payments->AddCharges($action_model->action_id, $letters_price);
        }

        return ['success' => true, 'message' => 'Letters send'];

    }

    public function dell($id, $status = 0)
    {
        if (!is_numeric($status)) {
            return ['success' => false, 'message' => 'Error status'];
        }
        $action_model = (new Query())
            ->from([Actions::tableName()]);
        if (is_array($id)) {
            $action_model->where([
                'and', ['in', 'action_id', $id],
                ['or', "user_from = $this->_userId", "user_to = $this->_userId"],
            ]);
            if ($action_model->count() != count($id)) {
                return ['success' => false, 'message' => 'One of lettes not found'];
            }
        } else {
            $action_model->where([
                'and', ['action_id' => $id],
                ['or', "user_from = $this->_userId", "user_to = $this->_userId"],
            ])->one();

            if (empty($action_model)) {
                return ['success' => false, 'message' => 'Letters not found'];
            }
        }

        $delete_status_title = null;

        if ($this->_user_type == 1) $delete_status_title = 'man_delete_status';
        if ($this->_user_type == 2) $delete_status_title = 'girl_delete_status';

        if (empty($delete_status_title)) {
            return ['success' => false, 'message' => 'Error user type'];
        }

        //если пригшел массив id
        if (is_array($id)) {
            $letters = LettersModel::find()
                ->where(['in', 'action_id', $id])
                ->count();
            if ($letters != count($id)) {
                return ['success' => false, 'message' => 'One of lettes not found'];
            }

            $ress = LettersModel::updateAll([$delete_status_title => $status], ['in', 'action_id', $id]);
            if (!$ress) {
                return ['success' => false, 'message' => 'Lettes update error'];
            }

            //усли пригшел один id
        } else {
            $letters = LettersModel::findOne(['action_id' => $id]);
            if (!$letters) {
                return ['success' => false, 'message' => 'Lettes not found'];
            }

            $letters->$delete_status_title = $status;
            if (!$letters->save()) {
                return ['success' => false, 'message' => 'Letters status error save'];
            }

        }


        return ['success' => true, 'letters delete'];
    }

    public function read($id)
    {
        if (!is_numeric($id)) {
            return ['success' => false, 'message' => 'Message_id error'];
        }

        $action_model = (new Query())
            ->from([Actions::tableName()])
            ->where([
                'and', ['action_id' => $id],
                ['or', "user_from = $this->_userId", "user_to = $this->_userId"],
            ])
            ->one();

        if (empty($action_model)) {
            return ['success' => false, 'message' => 'Letters not found'];
        }

        $letters = LettersModel::findOne(['action_id' => $id]);
        if (!$letters) {
            return ['success' => false, 'message' => 'Letters not found'];
        }

        $letters->date_read = date('Y-m-d H:i:s', time());
        if (!$letters->save()) {
            return ['success' => false, 'message' => 'Letters status error save'];
        }

        return ['success' => true];
    }

    public function get($letters_id, $theme_id, $type, $limit = null, $offset = null, $search_params = null)
    {
        $user_type = isset($this->_user_type) && $this->_user_type == 1 ? 'lett.man_delete_status' : 'lett.girl_delete_status';

        $featured_ids   = $this->getFeatured();
        $black_list_ids = (new User($this->_userId))->getBlackList();

        $query           = new Query();
        $letters         = $query->addSelect(['lett.*', 'actions.dt', 'lett_th.caption', 'photos.thumb_normal as another_user_avatar', 'user_pers.first_name as another_user_name', 'users.user_types']);
        $letters_type    = null;
        $another_user_id = "actions.user_from"; //by default
        switch ($type) {
            case 'new':
                $another_user_id = "actions.user_from";
                $letters->addSelect([$another_user_id . ' as another_user_id']);
                $letters->where(['actions.user_to' => $this->_userId, 'date_read' => null, $user_type => 1]);
                break;
            case 'inbox':
                $another_user_id = "actions.user_from";
                $letters->addSelect([$another_user_id . ' as another_user_id']);
                $letters->where(['actions.user_to' => $this->_userId, $user_type => 1]);
                break;
            case 'outbox':
                $another_user_id = "actions.user_to";
                $letters->addSelect([$another_user_id . ' as another_user_id']);
                $letters->where(['actions.user_from' => $this->_userId, $user_type => 1]);
                break;
            case 'featured':
                $another_user_id = "actions.user_from";
                $letters->addSelect([$another_user_id . ' as another_user_id']);

                if (!is_array($featured_ids) || empty($featured_ids)) {
                    return ['success' => true, 'letters_mass' => null, 'count_letters' => 0, 'featured_ids' => null, 'black_list_ids' => null];
                }

                $letters->andWhere(['in', 'lett.action_id', $featured_ids]);
                break;
            case 'deleted':
                $another_user_id = "actions.user_from";
                $letters->addSelect([$another_user_id . ' as another_user_id']);
                $letters->where(['actions.user_to' => $this->_userId, $user_type => 0]);
                $letters->orWhere(['actions.user_from' => $this->_userId, $user_type => 0]);
                break;
            case 'single':
                $letters_type = $this->getLettersTypeById($letters_id);
                if ($letters_type['type'] == 'inbox') {
                    $another_user_id = "actions.user_from";
                } else {
                    $another_user_id = "actions.user_to";
                }
                $letters->addSelect([$another_user_id . ' as another_user_id']);
                break;

        }

        if (isset($search_params['date_create']) && strtotime($search_params['date_create'])) {
            $date_frmat = date('Y-m-d', strtotime($search_params['date_create']));
            $letters->andWhere(['>=', 'actions.dt', $date_frmat]);
        }

        if (isset($search_params['is_favourite']) && is_numeric($search_params['is_favourite'])) {
            if ($search_params['is_favourite'] == 1) {
                $letters->andWhere(['in', 'lett.action_id', $featured_ids]);
            } else {

                $letters->andWhere(['NOT IN', 'lett.action_id', $featured_ids]);
            }
        }

        if (isset($search_params['other_user_id']) && is_numeric($search_params['other_user_id'])) {
            $letters->andWhere([$another_user_id => $search_params['other_user_id']]);
        }

        if (isset($search_params['answered']) && is_numeric($search_params['answered'])) {
            switch ($search_params['answered']) {
                case 1:
                    $letters->andWhere(['lett.answer_status' => 1]);
                    break;
                case 2:
                    $letters->andWhere('lett.date_read IS NOT NULL')
                        ->andWhere(['lett.answer_status' => 0]);
                    break;
                case 3:
                    $letters->andWhere(['lett.date_read' => null]);
                    break;
            }
        }

        $letters->from([LettersModel::tableName() . ' lett'])
            ->leftJoin(Letterstheme::tableName() . ' lett_th', 'lett.theme_id = lett_th.id')
            ->leftJoin(Actions::tableName() . ' actions', 'actions.action_id = lett.action_id')//;
            ->leftJoin(Users::tableName() . ' users', 'users.user_id = ' . $another_user_id)
            ->leftJoin(UserPersonal::tableName() . ' user_pers', 'user_pers.user_id = ' . $another_user_id)
            ->leftJoin(PhotosModel::tableName() . ' photos', 'users.avatar_photo_id = photos.photo_id');

        if ($letters_id !== null && is_numeric($letters_id)) {
            $letters->andWhere(['lett.action_id' => $letters_id]);
        }
        if ($theme_id !== null && is_numeric($theme_id)) {
            $letters->andWhere(['lett.theme_id' => $theme_id]);
        }

        //добавление состояния favorite
        if (!empty($this->_userId)) {
            $subQuery = (new Query)
                ->addSelect(['user_favorite_list.other_user_id', 'user_in_favorite' => new Expression(1)])
                ->from([UserFavoriteList::tableName() . ' user_favorite_list'])
                ->where(['user_favorite_list.user_id' => $this->_userId])
                ->limit(100000);
            $letters->leftJoin(['user_favorite_list' => $subQuery], 'user_favorite_list.other_user_id = users.user_id');
            $letters->addSelect(['user_favorite_list.user_in_favorite']);
        }

        $letters->orderBy('actions.dt DESC');
        //$letters->groupBy('lett_th.id');

        $count_letters = $letters->count();

        if ($limit !== null && is_numeric($limit)) {
            $letters->limit($limit);
        }
        if ($offset !== null && is_numeric($offset)) {
            $letters->offset($offset);
        }

        $ress = $letters->all();

        return ['success' => true, 'letters_mass' => $ress, 'count_letters' => $count_letters, 'featured_ids' => $featured_ids, 'black_list_ids' => $black_list_ids, 'letters_type' => $letters_type];

    }


    public function getHistoryById($letters_id,$my_ids)
    {
        $letters_model = Actions::findOne(['action_id'=>$letters_id]);

        if(empty($letters_model)){
            return ['success' => false, 'message' => 'Letters not founs'];
        }

        $access_flag = false;
        if($my_ids == 'admin'){
            $access_flag = true;
        }
        //agency
        if(is_array($my_ids) && (in_array($letters_model->user_from,$my_ids) || in_array($letters_model->user_to,$my_ids))){
            $access_flag = true;
        }
        //single user
        if(is_numeric($my_ids) && ($my_ids == $letters_model->user_from || $letters_model->user_to)){
            $access_flag = true;
        }
        if(!$access_flag){
            return ['success' => false, 'message' => 'Access denied'];
        }

        $subquery = (new Query())
            ->select(['users.user_id', 'photos.thumb_normal', 'user_pers.first_name', 'users.user_types'])
            ->from([Users::tableName() . ' users'])
            ->leftJoin(UserPersonal::tableName() . ' user_pers', 'user_pers.user_id = users.user_id')
            ->leftJoin(PhotosModel::tableName() . ' photos', 'users.avatar_photo_id = photos.photo_id')
            ->limit(100000);

        $query = (new Query())
            ->select(['lett.*', 'actions.dt', 'lett_th.caption',
                      'user_from.thumb_normal as from_user_avatar',
                      'user_from.first_name as from_user_name',
                      'user_from.user_types as from_user_type',
                      'user_from.user_id as from_user_id',
                      'user_to.thumb_normal as user_to_avatar',
                      'user_to.first_name as user_to_name',
                      'user_to.user_types as user_to_user_type',
                      'user_to.user_id as to_user_id',])
            ->from([LettersModel::tableName() . ' lett'])
            ->leftJoin(Letterstheme::tableName() . ' lett_th', 'lett.theme_id = lett_th.id')
            ->leftJoin(Actions::tableName() . ' actions', 'actions.action_id = lett.action_id')
            ->leftJoin(['user_from' => $subquery], 'user_from.user_id = actions.user_from')
            ->leftJoin(['user_to' => $subquery], 'user_to.user_id = actions.user_to')
            ->where(['actions.user_from'=>$letters_model->user_from,'actions.user_to'=>$letters_model->user_to])
            ->orWhere(['actions.user_from'=>$letters_model->user_to,'actions.user_to'=>$letters_model->user_from])
            ->orderBy('actions.action_id');

        if (!empty($this->_userId)) {
            $subQuery2 = (new Query)
                ->addSelect(['user_favorite_list.other_user_id', 'user_in_favorite' => new Expression(1)])
                ->from([UserFavoriteList::tableName() . ' user_favorite_list'])
                ->where(['user_favorite_list.user_id' => $this->_userId])
                ->limit(100000);
            $query->leftJoin(['user_from_favorite_list' => $subQuery2], 'user_from_favorite_list.other_user_id = actions.user_from');
            $query->leftJoin(['user_to_favorite_list' => $subQuery2], 'user_to_favorite_list.other_user_id = actions.user_to');
            $query->addSelect('user_from_favorite_list.user_in_favorite as user_from_in_favorite, user_to_favorite_list.user_in_favorite as user_to_in_favorite');
        }

        $count = $query->count();

        $ress = $query->all();

        return ['success' => true, 'letters' => $ress, 'count' => $count];
    }

    public function getToAdmin($search_params, $limit = null, $offset = null, $my_children_ids = null)
    {

        $date_from    = isset($search_params['date_from']) && strtotime($search_params['date_from']) ? $search_params['date_from'] : false;
        $date_to      = isset($search_params['date_to']) && strtotime($search_params['date_to']) ? $search_params['date_to'] : false;
        $user_id_from = isset($search_params['user_id_from']) && is_numeric($search_params['user_id_from']) ? $search_params['user_id_from'] : false;
        $user_id_to   = isset($search_params['user_id_to']) && is_numeric($search_params['user_id_to']) ? $search_params['user_id_to'] : false;
        $answered     = isset($search_params['answered']) && is_numeric($search_params['answered']) ? $search_params['answered'] : false;
        $read         = isset($search_params['read']) && is_numeric($search_params['read']) ? $search_params['read'] : false;

        if ($date_from && $date_to) {
            if (strtotime($date_from) > strtotime($date_to)) {
                return ['success' => false, 'message' => 'Error date format'];
            }
        }
        if ($date_from && strtotime($date_from) > time()) {
            return ['success' => false, 'message' => 'Error date format'];
        }

        $query = new Query();

        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, user_id')
            ->from([UserPersonal::tableName() . ' user_pers']);

        $letters = $query->addSelect(['lett.*', 'actions.dt', 'lett_th.caption', 'user_pers.user_id as user_from_id', 'user_pers.first_name as user_from_name', 'user_pers2.user_id as user_to_id', 'user_pers2.first_name as user_to_name']);

        switch ($search_params['type']) {
            case 'inbox':
                if (!empty($my_children_ids)) {
                    $letters->where(['in', 'actions.user_to', $my_children_ids]);
                } else {
                    $letters->where(['actions.user_to' => $this->_userId]);
                }
                break;
            case 'outbox':
                if (!empty($my_children_ids)) {
                    $letters->where(['in', 'actions.user_from', $my_children_ids]);
                } else {
                    $letters->where(['actions.user_from' => $this->_userId]);
                }
                break;
        }

        $letters->from([LettersModel::tableName() . ' lett'])
            ->leftJoin(Letterstheme::tableName() . ' lett_th', 'lett.theme_id = lett_th.id')
            ->leftJoin(Actions::tableName() . ' actions', 'actions.action_id = lett.action_id')
            ->leftJoin(['user_pers' => $subQuery], 'user_pers.user_id = actions.user_from')
            ->leftJoin(['user_pers2' => $subQuery], 'user_pers2.user_id = actions.user_to');

        if ($date_from) {
            $letters->andwhere(['>=', 'actions.dt', $date_from]);
        }
        if ($date_to) {
            $letters->andwhere(['<=', 'actions.dt', $date_to]);
        }
        if ($user_id_from) {
            $letters->andwhere(['actions.user_from' => $user_id_from]);
        }
        if ($user_id_to) {
            $letters->andwhere(['actions.user_to' => $user_id_to]);
        }
        if ($answered !== false) {
            $letters->andwhere(['lett.answer_status' => $answered]);
        }
        if ($read !== false) {
            if ($read == 0) {
                $letters->andwhere('lett.date_read IS NULL');
            } else {
                $letters->andwhere('lett.date_read IS NOT NULL');
            }

        }


        $letters->orderBy('actions.dt DESC');

        $count_letters = $letters->count();

        if ($limit !== null && is_numeric($limit)) {
            $letters->limit($limit);
        }
        if ($offset !== null && is_numeric($offset)) {
            $letters->offset($offset);
        }

        $ress = $letters->all();

        return ['success' => true, 'letters_mass' => $ress, 'count_letters' => $count_letters];

    }

    public function getLettersTypeById($letters_id)
    {
        $action_model = Actions::findOne(['action_id' => $letters_id, 'user_to' => $this->_userId]);

        if (empty($action_model)) {
            return ['type' => 'outbox'];
        }

        return ['type' => 'inbox'];

    }

    public function getPrevNextLetersIds($letters_id)
    {
        $user_type = isset($this->_user_type) && $this->_user_type == 1 ? 'man_delete_status' : 'girl_delete_status';

        if (!is_numeric($letters_id)) {
            return ['success' => false, 'message' => 'Message_id error'];
        }

        $letters_type = $this->getLettersTypeById($letters_id);
        if ($letters_type['type'] == 'inbox') {
            $fields_name = "user_to";
        } else {
            $fields_name = "user_from";
        }

        $action_model = Actions::findOne(['action_id' => $letters_id, $fields_name => $this->_userId]);

        if (empty($action_model)) {
            return ['prev' => null, 'next' => null];
        }


        $letters_model = LettersModel::findOne(['action_id' => $letters_id, $user_type => 1]);

        if (empty($letters_model)) {
            return ['prev' => null, 'next' => null];
        }

        $letters = (new Query())
            ->from([LettersModel::tableName() . " lett"])
            ->addSelect(['lett.action_id'])
            ->leftJoin(Actions::tableName() . ' actions', 'actions.action_id = lett.action_id')
            ->where(['actions.' . $fields_name => $this->_userId, 'lett.' . $user_type => 1])
            ->all();

        if (empty($letters)) {
            return ['prev' => null, 'next' => null];
        }

        for ($i = 0; $i < count($letters); $i++) {
            if ($letters[$i]['action_id'] == $letters_id) {
                $prev_id = isset($letters[$i - 1]['action_id']) ? $letters[$i - 1]['action_id'] : null;
                $next_id = isset($letters[$i + 1]['action_id']) ? $letters[$i + 1]['action_id'] : null;
            }
        }

        return ['prev' => $prev_id, 'next' => $next_id];

    }

    public function setFeatured($letters_id)
    {
        if (empty($letters_id) || (!is_numeric($letters_id) && !is_array($letters_id))) {
            return ['success' => false, 'message' => 'Missing params1'];
        }

        $action_model = (new Query())
            ->from([Actions::tableName()]);
        if (is_array($letters_id)) {
            if (empty($letters_id)) {
                return ['success' => false, 'message' => 'Missing params'];
            }
            $action_model->where([
                'and', ['in', 'action_id', $letters_id],
                ['or', "user_from = $this->_userId", "user_to = $this->_userId"],
            ]);
            if ($action_model->count() != count($letters_id)) {
                return ['success' => false, 'message' => 'One of lettes not found'];
            }
        } else {
            $action_model->where([
                'and', ['action_id' => $letters_id],
                ['or', "user_from = $this->_userId", "user_to = $this->_userId"],
            ])->one();

            if (empty($action_model)) {
                return ['success' => false, 'message' => 'Letters not found'];
            }
        }


        //если пригшел массив id
        if (is_array($letters_id)) {
            $letters = LettersModel::find()
                ->where(['in', 'action_id', $letters_id])
                ->count();
            if ($letters != count($letters_id)) {
                return ['success' => false, 'message' => 'One of lettes not found'];
            }

            foreach ($letters_id as $one) {
                $featured_model = FeaturedLetters::findOne(['letters_id' => $one, 'user_id' => $this->_userId]);
                if (empty($featured_model)) {
                    $featured_model             = new FeaturedLetters();
                    $featured_model->letters_id = $one;
                    $featured_model->user_id    = $this->_userId;
                    if (!$featured_model->save()) {
                        return ['success' => false, 'message' => 'Feature letters not save'];
                    }
                }
            }

            return ['success' => true, 'message' => 'Featured_message add'];
            //если пришел один id
        } else {
            $letters = LettersModel::findOne(['action_id' => $letters_id]);
            if (!$letters) {
                return ['success' => false, 'message' => 'Letters not found'];
            }

            $featured_model = FeaturedLetters::findOne(['letters_id' => $letters_id, 'user_id' => $this->_userId]);
            if (empty($featured_model)) {
                $featured_model             = new FeaturedLetters();
                $featured_model->letters_id = $letters_id;
                $featured_model->user_id    = $this->_userId;
                if (!$featured_model->save()) {
                    return ['success' => false, 'message' => 'Feature letters not save'];
                }

                return ['success' => true, 'featured_message_id' => $featured_model->id];
            } else {
                FeaturedLetters::deleteAll(['id' => $featured_model->id]);

                return ['success' => true, 'message' => 'Featured_message deleted'];
            }
        }


    }

    public function getFeatured()
    {
        $featured_model = FeaturedLetters::find()
            ->where(['user_id' => $this->_userId])
            ->all();
        if (empty($featured_model)) {
            return null;
        }

        foreach ($featured_model as $one) {
            $ids[] = $one->letters_id;
        }

        return $ids;

    }

    public function deleteAllFromFeatured()
    {
        FeaturedLetters::deleteAll(['user_id' => $this->_userId]);
    }

    public function getNewLetters()
    {
        $userType = ($this->_user_type == User::USER_MALE) ? 'letter.was_deleted_by_girl' : 'letter.was_deleted_by_man';
        $letters = (new Query())
            ->from([Letter::tableName()])
            ->leftJoin(Action::tableName(), 'action.id = letter.action_id')
            ->where(['action.action_receiver' => $this->_userId, 'letter.readed_at' => null, $userType => Letter::WAS_DELETED]);
        $count = $letters->count();

        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, id')
            ->from([UserProfile::tableName()]);
        //подзапрос на фото пользователя
        $subQuery2 = (new Query)
            ->select('user.id, photo.small_thumb')
            ->from([User::tableName()])
            ->leftJoin(PhotosModel::tableName(), 'user.avatar_photo_id = photo.id');

        $lastName = $letters
            ->addSelect('user_profile.first_name, photo.small_thumb')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
            ->leftJoin(['photo' => $subQuery2], 'photo.id = action.action_creator')
            ->orderBy('letter.action_id DESC')
            ->one();

        return [
            'count' => $count, 
            'users' => $lastName
        ];
    }

    public function getExpired()
    {
        $user   = new User($this->_userId);
        $my_ids = $user->getMyChidrensIds();
        if (empty($my_ids) || (!is_array($my_ids) && $my_ids != 'admin')) {
            return ['letters_mass' => [], 'count' => 0];

        }
        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, last_name, user_id')
            ->from([UserPersonal::tableName() . ' user_pers']);

        $letters_model = (new Query())
            ->select('womans.user_id as womans_user_id, actions.dt as date_create, lett.penalty_value, lett.action_id,lett.man_delete_status,lett.girl_delete_status,
             user_pers.user_id as from_user_id, user_pers.first_name as from_name, user_pers.last_name as from_last_name, 
             user_pers2.user_id as to_user_id, user_pers2.first_name as to_name, user_pers2.last_name as to_last_name,
             user_pers3.user_id as agency_user_id, user_pers3.first_name as agency_name, user_pers3.last_name as agency_last_name')
            ->from([LettersModel::tableName() . ' lett'])
            ->leftJoin(Actions::tableName() . ' actions', 'actions.action_id = lett.action_id')
            ->leftJoin(Womans::tableName() . ' womans', 'womans.user_id = actions.user_to')
            ->leftJoin(['user_pers' => $subQuery], 'user_pers.user_id = actions.user_from')
            ->leftJoin(['user_pers2' => $subQuery], 'user_pers2.user_id = actions.user_to')
            ->leftJoin(['user_pers3' => $subQuery], 'user_pers3.user_id = womans.agency_id')
            ->where(['>','lett.penalty_value', 0])
            ->andWhere('womans.user_id IS NOT NULL');
        if (is_array($my_ids)) {
            $letters_model->andWhere(['in', 'actions.user_to', $my_ids]);
        }
        $letters_model->orderBy('lett.action_id DESC');

        $count = $letters_model->count();

        $ress = $letters_model->all();

        return ['letters_mass' => $ress, 'count' => $count];
    }

    public function updatePenaltyLettersItem()
    {
        $pinalty_letter_time = \Yii::$app->params['pinalty_letter_time'];
        $pinalty_letter_time = $pinalty_letter_time*60*60; //in seconds
        $pinalty_letter_time_max = \Yii::$app->params['pinalty_letter_time_max'];
        $pinalty_letter_time_max = $pinalty_letter_time_max*60*60; //in seconds
        $billing_model = new Billing();
        $price_incriment = $billing_model->getPenaltyValue('letters');

        $letters_model = (new Query())
            ->select('actions.action_id, actions.dt, actions.user_to')
            ->from([LettersModel::tableName() . ' lett'])
            ->leftJoin(Actions::tableName() . ' actions', 'actions.action_id = lett.action_id')
            ->leftJoin(Womans::tableName() . ' womans', 'womans.user_id = actions.user_to')
            ->where(['lett.answer_status' => 0])
            ->andWhere('womans.user_id IS NOT NULL')
            ->andWhere(['<','actions.dt',date("Y-m-d H:i:s",time()-$pinalty_letter_time)])
            ->andWhere(['>','actions.dt',date("Y-m-d H:i:s",time()-$pinalty_letter_time_max)]);
        $count = $letters_model->count();
        $ress = $letters_model->all();

        $pinalty_date_mass = [];
        if(!empty($ress)){
            foreach($ress as $one){
                //$pinalty_date = ((date("j",time() - strtotime($one['dt'])) - 1) < $pinalty_letter_time_max/86400) ? (date("j",time() - (strtotime($one['dt'])+$pinalty_letter_time)) - 1) : floor($pinalty_letter_time_max/86400-1);
                $pinalty_date = (time() - strtotime($one['dt']) < $pinalty_letter_time_max) ? floor((time() - strtotime($one['dt']) - $pinalty_letter_time)/86400)+1 : floor($pinalty_letter_time_max/86400-1);
                $pinalty_date_mass[] = $pinalty_date;
                $ress2 = LettersModel::updateAll(
                    ['penalty_value' => $pinalty_date * $price_incriment],
                    ['action_id'=>$one['action_id']]
                );
            }
        }

        return $count;

    }



}