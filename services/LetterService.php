<?php
namespace app\services;

use app\models\AgencyBonus;
use Yii;
use app\models\Letter;
use app\models\LetterTheme;
use app\models\ActionType;
use app\models\Action;
use app\models\Agency;
use app\models\AgencyRevard;
use app\models\User;
use app\models\UserProfile;
use app\models\Photo;
use app\models\Letter as LettersModel;
use app\models\FeaturedLetter;
use app\models\UserFavoriteList;
use yii\db\Query;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\rest\UpdateAction;
use yii\web\BadRequestHttpException;
use yii\helpers\ArrayHelper;


class LetterService
{
    private $_userID   = null;
    private $_userType = null;

    function __construct($userID = null)
    {
        if ($userID !== null) {
            self::setUser($userID);
        }
    }

    public function setUser($userID)
    {
        $user = User::findOne(['id' => $userID]);
        if (empty($user)) {
            return [
                'success' => false,
                'code' => 1, 
                'message' => 'User_id error'
            ];
        }

        $this->_userID   = $user->id;
        $this->_userType = $user->user_type;
    }

    public function send($actionType, $skinID, $toUser, $title, $themeID, $desc, $previousLetterID)
    {
        if ($actionType === null || $skinID === null || !is_numeric($skinID) || !is_numeric($toUser) || ($title === null && $themeID === null)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        if ($this->_userID === null || $this->_userType === null) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User error'
            ];
        }

        (is_numeric($actionType)) ?
            $actionTypeModel = ActionType::findOne(['id' => (int)$actionType]) :
            $actionTypeModel = ActionType::findOne(['name' => $actionType]);

        if (empty($actionTypeModel)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Action type not found'
            ];
        }

        $user = User::findOne(['id' => $this->_userID]);

        $billingService = new BillingService($user->id);
        $paymentsService = new PaymentsService($user->id);
        //проверяем если мужик
        if ($this->_userType == User::USER_MALE) {
            
            $userBalance = $paymentsService->getBalance();

            $lettersPrice = $billingService->getActionPrice($skinID, $planID = 1, $actionTypeModel->id, $this->_userID);

            if (!is_numeric($lettersPrice)) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'Letters price error',
                ];
            }

            if ($userBalance < $lettersPrice) {
                return [
                    'success' => false, 
                    'message' => 'Not enough money'
                ];
            }
        }

        if ($themeID === null) {
            $letterTheme          = new LetterTheme();
            $letterTheme->theme = $title;
            if (!$letterTheme->save()) {
                return [
                    'success' => false, 
                    'message' => 'Letters theme error save'
                ];
            }
        } else {
            $letterTheme = LetterTheme::findOne(['id' => $themeID]);
            if (empty($letterTheme)) {
                return [
                    'success' => false, 
                    'message' => 'Letters theme not found'
                ];
            }
        }

        $action             = new Action();
        $action->action_type_id = $actionTypeModel->id;
        $action->skin_id        = $skinID;
        $action->action_creator = $this->_userID;
        $action->action_receiver = $toUser;

        if (!$action->save()) {
            return [
                'success' => false, 
                'message' => 'Action not save'
            ];
        }

        $letter              = new Letter();
        $letter->action_id   = $action->id;
        $letter->theme_id    = $letterTheme->id;        
        $letter->description = $desc;
        $letter->was_deleted_by_man = Letter::WASNT_DELETED;
        $letter->was_deleted_by_girl = Letter::WASNT_DELETED;


        //if action_type_id !== null answer_status = 1;
        if ($previousLetterID !== null) {
            $previousLetter = Letter::findOne(['id' => $previousLetterID]);
            if (empty($previousLetter)) {
                return [
                    'success' => false, 
                    'message' => 'Previous letters not found'
                ];
            }

            $previousLetter->reply_status = 1;

            //есди мужик отвечает на письмо девочки то агенству сразу заходит премия
            if ($this->_userType == User::USER_MALE) {
                $girl = User::findOne(['id' => $toUser, 'user_type' => User::USER_FEMALE]);
                if(!empty($girl)){
                    //даем бонус агенству
                    $agencyID = $girl->agency_id;
                    $agency = User::findOne(['agency_id'=>$agencyID, 'user_type' => User::USER_AGENCY]);
                    $agencyLetterPrice = $billingService->getAgencyPrice($skinID, $planID = 1, $actionTypeModel->id, $agencyID);
                    if (!empty($agencyLetterPrice)) {
                        $agencyBonus = new AgencyRevard();
                        $agencyBonus->action_id = $action->id;
                        $agencyBonus->user_id = $agency->id;
                        $agencyBonus->amount = $agencyLetterPrice;
                        if (!$agencyBonus->save()) {
                            return [
                                'success' => false, 
                                'message' => 'Agency bonus not save'
                            ];
                        }
                        $letter->pay_to_agency = 1;
                    }
                } else {
                    return [
                        'success' => false, 
                        'message' => 'User not exists'
                    ];
                }
            }elseif($this->_userType == User::USER_FEMALE){
                //девочка отвечает на письмо мужчины
                //если по письму на которое отвечает не было бонуса агенству, то выплатить этот бонус
                //и внести соответствующте данные в письмо на которое отвечает девочка
                //так как по ТЗ агенство получает бонус как только мужчина отвечаает на письмо девочки
                //то есть по одному писму не может быт сразу два бонуса агенству
                if($previousLetter->pay_to_agency == 0) {
                    $girl = User::findOne(['id'=>$this->_userID, 'user_type' => User::USER_FEMALE]);
                    if(!empty($girl)){
                        //даем бонус агенству
                        $agencyID = $girl->agency_id;
                        $agency = User::findOne(['agency_id'=>$agencyID, 'user_type' => User::USER_AGENCY]);
                        $agencyLetterPrice = $billingService->getAgencyPrice($skinID, $planID = 1, $actionTypeModel->id, $agencyID);
                        if(!empty($agencyLetterPrice)){
                            $agencyBonus = new AgencyRevard();
                            $agencyBonus->action_id = $previousLetter->action_id;
                            $agencyBonus->user_id = $agency->id;
                            $agencyBonus->amount = $agencyLetterPrice;
                            if (!$agencyBonus->save()) {                                
                                return [
                                    'success' => false, 
                                    'message' => 'Agency bonus not save'
                                ];
                            }
                            $previousLetter->pay_to_agency = 1;
                        }
                    }
                }
            }
            if (!$previousLetter->save()) {
                return ['success' => false, 'message' => 'Error save message status'];
            }
        }

        if (!$letter->save()) {
            return ['success' => false, 'message' => 'Letter not save'];
        }


        //с мужиков взымаем плату
        if ($this->_userType == User::USER_MALE) {
            $paymentsService->addCredits($action->id, $lettersPrice);
        }

        return [
            'success' => true, 
            'message' => 'Letter was send to user',            
        ];

    }

    public function setLetterStatus($id, $status = Letter::WAS_DELETED)
    {
        if (!is_numeric($status)) {
            return ['success' => false, 'message' => 'Error status'];
        }

        if (is_array($id)) {
            $errorMessage = 'One of letters not found';   
            $successMessage = 'Letters were deleted';
        } else {
            $errorMessage = 'Letter not found';
            $successMessage = 'Letter was deleted';
        }

        $letters = Letter::find()->where(['id' => $id]);
        if ($letters->count() != count($id)) {
            return [
                'success' => false, 
                'message' => $errorMessage
            ];
        }                
        $subQuery = $letters;
        $action = Action::find()->where(['or', "action_creator = $this->_userID", "action_receiver = $this->_userID"])
                                ->andWhere(['in', 'id', $subQuery->addSelect(['action_id'])]);                                    
        if (empty($action->all()) || ($action->count() != count($id))) {
            return ['success' => false, 'message' => $errorMessage];
        }

        $deleteStatus = null;

        if ($this->_userType == User::USER_MALE) $deleteStatus = 'was_deleted_by_man';
        if ($this->_userType == User::USER_FEMALE) $deleteStatus = 'was_deleted_by_girl';

        if (empty($deleteStatus)) {
            return ['success' => false, 'message' => 'Error user type'];
        }

        $result = Letter::updateAll([$deleteStatus => $status], ['in', 'id', $id]);
        if (!$result) {
            return ['success' => false, 'message' => 'Letters update error'];
        }
       
        return ['success' => true, 'message' => $successMessage];
    }

    public function read($id)
    {
        if (!is_numeric($id)) {
            return [
                'success' => false, 
                'message' => 'Wrong id'
            ];
        }

        $letter = Letter::findOne(['id' => $id]);

        if (empty($letter)) {
            return [
                'success' => false, 
                'message' => 'Letter not found'
            ];
        }

        $action = $letter->getAction()->where(                
                ['or', "action_creator = $this->_userID", "action_receiver = $this->_userID"]
            )
            ->one();

        if (empty($action)) {
            return ['success' => false, 'message' => 'Letter not found'];
        }
        if (empty($letter->readed_at)) {
            $letter->readed_at = date('Y-m-d H:i:s', time());
            if (!$letter->update(false)) {
                return ['success' => false, 'message' => 'Letters status error save'];
            }
        }
        
        return ['success' => true];
    }

    public function get($letterID, $themeID, $type, $limit = null, $offset = null, $searchParams = null)
    {
        $deleteStatus = isset($this->_userType) && $this->_userType == User::USER_MALE ? 'letter.was_deleted_by_man' : 'letter.was_deleted_by_girl';

        $featuredIDs   = $this->getFeatured();
        $blackListIDs = [];
        $currentUser = User::findOne(['id' => $this->_userID]);
        $blackList = $currentUser->getUserBlacklists0()->addSelect('blacklist_user_id')->all();
        foreach ($blackList as $id) {
            $blackListIDs[] = $id['blacklist_user_id'];
        }

        $query = $this->buildQuery($letterID, $themeID, $type, $featuredIDs, $blackListIDs, $deleteStatus, $searchParams);

        $letters = $query['query'];
        $letterType = $query['letterType'];

        $count_letters = $letters->count();

        if ($limit !== null && is_numeric($limit)) {
            $letters->limit($limit);
        }
        if ($offset !== null && is_numeric($offset)) {
            $letters->offset($offset);
        }

        $result = $letters->all();

        return [
            'success' => true, 
            'lettersArray' => $result, 
            'countLetters' => $count_letters, 
            'featuredIDs' => $featuredIDs, 
            'blackListIDs' => $blackListIDs, 
            'letterType' => $letterType,            
        ];

    }

    public function buildQuery($letterID, $themeID, $type, $featuredIDs, $blackListIDs, $deleteStatus, $searchParams)
    {
        $query           = new Query();
        $letters         = $query->addSelect(['letter.id', 'letter.action_id', 'letter.theme_id', 'letter.description', 'letter.readed_at', 'letter.reply_status','action.created_at', 'letter_theme.theme', 'photo.medium_thumb as another_user_avatar', 'user_profile.first_name as another_user_name', 'user.user_type', 'user.last_activity', 'user_profile.birthday']);
        $letterType    = null;
        $anotherUserID = "action.action_creator"; //by default
        switch ($type) {
            case 'new':
                $anotherUserID = "action.action_creator";
                $letters->addSelect([$anotherUserID . ' as another_user_id']);
                $letters->where(['action.action_receiver' => $this->_userID, 'readed_at' => null, $deleteStatus => Letter::WASNT_DELETED]);
                break;
            case 'inbox':
                $anotherUserID = "action.action_creator";
                $letters->addSelect([$anotherUserID . ' as another_user_id']);
                $letters->where(['action.action_receiver' => $this->_userID, $deleteStatus => Letter::WASNT_DELETED]);
                break;
            case 'outbox':
                $anotherUserID = "action.action_receiver";
                $letters->addSelect([$anotherUserID . ' as another_user_id']);
                $letters->where(['action.action_creator' => $this->_userID, $deleteStatus => Letter::WASNT_DELETED]);
                break;
            case 'featured':
                $anotherUserID = "action.action_receiver";
                $letters->addSelect([$anotherUserID . ' as another_user_id']);

                if (!is_array($featuredIDs) || empty($featuredIDs)) {
                    return [
                        'success' => true, 
                        'lettersArray' => null, 
                        'countLetters' => 0, 
                        'featuredIDs' => null, 
                        'blackListIDs' => null
                    ];
                }

                $letters->andWhere(['in', 'letter.id', $featuredIDs]);
                break;
            case 'deleted':
                $anotherUserID = "action.action_creator";
                $letters->addSelect([$anotherUserID . ' as another_user_id']);
                $letters->where(['action.action_receiver' => $this->_userID, $deleteStatus => Letter::WAS_DELETED]);
                $letters->orWhere(['action.action_creator' => $this->_userID, $deleteStatus => Letter::WAS_DELETED]);
                break;
            case 'single':
                $letterType = $this->getLettersTypeById($letterID);
                if ($letterType['type'] == 'inbox') {
                    $anotherUserID = "action.action_creator";
                } else {
                    $anotherUserID = "action.action_receiver";
                }
                $letters->addSelect([$anotherUserID . ' as another_user_id']);
                break;

        }

        if (isset($searchParams['created_at']) && strtotime($searchParams['created_at'])) {
            $dateFormat = date('Y-m-d', strtotime($searchParams['created_at']));
            $letters->andWhere(['>=', 'action.created_at', $dateFormat]);
        }

        if (isset($searchParams['is_favourite']) && is_numeric($searchParams['is_favourite'])) {
            if ($searchParams['is_favourite'] == 1) {
                $letters->andWhere(['letter.id' => $featuredIDs]);
            } else {
                if (is_array($featuredIDs)) {
                    $letters->andWhere(['NOT IN', 'letter.id', $featuredIDs]);
                }
                
            }
        }

        if (isset($searchParams['otherUserID']) && is_numeric($searchParams['otherUserID'])) {
            $letters->andWhere([$anotherUserID => $searchParams['otherUserID']]);
        }

        if (isset($searchParams['answered']) && is_numeric($searchParams['answered'])) {
            switch ($searchParams['answered']) {
                case 1:
                    $letters->andWhere(['letter.reply_status' => 1]);
                    break;
                case 2:
                    $letters->andWhere('letter.readed_at IS NOT NULL')
                        ->andWhere(['or', ['letter.reply_status' => 0], ['letter.reply_status' => 1], ['letter.reply_status' => null]]);
                    break;
                case 3:
                    $letters->andWhere(['letter.readed_at' => null]);
                    break;
            }
        }

        $letters->from([Letter::tableName()])
            ->leftJoin(LetterTheme::tableName(), 'letter.theme_id = letter_theme.id')
            ->leftJoin(Action::tableName(), 'action.id = letter.action_id')//;
            ->leftJoin(User::tableName(), 'user.id = ' . $anotherUserID)
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = ' . $anotherUserID)
            ->leftJoin(Photo::tableName(), 'user.avatar_photo_id = photo.id');

        if ($letterID !== null && is_numeric($letterID)) {
            $letters->andWhere(['letter.id' => $letterID]);
        }
        if ($themeID !== null && is_numeric($themeID)) {
            $letters->andWhere(['letter.theme_id' => $themeID]);
        }

        //добавление состояния favorite
        if (!empty($this->_userID)) {
            $subQuery = (new Query)
                ->addSelect(['user_favorite_list.favorite_user_id', 'user_in_favorite' => new Expression(1)])
                ->from([UserFavoriteList::tableName()])
                ->where(['user_favorite_list.user_id' => $this->_userID])
                ->limit(100000);
            $letters->leftJoin(['user_favorite_list' => $subQuery], 'user_favorite_list.favorite_user_id = user.id');
            $letters->addSelect(['user_favorite_list.user_in_favorite']);
        }

        $letters->orderBy('action.created_at DESC');

        return [
            'query' => $letters,
            'letterType' => $letterType
        ];
    }

    public function getLettersCount($letterID, $themeID, $type, $limit, $offset, $searchParams)
    {
        $deleteStatus = isset($this->_userType) && $this->_userType == User::USER_MALE ? 'letter.was_deleted_by_man' : 'letter.was_deleted_by_girl';

        $featuredIDs   = $this->getFeatured();
        $blackListIDs = [];
        $currentUser = User::findOne(['id' => $this->_userID]);
        $blackList = $currentUser->getUserBlacklists0()->addSelect('blacklist_user_id')->all();
        foreach ($blackList as $id) {
            $blackListIDs[] = $id['blacklist_user_id'];
        }

        $query = $this->buildQuery($letterID, $themeID, $type, $featuredIDs, $blackListIDs, $deleteStatus, $searchParams);

        $letters = $query['query'];

        return $letters->count();
    }

    public function getHistoryById($letterID, $myIDs, $limit=1000, $offset=0)
    {
        $letter = Letter::findOne(['id'=>$letterID]);

        if (empty($letter)) {
            return [
                'success' => false, 
                'message' => 'Letter not found'
            ];
        } else {
            $action = $letter->action;
        }

        $accessFlag = false;
        if ($myIDs == 'admin') {
            $accessFlag = true;
        }
        //agency
        if (is_array($myIDs) && (in_array($action->action_creator, $myIDs) || in_array($action->action_receiver, $myIDs))) {
            $accessFlag = true;
        }
        //single user
        if (is_numeric($myIDs) && ($myIDs == $action->action_creator || $myIDs == $action->action_receiver)) {
            $accessFlag = true;
        }
        if (!$accessFlag) {
            return [
                'success' => false, 
                'message' => 'Access denied'
            ];
        }

        $subquery = (new Query())
            ->select(['user.id', 'photo.medium_thumb', 'user_profile.first_name', 'user.user_type'])
            ->from([User::tableName()])
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = user.id')
            ->leftJoin(Photo::tableName(), 'user.avatar_photo_id = photo.id')
            ->limit(100000);

        $query = (new Query())
            ->select(['letter.id', 'letter.action_id', 'letter.theme_id', 'letter.description', 'letter.readed_at', 'letter.reply_status', 'action.created_at', 'letter_theme.theme',
                      'user_from.medium_thumb as from_user_avatar',
                      'user_from.first_name as from_user_name',
                      'user_from.user_type as from_user_type',
                      'user_from.id as from_user_id',
                      'user_to.medium_thumb as user_to_avatar',
                      'user_to.first_name as user_to_name',
                      'user_to.user_type as user_to_user_type',
                      'user_to.id as to_user_id'])
            ->from([Letter::tableName()])
            ->leftJoin(LetterTheme::tableName(), 'letter.theme_id = letter_theme.id')
            ->leftJoin(Action::tableName(), 'action.id = letter.action_id')
            ->leftJoin(['user_from' => $subquery], 'user_from.id = action.action_creator')
            ->leftJoin(['user_to' => $subquery], 'user_to.id = action.action_receiver')
            ->where(['action.action_creator'=>$action->action_creator,'action.action_receiver'=>$action->action_receiver])
            ->orWhere(['action.action_creator'=>$action->action_receiver,'action.action_receiver'=>$action->action_creator])
            ->orderBy('action.id')
            ->limit($limit)
            ->offset($offset);

        if (!empty($this->_userID)) {
            $subQuery2 = (new Query)
                ->addSelect(['user_favorite_list.favorite_user_id', 'user_in_favorite' => new Expression(1)])
                ->from([UserFavoriteList::tableName()])
                ->where(['user_favorite_list.user_id' => $this->_userID])
                ->limit(100000);
            $query->leftJoin(['user_from_favorite_list' => $subQuery2], 'user_from_favorite_list.favorite_user_id = action.action_creator');
            $query->leftJoin(['user_to_favorite_list' => $subQuery2], 'user_to_favorite_list.favorite_user_id = action.action_receiver');
            $query->addSelect('user_from_favorite_list.user_in_favorite as user_from_in_favorite, user_to_favorite_list.user_in_favorite as user_to_in_favorite');
        }

        $count = $query->count();

        $result = $query->all();

        return [
            'success' => true, 
            'letters' => $result, 
            'count' => $count
        ];
    }

    public function getToAdmin($searchParams, $limit = null, $offset = null, $myChildrenIDs = null)
    {

        $dateFrom    = isset($searchParams['dateFrom']) && strtotime($searchParams['dateFrom']) ? $searchParams['dateFrom'] : false;
        $dateTo      = isset($searchParams['dateTo']) && strtotime($searchParams['dateTo']) ? $searchParams['dateTo'] : false;
        $userID_from = isset($searchParams['userID_from']) && is_numeric($searchParams['userID_from']) ? $searchParams['userID_from'] : false;
        $userID_to   = isset($searchParams['userID_to']) && is_numeric($searchParams['userID_to']) ? $searchParams['userID_to'] : false;
        $answered     = isset($searchParams['answered']) && is_numeric($searchParams['answered']) ? $searchParams['answered'] : false;
        $read         = isset($searchParams['read']) && is_numeric($searchParams['read']) ? $searchParams['read'] : false;

        $letterID = isset($searchParams['letterID']) && is_numeric($searchParams['letterID']) ? (int)$searchParams['letterID'] : false;

        if ($dateFrom && $dateTo) {
            if (strtotime($dateFrom) > strtotime($dateTo)) {
                return [
                    'success' => false, 
                    'message' => 'Error date format'
                ];
            }
        }
        if ($dateFrom && strtotime($dateFrom) > time()) {
            return [
                'success' => false, 
                'message' => 'Error date format'
            ];
        }

        $query = new Query();

        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, id')
            ->from([UserProfile::tableName()]);

        $letters = $query->addSelect(['letter.id', 'letter.action_id', 'letter.theme_id', 'letter.description', 'letter.readed_at', 'letter.reply_status', 'action.created_at', 'letter_theme.theme', 'user_profile.id as user_from_id', 'user_profile.first_name as user_from_name', 'user_profile2.id as user_to_id', 'user_profile2.first_name as user_to_name']);

        switch ($searchParams['type']) {
            case 'inbox':
                if (!empty($myChildrenIDs)) {
                    $letters->where(['in', 'action.action_receiver', $myChildrenIDs]);
                } else {
                    $letters->where(['action.action_receiver' => $this->_userID]);
                }
                break;
            case 'outbox':
                if (!empty($myChildrenIDs)) {
                    $letters->where(['in', 'action.action_creator', $myChildrenIDs]);
                } else {
                    $letters->where(['action.action_creator' => $this->_userID]);
                }
                break;
        }

        $letters->from([Letter::tableName()])
            ->leftJoin(LetterTheme::tableName(), 'letter.theme_id = letter_theme.id')
            ->leftJoin(Action::tableName(), 'action.id = letter.action_id')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
            ->leftJoin(['user_profile2' => $subQuery], 'user_profile2.id = action.action_receiver');

        if ($dateFrom) {
            $letters->andWhere(['>=', 'action.created_at', $dateFrom]);
        }
        if ($dateTo) {
            $letters->andWhere(['<=', 'action.created_at', $dateTo]);
        }
        if ($userID_from) {
            $letters->andWhere(['action.action_creator' => $userID_from]);
        }
        if ($userID_to) {
            $letters->andWhere(['action.action_receiver' => $userID_to]);
        }
        if ($answered !== false) {
            if ($answered == 0) {
                $letters->andWhere('letter.reply_status IS NULL');
            } else {
                $letters->andWhere('letter.reply_status IS NOT NULL');
            }
        }
        if ($read !== false) {
            if ($read == 0) {
                $letters->andWhere('letter.readed_at IS NULL');
            } else {
                $letters->andWhere('letter.readed_at IS NOT NULL');
            }

        }

        if ($letterID != false) {
            $letters->andWhere(['letter.id' => $letterID]);
        }

        $letters->andWhere(['letter.was_deleted_by_man' => Letter::WASNT_DELETED, 
                            'letter.was_deleted_by_girl' => Letter::WASNT_DELETED]);

        $letters->orderBy('action.created_at DESC');

        $countLetters = $letters->count();

        if ($limit !== null && is_numeric($limit)) {
            $letters->limit($limit);
        }
        if ($offset !== null && is_numeric($offset)) {
            $letters->offset($offset);
        }

        $result = $letters->all();

        return [
            'success' => true, 
            'lettersArray' => $result, 
            'countLetters' => $countLetters
        ];

    }

    public function getLettersTypeById($letterID)
    {
        $letter = Letter::findOne(['id' => $letterID]);        
        if (!empty($letter)) {
            $action = $letter->getAction()->where(['action_receiver' => $this->_userID])->one();
        }        

        if (empty($action)) {
            return ['type' => 'outbox'];
        }

        return ['type' => 'inbox'];

    }

    public function getPrevNextLettersIds($letterID)
    {
        $deleteStatus = isset($this->_userType) && $this->_userType == User::USER_MALE ? 'was_deleted_by_man' : 'was_deleted_by_girl';

        if (!is_numeric($letterID)) {
            return ['success' => false, 'message' => 'Message_id error'];
        }

        $letterType = $this->getLettersTypeById($letterID);
        if ($letterType['type'] == 'inbox') {
            $fields_name = "action_receiver";
        } else {
            $fields_name = "action_creator";
        }
        $letter = Letter::findOne(['id' => $letterID]);
        if (!empty($letter)) {
            $action = $letter->getAction()->where([$fields_name => $this->_userID])->one();
        } else {
            return ['prev' => null, 'next' => null];
        }

        if (empty($action)) {
            return ['prev' => null, 'next' => null];
        }

        $letters = (new Query())
            ->from([Letter::tableName()])
            ->addSelect(['letter.id'])
            ->leftJoin(Action::tableName(), 'action.id = letter.action_id')
            ->where(['action.' . $fields_name => $this->_userID, 'letter.' . $deleteStatus => Letter::WASNT_DELETED])
            ->all();

        if (empty($letters)) {
            return ['prev' => null, 'next' => null];
        }

        for ($i = 0; $i < count($letters); $i++) {
            if ($letters[$i]['id'] == $letterID) {
                $prevID = isset($letters[$i - 1]['id']) ? $letters[$i - 1]['id'] : null;
                $nextID = isset($letters[$i + 1]['id']) ? $letters[$i + 1]['id'] : null;
            }
        }

        return ['prev' => $prevID, 'next' => $nextID];

    }

    public function setFeatured($letterID)
    {
        if (empty($letterID) || (!is_numeric($letterID) && !is_array($letterID))) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }

        if (is_array($letterID)) {
            $errorMessage = 'One of letters not found';   
            $successMessage = 'Letters were deleted';
        } else {
            $errorMessage = 'Letter not found';
            $successMessage = 'Letter was deleted';
        }

        $letters = Letter::find()->where(['id' => $letterID]);
        if ($letters->count() != count($letterID)) {
            return [
                'success' => false, 
                'message' => $errorMessage
            ];
        }                
        $subQuery = Letter::find()->where(['id' => $letterID]);
        $action = Action::find()->where(['or', "action_creator = $this->_userID", "action_receiver = $this->_userID"])
                                ->andWhere(['in', 'id', $subQuery->addSelect(['action_id'])]);                                    
        if (empty($action->all()) || ($action->count() != count($letterID))) {
            return ['success' => false, 'message' => $errorMessage];
        }

        //если пришел массив id

        if (is_array($letterID)) {
            $letters = $letters->asArray()->all();
            $lettersIDs = ArrayHelper::map($letters, 'id', 'id');            
            $rows = [];
            $featuredIDs = [];
            foreach ($lettersIDs as $id) {
                $rows[] = [
                    'user_id' => $this->_userID,
                    'letter_id' => $id
                ];
                $featuredIDs[] = $id;
            }
            $featuredLetters = FeaturedLetter::find()->where(['letter_id' => $lettersIDs])->asArray()->all();            
            FeaturedLetter::deleteAll(['user_id' => $this->_userID, 'letter_id' => $featuredIDs]);
            if (empty($featuredLetters)) {
                $featured = new FeaturedLetter;
                $result = Yii::$app->db->createCommand()->batchInsert(FeaturedLetter::tableName(), ['user_id', 'letter_id'], $rows)->execute();

                if (!$result) {
                    return ['success' => false, 'message' => 'Can\'t add letters to featured'];
                }

                return ['success' => true, 'message' => 'Letters added to featured'];    
            } else {
                return ['success' => true, 'message' => 'Letters removed to featured'];    
            }
            
            //если пришел один id
        } else {
            $letters = $letters->one();
            $featured = FeaturedLetter::findOne(['letter_id' => $letters->id, 'user_id' => $this->_userID]);
            if (empty($featured)) {
                $featured             = new FeaturedLetter();
                $featured->letter_id = $letterID;
                $featured->user_id    = $this->_userID;
                if (!$featured->save()) {
                    return ['success' => false, 'message' => 'Feature letters not save'];
                }

                return ['success' => true, 'featured_message_id' => $featured->id];
            } else {
                FeaturedLetter::deleteAll(['id' => $featured->id]);

                return ['success' => true, 'message' => 'Featured_message deleted'];
            }
        }


    }

    public function getFeatured()
    {
        $featured = FeaturedLetter::find()
            ->where(['user_id' => $this->_userID])
            ->all();
        if (empty($featured)) {
            return null;
        }

        foreach ($featured as $letter) {
            $IDs[] = $letter->letter_id;
        }

        return $IDs;

    }

    public function deleteAllFromFeatured()
    {
        FeaturedLetter::deleteAll(['user_id' => $this->_userID]);
    }

    public function getNewLetters()
    {
        $deleteStatus = ($this->_userType == User::USER_MALE) ? 'letter.was_deleted_by_girl' : 'letter.was_deleted_by_man';
        $letters = (new Query())
            ->from([Letter::tableName()])
            ->leftJoin(Action::tableName(), 'action.id = letter.action_id')
            ->where(['action.action_receiver' => $this->_userID, 'letter.readed_at' => null, $deleteStatus => Letter::WASNT_DELETED]);
        $count = $letters->count();

        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, id')
            ->from([UserProfile::tableName()]);
        //подзапрос на фото пользователя
        $subQuery2 = (new Query)
            ->select('user.id, photo.small_thumb')
            ->from([User::tableName()])
            ->leftJoin(Photo::tableName(), 'user.avatar_photo_id = photo.id');

        $lastName = $letters
            ->addSelect('user_profile.first_name, photo.small_thumb, count(*) as count')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
            ->leftJoin(['photo' => $subQuery2], 'photo.id = action.action_creator')
            ->orderBy('letter.action_id DESC')
            ->groupBy('action_creator')
            ->all();

        return [
            'count' => $count, 
            'users' => $lastName
        ];
    }

    public function getExpired($dateFrom = null, $dateTo = null, $agencyID = null)
    {
        $user   = User::findOne(['id' => $this->_userID]);
        if (empty($user)) {
            return [
                'success' => false,
                'message' => 'User not found'
            ];
        }
        $myIDs = $user->getMyChidrensIds();
        if (empty($myIDs) || (!is_array($myIDs) && $myIDs != 'admin')) {
            return [
                'lettersArray' => [], 
                'count' => 0
            ];
        }
        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, last_name, id')
            ->from([UserProfile::tableName()]);

        $subQuery2 = (new Query)
            ->select('agency.name, user.agency_id, user.id as agency_user_id')
            ->from(User::tableName())
            ->leftJoin(Agency::tableName(), 'user.agency_id = agency.id')
            ->where(['user_type' => User::USER_AGENCY]);

        $letters = (new Query())
            ->select('user.id as girl_user_id, action.created_at, letter.penalty_value, letter.action_id,letter.was_deleted_by_man,letter.was_deleted_by_girl,
             user_profile.id as from_user_id, user_profile.first_name as from_name, user_profile.last_name as from_last_name, 
             user_profile2.id as to_user_id, user_profile2.first_name as to_name, user_profile2.last_name as to_last_name,
             agency.agency_id, agency.name as agency_name, agency_user_id')
            ->from([Letter::tableName()])
            ->leftJoin(Action::tableName(), 'action.id = letter.action_id')
            ->leftJoin(User::tableName(), 'user.id = action.action_receiver')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
            ->leftJoin(['user_profile2' => $subQuery], 'user_profile2.id = action.action_receiver')
            ->leftJoin(['agency' => $subQuery2], 'agency.agency_id = user.agency_id')
            ->where(['>','letter.penalty_value', 0])
            ->andWhere(['user_type' => User::USER_FEMALE]);
        if (is_array($myIDs)) {
            $letters->andWhere(['in', 'action.action_receiver', $myIDs]);
        }

        if (!empty($dateFrom)) {
            $letters->andWhere(['>=', 'action.created_at', $dateFrom]);
        }
        if (!empty($dateTo)) {
            $letters->andWhere(['<=', 'action.created_at', $dateTo]);
        }
        if (!empty($agencyID)) {
            $letters->andWhere(['agency_user_id' => $agencyID]);
        }

        $letters->orderBy('letter.action_id DESC');

        $count = $letters->count();

        $result = $letters->all();

        return ['lettersArray' => $result, 'count' => $count];
    }

    public function updatePenaltyLettersItem()
    {
        $penaltyLetterTime = \Yii::$app->params['penalty_letter_time'];        
        $penaltyLetterTimeMax = \Yii::$app->params['penalty_letter_time_max'];        
        $billingService = new BillingService();
        $priceIncrement = $billingService->getPenaltyValue('letters');

        $letters = (new Query())
            ->select('action.id, action.created_at, action.action_receiver')
            ->from([LettersModel::tableName()])
            ->leftJoin(Action::tableName(), 'action.id = letter.action_id')
            ->leftJoin(User::tableName(), 'user.id = action.action_receiver')
            ->where(['letter.reply_status' => 0])
            ->andWhere(['user.user_type' => User::USER_FEMALE])
            ->andWhere(['<','action.created_at',date("Y-m-d H:i:s",time()-$penaltyLetterTime)])
            ->andWhere(['>','action.created_at',date("Y-m-d H:i:s",time()-$penaltyLetterTimeMax)]);
        $count = $letters->count();
        $result = $letters->all();

        $penaltyDateArray = [];
        if(!empty($result)){
            foreach($result as $letter) {
                //$pinalty_date = ((date("j",time() - strtotime($one['dt'])) - 1) < $pinalty_letter_time_max/86400) ? (date("j",time() - (strtotime($one['dt'])+$pinalty_letter_time)) - 1) : floor($pinalty_letter_time_max/86400-1);
                $penaltyDate = (time() - strtotime($letter['created_at']) < $penaltyLetterTimeMax) ? floor((time() - strtotime($letter['created_at']) - $penaltyLetterTime)/86400)+1 : floor($penaltyLetterTimeMax/86400-1);
                $penaltyDateArray[] = $penaltyDate;
                $result = Letter::updateAll(
                    ['penalty_value' => $penaltyDate * $priceIncrement],
                    ['action_id'=>$letter['action_id']]
                );
            }
        }

        return $count;

    }



}