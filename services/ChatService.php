<?php
namespace app\services;

use app\models\AgencyBonus;
use app\models\MissingChats;
use app\models\UserType;
use Yii;
use app\models\ActionTypes;
use app\models\Action;
use app\models\Chat;
use app\models\ChatSession;
use app\models\ChatInvite;
use app\models\User;
use app\models\UserProfile;
use app\models\Photo;
use yii\db\Query;
use yii\web\BadRequestHttpException;

class ChatService
{
    private $_userID    = null;
    private $_userType = null;
    
    function __construct($userID = null)
    {
        if ($userID !== null) {
            self::setUser($userID);
        }
    }

    public function setUser($userID)
    {
        $user = User::findOne(['id' => $userID]);
        if (empty($user)) {
            throw new BadRequestHttpException('user_id error', 1);
        }

        $this->_userID = $user->id;
        $this->_userType = $user->user_type;
    }

    public function Send($action_type, $skin_id, $user_to, $text, $from_user_type, $to_user_type, $sms_type = 0)
    {
        if ($action_type === null || $skin_id === null || !is_numeric($skin_id) || !is_numeric($user_to)) {
            return ['success' => FALSE, 'message' => 'Missing params'];
        }

        if ($this->_userID === null || $this->_user_type === null) {
            return ['success' => FALSE, 'message' => 'User error'];
        }

        (is_numeric($action_type)) ?
            $action_types_model = ActionTypes::findOne(['action_type_id' => $action_type]) :
            $action_types_model = ActionTypes::findOne(['code' => $action_type]);

        if (empty($action_types_model)) {
            return ['success' => FALSE, 'message' => 'Action type not found'];
        }

        $billing_model = new Billing();

        if ($from_user_type == 1) {
            $pay_user_id = $this->_userID;
        } else {
            $pay_user_id = $user_to;
        }

        $user              = new User($pay_user_id);
        $user_balance      = $user->payments->GetBalance();
        $chat_minute_price = $billing_model->getActionPrice($skin_id, $plan_id = 1, $action_one_chat_minute = 5, $pay_user_id);
        //return ['success' => FALSE, 'message' => $chat_minute_price];
        //$user = new User($this->_userId);
        //проверяем если мужик
        /*if ($this->_user_type == 1) {

            $user_balance = $user->payments->GetBalance();

            $billing_model = new Billing();
            $chats_price   = $billing_model->getActionPrice($skin_id, $plan_id = 1, $action_types_model->action_type_id, $this->_userId);
            if (!is_numeric($chats_price)) {
                return ['success' => FALSE, 'message' => 'chat price error'];
            }

            if ($user_balance < $chats_price) {
                return ['success' => FALSE, 'message' => 'not enough money'];
            }
        }*/

        $date_format = mktime(date("H"), date("i") - 30, date("s"), date("m"), date("d"), date("Y")); // тридцать минут назад

        $chat_session_model = ChatSession::find()
            ->where(['user_from' => $this->_userID, 'user_to' => $user_to])
            ->orWhere(['user_to' => $this->_userID, 'user_from' => $user_to])
            ->andWhere(['>=', 'date_update', date("Y-m-d H:i:s", $date_format)])
            ->andWhere(['status' => 1])
            ->orderBy('id DESC')
            ->one();
        if (empty($chat_session_model) && $from_user_type == 1 && $user_balance < $chat_minute_price) {
            return ['success' => FALSE, 'message' => 'Not enough money'];
        }

        //получаем последнее сообщение между двумя пользователями
        $last_message_model = null;

        if (!empty($chat_session_model)) {
            $last_message_model = (new Query())
                ->select('chat.answer_status, actions.dt, actions.action_id')
                ->from([ChatsModel::tableName() . ' chat'])
                ->leftJoin(Actions::tableName() . ' actions', 'actions.action_id = chat.action_id')
                ->where([
                    'chat.type' => 0,
                    'actions.action_type_id' => $action_types_model->action_type_id,
                    'actions.user_to'        => $this->_userID,
                    'actions.user_from'      => $user_to,
                ])
                ->orderBy('actions.dt DESC');
            $last_message_data  = $last_message_model->one();
        }

        $action_model                 = new Actions();
        $action_model->action_type_id = $action_types_model->action_type_id;
        $action_model->skin_id        = $skin_id;
        $action_model->user_from      = $this->_userID;
        $action_model->user_to        = $user_to;

        if (!$action_model->save()) {
            return ['success' => FALSE, 'message' => 'Action save error'];
        }

        if (!empty($chat_session_model)){
            //если один из учасников отвечает на первое сообщение в текущей сессии в течении 5 минут то агенство получает вознаграждение
            //если прошло больше 5 минут и нету ответа девушки, то фиксируем это. Сейчас фиксация просрочки так же происходит в кроне
            //получаем последнее входящее сообщение и смотрим его статус и дату отправки
            if (isset($last_message_data['answer_status']) && $last_message_data['answer_status'] == 0 && time() > strtotime($last_message_data['dt']) + 60*5) {
                //просрочка
                $missing_chats = new MissingChats();
                $missing_chats->chat_session_id = $chat_session_model->id;
                $missing_chats->action_id = $action_model->action_id;
                $missing_chats->user_from = $this->_userID;
                $missing_chats->user_to = $user_to;
                $ress = $missing_chats->save();
            } elseif($chat_session_model->start_pay_time === null) { //если оплаты по этой сессии еще не было
                //вовремя

                //проверяем это первое сообщение в этой сессии или нет, если первое то выставляем счет
                //найди мне все сообщения в этой сессии где я отправитель
                $isset_girls_message_model = (new Query())
                    ->select('chat.answer_status, actions.dt, actions.action_id')
                    ->from([ChatsModel::tableName() . ' chat'])
                    ->leftJoin(Actions::tableName() . ' actions', 'actions.action_id = chat.action_id')
                    ->where([
                        'actions.action_type_id' => $action_types_model->action_type_id,
                        'actions.user_to'        => $user_to,
                        'actions.user_from'      => $this->_userID,
                        'chat.chat_session_id'   => $chat_session_model->id,
                    ])
                    ->all();
                if (empty($isset_girls_message_model)) {
                    //если девушка не писала еще в этой сессии то выставляем счет
                    $user->payments->AddCharges($chat_session_model->action_id, $chat_minute_price);

                    $chat_session_model->start_pay_time = time();//добавляем временную метку о начале оплаты
                    $chat_session_model->duration       = 60;//и сразу ставит время 60 сек

                    //определяем ID девочки
                    $girs_id = ($from_user_type == 1) ? $user_to : $this->_userID;

                    //находим девочку и ее агенство
                    $women_model                        = Womans::findOne(['user_id' => $girs_id]);
                    if (!empty($women_model)) {
                        //даем бонус агенству
                        $agency_id = $women_model->agency_id;

                        $agency_minute_price = $billing_model->getAgencyPrice($skin_id, $plan_id = 1, $action_one_chat_minute = 5, $agency_id);

                        if (!empty($agency_minute_price)) {
                            $agency_bonus            = new AgencyBonus();
                            $agency_bonus->action_id = $chat_session_model->action_id;
                            $agency_bonus->user_id   = $agency_id;
                            $agency_bonus->amount    = $agency_minute_price;
                            if (!$agency_bonus->save()) {
                                return ['success' => FALSE, 'message' => 'Agency bonus not save'];
                            }
                            //$letters->pay_to_agency = 1;
                        }
                    }
                }
            }
        }

        if (!empty($last_message_model) && $last_message_data['answer_status'] == 0) {
            //update chat answer status
            $ress = ChatsModel::updateAll(['answer_status' => 1], ['action_id' => $last_message_data['action_id']]);
        }


        if (empty($chat_session_model)) {
            //return ['success' => FALSE, 'message' => 'Chat_session not found'];
            $chat_session_model            = new ChatSession();
            $chat_session_model->action_id = $action_model->action_id;
            $chat_session_model->user_from = ($from_user_type == 2) ? $user_to : $this->_userID; //чат сессию всегда создает мужчина
            $chat_session_model->user_to   = ($from_user_type == 2) ? $this->_userID : $user_to;
            if (!$chat_session_model->save()) {
                return ['success' => FALSE, 'message' => $chat_session_model->errors];
            }
        } /*else {
            //добавбяем длительность
            $chat_model = ChatsModel::find()
                ->where(['chat_session_id' => $chat_session_model['id']])
                ->orderBy('date_create ASC')
                ->one();

            if (empty($chat_model)) {
                return ['success' => FALSE, 'message' => 'chat_session not found'];
            }
            $diff_time = time() - strtotime($chat_model['date_create']); //разница в секундах

            ChatSession::updateAll(['duration' => $diff_time], "id = " . $chat_session_model['id']);

        }
*/

        $chat                  = new ChatsModel();
        $chat->action_id       = $action_model->action_id;
        $chat->chat_session_id = $chat_session_model->id;
        $chat->date_create     = date("Y-m-d H:i:s", time());
        $chat->text            = $text;
        $chat->type            = $sms_type;
        if (!$chat->save()) {
            return ['success' => FALSE, 'message' => 'Chat sms send'];
        }

        $chat_session_model->date_update = date('Y-m-d H:i:s', time());

        if (!$chat_session_model->save()) {
            return ['success' => FALSE, 'message' => 'Chat session not save'];
        }

        return ['success' => TRUE, 'message' => 'Chat sms send'];
    }

    public function delete($id, $status = 0)
    {
        if (!is_numeric($status) || !is_numeric($id)) {
            throw new BadRequestHttpException('Missing params', 1);
        }
        $action_model = Actions::findOne(['action_id' => $id, 'user_from' => $this->_userID]);
        if (empty($action_model)) {
            throw new BadRequestHttpException('Chat sms not found', 2);
        }

        $chat = ChatsModel::findOne(['action_id' => $id]);

        if (!$chat) {
            throw new BadRequestHttpException('Chat sms not found', 2);
        }
        $chat->status = $status;
        if (!$chat->save()) {
            throw new BadRequestHttpException('Chat sms status', 3);
        }

        return ['success' => TRUE, 'Chat sms delete'];
    }

    public function get($user_to, $chat_mess_type = 'all', $last_chat_mess_id = null)
    {
        if ($user_to === null) {
            return null;
        }
        $query = new Query();
        $chats = $query->addSelect(['actions.*', 'chat.*'])
            ->from([ChatsModel::tableName() . ' chat'])
            ->leftJoin(Actions::tableName() . ' actions', 'actions.action_id = chat.action_id')
            ->where(['actions.user_from' => $this->_userID, 'actions.user_to' => $user_to])
            ->orWhere(['actions.user_from' => $user_to, 'actions.user_to' => $this->_userID]);
        if ($chat_mess_type == 'new') {
            if (!empty($last_chat_mess_id)) {
                $chats->andWhere(['>', 'chat.action_id', (int)$last_chat_mess_id]);
            }
        }
        $ress = $chats->orderBy('actions.dt ASC')->all();

        return $ress;
    }

    //обновляет сессию в чате и снимает плату за время
    public function updateChatSession($skin_id, $user_to)
    {
        if ($this->_userID === null || $this->_user_type === null) {
            return ['success' => FALSE, 'message' => 'User_id error'];
        }

        //проверяем если мужик
        if ($this->_user_type != 1) {
            return ['success' => TRUE, 'message' => 'Girls chat session'];
        }

        $user = new User($this->_userId);

        $user_balance = $user->payments->GetBalance();

        $billing_model     = new Billing();
        $chat_minute_price = $billing_model->getActionPrice($skin_id, $plan_id = 1, $action_type_id = 5, $this->_userId);

        if (!is_numeric($chat_minute_price)) {
            return ['success' => FALSE, 'message' => 'Chat price error'];
        }

        $date_format = mktime(date("H"), date("i") - 30, date("s"), date("m"), date("d"), date("Y")); // 30 минут назад

        $chat_session_model = ChatSession::find()
            ->where(['user_from' => $this->_userId, 'user_to' => $user_to])
            ->orWhere(['user_to' => $this->_userId, 'user_from' => $user_to])
            ->andWhere(['>=', 'date_update', date("Y-m-d H:i:s", $date_format)])
            ->andWhere(['status' => 1])
            ->one();

        $duration_interval = 60; //60 sec
        if (empty($chat_session_model)) {
            return ['success' => TRUE, 'message' => 'Not active chat session'];
        }

        if ($user_balance < $chat_minute_price) {
            $chat_session_model->status = 0;
            $chat_session_model->save();
            //send sms service stop chat
            self::Send(3, $skin_id, $user_to, '<span class="name">Service</span><span class="text">stop chat</span>', 1, 2, 1);
            return ['success' => FALSE, 'message' => 'Not enough money'];
        }



        //$chat_session_model->date_update = date("Y-m-d H:i:s", time());
        //проверяем если сессия имеет метку что чат стартовал и пора за него платить то начинаем считать время

        if ($chat_session_model->start_pay_time === null) {
            return ['success' => TRUE, 'message' => 'Not start pay time'];
        }
        //сравниваем время c учетом что уже оплачено за 1 минуту
        //если разница больше в 2 раза, то выставляем счет
        $diff = time() - $chat_session_model->start_pay_time+$duration_interval;
        if ($diff > $chat_session_model->duration) {
            if (floor($diff / $duration_interval) > floor($chat_session_model->duration/$duration_interval)) {//пока количество целых минут прошедшего времни больше имеющегося, то пора оплачивать следующую
                $chat_session_model->duration += $duration_interval;
                if (!$chat_session_model->save()) {
                    return ['success' => FALSE, 'message' => 'Chat_session save error'];
                }
                $last_charges_id = $user->payments->GetChargesIdByActionId($chat_session_model->action_id);
                if(!$last_charges_id){
                    return ['success' => FALSE, 'message' => 'user charge not found'];
                }

                $charges_amount = $user->payments->getChargesAmount($last_charges_id);

                if ($charges_amount === FALSE || !is_numeric($charges_amount)) {
                    return ['success' => FALSE, 'message' => 'Charges_amount not found'];
                }

                $amount = $charges_amount + $chat_minute_price;

                $ress = $user->payments->updateCharges($last_charges_id, $amount);

                if (!$ress) {
                    return ['success' => FALSE, 'message' => 'Charges amount update error'];
                }
                //добавляем вонус агенству
                $women_model = Womans::findOne(['user_id' => $user_to]);
                if (!empty($women_model)) {
                    //даем бонус агенству
                    $agency_id = $women_model->agency_id;

                    $agency_minute_price = $billing_model->getAgencyPrice($skin_id = 1, $plan_id = 1, $action_one_chat_minute = 5, $agency_id);

                    $agency_bonus            = new AgencyBonus();
                    $agency_bonus->updateBonus($agency_id,$chat_session_model->action_id,$agency_minute_price);
                    /*
                     if (!empty($agency_minute_price)) {
                        $agency_bonus            = new AgencyBonus();
                        $agency_bonus->action_id = $action_model->action_id;
                        $agency_bonus->user_id   = $agency_id;
                        $agency_bonus->amount    = $agency_minute_price;
                        if (!$agency_bonus->save()) {
                            return ['success' => FALSE, 'message' => 'Agency bonus not save'];
                        }
                        //$letters->pay_to_agency = 1;
                    }*/
                }

                return ['success' => TRUE, 'message' => 'Chat session update and сharges amount add'];

            }
        }

        return ['success' => TRUE, 'message' => 'Chat session don\'t need to update'];


    }

    public function stopChatSession($user_to)
    {
        $date_format = mktime(date("H"), date("i") - 30, date("s"), date("m"), date("d"), date("Y")); // 30 минут назад

        if ($user_to == 0) {
            $ress = ChatSession::updateAll(['status' => 0], 'user_from = ' . $this->_userId . ' AND date_update >= "' . date("Y-m-d H:i:s", $date_format) . '"');

            return ['success' => $ress, 'message' => 'All chat session stop'];
        }

        $chat_session_model = ChatSession::find()
            ->where(['user_from' => $this->_userId, 'user_to' => $user_to])
            ->andWhere(['>=', 'date_update', date("Y-m-d H:i:s", $date_format)])
            ->orderBy('date_create DESC')
            ->one();
        if (empty($chat_session_model)) {
            return ['success' => FALSE, 'message' => 'Chat session not found'];
        }

        $chat_session_model->status = 0;
        if (!$chat_session_model->save()) {
            return ['success' => FALSE, 'message' => 'Chat session save error'];
        }

        return ['success' => TRUE, 'message' => 'Chat session stoped'];

    }

    public function getIviteList($limit = FALSE)
    {
        $query  = new Query();
        $womans = $query->addSelect(['chat_invite.*', 'user_pers.first_name', 'user_pers.user_id'])
            ->from([ChatInvite::tableName() . ' chat_invite'])
            ->leftJoin(UserPersonal::tableName() . ' user_pers', 'chat_invite.user_id = user_pers.user_id')
            ->where(['chat_invite.other_user_id' => $this->_userId]);
        if ($limit) {
            $womans->limit($limit);
        }
        $ress = $womans->all();

        return $ress;

    }

    public function readIvite($id = FALSE)
    {
        if (!$id) return FALSE;
        $ress = ChatInvite::updateAll(['date_view' => date("Y-m-d H:i:s", time())], ['id' => $id]);

        return $ress;
    }

    public function getLastChatUsersId($limit)
    {
        $chat_session_model = ChatSession::find();
        if ($this->_user_type == 1) {
            $chat_session_model
                ->select('user_to as other_user_id, max(date_update) as maxtime')
                ->where(['user_from' => $this->_userId]);
        } elseif ($this->_user_type == 2) {
            $chat_session_model
                ->select('user_from as other_user_id, max(date_update) as maxtime')
                ->where(['user_to' => $this->_userId]);
        }
        $chat_session_model->orderBy('maxtime DESC');

        if (!empty($limit)) {
            $chat_session_model->limit($limit);
        }
        $ress = $chat_session_model
            ->groupBy('other_user_id')
            ->asArray()
            ->all();

        if (empty($ress)) {
            return null;
        }

        foreach ($ress as $one) {
            $ids[] = $one['other_user_id'];
        }

        return $ids;
    }

    public function getNewMessageCount()
    {
        $chats = (new Query())
            ->from([Chat::tableName()])
            ->leftJoin(Action::tableName(), 'action.id = chat.action_id')
            ->leftJoin(ChatSession::tableName(), 'chat_session.id = chat.chat_session_id')
            ->where(['action_receiver' => $this->_userID, 'chat.readed_at' => null])
            ->andWhere(['not', ['message' => null]]);
        $count = $chats->count();

        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, birthday, id')
            ->from([UserProfile::tableName()]);
        //подзапрос на фото пользователя
        $subQuery2 = (new Query)
            ->select('user.id, user.last_activity, photo.small_thumb')
            ->from([User::tableName()])
            ->leftJoin(Photo::tableName(), 'avatar_photo_id = photo.id');

        $lastName = $chats
            ->addSelect('user_profile.first_name, user_profile.birthday, photo.last_activity, chat_session.active, photo.small_thumb, message, action_creator, count(*) as count')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action_creator')
            ->leftJoin(['photo' => $subQuery2], 'photo.id = action_creator')
            ->orderBy('action.id DESC')
            ->groupBy('action_creator')
            ->all();

        return [
            'count' => $count, 
            'users' => $lastName
        ];

    }

    public function getNewMessageDetailCount()
    {
        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, birthday, id')
            ->from([UserProfile::tableName()]);

        //подзапрос на фото пользователя
        $subQuery2 = (new Query)
            ->select('user.id, user.last_activity, photo.small_thumb')
            ->from([User::tableName()])
            ->leftJoin(Photo::tableName(), 'user.avatar_photo_id = photo.id');

        $chat = (new Query())
            ->select(['COUNT(action.action_creator) AS unreaded_messages', 'action.action_creator', 'first_name', 'birthday', 'last_activity', 'small_thumb', 'count(*) as count'])
            ->from([Chat::tableName()])
            ->leftJoin(Action::tableName(), 'action.id = chat.action_id')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
            ->leftJoin(['user' => $subQuery2], 'user.id = action.action_creator')
            ->where(['action.action_receiver' => $this->_userID, 'chat.readed_at' => null])
            ->andWhere(['>=', 'action.created_at', date('Y-m-d H:i:s', time() - 60 * 60 * 12)])
            ->groupBy('action.action_creator')
            ->all();

        return $chat;
    }

    public function readMessage($other_user_id)
    {
        $chat_model = (new Query())
            ->select(['chat.action_id'])
            ->from([ChatsModel::tableName() . ' chat'])
            ->leftJoin(Actions::tableName() . ' action', 'action.action_id = chat.action_id')
            ->where(['action.user_to' => $this->_userId, 'action.user_from' => $other_user_id, 'chat.date_read' => null])
            ->indexBy('action_id');
        $chat_ids   = $chat_model->all();
        if (!empty($chat_ids)) {
            //читаем все непрочитанные смс
            $ress = ChatsModel::updateAll(['date_read' => date('Y-m-d H:i:s', time())], ['action_id' => $chat_ids]);
            //только для мужчин
            /*if ($this->_user_type == 1) {
                //определяем, если девушка написала первая то при прочтении письма с мужика снимаем деньги
                //проверяем это первое сообщение в этой сессии или нет, если первое то выставляем счет
                $date_format = mktime(date("H"), date("i") - 30, date("s"), date("m"), date("d"), date("Y")); // тридцать минут назад

                $chat_session_model = ChatSession::find()
                    ->where(['user_from' => $this->_userId, 'user_to' => $other_user_id])
                    ->orWhere(['user_to' => $this->_userId, 'user_from' => $other_user_id])
                    ->andWhere(['>=', 'date_update', date("Y-m-d H:i:s", $date_format)])
                    ->andWhere(['status' => 1])
                    ->orderBy('id DESC')
                    ->one();
                if (empty($chat_session_model)) {
                    return ['success' => FALSE, 'message' => 'Chat session not found or not active'];
                }
                $isset_girls_message_model = (new Query())
                    ->select('chat.answer_status, actions.dt, actions.action_id')
                    ->from([ChatsModel::tableName() . ' chat'])
                    ->leftJoin(Actions::tableName() . ' actions', 'actions.action_id = chat.action_id')
                    ->where([
                        'actions.user_to'      => $other_user_id,
                        'actions.user_from'    => $this->_userId,
                        'chat.chat_session_id' => $chat_session_model->id,
                    ])
                    ->all();
                if (empty($isset_girls_message_model)) {
                    //если мужчина не писала еще в этой сессии то выставляем ему счет
                    $user              = new User($this->_userId);
                    $user_balance      = $user->payments->GetBalance();
                    $billing_model     = new Billing();
                    $chat_minute_price = $billing_model->getActionPrice($skin_id = 1, $plan_id = 1, $action_one_chat_minute = 5, $this->_userId);
                    if ($user_balance < $chat_minute_price) {
                        return ['success' => FALSE, 'message' => 'Not enough money'];
                    }

                    $user->payments->AddCharges($chat_session_model->action_id, $chat_minute_price);

                    $chat_session_model->start_pay_time = time();//добавляем временную метку о начале оплаты
                    $chat_session_model->duration       = 60;//и сразу ставим время 60 сек
                    if (!$chat_session_model->save()) {
                        return ['success' => FALSE, 'message' => 'Chat session not save'];
                    }
                    $women_model = Womans::findOne(['user_id' => $other_user_id]);
                    if (!empty($women_model)) {
                        //даем бонус агенству
                        $agency_id = $women_model->agency_id;

                        $agency_minute_price = $billing_model->getAgencyPrice($skin_id = 1, $plan_id = 1, $action_one_chat_minute = 5, $agency_id);

                        if (!empty($agency_minute_price)) {
                            $agency_bonus            = new AgencyBonus();
                            $agency_bonus->action_id = $chat_session_model->action_id;
                            $agency_bonus->user_id   = $agency_id;
                            $agency_bonus->amount    = $agency_minute_price;
                            if (!$agency_bonus->save()) {
                                return ['success' => FALSE, 'message' => 'Agency bonus not save'];
                            }
                            //$letters->pay_to_agency = 1;
                        }
                    }
                }
            }*/

        }

        return ['success' => TRUE, 'message' => 'chat sms read'];
    }


    public function getActiveChatUsersIds()
    {
        $chat_session_model = ChatSession::find();
        if ($this->_user_type == 1) {
            $chat_session_model
                ->select('user_to as other_user_id')
                ->where(['user_from' => $this->_userId]);
        } elseif ($this->_user_type == 2) {
            $chat_session_model
                ->select('user_from as other_user_id')
                ->where(['user_to' => $this->_userId]);
        }
        $date_format = mktime(date("H"), date("i") - 5, date("s"), date("m"), date("d"), date("Y")); // пять минут назад
        $chat_session_model
            ->andWhere(['status' => 1])
            ->andWhere(['>=', 'date_update', date("Y-m-d H:i:s", $date_format)])
            ->indexBy('other_user_id');

        /*$ress = $chat_session_model
            ->asArray()
            ->all();

        if (empty($ress)) {
            return null;
        }

        foreach ($ress as $one) {
            $ids[] = $one['other_user_id'];
        }*/

        return $chat_session_model;
    }

    public function checkMissingChats()
    {
        //получаем последнее чат смс в сессии чата
        $subQuery = (new Query)
            ->select('max(chat.action_id) as max_action_id, chat.chat_session_id, chat.answer_status')
            ->from([ChatsModel::tableName() . ' chat'])
            ->where(['chat.type'=>0])
            ->groupBy('chat.chat_session_id');

        $subQuery2 = (new Query)
            ->select('chat.action_id, chat.answer_status')
            ->from([ChatsModel::tableName() . ' chat']);

        $miss_chat_sms = (new Query())
            ->from([ChatSession::tableName() . ' chat_session'])
            ->select('chat.chat_session_id, actions.action_id, actions.user_from, actions.user_to, users.user_types, actions.dt, chat2.answer_status')
            ->leftJoin(['chat' => $subQuery ], 'chat.chat_session_id = chat_session.id')
            ->leftJoin(['chat2' => $subQuery2 ], 'chat2.action_id = chat.max_action_id')
            ->leftJoin(Actions::tableName() . ' actions', 'actions.action_id = chat.max_action_id')
            ->leftJoin(Users::tableName() . ' users', 'users.user_id = actions.user_to')
            ->where(['chat_session.status' => 1])
            ->andWhere(['<','actions.dt',date('Y-m-d H:i:s',time()-300)]);//неотвеченные более 5 мин назад

        $ress = $miss_chat_sms->all();

        if(!empty($ress)){
            foreach($ress as $one){
                if($one['answer_status'] != 1){
                    //if($one['user_types'] == 2){//если девушка
                        $missing_chats = new MissingChats();
                        $missing_chats->load(['MissingChats'=>$one]);
                        $ress = $missing_chats->save();
                    //}
                }
                //закрываем чат
                self::setUser($one['user_from']);
                self::Send(3, 1, $one['user_to'], '<span class="name">Service</span><span class="text">Chat stopped. User was not active during a long time</span>', ($one['user_types'] == 1) ? 2 : 1, $one['user_types'], 1);
                ChatSession::updateAll(['status'=>0],['id'=>$one['chat_session_id']]);
            }
        }

        return json_encode(['success' => TRUE,'ress'=>$ress]);
    }

    public function getMissingChats()
    {
        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, user_id')
            ->from([UserPersonal::tableName() . ' user_pers']);

        $missing_chats_model = (new Query)
            ->select('missing_chats.*,missing_chats.date_create as date_fix, user_pers.first_name, actions.dt as date_create, chat.text')
            ->from([MissingChats::tableName() . ' missing_chats'])
            ->leftJoin(Actions::tableName() . ' actions', 'actions.action_id = missing_chats.action_id')
            ->leftJoin(ChatsModel::tableName() . ' chat', 'chat.action_id = missing_chats.action_id')
            ->leftJoin(['user_pers' => $subQuery], 'user_pers.user_id = actions.user_from')
            ->where(['missing_chats.user_to'=>$this->_userId])
            ->orderBy('actions.action_id DESC');
        $ress = $missing_chats_model->all();

        return $ress;
    }

    public function checkActiveChatSession($skin_id,$user_to_id,$create)
    {
        $date_format = mktime(date("H"), date("i") - 10, date("s"), date("m"), date("d"), date("Y")); // 10 минут назад
        $chat_session_model = ChatSession::find()
            ->where(['user_from' => $this->_userId, 'user_to' => $user_to_id])
            ->orWhere(['user_to' => $this->_userId, 'user_from' => $user_to_id])
            ->andWhere(['>=', 'date_update', date("Y-m-d H:i:s", $date_format)])
            ->andWhere(['status' => 1])
            ->orderBy('id DESC')
            ->one();

        //если есть сессия и включена оплата
        if (!empty($chat_session_model) && $chat_session_model->start_pay_time != null)
            return TRUE;

        //если есть сессия но еще не включена оплата
        if (!empty($chat_session_model) && $chat_session_model->start_pay_time === null && $create) {
            $chat_session_model->start_pay_time = time();//добавляем временную метку о начале оплаты
            $chat_session_model->duration = 60;//и сразу ставит время 60 сек

            if (!$chat_session_model->save()) {
                return FALSE;
            }
            //addChargesToMan
            $user_pay_ress = $this->addChargesToMan($skin_id,$chat_session_model->action_id);
            if(!$user_pay_ress['success'])
                return FALSE;
            //pay to agency
            $pay_agency_ress = $this->payBonusToAgency($skin_id,$user_to_id,$chat_session_model->action_id);
            if(!$pay_agency_ress)
                return FALSE;

            return TRUE;
        }

        if (empty($chat_session_model) && $create) {
            //создаем екшен
            $action_id = $this->createActionForChat($skin_id,$user_to_id);
            if(!$action_id)
                return FALSE;
            //создаем сессию
            $chat_session_id = $this->createChatSession($action_id,$user_to_id,true);
            if(!$chat_session_id)
                return FALSE;
            //addChargesToMan
            $user_pay_ress = $this->addChargesToMan($skin_id,$action_id);
            if(!$user_pay_ress['success'])
                return FALSE;
            //pay to agency
            $pay_agency_ress = (new AgencyBonus())->createBonus($user_to_id,$action_id,5,$skin_id,$plan_id=1);
            if(!$pay_agency_ress)
                return FALSE;

            return TRUE;
        }

        return FALSE;
    }

    private function createActionForChat($skin_id,$user_to_id)
    {
        $action_model                 = new Actions();
        $action_model->action_type_id = 5;//chat 1 minute
        $action_model->skin_id        = $skin_id;
        $action_model->user_from      = $this->_userId;
        $action_model->user_to        = $user_to_id;

        if ($action_model->save()) {
            return $action_model->action_id;
        }

        return FALSE;
    }

    private function createChatSession($action_id,$user_to_id,$pay=false){
        $chat_session_model            = new ChatSession();
        $chat_session_model->action_id = $action_id;
        $chat_session_model->user_from = $this->_userId;
        $chat_session_model->user_to   = $user_to_id;
        if($pay) {
            $chat_session_model->start_pay_time = time();//добавляем временную метку о начале оплаты
            $chat_session_model->duration       = 60;//и сразу ставит время 60 сек
        }
        if ($chat_session_model->save()) {
            return $chat_session_model->id;
        }

        return FALSE;
    }

    /*public function payBonusToAgency($user_to_id,$skin_id,$action_id){
        $women_model = Womans::findOne(['user_id' => $user_to_id]);
        if (empty($women_model))
            return FALSE;

        //даем бонус агенству
        $agency_id = $women_model->agency_id;

        $billing_model = new Billing();
        $agency_minute_price = $billing_model->getAgencyPrice($skin_id, $plan_id = 1, 5, $agency_id);

        if (empty($agency_minute_price))
            return FALSE;

        $agency_bonus            = new AgencyBonus();
        $agency_bonus->action_id = $action_id;
        $agency_bonus->user_id   = $agency_id;
        $agency_bonus->amount    = $agency_minute_price;
        if ($agency_bonus->save()) {
            return TRUE;
        }

        return FALSE;
    }*/

    private function addChargesToMan($skin_id,$action_id){
        $user = new User($this->_userId);
        $billing_model = new Billing();
        $chat_minute_price = $billing_model->getActionPrice($skin_id, $plan_id = 1, $action_one_chat_minute = 5, $this->_userId);
        $user_pay_ress = $user->payments->AddCharges($action_id, $chat_minute_price);
        return $user_pay_ress;
    }

    public function getUnreadedMessages($otherUserID)
    {
        $query = Chat::find()
                ->select(['chat.readed_at'])                
                ->leftJoin(Action::tableName(), 'action.id = chat.action_id')                
                ->where(['chat.readed_at' => null])
                ->andWhere(['action_creator' => $otherUserID, 'action_receiver' => $this->_userID])
                ->andWhere(['not', ['message' => null]]);
        $query->limit(100);        
        $result = $query->count();
        return $result;       
    }

}