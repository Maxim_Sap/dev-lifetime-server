<?php
namespace app\services;

use app\models\Action;
use app\models\ActionType;
use app\models\PricelistPenalty;
use app\models\OtherAgencyPenalty;
use Yii;
use app\models\Pricelist;
use app\models\PricelistAgency;
use app\models\AgencyRevard;
use app\models\UserProfile;
use yii\db\Query;
use yii\db\Expression;

class BillingService
{
    public function getActionPrice($skinID, $planID, $actionTypeID, $userID)
    {
        $date = date("Y-m-d H:i:s", time());
        // ищем цену для конкретного пользователя с ограниченым периодом времени
        $query1 = (new Query())->select(['price'])
            ->from(Pricelist::tableName())
            ->where(['skin_id' => $skinID, 'plan_id' => $planID, 'action_type_id' => $actionTypeID]);        
        $query1 = $query1->andWhere(['or', ['user_id' => $userID], ['user_id' => null]]);        
        $query1 = $query1->andWhere(['<=', 'start_date', $date])
            ->andWhere(['or', ['>=', 'stop_date', $date], ['stop_date' => null]]);
            
        $query = $query1
            ->orderBy('user_id DESC, start_date DESC')
            ->limit(1);

        $result = $query->one();

        return $result['price'];
    }

    public function getAgencyPrice($skinID, $planID, $actionTypeID, $userID)
    {
        $date = date("Y-m-d H:i:s", time());        
        // ищем цену для конкретного пользователя с ограниченым периодом времени
        $query1 = (new Query())->select(['price', 'order' => new Expression(0)])
            ->from(PricelistAgency::tableName())            
            ->where(['skin_id' => $skinID, 'plan_id' => $planID, 'action_type_id' => $actionTypeID]);        
        $query1 = $query1->andWhere(['or', ['user_id' => $userID], ['user_id' => null]]);        
        $query1 = $query1->andWhere(['<=', 'start_date', $date])
            ->andWhere(['or', ['>=', 'stop_date', $date], ['stop_date' => null]]);
            
        $query = $query1
            ->orderBy('user_id DESC, start_date DESC')
            ->limit(1);
        $result = $query->one();

        return $result['price'];
    }

    public function getAgencyBalance($userID, $dateFrom = FALSE, $dateTo = FALSE)
    {
        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, last_name, id')
            ->from([UserProfile::tableName()]);

        $query = (new Query())
                ->select('bonus.*, action_type.name as action_name, 
                user_profile.first_name as user_from_first_name, user_profile.last_name as user_from_last_name, user_profile.id as user_from_user_id, 
                user_profile2.first_name as user_to_first_name, user_profile2.last_name as user_to_last_name, user_profile2.id as user_to_user_id')
                ->from(AgencyRevard::tableName() ." bonus")
                ->leftJoin(Action::tableName(), 'action.id = bonus.action_id')
                ->leftJoin(ActionType::tableName(), 'action_type.id = action.action_type_id')
                ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
                ->leftJoin(['user_profile2' => $subQuery], 'user_profile2.id = action.action_receiver')
                ->where(['bonus.user_id' => $userID]);
        if ($dateFrom && $dateTo) {
            $query->andWhere(['>=', 'bonus.created_at', $dateFrom])
                ->andWhere(['<', 'bonus.created_at', $dateTo]);
        }
        $result = $query->all();

        return $result;

    }

    public function getAgencyTotalBalance($userID, $dateFrom = FALSE, $dateTo = FALSE)
    {
        $query = (new Query())
            ->select('sum(bonus.amount) as total_amount')
            ->from(AgencyRevard::tableName() . " bonus")
            ->where(['bonus.user_id' => $userID]);
        if ($dateFrom && $dateTo) {
            $query->andWhere(['>=', 'bonus.created_at', $dateFrom])
                ->andWhere(['<', 'bonus.created_at', $dateTo]);
        }
        $bonus = $query->all();

        $query = (new Query())
            ->select('sum(agency_penalty.penalty_value) as total_penalty_value')
            ->from([OtherAgencyPenalty::tableName() . ' agency_penalty'])
            ->where(['agency_penalty.user_id' => $userID]);
        if ($dateFrom && $dateTo) {
            $query->andWhere(['>=', 'agency_penalty.created_at', $dateFrom])
                ->andWhere(['<', 'agency_penalty.created_at', $dateTo]);
        }
        $agencyTotalPenalty = $query->all();

        return [
            'bonus'                => $bonus,
            'agency_total_penalty' => $agencyTotalPenalty,
        ];;

    }

    public function getPenaltyValue($type)
    {
        switch ($type){
            case 'gifts':
                $type_id = 2;
                break;
            case 'letters':
                $type_id = 1;
                break;
            default :
                $type_id = 1;
                break;
        }
        $model = PricelistPenalty::findOne(['type_id'=>$type_id]);

        return $model->price;

    }

 }