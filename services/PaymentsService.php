<?php
namespace app\services;

use app\models\AgencyRevard;
use app\models\City;
use app\models\Country;
use app\models\Location;
use Yii;
use yii\web\BadRequestHttpException;
use app\models\User;
use app\models\UserTransfer;
use app\models\TransferType;
use app\models\Credit;
use app\models\Action;
use app\models\Agency;
use app\models\ActionCredit;
use app\models\ActionType;
use app\models\ChatSession;
use app\models\Chat;
use app\models\Photo;
use app\models\VideoChat;
use app\models\UserProfile;
use app\models\PaymentsSchedule;
use yii\db\Query;
use yii\db\Expression;


class PaymentsService
{
    private $userID = null;
    private $user_type = null;

    function __construct($userID = null)
    {
        if ($userID !== null) {
            self::setUser($userID);
        }
    }

    public function setUser($userID)
    {
        $user = User::findOne(['id' => $userID]);
        if (empty($user)) {
            return [
                'success' => false,
                'message' => 'User not found'
            ];
        }

        $this->userID = $user->id;
        $this->userType = $user->user_type;
    }

    public static function sanitize($code)
    {
        return str_replace(' ', '_', strtolower(trim($code)));
    }

    function getBalance()
    {
        //получаем приход денег по конкретному пользователю
        $transfer = UserTransfer::find()
            ->where(['user_id' => $this->userID, 'deposit' => true])
            ->sum('amount');

        $userDebet = (!empty($transfer)) ? (float)$transfer : 0;

        //получаем рсход денег по конкретному пользователю
        $credit = Credit::find()
            ->where(['user_id' => $this->userID])
            ->sum('amount');

        $userCredit  = (!empty($credit)) ? (float)$credit : 0;
        $userBalance = $userDebet - $userCredit;

        return round($userBalance, 2);
    }

    function addFunds($amount, $code, $random_key = '')
    {

        $checkPaymentKey = UserProfile::findOne(['id'=>$this->userID,'payment_key'=>$random_key]);
        if (empty($checkPaymentKey)) {
            return [
                'success' => false,
                'message' => 'Missing params'
            ];
        }

        $code = self::sanitize($code);

        $transferType = TransferType::findOne(['code' => $code]);

        if (empty($transferType)) {
            return [
                'success' => false,
                'message' => 'Transfer type code not found'
            ];
        }

        $transfer                = new UserTransfer();
        $transfer->transfer_type_id = $transferType->id;
        $transfer->user_id = $this->userID;
        $transfer->deposit       = true;
        $transfer->amount        = $amount;
        $transfer->fee           = $amount*0.01;

        if (!$transfer->save()) {
            return [
                'success' => false,
                'message' => 'Can\'t save data in database'
            ];
        }

        return ['success' => true];
    }

    function addCredits($actionID, $amount)
    {
        $credit          = new Credit();
        $credit->user_id = $this->userID;
        $credit->amount  = $amount;
        if (!$credit->save()) {
            return [
                'success' => false,
                'message' => 'Save credit error'
            ];
        }

        $actionCredit = new ActionCredit();
        $actionCredit->action_id = $actionID;
        $actionCredit->credit_id = $credit->id;

        if (!$actionCredit->save()) {
            return [
                'success' => false,
                'message' => 'Save error'
            ];
        }

        return ['success' => true];
    }

    function getCreditIdByActionId($actionID)
    {
        $joinTable = ActionCredit::findOne(['action_id' => $actionID]);
        if (empty($joinTable)) {
            return false;
        }

        return $joinTable->credit_id;
    }

    function getCreditAmount($creditID)
    {
        $credit = Credit::findOne(['id' => $creditID]);
        if (empty($credit)) {
            return false;
        }

        return $credit->amount;
    }

    function updateCredits($creditID, $amount)
    {
        $credit = Credit::findOne(['id' => $creditID]);
        if (empty($credit)) {
            return false;
        }
        $credit->amount = $amount;

        if (!$credit->save()) {
            return false;
        }

        return true;
    }

    function updateCreditsByActionId($actionID, $amount)
    {
        $credit = Credit::findOne(['id' => $this->getCreditIdByActionId($actionID)]);

        if (empty($credit)) {
            return false;
        }
        $credit->amount = $amount;

        if (!$credit->save()) {
            return false;
        }

        return true;
    }

    function getOneUserOrderHistory()
    {
        if (empty($this->userID)) return null;

        $subqueryChat = (new Query())
            ->select('duration, action_id')
            ->from([ChatSession::tableName()])
            ->leftJoin(Chat::tableName(), 'chat.chat_session_id = chat_session.id');

        $subqueryVideoChat = (new Query())
            ->select('duration, action_id')
            ->from([VideoChat::tableName()]);

        $query1 = (new Query())
            ->select(['action.id as order_id','chat_session.duration as chat_duration','video_chat.duration as video_chat_duration','action.action_receiver','credit.amount', 'credit.created_at', 'pay' => new Expression(0), 'action_type.name', 'action_type.description'])
            ->from([Credit::tableName()])
            ->leftJoin(ActionCredit::tableName(), 'action_credit.credit_id = credit.id')
            ->leftJoin(Action::tableName(), 'action.id = action_credit.action_id')
            ->leftJoin(ActionType::tableName(), 'action_type.id = action.action_type_id')
            ->leftJoin(['chat_session'=> $subqueryChat], 'chat_session.action_id = action.id')
            ->leftJoin(['video_chat_session'=> $subqueryVideoChat], 'video_chat.action_id = action.id')
            ->where(['user_id' => $this->userID])
            ->limit(1000000);

        $query2 = (new Query())
            ->select(['user_transfer.id as order_id', 'chat_duration' => new Expression('null'),'video_chat_duration' => new Expression('null'), 'action_receiver' => new Expression(0),'user_transfer.amount', 'user_transfer.created_at', 'pay' => new Expression(1), 'transfer_type.type as name', 'transfer_type.description'])
            ->from([UserTransfer::tableName()])
            ->leftJoin(TransferType::tableName(), 'transfer_type.id = user_transfer.transfer_type_id')
            ->where(['user_id' => $this->userID])
            ->limit(1000000);

        $query1->union($query2);

        $query = (new Query())
            ->select('chat_duration, video_chat_duration, order_id, action_receiver, amount, created_at, name, description, pay')
            ->from(['amount' => $query1])
            ->orderBy('created_at DESC')
            ->all();

        return $query;

    }

    public function getUserFinanceInfo($otherUserID, $searchParams)
    {

        $dateFrom = isset($searchParams['dateFrom']) && strtotime($searchParams['dateFrom']) ? $searchParams['dateFrom'] : date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
        $dateTo   = isset($searchParams['dateTo']) && strtotime($searchParams['dateTo']) ? $searchParams['dateTo'].' 23:59:59' : date('Y-m-d', time());

        $owner_user_type = $this->userType;
        //определяем тип пользователя
        self::setUser($otherUserID);
        $selectFrom = 'action_creator';
        $selectTo = 'action_receiver';

        if($this->userType == User::USER_MALE) {
            $selectFrom = 'action_receiver';
            $selectTo = 'action_creator';
        }

        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, id')
            ->from([UserProfile::tableName()]);

        $actionType = ActionType::findOne(['name' => 'send letter']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }

        //получаем сумму потраченную за услугу
        $subQuery2 = (new Query)
            ->select(['credit.amount','action_credit.action_id'])
            ->from([Credit::tableName()])
            ->leftJoin(ActionCredit::tableName(), 'action_credit.credit_id = credit.id');

        //получаем сумму заплаченную агенствам за услугу
        $subQuery3 = (new Query)
            ->select(['AgencyRevard.amount', 'AgencyRevard.action_id'])
            ->from([AgencyRevard::tableName() . ' AgencyRevard']);

        //letters
        $lettersQuery = (new Query())
            ->select(['action.created_at', 'action.'.$selectFrom.' as other_user_id','user_profile.first_name','credit.amount'])
            ->from([Action::tableName()])
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.'.$selectFrom);
        if (in_array($owner_user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            $lettersQuery->leftJoin(['credit' => $subQuery3], 'credit.action_id = action.id');
        }else{
            $lettersQuery->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id');
        }

        $lettersQuery->where(['action.action_type_id' => $actionType->id, 'action.'.$selectTo => $otherUserID])
            ->andwhere(['>=', 'action.created_at', $dateFrom])
            ->andwhere(['<=', 'action.created_at', $dateTo])
        ;
        $lettersCount = $lettersQuery->count();
        $lettersArray  = $lettersQuery->all();
        $letters       = ['count' => $lettersCount, 'lettersArray' => $lettersArray];

        //chat

        $chatQuery = (new Query())
            ->select(['chat_session.created_at', 'chat_session.duration', 'action.'.$selectFrom.' as otherUserID','user_profile.first_name','credit.amount'])
            ->from([ChatSession::tableName()])
            ->leftJoin(Chat::tableName(), 'chat.chat_session_id = chat_session.id')
            ->leftJoin(Action::tableName(), 'chat.action_id = action.id')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.'.$selectFrom);
        if (in_array($owner_user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            $chatQuery->leftJoin(['credit' => $subQuery3], 'credit.action_id = action.id');
        }else{
            $chatQuery->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id');
        }
        $chatQuery->where(['action.'.$selectTo => $otherUserID])
            ->andWhere('credit.amount IS NOT NULL')
            ->andwhere(['>=', 'chat_session.created_at', $dateFrom])
            ->andwhere(['<=', 'chat_session.created_at', $dateTo]);
        $chatCount  = $chatQuery->count();
        $chatArray  = $chatQuery->all();
        $chats      = ['count' => $chatCount, 'chatArray' => $chatArray];

        //video-chat

        $videoChatQuery = (new Query())
            ->select(['video_chat.created_at', 'video_chat.duration', 'action.'.$selectFrom.' as other_user_id','user_profile.first_name','credit.amount'])
            ->from([VideoChat::tableName()])
            ->leftJoin(Action::tableName(), 'action.id = video_chat.action_id')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.'.$selectFrom);
        if (in_array($owner_user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            $videoChatQuery->leftJoin(['credit' => $subQuery3], 'credit.action_id = action.id');
        }else{
            $videoChatQuery->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id');
        }
        $videoChatQuery->where(['action.'.$selectTo => $otherUserID])
            ->andWhere('credit.amount IS NOT NULL')
            ->andwhere(['>=', 'video_chat.created_at', $dateFrom])
            ->andwhere(['<=', 'video_chat.created_at', $dateTo]);
        $videoChatCount  = $videoChatQuery->count();
        $videoChatArray  = $videoChatQuery->all();
        $videoChats      = ['count' => $videoChatCount, 'chatArray' => $videoChatArray];

        $actionType = ActionType::findOne(['name' => 'send present']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //gifts
        $giftsQuery = (new Query())
            ->select(['action.created_at', 'action.'.$selectFrom.' as otherUserID','user_profile.first_name','credit.amount'])
            ->from([Action::tableName()])
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.'.$selectFrom);
        if (in_array($owner_user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            $giftsQuery->leftJoin(['credit' => $subQuery3], 'credit.action_id = action.id');
        }else{
            $giftsQuery->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id');
        }
        $giftsQuery->where(['action.action_type_id' => $actionType->id, 'action.'.$selectTo => $otherUserID])
            ->andwhere(['>=', 'action.created_at', $dateFrom])
            ->andwhere(['<=', 'action.created_at', $dateTo])
        ;

        $giftsCount  = $giftsQuery->count();
        $giftsArray  = $giftsQuery->all();
        $gifts       = ['count' => $giftsCount, 'giftsArray' => $giftsArray];

        //premium photo
        $premiumPhotoQuery = (new Query())
            ->select(['action.created_at', 'action.'.$selectFrom.' as otherUserID','user_profile.first_name','credit.amount'])
            ->from([Action::tableName()])
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.'.$selectFrom);
        if (in_array($owner_user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            $premiumPhotoQuery->leftJoin(['credit' => $subQuery3], 'credit.action_id = action.id');
        }else{
            $premiumPhotoQuery->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id');
        }

        $actionType = ActionType::findOne(['name' => 'watch photo']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        $premiumPhotoQuery->where(['action.action_type_id' => $actionType->id, 'action.'.$selectTo => $otherUserID])
            ->andwhere(['>=', 'action.created_at', $dateFrom])
            ->andwhere(['<=', 'action.created_at', $dateTo])
        ;
        $premiumPhotoCount = $premiumPhotoQuery->count();
        $premiumPhotoArray  = $premiumPhotoQuery->all();
        $premiumPhoto = ['count' => $premiumPhotoCount, 'premiumPhotosArray' => $premiumPhotoArray];

        //premium video
        $premiumVideoQuery = (new Query())
            ->select(['action.created_at', 'action.'.$selectFrom.' as otherUserID','user_profile.first_name','credit.amount'])
            ->from([Action::tableName()])
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.'.$selectFrom);
        if (in_array($owner_user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            $premiumVideoQuery->leftJoin(['credit' => $subQuery3], 'credit.action_id = action.id');
        }else{
            $premiumVideoQuery->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id');
        }

        $actionType = ActionType::findOne(['name' => 'watch video']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        $premiumVideoQuery->where(['action.action_type_id' => $actionType->id, 'action.'.$selectTo => $otherUserID])
            ->andwhere(['>=', 'action.created_at', $dateFrom])
            ->andwhere(['<=', 'action.created_at', $dateTo])
        ;
        $premiumVideoCount = $premiumVideoQuery->count();
        $premiumVideoArray  = $premiumVideoQuery->all();
        $premiumVideo = ['count' => $premiumVideoCount, 'premiumVideoArray' => $premiumVideoArray];

        return [
            'letters'=>$letters,
            'chats'=>$chats,
            'videoChats'=>$videoChats,
            'gifts'=>$gifts,
            'premiumPhotos'=>$premiumPhoto,
            'PremiumVideo'=>$premiumVideo,
        ];

    }

    public function getGroupFinanceInfo($myChildrenIDs, $searchParams, $limit, $offset)
    {
        $dateFrom = isset($searchParams['dateFrom']) && strtotime($searchParams['dateFrom']) ? $searchParams['dateFrom'] : date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
        $dateTo   = isset($searchParams['dateTo']) && strtotime($searchParams['dateTo']) ? $searchParams['dateTo'].' 23:59:59' : date('Y-m-d', time());

        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, id')
            ->from([UserProfile::tableName() . ' user_profile']);

        //получаем сумму потраченную за услугу
        $subQuery2 = (new Query)
            ->select(['credit.amount','action_credit.action_id'])
            ->from([Credit::tableName()])
            ->leftJoin(ActionCredit::tableName() . ' action_credit', 'action_credit.credit_id = credit.id');

        //получаем сумму заплаченную агенствам за услугу
        $subQuery3 = (new Query)
            ->select(['AgencyRevard.amount', 'AgencyRevard.action_id'])
            ->from([AgencyRevard::tableName() . ' AgencyRevard']);

        $type = isset($searchParams['type']) &&  trim($searchParams['type']) != "" ? trim($searchParams['type']) : 'letters';
        //letters
        $letters = [];
        $chats = [];
        $videoChats = [];
        $gifts = [];
        $PremiumPhotos = [];
        $PremiumVideo = [];

        if($type == 'letters'){
            $actionType = ActionType::findOne(['name' => 'send letter']);

            if (empty($actionType)) {
                return [
                    'success' => false,
                    'message' => 'Wrong action type'
                ];
            }
            $lettersQuery = (new Query())
                ->select(['action.created_at', 'action.action_creator as other_user_id','action.action_receiver','user_profile.first_name','user_profile2.first_name as to_name','credit.amount'])
                ->from([Action::tableName()])
                ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
                ->leftJoin(['user_profile2' => $subQuery], 'user_profile2.id = action.action_receiver');
            if (in_array($this->userType, [User::USER_AGENCY, User::USER_ADMIN])) {
                $lettersQuery->leftJoin(['credit' => $subQuery3], 'credit.action_id = action.id');
            }else{
                $lettersQuery->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id');
            }
            $lettersQuery->where(['action.action_type_id' => $actionType->id]);
            if (is_array($myChildrenIDs) && !empty($myChildrenIDs)) {
                $lettersQuery->andWhere(['in','action.action_receiver', $myChildrenIDs]);
            }

            $lettersQuery->andwhere(['>=', 'action.created_at', $dateFrom])
                ->andwhere(['<=', 'action.created_at', $dateTo]);
            $lettersQuery->orderBy('action.created_at DESC');
            $lettersCount = $lettersQuery->count();
            if ($limit !== null && is_numeric($limit)) {
                $lettersQuery->limit($limit);
            }
            if ($offset !== null && is_numeric($offset)) {
                $lettersQuery->offset($offset);
            }
            $lettersArray  = $lettersQuery->all();
            $letters       = ['count' => $lettersCount, 'lettersArray' => $lettersArray];
        }

        //chat
        if($type == 'chats'){
            $chatQuery = (new Query())
                ->select(['chat_session.created_at', 'chat_session.duration', 'action.action_creator as otherUserID','action.action_receiver','user_profile.first_name','user_profile2.first_name as to_name','credit.amount'])
                ->from([ChatSession::tableName()])
                ->leftJoin(Chat::tableName(), 'chat.chat_session_id = chat_session.id')
                ->leftJoin(Action::tableName(), 'chat.action_id = action.id')
                ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
                ->leftJoin(['user_profile2' => $subQuery], 'user_profile2.id = action.action_receiver');
            if (in_array($this->userType, [User::USER_AGENCY, User::USER_ADMIN])) {
                $chatQuery->leftJoin(['credit' => $subQuery3], 'credit.action_id = action.id');
            }else{
                $chatQuery->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id');
            }
            if (is_array($myChildrenIDs) && !empty($myChildrenIDs)) {
                $chatQuery->where(['or', ['in','action.action_receiver',$myChildrenIDs], ['in','action.action_creator',$myChildrenIDs]]);
            }
            $chatQuery->andwhere(['>=', 'chat_session.created_at', $dateFrom])
                ->andwhere(['<=', 'chat_session.created_at', $dateTo])
                ->andWhere(['chat_session.active' => 1]);
            $chatQuery->groupBy('chat_session.id');
            $chatCount = $chatQuery->count();
            if ($limit !== null && is_numeric($limit)) {
                $chatQuery->limit($limit);
            }
            if ($offset !== null && is_numeric($offset)) {
                $chatQuery->offset($offset);
            }
            $chatQuery->orderBy('chat_session.created_at DESC');

            $chatArray  = $chatQuery->all();
            $chats      = ['count' => $chatCount, 'chatArray' => $chatArray];
        }

        //video-chat
        if ($type == 'video-chat') {
            $videoChatQuery = (new Query())
                ->select(['video_chat.created_at', 'video_chat.duration', 'action.action_creator as other_user_id', 'action.action_receiver','user_profile.first_name','user_profile2.first_name as to_name','credit.amount'])
                ->from([VideoChat::tableName()])
                ->leftJoin(Action::tableName(), 'action.id = video_chat.action_id')
                ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
                ->leftJoin(['user_profile2' => $subQuery], 'user_profile2.id = action.action_receiver');
            if (in_array($this->userType, [User::USER_AGENCY, User::USER_ADMIN])) {
                $videoChatQuery->leftJoin(['credit' => $subQuery3], 'credit.action_id = action.id');
            }else{
                $videoChatQuery->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id');
            }
            if(is_array($myChildrenIDs) && !empty($myChildrenIDs)){
                $videoChatQuery->where(['in','action.action_receiver', $myChildrenIDs]);
            }
            $videoChatQuery->andwhere(['>=', 'action.created_at', $dateFrom])
                ->andwhere(['<=', 'action.created_at', $dateTo]);
            $videoChatCount = $videoChatQuery->count();
            if ($limit !== null && is_numeric($limit)) {
                $videoChatQuery->limit($limit);
            }
            if ($offset !== null && is_numeric($offset)) {
                $videoChatQuery->offset($offset);
            }
            $videoChatQuery->orderBy('video_chat.created_at DESC');
            $videoChatArray  = $videoChatQuery->all();
            $videoChats      = ['count' => $videoChatCount, 'chatArray' => $videoChatArray];
        }

        //gifts
        if ($type == 'gifts') {
            $actionType = ActionType::findOne(['name' => 'send present']);

            if (empty($actionType)) {
                return [
                    'success' => false,
                    'message' => 'Wrong action type'
                ];
            }
            $giftsQuery = (new Query())
                ->select(['action.created_at', 'action.action_creator as otherUserID','action.action_receiver','user_profile.first_name','user_profile2.first_name as to_name','credit.amount'])
                ->from([Action::tableName()])
                ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
                ->leftJoin(['user_profile2' => $subQuery], 'user_profile2.id = action.action_receiver');
            if (in_array($this->userType, [User::USER_AGENCY, User::USER_ADMIN])) {
                $giftsQuery->leftJoin(['credit' => $subQuery3], 'credit.action_id = action.id');
            }else{
                $giftsQuery->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id');
            }
            $giftsQuery->where(['action.action_type_id' => $actionType->id]);
            if(is_array($myChildrenIDs) && !empty($myChildrenIDs)){
                $giftsQuery->andWhere(['in','action.action_receiver',$myChildrenIDs]);
            }

            $giftsQuery->andwhere(['>=', 'action.created_at', $dateFrom])
                ->andwhere(['<=', 'action.created_at', $dateTo]);
            $giftsQuery->orderBy('action.created_at DESC');
            $giftCount = $giftsQuery->count();
            if ($limit !== null && is_numeric($limit)) {
                $giftsQuery->limit($limit);
            }
            if ($offset !== null && is_numeric($offset)) {
                $giftsQuery->offset($offset);
            }
            $giftsArray  = $giftsQuery->all();
            $gifts       = ['count' => $giftCount, 'giftsArray' => $giftsArray];

        }
        //premium-photo
        if ($type == 'premium_photos') {
            $actionType = ActionType::findOne(['name' => 'watch photo']);

            if (empty($actionType)) {
                return [
                    'success' => false,
                    'message' => 'Wrong action type'
                ];
            }
            $premiumPhotoQuery = (new Query())
                ->select(['action.created_at', 'action.action_creator as otherUserID','action.action_receiver','user_profile.first_name','user_profile2.first_name as to_name','credit.amount'])
                ->from([Action::tableName()])
                ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
                ->leftJoin(['user_profile2' => $subQuery], 'user_profile2.id = action.action_receiver');
            if (in_array($this->userType, [User::USER_AGENCY, User::USER_ADMIN])) {
                $premiumPhotoQuery->leftJoin(['credit' => $subQuery3], 'credit.action_id = action.id');
            }else{
                $premiumPhotoQuery->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id');
            }
            $premiumPhotoQuery->where(['action.action_type_id' => $actionType->id]);
            if(is_array($myChildrenIDs) && !empty($myChildrenIDs)){
                $premiumPhotoQuery->andWhere(['in','action.action_receiver',$myChildrenIDs]);
            }

            $premiumPhotoQuery->andwhere(['>=', 'action.created_at', $dateFrom])
                ->andwhere(['<=', 'action.created_at', $dateTo]);
            $premiumPhotoQuery->orderBy('action.created_at DESC');
            $premiumPhotoCount = $premiumPhotoQuery->count();
            if ($limit !== null && is_numeric($limit)) {
                $premiumPhotoQuery->limit($limit);
            }
            if ($offset !== null && is_numeric($offset)) {
                $premiumPhotoQuery->offset($offset);
            }
            $PremiumPhotosArray = $premiumPhotoQuery->all();
            $PremiumPhotos = ['count' => $premiumPhotoCount, 'premiumPhotosArray' => $PremiumPhotosArray];
        }
        //premium-video
        if ($type == 'premium_video') {
            $actionType = ActionType::findOne(['name' => 'watch video']);

            if (empty($actionType)) {
                return [
                    'success' => false,
                    'message' => 'Wrong action type'
                ];
            }
            $premiumVideoQuery = (new Query())
                ->select(['action.created_at', 'action.action_creator as otherUserID','action.action_receiver','user_profile.first_name','user_profile2.first_name as to_name','credit.amount'])
                ->from([Action::tableName()])
                ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
                ->leftJoin(['user_profile2' => $subQuery], 'user_profile2.id = action.action_receiver');
            if (in_array($this->userType, [User::USER_AGENCY, User::USER_ADMIN])) {
                $premiumVideoQuery->leftJoin(['credit' => $subQuery3], 'credit.action_id = action.id');
            }else{
                $premiumVideoQuery->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id');
            }
            $premiumVideoQuery->where(['action.action_type_id' => $actionType->id]);
            if(is_array($myChildrenIDs) && !empty($myChildrenIDs)){
                $premiumVideoQuery->andWhere(['in','action.action_receiver',$myChildrenIDs]);
            }

            $premiumVideoQuery->andwhere(['>=', 'action.created_at', $dateFrom])
                ->andwhere(['<=', 'action.created_at', $dateTo]);
            $premiumVideoQuery->orderBy('action.created_at DESC');
            $premiumVideoCount = $premiumVideoQuery->count();
            if ($limit !== null && is_numeric($limit)) {
                $premiumVideoQuery->limit($limit);
            }
            if ($offset !== null && is_numeric($offset)) {
                $premiumVideoQuery->offset($offset);
            }
            $PremiumVideoArray = $premiumVideoQuery->all();
            $PremiumVideo = ['count' => $premiumVideoCount, 'premiumVideoArray' => $PremiumVideoArray];
        }


        return [
            'letters'=>$letters,
            'chats'=>$chats,
            'videoChats'=>$videoChats,
            'gifts'=>$gifts,
            'premiumPhotos'=>$PremiumPhotos,
            'PremiumVideo'=>$PremiumVideo,
        ];
    }

    public function getAgencyFinanceInfo($agencyChildrenIDs, $searchParams)
    {

        if (empty($agencyChildrenIDs))
            return [
                'letters' => [
                    'count'=>0,
                    'amount'=>0
                ],
                'chats' => [
                    'duration'=>0,
                    'amount'=>0
                ],
                'videoChats' => [
                    'duration'=>0,
                    'amount'=>0
                ],
                'gifts' => [
                    'count'=>0,
                    'amount'=>0
                ],
                'premium-photo' => [
                    'count'=>0,
                    'amount'=>0
                ],
                'premium-video' => [
                    'count'=>0,
                    'amount'=>0
                ],

            ];

        $dateFrom = isset($searchParams['dateFrom']) && strtotime($searchParams['dateFrom']) ? $searchParams['dateFrom'] : date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
        $dateTo   = isset($searchParams['dateTo']) && strtotime($searchParams['dateTo']) ? $searchParams['dateTo'].' 23:59:59' : date('Y-m-d', time());

        //получаем сумму потраченную за услугу
        /*        $subQuery2 = (new Query)
                    ->select(['credit.amount','action_credit.action_id'])
                    ->from([Credit::tableName()])
                    ->leftJoin(ActionCredit::tableName() . ' action_credit', 'action_credit.credit_id = credit.id');
        */
        //получаем сумму заплаченную агенствам за услугу
        $subQuery2 = (new Query)
            ->select(['AgencyRevard.amount', 'AgencyRevard.action_id'])
            ->from([AgencyRevard::tableName() . ' AgencyRevard']);

        $actionType = ActionType::findOne(['name' => 'send letter']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //letters
        $lettersQuery = (new Query())
            ->select(['sum(credit.amount) as amount'])
            ->from([Action::tableName()])
            ->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id')
            ->where(['action.action_type_id' => $actionType->id])
            ->andWhere('credit.amount IS NOT NULL')
            ->andwhere(['in', 'action.action_receiver', $agencyChildrenIDs])
            ->andwhere(['>=', 'action.created_at', $dateFrom])
            ->andwhere(['<=', 'action.created_at', $dateTo]);
        $lettersCount = $lettersQuery->count();
        $lettersAmount  = $lettersQuery->all();
        $letters = [
            'count' => $lettersCount,
            'amount' => !empty($lettersAmount[0]['amount']) ? $lettersAmount[0]['amount'] : 0
        ];

        //chat
        $chatQuery = (new Query())
            ->select(['sum(chat_session.duration) as duration', 'sum(credit.amount) as amount'])
            ->from([ChatSession::tableName()])
            ->leftJoin(Chat::tableName(), 'chat.chat_session_id = chat_session.id')
            ->leftJoin(Action::tableName(), 'chat.action_id = action.id')
            ->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id')
            ->where(['in','action.action_receiver', $agencyChildrenIDs])
            ->andWhere(['chat_session.active' => 1])
            ->andWhere('credit.amount IS NOT NULL')
            ->andWhere(['>=', 'chat_session.created_at', $dateFrom])
            ->andWhere(['<=', 'chat_session.created_at', $dateTo])
            //->groupBy('chat_session.id')
        ;
        $chatArray  = $chatQuery->all();

        $duration = !empty($chatArray[0]['duration']) ? $chatArray[0]['duration'] : 0;
        $amount = !empty($chatArray[0]['amount']) ? $chatArray[0]['amount'] : 0;
        $chats = [
            'duration'=> $duration,
            'amount'=> $amount
        ];

        //video-chat
        $videoChatQuery = (new Query())
            ->select(['sum(video_chat.duration) as duration', 'sum(credit.amount) as amount'])
            ->from([VideoChat::tableName()])
            ->leftJoin(Action::tableName(), 'action.id = video_chat.action_id')
            ->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id')
            ->where(['in','action.action_receiver', $agencyChildrenIDs])
            ->andWhere('credit.amount IS NOT NULL')
            ->andwhere(['>=', 'action.created_at', $dateFrom])
            ->andwhere(['<=', 'action.created_at', $dateTo]);
        $videoChatArray  = $videoChatQuery->all();

        $duration = !empty($videoChatArray[0]['duration']) ? $videoChatArray[0]['duration'] : 0;
        $amount = !empty($videoChatArray[0]['amount']) ? $videoChatArray[0]['amount'] : 0;
        $videoChats =[
            'duration'=> $duration,
            'amount'=> $amount
        ];
        $actionType = ActionType::findOne(['name' => 'send present']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //gifts
        $giftsQuery = (new Query())
            ->select(['sum(credit.amount) as amount'])
            ->from([Action::tableName()])
            ->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id')
            ->where(['action.action_type_id' => $actionType->id])
            ->andWhere('credit.amount IS NOT NULL')
            ->andwhere(['in', 'action.action_receiver', $agencyChildrenIDs])
            ->andwhere(['>=', 'action.created_at', $dateFrom])
            ->andwhere(['<=', 'action.created_at', $dateTo]);
        $giftsCount = $giftsQuery->count();
        $giftsAmount  = $giftsQuery->all();
        $gifts = [
            'count' => $giftsCount,
            'amount' => !empty($giftsAmount[0]['amount']) ? $giftsAmount[0]['amount'] : 0
        ];

        $actionType = ActionType::findOne(['name' => 'watch photo']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //premium-photo
        $premiumPhotoQuery = (new Query())
            ->select(['sum(credit.amount) as amount'])
            ->from([Action::tableName()])
            ->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id')
            ->where(['action.action_type_id' => $actionType->id])
            ->andWhere('credit.amount IS NOT NULL')
            ->andwhere(['in', 'action.action_receiver', $agencyChildrenIDs])
            ->andwhere(['>=', 'action.created_at', $dateFrom])
            ->andwhere(['<=', 'action.created_at', $dateTo]);
        $premiumPhotoCount = $premiumPhotoQuery->count();
        $premiumPhotosAmount  = $premiumPhotoQuery->all();
        $premiumPhotos = [
            'count' => $premiumPhotoCount,
            'amount' => !empty($premiumPhotosAmount[0]['amount']) ? $premiumPhotosAmount[0]['amount'] : 0
        ];

        $actionType = ActionType::findOne(['name' => 'watch video']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //premium-video
        $premiumVideoQuery = (new Query())
            ->select(['sum(credit.amount) as amount'])
            ->from([Action::tableName()])
            ->leftJoin(['credit' => $subQuery2], 'credit.action_id = action.id')
            ->where(['action.action_type_id' => $actionType->id])
            ->andWhere('credit.amount IS NOT NULL')
            ->andwhere(['in', 'action.action_receiver', $agencyChildrenIDs])
            ->andwhere(['>=', 'action.created_at', $dateFrom])
            ->andwhere(['<=', 'action.created_at', $dateTo]);
        $premiumVideoCount = $premiumVideoQuery->count();
        $premiumVideoAmount  = $premiumVideoQuery->all();
        $premiumVideo = [
            'count' => $premiumVideoCount,
            'amount' => !empty($premiumVideoAmount[0]['amount']) ? $premiumVideoAmount[0]['amount'] : 0
        ];

        return [
            'letters'=>$letters,
            'chats'=>$chats,
            'videoChats'=>$videoChats,
            'gifts'=>$gifts,
            'premium_photos'=>$premiumPhotos,
            'premium_video'=>$premiumVideo,
        ];

    }

    public function getTotalFinanceInfo($searchParams)
    {

        if ($this->userType != User::USER_SUPERADMIN)
            return null;

        $dateFrom = isset($searchParams['dateFrom']) && strtotime($searchParams['dateFrom']) ? $searchParams['dateFrom'] : date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
        $dateTo   = isset($searchParams['dateTo']) && strtotime($searchParams['dateTo']) ? $searchParams['dateTo'].' 23:59:59' : date('Y-m-d', time()) .' 23:59:59';
        $sortBy   = isset($searchParams['sortBy']) ? $searchParams['sortBy'] : 'profile-count';
        $sortWay  = isset($searchParams['sortWay']) ? $searchParams['sortWay'] : 'down';

        $mass = [
            'title'=>'user_profile.name',
            'profile-count'=>'profile_count',
            'letters'=>'letters_count',
            'chat'=>'chat_duration',
            'video-chat'=>'video_chat_duration',
            'watch-video'=>'watch_video_count',
            'watch-photo'=>'watch_photo_count',
            'gifts'=>'gifts_count',
            'money'=>'total_amount', //TODO нужно выбрать общую сумму и по ней сортировать total_amount неработает
        ];
        if(array_key_exists($sortBy,$mass)){
            $sortBy = $mass[$sortBy];
        }

        if(in_array($sortWay,['up','down'])){
            $mass = ['up'=>SORT_ASC,'down'=>SORT_DESC];
            $sortWay = $mass[$sortWay];
        }

        /*
        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('user.id, agency_id, agency.name')
            ->from([UserProfile::tableName()." user_profile"])
            ->leftJoin(User::tableName(), 'user_profile.id = user.id')
            ->leftJoin(Agency::tableName(), 'agency.id = user.agency_id')
            ->where(['user_type' => User::USER_AGENCY]);
        */
        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('user.id, user.agency_id,user.status, agency.name')
            ->from([Agency::tableName()." agency"])
            ->leftJoin(User::tableName(), 'agency.id = user.agency_id')
            //->leftJoin(Agency::tableName(), '')
            ->where(['user.user_type' => User::USER_AGENCY]);


        //получаем сумму потраченную за услугу
        $subQuery_real = (new Query)
            ->select(['credit.amount','action_credit.action_id'])
            ->from([Credit::tableName()])
            ->leftJoin(ActionCredit::tableName() . ' action_credit', 'action_credit.credit_id = credit.id');

        //получаем сумму заплаченную агенствам за услугу
        $subQuery_agency = (new Query)
            ->select(['AgencyRevard.amount', 'AgencyRevard.action_id'])
            ->from([AgencyRevard::tableName() . ' AgencyRevard']);


        $actionType = ActionType::findOne(['name' => 'send letter']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //получаем сумму потраченную за письма
        $subQuery2 = (new Query)
            ->select(['sum(credit.amount) as amount','count(credit.action_id) as count','action.action_receiver'])
            ->from([Action::tableName()])
            //->leftJoin(ActionCredit::tableName() . ' action_credit', 'action_credit.credit_id = credit.id')
            ->leftJoin(['credit' => $subQuery_agency], 'credit.action_id = action.id')
            //->leftJoin(Action::tableName(), 'action.id = action_credit.action_id AND action.action_type_id = ' . $actionType->id)
            ->where(['action.action_type_id' =>$actionType->id])
            ->andWhere(['>=', 'action.created_at', $dateFrom])
            ->andWhere(['<=', 'action.created_at', $dateTo])
            ->groupBy(['action.action_receiver']);

        //получаем сумму потраченную за чат
        $subQuery3 = (new Query)
            ->select(['sum(credit.amount) as amount','action.action_receiver','sum(chat_session.duration) as duration'])
            ->from([Action::tableName()])
            ->leftJoin(['credit' => $subQuery_agency], 'credit.action_id = action.id')
            //->leftJoin(Credit::tableName(), 'credit.id = action_credit.credit_id')
            //->leftJoin(Action::tableName(), 'action.id = action_credit.action_id')
            ->leftJoin(Chat::tableName(), 'chat.action_id = action.id')
            ->leftJoin(ChatSession::tableName(), 'chat.chat_session_id = chat_session.id')
            ->where('credit.amount IS NOT NULL')
            ->andWhere(['>=', 'chat_session.created_at', $dateFrom])
            ->andWhere(['<=', 'chat_session.created_at', $dateTo])
            ->groupBy(['action.action_receiver']);

        $actionType = ActionType::findOne(['name' => 'videochat']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //получаем сумму потраченную за видео чат
        $subQuery4 = (new Query)
            ->select(['sum(credit.amount) as amount','action.action_receiver','sum(video_chat.duration) as duration'])
            ->from([Action::tableName()])
            ->leftJoin(['credit' => $subQuery_agency], 'credit.action_id = action.id')
            //->leftJoin(Credit::tableName(), 'credit.id = action_credit.credit_id')
            ->leftJoin(VideoChat::tableName(), 'video_chat.action_id = credit.action_id')
            //->leftJoin(Action::tableName(), 'action.id = action_credit.action_id AND action.action_type_id = ' . $actionType->id)
            ->where(['action.action_type_id'=>$actionType->id])
            ->andWhere(['>=', 'action.created_at', $dateFrom])
            ->andWhere(['<=', 'action.created_at', $dateTo])
            ->groupBy(['action.action_receiver']);

        $actionType = ActionType::findOne(['name' => 'send present']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //получаем сумму потраченную за подарки
        $subQuery5 = (new Query)
            ->select(['sum(credit.amount) as amount','action.action_receiver','count(credit.action_id) as count'])
            ->from([Action::tableName()])
            ->leftJoin(['credit' => $subQuery_agency], 'credit.action_id = action.id')
            //->leftJoin(ActionCredit::tableName(), 'action_credit.credit_id = credit.id')
            //->leftJoin(Action::tableName(), 'action.id = action_credit.action_id AND action.action_type_id = '. $actionType->id)
            ->where(['action.action_type_id' =>$actionType->id])
            ->andWhere(['>=', 'action.created_at', $dateFrom])
            ->andWhere(['<=', 'action.created_at', $dateTo])
            ->groupBy(['action.action_receiver']);

        $actionType = ActionType::findOne(['name' => 'watch video']);
        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }

        // get sum that user spend watching video
        $subQuery6 = (new Query)
            ->select(['sum(credit.amount) as amount','action.action_receiver','count(credit.action_id) as count'])
            ->from([Action::tableName()])
            ->leftJoin(['credit' => $subQuery_agency], 'credit.action_id = action.id')
            //->leftJoin(ActionCredit::tableName(), 'action_credit.credit_id = credit.id')
            //->leftJoin(Action::tableName(), 'action.id = action_credit.action_id AND action.action_type_id = '. $actionType->id)
            ->where(['action.action_type_id'=>$actionType->id])
            ->andWhere(['>=', 'action.created_at', $dateFrom])
            ->andWhere(['<=', 'action.created_at', $dateTo])
            ->groupBy(['action.action_receiver']);

        $actionType = ActionType::findOne(['name' => 'watch photo']);
        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        // get sum that user spend watching girl photo
        $subQuery7 = (new Query)
            ->select(['sum(credit.amount) as amount','action.action_receiver','count(credit.action_id) as count'])
            ->from([Action::tableName()])
            ->leftJoin(['credit' => $subQuery_agency], 'credit.action_id = action.id')
            //->leftJoin(ActionCredit::tableName(), 'action_credit.credit_id = credit.id')
            //->leftJoin(Action::tableName(), 'action.id = action_credit.action_id AND action.action_type_id = '. $actionType->id)
            ->where(['action.action_type_id'=>$actionType->id])
            ->andWhere(['>=', 'action.created_at', $dateFrom])
            ->andWhere(['<=', 'action.created_at', $dateTo])
            ->groupBy(['action.action_receiver']);

        // get active women profile
        $subQuery8 = (new Query)
            ->select(['count(user.agency_id) as count,agency_id'])
            ->from([User::tableName()])
            ->where(['user.status'=>User::STATUS_ACTIVE,'user.user_type'=>User::USER_FEMALE])
            ->groupBy(['user.agency_id']);

        $financeInfoQuery = (new Query())
            ->select(['sum(credit_letters.amount) as letters_amount','sum(credit_letters.count) as letters_count', 'user_profile.id', 'user_profile.name','user_profile.status'])
            ->addSelect(['sum(credit_chat.amount) as chat_amount','sum(credit_chat.duration) as chat_duration'])
            ->addSelect(['sum(credit_video_chat.amount) as video_chat_amount','sum(credit_video_chat.duration) as video_chat_duration'])
            ->addSelect(['sum(credit_gifts.amount) as gifts_amount, sum(credit_gifts.count) as gifts_count, sum(credit_watch_video.amount) as watch_video_amount, sum(credit_watch_video.count) as watch_video_count'])
            ->addSelect(['sum(credit_watch_photo.amount) as watch_photo_amount, sum(credit_watch_photo.count) as watch_photo_count','agency_profile_count.count as profile_count',
                         '(sum(credit_letters.amount) + sum(credit_chat.amount) + sum(credit_video_chat.amount) + sum(credit_gifts.amount) + sum(credit_watch_video.amount) + sum(credit_watch_photo.amount)) as total_amount'])
            //'(credit_chat.amount + credit_video_chat.amount) as total_amount'])
            ->from([User::tableName()])
            ->leftJoin(['credit_letters' => $subQuery2], 'credit_letters.action_receiver = user.id')
            ->leftJoin(['credit_chat' => $subQuery3], 'credit_chat.action_receiver = user.id')
            ->leftJoin(['credit_video_chat' => $subQuery4], 'credit_video_chat.action_receiver = user.id')
            ->leftJoin(['credit_gifts' => $subQuery5], 'credit_gifts.action_receiver = user.id')
            ->leftJoin(['credit_watch_video' => $subQuery6], 'credit_watch_video.action_receiver = user.id')
            ->leftJoin(['credit_watch_photo' => $subQuery7], 'credit_watch_photo.action_receiver = user.id')
            ->leftJoin(['agency_profile_count' => $subQuery8], 'agency_profile_count.agency_id = user.agency_id')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.agency_id = user.agency_id')
            ->where(['user_type' => User::USER_FEMALE])
            ->groupBy(['user.agency_id'])
            ->orderBy([$sortBy=>$sortWay])
        ;

        $agencyList  = $financeInfoQuery->all();

        return ['agencyList'=>$agencyList];

    }

    public function getTotalFinanceInfoByAgency($searchParams)
    {

        if (!in_array($this->userType, [User::USER_SUPERADMIN, User::USER_AGENCY]))
            return ['girlsList'=>[]];

        $dateFrom = isset($searchParams['dateFrom']) && strtotime($searchParams['dateFrom']) ? $searchParams['dateFrom'] : date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
        $dateTo   = isset($searchParams['dateTo']) && strtotime($searchParams['dateTo']) ? $searchParams['dateTo'] : date('Y-m-d', time()) .' 23:59:59';
        $sortBy   = isset($searchParams['sortBy']) ? $searchParams['sortBy'] : 'status';
        $sortWay  = isset($searchParams['sortWay']) ? $searchParams['sortWay'] : 'down';
        //$dateFrom = date('Y-m-d', mktime(0, 0, 0, 7, 1, date('Y')));
        if ($sortWay == 'down') {
            $sortWay = SORT_DESC;
        } else {
            $sortWay = SORT_ASC;
        }
        if (isset($searchParams['agency_id']) && $this->userType == User::USER_SUPERADMIN) {
            $agency_id = $searchParams['agency_id'];
        } elseif ($this->userType == User::USER_AGENCY) {
            $agency_id = Yii::$app->user->identity->agency_id;        
        } else {
            return ['girlsList'=>[]];
        }        

        $agency = Agency::findOne(['id' => $agency_id]);
        if (!$agency) {
            return ['girlsList'=>[]];
        }
        $otherUserIDs = $agency->getUsersIDs('girls', [User::STATUS_ACTIVE, User::STATUS_NO_ACTIVE,
                                                       User::STATUS_DELETED, User::STATUS_INVISIBLE]);

        $selectFrom = 'action_creator';
        $selectTo = 'action_receiver';
        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('user.id, user.last_activity, city.name as city_name, country.name as country_name, user_profile.first_name, user_profile.last_name, birthday, user.status, medium_thumb, small_thumb')
            ->from([User::tableName()])
            ->leftJoin(UserProfile::tableName(), 'user.id = user_profile.id')
            ->leftJoin(Location::tableName(), 'user_profile.id = location.user_id')
            ->leftJoin(Country::tableName(), 'country.id = location.country_id')
            ->leftJoin(City::tableName(), 'city.id = location.city_id')
            ->leftJoin(Photo::tableName(), 'user.avatar_photo_id = photo.id');


        //получаем сумму потраченную за услугу
        $subQuery_real = (new Query)
            ->select(['credit.amount','action_credit.action_id'])
            ->from([Credit::tableName()])
            ->leftJoin(ActionCredit::tableName() . ' action_credit', 'action_credit.credit_id = credit.id');

        $actionType = ActionType::findOne(['name' => 'send letter']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //получаем сумму потраченную за письма
        $lettersQuery = (new Query())
            ->select(['user_profile.id', 'user_profile.last_activity', 'user_profile.first_name', 'user_profile.last_name','user_profile.status', 'user_profile.birthday', 'user_profile.country_name', 'user_profile.city_name', 'medium_thumb', 'small_thumb','sum(credit.amount) as amount'])
            ->from([Action::tableName()])
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.'.$selectTo);
        $lettersQuery->leftJoin(['credit' => $subQuery_real], 'credit.action_id = action.id');
        $lettersQuery->where(['action.action_type_id' => $actionType->id, 'action.'.$selectTo => $otherUserIDs])
            ->andwhere(['>=', 'action.created_at', $dateFrom])
            ->andwhere(['<=', 'action.created_at', $dateTo])
            ->groupBy(['action.'.$selectTo]);

        $actionType = ActionType::findOne(['name' => 'chat']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //получаем сумму потраченную за чат
        $chatQuery = (new Query())
            ->select(['user_profile.id', 'user_profile.last_activity', 'user_profile.first_name', 'user_profile.last_name','user_profile.status', 'user_profile.birthday', 'user_profile.country_name', 'user_profile.city_name', 'medium_thumb', 'small_thumb','sum(credit.amount) as amount'])
            ->from([ChatSession::tableName()])
            ->leftJoin(Chat::tableName(), 'chat.chat_session_id = chat_session.id')
            ->leftJoin(Action::tableName(), 'chat.action_id = action.id')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.'.$selectTo);
        $chatQuery->leftJoin(['credit' => $subQuery_real], 'credit.action_id = action.id');
        $chatQuery->where(['action.action_type_id' => $actionType->id, 'action.'.$selectTo => $otherUserIDs])
            ->andWhere('credit.amount IS NOT NULL')
            ->andwhere(['>=', 'chat_session.created_at', $dateFrom])
            ->andwhere(['<=', 'chat_session.created_at', $dateTo])
            ->groupBy(['action.'.$selectTo]);

        $actionType = ActionType::findOne(['name' => 'videochat']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //получаем сумму потраченную за видео чат
        $videoChatQuery = (new Query())
            ->select(['user_profile.id', 'user_profile.last_activity', 'user_profile.first_name', 'user_profile.last_name','user_profile.status', 'user_profile.birthday', 'user_profile.country_name', 'user_profile.city_name', 'medium_thumb', 'small_thumb','sum(credit.amount) as amount'])
            ->from([VideoChat::tableName()])
            ->leftJoin(Action::tableName(), 'action.id = video_chat.action_id')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.'.$selectTo);
        $videoChatQuery->leftJoin(['credit' => $subQuery_real], 'credit.action_id = action.id');
        $videoChatQuery->where(['action.action_type_id' => $actionType->id, 'action.'.$selectTo => $otherUserIDs])
            ->andWhere('credit.amount IS NOT NULL')
            ->andwhere(['>=', 'video_chat.created_at', $dateFrom])
            ->andwhere(['<=', 'video_chat.created_at', $dateTo])
            ->groupBy(['action.'.$selectTo]);

        $actionType = ActionType::findOne(['name' => 'send present']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //получаем сумму потраченную за подарки
        $giftsQuery = (new Query())
            ->select(['user_profile.id', 'user_profile.last_activity', 'user_profile.first_name', 'user_profile.last_name','user_profile.status', 'user_profile.birthday', 'user_profile.country_name', 'user_profile.city_name', 'medium_thumb', 'small_thumb', 'sum(credit.amount) as amount'])
            ->from([Action::tableName()])
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.'.$selectTo);
        $giftsQuery->leftJoin(['credit' => $subQuery_real], 'credit.action_id = action.id');
        $giftsQuery->where(['action.action_type_id' => $actionType->id, 'action.'.$selectTo => $otherUserIDs])
            ->andwhere(['>=', 'action.created_at', $dateFrom])
            ->andwhere(['<=', 'action.created_at', $dateTo])
            ->groupBy(['action.'.$selectTo]);

        $actionType = ActionType::findOne(['name' => 'watch video']);
        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }

        // get sum that user spend watching video
        $premiumVideoQuery = (new Query())
            ->select(['user_profile.id', 'user_profile.last_activity', 'user_profile.first_name', 'user_profile.last_name','user_profile.status', 'user_profile.birthday', 'user_profile.country_name', 'user_profile.city_name', 'medium_thumb', 'small_thumb','sum(credit.amount) as amount'])
            ->from([Action::tableName()])
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.'.$selectTo);
        $premiumVideoQuery->leftJoin(['credit' => $subQuery_real], 'credit.action_id = action.id');
        $premiumVideoQuery->where(['action.action_type_id' => $actionType->id, 'action.'.$selectTo => $otherUserIDs])
            ->andwhere(['>=', 'action.created_at', $dateFrom])
            ->andwhere(['<=', 'action.created_at', $dateTo])
            ->groupBy(['action.'.$selectTo]);

        $actionType = ActionType::findOne(['name' => 'watch photo']);
        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        // get sum that user spend watching girl photo
        $premiumPhotoQuery = (new Query())
            ->select(['user_profile.id', 'user_profile.last_activity', 'user_profile.first_name', 'user_profile.last_name','user_profile.status', 'user_profile.birthday', 'user_profile.country_name', 'user_profile.city_name', 'medium_thumb', 'small_thumb','sum(credit.amount) as amount'])
            ->from([Action::tableName()])
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.'.$selectTo);
        $premiumPhotoQuery->leftJoin(['credit' => $subQuery_real], 'credit.action_id = action.id');
        $premiumPhotoQuery->where(['action.action_type_id' => $actionType->id, 'action.'.$selectTo => $otherUserIDs])
            ->andwhere(['>=', 'action.created_at', $dateFrom])
            ->andwhere(['<=', 'action.created_at', $dateTo])
            ->groupBy(['action.'.$selectTo]);

        $financeInfoQuery = (new Query())
            ->select(['user_profile.id', 'user_profile.last_activity', 'user_profile.first_name', 'user_profile.last_name','user_profile.status', 'user_profile.birthday', 'user_profile.country_name', 'user_profile.city_name', 'medium_thumb', 'small_thumb', new Expression("'0' as `amount`")])
            /*            ->addSelect([
                        'credit_letters.amount as letter_amount', 'credit_chat.amount as chat_amount',
                        'credit_video_chat.amount as videochat_amount', 'credit_gifts.amount',
                        'credit_watch_video.amount', 'credit_watch_photo.amount']) */
            ->from([User::tableName()])
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = user.id')
            ->where(['user_type' => User::USER_FEMALE])
            ->andWhere(['agency_id' => $agency_id])
            ->union($lettersQuery)
            ->union($chatQuery)
            ->union($videoChatQuery)
            ->union($giftsQuery)
            ->union($premiumVideoQuery)
            ->union($premiumPhotoQuery);

        //->leftJoin(['credit_letters' => $lettersQuery], 'credit_letters.other_user_id = user_profile.id')
        /*            ->leftJoin(['credit_chat' => $chatQuery], 'credit_chat.other_user_id = user_profile.id')
                    ->leftJoin(['credit_video_chat' => $videoChatQuery], 'credit_video_chat.other_user_id = user_profile.id')
                    ->leftJoin(['credit_gifts' => $giftsQuery], 'credit_gifts.other_user_id = user_profile.id')
                    ->leftJoin(['credit_watch_video' => $premiumVideoQuery], 'credit_watch_video.other_user_id = user_profile.id')
                    ->leftJoin(['credit_watch_photo' => $premiumPhotoQuery], 'credit_watch_photo.other_user_id = user_profile.id') */

        //->orderBy(['total_amount' => SORT_DESC, $sortBy=>$sortWay]);

        $girlsList  = $financeInfoQuery->all();

        return ['girlsList'=>$girlsList];

    }


    public function getTotalFinanceByPeriod($searchParams = null)
    {
        $dateFrom = isset($searchParams['dateFrom']) && strtotime($searchParams['dateFrom']) ? $searchParams['dateFrom'] : FALSE;
        $dateTo   = isset($searchParams['dateTo']) && strtotime($searchParams['dateTo']) ? $searchParams['dateTo'] . ' 23:59:59' : FALSE;

        //add from transaction
        $query = UserTransfer::find()
            ->select('sum(amount) as total_amount');
        if ($dateFrom) {
            $query->andWhere(['>=', 'date_create', $dateFrom]);
        }
        if ($dateTo) {
            $query->andWhere(['<=', 'date_create', $dateTo]);
        }
        $totalTransaction = $query->asArray()->all();

        //spent men
        $query = Credit::find()
            ->select('sum(amount) as total_amount');
        if ($dateFrom) {
            $query->andWhere(['>=', 'created_at', $dateFrom]);
        }
        if ($dateTo) {
            $query->andWhere(['<=', 'created_at', $dateTo]);
        }
        $totalCredits = $query->asArray()->all();

        //paid agency
        $query = PaymentsSchedule::find()
            ->select('sum(paid_value) as total_amount');
        if ($dateFrom) {
            $query->andWhere(['>=', 'period_date', $dateFrom]);
        }
        if ($dateTo) {
            $query->andWhere(['<=', 'period_date', $dateTo]);
        }
        $totalAgencyPayments = $query->asArray()->all();

        $result = [
            'totalTransaction'     => $totalTransaction,
            'totalCharges'         => $totalCredits,
            'totalAgencyPayments' => $totalAgencyPayments,
        ];

        return $result;
    }

    public function getAffiliatesFinanceByPeriod($search_params = null)
    {
        $date_from = isset($search_params['date_from']) && strtotime($search_params['date_from']) ? $search_params['date_from'] : FALSE;
        $date_to   = isset($search_params['date_to']) && strtotime($search_params['date_to']) ? $search_params['date_to'] . ' 23:59:59' : FALSE;
        $my_children_ids = $search_params['my_children_ids'];

        //add from transaction
        $query = UserTransfer::find()
            ->select('sum(amount) as total_amount')
            ->where(['in','user_id',$my_children_ids])
        ;
        if ($date_from) {
            $query->andWhere(['>=', 'date_create', $date_from]);
        }
        if ($date_to) {
            $query->andWhere(['<=', 'date_create', $date_to." 23:59:59"]);
        }
        $total_transaction = $query->asArray()->all();

        //spent men
        $query = Credit::find()
            ->select('sum(amount) as total_amount')
            ->where(['in','user_id',$my_children_ids])
        ;
        if ($date_from) {
            $query->andWhere(['>=', 'created_at', $date_from]);
        }
        if ($date_to) {
            $query->andWhere(['<=', 'created_at', $date_to." 23:59:59"]);
        }
        $total_charges = $query->asArray()->all();

        $ress = [
            'totalTransaction'     => $total_transaction,
            'totalCharges'         => $total_charges,
        ];

        return $ress;
    }

    public function getRandomKey()
    {

        $userModel = UserProfile::findOne(['id' => $this->userID]);
        $userModel->payment_key = Yii::$app->security->generateRandomString(20);
        if(!$userModel->save()){
            return [
                'success' => false,
                'message' => 'Error save user data'
            ];
        }

        return [
            'success' => true,
            'key' => $userModel->payment_key
        ];
    }

    public function getAgenciesFinanceInfo($searchParams)
    {

        $dateFrom = isset($searchParams['dateFrom']) && strtotime($searchParams['dateFrom']) ? $searchParams['dateFrom'] : date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
        $dateTo   = isset($searchParams['dateTo']) && strtotime($searchParams['dateTo']) ? $searchParams['dateTo'].' 23:59:59' : date('Y-m-d', time());

        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('agency.id as agency_user_id, agency.agency_id, agency.status,city.name as city,country.name as country')
            ->from([User::tableName()." agency"])
            ->leftJoin(Location::tableName(), 'location.user_id = agency.id')
            ->leftJoin(City::tableName(), 'city.id = location.city_id')
            ->leftJoin(Country::tableName(), 'country.id = location.country_id')
            //->leftJoin(Agency::tableName(), '')
            ->where(['agency.user_type' => User::USER_AGENCY]);


        //получаем сумму потраченную за услугу
        $subQuery_real = (new Query)
            ->select(['credit.amount','action_credit.action_id'])
            ->from([Credit::tableName()])
            ->leftJoin(ActionCredit::tableName() . ' action_credit', 'action_credit.credit_id = credit.id');

        //получаем сумму заплаченную агенствам за услугу
        $subQuery_agency = (new Query)
            ->select(['AgencyRevard.amount', 'AgencyRevard.action_id'])
            ->from([AgencyRevard::tableName() . ' AgencyRevard']);


        $actionType = ActionType::findOne(['name' => 'send letter']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //получаем сумму потраченную за письма
        $subQuery2 = (new Query)
            ->select(['sum(credit.amount) as amount','count(credit.action_id) as count','action.action_receiver'])
            ->from([Action::tableName()])
            //->leftJoin(ActionCredit::tableName() . ' action_credit', 'action_credit.credit_id = credit.id')
            ->leftJoin(['credit' => $subQuery_agency], 'credit.action_id = action.id')
            //->leftJoin(Action::tableName(), 'action.id = action_credit.action_id AND action.action_type_id = ' . $actionType->id)
            ->where(['action.action_type_id' =>$actionType->id])
            ->andWhere(['>=', 'action.created_at', $dateFrom])
            ->andWhere(['<=', 'action.created_at', $dateTo])
            ->groupBy(['action.action_receiver']);

        //получаем сумму потраченную за чат
        $subQuery3 = (new Query)
            ->select(['sum(credit.amount) as amount','action.action_receiver','sum(chat_session.duration) as duration'])
            ->from([Action::tableName()])
            ->leftJoin(['credit' => $subQuery_agency], 'credit.action_id = action.id')
            //->leftJoin(Credit::tableName(), 'credit.id = action_credit.credit_id')
            //->leftJoin(Action::tableName(), 'action.id = action_credit.action_id')
            ->leftJoin(Chat::tableName(), 'chat.action_id = action.id')
            ->leftJoin(ChatSession::tableName(), 'chat.chat_session_id = chat_session.id')
            ->where('credit.amount IS NOT NULL')
            ->andWhere(['>=', 'chat_session.created_at', $dateFrom])
            ->andWhere(['<=', 'chat_session.created_at', $dateTo])
            ->groupBy(['action.action_receiver']);

        $actionType = ActionType::findOne(['name' => 'videochat']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //получаем сумму потраченную за видео чат
        $subQuery4 = (new Query)
            ->select(['sum(credit.amount) as amount','action.action_receiver','sum(video_chat.duration) as duration'])
            ->from([Action::tableName()])
            ->leftJoin(['credit' => $subQuery_agency], 'credit.action_id = action.id')
            //->leftJoin(Credit::tableName(), 'credit.id = action_credit.credit_id')
            ->leftJoin(VideoChat::tableName(), 'video_chat.action_id = credit.action_id')
            //->leftJoin(Action::tableName(), 'action.id = action_credit.action_id AND action.action_type_id = ' . $actionType->id)
            ->where(['action.action_type_id'=>$actionType->id])
            ->andWhere(['>=', 'action.created_at', $dateFrom])
            ->andWhere(['<=', 'action.created_at', $dateTo])
            ->groupBy(['action.action_receiver']);

        $actionType = ActionType::findOne(['name' => 'send present']);

        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        //получаем сумму потраченную за подарки
        $subQuery5 = (new Query)
            ->select(['sum(credit.amount) as amount','action.action_receiver','count(credit.action_id) as count'])
            ->from([Action::tableName()])
            ->leftJoin(['credit' => $subQuery_agency], 'credit.action_id = action.id')
            //->leftJoin(ActionCredit::tableName(), 'action_credit.credit_id = credit.id')
            //->leftJoin(Action::tableName(), 'action.id = action_credit.action_id AND action.action_type_id = '. $actionType->id)
            ->where(['action.action_type_id' =>$actionType->id])
            ->andWhere(['>=', 'action.created_at', $dateFrom])
            ->andWhere(['<=', 'action.created_at', $dateTo])
            ->groupBy(['action.action_receiver']);

        $actionType = ActionType::findOne(['name' => 'watch video']);
        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }

        // get sum that user spend watching video
        $subQuery6 = (new Query)
            ->select(['sum(credit.amount) as amount','action.action_receiver','count(credit.action_id) as count'])
            ->from([Action::tableName()])
            ->leftJoin(['credit' => $subQuery_agency], 'credit.action_id = action.id')
            //->leftJoin(ActionCredit::tableName(), 'action_credit.credit_id = credit.id')
            //->leftJoin(Action::tableName(), 'action.id = action_credit.action_id AND action.action_type_id = '. $actionType->id)
            ->where(['action.action_type_id'=>$actionType->id])
            ->andWhere(['>=', 'action.created_at', $dateFrom])
            ->andWhere(['<=', 'action.created_at', $dateTo])
            ->groupBy(['action.action_receiver']);

        $actionType = ActionType::findOne(['name' => 'watch photo']);
        if (empty($actionType)) {
            return [
                'success' => false,
                'message' => 'Wrong action type'
            ];
        }
        // get sum that user spend watching girl photo
        $subQuery7 = (new Query)
            ->select(['sum(credit.amount) as amount','action.action_receiver','count(credit.action_id) as count'])
            ->from([Action::tableName()])
            ->leftJoin(['credit' => $subQuery_agency], 'credit.action_id = action.id')
            //->leftJoin(ActionCredit::tableName(), 'action_credit.credit_id = credit.id')
            //->leftJoin(Action::tableName(), 'action.id = action_credit.action_id AND action.action_type_id = '. $actionType->id)
            ->where(['action.action_type_id'=>$actionType->id])
            ->andWhere(['>=', 'action.created_at', $dateFrom])
            ->andWhere(['<=', 'action.created_at', $dateTo])
            ->groupBy(['action.action_receiver']);

        // get active women profile
        $subQuery8 = (new Query)
            ->select(['count(user.agency_id) as count,agency_id'])
            ->from([User::tableName()])
            ->where(['user.status'=>User::STATUS_ACTIVE,'user.user_type'=>User::USER_FEMALE])
            ->groupBy(['user.agency_id']);

        $financeInfoQuery = (new Query())
            ->select(['sum(credit_letters.amount) as letters_amount','sum(credit_letters.count) as letters_count', 'user_profile.agency_user_id', 'agency.id', 'agency.name','user_profile.status','user_profile.city','user_profile.country'])
            ->addSelect(['sum(credit_chat.amount) as chat_amount','sum(credit_chat.duration) as chat_duration'])
            ->addSelect(['sum(credit_video_chat.amount) as video_chat_amount','sum(credit_video_chat.duration) as video_chat_duration'])
            ->addSelect(['sum(credit_gifts.amount) as gifts_amount, sum(credit_gifts.count) as gifts_count, sum(credit_watch_video.amount) as watch_video_amount, sum(credit_watch_video.count) as watch_video_count'])
            ->addSelect(['sum(credit_watch_photo.amount) as watch_photo_amount, sum(credit_watch_photo.count) as watch_photo_count','agency_profile_count.count as profile_count'])
            ->from([Agency::tableName()])
            ->leftJoin(User::tableName() . " girl", 'girl.agency_id = agency.id AND girl.user_type = '. User::USER_FEMALE)
            ->leftJoin(['credit_letters' => $subQuery2], 'credit_letters.action_receiver = girl.id')
            ->leftJoin(['credit_chat' => $subQuery3], 'credit_chat.action_receiver = girl.id')
            ->leftJoin(['credit_video_chat' => $subQuery4], 'credit_video_chat.action_receiver = girl.id')
            ->leftJoin(['credit_gifts' => $subQuery5], 'credit_gifts.action_receiver = girl.id')
            ->leftJoin(['credit_watch_video' => $subQuery6], 'credit_watch_video.action_receiver = girl.id')
            ->leftJoin(['credit_watch_photo' => $subQuery7], 'credit_watch_photo.action_receiver = girl.id')
            ->leftJoin(['agency_profile_count' => $subQuery8], 'agency_profile_count.agency_id = agency.id')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.agency_id = agency.id')
            ->where(['<>','agency.id',1])
            ->groupBy(['agency.id'])
            //->orderBy([$sortBy=>$sortWay])
        ;

        $agencyList  = $financeInfoQuery->all();
        $agencyCount  = $financeInfoQuery->count();

        return ['agencyList'=>$agencyList,'agencyCount'=>$agencyCount]; 

    }

}