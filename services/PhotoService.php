<?php
namespace app\services;

use Yii;
use app\models\PhotoLike;
use app\models\Agency;
use app\models\Action;
use app\models\ActionType;
use app\models\Album;
use app\models\ApproveStatus;
use app\models\Photo;
use app\models\PhotoAccess;
use app\models\UploadForm;
use app\models\UploadChatForm;
use app\models\UploadVideoForm;
use app\models\Video;
use app\models\User;
use app\models\UserProfile;
use yii\web\UploadedFile;
use yii\db\Query;

class PhotoService
{
    private $userID = null;
    private $imageFile;

    function __construct($userID = null)
    {
        if ($userID !== null) {
            self::setUser($userID);
        }
    }

    public function setUser($userID)
    {
        $this->userID = $userID;
    }

    public function upload($photoTitle, $photoDesc)
    {
        if ($this->userID === null) {
            return [
                'success' => false, 
                'message' => 'User ID is null'
            ];
        }
        $album = Album::findOne(['id' => $this->userID, 'title' => 'default']);
        if (empty($album)) {
            return [
                'success' => false, 
                'message' => 'Album ID not found'
            ];
        } 

        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstanceByName('imageFile');
            if ($imgName = $model->upload()) {
                if (!empty($imgName)) {
                    $photo = new Photo();
                    $photo->album_id     = $album->id;
                    $photo->small_thumb  = $imgName['small'];
                    $photo->medium_thumb = $imgName['normal'];
                    $photo->original_image = $imgName['orign'];
                    $photo->title        = $photoTitle;
                    $photo->description        = $photoDesc;
                    $photo->status = Photo::STATUS_ACTIVE;
                    $photo->approve_status = Photo::STATUS_APPROVED;

                    if (!$photo->save()) {
                        return [
                            'success' => false, 
                            'message' => 'Error during photo save'
                        ];
                    }
                }
                return [
                    'success' => true, 
                    'message' => 'Photo upload'
                ];
            }
        }
    }

    public function uploadChatPhoto($otherUserID)
    {

        $model = new UploadChatForm();

        $actionType = ActionType::findOne(['name' => 'chat']);
        if (empty($actionType)) {
            $actionType = new ActionType();
            $actionType->name = 'chat';
            if (!$actionType->save()) {
                return [
                    'success' => false, 
                    'message' => 'Can\'t upload photo to chat'
                ];
            }
        }
        $action = Action::find()
                    ->where(['action_type_id' => $actionType->id, 
                             'action_creator' => $this->userID, 
                             'action_receiver' => $otherUserID])
                    ->orWhere(['action_type_id' => $actionType->id,
                             'action_creator' => $otherUserID, 
                             'action_receiver' => $this->userID])
                    ->orderBy(['created_at' => SORT_DESC])
                    ->one();        
        if (!empty($action)) {
            $chat = $action->getChats()->orderBy(['created_at' => SORT_DESC])->one();
        }            
        if (empty($chat)) {
            return [
                'success' => false, 
                'message' => 'Can\'t upload photo to chat'
            ];
        } else {
            $chatId = $chat->chat_id;
        }

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstanceByName('imageFile');
            if ($imgName = $model->upload($chatId)) {             
                return [
                    'success' => true,                                         
                    'medium_thumb' => $imgName['normal'],                     
                ];
            }
            return [
                'success' => false, 
                'message' => 'Error photo format'
            ];
        }
    }

    public function uploadGiftImg()
    {

        if ($this->userID === null) {
            return ['success' => false, 'message' => 'Missing params'];
        }

        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstanceByName('imageFile');

            if ($img_name = $model->uploadGiftImg()) {

                if (!empty($img_name)) {
                    return ['success' => true, 'thumb_normal' => $img_name['normal']];
                }

                return ['success' => false, 'message' => 'Error upload'];
            }
        }

    }

    public function uploadPostImg()
    {
        if ($this->userID === null) {
            return ['success' => false, 'code'=>1, 'message' => 'Missing params'];
        }

        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstanceByName('imageFile');

            if ($img_name = $model->uploadPostImg()) {

                if (!empty($img_name)) {
                    return ['success' => true, 'thumb_normal' => $img_name['normal']];
                }

                return ['success' => false, 'code'=>1, 'message' => 'Error upload'];
            }
        }

    }

    public function uploadVideo()
    {

        if ($this->userID === null) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }

        $model = new UploadVideoForm();

        if (Yii::$app->request->isPost) {
            $model->videoFile = UploadedFile::getInstanceByName('videoFile');

            if ($fileName = $model->upload()) {
                if (!empty($fileName) && !empty($fileName['errors'])) {
                    $errors = $fileName['errors']['videoFile'][0];
                    return [
                        'success' => false,
                        'message' => $errors
                    ];
                }
                if (!empty($fileName)) {
                    $video          = new Video();
                    $video->user_id = $this->userID;
                    $video->path    = $fileName['orign']; 
                    $video->poster = '';   
                    $video->status = ApproveStatus::STATUS_NOT_APPROVED;

                    if (!$video->save()) {
                        $video->validate();
                        return [
                            'success' => false, 
                            'message' => join(', ', $video->getFirstErrors())
                        ];
                    }
                    return [
                        'success' => true, 
                        'fileName' => $fileName['orign']
                    ];
                }
                return [
                    'success' => false, 
                    'message' => 'Error upload'
                ];
            }
            return [
                'success' => false, 
                'message' => 'Video not validate'
            ];
        }
    }

    public function setStatus($photoID, $status)
    {
        Photo::updateAll(['status' => $status], ['in', 'id', $photoID]);

        return [
            'success' => true, 
            'message' => 'Photos update'
        ];
    }

    public function getPhotoById($id, $array = false)
    {
        $result = Photo::findOne(['id' => $id]);

        if ($array) {
            return $result->attributes;
        }

        return $result;
    }

    public function deletePhotoById($id)
    {
        $photoArray = $this->getPhotoById($id);
        if (!empty($photoArray)) {
            $pathToSmallImg   = dirname(__FILE__) . '/../web/' . $photoArray->small_thumb;
            $watermarkToSmallImg   = dirname(__FILE__) . '/../web/' . $photoArray->watermark_small;
            $pathToNormalImg = dirname(__FILE__) . '/../web/' . $photoArray->medium_thumb;
            $watermarkToNormalImg = dirname(__FILE__) . '/../web/' . $photoArray->watermark_medium;
            $pathToOrignImg  = dirname(__FILE__) . '/../web/' . $photoArray->original_image;
            $watermarkToOrignImg  = dirname(__FILE__) . '/../web/' . $photoArray->watermark_orign;

            @unlink($pathToSmallImg);
            @unlink($pathToNormalImg);
            @unlink($pathToOrignImg);
            @unlink($watermarkToSmallImg);
            @unlink($watermarkToNormalImg);
            @unlink($watermarkToOrignImg);

        }
        $result = Photo::deleteAll(['id' => $id]);
        if (!$result) {
            return ['success' => false];
        }

        return ['success' => true];
    }

    public function updatePhotoTitleById($id, $title, $premium)
    {
        $photo = Photo::findOne(['id' => $id]);
        if (empty($photo)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Photo not found'
            ];
        }

        $photo->title = trim($title);
        $photo->premium = $premium;        

        if (!$photo->save()) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Photo update error'
            ];
        }

        if ($photo->premium == 1) {
            $watermarkFilePath = dirname(__FILE__) . '/../web/img/premium.png';
            $this->addWatermark($photo, $watermarkFilePath);
        }  

        return [
            'success' => true, 
            'message' => 'Photo update'
        ];

    }

    public function updatePhotoInfo($id, $title, $premium, $approveStatus = 1, $userDeleteStatus = 1)
    {
        $photo = Photo::findOne(['id' => $id]);
        if (empty($photo)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Photo not found'
            ];
        }

        $photo->title = trim($title);
        $photo->premium = $premium;
        $photo->approve_status = (int)$approveStatus;
        $photo->status = (int)$userDeleteStatus;
        if (!$photo->save()) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Photo update error'
            ];
        }

        if ($photo->premium == 1) {
            $watermarkFilePath = dirname(__FILE__) . '/../web/img/premium.png';
            $this->addWatermark($photo, $watermarkFilePath);
        }        

        return [
            'success' => true, 
            'message' => 'Photo update'
        ];

    }

    public function deleteAllPhotoByAlbumsIds($IDs)
    {
        $photo = Photo::find()
            ->select(['id'])
            ->where(['in', 'album_id', $IDs])
            ->indexBy('photo_id')->all();        

        if (!empty($photo)) {
            foreach ($photo as $item) {
                $result = $this->deletePhotoById($item);
            }
        }        
    }

    public function getOwnerId($photoID)
    {
        $album = Album::find()
                ->select('user_id')
                ->leftJoin(Photo::tableName(), 'album.id = photo.album_id')
                ->where(['photo.id' => $photoID])
                ->one();
        if (!empty($album)) {
            return $album->user_id;
        } else {
            return false;
        }        
    }

    public function getUserInfoByPhotoId($photoID)
    {
        //подзапрос на имя пользователя
        $subQuery = UserProfile::find()
            ->select('first_name, last_name, user.id, agency_id')
            ->leftJoin(User::tableName(), 'user.id = user_profile.id');            

        $subQuery2 = User::find()
            ->select('agency.name, user.agency_id, user.id as agency_user_id')
            ->leftJoin(Agency::tableName(), 'agency.id = user.agency_id')
            ->where(['user_type' => User::USER_AGENCY]);            

        //для досчета лайков
        $subQuery3 = (new Query)
            ->addSelect(['sum(likes) as likes', 'sum(dislikes) as dislikes', 'photo_id'])
            ->from([PhotoLike::tableName()])
            ->groupBy('photo_id');

        $photo = Photo::find()
            ->select('photo.*, user_profile.first_name, user_profile.last_name, album.user_id, album.id as album_id, album.title as album_title, agency_profile.name, agency_profile.agency_user_id, photo_like.likes, photo_like.dislikes')            
            ->leftJoin(Album::tableName(), 'album.id = photo.album_id')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = album.user_id')
            ->leftJoin(['agency_profile' => $subQuery2], 'agency_profile.agency_id = user_profile.agency_id')
            ->leftJoin(['photo_like' => $subQuery3], 'photo_like.photo_id = photo.id')
            ->where(['photo.id' => $photoID]);

        $result = $photo->asArray()->one();
        return $result;
    }

    public function setLike($photo_id)
    {
        $photo_model = self::getPhotoById($photo_id, true);
        if (empty($photo_model)) {
            return ['success' => false, 'code' => 1, 'message' => 'No photo'];
        }

        $photo_like_model = PhotosLike::findOne(['user_id' => $this->userID, 'photo_id' => $photo_id]);
        if (empty($photo_like_model)) {
            $photo_like_model           = new PhotosLike();
            $photo_like_model->user_id  = $this->userID;
            $photo_like_model->photo_id = $photo_id;
            if (!$photo_like_model->save()) {
                return ['success' => false, 'code' => 1, 'message' => 'Error save photo like'];
            }

        } else {
            $photo_like_model->delete();
        }

        $photo_like_model = PhotosLike::find()
            ->where(['photo_id' => $photo_id]);

        $count_like = $photo_like_model->count();

        return ['success' => true, 'count_like' => $count_like];
    }

    public function updateAccess($photoID)
    {
        $photo = Photo::findOne(['id' => $photoID]);

        if (empty($photo)) {
            return false;
        }
        $photoAccess = PhotoAccess::findOne(['user_id' => $this->userID, 'photo_id' => $photo->id]);

        if (empty($photoAccess)) {
            $photoAccess = new PhotoAccess();
            $photoAccess->user_id = $this->userID;
            $photoAccess->photo_id = $photo->id;
        }                
        $photoAccess->ended_at = date('Y-m-d H:i:s', time() + 20*365*86400);
        
        return $photoAccess->save();        

    }

    public function cropImage($photoID, $imageCoordinates, $imageSize)
    {
        if ($this->userID === null) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }

        list($x1, $y1, $x2, $y2) = $imageCoordinates;

        $orignPhoto = $this->getPhotoById($photoID);

        $originalImage = $orignPhoto->original_image;
        
        $originalPath  = Yii::getAlias('@app/web/' . $originalImage);
        $width = $imageSize[0]; //image width on front end
        $height = $imageSize[1]; //image width on front end

        $newWidth = $x2-$x1;
        $newHeight = $y2-$y1;

        $widthRatio = $newWidth/$width;
        $heightRatio = $newHeight/$height;

        $result = $this->cropOneImage($orignPhoto, $originalPath, 
                                      $width, $height, 
                                      $widthRatio, $heightRatio, 
                                      $x1, $y1);
        
        return [
            'success' => true,
            'result' => $result
        ];
    }

    private function cropOneImage($orignPhoto, $file, $width, $height, $widthRatio, $heightRatio, $x1, $y1)
    {
        $originalSize = getimagesize($file);
        $originalWidth = $originalSize[0];
        $originalHeight = $originalSize[1];
        $xRatio = $width/$originalWidth;
        $yRatio = $height/$originalHeight;
        $newWidth = round($originalWidth*$widthRatio);
        $newHeight = round($originalHeight*$heightRatio);
        $newX = round($x1/$xRatio);
        $newY = round($y1/$yRatio);
        $image = Yii::$app->image->load($file);

        $extension=pathinfo($file);
        $extension = $extension['extension'];

        $image->crop($newWidth, $newHeight, $newX, $newY);

        // delete old photos
        $mediumThumb = str_replace('/original/','/thumb_255x290/',$file);
        @unlink($mediumThumb);

        $smallThumb = str_replace('/original/','/thumb_63x63/',$file);
        @unlink($smallThumb);

        $newName           = md5($file . time());
        $mediumThumbPath   = 'uploads/thumb_255x290/';
        $smallThumbPath    = 'uploads/thumb_63x63/';

        $mediumThumbName = $mediumThumbPath . $newName . '.' . $extension;
        $smallThumbName   = $smallThumbPath . $newName . '.' . $extension;

        $image->resize(255, 290, \yii\image\drivers\Image::WIDTH);

        $result = $image->save($mediumThumbName);

        $image->resize(63, 63, \yii\image\drivers\Image::WIDTH);
        $result = $image->save($smallThumbName);

        $orignPhoto->small_thumb = $smallThumbName;
        $orignPhoto->medium_thumb = $mediumThumbName;
        $result = $orignPhoto->save();
        
        return $result;
    }

    public function addWatermark($photo, $watermarkFileName)
    {
/*        $baseDirectory = 'uploads/watermarks/';
        $orignDirectory = $baseDirectory . 'original/';
        $mediumThumbDirectory = $baseDirectory . 'thumb_255x290/';
        $smallThumbDirectory = $baseDirectory . 'thumb_63x63/';
        if (!file_exists($baseDirectory)) {
            mkdir($baseDirectory, 0777, true);
        }
        if (!file_exists($orignDirectory)) {
            mkdir($orignDirectory, 0777, true);
        }
        if (!file_exists($mediumThumbDirectory)) {
            mkdir($mediumThumbDirectory, 0777, true);
        }
        if (!file_exists($smallThumbDirectory)) {
            mkdir($smallThumbDirectory, 0777, true);
        }

        $watermark = Yii::$app->image->load($watermarkFileName);        
        
        $pathForWatermarkOrign = $this->addWatermarkToImage($photo->original_image, $orignDirectory, $watermark);
        $photo->watermark_orign = $pathForWatermarkOrign;

        $pathForWatermarkMedium = $this->addWatermarkToImage($photo->medium_thumb, $mediumThumbDirectory, $watermark);
        $photo->watermark_medium = $pathForWatermarkMedium;

        $pathForWatermarkSmall = $this->addWatermarkToImage($photo->small_thumb, $smallThumbDirectory, $watermark);
        $photo->watermark_small = $pathForWatermarkSmall;

        if (!$photo->save()) {
            return [
                'success' => false
            ];
        }*/

        return [
            'success' => true
        ];
    }

    protected function addWatermarkToImage($filepath, $baseDirectory, $watermark)
    {
        $image = Yii::$app->image->load($filepath);
        $originalSize = getimagesize($filepath);
        $originalWidth = $originalSize[0];
        $originalHeight = $originalSize[1];

        $newWidth = round($originalWidth/2);
        $newHeight = round($originalHeight/2);

        $watermark->resize($newWidth, $newHeight, \yii\image\drivers\Image::WIDTH);
        $image->background('#FF0000', $opacity = 60);

        $im = @imagecreatetruecolor($originalWidth, $originalHeight);
        $red = imagecolorallocate($im, 254, 80, 49);
        $text_color = imagecolorallocate($im, 255, 255, 255);
        $text = 'PREMIUM ONLY';
        $font = dirname(__FILE__) . '/../web/fonts/open-sans-bold.ttf';
        imagefilledrectangle($im, 0, 0, $originalWidth, $originalHeight, $red);
        if ($watermark->height >= $newHeight) {
            imagettftext($im, round($originalWidth/12), 0, $originalWidth-round(0.95*$originalWidth), $newHeight+round($newHeight*0.75), $text_color, $font, $text);
        } else {
            imagettftext($im, round($originalWidth/12), 0, $originalWidth-round(0.95*$originalWidth), 2*$watermark->height+round($watermark->height/8.5), $text_color, $font, $text);
        }
        imagepng($im, $baseDirectory . 'text.png');
        imagedestroy($im);
        $text = Yii::$app->image->load($baseDirectory . 'text.png');
        $image->watermark($text, 0, 0, $opacity = 70);
        $extension=pathinfo($filepath);
        $extension = $extension['extension'];
        if ($watermark->height >= $newHeight) {
            $image->watermark($watermark, $newWidth-round($newWidth/2), $originalHeight-round(1.9*$originalHeight/2), $opacity = 100);
        } else {
            $image->watermark($watermark, $newWidth-round($newWidth/2), $watermark->height-round($watermark->height/3.9), $opacity = 100);
        }
        
        $newFileName = md5($filepath . time());
        $newfilePath = $baseDirectory . 'watermark' . $newFileName . '.' . $extension;
        $result = $image->save($newfilePath);
        return $newfilePath;
    }


}