<?php
namespace app\services;

use app\models\Status;
use app\models\StatusType;
use app\models\User;
use yii\web\NotAcceptableHttpException;
use yii\db\Query;

class StatusService
{
    private static $_userID;

    function __construct($userID = null)
    {
        if ($userID !== null) {
            self::setUser($userID);
        }
    }

    public static function checkOldStatuses()
    {
        /*
        * функция закрывающая статусы которым более 1 месяца
        */
        $now = date('Y-m-d H:i:s');
        $lastMonth = date("Y-m-d", mktime(0, 0, 0, date("m") - 1, date("d"), date("Y")));
        Status::updateAll(['stop_datetime' => $now], "start_datetime < '$lastMonth'");

        return [
            'success' => true, 
            'message' => 'All old statuses stoped'
        ];
    }

    public static function stopStatus($id)
    {
        Status::deleteAll("id = " . (int)$id);
    }

    /**
     * @param      $code
     * @param null $name
     *
     * @return int
     * @throws NotAcceptableHttpException
     */
    public static function Status($code, $name = null)
    {
        $code = self::sanitize($code);

        if (strlen($code) < 4) {
            return [
                'success' => false, 
                'message' => 'Status - incorrect params'
            ];
        }

        $model = Status::findOne(['code' => $code]);

        if (!empty($model)) {
            return $model->id;
        }

        $model = new Status();

        $model->load([
            'Status' =>
                [
                    'title' => null === $name ? $code : trim($name),
                    'code'  => $code,
                ],
        ]);

        if (!$model->save()) {
            return ['success' => false, 'message' => 'Status not save'];
        }

        return $model->id;
    }

    public function setUser($userID)
    {
        self::$_userID = $userID;
    }

    public function setStatuses($status)
    {
        self::set($status);
    }

    public static function sanitize($status)
    {
        return str_replace(' ', '_', strtolower(trim($status)));
    }

    /**
     * @param $user
     * @param $p
     *
     * @throws NotAcceptableHttpException
     */
    public static function set($status)
    {
//        $p = [
//            'status' => 'Status1,Status2,Status3',     // Строка со статусами
//            'type'   => true,                          // true = включать false = выключать
//            'start'  => '2018-04-01',
//            'end'    => '2018-04-30',                  // Необязательный
//        ];

        if ($status['type'] === null) {
            return ['success' => false, 'message' => 'Status - incorrect code params'];
        }

        if (strtolower($status['type']) == "true") {
            $type = true;
        } elseif (strtolower($status['type']) == "false") {
            $type = false;
        } else {
            return ['success' => false, 'message' => 'Status - incorrect code params'];
        }

        if (!isset($status['status'])) {
            return ['success' => false, 'message' => 'Status - incorrect code params'];
        }

        if (isset($status['start']) && !strtotime($status['start'])) {
            return ['success' => false, 'message' => 'Status - incorrect date params'];
        }

        if (isset($status['end']) && !strtotime($status['end'])) {
            return ['success' => false, 'message' => 'Status - incorrect date params'];
        }

        $start = isset($p['start']) ? $status['start'] : NULL;
        $end   = isset($p['end']) ? $status['end'] : NULL;

        $statuses = explode(',', $status['status']);

        foreach ($statuses as $item) {
            self::_status(self::$_userID, $type, $item, $start, $end);
        }
    }

    public static function _status($userId, $active, $status, $start, $end)
    {
        $status_id         = self::Status($status);
        $status            = new Status();
        $status->user_id   = (int)$userId;
        $status->status_type_id = $status_id;
        $status->active     = $active;
        $status->start_datetime = $start;
        $status->stop_datetime = $end;

        if (!$status->save()) {
            return [
                'success' => false, 
                'message' => 'Status - error save'
            ];
        }
    }

    public static function getStatuses($user, $date = null, $status = null, $type = null)
    {
//            $user = 1  // Обязательный параметр
//            $date = '2018-06-10 11:58:33',     // Дата на которую ищем статус
//            $status = 'code_1,code_2,code_3',  // Строка со статусами
//            $type   = true,                    // true = включеный или false = выключеный

        if (!isset($user) || !is_numeric($user)) {
            return [
                'success' => false, 
                'message' => 'Status - incorrect code params'
            ];
        }
        $status_sql = '';
        if ($status !== null) {
            $statuses = explode(',', $status);
            $statusIdArray = [];
            foreach ($statuses as $one_status) {
                $statusId        = self::Status($one_status);
                $statusIdArray[] = $statusId;
            }
            $status_sql = implode(' OR status.status_type_id = ', $statusIdArray);
        }
        //массив ON
        $resultOn = null;
        if ($type !== null && strtolower($type) == 'true' || $type === null) {
            $resultOn = (new Query())->addSelect(['status.*', 'status_type.title', 'status.description'])
                ->from([Status::tableName()])
                ->leftJoin(StatusType::tableName(), 'status.status_type_id = status_type.id')
                ->where(['user_id' => $user])
                ->andWhere(['active' => true]);
            if ($status !== null) {
                $resultOn->andWhere('status.status_type_id = ' . $status_sql);
            }
            if ($date !== null && strtotime($date)) {
                $resultOn->andWhere(['<=', 'status.start_datetime', $date])
                    ->andWhere(['>=', 'status.stop_datetime', $date]);
            }
            $resultOn = $resultOn->all();
        }
        //массив OFF
        $resultOff = null;
        if ($type !== null && strtolower($type) == 'false' || $type === null) {
            $resultOff = (new Query())->addSelect(['status.*', 'status_type.title', 'status_type.description'])
                ->from([Status::tableName()])
                ->leftJoin(StatusType::tableName(), 'status.status_type_id = status_type.status_id')
                ->where(['user_id' => $user])
                ->andWhere(['active' => false]);
            if ($status !== null) {
                $resultOff->andWhere('status.status_id = ' . $status_sql);
            }
            if ($date !== null && strtotime($date)) {
                $resultOff->andWhere(['<=', 'status.start_datetime', $date])
                    ->andWhere(['>=', 'statuses.stop_datetime', $date]);
            }
            $resultOff = $resultOff->all();
        }
        //если статус встречается и в массиве ON и в массиве OFF, то убрать элемент из обоих массивов
        if ($date !== null && strtotime($date) && !empty($resultOn) && !empty($resultOff)) {
            $resultOnCount = count($resultOn);
            $resultOffCount = count($resultOff);
            for ($i = 0; $i <= $resultOnCount; $i++) {
                for ($j = 0; $j <= $resultOffCount; $j++) {
                    if ($resultOn[$i]['status_id'] == $resultOff[$j]['status_id']) {
                        unset($resultOn[$i]);
                        unset($resultOff[$j]);
                    }
                }
            }
        }

        return [
            'statusOn' => $resultOn, 
            'statusOff' => $resultOff
        ];
    }

    public static function findUsers($status, $active, $date = null)
    {
//            $status = 'code_1,code_2,code_3',  // Строка со статусами
//            $active   = true,                    // true = включеный или false = выключеный
//            $date = '2018-06-10 11:58:33',     // Дата на которую ищем статус
        if (!isset($status) || $status == '') {
            return [
                'success' => false, 
                'message' => 'findUsers - incorrect code params'
            ];
        }

        $status = explode(',', $status);
        $statusIdArray = [];
        foreach ($status as $one_status) {
            $status_id        = self::Status($one_status);
            $statusIdArray[] = $status_id;
        }
        $status_sql = implode(' OR status_type_id = ', $statusIdArray);

        $result = Status::find()
            ->where('status_id = ' . $status_sql);
        if ($active !== null && strtolower($active) == 'true') {
            $result->andWhere(['active' => true]);
        }
        if ($active !== null && strtolower($active) == 'false') {
            $result->andWhere(['active' => false]);
        }

        if ($date !== null && strtotime($date)) {
            $result->andWhere(['<=', 'start_datetime', $date])
                ->andWhere(['>=', 'stop_datetime', $date]);
        }        

        return [
            'statuses' => $result->all()
        ];
    }

    public function deleteAllUserStatus()
    {
        return Status::deleteAll(['user_id' => self::$_userID]);
    }

    public function setOnlineStatus($userIds, $status, $userType, $agencyID, $timeTo = 60*60*24*7)
    {
        $onlineTimeTo = time()-60;//offline
        if((int)$status == 1){
            $onlineTimeTo = time()+$timeTo;//7 day
        }

        if ($userType == User::USER_SUPERADMIN) {
            if (empty($userIds)) {
                return [
                    'success' => false, 
                    'message' => 'user ids is empty'
                ];
            }
            $result = User::updateAll(['last_activity' => $onlineTimeTo], 'id in (' . implode(',', $userIds) . ")");
        } elseif (in_array($userType, [User::USER_AGENCY, User::USER_ADMIN])) {
            $inc_ids_count = count($userIds);

            $user = User::find()
                ->where(['in','id', $userIds])
                ->andWhere(['user_type' => User::USER_FEMALE])
                ->andWhere(['agency_id' => $agencyID]);
            $real_ids_count = $user->count();

            if ($inc_ids_count != $real_ids_count) {
                return [
                    'success' => false, 'message' => 'some user not yours',
                    'message2' => $userIds
                ];
            }

            $result = User::updateAll(['last_activity' => $onlineTimeTo], 'id in (' . implode(',', $userIds) . ")");

            if (!$result) {
                return [
                    'success' => false, 
                    'message' => 'User online statuses error update'
                ];
            }
        }

        return [
            'success' => true
        ];
    }

}


