<?php
namespace app\services;

use app\models\Comments;
use Yii;
use app\models\GiftStatus;
use app\models\Order;
use app\models\OrderStatus;
use app\models\Gift;
use app\models\GiftType;
use app\models\User;
use app\models\UserProfile;
use app\models\ActionType;
use app\models\Action;
use app\models\Agency;
use yii\db\Query;
use yii\db\Expression;
use yii\web\BadRequestHttpException;

class ShopService
{
    private $_userID;
    private $_userType = null;

    function __construct($userID = null)
    {
        if ($userID != null) {
            self::setUser($userID);
        }
    }

    public function SendGift($skin_id, $user_to, $gift_mass)
    {

        if ($this->_userId === null) {
            return ['success' => FALSE, 'message' => 'Missing params'];
        }

        $user = new User($this->_userId);
        //проверяем если мужик
        if ($this->_user_type != 1) {
            return ['success' => FALSE, 'message' => 'Missing params'];
        }

        $gift_price    = 0;
        $gift_mass_new = array();
        foreach ($gift_mass as $one) {
            $gift_model = Gift::findOne(['id' => (int)$one['gift_id']]);
            if (empty($gift_model)) {
                return ['success' => FALSE, 'message' => 'Missing params'];
            }

            $gift_price += $gift_model->price * (int)$one['count'];

            $gift_mass_new[] = [
                'id'    => (int)$one['gift_id'],
                'price' => $gift_model->price,
                'count' => (int)$one['count'],
            ];
        }

        $user_balance = $user->payments->GetBalance();

        if ($user_balance < $gift_price) {
            return ['success' => FALSE, 'message' => 'Not enough money'];
        }

        foreach ($gift_mass_new as $one) {
            $action_model                 = new Actions();
            $action_model->action_type_id = 1;//send gift
            $action_model->skin_id        = $skin_id;
            $action_model->user_from      = $this->_userId;
            $action_model->user_to        = $user_to;

            if (!$action_model->save()) {
                return ['success' => FALSE, 'message' => 'Error save action'];
            }

            $shop_model            = new ShopModel();
            $shop_model->action_id = $action_model->action_id;
            $shop_model->gift_id   = $one['id'];
            $shop_model->count     = $one['count'];
            $shop_model->status    = 1;//in process

            if (!$shop_model->save()) {
                return ['success' => FALSE, 'message' => 'Error save shop'];
            }

            $user->payments->AddCharges($action_model->action_id, $one['price'] * $one['count']);

        }

        return ['success' => TRUE, 'message' => 'Gift send'];
    }

    public static function addGift($title, $desc, $price, $type_id, $status = 1, $img = '')
    {
        if ($title === null || trim($title) == "" || $price === null) {
            return ['success' => FALSE, 'code' => 1, 'message' => 'Missing params'];
        }

        if ($status === null) {
            $status = 1;
        }

        $new_img_name = str_replace('/temp/','/gifts/',$img);

        $gift_model = Gift::findOne(['title' => trim($title)]);
        if (!empty($gift_model)) {
            return ['success' => FALSE, 'code' => 1, 'message' => 'title: '.$title.' are used'];
        }

        $gifts_dir = 'uploads/gifts/';
        if ($img != '' && $img != $new_img_name) {
            if (!file_exists($gifts_dir)) {
                mkdir($gifts_dir, 0777, TRUE);
            }
            @rename(dirname(__FILE__) . '/../web/' .$img,dirname(__FILE__) . '/../web/' .$new_img_name);
            @unlink(dirname(__FILE__) . '/../web/' . $gift_model->img);
        }



        $gift_model = new Gift();
        $gift_model->title      = trim($title);
        $gift_model->desc       = $desc;
        $gift_model->price      = $price;
        $gift_model->type_id    = $type_id;
        $gift_model->img        = $new_img_name;
        $gift_model->status     = $status;
        if (!$gift_model->save()) {
            return ['success' => FALSE, 'code' => 3, 'message' => 'Gift not save'];
        }

        return ['success' => TRUE, 'gift_id' => $gift_model->id];
    }

    public static function setGiftParams($id, $title, $desc, $price, $img, $type_id, $status)
    {
        $gift_model = Gift::findOne(['id' => $id]);

        if (empty($gift_model)) {
            return ['success' => FALSE, 'code' => 3, 'message' => 'Gift not found'];
        }

        $gift_model->title = $title;
        $gift_model->desc  = $desc;
        $gift_model->price = $price;
        $img = str_replace($_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/','',$img);
        $new_img_name = str_replace('/temp/','/gifts/',$img);
        //удаляем старую картинку
        $gifts_dir = 'uploads/gifts/';
        if ($gift_model->img != '' && $gift_model->img != $new_img_name) {
            if (!file_exists($gifts_dir)) {
                mkdir($gifts_dir, 0777, TRUE);
            }
            @rename(dirname(__FILE__) . '/../web/' .$img,dirname(__FILE__) . '/../web/' .$new_img_name);
            @unlink(dirname(__FILE__) . '/../web/' . $gift_model->img);
        }

        $gift_model->img        = $new_img_name;
        $gift_model->status     = $status;
        $gift_model->type_id = $type_id;

        if (!$gift_model->save()) {
            return ['success' => FALSE, 'code' => 3, 'message' => 'Gift not save'];
        }

        return ['success' => TRUE];
    }

    public static function getAllGifts($status = null, $limit = null, $offset = null,$search_params = null)
    {

        $gift_model = Gift::find();
        if (!empty($status)) {
            $gift_model->where(['status' => $status]);
        }

        $gift_category = isset($search_params['gift_category']) && is_array($search_params['gift_category']) ? $search_params['gift_category']: null;
        //return ['success' => TRUE, 'gift_mass' => $gift_category];
        if(!empty($gift_category)){
            if(in_array('accessory',$gift_category)){
                $gift_model->orWhere(['type_id' => 1]);
            }
            if(in_array('cellphone',$gift_category)){
                $gift_model->orWhere(['type_id' => 2]);
            }
            if(in_array('notebook',$gift_category)){
                $gift_model->orWhere(['type_id' => 3]);
            }
            if(in_array('perfume',$gift_category)){
                $gift_model->orWhere(['type_id' => 4]);
            }
            if(in_array('perfume',$gift_category)){
                $gift_model->orWhere(['type_id' => 4]);
            }
            if(in_array('food',$gift_category)){
                $gift_model->orWhere(['type_id' => 5]);
            }
            if(in_array('other',$gift_category)){
                $gift_model->orWhere(['type_id' => 6]);
            }
            if(in_array('individual',$gift_category)){
                $gift_model->orWhere(['type_id' => 7]);
            }
            if(in_array('flowers',$gift_category)){
                $gift_model->orWhere(['type_id' => 8]);
            }
            if(in_array('lingerie',$gift_category)){
                $gift_model->orWhere(['type_id' => 9]);
            }
            if(in_array('new',$gift_category)){
                $date_format = date('Y-m-d H:i:s', time()-60*60*24*30); //1 month
                $gift_model->orWhere(['>=','date_creats',$date_format]);
            }

        }

        $sort = isset($search_params['sort']) && $search_params['sort'] == 'down' ? 'price DESC': 'price ASC';

        $gift_model->orderBy($sort);

        if ($limit !== null && is_numeric($limit)) {
            $gift_model->limit($limit);
        }
        if ($offset !== null && is_numeric($offset)) {
            $gift_model->offset($offset);
        }

        $count = $gift_model->count();
        $ress  = $gift_model->asArray()->all();

        return ['success' => TRUE, 'gift_mass' => $ress, 'count' => $count];
    }

    public static function getGiftsByParams($gift_ids, $limit = null, $offset = null)
    {

        $gift_model = Gift::find();
        if (!empty($gift_ids)) {
            $gift_model->where(['in', 'id', $gift_ids]);
        }

        $ress = $gift_model->asArray()->all();

        return ['success' => TRUE, 'gift_mass' => $ress,'count' => $gift_model->count()];
    }


    public static function dellGift($id)
    {
        if (!is_numeric($id)) {
            return ['success' => FALSE, 'code' => 3, 'message' => 'Missing param'];
        }

        $gift_model = Gift::findOne($id);
        if (empty($gift_model)) {
            return ['success' => FALSE, 'code' => 3, 'message' => 'Gift not found'];
        }

        if ($gift_model->img != '') {
            @unlink(dirname(__FILE__) . '/../web/' . $gift_model->img);
        }

        $gift_model->delete();

        return ['success' => TRUE, 'gift id: ' . $id . ' delete'];
    }

    public function setUser($userID)
    {
        $user = User::findOne(['id' => $userID]);
        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 3, 
                'message' => 'User id not found'
            ];
        }

        $this->_userID    = $user->id;
        $this->_userType = $user->user_type;
    }

    public function getMyShopItems($actionID = null, $dateFrom = null, $dateTo = null)
    {

        $user   = User::findOne(['id' => $this->_userID]);
        $myIDs = $user->getMyChidrensIds();

        $subQuery = (new Query)
            ->select('first_name,last_name, id')
            ->from([UserProfile::tableName()]);

        $subQuery2 = (new Query)
            ->select('agency.id, agency.name, user.id as agency_user_id')
            ->from(User::tableName())
            ->leftJoin(Agency::tableName(), 'user.agency_id = agency.id')
            ->where(['user_type' => User::USER_AGENCY]);

        //подзапрос на новые комменты
        $task_comments_read = (new Query)
            ->select('comments.id as comments_id, comments.resource_id')
            ->from([Comments::tableName() . ' comments'])
            ->leftJoin(User::tableName() . ' users', 'users.id = comments.user_id')
            ->where(['resource_type_id' => 2, 'comments.date_read' => null]);
        if (in_array($this->_userType, [User::USER_AGENCY, User::USER_INTERPRETER, User::USER_ADMIN])) {
            $task_comments_read->andWhere(['users.user_type' => User::USER_SUPERADMIN]);
        } elseif ($this->_userType == User::USER_SUPERADMIN) {
            $task_comments_read->andWhere(['in', 'users.user_type', [User::USER_AGENCY, User::USER_INTERPRETER, User::USER_ADMIN]]);
        }
        $task_comments_read->groupBy('comments.resource_id');

        $query = (new Query())
            ->select('order.*,order_status.description as status_title, gift.name as gift_name,
             gift.image as gift_img, gift.price as gift_price, user_profile.first_name as user_to_name, 
             user_profile.last_name as user_to_last_name, user_profile.id as user_to_id, 
             user_profile2.id as user_from_id, user_profile2.first_name as user_from_name, 
             user_profile2.last_name as user_from_last_name, agency.agency_user_id as agency_id, 
             agency.name as agency_name, task_comments_read.comments_id')
            ->from([Order::tableName()])
            ->leftJoin(Action::tableName(), 'action.id = order.action_id')
            ->leftJoin(OrderStatus::tableName(), 'order_status.id = order.order_status_id')
            ->leftJoin(Gift::tableName(), 'gift.id = order.gift_id')
            ->leftJoin(User::tableName(), 'user.id = action.action_receiver')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_receiver')
            ->leftJoin(['user_profile2' => $subQuery], 'user_profile2.id = action.action_creator')
            ->leftJoin(['agency' => $subQuery2], 'agency.id = user.agency_id')
            ->leftJoin(['task_comments_read' => $task_comments_read], 'task_comments_read.resource_id = order.action_id')
            ->where(['user_type' => User::USER_FEMALE]);
        if (is_array($myIDs)) {
            $query->where(['in', 'action.action_receiver', $myIDs]);
        }elseif(is_numeric($myIDs)){
            $query->where(['action.action_receiver' => $myIDs])
                ->orWhere(['action.action_creator' => $myIDs]);
        }

        $comments = null;
        if(!empty($actionID)){
            $query->andWhere(['action.id' => $actionID]);
            $comments = (new Comments())->getCommentsByShopId($actionID);
        }

        if (!empty($dateFrom)) {
            $query->andWhere(['>=', 'order.created_at', $dateFrom]);
        }
        if (!empty($dateTo)) {
            $query->andWhere(['<=', 'order.created_at', $dateTo]);
        }

        $query->orderBy('action_id DESC');

        $result = $query->all();

        $orderStatusArray = OrderStatus::find()->asArray()->all();

        return ['success' => true, 'items' => $result, 'orderStatusArray' => $orderStatusArray, 'discuss' => $comments];

    }

    public function setShopGiftStatus($shopItemID, $status, $comment = null)
    {
        $user   = User::findOne(['id' => $this->_userID]);
        $myIDs = $user->getMyChidrensIds();
        if (empty($myIDs) || (!is_array($myIDs) && $myIDs != 'admin')) {
            return [
                'success' => false, 
                'items' => []
            ];
        }

        $query = (new Query())
            ->select('order.*')
            ->from([Order::tableName()])
            ->leftJoin(Action::tableName(), 'action.id = order.action_id')
            ->where(['order.action_id' => $shopItemID]);
        if (is_array($myIDs)) {
            $query->andWhere(['in', 'action.action_receiver', $myIDs]);
        }

        $result = $query->all();

        if (empty($result)) {
            return [
                'success' => false, 
                'message' => 'Permission denied'
            ];
        }

        if ($this->_userType != User::USER_SUPERADMIN && !in_array($result[0]['order_status_id'], [OrderStatus::STATUS_NEW, OrderStatus::STATUS_WAIT_CANCEL, OrderStatus::STATUS_WAIT_APPROVE])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access denied'
            ];
        }

        $penalty_gift_time_max = \Yii::$app->params['penalty_gift_time_max'];

        if($this->_userType != User::USER_SUPERADMIN && (strtotime($result[0]['created_at']) < (time()-$penalty_gift_time_max))){
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access denied time out'
            ];
        }

        $result = Order::updateAll(['order_status_id' => $status, 'updated_at' => date('Y-m-d H:i:s', time()),'comment'=>$comment], ['action_id' => $shopItemID]);

        return [
            'success' => true            
        ];

    }

    public function getGiftTypes()
    {
        $ress = GiftType::find()->asArray()->all();
        if(empty($ress)){
            return ['success' => FALSE, 'message' => 'Gift type empty'];
        }

        return ['success' => TRUE, 'gift_type_mass' => $ress];
    }

    public function updatePenaltyShopItem()
    {
        $penaltyGiftTime = \Yii::$app->params['penalty_gift_time'];        
        $penaltyGiftTimeMax = \Yii::$app->params['penalty_gift_time_max'];        
        $billingService = new BillingService();
        $priceIncriment = $billingService->getPenaltyValue('gifts');
        
        $result = Order::updateAll(
            ['penalty_value' => new Expression("(TIMESTAMPDIFF(DAY,created_at,NOW())+1-".floor($penaltyGiftTime/86400).") * ".$priceIncriment)],
            [
                'and',
                    ['<','created_at',date("Y-m-d H:i:s",time()-$penaltyGiftTime)],
                    ['>','created_at',date("Y-m-d H:i:s",time()-$penaltyGiftTimeMax)],
                    ['in','order_status_id',[1,2,4]],
            ]
        );

        return $result;

    }

    public function getPenaltyShopItems($dateFrom = null, $dateTo = null, $agencyID = null)
    {

        $user = User::findOne(['id' => $this->_userID]);
        if (!empty($user)) {
            $myIDs = $user->getMyChidrensIds();    
        } else {
            return [
                'success' => false,
                'message' => 'User not found'
            ];
        }        

        $subQuery = (new Query)
            ->select('first_name, last_name, id')
            ->from([UserProfile::tableName() . ' user_profile']);

        $subQuery2 = (new Query)
            ->select('agency.name, user.agency_id, user.id as agency_user_id')
            ->from(User::tableName())
            ->leftJoin(Agency::tableName(), 'user.agency_id = agency.id')
            ->where(['user_type' => User::USER_AGENCY]);

        $query = (new Query())
            ->select('order.*, user_profile.first_name as user_to_name, 
             user_profile.last_name as user_to_last_name, user_profile.id as user_to_id, 
             user_profile2.id as user_from_id, user_profile2.first_name as user_from_name, 
             user_profile2.last_name as user_from_last_name, agency.agency_id as agency_id, 
             agency.name as agency_name, agency_user_id')
            ->from([Order::tableName()])
            ->leftJoin(Action::tableName(), 'action.id = order.action_id')
            ->leftJoin(User::tableName(), 'user.id = action.action_receiver')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_receiver')
            ->leftJoin(['user_profile2' => $subQuery], 'user_profile2.id = action.action_creator')
            ->leftJoin(['agency' => $subQuery2], 'agency.agency_id = user.agency_id')
            ->where(['>', 'order.penalty_value', 0])
            ->andWhere(['user_type' => User::USER_FEMALE]);
        if (is_array($myIDs)) {
            $query->andWhere(['in', 'action.action_receiver', $myIDs]);
        }elseif(is_numeric($myIDs)){
            $query->andWhere(['action.action_creator' => $myIDs])
                ->orWhere(['action.action_receiver' => $myIDs]);
        }

        if (!empty($dateFrom)) {
            $query->andWhere(['>=', 'order.created_at', $dateFrom]);
        }
        if (!empty($dateTo)) {
            $query->andWhere(['<=', 'order.created_at', $dateTo]);
        }
        if (!empty($agencyID)) {
            $query->andWhere(['agency_user_id' => $agencyID]);
        }

        $query->orderBy('order.action_id DESC');

        $result = $query->all();

        return $result;

    }


}
