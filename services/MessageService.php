<?php
namespace app\services;

use Yii;
use app\models\Message;
use app\models\User;
use app\models\UserProfile;
use app\models\Photo;
use app\models\Agency;
use yii\db\Query;

class MessageService
{
    private $userId    = null;
    private $userType = null;

    function __construct($userID = null)
    {
        if ($userID !== null) {
            self::setUser($userID);
        }
    }

    public function setUser($userID)
    {
        $user = User::findOne(['id' => $userID]);
        if (empty($user)) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }

        $this->userId = $userID;
        $this->userType = $user->user_type;
    }

    public function sendMessage($toUser, $title, $desc, $lastMessageId = null)
    {
        if ((empty($toUser) && empty($this->userId)) || $title === null || $desc === null) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }

        $otherUser = User::findOne(['id' => $toUser]);
        if (empty($otherUser)) {
            return [
                'success' => false, 
                'message' => 'User not found'
            ];
        }

        $message = new Message();
        $message->message_creator = ($this->userId == $otherUser->id && $this->userType == User::USER_SUPERADMIN) ? null : $this->userId;
        $message->message_receiver = $toUser;
        $message->title = $title;
        $message->description  = $desc;
        $message->was_deleted_by_user = Message::WASNT_DELETED;
        $message->was_deleted_by_admin = Message::WASNT_DELETED;
        if (!$message->save()) {
            return [
                'success' => false, 
                'message' => 'Error save'
            ];
        }

        if (!empty($lastMessageId)) {
            //ставим отвеченное предыдущему письму
            $lastMessage = Message::findOne($lastMessageId);
            if (empty($lastMessage)) {
                return [
                    'success' => false, 
                    'message' => 'Last message ID not found'
                ];
            }
            $lastMessage->reply_status = 1;
            if (!$lastMessage->save()) {
                return [
                    'success' => false, 
                    'message' => 'Error save message status'
                ];
            }
        }

        return [
            'success' => true, 
            'message_id' => $message->id, 
            'message' => 'System message send'
        ];

    }

    /**
     * @param $id
     * $status
     * status = 1 активно
     * status = 0 удалено/не отображать
     *
     * @return int
     * @throws NotAcceptableHttpException
     */
    public function deleteMessage($id, $status = Message::WAS_DELETED)
    {
        $deleteStatus = null;

        if ($this->userType == User::USER_MALE || $this->user_type == User::USER_FEMALE) {
            $deleteStatus = 'was_deleted_by_user'; // for man & women
        }

        if (empty($deleteStatus)) {
            return [
                'success' => false, 
                'message' => 'Error user type'
            ];
        }

        if (!is_array($id) && is_numeric($id)) {
            $message = Message::find()
                ->where(['id' => $id])
                ->andWhere(['or','message_creator ='.$this->userId,'message_receiver = '.$this->userId])
                ->one();

            if (empty($message)) {
                return [
                    'success' => false, 
                    'message' => 'Message not found'
                ];
            }

            $message->$deleteStatus = $status;

            if (!$message->save()) {
                return [
                    'success' => false, 
                    'message' => 'Message error save'
                ];
            }
        } elseif (is_array($id) && !empty($id)) {
            $message = (new Query())
                ->from([Message::tableName()]);
            $message->where([
                'and', ['in', 'id', $id],
                ['or', "message_creator = $this->userId", "message_receiver = $this->userId"],
            ]);
            if ($message->count() != count($id)) {
                return [
                    'success' => false, 
                    'message' => 'One of message not found'
                ];
            }

            $result = Message::updateAll([$deleteStatus => $status], ['in', 'id', $id]);
            if (!$result) {
                return [
                    'success' => false, 
                    'message' => 'Message update error'
                ];
            }
        }

        return [
            'success' => true
        ];
    }

    public function readMessage($id)
    {
        if (!is_numeric($id)) {
            return [
                'success' => false, 
                'message' => 'Error message ID'
            ];
        }

        //если не админ
        if (!in_array($this->userType, [User::USER_SUPERADMIN, User::USER_AGENCY, User::USER_ADMIN])) {
            $message = (new Query())
                ->from([Message::tableName()])
                ->where(['id' => $id, 'readed_at' => null])                
                ->andWhere(['or', ['message_receiver' => $this->userId], ['message_creator' => $this->userId]])
                ->one();
        } else {
            if (Yii::$app->user->identity->user_type == User::USER_SUPERADMIN) {           
                $adminIds = User::find()->where(['user_type' => User::USER_SUPERADMIN])->all();
                $adminIdsArray = [];
                if(!empty($adminIds)){
                    foreach($adminIds as $adminId) {
                        $adminIdsArray[] = $adminId->id;
                    }
                }
            } elseif (Yii::$app->user->identity->user_type == User::USER_ADMIN || Yii::$app->user->identity->user_type == User::USER_AGENCY) {
                $adminIdsArray = [];
                $adminIdsArray[] = Yii::$app->user->identity->id;
            }  

            $message = (new Query())
                ->from([Message::tableName()])
                ->where(['id' => $id, 'readed_at' => null])
                ->andWhere(['in','message_receiver', $adminIdsArray])
                ->one();
        }

        if ($message) {
            $messageModel = Message::findOne(['id' => $id]);
            if (empty($messageModel->readed_at)) {
                $messageModel->readed_at = date('Y-m-d H:i:s', time());
                if (!$messageModel->update(false)) {
                    return [
                        'success' => false, 
                        'message' => 'Message read error save'
                    ];
                }
            }            
        }
        return [
            'success' => true
        ];
    }

    public function getMessage($messageID, $limit = null, $offset = null)
    {
        if ($this->userId === null) {
            return [
                'success' => false, 
                'message' => 'User Id is null'
            ];
        }

        $query = new Query();
        
        $message = $query->addSelect(['message.*', 'photo.medium_thumb as another_user_avatar', 'user_profile.first_name as another_user_name'])   
            ->from([Message::tableName()])                     
            ->leftJoin(User::tableName(), 'user.id = message.message_creator')
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = message.message_creator')
            ->leftJoin(Photo::tableName(), 'user.avatar_photo_id = photo.id')          
            ->where(['message.message_creator' => $this->userId])
            ->orWhere(['message.message_receiver' => $this->userId]);

        if (in_array($this->userType, [User::USER_SUPERADMIN, User::USER_AGENCY, User::USER_ADMIN])) {
            $message->addSelect('user.id as another_user_id');
        }

        if ($this->userType == User::USER_MALE || $this->userType == User::USER_FEMALE) {
            $message->andWhere(['message.was_deleted_by_user' => Message::WASNT_DELETED]);
        }
        if ($messageID !== null && is_numeric($messageID)) {
            $message->andWhere(['message.id' => $messageID]);
        }

        $countMessages = $message->count();

        if ($limit !== null && is_numeric($limit)) {
            $message->limit($limit);
        }
        if ($offset !== null && is_numeric($offset)) {
            $message->offset($offset);
        }

        $result = $message->orderBy(['created_at' => SORT_DESC, 'message.id' => SORT_DESC])->all();

        return [
            'success' => true, 
            'messageArray' => $result, 
            'count' => $countMessages
        ];
    }

    public function getMessagesCount()
    {
        if ($this->userId === null) {
            return [
                'success' => false, 
                'message' => 'User Id is null'
            ];
        }

        $message = Message::find()         
            ->where(['message.message_receiver' => $this->userId]);

        if ($this->userType == User::USER_MALE || $this->userType == User::USER_FEMALE) {
            $message->andWhere(['message.was_deleted_by_user' => Message::WASNT_DELETED]);
        }

        $countMessages = $message->count();

        return [
            'success' => true, 
            'count' => $countMessages
        ];
    }

    public function getMessageToAdmin($messageId = null, $limit = null, $offset = null, $params)
    {
        if ($this->userId === null) {
            return [
                'success' => false, 
                'message' => 'User Id is null'
            ];
        }

        $subQuery = (new Query)
            ->select('first_name, last_name, id')
            ->from([UserProfile::tableName()]);
        $subQuery2 = (new Query)
            ->select('medium_thumb, id')
            ->from([Photo::tableName()]);

        $type = isset($params['type']) ? $params['type'] : 'inbox';
        $answered = isset($params['answered']) ? $params['answered'] : null;
        $read = isset($params['read']) ? $params['read'] : null;
        $creator = isset($params['userID_from']) && is_numeric($params['userID_from']) ? (int)$params['userID_from'] : null;
        $receiver = isset($params['userID_to']) && is_numeric($params['userID_to']) ? (int)$params['userID_to'] : null;
        $dateFrom = isset($params['dateFrom']) && strtotime($params['dateFrom']) ? $params['dateFrom'] : null;
        $dateTo = isset($params['dateTo']) && strtotime($params['dateTo']) ? $params['dateTo'] : null;

        if ($type == 'inbox') {
            $otherUserName = 'message_creator';
            $userName = 'message_receiver';
        } else {
            $otherUserName = 'message_receiver';
            $userName = 'message_creator';
        }
        if (Yii::$app->user->identity->user_type == User::USER_SUPERADMIN || Yii::$app->user->identity->user_type == User::USER_SITEADMIN) {
            $adminIds = User::find()->where(['user_type' => Yii::$app->user->identity->user_type])->all();
            $adminIdsArray = [];
            if(!empty($adminIds)){
                foreach($adminIds as $adminId) {
                    $adminIdsArray[] = $adminId->id;
                }
            }
        } elseif (Yii::$app->user->identity->user_type == User::USER_ADMIN || Yii::$app->user->identity->user_type == User::USER_AGENCY) {
            $adminIdsArray = [];
            $adminIdsArray[] = Yii::$app->user->identity->id;
        }        

        $query    = new Query();
        $message = $query->addSelect(['message.*', 'agency.name', 'user_profile.first_name','user_profile.last_name','photo.medium_thumb as another_user_avatar'])
            ->from([Message::tableName()])
            ->leftJoin(User::tableName(), 'user.id = message.'.$otherUserName)
            ->leftJoin(Agency::tableName(), 'agency.id = user.agency_id')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = message.'.$otherUserName)
            ->leftJoin(['photo' => $subQuery2], 'user.avatar_photo_id = photo.id');

        if ($messageId !== null && is_numeric($messageId)) {
            $message->andWhere(['message.id' => $messageId]);
        } else {
            //если нету message_id то все письма которые адресованы админам
            $query->andWhere(['in','message.'.$userName, $adminIdsArray]);
        }
        if (Yii::$app->user->identity->user_type == User::USER_ADMIN || 
            Yii::$app->user->identity->user_type == User::USER_AGENCY) {
            $query->andWhere(['message.'.$userName => Yii::$app->user->identity->id]);
        }


        if ($answered !== null && $answered != "") {
            if ($answered == 0) {
                $message->andWhere(['message.reply_status' => null]);
            } else {
                $message->andWhere('message.reply_status IS NOT NULL');
            }   
            
        }
        if ($read !== null && $read != '') {
            if ($read == 0) {
                $message->andwhere('message.readed_at IS NULL');
            } else {
                $message->andwhere('message.readed_at IS NOT NULL');
            }
        }
        if ($dateFrom) {
            $message->andwhere(['>=', 'message.created_at', $dateFrom]);
        }
        if ($dateTo) {
            $message->andwhere(['<=', 'message.created_at', $dateTo]);
        }
        if ($creator) {
            $message->andwhere(['message.message_creator' => $creator]);
        }
        if ($receiver) {
            $message->andwhere(['message_receiver' => $receiver]);
        }

        $countMessages = $message->count();

        if ($limit !== null && is_numeric($limit)) {
            $message->limit($limit);
        }
        if ($offset !== null && is_numeric($offset)) {
            $message->offset($offset);
        }

        $result = $message->orderBy(['created_at' => SORT_DESC, 'message.id' => SORT_DESC])->all();

        return [
            'success' => true, 
            'messageArray' => $result, 
            'count' => $countMessages
        ];
    }

    public function getPrevNextMessageIds($messageId)
    {

        if (!is_numeric($messageId)) {
            return [
                'success' => false, 
                'message' => 'Message_id error'
            ];
        }

        $prevId = null;
        $nextId = null;

        $message = (new Query())
            ->from([Message::tableName()])
            ->addSelect(['message.id'])
            ->where(['message.message_receiver' => $this->userId, 'message.was_deleted_by_user' => Message::WASNT_DELETED])
            ->all();

        if (empty($message)) {
            return [
                'prev' => null, 
                'next' => null
            ];
        }

        for ($i = 0; $i < count($message); $i++) {
            if ($message[$i]['id'] == $messageId) {
                $prevId = isset($message[$i - 1]['id']) ? $message[$i - 1]['id'] : null;
                $nextId = isset($message[$i + 1]['id']) ? $message[$i + 1]['id'] : null;
            }
        }

        return [
            'prev' => $prevId, 
            'next' => $nextId
        ];

    }

    public function getNewMessage()
    {
        $deleteStatus = ($this->userType == User::USER_MALE || $this->userType == User::USER_FEMALE) ? 'was_deleted_by_user' : false; // for man & women
        if (!$deleteStatus) {
            return [
                'count' => 0,
                'users' => false
            ];
        }
        $messages = (new Query())
            ->from([Message::tableName()])
            ->where(['message_receiver' => $this->userId, 'readed_at' => null, $deleteStatus => Message::WASNT_DELETED]);
        $count = $messages->count();

        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, id')
            ->from([UserProfile::tableName()]);
        //подзапрос на фото пользователя
        $subQuery2 = (new Query)
            ->select('user.id, photo.small_thumb')
            ->from([User::tableName()])
            ->leftJoin(Photo::tableName(), 'user.avatar_photo_id = photo.id');

        $lastName = $messages
            ->addSelect('user_profile.first_name, photo.small_thumb')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = message_creator')
            ->leftJoin(['photo' => $subQuery2], 'photo.id = message_creator')
            ->orderBy('message.id DESC')
            ->all();

        return [
            'count' => $count,
            'users' => $lastName
        ];
    }

}