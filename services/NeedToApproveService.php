<?php
namespace app\services;

use Yii;

use app\models\Agency;
use app\models\Action;
use app\models\Photo;
use app\models\Album;
use app\models\Video;
use app\models\User;
use app\models\Order;
use app\models\OrderStatus;
use app\models\Letter;
use app\models\Message;
use app\models\LetterTheme;
use app\models\Task;
use app\models\TaskType;
use app\models\UserProfile;
use app\models\ApproveStatus;
use app\models\MailingTemplate;
use yii\db\Query;


class NeedToApproveService
{
    private $_userID;
    private $_userType = null;
    private $_agencyID;
    private $_children = [];

    function __construct($userID)
    {
        if ($userID !== null) {
            self::setUser($userID);
        }
    }

    public function setUser($userID)
    {
        $user = User::findOne(['id' => $userID]);
        if (empty($user)) {
            return [
                'success' => false, 
                'message' => 'User_id not found'
            ];
        }

        $this->_userID   = $user->id;
        $this->_userType = $user->user_type;
        $this->_agencyID = $user->agency_id;
        $this->_children = $user->getMyChidrensIds();
    }

    public function getNeedToApproveAgencies($agencyID = null) 
    {      
        $query =  $this->agencyQuery();
        if ($this->_userType == User::USER_SUPERADMIN && $agencyID != null) {
            $query->andWhere(['agency.id' => $agencyID]);                 
        } elseif ($this->_userType == User::USER_AGENCY && $this->_agencyID != null) {
            $query->andWhere(['agency.id' => $this->_agencyID]);
        }

        return $query;
    }     

    public function getNeedToApproveUsers($agencyID = null) 
    {
        $query = $this->userQuery();
        if ($this->_userType == User::USER_SUPERADMIN && $agencyID != null) {            
            $query->andWhere(['agency.id' => $agencyID]);                 
        } elseif ($this->_userType == User::USER_AGENCY && $this->_agencyID != null) {
            $query->andWhere(['agency.id' => $this->_agencyID]);
        }

        return $query;
    }

    public function getNeedToApproveUserByUserID($userID = null) 
    {
        $query = $this->userQuery();
        if ($this->_userType == User::USER_SUPERADMIN && $userID != null) {            
            $query->andWhere(['user.id' => $userID]);                 
        }

        return $query;
    }

    public function getNeedToApproveVideo($agencyID = null) 
    {
        $query = $this->videoQuery();

        if ($this->_userType == User::USER_SUPERADMIN && $agencyID != null) {            
            $query->andWhere(['user.agency_id' => $agencyID]);                 
        } elseif ($this->_userType == User::USER_AGENCY && $this->_agencyID != null) {
            $query->andWhere(['user.agency_id' => $this->_agencyID]);
        }        

        return $query;   
    }

    public function getNeedToApproveVideoByUserID($userID = null) 
    {
        $query = $this->videoQuery();

        if ($this->_userType == User::USER_SUPERADMIN && $userID != null) {            
            $query->andWhere(['user.id' => $userID]);                 
        }      

        return $query;   
    }

    public function getNeedToApprovePhoto($agencyID = null) 
    {
        $query = $this->photoQuery();
        $query->andWhere(['photo.approve_status' => [ApproveStatus::STATUS_NOT_APPROVED, ApproveStatus::STATUS_IN_PROGRESS]]);
        $query->leftJoin(User::tableName(), 'user.id = user_profile.id');
        $query->addSelect(['user.agency_id']);

        if ($this->_userType == User::USER_SUPERADMIN && $agencyID != null) {            
            $query->andWhere(['agency_id' => $agencyID]);                 
        } elseif ($this->_userType == User::USER_AGENCY && $this->_agencyID != null) {
            $query->andWhere(['agency_id' => $this->_agencyID]);
        }        

        return $query;   
    }

    public function getNeedToApprovePhotoByUserID($userID = null) 
    {
        $query = $this->photoQuery();
        $query->where(['photo.approve_status' => [ApproveStatus::STATUS_NOT_APPROVED, ApproveStatus::STATUS_IN_PROGRESS]]);
        $query->leftJoin(User::tableName(), 'user.id = user_profile.id');
        $query->addSelect(['user.agency_id']);

        if ($this->_userType == User::USER_SUPERADMIN && $userID != null) {            
            $query->andWhere(['user.id' => $userID]);                 
        }      

        return $query;   
    }

    public function getNeedToApproveDeletedPhoto($agencyID = null) 
    {
        $query = $this->photoQuery();
        $query->where(['photo.status' => [Photo::STATUS_DELETED]]);
        $query->leftJoin(User::tableName(), 'user.id = user_profile.id');
        $query->addSelect(['user.agency_id']);
        
        if ($this->_userType == User::USER_SUPERADMIN && $agencyID != null) {            
            $query->andWhere(['agency_id' => $agencyID]);                 
        } elseif ($this->_userType == User::USER_AGENCY && $this->_agencyID != null) {
            $query->andWhere(['agency_id' => $this->_agencyID]);
        }        

        return $query;   
    }

    public function getNeedToApproveDeletedPhotoByUserID($userID = null) 
    {
        $query = $this->photoQuery();
        $query->where(['photo.status' => [Photo::STATUS_DELETED]]);
        $query->leftJoin(User::tableName(), 'user.id = user_profile.id');

        if ($this->_userType == User::USER_SUPERADMIN && $userID != null) {            
            $query->andWhere(['user.id' => $userID]);                 
        }       

        return $query;   
    }

    public function getNeedToApproveGifts($agencyID = null) 
    {
        $query = $this->giftQuery();

        if ($this->_userType == User::USER_SUPERADMIN && $agencyID != null) {                        
            $query->andWhere(['agency_id' => $agencyID]);                 
        } elseif (($this->_userType == User::USER_AGENCY || $this->_userType == User::USER_ADMIN) && $this->_agencyID != null) {
            $query->andWhere(['agency_id' => $this->_agencyID]);
        } else {
            $query->groupBy('user.agency_id');
        }        

        return $query;   
    }

    public function getNeedToApproveTasks($agencyID = null) 
    {
        $query = $this->taskQuery();

        if ($this->_userType == User::USER_SUPERADMIN && $agencyID != null) {            
            $query->andWhere(['agency_id' => $agencyID]);                 
        } elseif (($this->_userType == User::USER_AGENCY || $this->_userType == User::USER_ADMIN) && $this->_agencyID != null) {
            $query->andWhere(['agency_id' => $this->_agencyID]);
        }        

        return $query;   
    }

    public function getNeedToReplyLetters($agencyID = null)
    {
        $query = $this->letterQuery();

        if ($this->_userType == User::USER_SUPERADMIN && $agencyID != null) {
            $query->andWhere(['agency_id' => $agencyID]);     
        } elseif (($this->_userType == User::USER_AGENCY || $this->_userType == User::USER_ADMIN) && $this->_agencyID != null) {
            $query->andWhere(['user_receiver.agency_id' => $this->_agencyID]);
        }

        return $query;
    }

    public function getNeedToReplyMessages($agencyID = null)
    {
        $query = $this->messageQuery();

        if ($this->_userType == User::USER_SUPERADMIN && $agencyID != null) {
            $query->andWhere(['agency_id' => $agencyID]);     
        } elseif (($this->_userType == User::USER_AGENCY || $this->_userType == User::USER_ADMIN) && $this->_agencyID != null) {
            $query->andWhere(['user_receiver.agency_id' => $this->_agencyID]);
        }

        return $query;
    }

    public function getUndoneTasks($agencyID = null)
    {
        $query = $this->undoneTaskQuery();

        if ($this->_userType == User::USER_SUPERADMIN && $agencyID != null) {
            $query->andWhere(['agency_id' => $agencyID]);             
        } elseif (($this->_userType == User::USER_AGENCY || $this->_userType == User::USER_ADMIN) && $this->_agencyID != null) {
            $query->andWhere(['agency_id' => $this->_agencyID]);
        }

        return $query;        
    }

    public function getUndoneGifts($agencyID = null)
    {
        $query = $this->undoneGiftQuery();

        if ($this->_userType == User::USER_SUPERADMIN && $agencyID != null) {                        
            $query->andWhere(['agency_id' => $agencyID]);                 
        } elseif (($this->_userType == User::USER_AGENCY || $this->_userType == User::USER_ADMIN) && $this->_agencyID != null) {
            $query->andWhere(['agency_id' => $this->_agencyID]);
        } else {
            $query->groupBy('user.agency_id');
        }

        return $query;    
    }

    public function getNotApprovedChatTemplates($agencyID = null)
    {
        $query = $this->notApprovedChatTemplateQuery();

        if ($this->_userType == User::USER_SUPERADMIN && $agencyID != null) {                        
            $query->andWhere(['agency_id' => $agencyID]);                 
        } elseif ($this->_userType == User::USER_AGENCY && $this->_agencyID != null) {
            $query->andWhere(['agency_id' => $this->_agencyID]);
            $query->andWhere(['or', ['template_creator' => $this->_children], ['template_creator' => $this->_userID]]);
        } else {
            $query->groupBy('mailing_template.agency_id');
        }

        return $query;    
    }

    public function getNotApprovedMessageTemplates($agencyID = null)
    {
        $query = $this->notApprovedMessageTemplateQuery();

        if ($this->_userType == User::USER_SUPERADMIN && $agencyID != null) {                        
            $query->andWhere(['agency_id' => $agencyID]);                 
        } elseif ($this->_userType == User::USER_AGENCY && $this->_agencyID != null) {
            $query->andWhere(['agency_id' => $this->_agencyID]);
            $query->andWhere(['or', ['template_creator' => $this->_children], ['template_creator' => $this->_userID]]);
        } else {
            $query->groupBy('mailing_template.agency_id');
        }

        return $query;    
    }          

    private function agencyQuery() 
    {
        return Agency::find()
            ->select('user.id, agency.approve_status, user.last_activity, agency.name')
            ->leftJoin(User::tableName(), 'user.agency_id = agency.id')
            ->where(['user.user_type' => User::USER_AGENCY])
            ->andWhere(['in', 'agency.approve_status', [ApproveStatus::STATUS_NOT_APPROVED, ApproveStatus::STATUS_IN_PROGRESS]]);
    }

    private function userQuery() 
    {
        return User::find()
            ->select('user.id, user.last_activity, user_profile.first_name, user_profile.last_name, approve_status.description, agency.name, user.agency_id')
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = user.id')
            ->leftJoin(ApproveStatus::tableName(), 'approve_status.id = user.approve_status_id')
            ->leftJoin(Agency::tableName(), 'agency.id = user.agency_id')
            ->where(['in', 'approve_status_id', [ApproveStatus::STATUS_NOT_APPROVED, ApproveStatus::STATUS_IN_PROGRESS]]);
    }

    private function videoQuery()
    {
        return Video::find()
            ->select('video.id, user.agency_id, user_profile.first_name, user_profile.last_name, user_profile.id as user_id, video.created_at')
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = video.user_id')
            ->leftJoin(User::tableName(), 'user.id = user_profile.id')
            ->where(['video.status' => [ApproveStatus::STATUS_NOT_APPROVED, ApproveStatus::STATUS_IN_PROGRESS]]);
    }

    private function photoQuery()
    {
        return Photo::find()
            ->select('photo.id, album.created_at, photo.album_id, photo.small_thumb, user_profile.first_name, user_profile.last_name, user_profile.id as user_id')
            ->leftJoin(Album::tableName(), 'album.id = photo.album_id')
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = album.user_id');            
    }

    private function giftQuery()
    {
        return Order::find()
            ->select('user.agency_id, agency.name, user.id, user_profile.first_name, user_profile.last_name, action.id as action_id')
            ->leftJoin(Action::tableName(), 'action.id = order.action_id')
            ->leftJoin(User::tableName(), 'user.id = action.action_receiver')
            ->leftJoin(Agency::tableName(), 'user.agency_id = agency.id')
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = user.id')
            ->where(['user_type' => User::USER_FEMALE])
            ->andWhere(['order.order_status_id' => OrderStatus::STATUS_WAIT_APPROVE]);
    }

    private function undoneGiftQuery()
    {
        return Order::find()
            ->select('user.agency_id, agency.name, user.id, user_profile.first_name, user_profile.last_name, action.id as action_id')
            ->leftJoin(Action::tableName(), 'action.id = order.action_id')
            ->leftJoin(User::tableName(), 'user.id = action.action_receiver')
            ->leftJoin(Agency::tableName(), 'user.agency_id = agency.id')
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = user.id')
            ->where(['user_type' => User::USER_FEMALE])
            ->andWhere(['order.order_status_id' => [OrderStatus::STATUS_WAIT_APPROVE, OrderStatus::STATUS_NEW]]);
    }

    private function taskQuery()
    {
        return Task::find()
            ->select('task.task_performer, agency.name, task.id, task.description, user.agency_id as agency_id')
            ->leftJoin(User::tableName(), 'user.id = task.task_performer')
            ->leftJoin(Agency::tableName(), 'user.agency_id = agency.id')
            ->where(['task.status_id' => TaskType::TYPE_WAIT_TO_APPROVE, 'user_type' => User::USER_AGENCY])
            ->groupBy('task.task_performer');
    }

    private function undoneTaskQuery()
    {
        return Task::find()
            ->select('task.task_performer, agency.name, task.id, task.description, user.agency_id as agency_id')
            ->leftJoin(User::tableName(), 'user.id = task.task_performer')
            ->leftJoin(Agency::tableName(), 'user.agency_id = agency.id')
            ->where(['task.status_id' => [TaskType::TYPE_NEW, TaskType::TYPE_WAIT_TO_APPROVE], 'user_type' => User::USER_AGENCY])
            ->groupBy('task.task_performer');
    }    

    private function letterQuery()
    {
        $subQuery = UserProfile::find()
            ->select(['user_profile.id', 'user_profile.first_name', 'user_profile.last_name', 'user.agency_id'])
            ->leftJoin(User::tableName(), 'user.id = user_profile.id');


        return Letter::find()
            ->select('letter.id, letter_theme.theme, letter.description, user_sender.first_name, user_sender.last_name')
            ->leftJoin(Action::tableName(), 'action.id = letter.action_id')
            ->leftJoin(LetterTheme::tableName(), 'letter.theme_id = letter_theme.id')
            ->leftJoin(['user_sender' => $subQuery], 'action.action_creator = user_sender.id')
            ->leftJoin(['user_receiver' => $subQuery], 'action.action_receiver = user_receiver.id')
            ->where(['or', ['reply_status' => Letter::HAVENT_REPLY], ['reply_status' => null]])
            ->andWhere(['letter.was_deleted_by_man' => Letter::WASNT_DELETED, 
                        'letter.was_deleted_by_girl' => Letter::WASNT_DELETED]);
    }

    private function messageQuery()
    {
        $subQuery = UserProfile::find()
            ->select(['user_profile.id', 'user_profile.first_name', 'user_profile.last_name', 'user.agency_id'])
            ->leftJoin(User::tableName(), 'user.id = user_profile.id');


        return Message::find()
            ->select('message.id, message.title, letter.description, user_sender.first_name, user_sender.last_name')
            ->leftJoin(['user_sender' => $subQuery], 'message_creator = user_sender.id')
            ->leftJoin(['user_receiver' => $subQuery], 'message_receiver = user_receiver.id')
            ->where(['or', ['reply_status' => Message::HAVENT_REPLY], ['reply_status' => null]])
            ->andWhere(['message.was_deleted_by_user' => Message::WASNT_DELETED, 
                        'message.was_deleted_by_admin' => Message::WASNT_DELETED]);
    }

    private function notApprovedChatTemplateQuery()
    {
        return MailingTemplate::find()
            ->select('first_name, last_name, agency.name, mailing_template.id, title, body')
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = template_creator')
            ->leftJoin(Agency::tableName(), 'agency.id = mailing_template.agency_id')
            ->where(['mailing_template.status' => [ApproveStatus::STATUS_NOT_APPROVED, ApproveStatus::STATUS_IN_PROGRESS],
                     'created_for' => MailingTemplate::CREATED_FOR_CHAT]);     
    }

    private function notApprovedMessageTemplateQuery()
    {
        return MailingTemplate::find()
            ->select('first_name, last_name, agency.name, mailing_template.id, title, body')
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = template_creator')
            ->leftJoin(Agency::tableName(), 'agency.id = mailing_template.agency_id')
            ->where(['mailing_template.status' => [ApproveStatus::STATUS_NOT_APPROVED, ApproveStatus::STATUS_IN_PROGRESS],
                     'created_for' => MailingTemplate::CREATED_FOR_MESSAGE]);  
    }

}