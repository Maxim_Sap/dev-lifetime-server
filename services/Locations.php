<?php
namespace app\services;

use app\models\City;
use app\models\Country;
use app\models\LocationType;
use app\models\Location as LocationsModel;
use yii\web\NotAcceptableHttpException;
use yii\db\Query;
use yii\helpers\Json;

class Locations
{
    private $_userID;

    /**
     * @param $city
     *
     * @return int
     * @throws NotAcceptableHttpException
     */

    function __construct($userID = null)
    {
        if ($userID !== null) {
            self::setUser($userID);
        }
    }

    public static function city($cityName)
    {
        $cityName = trim($city);

        $city = City::findOne(['name' => $cityName]);

        if (!empty($city)) {
            return $city->id;
        }

        $city = new City();
        $city->title = $city;
        $city->proxy = null;

        if (!$city->save()) {
            return JSON::encode([
                'success' => false, 
                'message' => 'Error occurred while processing your request'
            ]);
        }

        return $city->id;
    }

    protected function setUser($userID)
    {
        $this->_userId = $userID;
    }

    protected function setLocation($location)
    {
        self::set($location);
    }

    /**     
     * @param $location
     *
     * @throws NotAcceptableHttpException
     */
    public static function set($location)
    {
//        $location = [
//            'country' => 1,              // Если строка - поиск в БД номера
//            'city'    => "Nikolaev",     // Если строка - поиск в БД номера
//            'type'    => 3,              // Если строка - поиск в БД номера
//            'start'   => '2018-04-01',
//            'end'     => '2018-04-30',   // Необязательный
//        ];

        if (!isset($location['country'], $location['type'])) {
            return json_encode(['success' => false, 'message' => 'Missing params']);
        }

        if (isset($location['start']) && !strtotime($location['start'])) {
            return json_encode(['success' => false, 'message' => 'Location - incorrect date params']);
        }

        if (isset($location['end']) && !strtotime($location['end'])) {
            return json_encode(['success' => false, 'message' => 'Location - incorrect date params']);
        }

        if (isset($location['city']) && trim($location['city']) != "") {
            $cityId = is_numeric($location['city']) ? $location['city'] : self::City($location['city']);
        } else {
            $cityId = null;
        }

        $locationTypeId = is_numeric($location['type']) ? $location['type'] : self::Type($location['type']);

        $currentLocation = LocationsModel::findOne(['user_id' => $user, 'location_type_id' => $location_type_id]);
        if (empty($currentLocation)) {
            $currentLocation = new LocationsModel();
        }

        $currentLocation->user_id          = $user;
        $currentLocation->country_id       = is_numeric($location['country']) ? $location['country'] : self::Country($location['country']);
        $currentLocation->city_id          = $cityId;
        $currentLocation->location_type_id = $locationTypeId;

        if (!$currentLocation->save()) {
            return ['success' => false, 'message' => $currentLocation->errors];
        }

        return ['success' => true];
    }

    /**
     * @param $country
     *
     * @return int
     * @throws NotAcceptableHttpException
     */

    public static function country($country)
    {
        $country      = trim($country);
        $countryModel = Country::findOne(['title' => $country]);

        if (!empty($countryModel)) {
            return $countryModel->country_id;
        }

        $countryModel        = new Country();
        $countryModel->title = $country;
        $countryModel->proxy = null;

        if (!$countryModel->save()) {
            return [
                'success' => false, 
                'message' => 'Error Countries save'
            ];
        }

        return $countryModel->id;
    }

    public static function getCountry()
    {
        $country = Countries::find()
            ->orderBy('sort DESC, title ASC')
            ->asArray()->all();

        return $country;
    }

    /**
     * @param $title
     *
     * @return int
     * @throws NotAcceptableHttpException
     */
    public static function type($title)
    {
        $title = trim(strtolower($title));

        $locTypeModel = LocationType::findOne(['title' => $title]);

        if (!empty($locTypeModel)) {
            return $locTypeModel->location_type_id;
        }

        $locTypeModel        = new LocationTypes();
        $locTypeModel->title = $title;

        if (!$locTypeModel->save()) {
            return ['success' => false, 'message' => 'Error Location_type save'];
        }

        return $locTypeModel->location_type_id;
    }

    static public function getCitiesByLitera($litera, $limit = 10)
    {
        /*
        * @param litera
        */
        if (!isset($litera) || trim($litera) == '' || strlen($litera) < 3) {
            return ['success' => false, 'message' => 'Missing params litera'];
        }

        $litera = mb_strtolower(trim($litera), 'UTF-8');
        $ress   = City::find()->Where(['LIKE', 'LOWER(title)', $litera . '%', false])->orderBy(['original' => SORT_DESC])->limit($limit)->asArray()->all();

        return $ress;
    }

    public static function getLocationByIds($ids)
    {
        $query          = new Query();
        $location = $query->addSelect([
            'location.id',
            'location.user_id',
            'city'       => 'city.name',
            'city_id'    => 'city.id',
            'country'    => 'country.name',
            'country_id' => 'country.id',
            'count'      => 'COUNT(*)',
        ])
            ->from([LocationsModel::tableName()])
            ->leftJoin(City::tableName(), 'city.id = location.city_id')
            ->leftJoin(Country::tableName(), 'country.id = location.country_id')
            ->where(['in', 'location.user_id', $ids])
            ->groupBy(['country.id']);
        $result = $location->all();

        return $result;
    }

    public function deleteAllUserLocation()
    {
        return LocationsModel::deleteAll(['user_id' => $this->_userID]);
    }

}
