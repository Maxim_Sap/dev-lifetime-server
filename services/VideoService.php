<?php
namespace app\services;

use app\models\AgencyBonus;
use Yii;

use app\models\Actions;
use app\models\ActionTypes;
use app\models\VideoChats;
use app\models\ChatSession;
use app\models\User;
use app\models\UserVideoSettings;
use app\models\UserVideoTempSession;
use app\models\Video;
use app\models\VideoAccess;
use yii\db\Query;
use yii\web\BadRequestHttpException;

class VideoService
{
    private $_userID;
    private $_userType = null;

    function __construct($userID)
    {
        if ($userID !== null) {
            self::setUser($userID);
        }
    }

    public function setUser($userID)
    {
        $user = User::findOne(['id' => $userID]);
        if (empty($user)) {
            return [
                'success' => false, 
                'message' => 'User_id not found'
            ];
        }

        $this->_userID   = $user->id;
        $this->_userType = $user->user_type;
    }

    public function Start($skin_id, $user_to)
    {
        if ($skin_id === null || !is_numeric($skin_id) || !is_numeric($user_to) || $this->_userId === null || $this->_user_type === null) {
            return ['success' => FALSE, 'message' => 'Missing params'];
        }

        $action_type_id = 4;//VideoChat

        $user = new User($this->_userId);
        //проверяем если мужик
        if ($this->_user_type == 1) {

            $user_balance = $user->payments->GetBalance();

            $billing_model = new Billing();
            $video_price   = $billing_model->getActionPrice($skin_id, $plan_id = 1, $action_type_id, $this->_userId);
            if (!is_numeric($video_price)) {
                return ['success' => FALSE, 'message' => 'Video price error'];
            }

            if ($user_balance < $video_price) {
                return ['success' => FALSE, 'message' => 'Not enough money'];
            }
        }
        $action_model                 = new Actions();
        $action_model->action_type_id = $action_type_id;
        $action_model->skin_id        = $skin_id;
        $action_model->user_from      = $this->_userId;
        $action_model->user_to        = $user_to;

        if (!$action_model->save()) {
            return ['success' => FALSE, 'message' => 'Action save error'];
        }

        $video_model            = new VideoChats();
        $video_model->action_id = $action_model->action_id;
        $video_model->duration  = 60;//60 sec on start
        if (!$video_model->save()) {
            return ['success' => FALSE, 'message' => 'Action video save error'];
        }

        if ($this->_user_type == 1) {
            $user->payments->AddCharges($action_model->action_id, $video_price);//при старте выставляем счет равен стоимости 1 единицы, постом при каждой секунде/минуте будем обновлять сумму выставленного счета
            //оплачиваем премию агенству
            //$agency_bonus            = new AgencyBonus();
            //$agency_minute_price = $agency_bonus->get_minute_price($agency_id,$action_type,$skin_id = 1, $plan_id = 1) 
            //$update_bonus_res = $agency_bonus->updateBonusByWomenId($user_to, $action_model->action_id, $action_type_id, $skin_id, $plan_id = 1);

            $create_bonus_ress = (new AgencyBonus())->createBonus($user_to,$action_model->action_id,$action_type_id,$skin_id,$plan_id=1);

        }

        return ['success' => TRUE, 'action_id' => $action_model->action_id, 'message' => 'ok'];
    }

    public function Tariffing($skin_id, $user_to, $action_id)
    {
        if ($skin_id === null || !is_numeric($skin_id) || !is_numeric($user_to) || !is_numeric($action_id) || $this->_userId === null || $this->_user_type === null) {
            return ['success' => FALSE, 'message' => 'Missing params'];
        }

        $action_type_id = 4;//VideoChat

        //проверяем если мужик
        if ($this->_user_type != 1) {
            return ['success' => FALSE, 'message' => 'Wrong user type'];
        }

        $user = new User($this->_userId);

        $user_balance = $user->payments->GetBalance();

        $billing_model = new Billing();
        $video_price   = $billing_model->getActionPrice($skin_id, $plan_id = 1, $action_type_id, $this->_userId);
        if (!is_numeric($video_price)) {
            return ['success' => FALSE, 'message' => 'Video price error'];
        }

        if ($user_balance < $video_price) {
            return ['success' => FALSE, 'message' => 'Not enough money'];
        }

        $action_model = Actions::findOne(['action_id' => $action_id, 'action_type_id' => $action_type_id, 'user_from' => $this->_userId, 'user_to' => $user_to]);
        if (empty($action_model)) {
            return ['success' => FALSE, 'message' => 'Action not found'];
        }

        //$start_time = $action_model->dt;
        //$diff_time  = time() - strtotime($start_time); //разница в секундах

        $video_model = VideoChats::findOne(['action_id' => $action_model->action_id]);
        if (empty($video_model)) {
            return ['success' => FALSE, 'message' => 'Action_id not found in video chat'];
        }

        //$video_model->duration = $diff_time;
        $video_model->duration += 60; //60 sec
        if (!$video_model->save()) {
            return ['success' => FALSE, 'message' => 'Video duration error save'];
        }

        $charges_id = $user->payments->GetChargesIdByActionId($action_model->action_id);
        if (!$charges_id) {
            return ['success' => FALSE, 'message' => 'Charge_id not found'];
        }

        $charges_amount = $user->payments->getChargesAmount($charges_id);

        if ($charges_amount === FALSE || !is_numeric($charges_amount)) {
            return ['success' => FALSE, 'message' => 'Charges_amount not found'];
        }

        $amount = $charges_amount + $video_price;

        $ress = $user->payments->updateCharges($charges_id, $amount);

        if (!$ress) {
            return ['success' => FALSE, 'message' => 'Charges update error'];
        }
        //update agency bonus
        $update_bonus_res = (new AgencyBonus())->updateBonusByWomenId($user_to, $action_id, $action_type_id, $skin_id, $plan_id = 1);

        return ['success' => TRUE];
    }

    public function getVideoSettings($type=null) {
        $users_video_settings_model = UsersVideoSettings::find()
            ->where(['to_user_id'=>$this->_userId,'status'=>1])
            ->orderBy('id ASC')
            ->one();
        if(empty($users_video_settings_model))
            return null;

        if(empty($type))
            return $users_video_settings_model->attributes;
        if($type == 'message'){
            //$message = $users_video_settings_model->message;
            if(empty($users_video_settings_model->message)){
                $ressult = json_decode($users_video_settings_model->session_description);
            }else{
                $ressult = $users_video_settings_model->message;
                //UsersVideoSettings::deleteAll(['id' => $users_video_settings_model->id]);
            }
            $user_from = $users_video_settings_model->from_user_id;
            $users_video_settings_model->status = 0;
            $users_video_settings_model->save();
            //$users_video_settings_model->message = '';
            //if(!$users_video_settings_model->save())
//                return null;
            return ['user_from'=>$user_from,'data'=>$ressult];
        }

    }

    public function updateAccess($videoID)
    {
        $video = Video::findOne(['id' => $videoID]);

        if (empty($video)) {
            return false;
        }
        $videoAccess = VideoAccess::findOne(['user_id' => $this->_userID, 'video_id' => $video->id]);

        if (empty($videoAccess)) {
            $videoAccess = new VideoAccess();
            $videoAccess->user_id = $this->_userID;
            $videoAccess->video_id = $video->id;
        }                
        $videoAccess->ended_at = date('Y-m-d H:i:s', time() + 20*365*86400);
        
        return $videoAccess->save();        

    }

    public function setVideoSettings($other_user_id,$message=null,$skin_id=1){
        if(!isset($other_user_id) || !is_numeric($other_user_id))
            return false;

        if(substr(trim($message),0,1) == "{") {
            $users_video_settings_model               = new UsersVideoSettings();
            $users_video_settings_model->from_user_id = $this->_userId;
            $users_video_settings_model->to_user_id   = $other_user_id;
            $users_video_settings_model->session_description = $message;
            if(!$users_video_settings_model->save())
                return false;
        }else{
            $users_video_settings_model               = new UsersVideoSettings();
            $users_video_settings_model->from_user_id = $this->_userId;
            $users_video_settings_model->to_user_id   = $other_user_id;
            $users_video_settings_model->message = $message;
            if(!$users_video_settings_model->save())
                return false;
            //добавляем пользователя в сессию видеочата
            if($message == 'join'){
                $users_video_temp_session_model = new UsersVideoTempSession();
                $users_video_temp_session_model->from_user_id = $this->_userId;
                $users_video_temp_session_model->to_user_id   = $other_user_id;
                if(!$users_video_temp_session_model->save())
                    return false;
                //проверяем если нету текущей час сессии то создаем ее
                $chat_model = new Chats($this->_userId);
                $chech_active_chat_session = $chat_model->checkActiveChatSession($skin_id,$other_user_id,TRUE);
                if(!$chech_active_chat_session)
                    return false;

            //если девочка отключила камеру, то выбираем всех пользователей с ее сессии и отправляем им коне видеоессии
            }else if($message == 'bye'){
                $query = UsersVideoTempSession::find()
                    ->where(['to_user_id'=>$this->_userId])
                    ->andWhere(['<>','from_user_id',$other_user_id]);
                $ress = $query->asArray()->all();
                if(!empty($ress)){
                    foreach($ress as $one){
                        $users_video_settings_model               = new UsersVideoSettings();
                        $users_video_settings_model->from_user_id = $this->_userId;
                        $users_video_settings_model->to_user_id   = $one['from_user_id'];
                        $users_video_settings_model->message = 'bye';
                        if(!$users_video_settings_model->save())
                            return false;
                    }
                }
            }
        }

        return true;
    }

}