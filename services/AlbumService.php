<?php
namespace app\services;

use Yii;
use app\models\PhotoLike;
use app\models\Video;
use app\models\User;
use app\models\Album;
use app\models\Photo;
use app\models\UploadForm;
use app\models\Action;
use app\models\UserProfile;

use app\services\Billing;

use yii\web\UploadedFile;
use yii\web\BadRequestHttpException;
use yii\db\Query;
use yii\db\Expression;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

class AlbumService
{
    private $userID = null;
    private $user_type = null;
    private $title;
    private $imageFile;

    function __construct($userID = null)
    {
        if ($userID !== null) {
            self::setUser($userID);
        }
    }

    public function setUser($userID)
    {

        $user = User::findOne(['id' => $userID]);
        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User_id error'
            ];
        }

        $this->userID    = $user->id;
        $this->user_type = $user->user_type;
    }

    public function GetAll()
    {
        if ($this->userID === null) {
            return $result = Album::find()->all();
        }

        $result = Album::find()->where(['id' => $this->userID])->all();

        return $result;
    }

    public function getAllAlbumsIds()
    {
        if ($this->userID === null) {
            return false;
        }

        $result = Album::find()
            ->select('id')
            ->where(['user_id' => $this->userID])
            ->indexBy('id')->all();        

        return $result;
    }

    public function getAlbumsListInfo($public = null, $photoApproveStatus = null)
    {
        if ($this->userID === null) {
            return null;
        }

        $user = Yii::$app->user->identity;

        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, last_name, id')
            ->from([UserProfile::tableName()]);
        $countPhoto = (new Query)
            ->select('COUNT(*) as count_photos, album_id')
            ->from([Photo::tableName()]);
        if ($user->user_type == User::USER_MALE) {            
            $countPhoto->where(['photo.status' => 1]);
        }
        if ($photoApproveStatus !== null) {
            $countPhoto->andWhere(['photo.approve_status' => $photoApproveStatus]);
        }
        $countPhoto->groupBy('photo.album_id');

        $album = Album::find()->select(['album.id', 'album.title', 'photo.small_thumb', 'photo.medium_thumb', 'photo.status','photos_count.count_photos','user_profile.first_name', 'user_profile.last_name'])  
            ->leftJoin(Photo::tableName(), 'album.main_photo_id = photo.id')          
            ->leftJoin(['photos_count' => $countPhoto], 'album.id = photos_count.album_id')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = album.user_id')
            ->where(['album.user_id' => $this->userID]);
        if ($public !== null) {
            $album->andWhere(['album.public' => $public]);
        }
        $result = $album->asArray()->all();       
        $i=0;
        foreach ($result as $album) {
            if ($album['status'] == 0) {
                $result[$i]['small_thumb'] = null;
                $result[$i]['medium_thumb'] = null;                
            }
            $i++;
        }

        return $result;

    }

    public function getPhotos($titles = null, $status = null, $approve_status = null)
    {
        $titleArray = [];
        if ($this->userID === null) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        if ($titles === null) {
            $titles = $this->GetAll();
            if (empty($titles)) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'user albums not found'
                ];
            }
            foreach ($titles as $title) {
                $titleArray[] = $title['title'];
            }
        } else {
            $titleArray = explode(',', $titles);
        }

        $albumIds = [];

        foreach ($titleArray as $title) {
            if (is_numeric($title)) {
                $albumIds[] = $title;
            } else {
                if ($this->getIdByTitle($title)) {
                    $albumIds[] = $this->getIdByTitle($title);
                }
            }
        }


        if ($albumIds === null) return ['result' => null];

        $query = (new Query)
            ->select(['photo.id', 'photo.title', 'photo.status', 'photo.approve_status', 'photo.small_thumb', 'photo.medium_thumb', 'photo.original_image', 'photo.watermark_small', 'photo.watermark_medium', 'photo.watermark_orign', 'photo.premium', 'photo_like.likes','photo_like.dislikes'])
            ->from([Photo::tableName()])
            ->where(['in', 'album_id', $albumIds]);

        if (!empty($status)) {
            $query->andWhere(['status' => $status]);
        }

        if ($approve_status !== null) {
            $query->andWhere(['approve_status' => $approve_status]);
        }

        //для досчета лайков
        $subQuery = (new Query)
            ->addSelect(['sum(likes) as likes', 'sum(dislikes) as dislikes', 'photo_id'])
            ->from([PhotoLike::tableName()])
            ->groupBy('photo_id');
            

        $query->leftJoin(['photo_like' => $subQuery], 'photo_like.photo_id = photo.id');


        $result = $query->all();

        if (empty($result)) {
            return null;
        }

        return $result;
    }

    public function Create($public = false, $title = 'default', $descr = 'standart album')
    {
        $album = Album::findOne(['user_id' => $this->userID, 'title' => $title]);
        if (empty($album)) {
            $album = new Album();  
            $album->user_id = $this->userID;
            $album->public  = (int)$public;
            $album->title   = $title;
            $album->description   = $descr;
            $album->active = Album::STATUS_ACTIVE;        
            if (!$album->save()) {
                $album->validate();
                return [
                    'success' => false, 
                    'errors' => $album->errors,
                    'message' => 'Album save error'
                ];
            }      
        }
        
        return [
            'success' => true, 
            'album_id' => $album->id
        ];
    }

    public function Get($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getAllLikes()
    {
        $photos = $this->getPhotos(null, 1);
        
        $likes = 0;
        if (isset($photos['success']) && $photos['success'] == false) {                  
            return $likes;
        } else {
            if (!empty($photos)) {
                foreach ($photos as $photo) {            
                    $likes += $photo['likes'];
                }
            }            
        }
        return $likes;
    }

    public function getGirlsThatLikedMansPhoto($manID)
    {
        $titleArray = [];
        if ($this->userID === null) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }
        $titles = null;
        if ($titles === null) {
            $titles = $this->GetAll();
            if (empty($titles)) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'user albums not found'
                ];
            }
            foreach ($titles as $title) {
                $titleArray[] = $title['title'];
            }
        } else {
            $titleArray = explode(',', $titles);
        }

        $albumIds = [];

        foreach ($titleArray as $title) {
            if (is_numeric($title)) {
                $albumIds[] = $title;
            } else {
                if ($this->getIdByTitle($title)) {
                    $albumIds[] = $this->getIdByTitle($title);
                }
            }
        }

        if ($albumIds === null) {
            return [
                'success' => true,
                'girls' => []
            ];
        }

        $subQuery = (new Query)
            ->select('first_name, small_thumb, medium_thumb, user.id, user.last_activity')            
            ->from([UserProfile::tableName()])
            ->leftJoin(User::tableName(), 'user_profile.id = user.id')
            ->leftJoin(Photo::tableName(), 'user.avatar_photo_id = photo.id');

        $query = (new Query)
            ->select(['user.first_name', 'user.id', 'user.small_thumb', 'user.medium_thumb', 'user.last_activity'])
            ->from([Photo::tableName()])
            ->leftJoin(PhotoLike::tableName(), 'photo_like.photo_id = photo.id')
            ->leftJoin(['user' => $subQuery], 'user.id = photo_like.user_id')
            ->leftJoin(Album::tableName(), 'album.id = photo.album_id')
            ->where(['in', 'album_id', $albumIds])
            ->andWhere(['photo_like.likes' => 1, 'album.user_id' => $manID]);     

        $result = $query->orderBy(['last_activity' => SORT_DESC])->limit(5)->all();

        if (empty($result)) {
            return [
                'success' => true,
                'girls' => []
            ];
        }

        return [
            'success' => true,
            'girls' => $result
        ];
    }

    public function uploadPhoto($photoTitle, $photoDesc)
    {
        $model = new UploadForm();
        $user = Yii::$app->user->identity;

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstanceByName('imageFile');
            if ($imgName = $model->upload()) {
                $result = $this->Create(false, $this->title);
                
                if (!$result['success']) {
                    return [
                        'success' => false, 
                        'message' => $result['message']
                    ];
                }
                $albumID = $result['album_id'];
                if (!empty($imgName)) {
                    $photo               = new Photo();
                    $photo->album_id     = $albumID;
                    $photo->small_thumb  = $imgName['small'];
                    $photo->medium_thumb = $imgName['normal'];
                    $photo->original_image = $imgName['orign'];
                    $photo->title        = $photoTitle;
                    $photo->description  = $photoDesc;
                    $photo->status = Photo::STATUS_ACTIVE;
                    if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN, User::USER_SUPERADMIN])) {
                        $photo->approve_status = Photo::STATUS_APPROVED;
                    } else if($this->title == 'letters'){//исключение для писем
                        $photo->approve_status = Photo::STATUS_APPROVED;
                    }else{
                        $photo->approve_status = Photo::STATUS_NOT_APPROVED;
                    }
                    
                    if (!$photo->save()) {
                        return [
                            'success' => false, 
                            'message' => 'Error during photo save'
                        ];
                    }
                    $photoService = new PhotoService();
                    $watermarkFilePath = dirname(__FILE__) . '/../web/img/premium.png';
                    $photoService->addWatermark($photo, $watermarkFilePath);
                }

                return [
                    'success' => true, 
                    'message' => 'photo upload', 
                    'photo_id' => $photo->id, 
                    'medium_thumb' => $photo->medium_thumb, 
                    'small_thumb' => $photo->small_thumb,
                    'original_image' => $photo->original_image
                ];
            }
            return [
                'success' => false, 
                'message' => 'Error photo format'
            ];
        }
    }

    public function setStatus($albums, $status)
    {

        $albumIds = $this->titleToId($albums);

        if ($albumIds === null) return ['success' => false];

        Album::updateAll(['status' => $status], 'album_id in (' . implode(',', $albumIds) . ")");

        return ['success' => true, 'message' => 'status update'];
    }


    public function edit($albumID, $public, $title, $description, $active = 1)
    {

        $album = Album::findOne(['user_id' => $this->userID, 'id' => $albumID]);

        if (empty($album))
            return [
                'success' => false, 
                'message' => 'Album not found'
            ];

        if ($album->title == 'default') {
            return [
                'success' => false, 
                'message' => 'Access is denied'
            ];
        }

        if (!empty($title)) {
            $album->title = Html::encode(HtmlPurifier::process($title));
        }
        if (!empty($description)) {
            $album->description = Html::encode(HtmlPurifier::process($description));
        }

        if (isset($active) && $active) {
            $album->active = Album::STATUS_ACTIVE;
        } else {
            $album->active = Album::STATUS_DELETED;
        }

        if (isset($public) && $public) {
            $album->public = 1;
        } else {
            $album->public = 0;
        }
                        
        if (!$album->save()) {
            return [
                'success' => false, 
                'message' => $album->errors
            ];
        }

        return [
            'success' => true, 
            'message' => 'album was updated'
        ];
    }

    public function delete($title)
    {
        $album_model = Album::findOne(['user_id' => $this->userId, 'title' => $title]);
        if (empty($album_model)) {
            return ['success' => false, 'message' => 'Album not found'];
        }
        $album_model->delete();

        return ['success' => true, 'message' => 'album delete'];
    }

    public function deleteAllByUserId()
    {
        $result = Album::deleteAll(['user_id' => $this->userID]);

        return $result;
    }

    public function getIdByTitle($title)
    {
        $album = Album::findOne(['user_id' => $this->userID, 'title' => $title]);

        if (!empty($album)) {
            return $album->id;
        } else {
            return false;
        }

    }

    public function getAlbumInfo($title)
    {
        $titleArray = explode(',', $title);

        if (!is_numeric($titleArray[0])) {
            $id = $this->getIdByTitle($titleArray[0]);
        } else {
            $id = $titleArray[0];
        }

        //подзапрос на имя пользователя
        $subQuery = UserProfile::find()
            ->select('first_name, last_name, id');          
        $album = Album::find()
            ->select(['album.*','user_profile.first_name','user_profile.last_name'])            
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = album.user_id')
            ->where(['album.id' => $id]);
        $result = $album->asArray()->one();

        if (empty($result)) {
            return false;
        }

        return $result;

    }

    public function titleToId($albums)
    {
        $title_array = explode(',', $albums);
        $albumIds    = null;
        foreach ($title_array as $one) {
            if (is_numeric($one)) {
                $albumIds[] = $one;
            } else {
                if ($this->getIdByTitle($one)) {
                    $albumIds[] = $this->getIdByTitle($one);
                }
            }
        }

        return $albumIds;
    }

    public function setAlbumCover($photoID, $albumID)
    {
        $album = Album::findOne(['user_id' => $this->userID, 'id' => $albumID]);
        if (empty($album)) {
            return [
                'success' => false, 
                'message' => 'Album not found'
            ];
        }
        $album->main_photo_id = $photoID;
        if (!$album->save()) {
            return [
                'success' => false, 
                'message' => 'Album save error'
            ];
        }

        return [
            'success' => true, 
            'message' => 'This photo was set as album cover'
        ];
    }

    public function getUserVideoList($approved = null)
    {
        if ($this->userID === null) {
            return null;
        }

        $video = Video::find()
            ->select('id, poster, description')
            ->where(['user_id' => $this->userID]);
        if ($approved !== null) {
            $video
                ->andWhere(['status'=>(int)$approved])
                ->limit(9);
        }
        $result = $video
            ->asArray()
            ->all();

        return $result;

    }

    public function getOtherVideo($skin_id,$video_id)
    {
        if ($this->userId === null || !is_numeric($video_id)) {
            return ['success' => false, 'code' => 1, 'message' => 'user error'];
        }

        $video_model = Videos::findOne(['video_id'=>$video_id,'status'=>1]);

        if(empty($video_model)){
            return ['success' => false, 'code' => 1, 'message' => 'video not found'];
        }

        //проверяем если мужик
        if ($this->user_type == 1) {
            $user = new User($this->userId);
            $user_balance = $user->payments->GetBalance();

            $billing_model = new Billing();
            $watch_video_price = $billing_model->getActionPrice($skin_id, $plan_id = 1, 6, $this->userId);

            if (!is_numeric($watch_video_price)) {
                return ['success' => false, 'code' => 1, 'message' => 'Watch user video price error'];
            }

            if ($user_balance < $watch_video_price) {
                return ['success' => false, 'message' => 'Not enough money'];
            }
            //add new action
            $action_model                 = new Actions();
            $action_model->action_type_id = 6;
            $action_model->skin_id        = $skin_id;
            $action_model->user_from      = $this->userId;
            $action_model->user_to        = $video_model->user_id;

            if (!$action_model->save()) {
                return ['success' => false, 'message' => 'Action not save'];
            }

            //с мужиков взымаем плату
            $user->payments->AddCharges($action_model->action_id, $watch_video_price);

        }

        return ['success' => true, 'video_data' => $video_model->attributes];

    }
}