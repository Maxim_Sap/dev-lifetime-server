<?php
namespace app\services;

use Yii;
use app\models\Message as MessageModel;
use app\models\User;
use app\models\UserProfile;
use app\models\Photo as PhotoModel;
use yii\db\Query;

class Message
{
    private $userId    = null;
    private $userType = null;

    function __construct($userID = null)
    {
        if ($userID !== null) {
            self::setUser($userID);
        }
    }

    public function setUser($userID)
    {
        $user = User::findOne(['id' => $userID]);
        if (empty($user)) {
            return [
                'success' => FALSE, 
                'message' => 'Missing params'
            ];
        }

        $this->userId = $userID;
        $this->userType = $user->user_type;
    }

    public function Send($to_user_id, $caption, $desc, $last_message_id = null)
    {
        if ((empty($to_user_id) && empty($this->userId)) || $caption === null || $desc === null) {
            return ['success' => FALSE, 'message' => 'Missing params'];
        }

        $other_user_type = Users::findOne(['user_id'=>$to_user_id]);
        if (empty($other_user_type)) {
            return ['success' => FALSE, 'message' => 'User not found'];
        }

        $message               = new MessageModel();
        $message->from_user_id = $this->userId;
        $message->to_user_id   = $to_user_id;
        $message->caption      = $caption;
        $message->description  = $desc;
        if (!$message->save()) {
            return ['success' => FALSE, 'message' => 'Error save'];
        }

        if(!empty($last_message_id)){
            //ставим отвеченное предыдущему письму
            $message_last_model = MessageModel::findOne($last_message_id);
            if (empty($message_last_model)) {
                return ['success' => FALSE, 'message' => 'Message_last_id not found'];
            }
            $message_last_model->answer_status = 1;
            if (!$message_last_model->save()) {
                return ['success' => FALSE, 'message' => 'Error save message status'];
            }
        }

        return ['success' => TRUE, 'message_id' => $message->id, 'message' => 'System message send'];

    }

    /**
     * @param $id
     * $status
     * status = 1 активно
     * status = 0 удалено/не отображать
     *
     * @return int
     * @throws NotAcceptableHttpException
     */
    public function dell($id, $status = 0)
    {
        $delete_status_title = null;

        if ($this->user_type == 1 || $this->user_type == 2) $delete_status_title = 'user_delete_status'; // for man & women

        if (empty($delete_status_title)) {
            return ['success' => FALSE, 'message' => 'Error user type'];
        }

        if (!is_array($id) && is_numeric($id)) {
            $message = MessageModel::find()
                ->where(['id' => $id])
                ->andWhere(['or','from_user_id ='.$this->userId,'to_user_id = '.$this->userId])
                ->one();

            if (empty($message)) {
                return ['success' => FALSE, 'message' => 'Message not found'];
            }


            $message->$delete_status_title = $status;

            if (!$message->save()) {
                return ['success' => FALSE, 'message' => 'Message error save'];
            }
        }elseif(is_array($id) && !empty($id)){
            $message = (new Query())
                ->from([MessageModel::tableName()]);
            $message->where([
                'and', ['in', 'id', $id],
                ['or', "from_user_id = $this->userId", "to_user_id= $this->userId"],
            ]);
            if ($message->count() != count($id)) {
                return ['success' => FALSE, 'message' => 'One of message not found'];
            }

            $ress = MessageModel::updateAll([$delete_status_title => $status], ['in', 'id', $id]);
            if (!$ress) {
                return ['success' => FALSE, 'message' => 'Message update error'];
            }
        }

        return ['success' => TRUE];
    }

    public function read($id)
    {
        if (!is_numeric($id)) {
            return ['success' => FALSE, 'message' => 'Eroor message_id'];
        }

        //если не админ
        if($this->user_type != 6){
            $message = (new Query())
                ->from([MessageModel::tableName()])
                ->where('id ='. $id.' AND date_read IS NULL AND to_user_id = '.$this->userId)
                ->one();
        }else{

            $admin_ids_model = Users::find()->where(['user_types'=>6])->all();
            $admin_ids = [];
            if(!empty($admin_ids_model)){
                foreach($admin_ids_model as $one){
                    $admin_ids[] = $one->user_id;
                }
            }

            $message = (new Query())
                ->from([MessageModel::tableName()])
                ->where('id ='. $id.' AND date_read IS NULL')
                ->andWhere(['in','to_user_id',$admin_ids])
                ->one();
        }

        if ($message) {
            $message_model = MessageModel::findOne(['id' => $id]);

            $message_model->date_read = date('Y-m-d H:i:s', time());
            if (!$message_model->save()) {
                return ['success' => FALSE, 'message' => 'Message read error save'];
            }
        }


        return ['success' => TRUE];
    }

    public function get($message_id, $limit = null, $offset = null)
    {
        if ($this->userId === null) {
            return ['success' => FALSE, 'message' => 'User_id is null'];
        }

        $query    = new Query();
        //$subQuery = (new Query)->select('COUNT(*) as countletters_in_thema, theme_id')->from([MessageModel::tableName() . ' mes'])->groupBy('mes.theme_id');

        $message = $query->addSelect(['mes.*', 'photos.thumb_normal as another_user_avatar', 'user_pers.first_name as another_user_name', 'users.user_id as another_user_id'])
            ->from([MessageModel::tableName() . ' mes'])
            //->leftJoin(Messagetheme::tableName() . ' mes_th', 'mes.theme_id = mes_th.id')
            ->leftJoin(Users::tableName() . ' users', 'users.user_id = mes.from_user_id')
            ->leftJoin(UserPersonal::tableName() . ' user_pers', 'user_pers.user_id = mes.from_user_id')
            ->leftJoin(PhotoModel::tableName() . ' photos', 'users.avatar_photo_id = photos.photo_id')
          //  ->leftJoin(['messagecounts' => $subQuery], 'messagecounts.theme_id = mes.theme_id')
            ->where(['mes.from_user_id' => $this->userId])
            ->orWhere(['mes.to_user_id' => $this->userId]);

        if ($this->user_type == 1 || $this->user_type == 2) {
            $message->andWhere(['mes.user_delete_status' => 1]);
        }
        if ($message_id !== null && is_numeric($message_id)) {
            $message->andWhere(['mes.id' => $message_id]);
        }

        $count_message = $message->count();

        if ($limit !== null && is_numeric($limit)) {
            $message->limit($limit);
        }
        if ($offset !== null && is_numeric($offset)) {
            $message->offset($offset);
        }

        $ress = $message->orderBy('date_create DESC')->all();

        return ['success' => TRUE, 'message_mass' => $ress, 'count_message' => $count_message];
    }

    public function getToAdmin($message_id = null, $limit = null, $offset = null,$params)
    {
        if ($this->userId === null) {
            return ['success' => FALSE, 'message' => 'User_id is null'];
        }

        $subQuery = (new Query)
            ->select('first_name, last_name, user_id')
            ->from([UserPersonal::tableName() . ' user_pers']);
        $subQuery2 = (new Query)
            ->select('thumb_normal, photo_id')
            ->from([PhotoModel::tableName() . ' photos']);

        $type = isset($params['type']) ? $params['type'] : 'inbox';
        $answered = isset($params['answered']) ? $params['answered'] : null;
        $read = isset($params['read']) ? $params['read'] : null;
        $user_id_from = isset($params['user_id_from']) && is_numeric($params['user_id_from']) ? (int)$params['user_id_from'] : null;
        $user_id_to = isset($params['user_id_to']) && is_numeric($params['user_id_to']) ? (int)$params['user_id_to'] : null;
        $date_from = isset($params['date_from']) && strtotime($params['date_from']) ? $params['date_from'] : null;
        $date_to = isset($params['date_to']) && strtotime($params['date_to']) ? $params['date_to'] : null;

        if($type == 'inbox'){
            $other_user_name = 'from_user_id';
            $user_name = 'to_user_id';
        }else{
            $other_user_name = 'to_user_id';
            $user_name = 'from_user_id';
        }

        $admin_ids_model = Users::find()->where(['user_types'=>6])->all();
        $admin_ids = [];
        if(!empty($admin_ids_model)){
            foreach($admin_ids_model as $one){
                $admin_ids[] = $one->user_id;
            }
        }

        $query    = new Query();
        $message = $query->addSelect(['mes.*', 'user_pers.first_name','user_pers.last_name','photos.thumb_normal as another_user_avatar'])
            ->from([MessageModel::tableName() . ' mes'])
            ->leftJoin(['user_pers' => $subQuery], 'user_pers.user_id = mes.'.$other_user_name)
            ->leftJoin(Users::tableName() . ' users', 'users.user_id = mes.'.$other_user_name)
            ->leftJoin(['photos' => $subQuery2], 'users.avatar_photo_id = photos.photo_id');

        if ($message_id !== null && is_numeric($message_id)) {
            $message->andWhere(['mes.id' => $message_id]);
        }else{
            //если нету message_id то все письма которые адресованы админам
            $query->where(['in','mes.'.$user_name, $admin_ids]);
        }


        if($answered !== null && $answered != "") {
            $message->andWhere(['mes.answer_status' => $answered]);
        }
        if($read !== null && $read != '') {
            if ($read == 0) {
                $message->andwhere('mes.date_read IS NULL');
            } else {
                $message->andwhere('mes.date_read IS NOT NULL');
            }
        }
        if ($date_from) {
            $message->andwhere(['>=', 'mes.date_create', $date_from]);
        }
        if ($date_to) {
            $message->andwhere(['<=', 'mes.date_create', $date_to]);
        }
        if ($user_id_from) {
            $message->andwhere(['mes.from_user_id' => $user_id_from]);
        }
        if ($user_id_to) {
            $message->andwhere(['mes.to_user_id' => $user_id_to]);
        }

        $count_message = $message->count();

        if ($limit !== null && is_numeric($limit)) {
            $message->limit($limit);
        }
        if ($offset !== null && is_numeric($offset)) {
            $message->offset($offset);
        }

        $ress = $message->orderBy('date_create DESC')->all();

        return ['success' => TRUE, 'message_mass' => $ress, 'count_message' => $count_message];
    }

    public function getPrevNextMessageIds($message_id)
    {

        if (!is_numeric($message_id)) {
            return ['success' => FALSE, 'message' => 'Message_id error'];
        }

        $prev_id = null;
        $next_id = null;

        $message = (new Query())
            ->from([MessageModel::tableName() . " mess"])
            ->addSelect(['mess.id'])
            ->where(['mess.to_user_id' => $this->userId, 'mess.user_delete_status' => 1])
            ->all();

        if (empty($message)) {
            return ['prev' => null, 'next' => null];
        }

        for ($i = 0; $i < count($message); $i++) {
            if ($message[$i]['id'] == $message_id) {
                $prev_id = isset($message[$i - 1]['id']) ? $message[$i - 1]['id'] : null;
                $next_id = isset($message[$i + 1]['id']) ? $message[$i + 1]['id'] : null;
            }
        }

        return ['prev' => $prev_id, 'next' => $next_id];

    }

    public function getNewMessage()
    {
        $deleteStatus = ($this->userType == User::USER_MALE || $this->userType == User::USER_FEMALE) ? 'was_deleted_by_user' : false; // for man & women
        if (!$deleteStatus) {
            return [
                'count' => 0,
                'users' => false
            ];
        }
        $messages = (new Query())
            ->from([MessageModel::tableName()])
            ->where(['message_receiver' => $this->userId, 'readed_at' => null, $deleteStatus => MessageModel::WAS_DELETED]);
        $count = $messages->count();

        //подзапрос на имя пользователя
        $subQuery = (new Query)
            ->select('first_name, id')
            ->from([UserProfile::tableName()]);
        //подзапрос на фото пользователя
        $subQuery2 = (new Query)
            ->select('user.id, photo.small_thumb')
            ->from([User::tableName()])
            ->leftJoin(PhotoModel::tableName(), 'user.avatar_photo_id = photo.id');

        $lastName = $messages
            ->addSelect('user_profile.first_name, photo.small_thumb')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = message_creator')
            ->leftJoin(['photo' => $subQuery2], 'photo.id = message_creator')
            ->orderBy('message.id DESC')
            ->one();

        return [
            'count' => $count,
            'users' => $lastName
        ];
    }

}