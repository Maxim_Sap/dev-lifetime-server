<?php

return [
    'adminEmail'                     => 'contact@web-synthesis.ru',
    'supportEmail'                   => 'support@test.com',
    'clientUrl'                      => 'https://brachka.web-synthesis.ru',
    'baseUrl'                      => 'https://api.brachka.web-synthesis.ru',
    'user.passwordResetTokenExpire'  => 3 * 60 * 60,
    'skin_url'                       => 'https://russbride.com',
    'google_auth'                    => [
        'client_id'         => '1043569066182-5vdmmbkedm85e7vrgj4f0s479h4pa6kb.apps.googleusercontent.com',
        'client_secret'     => 'QVaei3oS5M5MlsflgdOr-CFE',
        'redirect_uri'      => 'https://brachka.web-synthesis.ru/reg-social/google',
        'url'               => 'https://accounts.google.com/o/oauth2/auth',
        'url_token'         => 'https://accounts.google.com/o/oauth2/token',
        'scope'             => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile',
        'get_user_info_url' => 'https://www.googleapis.com/oauth2/v1/userinfo',
    ],
    'stripe'                         => [
        'api_key'      => 'sk_test_BQokikJOvBiI2HlWgH4olfQ2',
        'publish_key'  => 'pk_test_6pRNASCoBOKtIshFeQd4XMUh',
        'redirect_url' => '/user/add-funds-by-stripe',
    ],
    'girls_top'                      => [3, 4, 6, 11], //girls ID
    'men_top'                        => [2, 3, 4, 5], // men ID    
    'token_time'                     => 3 * 60 * 60, // 3 hour
    'remember_token_time'            => 7 * 24 * 60 * 60, // 7 days
    'penalty_gift_time'              => 5 * 24 * 60 * 60, // 5 days
    'penalty_gift_time_max'          => 15 * 24 * 60 * 60, // 15 days    
    'penalty_letter_time'            => 3 * 24 * 60 * 60, // 3 days
    'penalty_letter_time_max'        => 7 * 24 * 60 * 60, // 7 days   
    'origin' => ['https://brachka.web-synthesis.ru','https://www.brachka.web-synthesis.ru'],
    'admin_permissions' => [
        'users_access' => [
                'description' => 'Доступ к пользователям',
                'value' => 0
        ],
        'gift_access' => [
                'description' => 'Доступ к подаркам',
                'value' => 0
        ],
        'tasks_access' => [
                'description' => 'Доступ к задачам',
                'value' => 0
        ],
        'letter_access' => [
                'description' => 'Доступ к письмам',
                'value' => 0
        ],        
        'finance_access' => [
                'description' => 'Доступ к финансам',
                'value' => 0
        ]
    ],
    'siteadmin_permissions' => [
        'users_access' => [
                'description' => 'Доступ к пользователям',
                'value' => 0
        ],
        'gift_access' => [
                'description' => 'Доступ к подаркам',
                'value' => 0
        ],
        'tasks_access' => [
                'description' => 'Доступ к задачам',
                'value' => 0
        ],
        'letter_access' => [
                'description' => 'Доступ к письмам',
                'value' => 0
        ],        
        'finance_access' => [
                'description' => 'Доступ к финансам',
                'value' => 0
        ],
        'blog_access' => [
                'description' => 'Доступ к блогу',
                'value' => 0
        ],  
        'staticpage_access' => [
                'description' => 'Доступ к статическим страницам',
                'value' => 0
        ],      
    ]
];
