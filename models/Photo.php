<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "photo".
 *
 * @property integer $id
 * @property integer $album_id
 * @property string $small_thumb
 * @property string $medium_thumb
 * @property string $original_image
 * @property string $title
 * @property string $description
 * @property integer $status
 * @property integer $approve_status
 *
 * @property Album $album
 * @property PhotoLike[] $photoLikes
 */
class Photo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    const STATUS_NOT_APPROVED = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_DECLINED = 3;
    const STATUS_APPROVED = 4;

    public static function tableName()
    {
        return 'photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['album_id', 'status', 'approve_status'], 'integer'],
            [['approve_status'], 'required'],
            [['small_thumb', 'medium_thumb', 'original_image', 'title', 'description'], 'string', 'max' => 255],
            [['album_id'], 'exist', 'skipOnError' => true, 'targetClass' => Album::className(), 'targetAttribute' => ['album_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'album_id' => 'Album ID',
            'small_thumb' => 'Small Thumb',
            'medium_thumb' => 'Medium Thumb',
            'original_image' => 'Original Image',
            'title' => 'Title',
            'description' => 'Description',
            'status' => 'Status',
            'approve_status' => 'Approve Status',
        ];
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {     
            if (!$insert) {                
                $this->title = Html::encode(strip_tags(HtmlPurifier::process($this->title)));
                $this->description = Html::encode(strip_tags(HtmlPurifier::process($this->description)));                
            } else {
                $this->title = Html::encode(strip_tags(HtmlPurifier::process($this->title)));
                $this->description = Html::encode(strip_tags(HtmlPurifier::process($this->description)));
            }            
            return true;
        }
        return false;
    }

    public function fields()
    {
        $fieldsList = [
            // field name is the same as the attribute name
            'id',
            'album_id',            
            'small_thumb',
            'medium_thumb',
            'original_image',
            'title' => function($model) {
                return Html::decode($model->title);
            },
            'description' => function($model) {
                return Html::decode($model->description);
            },      
            'status',
            'approve_status'          
        ];
        $user = \Yii::$app->user->identity;
        if (!empty($user) && in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN, User::USER_SUPERADMIN])) {           
        }
        return $fieldsList;     
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbum()
    {
        return $this->hasOne(Album::className(), ['id' => 'album_id']);
    }

    public function getPhotoAccesses()
    {
        return $this->hasMany(PhotoAccess::className(), ['photo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotoLikes()
    {
        return $this->hasMany(PhotoLike::className(), ['photo_id' => 'id']);
    }
}
