<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pricelist_agency".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $skin_id
 * @property integer $plan_id
 * @property integer $action_type_id
 * @property string $start_date
 * @property string $stop_date
 * @property string $price
 * @property integer $important
 *
 * @property ActionType $actionType
 * @property Plan $plan
 * @property Skin $skin
 * @property User $user
 */
class PricelistAgency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pricelist_agency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'skin_id', 'plan_id', 'action_type_id', 'important'], 'integer'],
            [['skin_id', 'plan_id', 'action_type_id', 'price'], 'required'],
            [['start_date', 'stop_date'], 'safe'],
            [['price'], 'number'],
            [['action_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActionType::className(), 'targetAttribute' => ['action_type_id' => 'id']],
            [['plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Plan::className(), 'targetAttribute' => ['plan_id' => 'id']],
            [['skin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Skin::className(), 'targetAttribute' => ['skin_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'skin_id' => 'Skin ID',
            'plan_id' => 'Plan ID',
            'action_type_id' => 'Action Type ID',
            'start_date' => 'Start Date',
            'stop_date' => 'Stop Date',
            'price' => 'Price',
            'important' => 'Important',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionType()
    {
        return $this->hasOne(ActionType::className(), ['id' => 'action_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(Plan::className(), ['id' => 'plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkin()
    {
        return $this->hasOne(Skin::className(), ['id' => 'skin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
