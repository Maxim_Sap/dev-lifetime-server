<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "action".
 *
 * @property integer $id
 * @property integer $action_type_id
 * @property integer $skin_id
 * @property integer $action_creator
 * @property integer $action_receiver
 * @property string $created_at
 *
 * @property User $actionCreator
 * @property ActionType $actionType
 * @property Skin $skin
 * @property AgencyRevard[] $agencyRevards
 * @property Chat[] $chats
 * @property ChatSession[] $chatSessions
 * @property Letter[] $letters
 * @property MissingChat[] $missingChats
 * @property Order[] $orders
 * @property VideoChat[] $videoChats
 */
class Action extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_type_id', 'skin_id', 'action_creator', 'action_receiver'], 'required'],
            [['action_type_id', 'skin_id', 'action_creator', 'action_receiver'], 'integer'],
            [['created_at'], 'safe'],
            [['action_creator'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['action_creator' => 'id']],
            [['action_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActionType::className(), 'targetAttribute' => ['action_type_id' => 'id']],
            [['skin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Skin::className(), 'targetAttribute' => ['skin_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_type_id' => 'Action Type ID',
            'skin_id' => 'Skin ID',
            'action_creator' => 'Action Creator',
            'action_receiver' => 'Action Receiver',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'action_creator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionType()
    {
        return $this->hasOne(ActionType::className(), ['id' => 'action_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkin()
    {
        return $this->hasOne(Skin::className(), ['id' => 'skin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgencyRevards()
    {
        return $this->hasMany(AgencyRevard::className(), ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChats()
    {
        return $this->hasMany(Chat::className(), ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChatSessions()
    {
        return $this->hasMany(ChatSession::className(), ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLetters()
    {
        return $this->hasMany(Letter::className(), ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMissingChats()
    {
        return $this->hasMany(MissingChat::className(), ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideoChats()
    {
        return $this->hasMany(VideoChat::className(), ['action_id' => 'id']);
    }
}
