<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogTable;

/**
 * LogSearch represents the model behind the search form about `app\models\LogTable`.
 */
class LogSearch extends LogTable
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['user_type.title']);
    }

    public function rules()
    {
        return [
            [['id', 'user_id', 'user_type'], 'integer'],
            [['description', 'ip', 'created_at', 'user_type.title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($agencyUsersIds, $otherUserID, $params, $page, $limit)
    {
        $query = LogTable::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'page' => $page,
                'pageSize' => $limit,                
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC, 
                ]
            ],        
        ]);

        $query->select(['log_table.id', 'user_id', 'user_type', 'user_type.title', 'description', 'created_at'])->joinWith('userType');

/*        $dataProvider->sort->attributes['user_type.title'] = [
            'asc' => ['user_type.title' => SORT_ASC],
            'desc' => ['user_type.title' => SORT_DESC],
        ];*/

        $fields = [
            self::formName() => [
                'user_id' => $otherUserID,
                'user_type' => (!empty($params['userType'])) ? $params['userType'] : null,
            ]
        ];

        $this->load($fields);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'user_type' => $this->user_type,
        ]);

        if (!empty($agencyUsersIds) && empty($otherUserID)) {
            $query->andFilterWhere(['in','user_id',$agencyUsersIds]);
        }

        if (!empty($params['dateFrom'])) {
            $query->andFilterWhere(['>=','created_at', $params['dateFrom']]);
        }

        if (!empty($params['dateTo'])) {
            $query->andFilterWhere(['<=','created_at',$params['dateTo']]);
        }
       
        return $dataProvider;
    }
}