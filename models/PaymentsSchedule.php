<?php

namespace app\models;

use app\services\BillingService;
use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "payments_schedule".
 *
 * @property integer $agency_id
 * @property string  $period_date
 * @property integer $status
 * @property integer $earnings
 * @property integer $penalty
 * @property integer $paid_value
 * @property integer $transfer
 * @property string  $comments
 * @property string  $date_of_pay
 *
 */
class PaymentsSchedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    const STATUS_CURRENT = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_CLOSED = 3;

    public static function tableName()
    {
        return 'payments_schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agency_id', 'status'], 'required'],
            [['agency_id', 'status'], 'integer'],
            [['earnings', 'penalty', 'paid_value', 'transfer'], 'double'],
            [['comments'], 'string'],
            [['payment_day', 'period_date'], 'safe'],
            [['comments'], function ($attribute) {
                $this->$attribute = Html::encode(strip_tags(HtmlPurifier::process($this->$attribute)));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'agency_id'   => 'agency_id',
            'period_date' => 'period_date',
            'status'      => 'status', //1-current, 2-in_process, 3-closed
            'earnings'    => 'earnings',
            'penalty'     => 'penalty',
            'paid_value'  => 'paid_value',
            'transfer'    => 'transfer',
            'comments'    => 'comments',
            'payment_day' => 'payment_day',
        ];
    }

    public static function getPeriod($agencyID, $dateFrom = null, $dateTo = null)
    {
        $model = self::find()
            ->where(['agency_id' => $agencyID]);
        if ($dateFrom != null) {
            $model->andWhere(['>=', 'period_date', $dateFrom]);
        }
        if ($dateTo != null) {
            $model->andWhere(['<=', 'period_date', $dateTo]);
        }

        $model->orderBy('period_date DESC, id DESC');
        $result = $model
            ->asArray()
            ->all();

        return $result;
    }

    public static function getByOrderId($orderID)
    {
        $model = self::find()
            ->select('payments_schedule.*, user_profile.first_name, user_profile.last_name')
            ->leftJoin(User::tableName(), 'user.agency_id = payments_schedule.agency_id')
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = user.id')
            ->where(['payments_schedule.id' => $orderID, 'payments_schedule.status' => 2]);
            //->andWhere(['user_type' => User::USER_AGENCY]);
            

        $result = $model
            ->asArray()
            ->one();

        return $result;
    }

    public static function updateCurrMonth($agencyID, $earnings, $penalty)
    {
        $firstDayOfMonth = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
        $model         = self::findOne(['agency_id' => $agencyID, 'period_date' => $firstDayOfMonth]);
        if (empty($model)) {
            $model              = new PaymentsSchedule();
            $model->agency_id   = $agencyID;
            $model->period_date = $firstDayOfMonth;
            $model->transfer    = 0;
            $model->status      = 1;
            $model->paid_value  = 0;
            //update last month 
            self::checkLastMonth($agencyID);
        }
        $model->earnings = $earnings;
        $model->penalty  = $penalty;

        if (!$model->save()) {
            return $model->errors;
        }

        return 1;
    }

    public static function checkLastMonth($agencyID)
    {
        //обновляем статус прошедшего месяца если он еще текущий и полную сумму за весь месяц
        $firstDayOfPrevMonth = date('Y-m-d', mktime(0, 0, 0, date('m') - 1, 1, date('Y')));
        $model = self::findOne(['agency_id' => $agencyID, 'period_date' => $firstDayOfPrevMonth, 'status' => 1]);
        if (!empty($model)) {
            $billingService  = new BillingService();
            $dateFrom      = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m') - 1, 1, date('Y')));
            $dateTo        = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m'), 1, date('Y')));
            $agencyBalance = $billingService->getAgencyTotalBalance($agencyID, $dateFrom, $dateTo);
            $earnings       = (isset($agencyBalance['bonus'][0]['total_amount'])) ? $agencyBalance['bonus'][0]['total_amount'] : 0;
            $penalty        = (isset($agencyBalance['agency_total_penalty'][0]['total_penalty_value'])) ? $agencyBalance['agency_total_penalty'][0]['total_penalty_value'] : 0;

            $model->earnings = $earnings;
            $model->penalty  = $penalty;
            $model->status   = PaymentsSchedule::STATUS_IN_PROGRESS;
            $newTransfer    = self::checkTransfer($agencyID);
            $model->transfer = $newTransfer;
            if (!$model->save()) {
                return $model->errors;
            }
        }

        return 1;
    }

    public static function checkTransfer($agencyID)
    {
        //проверяем не закрыт ли позапрошлый месяц, если нет, то переносим сумму в прошлый месяц
        $firstDayOfBeforePrevMonth = date('Y-m-d', mktime(0, 0, 0, date('m') - 2, 1, date('Y')));
        $model = self::findOne([
                            'agency_id' => $agencyID, 
                            'period_date' => $firstDayOfBeforePrevMonth, 
                            'status' => 2
                        ]);
        $newTransfer = 0;
        if (!empty($model)) {
            $newTransfer    = $model->transfer + $model->earnings - $model->penalty;
            $model->comments = 'выплата перенесена';// closed
            $model->status = PaymentsSchedule::STATUS_CLOSED;
            $model->save();
        }
        return $newTransfer;
    }

    public static function addPayments($agencyID, $orderID, $paidValue, $comments)
    {
        $model = self::findOne(['id' => $orderID, 'agency_id' => $agencyID, 'status' => PaymentsSchedule::STATUS_IN_PROGRESS]);
        if (empty($model)) {
            return [
                'success' => FALSE, 
                'code' => 2, 
                'message' => 'Order not found'
            ];
        }

        $model->status      = PaymentsSchedule::STATUS_CLOSED;
        $model->comments    = $comments;
        $model->paid_value  = $paidValue;
        $model->payment_day = date('Y-m-d H:i:s', time());
        if (!$model->save()) {
            return [
                'success' => FALSE, 
                'code' => 2, 
                'message' => 'Order not save'
            ];
        }


        return [
            'success' => TRUE, 
            'code' => 1, 
            'message' => 'payments added'
        ];
    }

    public static function transferPayments($orderID)
    {
        $model = self::findOne(['id' => $orderID, 'status' => PaymentsSchedule::STATUS_IN_PROGRESS]);
        if (empty($model)) {
            return [
                'success' => FALSE, 
                'code' => 2, 
                'message' => 'Order not found'
            ];
        }

        $transferValue  = $model->transfer + $model->earnings - $model->penalty;
        $agencyID       = $model->agency_id;
        $model->comments = 'выплата перенесена';// closed
        $model->status   = PaymentsSchedule::STATUS_CLOSED;// closed

        if (!$model->save()) {
            return [
                'success' => FALSE, 
                'code' => 2, 
                'message' => 'Order not save'
            ];
        }

        $model = self::findOne(['agency_id' => $agencyID, 'status' => 1]);
        if (empty($model)) {
            return [
                'success' => FALSE, 
                'code' => 2, 
                'message' => 'current order not found'
            ];
        }

        $model->transfer = $transferValue;

        if (!$model->save()) {
            return [
                'success' => FALSE, 
                'code' => 2, 
                'message' => 'current order not save'
            ];
        }

        return [
            'success' => TRUE, 
            'code' => 1, 
            'message' => 'payments transferred'
        ];
    }

}
