<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_wink".
 *
 * @property integer $id
 * @property integer $user_creator
 * @property integer $user_receiver
 * @property string $created_at
 * @property string $viewed_at
 *
 * @property User $userCreator
 * @property User $userReceiver
 */
class UserWink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_wink';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_creator', 'user_receiver'], 'required'],
            [['user_creator', 'user_receiver'], 'integer'],
            [['created_at', 'viewed_at'], 'safe'],
            [['user_creator'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_creator' => 'id']],
            [['user_receiver'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_receiver' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ]            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_creator' => 'User Creator',
            'user_receiver' => 'User Receiver',
            'created_at' => 'Created At',
            'viewed_at' => 'Viewed At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'user_creator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserReceiver()
    {
        return $this->hasOne(User::className(), ['id' => 'user_receiver']);
    }
}
