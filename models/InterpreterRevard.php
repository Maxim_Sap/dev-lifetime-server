<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "interpreter_revard".
 *
 * @property integer $id
 * @property integer $interpreter_type_id
 * @property integer $user_id
 * @property integer $deposit
 * @property string $amount
 * @property string $fee
 *
 * @property InterpreterType $interpreterType
 * @property User $user
 */
class InterpreterRevard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'interpreter_revard';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['interpreter_type_id', 'user_id', 'deposit'], 'integer'],
            [['amount', 'fee'], 'number'],
            [['interpreter_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => InterpreterType::className(), 'targetAttribute' => ['interpreter_type_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'interpreter_type_id' => 'Interpreter Type ID',
            'user_id' => 'User ID',
            'deposit' => 'Deposit',
            'amount' => 'Amount',
            'fee' => 'Fee',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterpreterType()
    {
        return $this->hasOne(InterpreterType::className(), ['id' => 'interpreter_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
