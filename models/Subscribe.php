<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subscribe".
 *
 * @property integer $id
 * @property integer $skin_id
 * @property string  $email
 * @property string  $created_at
 */
class Subscribe extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscribe';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['skin_id'], 'integer'],
            [['created_at'], 'safe'],
            [['email'], 'string', 'max' => 255],
            ['email', 'trim'],            
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'skin_id'    => 'Skin ID',
            'email'      => 'Email',
            'created_at' => 'Created At',
        ];
    }

    public function addUser($email, $skinID)
    {
        $model = self::findOne(['email' => $email]);
        if (empty($model)) {
            $model          = new Subscribe();
            $model->email   = $email;
            $model->skin_id = $skinID;
            if (!$model->save()) {
                return [
                    'success' => FALSE,
                    'code'    => 2,
                    'message' => 'Subscribe not save',
                ];
            }

            return [
                'success' => TRUE,
                'code'    => 1,
                'message' => 'Congratulation you email: ' . $email . ' was added to our subscription',
            ];

        }

        return [
            'success' => FALSE,
            'code'    => 2,
            'message' => 'You email exists in our subscription',
        ];
    }
}
