<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "gift".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $description
 * @property string  $price
 * @property string  $agency_price
 * @property string  $image
 * @property integer $status
 * @property string  $created_at
 *
 * @property Order[] $orders
 */
class Gift extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gift';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['price', 'agency_price'], 'number'],
            [['status', 'type_id'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['image'], 'string', 'max' => 255],
            [['name', 'image', 'description'], function ($attribute) {
                $this->$attribute = Html::encode(strip_tags(HtmlPurifier::process($this->$attribute)));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'name'         => 'Name',
            'description'  => 'Description',
            'price'        => 'Price',
            'agency_price' => 'Agency Price',
            'image'        => 'Image',
            'status'       => 'Status',
            'created_at'   => 'Created At',
        ];
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return FALSE;
        }

        if ($this->image != '') {
            @unlink(dirname(__FILE__) . '/../web/uploads/gifts/large/' . $this->image);
            @unlink(dirname(__FILE__) . '/../web/uploads/gifts/thumb/' . $this->image);
        }

        return TRUE;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->image = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/', '', $this->image);
            $ImageName   = str_replace('uploads/temp/', '', $this->image);
            //$newThumbImageName = str_replace('/temp/', '/gifts/thumb', $this->image);
            // remove old image
            $giftsLargeDir = 'uploads/gifts/large';
            $giftsThumbDir = 'uploads/gifts/thumb';
            if ($this->image != '' && $this->image != $ImageName) {
                if (!file_exists($giftsLargeDir)) {
                    mkdir($giftsLargeDir, 0777, TRUE);
                }
                if (!file_exists($giftsThumbDir)) {
                    mkdir($giftsThumbDir, 0777, TRUE);
                }
                @rename(dirname(__FILE__) . '/../web/' . $this->image, dirname(__FILE__) . '/../web/' . $giftsLargeDir . '/' . $ImageName);
                @unlink(dirname(__FILE__) . '/../web/' . $this->image);
                @rename(dirname(__FILE__) . '/../web/' . str_replace('/temp/', '/temp/thumb_', $this->image), dirname(__FILE__) . '/../web/' . $giftsThumbDir . '/' . $ImageName);
                @unlink(dirname(__FILE__) . '/../web/' . str_replace('/temp/', '/temp/thumb_', $this->image));
            }

            $this->image = $ImageName;

            return TRUE;
        }

        return FALSE;
    }

    public function fields()
    {
        $fieldsList = [
            // field name is the same as the attribute name
            'id',
            'name',
            'description',
            'price',
            'agency_price',
            'image',
            'type_id',
        ];
        $user       = \Yii::$app->user->identity;
        if (!empty($user) && in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN, User::USER_SUPERADMIN])) {
            $fieldsList[] = 'status';
        }

        return $fieldsList;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['gift_id' => 'id']);
    }
}
