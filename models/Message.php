<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property integer $message_creator
 * @property integer $message_receiver
 * @property string $title
 * @property string $description
 * @property integer $reply_status
 * @property integer $was_deleted_by_user
 * @property integer $was_deleted_by_admin
 * @property string $created_at
 * @property string $readed_at
 *
 * @property User $messageCreator
 */
class Message extends \yii\db\ActiveRecord
{

    const WASNT_DELETED = 0;
    const WAS_DELETED = 10;

    const HAVE_REPLY = 1;
    const HAVENT_REPLY = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_creator', 'message_receiver', 'reply_status', 'was_deleted_by_user', 'was_deleted_by_admin'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'readed_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['message_creator'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['message_creator' => 'id']],
            [['title', 'description'], function ($attribute) {
                $this->$attribute = Html::encode(strip_tags(HtmlPurifier::process($this->$attribute)));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message_creator' => 'Message Creator',
            'message_receiver' => 'Message Receiver',
            'title' => 'Title',
            'description' => 'Description',
            'reply_status' => 'Reply Status',
            'was_deleted_by_user' => 'Was Deleted By User',
            'was_deleted_by_admin' => 'Was Deleted By Admin',
            'created_at' => 'Created At',
            'readed_at' => 'Readed At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessageCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'message_creator']);
    }
}
