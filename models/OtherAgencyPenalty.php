<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "other_agency_penalty".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $created_at
 * @property string $description
 * @property double $penalty_value
 *
 * @property User $user
 */
class OtherAgencyPenalty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'other_agency_penalty';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['description'], 'string'],
            [['penalty_value'], 'number'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'description' => 'Description',
            'penalty_value' => 'Penalty Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
