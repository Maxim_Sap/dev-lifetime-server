<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property integer $resource_type_id
 * @property integer $resource_id
 * @property integer $user_id
 * @property string $comments
 * @property string $date_create
 * @property string $date_read
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['resource_type_id', 'resource_id', 'user_id'], 'required'],
            [['resource_type_id', 'resource_id', 'user_id'], 'integer'],
            [['date_create', 'date_read'], 'safe'],
            [['comments'], 'string', 'max' => 255],
            [['comments'], function ($attribute) {
                $this->$attribute = Html::encode(strip_tags(HtmlPurifier::process($this->$attribute)));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'resource_type_id' => 'Resource Type ID',
            'resource_id' => 'Resource ID',
            'user_id' => 'User ID',
            'comments' => 'Comments',
            'date_create' => 'Date Create',
            'date_read' => 'Date Read',
        ];
    }

    public function getCommentsByTaskId($taskID)
    {
        $subQuery = (new Query())
            ->select('name, id')
            //->from([UserProfile::tableName() . ' user_pers']);
            ->from([Agency::tableName() . ' agency']);

        $model = (new Query)
            ->select('agency.name, comments.*')
            ->from([self::tableName() . ' comments'])
            ->leftJoin(User::tableName() . ' users', 'users.id = comments.user_id')
            ->leftJoin(['agency' => $subQuery], 'agency.id = users.agency_id')
            ->where(['resource_id'=>$taskID,'resource_type_id'=>1]);

        return $model->all();

    }

    public function getCommentsByShopId($action_id)
    {
        $subQuery = (new Query())
            ->select('name, id')
            //->from([UserPersonal::tableName() . ' user_pers']);
            ->from([Agency::tableName() . ' agency']);

        $model = (new Query)
            ->select('agency.name, comments.*')
            ->from([self::tableName() . ' comments'])
            ->leftJoin(User::tableName() . ' users', 'users.id = comments.user_id')
            ->leftJoin(['agency' => $subQuery], 'agency.id = users.agency_id')
            ->where(['resource_id'=>$action_id,'resource_type_id'=>2]);

        return $model->all();

    }

    public function readCommentsByResourceId($user_type_id,$resource_id)
    {

        $task_comments_read = (new Query)
            ->select('comments.id')
            ->from([Comments::tableName() . ' comments'])
            ->leftJoin(User::tableName() . ' users', 'users.id = comments.user_id')
            ->where(['comments.date_read'=>null,'comments.resource_id'=>$resource_id]);

        if (in_array($user_type_id,[User::USER_AGENCY, User::USER_INTERPRETER, User::USER_ADMIN])) {
            $task_comments_read->andWhere(['users.user_type'=>User::USER_SUPERADMIN]);
        }elseif($user_type_id == User::USER_SUPERADMIN){
            $task_comments_read->andWhere(['in','users.user_type',[User::USER_AGENCY, User::USER_INTERPRETER, User::USER_ADMIN]]);
        }
        $task_comments_read->indexBy('id');
        $ids = $task_comments_read->all();
        $ress = [];

        if(!empty($ids)){
            $ress = Comments::updateAll([
                'date_read'=>date('Y-m-d H:i:s')
            ],[
                'in','id',$ids
            ]);
        }

        return $ress;
    }


    public function notReadCommentsCount($user_type_id,$user_id,$resource_type_id = 1){
        $model = Comments::find()
            ->leftJoin(User::tableName() . ' users', 'users.user_id = comments.user_id')
            ->where(['comments.resource_type_id'=>$resource_type_id,'comments.date_read'=>null]);
        if (in_array($user_type_id,[3,4,7])) {
            if($resource_type_id == 1){
                $agency_id = (new User($user_id))->getOwnId($user_type_id);
                $model->leftJoin(Task::tableName() . ' tasks', 'tasks.id = comments.resource_id')
                    ->andWhere(['tasks.other_user_id' => $agency_id])
                    ->andWhere(['users.user_types'=>6]);
            }elseif($resource_type_id == 2){
                //shop
            }
        }elseif($user_type_id == 6){
            $model->andWhere(['in','users.user_types',[3,4,7]]);
        }

        $count = $model->count();

        return $count;

    }
}
