<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hair_color".
 *
 * @property integer $id
 * @property string $name
 * @property string $color
 *
 * @property UserProfile[] $userProfiles
 */
class HairColor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hair_color';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'color' => 'Color',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfiles()
    {
        return $this->hasMany(UserProfile::className(), ['hair' => 'id']);
    }
}
