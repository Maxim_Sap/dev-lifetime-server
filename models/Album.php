<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "album".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $description
 * @property integer $main_photo_id
 * @property integer $public
 * @property string $created_at
 * @property integer $active
 *
 * @property User $user
 * @property Photo[] $photos
 */
class Album extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    public static function tableName()
    {
        return 'album';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'main_photo_id', 'public', 'active'], 'integer'],
            [['created_at'], 'safe'],
            [['title', 'description'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'description' => 'Description',
            'main_photo_id' => 'Main Photo ID',
            'public' => 'Public',
            'created_at' => 'Created At',
            'active' => 'Active',
        ];
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {     
            if (!$insert) {                
                $this->title = Html::encode(strip_tags(HtmlPurifier::process($this->title)));
                $this->description = Html::encode(strip_tags(HtmlPurifier::process($this->description)));                
            } else {
                $this->title = Html::encode(strip_tags(HtmlPurifier::process($this->title)));
                $this->description = Html::encode(strip_tags(HtmlPurifier::process($this->description)));
            }            
            return true;
        }
        return false;
    }

    public function fields()
    {
        $fieldsList = [
            // field name is the same as the attribute name
            'id',
            'user_id',            
            'main_photo_id',
            'title' => function($model) {
                return Html::decode($model->title);
            },
            'description' => function($model) {
                return Html::decode($model->description);
            },      
            'public',
            'created_at',
            'active'          
        ];
        $user = \Yii::$app->user->identity;
        if (!empty($user) && in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN, User::USER_SUPERADMIN])) {           
        }
        return $fieldsList;     
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['album_id' => 'id']);
    }
}
