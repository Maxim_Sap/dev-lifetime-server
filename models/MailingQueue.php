<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mailing_queue".
 *
 * @property integer $id
 * @property integer $mailing_session_id
 * @property integer $template_id
 * @property integer $sender
 * @property integer $receiver
 * @property integer $was_received
 * @property integer $was_aborted
 * @property string $created_at
 * @property string $received_at
 */
class MailingQueue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    const WASNT_RECEIVED = 0;
    const WASNT_ABORTED = 0;
    const WAS_RECEIVED = 1;
    const WAS_ABORTED = 1;
    const CREATED_FOR_CHAT = 0;
    const CREATED_FOR_MESSAGE = 1;

    public static function tableName()
    {
        return 'mailing_queue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mailing_session_id', 'template_id', 'sender', 'receiver'], 'required'],
            [['mailing_session_id', 'created_for', 'template_id', 'sender', 'receiver', 'was_received', 'was_aborted'], 'integer'],
            ['created_for', 'in', 'range' => [self::CREATED_FOR_CHAT, self::CREATED_FOR_MESSAGE]],
            [['created_at', 'received_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mailing_session_id' => 'Mailing Session ID',
            'template_id' => 'Template ID',
            'sender' => 'Sender',
            'receiver' => 'Receiver',
            'was_received' => 'Was Received',
            'was_aborted' => 'Was Aborted',
            'created_at' => 'Created At',
            'received_at' => 'Received At',
        ];
    }

    public function getMailingSession()
    {
        return $this->hasOne(MailingSession::className(), ['id' => 'mailing_session_id']);
    }

    public function getSender()
    {
        return $this->hasOne(User::className(), ['id' => 'sender']);
    }

    public function getReceiver()
    {
        return $this->hasOne(User::className(), ['id' => 'receiver']);
    }    

    public function getTemplate()
    {
        return $this->hasOne(MailingTemplate::className(), ['id' => 'template_id']);
    }

}
