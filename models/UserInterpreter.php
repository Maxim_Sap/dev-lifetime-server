<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_interpreter".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $interpreter_id
 *
 * @property User $interpreter
 * @property User $user
 */
class UserInterpreter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_interpreter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'interpreter_id'], 'integer'],
            [['interpreter_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['interpreter_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'interpreter_id' => 'Interpreter ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterpreter()
    {
        return $this->hasOne(User::className(), ['id' => 'interpreter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
