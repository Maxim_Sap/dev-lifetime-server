<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;


/**
 * This is the model class for table "rule_violation_by_agency".
 *
 * @property integer $id
 * @property integer $agency_id
 * @property string $description
 * @property string $created_at
 *
 * @property Agency $agency
 */
class RuleViolationByAgency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rule_violation_by_agency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agency_id'], 'required'],
            [['agency_id'], 'integer'],
            [['description'], 'string'],
            [['created_at'], 'safe'],
            [['agency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agency::className(), 'targetAttribute' => ['agency_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agency_id' => 'Agency ID',
            'description' => 'Description',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {     
            if (!$insert) {
                $this->agency_id = $this->oldAttributes['agency_id'];
                $this->description = Html::encode(HtmlPurifier::process($this->description));
                $this->created_at = $this->oldAttributes['created_at'];
            } else {
                $this->description = Html::encode(HtmlPurifier::process($this->description));
            }            
            return true;
        }
        return false;
    }

}
