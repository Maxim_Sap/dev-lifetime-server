<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "eyes_color".
 *
 * @property integer $id
 * @property string $name
 * @property string $color
 *
 * @property UserProfile[] $userProfiles
 */
class EyesColor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eyes_color';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'color' => 'Color',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfiles()
    {
        return $this->hasMany(UserProfile::className(), ['eyes' => 'id']);
    }
}
