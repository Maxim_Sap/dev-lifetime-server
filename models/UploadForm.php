<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\web\BadRequestHttpException;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => ['png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG', 'gif', 'GIF'],'maxSize' => 1024*1024*3],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $new_name           = 'img'. md5($this->imageFile->baseName . time());
            $orign_dir          = 'uploads/original/';
            $thumb_255x290      = 'uploads/thumb_255x290/';
            $thumb_small        = 'uploads/thumb_63x63/';
            $orign_name         = $orign_dir . $new_name . '.' . $this->imageFile->extension;
            $thumb_255x290_name = $thumb_255x290 . $new_name . '.' . $this->imageFile->extension;
            $thumb_small_name   = $thumb_small . $new_name . '.' . $this->imageFile->extension;

            if (!file_exists($orign_dir)) {
                mkdir($orign_dir, 0777, true);
            }
            if (!file_exists($thumb_255x290)) {
                mkdir($thumb_255x290, 0777, true);
            }
            if (!file_exists($thumb_small)) {
                mkdir($thumb_small, 0777, true);
            }

            $this->imageFile->saveAs($orign_name);

            $file  = Yii::getAlias('@app/web/' . $orign_name);
            $image = Yii::$app->image->load($file);
            $image->resize(255, 290, \yii\image\drivers\Image::WIDTH);
            $image->save($thumb_255x290_name);
            $image->resize(50, 50, \yii\image\drivers\Image::WIDTH);
            $image->save($thumb_small_name);

            return ['orign' => $orign_name, 'normal' => $thumb_255x290_name, 'small' => $thumb_small_name];
        } else {
            return false;            
        }
    }

    public function uploadGiftImg()
    {
        if ($this->validate()) {

            $new_name           = md5($this->imageFile->baseName . time());
            $temp_dir          = 'uploads/temp/';
            $temp_name         = $temp_dir . $new_name . '.' . $this->imageFile->extension;
            $temp_thumb_name         = $temp_dir ."thumb_". $new_name . '.' . $this->imageFile->extension;

            if (!file_exists($temp_dir)) {
                mkdir($temp_dir, 0777, true);
            }

            $this->imageFile->saveAs($temp_name);

            $file  = Yii::getAlias('@app/web/' . $temp_name);
            $image = Yii::$app->image->load($file);

            $image->resize(1024, 700, \yii\image\drivers\Image::WIDTH);
            $image->save($temp_name);
            $image->resize(158, 198, \yii\image\drivers\Image::WIDTH);
            $image->save($temp_thumb_name);

            return ['normal' => $temp_name];
        } else {
            return false;
        }
    }

    public function uploadPostImg()
    {
        if ($this->validate()) {

            $new_name           = md5($this->imageFile->baseName . time());
            $temp_dir          = 'uploads/temp/';
            $temp_name         = $temp_dir . $new_name . '.' . $this->imageFile->extension;

            if (!file_exists($temp_dir)) {
                mkdir($temp_dir, 0777, true);
            }

            $this->imageFile->saveAs($temp_name);

            $file  = Yii::getAlias('@app/web/' . $temp_name);
            $image = Yii::$app->image->load($file);

            $image->resize(527, 292, \yii\image\drivers\Image::WIDTH);
            $image->save($temp_name);

            return ['normal' => $temp_name];
        } else {
            return false;
        }
    }
}