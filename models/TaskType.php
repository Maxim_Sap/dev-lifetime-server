<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task_type".
 *
 * @property integer $id
 * @property string $type
 *
 * @property Task[] $tasks
 */
class TaskType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const TYPE_NEW = 1;
    const TYPE_WAIT_TO_APPROVE = 2;
    const TYPE_EXPIRED = 3;
    const TYPE_DONE = 4;

    public static function tableName()
    {
        return 'task_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['status_id' => 'id']);
    }
}
