<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "video".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $path
 * @property integer $poster
 * @property string $description
 * @property integer $status
 * @property integer $public
 * @property integer $premium
 * @property string $created_at
 *
 * @property User $user
 */
class Video extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'poster', 'status', 'public', 'premium'], 'integer'],
            [['path'], 'required'],
            [['description'], 'string'],
            [['created_at'], 'safe'],
            [['path'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['description'], function ($attribute) {
                $this->$attribute = Html::encode(strip_tags(HtmlPurifier::process($this->$attribute)));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'path' => 'Path',
            'poster' => 'Poster',
            'description' => 'Description',
            'status' => 'Status',
            'public' => 'Public',
            'premium' => 'Premium',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getPosterImage()
    {
        return $this->hasOne(Photo::className(), ['id' => 'poster']);
    }

    public function getVideoAccesses()
    {
        return $this->hasMany(VideoAccess::className(), ['video_id' => 'id']);
    }

}
