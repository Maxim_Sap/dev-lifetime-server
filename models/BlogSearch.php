<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Blog;

/**
 * BlogSearch represents the model behind the search form about `app\models\Blog`.
 */
class BlogSearch extends Blog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'on_main', 'status', 'sort_order'], 'integer'],
            [['title', 'description', 'image', 'category', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Blog::find();

        // add conditions that should always apply here

        $query = $query->where(['status' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'page' => $params['page'],
                'pageSize' => $params['limit'],                
            ],
            'sort' => [
                'defaultOrder' => [
                    'sort_order' => (!empty($params['searchParams']['sort_order']) && $params['searchParams']['sort_order']=='desc') ? SORT_DESC : SORT_ASC,
                    'id' => SORT_ASC, 
                ]
            ],  
        ]);

        $fields = [
            self::formName() => [
                'title' => $params['search'],
                'description' => $params['search'],
            ]
        ];
        if (!empty($params['category'])) {
            $fields[self::formName()]['category'] = $params['category'];
        }

        $this->load($fields);        

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'category' => $this->category,            
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description]);
        
        return $dataProvider;
    }
}
