<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "agency".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $contact_person
 * @property string  $passport_number
 * @property string  $office_address
 * @property string  $office_phone
 * @property string  $m_phone
 * @property string  $director_address
 * @property string  $skype
 * @property string  $bank_detail
 * @property string  $card_number
 * @property string  $card_user_name
 * @property integer $approve_status
 *
 * @property User[]  $users
 */
class Agency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['approve_status'], 'integer'],
            [['name', 'director_address', 'contact_person', 'card_user_name'], 'string', 'max' => 255],
            [['bank_detail'], 'string'],
            [['passport_number'], 'string', 'max' => 30],
            [['office_address', 'skype'], 'string', 'max' => 150],
            [['office_phone', 'm_phone', 'card_number'], 'string', 'max' => 50],
            [['name', 'director_address', 'contact_person', 
              'card_user_name', 'bank_detail', 'passport_number', 
              'office_address', 'skype', 'office_phone', 'm_phone', 'card_number'], function ($attribute) {
                $this->$attribute = Html::encode(strip_tags(HtmlPurifier::process($this->$attribute)));
            }],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'name'             => 'Name',
            'contact_person'   => 'Contact Person',
            'director_address' => 'Director address',
            'bank_detail'      => 'bank_detail',
            'skype'            => 'skype',
            'passport_number'  => 'Passport Number',
            'office_address'   => 'Office Address',
            'office_phone'     => 'Office Phone',
            'm_phone'          => 'Mob Phone',
            'card_number'      => 'Card Number',
            'card_user_name'   => 'Card User Name',
            'approve_status'   => 'Approve Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['agency_id' => 'id']);
    }

    public function getViolations()
    {
        return $this->hasMany(RuleViolationByAgency::className(), ['agency_id' => 'id']);
    }

    public function getUsersIDs($type = 'girls', $status = [User::STATUS_ACTIVE])
    {
        $IDs = [];
        if ($type == 'admin') {
            $result = $this->getUsers()->where(['user_type' => User::USER_ADMIN, 'status' => $status])->asArray()->all();
        } elseif ($type == 'girls') {
            $result = $this->getUsers()->where(['user_type' => User::USER_FEMALE, 'status' => $status])->asArray()->all();
        } elseif ($type == 'interpreters') {
            $result = $this->getUsers()->where(['user_type' => User::USER_INTERPRETER, 'status' => $status])->asArray()->all();
        } elseif ($type == 'all') {
            $result = $this->getUsers()->where(['user_type' => [User::USER_FEMALE, User::USER_AGENCY, User::USER_INTERPRETER, User::USER_ADMIN], 'status' => $status])->asArray()->all();
        }
        if (!empty($result)) {
            foreach ($result as $row) {
                $IDs[] = $row["id"];
            }
        }

        return $IDs;
    }


}
