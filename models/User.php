<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\web\Request as WebRequest;
use Firebase\JWT\JWT;

use app\services\AlbumService;
use app\services\PhotoService;
use app\services\Locations;
use app\services\LetterService;
use app\services\StatusService;


/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $email
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $skin_id
 * @property integer $user_type
 * @property integer $avatar_photo_id
 * @property string $access_token
 * @property integer $token_end_date
 * @property integer $last_activity
 * @property integer $cam_online
 * @property integer $referral_id
 * @property integer $status
 * @property string $created_at
 *
 * @property Action[] $actions
 * @property Agency[] $agencies
 * @property AgencyRevard[] $agencyRevards
 * @property Album[] $albums
 * @property FeaturedLetter[] $featuredLetters
 * @property Interpreter[] $interpreters
 * @property Location[] $locations
 * @property LogTable[] $logTables
 * @property Message[] $messages
 * @property MissingChat[] $missingChats
 * @property MissingChat[] $missingChats0
 * @property PhotoLike[] $photoLikes
 * @property Pricelist[] $pricelists
 * @property Status[] $statuses
 * @property Task[] $tasks
 * @property Task[] $tasks0
 * @property UserProfile $profile
 * @property Skin $skin
 * @property UserType $userType
 * @property UserAuthFacebook $userAuthFacebook
 * @property UserAuthGoogle $userAuthGoogle
 * @property UserBlacklist[] $userBlacklists
 * @property UserBlacklist[] $userBlacklists0
 * @property UserFavoriteList[] $userFavoriteLists
 * @property UserFavoriteList[] $userFavoriteLists0
 * @property UserVideoSetting[] $userVideoSettings
 * @property UserVideoSetting[] $userVideoSettings0
 * @property UserVideoTempSession[] $userVideoTempSessions
 * @property UserVideoTempSession[] $userVideoTempSessions0
 * @property UserVisiters[] $userVisiters
 * @property UserWink[] $userWinks
 * @property UserWink[] $userWinks0
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_NO_ACTIVE = 3;
    const STATUS_NO_EMAIL_CONFIRM = 5;
    const STATUS_WAIT_FOR_APPROVE = 7;
    const STATUS_ACTIVE = 10;  
    const STATUS_VISIBLE = 10;
    const STATUS_INVISIBLE = 0;
    const STATUS_FAKE_YES = 10;
    const STATUS_FAKE_NO = 0;

    const USER_MALE = 1;
    const USER_FEMALE = 2;
    const USER_AGENCY = 3;
    const USER_INTERPRETER = 4;
    const USER_AFFILIATE = 5;
    const USER_SUPERADMIN = 6;
    const USER_ADMIN = 7;
    const USER_SITEADMIN = 8;
    
    const SCENARIO_REGISTER_BY_SOCIAL = 'social';

    use \damirka\JWT\UserTrait;
    use \highweb\ratelimiter\UserRateLimiterTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['skin_id', 'user_type', 'token_end_date', 'email'], 'required'],
            [['skin_id', 'user_type', 'avatar_photo_id', 'token_end_date', 'last_activity', 'cam_online', 'status', 'approve_status_id', 'agency_id', 'fake', 'visible'], 'integer'],
            [['created_at', 'interpreter_ids', 'user_ids'], 'safe'],
            ['email', 'trim'],            
            ['email', 'email'],
            [['interpreter_ids', 'user_ids'], 'each', 'rule' => ['integer']],
            [['email'], 'string', 'max' => 255],
            //[['referral_id'], 'integer'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            [['skin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Skin::className(), 'targetAttribute' => ['skin_id' => 'id']],
            [['user_type'], 'exist', 'skipOnError' => true, 'targetClass' => UserType::className(), 'targetAttribute' => ['user_type' => 'id']],
            [['agency_id'], 'exist', 'targetClass' => Agency::className(), 'targetAttribute' => ['agency_id' => 'id'], 'message' => "Please choose agency"],
            [['approve_status_id'], 'exist', 'targetClass' => ApproveStatus::className(), 'targetAttribute' => ['approve_status_id' => 'id'], 'message' => "Please choose approve status"],            
        ];
    }
    
     public function scenarios()
    {
         $scenarios = parent::scenarios();
         $scenarios[self::SCENARIO_REGISTER_BY_SOCIAL] = ['skin_id', 'user_type', 'token_end_date','approve_status_id','last_activity', 'cam_online', 'status'];
         
         return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'skin_id' => 'Skin ID',            
            'user_type' => 'User Type',
            'avatar_photo_id' => 'Avatar Photo ID',
            'access_token' => 'Access Token',
            'token_end_date' => 'Token End Date',
            'last_activity' => 'Last Activity',
            'cam_online' => 'Cam Online',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    public function fields()
    {
        $fieldsList = [
            // field name is the same as the attribute name
            'id',            
            'username' => function($model) {
                if (!empty($model->profile->first_name) && !empty($model->profile->last_name)) {
                    return $model->profile->first_name . ' ' . $model->profile->last_name;
                } elseif (!empty($model->profile->first_name)) {
                    return $model->profile->first_name;
                }  else {
                    return 'Noname';
                }                
            },
            'avatar' => function($model) {
                return (!empty($model->avatar->small_thumb)) ? $model->avatar->small_thumb : null;
            },            
            'cam_online',
            'user_type'
        ];
        $user = \Yii::$app->user->identity;
        if (!empty($user) && in_array($user->user_type, [User::USER_AGENCY, User::USER_AFFILIATE, User::USER_ADMIN, User::USER_SUPERADMIN])) {
            $fieldsList[] = 'agency_id';
            $fieldsList[] = 'created_at';            
            $fieldsList[] = 'email';
            $fieldsList[] = 'fake';
            $fieldsList[] = 'last_activity';
            $fieldsList[] = 'owner_status';
            $fieldsList[] = 'skin_id';
            $fieldsList[] = 'status';            
            $fieldsList[] = 'token_end_date';            
            $fieldsList[] = 'visible';            
            $fieldsList[] = 'user_ids';            
        }
        if (!empty($user) && in_array($user->user_type, [User::USER_SUPERADMIN])) {
            $fieldsList[] = 'access_token';
        }
        return $fieldsList;
    }

    protected static function getSecretKey()
    {
        $config = parse_ini_file(__DIR__ . '/../config/settings.ini', true);
        return $config["jwt_secret_key"];        
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            [
            'class' => \voskobovich\linker\LinkerBehavior::className(),
                'relations' => [
                    'interpreter_ids' => [
                        'interpreters'
                    ],
                    'user_ids' => [
                        'users'
                    ]
                ],
            ],
        ];
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        $secret = static::getSecretKey();

        // Decode token and transform it into array.
        // Firebase\JWT\JWT throws exception if token can not be decoded
        try {
            $decoded = JWT::decode($token, $secret, [static::getAlgo()]);
        } catch (\Exception $e) {
            return false;
        }

        static::$decodedToken = (array) $decoded;

        // If there's no jti param - exception
        if (!isset(static::$decodedToken['jti'])) {
            return false;
        }
        // Token expired
        if (!isset(static::$decodedToken['expiresOn']) || (static::$decodedToken['expiresOn'] < time())) {
            return false;
        }


        // JTI is unique identifier of user.
        // For more details: https://tools.ietf.org/html/rfc7519#section-4.1.7
        $id = static::$decodedToken['jti'];

        return static::findByJTI($id);
    }

    public function getJWT($timeDelta = 0)
    {
        // Collect all the data
        $secret      = static::getSecretKey();
        $currentTime = time();
        $request     = Yii::$app->request;
        $hostInfo    = '';

        // There is also a \yii\console\Request that doesn't have this property
        if ($request instanceof WebRequest) {
            $hostInfo = $request->hostInfo;
        }

        if ($timeDelta == 0) {
            $timeDelta = Yii::$app->params['token_time'];
        }

        // Merge token with presets not to miss any params in custom
        // configuration
        $token = array_merge([
            'iss' => $hostInfo,
            'aud' => $hostInfo,
            'iat' => $currentTime,
            'nbf' => $currentTime
        ], static::getHeaderToken());

        // Set up id
        $token['jti'] = $this->getJTI();
        $token['expiresOn'] = $currentTime + $timeDelta;

        return JWT::encode($token, $secret, static::getAlgo());
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {            
            
            return true;
        }
        return false;
    }        

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->created_at > date('Y-m-d H:i:s', time() - 30)) {
            $this->access_token = $this->getJWT();
            $this->updateAttributes(['access_token']);                      
            parent::afterSave($insert, $changedAttributes);        
            //LogTable::addLog('token change' . $this->id);
        }
    }  

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => [self::STATUS_ACTIVE]]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
            'status' => [self::STATUS_ACTIVE, self::STATUS_NO_EMAIL_CONFIRM],
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString(40);
    }
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString(40) . '_' . time();
    }
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function setLastActivity()
    {

        $user = Yii::$app->user->identity;
        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }
        $lastActivity = $user->last_activity;
        $user->last_activity = time();
        $user->scenario = User::SCENARIO_REGISTER_BY_SOCIAL;
        if (!$user->save()) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User last activity error save'
            ];
        }

        $dateFormat = date('Y-m-d H:i:s', time()-60); // 1 минуту назад

        $activity = Activity::find()
            ->where(['user_id' => $user->id])
            ->orderBy(['ended_at' => SORT_DESC])->one();
        
        if (empty($activity)) {
            $activity             = new Activity();
            $activity->user_id    = $user->id;
            $activity->started_at   = date('Y-m-d H:i:s', time());
            $activity->ended_at   = date('Y-m-d H:i:s', time());

        } else {
            $activity->ended_at   = date('Y-m-d H:i:s', time());
            $duration             = time() - $lastActivity;                       
            if ($user->cam_online) {
                $activity->cam_duration += $duration;
            }
        }

        if (!$activity->save()) {
            return [
                'success' => false, 
                'message' => 'User activity error save'
            ];
        }

        return ['success' => true];
    }

    public function setApproveStatus($otherUserID)
    {
        $user = User::findOne(['id' => $otherUserID]);
        if (empty($user)) {
            return [
                'success' => FALSE, 
                'message' => 'User not found'
            ];
        }
        $userNewApproveStatus = ($user->approve_status_id == ApproveStatus::STATUS_IN_PROGRESS) ? ApproveStatus::STATUS_NOT_APPROVED : ApproveStatus::STATUS_IN_PROGRESS;
        $user->approve_status_id     = $userNewApproveStatus;
        if (!$user->save()) {
            return [
                'success' => FALSE, 
                'message' => 'User invisible status error save'
            ];
        }

        return [
            'success' => TRUE, 
            'status' => $userNewApproveStatus
        ];
    }

    public function setToBlackList($otherUserID)
    {
        if (empty($otherUserID) || (!is_numeric($otherUserID) && !is_array($otherUserID))) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }

        //если пришел массив
        if (is_array($otherUserID) && array_filter($otherUserID, 'is_numeric')) {
            if (empty($otherUserID)) {
                return [
                    'success' => false, 
                    'message' => 'Missing params'
                ];
            }
            $rows = [];
            $blacklistIDs = [];
            foreach ($otherUserID as $id) {
                $rows[] = [
                    'user_id' => $this->id,
                    'blacklist_user_id' => $id
                ];
                $blacklistIDs[] = $id;
            }
            UserBlacklist::deleteAll(['user_id' => $this->id, 'blacklist_user_id' => $blacklistIDs]);
            $result = Yii::$app->db->createCommand()->batchInsert(UserBlacklist::tableName(), ['user_id', 'blacklist_user_id'], $rows)->execute();

            if (!$result) {
                return ['success' => false, 'message' => 'Can\'t add users to black list'];
            }

            //если пришел один id
        } else {
            $blackList = UserBlacklist::findOne(['user_id' => $this->id, 'blacklist_user_id' => $otherUserID]);

            if (empty($blackList)) {
                $blackList                = new UserBlackList();
                $blackList->user_id       = $this->id;
                $blackList->blacklist_user_id = $otherUserID;
                if (!$blackList->save()) {
                    return ['success' => false, 'message' => 'black list error save'];
                }

                return ['success' => true, 'message' => 'add user to black list'];
            }

            UserBlacklist::deleteAll(['id' => $blackList->id]);

        }

        return ['success' => true, 'message' => 'user from black list deleted'];

    }

    public function getUsersInfoFromAgency($type='girls', $userType = null, $currentUserID = null)
    {        

        //for translators
        if($userType == User::USER_INTERPRETER && $currentUserID != null){        
            $userIDs = $this->user_ids;
        }else{
            $userIDs = $this->getUsersFromAgency($type);
        }

        if (empty($userIDs)) {
            return null;
        }
        $usersInfoModel = UserProfile::find()
            ->select(['id', 'first_name', 'last_name'])
            ->where(['in', 'id', $userIDs])
            ->asArray()
            ->all();

        return $usersInfoModel;
    }

    public function getUserActivity($otherUserID, $agencyUsersIds, $searchParams, $offset, $limit)
    {
        $activity = 0;
        $dateFrom = (isset($searchParams['dateFrom']) && strtotime($searchParams['dateFrom'])) ? $searchParams['dateFrom'] . ' 00:00:00' : date('Y-m-d H:i:s', mktime(0, 0, 0, date('m'), 1, date('Y')));
        $dateTo   = (isset($searchParams['dateTo']) && strtotime($searchParams['dateTo'])) ? $searchParams['dateTo'] .' 23:59:59' : date('Y-m-d H:i:s', time());

        if ($otherUserID == null) {
            $activity = (new Query())
                ->select(['activity.*', 'user_profile.first_name', 'user_profile.last_name', 'agency.name'])
                ->from([Activity::tableName()])
                ->leftJoin(User::tableName(), 'activity.user_id = user.id')
                ->leftJoin(Agency::tableName(), 'agency.id = user.agency_id')
                ->leftJoin(UserProfile::tableName(), 'user_profile.id = user.id');
            if (!empty($agencyUsersIds) && is_array($agencyUsersIds)) {
                $activity->where(['in', 'activity.user_id', $agencyUsersIds]);
            } elseif (!empty($searchParams['userType']) && is_numeric($searchParams['userType'])) {
                $activity
                    ->leftJoin(User::tableName(), 'user.id = activity.user_id')
                    ->where(['user.user_type' => $searchParams['userType']]);
            }

            if (isset($searchParams['agencyID']) && is_numeric($searchParams['agencyID']) && isset($searchParams['userType']) && $searchParams['userType'] == User::USER_FEMALE) {
                $activity->andWhere(['user.agency_id' => $searchParams['agencyID'], 'user.user_type' => $searchParams['userType']]);
            }

            if (isset($searchParams['name']) && trim($searchParams['name']) != "") {
                $activity->andFilterWhere([
                    'or',
                    ['like', 'user_profile.first_name', trim($searchParams['name'])],
                    ['like', 'user_profile.last_name', trim($searchParams['name'])],
                ]);
            }

            $activity->andwhere(['>=', 'activity.started_at', $dateFrom])
                ->andwhere(['<=', 'activity.ended_at', $dateTo])
                ->orderBy('activity.started_at DESC');
        } else {
            $activity = Activity::find()
                ->where(['user_id' => $otherUserID])
                ->andwhere(['>=', 'started_at', $dateFrom])
                ->andwhere(['<=', 'ended_at', $dateTo])
                ->orderBy('started_at DESC')
                ->asArray();
        }


        $count = $activity->count();

        if ($limit !== null && is_numeric($limit)) {
            $activity->limit($limit);
        }
        if ($offset !== null && is_numeric($offset)) {
            $activity->offset($offset);
        }

        if (!empty($activity)) {
            $activity = $activity->all();
        }

        return [
            'activityArray' => $activity, 
            'count' => $count
        ];
    }

    public function getOtherPenalty($userType, $penaltyID = null, $dateFrom = null, $dateTo = null, $agencyID = null) {

        $subQuery = (new Query)
            ->select('agency.name, user.agency_id, user.id')
            ->from(User::tableName())
            ->leftJoin(Agency::tableName(), 'user.agency_id = agency.id')
            ->where(['user_type' => User::USER_AGENCY]);

        $query = (new Query())
            ->select('penalty.*,user_profile.name as agency_name, 
             user_profile.id as user_to_id')
            ->from([OtherAgencyPenalty::tableName() . ' penalty'])
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = penalty.user_id');
        if ($userType != User::USER_SUPERADMIN) {
            $query->where(['penalty.user_id' => $this->id]);
        }
        if($penaltyID !== null){
            $query->where(['penalty.id' => $penaltyID]);
        }

        if (!empty($dateFrom)) {
            $query->andWhere(['>=', 'penalty.created_at', $dateFrom]);
        }
        if (!empty($dateTo)) {
            $query->andWhere(['<=', 'penalty.created_at', $dateTo]);
        }
        if (!empty($agencyID)) {
            $query->andWhere(['user_profile.id' => $agencyID]);
        }

        $query->orderBy('penalty.id DESC');

        $result = $query->all();

        return $result;
    }

    public static function getGuest($userID)
    {
        $visiters = UserVisiters::find()
            ->where(['visited_user_id' => $userID])
            ->all();
        if (empty($visiters)) {
            return null;
        }

        foreach ($visiters as $visitor) {
            $ids[] = $visitor->user_id;
        }

        return $ids;
    }

    public static function getRecent($userID)
    {
        $visits = UserVisiters::find()
            ->where(['user_id' => $userID])
            ->orderBy('visit_datetime DESC')
            ->all();
        if (empty($visits)) {
            return null;
        }

        foreach ($visits as $visit) {
            $ids[] = $visit->visited_user_id;
        }

        return $ids;
    }

    public static function getFavorite($userID)
    {
        $favorites = UserFavoriteList::find()
            ->where(['user_id' => $userID])
            ->all();
        if (empty($favorites)) {
            return null;
        }

        foreach ($favorites as $favorite) {
            $ids[] = $favorite->favorite_user_id;
        }

        return $ids;
    }

    public function setVisitor($otherUserID)
    {

        $visitor = UserVisiters::findOne(['user_id' => $this->id, 'visited_user_id' => $otherUserID]);
        if (empty($visitor)) {
            $visitor = new UserVisiters();
        }

        $visitor->user_id = $this->id;
        $visitor->visited_user_id = $otherUserID;
        $visitor->visit_datetime = date('Y-m-d H:i:s', time());
        if (!$visitor->save()) {
            return [
                'success' => false, 
                'message' => 'user visitor not save'
            ];
        }

        return [
            'success' => true
        ];
    }

    public function deleteUser()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            switch ($this->user_type) {
                //если мальчик
                case User::USER_MALE:
                case User::USER_ADMIN:
                case User::USER_SUPERADMIN:
                    UserProfile::deleteAll(['id' => $this->id]);
                    break;
                //если девочка
                case User::USER_FEMALE:
                    UserProfile::deleteAll(['id' => $this->id]);
                    break;
                //если агенство
                case User::USER_AGENCY:
                    User::updateAll(['agency_id' => 1], ['agency_id' => $this->agency_id]);
                    Agency::deleteAll(['id' => $this->agency_id]);
                    break;
                //если переводчик
                case User::USER_INTERPRETER:                
                    UserInterpreter::deleteAll(['interpreter_id' => $this->id]);
                    Interpreter::deleteAll(['id' => $this->id]);
                    break;
                //если аффилейт
                case User::USER_AFFILIATE:
                    User::updateAll(['agency_id' => 1], ['agency_id' => $this->id]);
                    Affiliates::deleteAll(['id' => $this->id]);
                    break;
            }

            $albumService = new AlbumService($this->id);
            $albumsIDs = $albumService->getAllAlbumsIds();
            
            // delete all user photo from all albums
            $photoService = new PhotoService($this->id);
            $photoService->deleteAllPhotoByAlbumsIds($albumsIDs);

            // delete all user albums
                    
            $albumService->deleteAllByUserId();

            // delete all messages from featured
            $letterService = new LetterService($this->id);        
            $letterService->deleteAllFromFeatured();

            // delete user location
            $location = new Locations($this->id);        
            $location->deleteAllUserLocation();
            
            // delete all user status
            $statusService = new StatusService($this->id);        
            $statusService->deleteAllUserStatus();
            
            // delete from facebook table
            UserAuthFacebook::deleteAll(['user_id' => $this->id]);
            // delete from google table
            UserAuthGoogle::deleteAll(['user_id' => $this->id]);
            // delete from favorite table
            UserFavoriteList::deleteAll(['user_id' => $this->id]);
            UserFavoriteList::deleteAll(['favorite_user_id' => $this->id]);

            // delete from user table
            $result = User::deleteAll(['id' => $this->id]);
            // delete from blacklist
            UserBlacklist::deleteAll(['user_id' => $this->id]);
            UserBlacklist::deleteAll(['blacklist_user_id' => $this->id]);
            
            // delete from user_wink table
            UserWink::deleteAll(['user_creator' => $this->id]);
            UserWink::deleteAll(['user_receiver' => $this->id]);

            $transaction->commit();
            return [
                'success' => true
            ];
        } catch (\Exception $e) {
            $transaction->rollBack();
            return [
                'success' => false,
                'errors' => $e->getMessage()
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            return [
                'success' => false,
                'errors' => $e->getMessage()
            ];
        }     
    }

    public function checkOwn($otherUserID)
    {

        if ($this->user_type == User::USER_SUPERADMIN) {
            return true;
        }
        //agency
        if ($this->user_type == User::USER_AGENCY) {
            if($this->id == $otherUserID){
                return true;
            }

            $otherUser = User::findOne(['id' => $otherUserID, 'agency_id' => $this->agency_id]);
            if (empty($otherUser)) {
                return false;
            } else {
                return true;
            }            
        }

        //translators
        if ($this->user_type == User::USER_INTERPRETER) {
            
            if (in_array($otherUserID, $this->user_ids)) {
                return true;
            } else {
                return false;
            }
            
        }

        //agency_admin
        if ($this->user_type == User::USER_ADMIN) {

            $otherUser = User::findOne(['id' => $otherUserID, 'agency_id' => $this->agency_id]);
            if (empty($otherUser)) {
                return false;
            } else {
                return true;
            }
        }
    }

    public static function getAdmires($userID)
    {
        $model = UserFavoriteList::find()
            ->where(['favorite_user_id' => $userID])
            ->all();
        if (empty($model)) {
            return null;
        }

        foreach ($model as $one) {
            $ids[] = $one->user_id;
        }

        return $ids;
    }

    public static function getMatches($userID)
    {
        $favorite_model = UserFavoriteList::find()
            ->where(['favorite_user_id' => $userID])
            ->all();
        if (empty($favorite_model)) {
            return null;
        }

        foreach ($favorite_model as $one) {
            $ids[] = $one->user_id;
        }

        //return $ids;

        $favorite_model = UserFavoriteList::find()
            ->where(['user_id' => $userID])
            ->andWhere(['in', 'favorite_user_id', $ids])
            ->all();
        if (empty($favorite_model)) {
            return null;
        }

        foreach ($favorite_model as $one) {
            $ids2[] = $one->favorite_user_id;
        }

        return $ids2;
    }

    public function setToFavorite($otherUserID)
    {
        if (empty($otherUserID) || !is_numeric($otherUserID)) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }

        $favorite = UserFavoriteList::findOne(['user_id' => $this->id, 'favorite_user_id' => $otherUserID]);

        if (empty($favorite)) {
            $favorite                = new UserFavoriteList();
            $favorite->user_id       = $this->id;
            $favorite->favorite_user_id = $otherUserID;
            if (!$favorite->save()) {
                return [
                    'success' => false, 
                    'message' => 'Some error happend during processing your request'
                ];
            }
            return [
                'success' => true, 
                'message' => "User add to favorite", 
                'status' => 1
            ];
        }

        UserFavoriteList::deleteAll(['id' => $favorite->id]);

        return [
            'success' => true, 
            'message' => "User delete from favorite", 
            'status' => 0
        ];
    }

    public function deleteVideo($videoID)
    {
        $video = Video::findOne(['user_id' => $this->id, 'id' => $videoID]);

        if (empty($video)) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => 'Video not found'
            ];
        }
        if (!empty($video->poster)) {
            $posterPhoto = Photo::findOne(['id' => $video->poster]);
            @unlink(dirname(__FILE__) . '/../web/' . $posterPhoto->small_thumb);
            @unlink(dirname(__FILE__) . '/../web/' . $posterPhoto->medium_thumb);
            @unlink(dirname(__FILE__) . '/../web/' . $posterPhoto->original_image);
            $posterPhoto->delete();
        }        

        $path = dirname(__FILE__) . '/../web/' . $video->path;

        @unlink($path);

        $video->delete();

        return [
            'success' => true, 
            'message' => 'Video deleted'
        ];
    }

    public function inBlackList($otherUserID)
    {
        $user = $this->getUserBlacklists()->where(['user_id' => $otherUserID])->all();
        
        if (!empty($user)) {
            return true;
        }

        return false;
    }

    public function updateVideoDescription($videoID, $videoDescription, $videoStatus = 0, $publicVideo, $premiumVideo)
    {
        $video = Video::findOne(['user_id' => $this->id, 'id' => $videoID]);
        if (empty($video)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'Video not found'
            ];
        }
        $video->description = $videoDescription;
        $video->status      = $videoStatus;
        $video->public = $publicVideo;
        $video->premium = $premiumVideo;

        if ($video->premium == 1 && !empty($video->poster)) {
            $photoService = new PhotoService($this->id);
            $photo = $video->posterImage;            
            $watermarkFilePath = dirname(__FILE__) . '/../web/img/premium.png';
            $photoService->addWatermark($photo, $watermarkFilePath);
        }        

        if (!$video->save()) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Error video description save'
            ];
        }

        return [
            'success' => true, 
            'message' => 'Video updated'
        ];
    }

    public function getUsersFromAgency($type = 'girls')
    {
        $ids = [];
        $agency = $this->getAgency()->where(['!=', 'id', 1])->andWhere(['approve_status' => ApproveStatus::STATUS_APPROVED])->one();
        if (empty($agency)) {
            return $ids;
        }
        if($type == 'admin'){
            $agencyIDs = User::find()
                ->select(['id'])
                ->where(['agency_id' => $agency->id, 'user_type' => User::USER_ADMIN])
                ->all();
        }
        if($type == 'girls'){
            $agencyIDs = User::find()
                ->select(['id'])
                ->where(['agency_id' => $agency->id, 'user_type' => User::USER_FEMALE])
                ->all();
        }
        if ($type == 'men') {
            $agencyIDs = User::find()
                ->select(['id'])
                ->where(['agency_id' => $agency->id, 'user_type' => User::USER_MALE])
                ->all();
        }
        if($type == 'translators'){
            $agencyIDs = User::find()
                ->select(['id'])
                ->where(['agency_id' => $agency->id, 'user_type' => User::USER_INTERPRETER])
                ->all();
        }
        if($type == 'all') {
            $agencyIDs = User::find()
                ->select(['id'])
                ->where(['agency_id' => $agency->id])
                ->all();
        }

        if (empty($agencyIDs)) {
            return null;
        }


        foreach ($agencyIDs as $id) {
            $id = (object)$id;
            $ids[] = $id->id;
        }

        return $ids;
    }

    public function getMyChidrensIds($params = 'girls')
    {        

        //agency or agency admin
        if ($this->user_type == User::USER_AGENCY || $this->user_type == User::USER_ADMIN) {
            if ($this->status == User::STATUS_ACTIVE && $this->owner_status == User::STATUS_ACTIVE) {
                $userIDs = $this->getUsersFromAgency($params);

                return $userIDs;
            }            
        }

        //agency translate
        if ($this->user_type == User::USER_INTERPRETER) {
            if ($this->status == User::STATUS_ACTIVE && $this->owner_status == User::STATUS_ACTIVE) {
                $userIDs = $this->user_ids;
            }

            return $userIDs;
        }

        //admin
        if ($this->user_type == User::USER_SUPERADMIN) {
            return 'admin';
        }

        //male or female
        if ($this->user_type == User::USER_MALE || $this->user_type == User::USER_FEMALE) {

            return $this->id;
        }

    }

    public function getAffiliatesChildrenIds()
    {

        $ids = [];
        $userIDs = User::find()
                ->select(['id'])
                ->where(['referral_id' => $this->id])
                ->all();

        if(!empty($userIDs)){
            foreach ($userIDs as $id) {
                $id = (object)$id;
                $ids[] = $id->id;
            }
        }

        return $ids;

    }

    public function getUserTasks($userType, $searchParams, $limit, $offset, $taskID = null)
    {
        $dateFrom = isset($searchParams['dateFrom']) && strtotime($searchParams['dateFrom']) ? $searchParams['dateFrom'] : false;
        $dateTo   = isset($searchParams['dateTo']) && strtotime($searchParams['dateTo']) ? $searchParams['dateTo'] : false;
        $status   = isset($searchParams['status']) && is_numeric($searchParams['status']) ? $searchParams['status'] : false;

        //подзапрос на имя агентва
        $subQuery = (new Query)
            ->select('name, user.id')
            ->from([User::tableName()])
            ->leftJoin(Agency::tableName(), 'user.agency_id = agency.id')
            ->where(['user_type' => User::USER_AGENCY]);

        //подзапрос на новые комменты
        $task_comments_read = (new Query)
            ->select('comments.id as comments_id, comments.resource_id')
            ->from([Comments::tableName() . ' comments'])
            ->leftJoin(User::tableName() . ' users', 'users.id = comments.user_id')
            ->where(['resource_type_id' => 1, 'comments.date_read' => null]);
        if (in_array($userType, [User::USER_AGENCY, User::USER_INTERPRETER, User::USER_ADMIN])) {
            $task_comments_read->andWhere(['users.user_type' => User::USER_SUPERADMIN]);
        } elseif ($userType == User::USER_SUPERADMIN) {
            $task_comments_read->andWhere(['in', 'users.user_type', [User::USER_AGENCY, User::USER_INTERPRETER, User::USER_ADMIN]]);
        }
        $task_comments_read->groupBy('comments.resource_id');

        $task = (new Query)
            ->select('user_profile.name, task.*,task_type.type as status_name, task_comments_read.comments_id')
            ->from([Task::tableName()])
            ->leftJoin(TaskType::tableName(), 'task.status_id = task_type.id')
            ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = task.task_performer')
            ->leftJoin(['task_comments_read' => $task_comments_read], 'task_comments_read.resource_id = task.id');

        if($taskID !== null && is_numeric($taskID)) {
            $task->where(['task.id' => $taskID]);
                $task = $task->one();
            if(empty($task))
                return [
                    'success' => false, 
                    'message' => 'task not found'
                ];

            $comments = (new Comments())->getCommentsByTaskId($taskID);

            return ['success' => TRUE, 'code' => 1, 'message' => 'ok', 'task' => $task, 'comments' => $comments];
        }

        if (in_array($userType, [User::USER_AGENCY, User::USER_INTERPRETER, User::USER_ADMIN])) {
            $task->where(['task.task_performer' => $this->id]);
        }

        $task->orderBy('task.id DESC');

        if ($status !== false) {
            $task->andWhere(['task.status_id' => $status]);
        }

        if ($dateFrom) {
            $task->andWhere(['>=', 'task.approved_at', $dateFrom]);
        }
        if ($dateTo) {
            $task->andWhere(['<=', 'task.approved_at', $dateTo.' 23:59:59']);
        }

        $taskCount = $task->count();

        if ($limit !== null && is_numeric($limit)) {
            $task->limit($limit);
        }
        if ($offset !== null && is_numeric($offset)) {
            $task->offset($offset);
        }

        $result = $task->all();

        return ['success' => true, 'tasksList' => $result, 'count' => $taskCount];

    }

    public function addTask($userType, $agencyIDs, $dateTo, $description, $taskID = false, $status = false)
    {
        if (!is_array($agencyIDs) || empty($agencyIDs))
            return [
                'success' => false,
                'code' => 1, 
                'message' => 'Missing params'
            ];

        if ($taskID && is_numeric($taskID) && $status && is_numeric($status)) {

            $task = Task::findOne(['id'=>$taskID]);
            if (empty($task)) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'Task not found'
                ];
            }

            $task->status_id = $status;
            if ($userType == User::USER_SUPERADMIN) {
                $task->approved_at = ($dateTo) ? $dateTo." 23:59:59" : null;
                $task->description = ($description) ? $description : '';
            }

            if (!$task->save()) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => join(', ', $task->getFirstErrors())
                ];
            }

            return [
                'success' => TRUE,
                'code' => 1,
                'message' => 'all tasks add',
                'task_id' => $task->id
            ];

        }
        foreach ($agencyIDs as $agencyID) {
            $task = new Task();
            $task->task_creator = $this->id;
            $task->task_performer = $agencyID;
            $task->approved_at = $dateTo." 23:59:59";
            $task->description = $description;
            $task->status_id = 1;

            if(!$task->save()){
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'error during save'
                ];
            }
        }

        return [
            'success' => TRUE,
            'code' => 1,
            'message' => 'all tasks add',
            'task_id' => $task->id
        ];


    }

    public function deleteTask($taskID)
    {

        $task = Task::findOne(['id'=>$taskID]);
        if(empty($task))
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Task not found'
            ];

        $task->delete();

        Comments::deleteAll(['resource_id'=>$taskID]);

        return [
            'success' => true, 
            'message' => 'task delete'
        ];

    }

    public function updateWinks($otherUserID)
    {

        $winks = $this->getUserWinks()->where(['user_receiver' => $otherUserID])->one();
        if (empty($winks)) {
            $winks = new UserWink();
            $winks->user_creator       = $this->id;
            $winks->user_receiver = $otherUserID;

            if (!$winks->save()) {
                return [
                    'success' => false, 
                    'message' => 'user winks not save'
                ];
            }

            return ['success' => true, 'message' => 'Add user wink'];

        } else {
            
            $winks->created_at = date('Y-m-d H:i:s', time());
            $winks->viewed_at   = null;
            if (!$winks->save()) {
                return [
                    'success' => false, 
                    'message' => 'user winks not save'
                ];
            }

            return [
                'success' => true, 
                'message' => 'user winks update'
            ];
        }

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActions()
    {
        return $this->hasMany(Action::className(), ['action_creator' => 'id']);
    }

    public function getAvatar()
    {
        return $this->hasOne(Photo::className(), ['id' => 'avatar_photo_id']);
    }

    public function getProfile() 
    {
        return $this->hasOne(UserProfile::className(), ['id' => 'id']);
    }

    public function getPermissions()
    {
        return $this->hasOne(AdminPermissions::className(), ['id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAffiliates()
    {
        return $this->hasOne(Affiliates::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgencyRevards()
    {
        return $this->hasMany(AgencyRevard::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbums()
    {
        return $this->hasMany(Album::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeaturedLetters()
    {
        return $this->hasMany(FeaturedLetter::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterpreterProfile()
    {
        return $this->hasOne(Interpreter::className(), ['user_id' => 'id']);
    }

    public function getInterpreters()
    {
        return $this->hasMany(
            User::className(),
            ['id' => 'interpreter_id']
        )->viaTable(
            '{{%user_interpreter}}',
            ['user_id' => 'id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(
            User::className(), 
            ['id' => 'user_id']
        )->viaTable(
            '{{%user_interpreter}}',
            ['interpreter_id' => 'id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogTables()
    {
        return $this->hasMany(LogTable::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['message_creator' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMissingChats()
    {
        return $this->hasMany(MissingChat::className(), ['chat_creator' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMissingChats0()
    {
        return $this->hasMany(MissingChat::className(), ['chat_receiver' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotoLikes()
    {
        return $this->hasMany(PhotoLike::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPricelists()
    {
        return $this->hasMany(Pricelist::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatuses()
    {
        return $this->hasMany(Status::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['task_creator' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks0()
    {
        return $this->hasMany(Task::className(), ['task_performer' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkin()
    {
        return $this->hasOne(Skin::className(), ['id' => 'skin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserType()
    {
        return $this->hasOne(UserType::className(), ['id' => 'user_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAuthFacebook()
    {
        return $this->hasOne(UserAuthFacebook::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAuthGoogle()
    {
        return $this->hasOne(UserAuthGoogle::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserBlacklists()
    {
        return $this->hasMany(UserBlacklist::className(), ['blacklist_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserBlacklists0()
    {
        return $this->hasMany(UserBlacklist::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserFavoriteLists()
    {
        return $this->hasMany(UserFavoriteList::className(), ['favorite_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserFavoriteLists0()
    {
        return $this->hasMany(UserFavoriteList::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVideoSettings()
    {
        return $this->hasMany(UserVideoSetting::className(), ['video_creator' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVideoSettings0()
    {
        return $this->hasMany(UserVideoSetting::className(), ['video_receiver' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVideoTempSessions()
    {
        return $this->hasMany(UserVideoTempSession::className(), ['video_creator' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVideoTempSessions0()
    {
        return $this->hasMany(UserVideoTempSession::className(), ['video_receiver' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVisiters()
    {
        return $this->hasMany(UserVisiters::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserWinks()
    {
        return $this->hasMany(UserWink::className(), ['user_creator' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserWinks0()
    {
        return $this->hasMany(UserWink::className(), ['user_receiver' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideos()
    {
        return $this->hasMany(Video::className(), ['user_id' => 'id']);
    }

}
