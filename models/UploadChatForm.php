<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\web\BadRequestHttpException;

class UploadChatForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, PNG, jpg, JPG, jpeg, JPEG, gif, GIF','maxSize' => 1024*1024*2],
        ];
    }

    public function upload($chatId)
    {
        if ($this->validate()) {
            $new_name           = 'img'. md5($this->imageFile->baseName . time());
            $orign_dir          = 'uploads/chat/original/' . md5($chatId) . '/' . md5(date('Y-m-d', time())) . '/';
            $thumb_255x290      = 'uploads/chat/thumb_255x290/' . md5($chatId) . '/' . md5(date('Y-m-d', time())) . '/';
            $thumb_small        = 'uploads/chat/thumb_50x50/' . md5($chatId) . '/' . md5(date('Y-m-d', time())) . '/';
            $orign_name         = $orign_dir . $new_name . '.' . $this->imageFile->extension;
            $thumb_255x290_name = $thumb_255x290 . $new_name . '.' . $this->imageFile->extension;
            $thumb_small_name   = $thumb_small . $new_name . '.' . $this->imageFile->extension;

            if (!file_exists($orign_dir)) {
                mkdir($orign_dir, 0777, true);
            }
            if (!file_exists($thumb_255x290)) {
                mkdir($thumb_255x290, 0777, true);
            }
            if (!file_exists($thumb_small)) {
                mkdir($thumb_small, 0777, true);
            }

            $this->imageFile->saveAs($orign_name);

            $file  = Yii::getAlias('@app/web/' . $orign_name);
            $image = Yii::$app->image->load($file);
            $image->resize(255, 290, \yii\image\drivers\Image::WIDTH);
            $image->save($thumb_255x290_name);
            $image->resize(50, 50, \yii\image\drivers\Image::WIDTH);
            $image->save($thumb_small_name);

            return ['orign' => $orign_name, 'normal' => $thumb_255x290_name, 'small' => $thumb_small_name];
        } else {
            return false;            
        }
    }

}