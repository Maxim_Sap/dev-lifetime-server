<?php

namespace app\models\forms;
use Yii;
use yii\base\Model;
use app\models\User;
use app\models\UserProfile;
use app\models\Agency;
use app\models\Affiliates;
use app\models\AdminPermissions;
use app\models\LogTable;
use app\models\Interpreter;
use app\services\AlbumService;
use fzaninotto\Faker;
/**
 * Signup form
 */
class RegisterNewUserForm extends Model
{
    public $firstName;    
    public $email;
    public $password;
    public $userType;
    public $skinID;
    public $parentUserID;
    public $fakeMan;
    public $permissions;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstName'], 'trim'],
            ['firstName', 'required'],            
            [['firstName'], 'string', 'min' => 2, 'max' => 255],
            ['fakeMan', 'boolean'],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['permissions', 'safe'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            [['userType', 'parentUserID', 'skinID'], 'integer'],
            [['parentUserID', 'userType'], 'required'],            
            ['userType', 'in', 'range' => [User::USER_MALE, User::USER_FEMALE, User::USER_AGENCY, User::USER_INTERPRETER, User::USER_AFFILIATE, User::USER_ADMIN, User::USER_SUPERADMIN, User::USER_SITEADMIN], 'message' => 'Wrong user type'],
        ];
    }
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return [
                'success' => false,
                'errors' => $this->errors
            ];            
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            // for agency we have agency profile
            if ($this->userType == User::USER_AGENCY) {
                $agency = new Agency();
                $agency->name = $this->firstName;                
                $agency->contact_person = null;
                $agency->passport_number = null;
                $agency->office_address = null;
                $agency->office_phone   = null;
                $agency->card_number    = null;
                $agency->card_user_name = null;
                $agency->approve_status = 4;
                if (!$agency->save()) {
                    throw new \Exception('Can\'t be saved agency. Errors: '. join(', ', $agency->getFirstErrors()));
                }
            }
            $user = new User();
            $generator = \Faker\Factory::create();   
            $lastAddedId = User::find()->select('id')->where(['user_type' => $this->userType])->orderBy(['created_at' => SORT_DESC])->one();
            if ($this->userType == User::USER_MALE && $lastAddedId->id <= 1000001) {
                $user->id = 1000001 + $generator->numberBetween(10, 20);
            } elseif ($this->userType == User::USER_FEMALE && $lastAddedId->id <= 5000001) {
                $user->id = 5000001 + $generator->numberBetween(10, 20);
            } elseif ($this->userType == User::USER_MALE || $this->userType == User::USER_FEMALE) {
                $user->id = $lastAddedId->id + $generator->numberBetween(10, 20);
            } elseif (!empty($lastAddedId)) {
                $user->id = $lastAddedId->id + 1;
            } elseif (empty($lastAddedId)) {
                $lastAddedId = User::find()->select('id')->orderBy(['created_at' => SORT_DESC])->one();
                if (!empty($lastAddedId)) {
                    $user->id = $lastAddedId->id + 1;
                } else {
                    $user->id = 1;
                }                
            }          
            $user->email = $this->email;
            $user->user_type = $this->userType;
            $user->skin_id = $this->skinID;
            $model = Agency::find()->where(['approve_status' => 4, 'user.id' => $this->parentUserID])
                                   ->leftJoin(User::tableName(), 'user.agency_id = agency.id')
                                   ->one();
            if (empty($model)) {
                $model = Agency::find()->where(['approve_status' => 4, 'id' => $this->parentUserID])->one();
            }            
            if (!empty($model)) {
                $user->agency_id = $model->id;
            } else {
                $user->agency_id = 1;
            }
            if (!empty($agency)) {
                $user->agency_id = $agency->id;
            }
            if (($this->userType == User::USER_MALE && !$this->fakeMan)) {
                $user->agency_id = 1;
            }
            if($this->userType == User::USER_AFFILIATE || $this->userType == User::USER_SITEADMIN){
                $user->agency_id = 1;
            }
            $user->avatar_photo_id = 0;
            $user->fake = User::STATUS_FAKE_NO;
            if ($this->fakeMan) {
                $user->fake = User::STATUS_FAKE_YES;
            } else {
                $user->fake = User::STATUS_FAKE_NO;        
            }

            $user->token_end_date = time() + Yii::$app->params['token_time'];
            $user->cam_online = 0;
            $user->visible = User::STATUS_VISIBLE;

            if (!empty(Yii::$app->user) && Yii::$app->user->identity->user_type == User::USER_SUPERADMIN) {
                $user->approve_status_id = 4;
            } else {
                $user->approve_status_id = 1;
            }            
            $user->setPassword($this->password);
            $user->generateAuthKey();
            while (User::findOne(['id' => $user->id])) {
                $user->id = $user->id + $generator->numberBetween(10, 20);
            }
            if (!$user->save()) {
                throw new \Exception('Can\'t be saved user. Errors: '. join(', ', $user->getFirstErrors()));
            }

            // for agency we have don't need a user profile, but need an agency profile
            if (!in_array($this->userType,[User::USER_AGENCY,User::USER_AFFILIATE])) {
                $userProfile = new UserProfile();
                $userProfile->id = $user->id;       
                $userProfile->first_name = $this->firstName;

                if (!$userProfile->save()) {
                    throw new \Exception('Can\'t be saved user profile. Errors: '. join(', ', $userProfile->getFirstErrors()));
                }
            }

            // for interpreter we need not only user profile, but also interpreter profile
            if ($this->userType == User::USER_INTERPRETER) {
                $interpreterProfile = new Interpreter();
                $interpreterProfile->id = $user->id;
                $interpreterProfile->interpreter_type_id = 1;
                $interpreterProfile->deposit = 0;
                $interpreterProfile->amount = 0;
                $interpreterProfile->fee = 0;
                if (!$interpreterProfile->save()) {
                    throw new \Exception('Can\'t be saved interpreter profile. Errors: '. join(', ', $interpreterProfile->getFirstErrors()));
                }
            }

            // for interpreter we need not only user profile, but also interpreter profile
            if ($this->userType == User::USER_AFFILIATE) {
                $Affiliates = new Affiliates();
                $Affiliates->id = $user->id;
                $Affiliates->name = $this->firstName;
                $Affiliates->referral_link = Yii::$app->security->generateRandomString(10);
                if (!$Affiliates->save()) {
                    throw new \Exception('Can\'t be saved Affiliates profile. Errors: '. join(', ', $Affiliates->getFirstErrors()));
                }
            }

            if ($this->userType == User::USER_ADMIN) {
                $adminPermissions = new AdminPermissions();
                $adminPermissions->id = $user->id;
                $newPermissions = Yii::$app->params['admin_permissions'];
                if (!empty($this->permissions) && json_decode($this->permissions)) {
                    $modifiedPermissions = [];
                    $permissions = json_decode($this->permissions);
                    foreach ($permissions as $access => $permission) {
                        foreach ($newPermissions as $newAccess => $newPermission) {
                            if ($access == $newAccess) {
                                $modifiedPermissions[$newAccess] = ($permission == AdminPermissions::ALLOW) ? AdminPermissions::ALLOW : AdminPermissions::DENY;
                            }
                        }
                    }                                        
                } else {
                    $modifiedPermissions = [];
                    foreach ($newPermissions as $newAccess => $newPermission) {
                        $modifiedPermissions[$newAccess] = $newPermission['value'];
                    }
                }
                $adminPermissions->permissions = json_encode($modifiedPermissions);
                if (!$adminPermissions->save()) {
                    throw new \Exception('Can\'t be saved admin permissions. Errors: '. join(', ', $adminPermissions->getFirstErrors()));
                }
            }
            if ($this->userType == User::USER_SITEADMIN) {
                $adminPermissions = new AdminPermissions();
                $adminPermissions->id = $user->id;
                $newPermissions = Yii::$app->params['siteadmin_permissions'];
                if (!empty($this->permissions) && json_decode($this->permissions)) {
                    $modifiedPermissions = [];
                    $permissions = json_decode($this->permissions);
                    foreach ($permissions as $access => $permission) {
                        foreach ($newPermissions as $newAccess => $newPermission) {
                            if ($access == $newAccess) {
                                $modifiedPermissions[$newAccess] = ($permission == AdminPermissions::ALLOW) ? AdminPermissions::ALLOW : AdminPermissions::DENY;
                            }
                        }
                    }                                        
                } else {
                    $modifiedPermissions = [];
                    foreach ($newPermissions as $newAccess => $newPermission) {
                        $modifiedPermissions[$newAccess] = $newPermission['value'];
                    }
                }
                $adminPermissions->permissions = json_encode($modifiedPermissions);
                if (!$adminPermissions->save()) {
                    throw new \Exception('Can\'t be saved admin permissions. Errors: '. join(', ', $adminPermissions->getFirstErrors()));
                }
            }


/*            if ($this->userType == User::USER_MALE) {
                $credit = new Credit();
                $credit->id = $user->id;
                $credit->amount = 0.00;                  

                if (!$credit->save()) {
                    throw new \Exception('Can\'t be saved user profile. Errors: '. join(', ', $credit->getFirstErrors()));
                }
            }*/

            // create a public album for user
            $albumService = new AlbumService($user->id);
            $albumService->Create(1, 'Public Album', 'Public Album');

            $transaction->commit();
            return [
                'success' => true,
                'user' => $user
            ];
        } catch (\Exception $e) {
            $transaction->rollBack();
            return [
                'success' => false,
                'errors' => $e->getMessage()
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            return [
                'success' => false,
                'errors' => $e->getMessage()
            ];
        }        
    }
}