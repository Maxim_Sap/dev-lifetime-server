<?php

namespace app\models\forms;
use app\models\Affiliates;
use app\services\AlbumService;
use Yii;
use yii\base\Model;
use app\models\User;
use app\models\UserProfile;
use fzaninotto\Faker;
/**
 * Signup form
 */
class SignupForm extends Model
{    
    public $email;
    public $password;
    //public $userTypeID;
    public $skinID;
    public $agree;
    public $referralLink;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            [['skinID'], 'integer'],
            ['referralLink', 'string','min'=>6],
/*            ['userTypeID', 'in', 'range' => [User::USER_MALE, User::USER_FEMALE], 'message' => 'Please select your gender: Man or Woman'],*/
            ['agree', 'required', 'message' => 'You must agree with License agreement'],
            ['agree', 'compare', 'compareValue' => 1, 'message' => 'You must agree with License agreement']
        ];
    }
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return [
                'success' => false,
                'errors' => $this->errors
            ];
        }

        $referral_id = null;
        if($this->referralLink != ''){
            $AffiliatesModel = Affiliates::findOne(['referral_link'=>$this->referralLink]);
            if(!empty($AffiliatesModel)){
                $referral_id = $AffiliatesModel->id;
            }
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $lastManId = User::find()->select('id')->where(['user_type' => User::USER_MALE])->orderBy(['created_at' => SORT_DESC])->one();
            $generator = \Faker\Factory::create();    
            $user = new User();        
            if ($lastManId->id < 1000001) {
                $user->id = 1000001 + $generator->numberBetween(10, 20);
            } else {
                $user->id = $lastManId->id + $generator->numberBetween(10, 20);
            }        	
	        $user->email = $this->email;
	        $user->user_type = User::USER_MALE;
	        $user->skin_id = $this->skinID;
	        $user->agency_id = 1;
	        $user->referral_id = $referral_id;
	        $user->avatar_photo_id = 0;
	        $user->status = User::STATUS_NO_EMAIL_CONFIRM;        
	        $user->token_end_date = time() + Yii::$app->params['token_time'];
	        $user->visible = User::STATUS_VISIBLE;
	        $user->fake = User::STATUS_FAKE_NO;
	        $user->cam_online = 0;
	        $user->approve_status_id = 1;
	        $user->setPassword($this->password);
	        $user->generateAuthKey();

            while (User::findOne(['id' => $user->id])) {
                $user->id = $user->id + $generator->numberBetween(10, 20);
            }
	        if (!$user->save()) {
                throw new \Exception('Can\'t be saved user. Errors: '. join(', ', $user->getFirstErrors()));
            }

            $userProfile = new UserProfile();
	        $userProfile->id = $user->id;       	        

	        if (!$userProfile->save()) {
                throw new \Exception('Can\'t be saved user profile. Errors: '. join(', ', $userProfile->getFirstErrors()));
            }

            // create a public album for user
            $albumService = new AlbumService($user->id);
            $albumService->Create(1, 'Public Album', 'Public Album');

/*            $credit = new Credit();
            $credit->id = $user->id;
            $credit->amount = 0.00;                  

            if (!$credit->save()) {
                throw new \Exception('Can\'t be saved user profile. Errors: '. join(', ', $credit->getFirstErrors()));
            }*/

            $transaction->commit();
            return [
                'success' => true,
                'user' => $user
            ];

        } catch (\Exception $e) {
            $transaction->rollBack();
            return [
                'success' => false,
                'errors' => $e->getMessage()
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            return [
                'success' => false,
                'errors' => $e->getMessage()
            ];
        }     
        
    }
}