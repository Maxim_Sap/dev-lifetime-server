<?php 
namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\User;

class ChangePasswordForm extends Model
{	
	public $otherUserID;
    public $oldPassword;
    public $newPassword;
    public $confirmPassword;

    private $_user;
    
    public function rules() 
    {
        return [        	
            [['oldPassword','oldPassword','oldPassword', 'otherUserID'], 'required'],
            [['otherUserID'], 'integer'],
            ['oldPassword','findPasswords', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['newPassword', 'confirmPassword'], 'string', 'min' => 6],
            ['confirmPassword','compare','compareAttribute'=>'newPassword'],
        ];
    }
    
    public function findPasswords($attribute, $params, $validator)
    {
        $this->_user = User::find()->where(['id'=> $this->otherUserID])->andWhere(['not in', 'status', [User::STATUS_DELETED, User::STATUS_NO_ACTIVE, User::STATUS_NO_EMAIL_CONFIRM]])->one();
        if (empty($this->_user)) {
            $this->addError($attribute,'User doesn\'t exist or not active');
        }
        $userEditor = Yii::$app->user->identity;
        if (empty($userEditor) || !$userEditor->validatePassword($this->oldPassword))
            $this->addError($attribute,'Old password is incorrect');
    }
    
    public function attributeLabels()
    {
        return [
            'oldPassword'=>'Old Password',
            'newPassword'=>'New Password',
            'confirmPassword'=>'Confirm New Password',
        ];
    }

    public function saveNewPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->newPassword);        
        $user->access_token = $user->getJWT();
        return $user->save(false);
    }

}