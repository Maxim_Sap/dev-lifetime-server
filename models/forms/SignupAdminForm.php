<?php

namespace app\models\forms;
use Yii;
use yii\base\Model;
use app\models\User;
use app\models\UserProfile;
/**
 * Signup form
 */
class SignupAdminForm extends Model
{
    public $firstName;
    public $lastName;    
    public $email;
    public $password;
    public $userTypeID;
    public $skinID;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName'], 'trim'],
            ['firstName', 'required'],            
            [['firstName', 'lastName'], 'string', 'min' => 2, 'max' => 255],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            [['userTypeID', 'skinID'], 'integer'],
            ['userTypeID', 'in', 'range' => [User::USER_AGENCY, User::USER_INTERPRETER, User::USER_AFFILIATE], 'message' => 'Wrong user type'],
        ];
    }
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return [
                'success' => false,
                'errors' => $this->errors
            ];
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user = new User();
            $user->email = $this->email;
            $user->user_type = 3;
            $user->skin_id = $this->skinID;
            $user->agency_id = 1;
            $user->avatar_photo_id = 0;
            $user->status = User::STATUS_NO_ACTIVE;        
            $user->token_end_date = time() + Yii::$app->params['token_time'];
            $user->visible = User::STATUS_VISIBLE;
            $user->fake = User::STATUS_FAKE_NO;
            $user->cam_online = 0;
            $user->approve_status_id = 1;
            $user->setPassword($this->password);
            $user->generateAuthKey();

            if (!$user->save()) {
                throw new \Exception('Can\'t be saved user. Errors: '. join(', ', $user->getFirstErrors()));
            }

            $userProfile = new UserProfile();
            $userProfile->id = $user->id;       
            $userProfile->first_name = $this->firstName;
            $userProfile->last_name = (isset($this->lastName)) ? $this->lastName : null;

            if (!$userProfile->save()) {
                throw new \Exception('Can\'t be saved user profile. Errors: '. join(', ', $userProfile->getFirstErrors()));
            }

            $transaction->commit();
            return [
                'success' => true,
                'user' => $user
            ];
        } catch (\Exception $e) {
            $transaction->rollBack();
            return [
                'success' => false,
                'errors' => $e->getMessage()
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            return [
                'success' => false,
                'errors' => $e->getMessage()
            ];
        }     
  
    }
}