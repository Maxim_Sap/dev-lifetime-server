<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mailing_session".
 *
 * @property integer $id
 * @property integer $sended_messages
 * @property integer $total_messages
 * @property integer $exclude_blacklist
 * @property integer $paused
 * @property integer $was_finished
 * @property string $started_at
 * @property string $ended_at
 */
class MailingSession extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    const PAUSED = 1;
    const NOT_PAUSED = 0;
    const WAS_FINISHED = 1;
    const WASNT_FINISHED = 0;
    const CREATED_FOR_CHAT = 0;
    const CREATED_FOR_MESSAGE = 1;


    public static function tableName()
    {
        return 'mailing_session';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator'], 'required'],
            [['creator', 'template_id', 'created_for', 'sended_messages', 'dublicated_messages', 'total_messages', 'exclude_blacklist', 'paused', 'was_finished'], 'integer'],
            ['created_for', 'in', 'range' => [self::CREATED_FOR_CHAT, self::CREATED_FOR_MESSAGE]],
            [['started_at', 'ended_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creator' => 'Creator',
            'sended_messages' => 'Sended Messages',
            'dublicated_messages' => 'Dublicated Messages',
            'total_messages' => 'Total Messages',
            'exclude_blacklist' => 'Exclude Blacklist',
            'paused' => 'Paused',
            'was_finished' => 'Was Finished',
            'started_at' => 'Started At',
            'ended_at' => 'Ended At',
        ];
    }

    public function getMailingQueues()
    {
        return $this->hasMany(MailingQueue::className(), ['mailing_session_id' => 'id']);
    }

}
