<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "mailing_template".
 *
 * @property integer $id
 * @property integer $template_creator
 * @property integer $agency_id
 * @property string $title
 * @property string $body
 * @property integer $created_for
 * @property integer $status
 * @property string $created_at
 */
class MailingTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const CREATED_FOR_CHAT = 0;
    const CREATED_FOR_MESSAGE = 1;

    public static function tableName()
    {
        return 'mailing_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template_creator', 'agency_id', 'title', 'body'], 'required'],
            [['template_creator', 'agency_id', 'created_for', 'status'], 'integer'],
            [['body'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'template_creator' => 'Template Creator',
            'agency_id' => 'Agency ID',
            'title' => 'Title',
            'body' => 'Body',
            'created_for' => 'Created For',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {     

            if (!$insert) {
                $this->template_creator = $this->oldAttributes['template_creator'];
                $this->agency_id = $this->oldAttributes['agency_id'];

                $this->title = Html::encode(HtmlPurifier::process($this->title));
                $this->body = Html::encode(HtmlPurifier::process($this->body));

                $this->created_for = $this->oldAttributes['created_for'];
                $this->created_at = $this->oldAttributes['created_at'];

            } else {

                $this->title = Html::encode(HtmlPurifier::process($this->title));
                $this->body = Html::encode(HtmlPurifier::process($this->body));

            }            

            return true;

        }

        return false;
    }

    public function fields()
    {
        $fieldsList = [
            // field name is the same as the attribute name
            'id',            
            'template_creator',
            'title' => function($model) {
                return Html::decode($model->title);
            },
            'body' => function($model) {
                return Html::decode($model->body);
            },      
            'status',           
        ];
        $user = \Yii::$app->user->identity;
        if (!empty($user) && in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN, User::USER_SUPERADMIN])) {
            $fieldsList[] = 'agency_id';
            $fieldsList[] = 'created_at';            
            $fieldsList[] = 'created_for';            
        }
        return $fieldsList;     
    }

}
