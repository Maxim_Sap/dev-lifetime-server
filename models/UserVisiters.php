<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_visiters".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $visited_user_id
 * @property string $visit_datetime
 *
 * @property User $user
 */
class UserVisiters extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_visiters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'visited_user_id'], 'required'],
            [['user_id', 'visited_user_id'], 'integer'],
            [['visit_datetime'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'visited_user_id' => 'Visited User ID',
            'visit_datetime' => 'Visit Datetime',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
