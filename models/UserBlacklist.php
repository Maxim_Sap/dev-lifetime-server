<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_blacklist".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $blacklist_user_id
 *
 * @property User $blacklistUser
 * @property User $user
 */
class UserBlacklist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_blacklist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'blacklist_user_id'], 'required'],
            [['user_id', 'blacklist_user_id'], 'integer'],
            [['blacklist_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['blacklist_user_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'blacklist_user_id' => 'Blacklist User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlacklistUser()
    {
        return $this->hasOne(User::className(), ['id' => 'blacklist_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
