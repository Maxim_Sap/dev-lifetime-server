<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "interpreter".
 *
 * @property integer $id
 * @property integer $interpreter_type_id
 * @property integer $deposit
 * @property string $amount
 * @property string $fee
 *
 * @property InterpreterType $interpreterType
 * @property User $id0
 */
class Interpreter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'interpreter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['interpreter_type_id', 'deposit'], 'integer'],
            [['amount', 'fee'], 'number'],
            [['interpreter_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => InterpreterType::className(), 'targetAttribute' => ['interpreter_type_id' => 'id']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'interpreter_type_id' => 'Interpreter Type ID',
            'deposit' => 'Deposit',
            'amount' => 'Amount',
            'fee' => 'Fee',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterpreterType()
    {
        return $this->hasOne(InterpreterType::className(), ['id' => 'interpreter_type_id']);
    }


}
