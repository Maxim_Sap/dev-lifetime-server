<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property string $name
 * @property integer $proxy
 * @property integer $original
 *
 * @property Location[] $locations
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proxy', 'original'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], function ($attribute) {
                $this->$attribute = Html::encode(strip_tags(HtmlPurifier::process($this->$attribute)));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'proxy' => 'Proxy',
            'original' => 'Original',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocations()
    {
        return $this->hasMany(Location::className(), ['city_id' => 'id']);
    }
}
