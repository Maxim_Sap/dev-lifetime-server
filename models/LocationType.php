<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "location_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Location[] $locations
 */
class LocationType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], function ($attribute) {
                $this->$attribute = Html::encode(strip_tags(HtmlPurifier::process($this->$attribute)));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocations()
    {
        return $this->hasMany(Location::className(), ['location_type_id' => 'id']);
    }
}
