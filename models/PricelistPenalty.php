<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pricelist_penalty".
 *
 * @property integer $id
 * @property integer $type_id
 * @property double $price
 */
class PricelistPenalty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pricelist_penalty';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'price'], 'required'],
            [['type_id'], 'integer'],
            [['price'], 'number'],
            [['type_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'price' => 'Price',
        ];
    }
}
