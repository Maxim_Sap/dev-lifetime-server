<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transfer_type".
 *
 * @property integer $id
 * @property string $code
 * @property string $title
 *
 * @property UserTransfer[] $userTransfers
 */
class TransferType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transfer_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'title'], 'required'],
            [['code', 'title'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTransfers()
    {
        return $this->hasMany(UserTransfer::className(), ['transfer_type_id' => 'id']);
    }
}
