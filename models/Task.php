<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property integer $task_creator
 * @property integer $task_performer
 * @property string $description
 * @property string $created_at
 * @property string $approved_at
 * @property integer $status_id
 *
 * @property TaskType $status
 * @property User $taskCreator
 * @property User $taskPerformer
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_creator', 'task_performer'], 'required'],
            [['task_creator', 'task_performer', 'status_id'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'approved_at'], 'safe'],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskType::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['task_creator'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['task_creator' => 'id']],
            [['task_performer'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['task_performer' => 'id']],
            [['description'], function ($attribute) {
                $this->$attribute = Html::encode(HtmlPurifier::process($this->$attribute));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_creator' => 'Task Creator',
            'task_performer' => 'Task Performer',
            'description' => 'Description',
            'created_at' => 'Created At',
            'approved_at' => 'Approved At',
            'status_id' => 'Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(TaskType::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'task_creator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskPerformer()
    {
        return $this->hasOne(User::className(), ['id' => 'task_performer']);
    }
}
