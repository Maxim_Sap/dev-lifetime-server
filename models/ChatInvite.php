<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chat_invite".
 *
 * @property integer $id
 * @property integer $invite_sender
 * @property integer $invite_receiver
 * @property string $message
 * @property integer $show
 * @property string $created_at
 */
class ChatInvite extends \yii\db\ActiveRecord
{
    const SHOW_INVITE = 1;
    const HIDE_INVITE = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat_invite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invite_sender', 'invite_receiver'], 'required'],
            [['invite_sender', 'invite_receiver', 'show'], 'integer'],
            [['message'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invite_sender' => 'Invite Sender',
            'invite_receiver' => 'Invite Receiver',
            'message' => 'Message',
            'show' => 'Show',
            'created_at' => 'Created At',
        ];
    }

    public function getInviteSender()
    {
        return $this->hasOne(User::className(), ['id' => 'invite_sender']);
    }

    public function getInviteReceiver()
    {
        return $this->hasOne(User::className(), ['id' => 'invite_receiver']);
    }    
}
