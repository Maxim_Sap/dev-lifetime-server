<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "user_profile".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $m_phone
 * @property string $payment_key
 * @property string $birthday
 * @property integer $sex
 * @property integer $religion
 * @property integer $marital
 * @property integer $kids
 * @property integer $height
 * @property integer $weight
 * @property integer $eyes
 * @property integer $hair
 * @property integer $smoking
 * @property integer $alcohol
 * @property string $address
 * @property string $passport
 * @property integer $education
 * @property string $occupation
 * @property integer $english_level
 * @property integer $looking_age_from
 * @property integer $looking_age_to
 * @property string $about_me
 * @property string $hobbies
 * @property string $my_ideal
 * @property string $other_language
 *
 * @property User $id0
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['birthday', 'deactivation_time'], 'safe'],
            [['sex', 'religion', 'marital', 'eyes', 'hair', 'smoking', 'ethnos', 'alcohol', 'education', 'english_level', 'looking_age_from', 'looking_age_to'], 'integer'],
            [['about_me', 'hobbies', 'my_ideal'], 'string'],
            [['first_name', 'last_name', 'address', 'passport', 'occupation', 'other_language','payment_key', 'deactivation_reason', ], 'string', 'max' => 255],
            [['phone','m_phone'], 'string', 'max' => 50],
            [['eyes', 'hair', 'education', 'marital', 'religion', 'physique'], 'in', 'range' => [1, 2, 3, 4, 5, 6, 7, 8, 9], 'message' => 'This parameter can\'t take this value'],
            [['smoking', 'alcohol', 'english_level'], 'in', 'range' => [1,2,3,4], 'message' => 'This parameter can\'t take this value'],
            [['kids', 'deactivation_by_agency'], 'integer', 'min' => 0, 'max' => 5],
            [['height', 'weight'], 'integer', 'min' => 30, 'max' => 250],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id' => 'id']],
            [['about_me', 'hobbies', 'my_ideal', 'first_name', 
              'last_name', 'address', 'passport', 'occupation', 
              'other_language','payment_key', 'deactivation_reason',
              'phone','m_phone'], function ($attribute) {
                $this->$attribute = Html::encode(strip_tags(HtmlPurifier::process($this->$attribute)));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone' => 'Phone',
            'm_phone' => 'Mob Phone',
            'payment_key' => 'Payment Key',
            'birthday' => 'Birthday',
            'sex' => 'Sex',
            'religion' => 'Religion',
            'marital' => 'Marital',
            'kids' => 'Kids',
            'height' => 'Height',
            'weight' => 'Weight',
            'eyes' => 'Eyes',
            'hair' => 'Hair',
            'smoking' => 'Smoking',
            'alcohol' => 'Alcohol',
            'address' => 'Address',
            'passport' => 'Passport',
            'education' => 'Education',
            'occupation' => 'Occupation',
            'english_level' => 'English Level',
            'looking_age_from' => 'Looking Age From',
            'looking_age_to' => 'Looking Age To',
            'about_me' => 'About Me',
            'hobbies' => 'Hobbies',
            'my_ideal' => 'My Ideal',
            'other_language' => 'Other Language',
            'deactivation_reason' => 'Deactivation text',
            'deactivation_time' => 'Deactivation time',
        ];
    }

    public function fields()
    {
        $fieldsList = [
            'id',
            'first_name',
            'last_name',
            'phone',
            'm_phone',
            'birthday',
            'sex',
            'religion',
            'marital',
            'kids',
            'height',
            'weight',
            'eyes',
            'hair',
            'smoking',
            'alcohol',
            'physique',
            'ethnos',
            'address',
            'passport',
            'education',
            'occupation',
            'english_level',
            'looking_age_from',
            'looking_age_to',
            'about_me',
            'hobbies',
            'my_ideal',
            'other_language',
        ];
        return $fieldsList;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEyesColor()
    {
        return $this->hasOne(EyesColor::className(), ['id' => 'eyes']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHairColor()
    {
        return $this->hasOne(HairColor::className(), ['id' => 'hair']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhysique0()
    {
        return $this->hasOne(Physique::className(), ['id' => 'physique']);
    }

}
