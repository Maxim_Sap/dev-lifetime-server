<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "affiliates".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $referral_link
 * @property string  $m_phone
 */
class Affiliates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'affiliates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'referral_link'], 'required'],
            [['name', 'referral_link', 'm_phone'], 'string', 'max' => 255],
            [['name', 'referral_link', 'm_phone'], function ($attribute) {
                $this->$attribute = Html::encode(strip_tags(HtmlPurifier::process($this->$attribute)));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'name'          => 'Name',
            'referral_link' => 'Referral Link',
            'm_phone'       => 'M Phone',
        ];
    }
}
