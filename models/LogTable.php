<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use app\models\LogSearch;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "log_table".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $description
 * @property string $created_at
 *
 * @property User $user
 */
class LogTable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_table';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
            [['created_at'], 'safe'],
            [['description'], 'string', 'max' => 255],  
            [['description'], function ($attribute) {
                $this->$attribute = Html::encode(strip_tags(HtmlPurifier::process($this->$attribute)));
            }],          
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',            
            'description' => 'Description',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getUserType()
    {
        return $this->hasOne(UserType::className(), ['id' => 'user_type']);
    }

    public static function addLog($userID, $userType, $description) {
        $model = new LogTable(); 
        $model->user_id = $userID;
        $model->user_type = $userType;     
        $model->description = $description;
        $model->ip = Yii::$app->getRequest()->getUserIP();
        return $model->save();
    }

    public static function getLog($agencyUsersIds = null, 
        $otherUserID = null, $searchParams, $page = null, $limit = null)
    {
        $searchModel  = new LogSearch();
        $dataProvider = $searchModel->search($agencyUsersIds, $otherUserID, $searchParams, $page, $limit);     

        return $dataProvider;   
    }

}
