<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Gift;

/**
 * GiftSearch represents the model behind the search form about `app\models\Gift`.
 */
class GiftSearch extends Gift
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['id', 'type_id'], 'each', 'rule' => ['integer']],
            [['name', 'description'], 'safe'],
            [['price','agency_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Gift::find();
        //return $params['searchParams']['giftsCategory'];
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'page' => $params['page'],
                'pageSize' => $params['limit'],                
            ],
            'sort' => [
                'defaultOrder' => [
                    'price' => (!empty($params['searchParams']['sort']) && $params['searchParams']['sort']=='asc') ? SORT_ASC : SORT_DESC,
                    'id' => SORT_ASC, 
                ]
            ],        
        ]);

        $fields = [
            self::formName() => [
                'id' => $params['giftsIDs'],
                'type_id' => (!empty($params['searchParams']['giftsCategory'])) ? $params['searchParams']['giftsCategory'] : [],
            ]
        ];
        if (!empty($params['status'])) {
            $fields[self::formName()]['status'] = $params['status'];
        }
        
        $this->load($fields);           

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'agency_price' => $this->agency_price,
            'status' => $this->status,
            'type_id' => $this->type_id
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);           

        return $dataProvider;
    }
}