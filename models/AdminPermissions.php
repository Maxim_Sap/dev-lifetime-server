<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "admin_permissions".
 *
 * @property integer $id
 * @property string $permissions
 */
class AdminPermissions extends \yii\db\ActiveRecord
{
	const ALLOW = 1;
	const DENY = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_permissions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['permissions'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'permissions' => 'Permissions',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    public function canAccess($category)
    {
        if (!empty($this->permissions) && json_decode($this->permissions)) {
            $permissions = json_decode($this->permissions);
            foreach ($permissions as $access => $permission) {
                if ($access == $category && $permission == self::ALLOW) {
                    return true;
                }
            }
        }
        return false;
    }

    public function cantAccess($category)
    {
        return !$this->canAccess($category);
    }
}
