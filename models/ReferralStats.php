<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "referral_stats".
 *
 * @property integer $id
 * @property string $referral_link
 * @property string $ip
 * @property integer $type
 * @property string $date_create
 */
class ReferralStats extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'referral_stats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['referral_link', 'ip', 'type'], 'required'],
            [['type'], 'integer'],
            [['date_create'], 'safe'],
            [['referral_link', 'ip'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'referral_link' => 'Referral Link',
            'ip' => 'Ip',
            'type' => 'Type',
            'date_create' => 'Date Create',
        ];
    }

    public static function addVisitor($referralLink, $ip, $type){
        $model = new ReferralStats();
        $model->referral_link = $referralLink;
        $model->ip = $ip;
        $model->type = $type;
        return $model->save();
    }

    public function getStats($other_user_id, $date_from, $date_to, $limit = null, $offset = null){

        //$model = self::find();

        //получаем количество переходов
        /*        $subQuery1 = (new Query())
                    ->select(['referral_link','date_create','type'])
                    ->from([self::tableName()." visitor"])
                    ->where(['type'=> 1])
                    ->andWhere(['>=', 'date_create', $date_from])
                    ->andWhere(['<=', 'date_create', $date_to])
                    ->groupBy('referral_link')
                ;
                //получаем количество регистраций
                $subQuery2 = (new Query())
                    ->select(['referral_link','date_create','type'])
                    ->from([self::tableName()." signup"])
                    ->where(['type'=> 2])
                    ->andWhere(['>=', 'date_create', $date_from])
                    ->andWhere(['<=', 'date_create', $date_to])
                    ->groupBy('referral_link')
                ;

                $query = (new Query())
                    ->select(['count(visitor.referral_link) as visit_count',
                              'count(signup.referral_link) as signup_count',
                              'referral.referral_link',
                              'referral.date_create'])
                    ->from([self::tableName() . ' referral'])
                    ->leftJoin(['visitor' => $subQuery1], 'visitor.referral_link = referral.referral_link')
                    ->leftJoin(['signup' => $subQuery2], 'signup.referral_link = referral.referral_link')
                    //->where('referral.type IS NOT NULL')
                    //->groupBy('referral.referral_link')
                    ->orderBy('referral.date_create ASC')
                    ->groupBy(['referral.referral_link','DAY(referral.date_create)'])
                ;
        */

        //подзапрос на имя пользователя
        //$personalSubQuery = (new Query)
            //->select('first_name, last_name, id')
            //->from([UserProfile::tableName()]);

        $affiliatesSubQuery = (new Query)
            ->select('id,name,referral_link')
            ->from([Affiliates::tableName()]);

        $query = (new Query())
            ->select(['referral.*','affiliates.id as affiliates_id','affiliates.name'])
            ->from([self::tableName() . ' referral'])
            ->leftJoin(['affiliates' => $affiliatesSubQuery], 'affiliates.referral_link = referral.referral_link')
            //->leftJoin(['pers' => $personalSubQuery], 'pers.id = affiliates.id');
        ;

        if(!empty($other_user_id)){
            $query->andWhere(['affiliates.id'=>$other_user_id]);
        }
        if(!empty($date_from)){
            $query->andWhere(['>=', 'referral.date_create', $date_from]);
        }
        if(!empty($date_to)){
            $query->andWhere(['<=', 'referral.date_create', $date_to." 23:59:59"]);
        }

        $ressult = $query->all();

        //$ress = $model->asArray()->all();
        //return ['success' => FALSE, 'code' => 2, 'message' => 'Access denied'];
        return ['success' => TRUE, 'stats'=>$ressult];
    }
}
