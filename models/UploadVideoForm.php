<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadVideoForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $videoFile;

    public function rules()
    {
        return [            
            [['videoFile'], 'file', 'skipOnEmpty' => false, 'mimeTypes' => 'video/*', 'extensions' => ['avi', 'AVI', 'mp4', 'MP4', 'FLV', 'flv'],'maxSize' => 20*1024*1024],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $newName           = 'video'. md5($this->videoFile->baseName . time());
            $orign_dir          = 'uploads/video/';
            $orign_name         = $orign_dir . $newName . '.' . $this->videoFile->extension;

            if (!file_exists($orign_dir)) {
                mkdir($orign_dir, 0777, true);
            }

            $this->videoFile->saveAs($orign_name);
            
            return ['orign' => $orign_name];
        } else {
            return ['errors' => $this->errors];
            return false;
        }
    }
}