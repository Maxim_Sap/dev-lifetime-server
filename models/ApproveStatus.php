<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "approve_status".
 *
 * @property integer $id
 * @property string $description
 */
class ApproveStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    const STATUS_NOT_APPROVED = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_DECLINED = 3;
    const STATUS_APPROVED = 4;
    
    public static function tableName()
    {
        return 'approve_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string', 'max' => 250],
            [['description'], function ($attribute) {
                $this->$attribute = Html::encode(strip_tags(HtmlPurifier::process($this->$attribute)));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
        ];
    }
}
