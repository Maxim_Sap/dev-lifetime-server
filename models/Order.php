<?php

namespace app\models;

use app\services\BillingService;
use app\services\PaymentsService;
use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer     $id
 * @property integer     $action_id
 * @property integer     $gift_id
 * @property integer     $quantity
 * @property string      $created_at
 * @property string      $updated_at
 * @property integer     $order_status_id
 *
 * @property Action      $action
 * @property Gift        $gift
 * @property OrderStatus $orderStatus
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'gift_id', 'quantity', 'order_status_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['action_id'], 'exist', 'skipOnError' => TRUE, 'targetClass' => Action::className(), 'targetAttribute' => ['action_id' => 'id']],
            [['gift_id'], 'exist', 'skipOnError' => TRUE, 'targetClass' => Gift::className(), 'targetAttribute' => ['gift_id' => 'id']],
            [['order_status_id'], 'exist', 'skipOnError' => TRUE, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['order_status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'action_id'       => 'Action ID',
            'gift_id'         => 'Gift ID',
            'quantity'        => 'Quantity',
            'created_at'      => 'Created At',
            'updated_at'      => 'Updated At',
            'order_status_id' => 'Order Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGift()
    {
        return $this->hasOne(Gift::className(), ['id' => 'gift_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'order_status_id']);
    }

    public function addOrder($skinID, $userFrom, $userTo, $giftArray)
    {

        $gift_price    = 0;
        $gift_mass_new = array();
        foreach ($giftArray as $one) {
            $gift_model = Gift::findOne(['id' => (int)$one['giftID']]);
            if (empty($gift_model)) {
                return ['success' => FALSE, 'code' => 2, 'message' => 'Gift not found'];
            }

            $gift_price += $gift_model->price * (int)$one['count'];

            $gift_mass_new[] = [
                'id'    => (int)$one['giftID'],
                'price' => $gift_model->price,
                'agency_price' => $gift_model->agency_price,
                'count' => (int)$one['count'],
            ];
        }

        $paymentsService = new PaymentsService($userFrom);
        $userBalance = $paymentsService->getBalance();

        if ($userBalance < $gift_price) {
            return [
                'success' => false,
                'message' => 'Not enough money'
            ];
        }

        $girl = User::findOne(['id' => $userTo, 'user_type' => User::USER_FEMALE]);
        if(empty($girl)){
            return [
                'success' => false,
                'message' => 'Girls not found'
            ];
        }
        $agencyID = $girl->agency_id;

        foreach ($gift_mass_new as $one) {
            $action_model                  = new Action();
            $action_model->action_type_id  = 4;//send present
            $action_model->skin_id         = $skinID;
            $action_model->action_creator  = $userFrom;
            $action_model->action_receiver = $userTo;

            if (!$action_model->save()) {
                return ['success' => FALSE, 'code' => 2, 'message' => 'Error save action'];
            }

            $order_model                  = new Order();
            $order_model->action_id       = $action_model->id;
            $order_model->gift_id         = $one['id'];
            $order_model->quantity        = $one['count'];
            $order_model->order_status_id = 1;//in process

            if (!$order_model->save()) {
                return ['success' => FALSE, 'code' => 2, 'message' => 'Error save order','errors'=>$order_model->errors];
            }

            //с мужиков взымаем плату
            $paymentsService->addCredits($action_model->id, $one['price'] * $one['count']);

            //добавляем премию агенству
                $agencyUser = User::findOne(['agency_id'=>$agencyID, 'user_type' => User::USER_AGENCY]);
                    $agencyBonus = new AgencyRevard();
                    $agencyBonus->action_id = $action_model->id;
                    $agencyBonus->user_id = $agencyUser->id;
                    $agencyBonus->amount = $one['agency_price'] * $one['count'];
                    if (!$agencyBonus->save()) {
                        return [
                            'success' => false,
                            'message' => 'Agency bonus not save'
                        ];
                    }
        }

        return ['success' => TRUE, 'code' => 1, 'message' => 'Gift send'];
    }
}
