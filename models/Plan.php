<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
/**
 * This is the model class for table "plan".
 *
 * @property integer $id
 * @property integer $skin_id
 * @property string $name
 * @property string $price
 * @property string $created_at
 *
 * @property Skin $skin
 * @property Pricelist[] $pricelists
 * @property PricelistAgency[] $pricelistAgencies
 */
class Plan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['skin_id'], 'integer'],
            [['price'], 'number'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['skin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Skin::className(), 'targetAttribute' => ['skin_id' => 'id']],
            [['name'], function ($attribute) {
                $this->$attribute = Html::encode(strip_tags(HtmlPurifier::process($this->$attribute)));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'skin_id' => 'Skin ID',
            'name' => 'Name',
            'price' => 'Price',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkin()
    {
        return $this->hasOne(Skin::className(), ['id' => 'skin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPricelists()
    {
        return $this->hasMany(Pricelist::className(), ['plan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPricelistAgencies()
    {
        return $this->hasMany(PricelistAgency::className(), ['plan_id' => 'id']);
    }
}
