<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_transfer".
 *
 * @property integer      $id
 * @property integer      $transfer_type_id
 * @property integer      $user_id
 * @property integer      $deposit
 * @property string       $amount
 * @property string       $date_create
 * @property string       $fee
 *
 * @property TransferType $transferType
 * @property User         $user
 */
class UserTransfer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_transfer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['transfer_type_id', 'user_id', 'deposit'], 'integer'],
            [['amount', 'fee'], 'number'],
            [['date_create'], 'save'],
            [['transfer_type_id'], 'exist', 'skipOnError' => TRUE, 'targetClass' => TransferType::className(), 'targetAttribute' => ['transfer_type_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => TRUE, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'transfer_type_id' => 'Transfer Type ID',
            'user_id'          => 'User ID',
            'deposit'          => 'Deposit',
            'date_create'      => 'date_create',
            'amount'           => 'Amount',
            'fee'              => 'Fee',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getTransferType()
    {
        return $this->hasOne(TransferType::className(), ['id' => 'transfer_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
