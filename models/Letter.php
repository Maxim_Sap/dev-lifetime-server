<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "letter".
 *
 * @property integer $id
 * @property integer $action_id
 * @property string $title
 * @property string $description
 * @property string $readed_at
 * @property integer $reply_status
 * @property integer $was_deleted_by_man
 * @property integer $was_deleted_by_girl
 * @property integer $status
 *
 * @property FeaturedLetter[] $featuredLetters
 * @property Action $action
 */
class Letter extends \yii\db\ActiveRecord
{
    const WASNT_DELETED = 5;
    const WAS_DELETED = 10;

    const HAVE_REPLY = 1;
    const HAVENT_REPLY = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'letter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'reply_status', 'was_deleted_by_man', 'was_deleted_by_girl', 'status'], 'integer'],
            [['description'], 'string'],
            [['readed_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => Action::className(), 'targetAttribute' => ['action_id' => 'id']],
            [['title', 'description'], function ($attribute) {
                $this->$attribute = Html::encode(HtmlPurifier::process($this->$attribute));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_id' => 'Action ID',
            'title' => 'Title',
            'description' => 'Description',
            'readed_at' => 'Readed At',
            'reply_status' => 'Reply Status',
            'was_deleted_by_man' => 'Was Deleted By Man',
            'was_deleted_by_girl' => 'Was Deleted By Girl',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeaturedLetters()
    {
        return $this->hasMany(FeaturedLetter::className(), ['letter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id']);
    }
}
