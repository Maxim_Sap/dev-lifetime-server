<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "blog".
 *
 * @property integer $id
 * @property string  $title
 * @property string  $description
 * @property string  $image
 * @property string  $category
 * @property integer $on_main
 * @property integer $status
 * @property integer $sort_order
 * @property string  $created_at
 */
class Blog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['on_main', 'status', 'sort_order'], 'integer'],
            //[['created_at'], 'safe'],
            [['title', 'category', 'image'], 'string', 'max' => 255],
            [['title', 'category', 'image', 'description'], function ($attribute) {
                $this->$attribute = Html::encode(strip_tags(HtmlPurifier::process($this->$attribute)));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'title'       => 'Title',
            'description' => 'Description',
            'image'       => 'Image',
            'category'    => 'Category',
            'on_main'     => 'On Main',
            'status'      => 'Status',
            'sort_order'  => 'Sort Order',
            'created_at'  => 'Created At',
        ];
    }

    public function fields()
    {
        $fieldsList = [
            // field name is the same as the attribute name
            'id',
            'title',
            'description',
            'image',
            'category',
            'created_at',
        ];
        $user       = \Yii::$app->user->identity;
        if (!empty($user) && in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN, User::USER_SUPERADMIN])) {
            $fieldsList[] = 'on_main';
            $fieldsList[] = 'status';
            $fieldsList[] = 'sort_order';
            
        }

        return $fieldsList;
    }

    public static function getList($params=false)
    {
        $model = self::find();
        if($params && isset($params['category'])){
            $model->where(['category'=>$params['category']]);
        }
        $result = $model->all();

        return $result;
    }

    public static function getOnHomapage()
    {
        $model_story = self::find()
            ->where(['status'=>1,'on_main'=>1,'category'=>'story'])
            ->orderBy('sort_order DESC, created_at DESC')
            ->limit(3)
            ->all();

        return $model_story;
    }

    public static function getFooterPost()
    {
        $model_post = self::find()
            ->where(['status'=>1,'category'=>'post'])
            ->orderBy('sort_order DESC, created_at DESC')
            ->limit(2)
            ->all();

        return $model_post;
    }

    public static function addPost($post_title, $post_desc, $post_image, $category,$status = 0, $on_main = 0, $sort_order = 0)
    {
        $model              = new Blog();
        $model->title       = $post_title;
        $model->description = $post_desc;
        $model->status      = $status;
        $model->on_main     = $on_main;
        $model->category    = $category;
        $model->sort_order  = $sort_order;
        $model->image       = self::savePostImage($post_image);
        if (!$model->save()) {
            return [
                'success' => FALSE,
                'code'    => 2,
                'message' => 'error save post',
            ];
        }

        return [
            'success' => TRUE,
            'code'    => 1,
            'message' => 'ok',
            'post_id' => $model->id,
        ];
    }

    public static function getItem($PostID)
    {
        $model = self::findOne(['id' => $PostID]);

        return $model;
    }

    public static function editPost($post_id, $post_title, $post_desc, $post_image, $category, $status = 0, $on_main = 0, $sort_order = 0)
    {
        $model = self::findOne(['id' => $post_id]);
        if (empty($model)) {
            return [
                'success' => FALSE,
                'code'    => 2,
                'message' => 'post not found',
            ];
        }
        $model->title       = $post_title;
        $model->description = $post_desc;
        $model->status      = $status;
        $model->on_main     = $on_main;
        $model->category    = $category;
        $model->sort_order  = $sort_order;
        $model->image       = self::savePostImage($post_image, $post_id);

        if (!$model->save()) {
            return [
                'success' => FALSE,
                'code'    => 2,
                'message' => 'error save post',
            ];
        }

        return [
            'success' => TRUE,
            'code'    => 1,
            'message' => 'ok',
            'post_id' => $model->id,
        ];
    }

    public static function savePostImage($post_image, $post_id = null)
    {
        $dir = Yii::getAlias('@webroot') . '/uploads/post/';
        if (!file_exists($dir)) {
            mkdir($dir, 0777, TRUE);
        }
        $image_info = pathinfo($post_image);
        $image_name = $image_info['basename'];

        if (!empty($post_id)) {
            $imgDb = self::find()->where(['id' => $post_id])->one();
            // Если картинка новая
            if (!$imgDb && !empty($image_name)) {
                self::moveImageFromTemp($post_image, $dir . $image_name);
            }
            // Если картинку заменили
            if (isset($imgDb) && $imgDb->image != $image_name) {

                // Удаляем старую
                if (is_file(Yii::getAlias('@webroot') . "/" . $imgDb->image)) {
                    @unlink(Yii::getAlias('@webroot') . "/" . $imgDb->image);
                }

                self::moveImageFromTemp($post_image, $dir . $image_name);
            }
        } else {
            self::moveImageFromTemp($post_image, $dir . $image_name);
        }

        return 'uploads/post/' . $image_name;
    }

    public static function moveImageFromTemp($temp_image, $new_image_place)
    {
        if (is_file(Yii::getAlias('@webroot') . "/" . $temp_image)) {
            @rename(Yii::getAlias('@webroot') . "/" . $temp_image, $new_image_place);

            return TRUE;
        }

        return FALSE;
    }

    public static function deletePost($post_id)
    {
        $model = self::findOne(['id' => $post_id]);
        if (empty($model)) {
            return [
                'success' => FALSE,
                'code'    => 2,
                'message' => 'post not found',
            ];
        }

        $img = $model->image;
        if (is_file(Yii::getAlias('@webroot') . "/" . $img)) {
            @unlink(Yii::getAlias('@webroot') . "/" . $img);
        }

        $model->delete();

        return [
            'success' => TRUE,
            'code'    => 1,
            'message' => 'ok',
        ];

    }

}
