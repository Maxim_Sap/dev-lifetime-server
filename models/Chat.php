<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "chat".
 *
 * @property integer $id
 * @property integer $action_id
 * @property integer $chat_session_id
 * @property string $chat_id
 * @property string $message
 * @property string $original_message
 * @property string $created_at
 * @property string $readed_at
 * @property integer $edited
 * @property integer $active
 *
 * @property Action $action
 * @property ChatSession $chatSession
 */
class Chat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'chat_session_id', 'chat_id'], 'required'],
            [['action_id', 'chat_session_id', 'edited', 'active'], 'integer'],
            [['message', 'original_message'], 'string'],
            [['created_at', 'readed_at'], 'safe'],
            [['chat_id'], 'string', 'max' => 60],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => Action::className(), 'targetAttribute' => ['action_id' => 'id']],
            [['chat_session_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChatSession::className(), 'targetAttribute' => ['chat_session_id' => 'id']],
            [['message', 'original_message'], function ($attribute) {
                $this->$attribute = Html::encode(HtmlPurifier::process($this->$attribute));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_id' => 'Action ID',
            'chat_session_id' => 'Chat Session ID',
            'chat_id' => 'Chat ID',
            'message' => 'Message',
            'original_message' => 'Original Message',
            'created_at' => 'Created At',
            'readed_at' => 'Readed At',
            'edited' => 'Edited',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChatSession()
    {
        return $this->hasOne(ChatSession::className(), ['id' => 'chat_session_id']);
    }


    public function afterSave($insert, $changedAttributes)
    {
        $user = Yii::$app->user->identity;
        if (!empty($this->message)) {
            $contactList = ContactList::find()
                                        ->where([
                                            'user_id' => $this->action->action_creator, 
                                            'contact_id' => $this->action->action_receiver
                                        ])
                                        ->one();
            if (empty($contactList)) {
                $contactList = new ContactList();
                $contactList->user_id = $this->action->action_creator;
                $contactList->contact_id = $this->action->action_receiver;
                $contactList->save();
                $contactList = new ContactList();
                $contactList->user_id = $this->action->action_receiver;
                $contactList->contact_id = $this->action->action_creator;
                $contactList->save();
            }            
        }        
        if ((Yii::$app->controller->id == "mailing-queue" || (!empty($user) && $user->user_type == User::USER_FEMALE)) && !empty($this->message)) {
            $chatInvite = new ChatInvite();
            $chatInvite->invite_sender = $this->action->action_creator;
            $chatInvite->invite_receiver = $this->action->action_receiver;
            $chatInvite->message = $this->message;
            $chatInvite->show = ChatInvite::SHOW_INVITE;
            $chatInvite->created_at = $this->created_at;
            $chatInvite->save();
        }        
        parent::afterSave($insert, $changedAttributes);  
    }
}
