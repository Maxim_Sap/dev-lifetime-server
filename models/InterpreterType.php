<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "interpreter_type".
 *
 * @property integer $id
 * @property string $type
 * @property string $description
 *
 * @property Interpreter[] $interpreters
 */
class InterpreterType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'interpreter_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterpreters()
    {
        return $this->hasMany(Interpreter::className(), ['interpreter_type_id' => 'id']);
    }
}
