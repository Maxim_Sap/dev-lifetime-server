<?php
date_default_timezone_set('UTC');
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require_once(__DIR__ . '/../config/console.php');
$application = new yii\console\Application($config);

use Workerman\Worker;

$chatConfig = require_once(__DIR__ . '/Applications/chatConfig.php');

// SSL context.
$context = array(
    'ssl' => array(
        'local_cert' => __DIR__ . $chatConfig['pathToCertificate'],
        'local_pk'   => __DIR__ . $chatConfig['pathToKey'],
        'allow_self_signed' => true,
        'verify_peer'  => false,        
    )
);

ini_set('display_errors', 'on');

if(strpos(strtolower(PHP_OS), 'win') === 0)
{
    exit("start.php not support windows, please use start_for_win.bat\n");
}

// check extensions
if(!extension_loaded('pcntl'))
{
    exit("Please install pcntl extension. See http://doc3.workerman.net/appendices/install-extension.html\n");
}

if(!extension_loaded('posix'))
{
    exit("Please install posix extension. See http://doc3.workerman.net/appendices/install-extension.html\n");
}

Yii::$app->cache->flush();

// constant for global start
define('GLOBAL_START', 1);

// load all applications from Applications/*/start.php， to enable services
foreach(glob(__DIR__.'/Applications/*.php') as $start_file)
{
    require_once $start_file;
}

// all services work
Worker::runAll();
