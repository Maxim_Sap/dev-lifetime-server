<?php 

return [
	"webSocketServer" => "api.brachka.web-synthesis.ru",
	"chatPort" => "2347",
	"videoChatPort" => "2477",
	"pathToCertificate" => "/params/api_brachka_web_synthesis_ru_a2fb7_58c25_1515049415_37f323b5cac3b71e16f3d2b2ebf7323c.crt",
	"pathToKey" => "/params/a2fb7_58c25_253d07a61d1f81c7cf2a147df864d904.key"
];