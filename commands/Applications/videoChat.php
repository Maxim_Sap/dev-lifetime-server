<?php

use Workerman\Worker;
use yii\helpers\Json;
use app\components\VideoManager;
use app\components\AbstractStorage;
use Workerman\Lib\Timer;
use yii\helpers\HtmlPurifier;
use app\components\UserModel;
use app\components\Action;
use app\components\ActionType;
use app\components\VideoChat;

use app\models\AgencyRevard;
use app\services\PaymentsService;
use app\services\BillingService;


$videoChatWorker = new Worker("websocket://0.0.0.0:" . $chatConfig['videoChatPort'], $context);

$videoChatWorker->name = "VideoChat";
$videoChatWorker->transport = 'ssl';
$videoChatWorker->protocol = 'udp';
Worker::$stdoutFile =  __DIR__. '/chat.log';

$timerInterval = 1*15;

$videoChatWorker->onWorkerStart = function()
{
    global $timerInterval;
    Timer::add($timerInterval, 'videoPing');
};

$videoClients = [];
$videoManager = new VideoManager();
$rooms = [];
$ticks = 0;

$videoChatWorker->onConnect = function($connection)
{
/*    $connection->onWebSocketConnect = function($connection, $http_header)
    {
        // Check $_SERVER['HTTP_ORIGIN'] here.
        if($_SERVER['HTTP_ORIGIN'] != Yii::$app->params['clientUrl'])
        {
            echo 'Wrong origin';
            $connection->close();
        }
    };*/
    global $videoClients;
    $rid = getVideoResourceId($connection);
    $videoClients[$rid] = $connection;
    echo "New Video Connection with $rid " . date("Y-m-d H:i:s") . "\n";  

};

// Emitted when data received
$videoChatWorker->onMessage = function($connection, $data)
{
    date_default_timezone_set('UTC');
    global $videoClients, $videoManager, $rooms, $users, $timerInterval;
    try {
        $messageFromClient = Json::decode($data, true);    
        $rid = array_search($connection, $videoClients);
        //var_dump($messageFromClient);
        if (!empty($messageFromClient['userReceiverId'])) {
            $userReceiverID =  $messageFromClient['userReceiverId'];
        } else {
            echo 'Auth. Empty user id';
            $videoClients[$rid]->close();
            return;
        }
        if (!empty($messageFromClient['token'])) {
            $authUser = UserModel::findIdentityByAccessToken($messageFromClient['token']);
            if (empty($authUser)) {
                echo 'Auth. Not authorizied';
                $videoClients[$rid]->close();
                return;
            }
        } else {
            echo 'Auth. Empty token';
            $videoClients[$rid]->close();
            return;
        }
        $actionType = ActionType::findOne(['name' => 'videochat']);
        if (empty($actionType)) {
            $actionType = new ActionType();
            $actionType->name = 'videochat';
            if (!$actionType->save()) {
                echo 'Auth. Action type save error';
                $videoClients[$rid]->close();
                return;
            }
        }
        $action = Action::find()
                    ->where(['action_type_id' => $actionType->id, 
                             'action_creator' => $authUser->id, 
                             'action_receiver' => $userReceiverID])
                    ->orWhere(['action_type_id' => $actionType->id,
                             'action_creator' => $userReceiverID, 
                             'action_receiver' => $authUser->id])
                    ->andWhere(['>=', 'updated_at', date("Y-m-d H:i:s", time()-5*60)])
                    ->orderBy(['created_at' => SORT_DESC])
                    ->one();
        if (!empty($action)) {
            $videoChat = $action->getVideoChats()->orderBy(['created_at' => SORT_DESC])->one();
        }   

        if (empty($videoChat)) {
            $videoChatId = "";
        } else {
            $videoChatId = $videoChat->video_chat_id;
        }
        //$room = isset($videoChatId) ? $videoChatId : "";
        $room = isset($messageFromClient['data']['room']) ? $messageFromClient['data']['room'] : "";
        
        if ($authUser->user_type == UserModel::USER_MALE && in_array($messageFromClient['data']['action'], ['candidate'])) {
            $videoManager->storeVideoParams($authUser->id, $room, $userReceiverID, time());
        }        
        
        if ($messageFromClient['data']['action'] == 'subscribe' && $room) {
            $billingService = new BillingService($authUser->id);
            $paymentsService = new PaymentsService($authUser->id);
            $userBalance = $paymentsService->getBalance();
            $actionType = ActionType::findOne(['name' => 'videochat']);
            if (empty($actionType)) {
                $actionType = new ActionType();
                $actionType->name = 'videochat';
                if (!$actionType->save()) {
                    echo 'Auth. Action type save error';
                    $connection->close();
                    return;
                }
            }
            $videoChatPricePerMinute = $billingService->getActionPrice($skinID = 1, $planID = 1, $actionType->id, $authUser->id);
            if (!is_numeric($videoChatPricePerMinute)) {
                echo 'Can\'t get video price';
                $connection->close();
                return;
            }
            $videoChatPrice = $timerInterval*$videoChatPricePerMinute/60; // price for 15 sec videochat

            if ($userBalance < $videoChatPrice && $authUser->user_type == UserModel::USER_MALE) {        
                $connection->send(Json::encode(['action' => 'error', 'room' => $messageFromClient['data']['room'], 'data' => ['user' => ['id' => $authUser->id], 'message' => 'You have not enough money to start videochat', 'videochatID' => true]]));
                $connection->close();
                return;
            }
            $videoManager->addUser($rid, $authUser->id);    
            $videoChat = $videoManager->findVideoChat($room, $rid);
            if((array_key_exists($room, $rooms) && 
                !in_array($connection, $rooms[$room])) ||
                !array_key_exists($room, $rooms)){                
                    if(isset($rooms[$room]) && count($rooms[$room]) >= 2){
                        //maximum number of connection reached
                        $messageToSend = json_encode(['action'=>'subRejected']);
                    
                        $connection->send($messageToSend);
                        echo 'subRejected';
                        $videoClients[$rid]->close();
                    }
                    
                    else{
                        $rooms[$room][] = $connection;//subscribe user to room
                    
                        notifyUsersOfConnection($room, $connection);
                    }
                }        
            else{
                //tell user he has subscribed on another device/browser
                $messageToSend = json_encode(['action'=>'subRejected']);
                
                $videoClients[$rid]->send($messageToSend);
                echo 'subRejected';
                $videoClients[$rid]->close();
            }
        }
        else if($room && isset($rooms[$room])) {
            //send to everybody subscribed to the room received except the sender
            foreach($rooms[$room] as $client){
                if ($client !== $connection) {
                    $client->send(Json::encode($messageFromClient['data']));
                }
            }
        }
    } catch (Exception $e) {
        echo $e->getMessage();
        echo $e->getTraceAsString();
        \Yii::$app->db->close();
        \Yii::$app->db->open();
        $connection->close();
    }               

};

// Emitted when connection closed
$videoChatWorker->onClose = function($connection)
{
    global $videoClients, $rooms;
    $rid = array_search($connection, $videoClients);  
    closeVideoRequest($rid);  
    unset($videoClients[$rid]);        
    if(count($rooms)){//if there is at least one room created
        foreach($rooms as $room=>$arrayOfSubscribers){//loop through the rooms
            foreach ($arrayOfSubscribers as $key=>$websocketConnection){//loop through the users connected to each room
                if($websocketConnection == $connection){//if the disconnecting user subscribed to this room
                    unset($rooms[$room][$key]);//remove him from the room                    
                    //notify other subscribers that he has disconnected
                    notifyUsersOfDisconnection($room, $connection);
                }
            }
        }
    }
    echo "Video connection with $rid is closed " . date("Y-m-d H:i:s") . "\n";
};

function getVideoResourceId($connection)
{    
    return $connection->id;
}

function notifyUsersOfConnection($room, $from){
    global $rooms;    
    echo "User subscribed to room ".$room ."\n";
    $msg_to_broadcast = json_encode(['action'=>'newSub', 'room'=>$room]);
    //notify user that someone has joined room
    foreach($rooms[$room] as $client) {
        if ($client !== $from) {
            $client->send($msg_to_broadcast);
        }
    }
}

function notifyUsersOfDisconnection($room, $from){
    global $rooms;    
    $msg_to_broadcast = json_encode(['action'=>'imOffline', 'room'=>$room]);
    //notify user that remote has left the room
    foreach($rooms[$room] as $client) {
        if ($client !== $from) {
            $client->send($msg_to_broadcast);
        }
    }
}

function closeVideoRequest($rid)
{
    date_default_timezone_set('UTC');
    //get user for closed connection    
    global $videoManager, $videoClients;    
    $requestUser = $videoManager->getUserByRid($rid);
    $videoChat = $videoManager->getUserVideoChat($rid);
    if (isset($requestUser->id)) {
        $authUser = UserModel::findOne(['id' => $requestUser->id]);
        try {
            $authUser->cam_online = 0;    
            $authUser->save();
        } catch (\Exception $e) {

        }   
        if ($authUser->user_type == UserModel::USER_MALE) {  

            $videoChatId = $videoChat->getUid();
            $userVideoChat = VideoChat::find()->where(['video_chat_id' => $videoChatId])
                                        ->andWhere(['>=', 'updated_at', date("Y-m-d H:i:s", time()-5*60)])->orderBy(['updated_at' => SORT_DESC])->one();            
            if (!empty($userVideoChat)) {
                updateVideoDuration($userVideoChat, $authUser, $videoClients[$rid]);
                AbstractStorage::factory()->insertNewChatMessage($userVideoChat->action->action_creator, $userVideoChat->action->action_receiver, null, date("Y-m-d H:i:s", time()));
            }

        }
    }    
}

function videoPing()
{    
    date_default_timezone_set('UTC');
    global $videoClients, $videoManager, $ticks;
    $ticks += 15;
/*    if ($ticks % 30 == 0) {
        // get some data to support mysql connection
        $actionType = ActionType::findOne(['id' => 1]);
    }*/
    
    foreach ($videoClients as $connection) {        
        $rid = getVideoResourceId($connection);        
        $requestUser = $videoManager->getUserByRid($rid);        
        $videoChat = $videoManager->getUserVideoChat($rid);
        if (empty($requestUser) || empty($videoChat)) {
            $connection->close();
            return;
        }        
        $authUser = UserModel::findOne(['id' => $requestUser->id]);
        if ($authUser->user_type == UserModel::USER_FEMALE) {
            try {
                $authUser->cam_online = 1;    
                $authUser->save();
            } catch (\Exception $e) {

            }  
        }
        if ($authUser->user_type == UserModel::USER_MALE) {       
            $videoChatId = $videoChat->getUid();
            $userVideoChat = VideoChat::find()->where(['video_chat_id' => $videoChatId])
                                        ->andWhere(['>=', 'updated_at', date("Y-m-d H:i:s", time()-5*60)])->orderBy(['updated_at' => SORT_DESC])->one();            
            if (!empty($userVideoChat)) {
                updateVideoDuration($userVideoChat, $authUser, $connection);
            }
        }
    }       
}

function updateVideoDuration($userVideoChat, $authUser, $connection)
{   
    date_default_timezone_set('UTC');
    //$tz  = new \DateTimeZone(date_default_timezone_get());  
    $sinceLastVideo = \DateTime::createFromFormat('Y-m-d H:i:s', $userVideoChat->updated_at/*, $tz*/)
     ->diff(new \DateTime('now'/*, $tz*/));                 
    //echo $sinceLastVideo->i*60 + $sinceLastVideo->s . "\n";
    $userVideoChat->duration += $sinceLastVideo->i*60 + $sinceLastVideo->s;
    $userVideoChat->updated_at = date("Y-m-d H:i:s", time());
    //echo "in ping $userVideoChat->duration \n";
    $userVideoChat->save();
    $billingService = new BillingService($authUser->id);
    $paymentsService = new PaymentsService($authUser->id);
    $userBalance = $paymentsService->getBalance();
    $actionType = ActionType::findOne(['name' => 'videochat']);
    if (empty($actionType)) {
        $actionType = new ActionType();
        $actionType->name = 'videochat';
        if (!$actionType->save()) {
            echo 'Auth. Action type save error';
            $connection->close();
            return;
        }
    }
    $videoChatPricePerMinute = $billingService->getActionPrice($skinID = 1, $planID = 1, $actionType->id, $authUser->id);
    if (!is_numeric($videoChatPricePerMinute)) {
        echo 'Can\'t get video price';
        $connection->close();
        return;
    }
    $videoChatPrice = ($sinceLastVideo->i*60 + $sinceLastVideo->s)*$videoChatPricePerMinute/60;    

    if ($userBalance < $videoChatPrice) {        
        $connection->send(Json::encode(['message' => 'You have not enough money to continue videochat']));
        $connection->close();
        return;
    }
    $videoChatPrice = ($userVideoChat->duration)*$videoChatPricePerMinute/60;
    $updateSuccessfull = $paymentsService->updateCreditsByActionId($userVideoChat->action_id, $videoChatPrice);
    if (!$updateSuccessfull) {
        $paymentsService->addCredits($userVideoChat->action_id, $videoChatPrice);
    }
    // agency revard
    // send bonus to agency
    $girl = Action::find()->select(['user_type', 'status', 'agency_id'])
                          ->leftJoin(UserModel::tableName(), 'user.id = action.action_receiver')
                          ->where(['user_type' => UserModel::USER_FEMALE, 'status' => UserModel::STATUS_ACTIVE, 'action.id' => $userVideoChat->action_id])->asArray()->one();
    if (empty($girl)) {
        $girl = Action::find()->select(['user_type', 'status', 'agency_id'])
                              ->leftJoin(UserModel::tableName(), 'user.id = action.action_creator')
                              ->where(['user_type' => UserModel::USER_FEMALE, 'status' => UserModel::STATUS_ACTIVE, 'action.id' => $userVideoChat->action_id])->asArray()->one();
    }
    if (!empty($girl)) {
        $girl = (object)$girl;
        $girlAgency = UserModel::find()->where([
                                            'user_type' => UserModel::USER_AGENCY, 
                                            'agency_id'=> $girl->agency_id, 
                                            'status' => UserModel::STATUS_ACTIVE
                                        ])
                                       ->one();
    }
    if (!empty($girlAgency)) {
        $agencyPricePerMinute = $billingService->getAgencyPrice($skinID = 1, $planID = 1, $actionType->id, $girlAgency->id);

        if (!empty($agencyPricePerMinute)) {
            $agencyRevard = AgencyRevard::findOne(['action_id' => $userVideoChat->action_id]);
            $increment = ($sinceLastVideo->i*60 + $sinceLastVideo->s)*$agencyPricePerMinute/60;
            if ($increment < 0.01) {
                $increment = 0.01;
            }

            if (empty($agencyRevard)) {
                $agencyRevard            = new AgencyRevard();
                $agencyRevard->action_id = $userVideoChat->action_id;
                $agencyRevard->user_id   = $girlAgency->id;
                $agencyRevard->amount    = $increment;
            } else {
                $agencyRevard->amount    += $increment;
            }

            if (!$agencyRevard->save()) {
                echo 'Can\'t be saved chat session. Errors: '. join(', ', $agencyRevard->getFirstErrors());
                $connection->close();
                return;
            }            
        }
    } else {
        echo 'Can\'t get agency';
        $connection->close();
        return;
    }    
}

function videopongRequest($rid, array $data)
{
    global $videoManager, $videoClients;
    echo "Video client with $rid send response to ping\r\n";    
}

// Run worker
if(!defined('GLOBAL_START'))
{
    Worker::runAll();
}