<?php

use Workerman\Worker;
use yii\helpers\Json;
use app\components\ChatManager;
use Workerman\Lib\Timer;
use yii\helpers\HtmlPurifier;
use yii\helpers\Html;
use app\components\Chat;
use app\components\VideoChat;
use app\components\ChatSession;
use app\components\UserModel;
use app\components\Action;
use app\components\UserProfile;
use app\components\ActionType;
use yii\db\Query;

use app\models\AgencyRevard;
use app\models\MissingChat;
use app\services\PaymentsService;
use app\services\BillingService;

const MESSAGE_LIMIT = 60;
const TIMER_INTERVAL = 15;
// Create a Websocket server

$chatWorker = new Worker("websocket://0.0.0.0:" . $chatConfig['chatPort'], $context);

//Yii::$app->getDb();

// 1 process
$chatWorker->count = 1;

$chatWorker->transport = 'ssl';

$chatWorker->name = 'Chat';

global $chatClients, $chatManager, $chatRequests, $messagesPerMinute, $ticks;
$chatClients = [];
$chatManager = new ChatManager();
$chatRequests = ['auth', 'message', 'pong', 'videostart', 'videostop', 'startVideoCall', 'loadhistory', 'image'];
$messagesPerMinute = [];
$ticks = 0;

$chatWorker->onWorkerStart = function() {
    $timerInterval = TIMER_INTERVAL; // 15 seconds
    Timer::add($timerInterval, 'ping');
};
// Emitted when new connection come
$chatWorker->onConnect = function($connection)
{
/*    $connection->onWebSocketConnect = function($connection , $http_header)
    {
        // Check $_SERVER['HTTP_ORIGIN'] here.
        if($_SERVER['HTTP_ORIGIN'] != Yii::$app->params['clientUrl'])
        {
            echo '';
            $connection->close();
        }
    };*/
    global $chatClients;
    $rid = getResourceId($connection);
    $chatClients[$rid] = $connection;
	echo "New Connection with rid $rid " . date('Y-m-d H:i:s') . "\n";        
};

// Emitted when data received
$chatWorker->onMessage = function($connection, $data)
{
    global $chatClients, $chatRequests;
    $message = Json::decode($data, true);
    $rid = array_search($connection, $chatClients);
    if (in_array($message['type'], $chatRequests)) {
        call_user_func_array($message['type'].'Request', [$rid, $message['data']]);
    }
    
};

// Emitted when connection closed
$chatWorker->onClose = function($connection)
{
    global $chatClients, $chatManager;
    $rid = array_search($connection, $chatClients);
    if ($chatManager->getUserByRid($rid)) {
        closeRequest($rid);
    }
    unset($chatClients[$rid]);
    echo "Connection $rid is closed" . date('Y-m-d H:i:s') . "\n";
};

function abuseProtection($rid, $userID)
{
    global $messagesPerMinute;    
    if (!isset($messagesPerMinute[$userID])) {
        $messagesPerMinute[$userID] = 0;
    } else {
        $messagesPerMinute[$userID]++;
    }
    if ($messagesPerMinute[$userID] > MESSAGE_LIMIT) {
        return false;        
    }
    return true;
}

function authRequest($rid, array $data)
{
    try {
        date_default_timezone_set('UTC');
        global $chatManager, $chatClients;
        if (!empty($data['user']['id'])) {
            $userReceiverID =  $data['user']['id'];
        } else {
            $chatClients[$rid]->close();
            return;
        }
        if (!empty($data['token'])) {
            $authUser = UserModel::findIdentityByAccessToken($data['token']);
            if (empty($authUser)) {
                $chatClients[$rid]->close();
                return;
            }
        } else {
            $chatClients[$rid]->close();
            return;
        }    
        if ($authUser->user_type == UserModel::USER_INTERPRETER) {
            if (empty($data['asUser']) || !in_array($data['asUser'], $authUser->user_ids)) {
                $chatClients[$rid]->close();
                return;
            } else {
                $authUser = UserModel::findOne(['id' => $data['asUser']]);
            }
        }
        if (!abuseProtection($rid, $authUser->id)) {
            $conn = $chatClients[$rid];
            $conn->send(Json::encode(['type' => 'warning', 'data' => ['user' => ['id' => $authUser->id], 'message' => 'You send too many messages', 'chatID' => true]]));
            $chatClients[$rid]->close();
            return;
        }
        if (!userHaveEnoughMoney($rid, $authUser)) {
            return;
        }
        $chatId = $chatManager->getChatIdFromDb($authUser->id, $userReceiverID);
        echo 'Auth request from rid: '.$rid.' and chat: '.$chatId . "\r\n";
        
        //var_dump($userId);
        //var_dump($chatManager->isUserExistsInChat($userId, $chatId));
        //the same user already connected to current chat, need to close old connect
        if ($oldRid = $chatManager->isUserExistsInChat($authUser->id, $chatId)) {        
            closeRequest($oldRid);
        }
        //var_dump($chatManager);
        $chatManager->addUser($rid, $authUser->id);    
        $chat = $chatManager->findChat($chatId, $rid);
        $users = $chat->getUsers();
        // chat room can't have more than 2 users
        if (count($users) > 2) {
            closeRequest($rid);
        }

        //var_dump($chatManager);
        $joinedUser = $chatManager->getUserByRid($rid);
        $response = [        
            'user' => $joinedUser,        
            'join' => true,
        ];
        
        foreach ($users as $user) {
            //send message for other users of this chat
            if ($authUser->id != $user->getId()) {
                $conn = $chatClients[$user->getRid()];
                $conn->send(Json::encode([
                    'type' => 'auth', 
                    'timestamp' => date('c',time()),
                    'chatID' => true,
                    'data' => $response
                ]));
            }
        }
        //send auth response for joined user
        $response = [        
            'user' => $joinedUser,
            'users' => $users,
            'history' => $chatManager->getHistory($chat->getUid(), $authUser->id),
            'unreadedMessages' => $chatManager->getUnreadedMessages($chat->getUid(), $authUser->id),
        ];

        $conn = $chatClients[$rid];
        $conn->send(Json::encode([
            'type' => 'auth', 
            'timestamp' => date('c',time()),
            'chatID' => true,
            'data' => $response
        ]));
        //$chatManager->setAllMessageAsReaded($chat->getUid(), $userReceiverID, $authUser->id);
    } catch (Exception $e) {
        echo $e->getMessage();
        \Yii::$app->db->close();
        \Yii::$app->db->open();
        $chatClients[$rid]->close();
    }

}

function userHaveEnoughMoney($rid, $authUser)
{
    global $chatClients;
    if ($authUser->user_type == UserModel::USER_MALE) {
        $billingService = new BillingService($authUser->id);
        $paymentsService = new PaymentsService($authUser->id);
        $userBalance = $paymentsService->getBalance();
        $actionType = ActionType::findOne(['name' => 'chat']);
        if (empty($actionType)) {
            $actionType = new ActionType();
            $actionType->name = 'chat';
            if (!$actionType->save()) {
                echo 'Auth. Action type save error';
                $chatClients[$rid]->close();
                return false;
            }
        }
        $chatPrice = $billingService->getActionPrice($skinID = 1, $planID = 1, $actionType->id, $authUser->id);
        
        if (!is_numeric($chatPrice)) {
            echo 'Can\'t get chat price';
            $chatClients[$rid]->close();
            return false;
        }

        if ($userBalance < $chatPrice) {        
            $chatClients[$rid]->send(Json::encode(['type' => 'error', 'data' => ['user' => ['id' => $authUser->id], 'message' => 'You have not enough money to start chat', 'chatID' => true]]));
            $chatClients[$rid]->close();
            return false;
        }
    }
    return true;
}

function messageRequest($rid, array $data)
{
    date_default_timezone_set('UTC');
    global $chatManager, $chatClients;    
    if (!empty($data['user']['id'])) {
        $userReceiverID =  $data['user']['id'];
    } else {
        $chatClients[$rid]->close();
        return;
    }
    if (!empty($data['token'])) {
        $authUser = UserModel::findIdentityByAccessToken($data['token']);
        if (empty($authUser)) {
            $chatClients[$rid]->close();
            return;
        }
    } else {
        $chatClients[$rid]->close();
        return;
    }
    if ($authUser->user_type == UserModel::USER_INTERPRETER) {
        if (empty($data['asUser']) || !in_array($data['asUser'], $authUser->user_ids)) {
            $chatClients[$rid]->close();
            return;
        } else {
            $authUser = UserModel::findOne(['id' => $data['asUser']]);
        }
    }
    if (!abuseProtection($rid, $authUser->id)) {
        $conn = $chatClients[$rid];
        $conn->send(Json::encode(['type' => 'message', 'data' => ['user' => ['id' => $authUser->id], 'message' => 'You send too many messages', 'chatID' => true]]));
        $chatClients[$rid]->close();
        return;
    }
    if ($authUser->user_type == UserModel::USER_MALE && !userHaveEnoughMoney($rid, $authUser)) {
        return;
    }
    //echo 'Message from: ' . $rid . "\r\n";
    $chat = $chatManager->getUserChat($rid);
    if (!$chat) {
        return;
    }
    $result = [];
    $result['message'] = HtmlPurifier::process($data['message']);
    $result['message'] = str_replace(array("\r\n", "\r", "\n"), "<br />", trim($result['message']));
    if (trim($data['message']) == '') {
        return;
    }
    var_dump($result['message']);
    $result['message'] = Html::encode(Chat::removeBadWords($result['message']));
    $timestamp = time();
    $result['created_at'] = date('c', $timestamp);
    $result['readed_at'] = 0;
    $result['chatID'] = true;    
    $userSender = $chatManager->getUserByRid($rid);
    $userSender->setLastChatActivity(date('c', $timestamp));
    $chatManager->storeMessage($userSender, $chat, $userReceiverID, $result['message'], $timestamp);      
    $result['user'] = $userSender;
    foreach ($chat->getUsers() as $user) { 
        if ($user->getRid() != $rid) {
            $result['unreadedMessages'] = $chatManager->getUnreadedMessages($chat->getUid(), $userReceiverID);
        } else {
            unset($result['unreadedMessages']);
        }             
        $conn = $chatClients[$user->getRid()];
        $conn->send(Json::encode(['type' => 'message', 'data' => $result]));
    }
    $chatManager->setAllMessageAsReaded($chat->getUid(), $userReceiverID, $authUser->id);
}

function imageRequest($rid, array $data)
{
    date_default_timezone_set('UTC');
    global $chatManager, $chatClients;    
    if (!empty($data['user']['id'])) {
        $userReceiverID =  $data['user']['id'];
    } else {
        $chatClients[$rid]->close();
        return;
    }
    if (!empty($data['token'])) {
        $authUser = UserModel::findIdentityByAccessToken($data['token']);
        if (empty($authUser)) {
            $chatClients[$rid]->close();
            return;
        }
    } else {
        $chatClients[$rid]->close();
        return;
    }
    if ($authUser->user_type == UserModel::USER_INTERPRETER) {
        if (empty($data['asUser']) || !in_array($data['asUser'], $authUser->user_ids)) {
            $chatClients[$rid]->close();
            return;
        } else {
            $authUser = UserModel::findOne(['id' => $data['asUser']]);
        }
    }
    if (!abuseProtection($rid, $authUser->id)) {
        $conn = $chatClients[$rid];
        $conn->send(Json::encode(['type' => 'message', 'data' => ['user' => ['id' => $authUser->id], 'message' => 'You send too many messages', 'chatID' => true]]));
        $chatClients[$rid]->close();
        return;
    }
    if ($authUser->user_type == UserModel::USER_MALE && !userHaveEnoughMoney($rid, $authUser)) {
        return;
    }
    echo 'Image Message from: ' . $rid . "\r\n";
    $chat = $chatManager->getUserChat($rid);
    if (!$chat) {
        return;
    }
    $result = [];
    $result['message'] = Html::a(Html::img(Yii::$app->params['baseUrl'] . '/' . Html::encode(HtmlPurifier::process($data['image']))),Yii::$app->params['baseUrl']. '/' . str_replace('thumb_255x290', 'original', Html::encode(HtmlPurifier::process($data['image']))), ['target' => '_blank']);
    if (trim($data['image']) == '') {
        return;
    }
    $timestamp = time();
    $result['created_at'] = date('c', $timestamp);
    $result['readed_at'] = 0;
    $result['chatID'] = true;    
    $userSender = $chatManager->getUserByRid($rid);
    $userSender->setLastChatActivity(date('c', $timestamp));
    $chatManager->storeMessage($userSender, $chat, $userReceiverID, $result['message'], $timestamp);      
    $result['user'] = $userSender;
    foreach ($chat->getUsers() as $user) { 
        if ($user->getRid() != $rid) {
            $result['unreadedMessages'] = $chatManager->getUnreadedMessages($chat->getUid(), $userReceiverID);
        } else {
            unset($result['unreadedMessages']);
        }             
        $conn = $chatClients[$user->getRid()];
        $conn->send(Json::encode(['type' => 'image', 'data' => $result]));
    }
    $chatManager->setAllMessageAsReaded($chat->getUid(), $userReceiverID, $authUser->id);
}

function videostartRequest($rid, array $data)
{
    global $chatManager, $chatClients;    
    
    if (!empty($data['token'])) {
        $authUser = UserModel::findIdentityByAccessToken($data['token']);
        if (empty($authUser)) {
            $chatClients[$rid]->close();
            return;
        }
    } else {
        $chatClients[$rid]->close();
        return;
    }
    // only girl can use this function
    if ($authUser->user_type != UserModel::USER_FEMALE) {
        return;
    }
    if (!abuseProtection($rid, $authUser->id)) {
        $conn = $chatClients[$rid];
        $conn->send(Json::encode(['type' => 'message', 'data' => ['user' => ['id' => $authUser->id], 'message' => 'You send too many messages', 'chatID' => true]]));
        $chatClients[$rid]->close();
        return;
    }
    $chat = $chatManager->getUserChat($rid);
    if (!$chat) {
        return;
    }
    $result = [];
    $result['user'] = $chatManager->getUserByRid($rid);
    try {
        $authUser->cam_online = 1;    
        $authUser->save();
    } catch (\Exception $e) {

    } 
    if (!empty($data['room'])) {
        $result['room'] = $data['room'];
    }    
    $sendedTo = [];
    // broadcasting message that some girl start videochat
    foreach ($chatClients as $connection) {
        $requestUser = $chatManager->getUserByRid(getResourceId($connection));
        if (!empty($requestUser) && getResourceId($connection) != $rid && empty($sendedTo[$requestUser->id])) {
            $connection->send(Json::encode(['type' => 'videostart', 'data' => $result]));
            $sendedTo[$requestUser->id] = true;
        }                  
    }
}

function videostopRequest($rid, array $data)
{
    global $chatManager, $chatClients;    
    
    if (!empty($data['token'])) {
        $authUser = UserModel::findIdentityByAccessToken($data['token']);
        if (empty($authUser)) {
            $chatClients[$rid]->close();
            return;
        }
    } else {
        $chatClients[$rid]->close();
        return;
    }
    // only girl can use this function
    if ($authUser->user_type != UserModel::USER_FEMALE) {
        return;
    }
    if (!abuseProtection($rid, $authUser->id)) {
        $conn = $chatClients[$rid];
        $conn->send(Json::encode(['type' => 'message', 'data' => ['user' => ['id' => $authUser->id], 'message' => 'You send too many messages', 'chatID' => true]]));
        $chatClients[$rid]->close();
        return;
    }
    $chat = $chatManager->getUserChat($rid);
    if (!$chat) {
        return;
    }
    try {
        $authUser->cam_online = 0;    
        $authUser->save();
    } catch (\Exception $e) {

    }    
    $result = [];
    $result['user'] = $chatManager->getUserByRid($rid);
    $sendedTo = [];
    // broadcast message that some girl stop videochat
    foreach ($chatClients as $connection) { 
        $requestUser = $chatManager->getUserByRid(getResourceId($connection));
        if (!empty($requestUser) && getResourceId($connection) != $rid && empty($sendedTo[$requestUser->id])) {
            $connection->send(Json::encode(['type' => 'videostop', 'data' => $result]));
            $sendedTo[$requestUser->id] = true;
        }                  
    }
}

function loadhistoryRequest($rid, array $data)
{
    global $chatManager, $chatClients;
    if (!empty($data['user']['id'])) {
        $userReceiverID =  $data['user']['id'];
    } else {
        $chatClients[$rid]->close();
        return;
    }
    if (!empty($data['token'])) {
        $authUser = UserModel::findIdentityByAccessToken($data['token']);
        if (empty($authUser)) {
            $chatClients[$rid]->close();
            return;
        }
    } else {
        $chatClients[$rid]->close();
        return;
    }
    if ($authUser->user_type == UserModel::USER_INTERPRETER) {
        if (empty($data['asUser']) || !in_array($data['asUser'], $authUser->user_ids)) {
            $chatClients[$rid]->close();
            return;
        } else {
            $authUser = UserModel::findOne(['id' => $data['asUser']]);
        }
    }
    if (!empty($data['messages']) && is_numeric($data['messages'])) {
        $limit = (int)$data['messages'];
    } else {
        $limit = 10;
    }
    
    if (!abuseProtection($rid, $authUser->id)) {
        $conn = $chatClients[$rid];
        $conn->send(Json::encode(['type' => 'message', 'data' => ['user' => ['id' => $authUser->id], 'message' => 'You send too many messages', 'chatID' => true]]));
        $chatClients[$rid]->close();
        return;
    }
    $chatId = $chatManager->getChatIdFromDb($authUser->id, $userReceiverID);
    echo 'History request from rid: '.$rid.' and chat: '.$chatId . "\r\n";
    $chat = $chatManager->getUserChat($rid);
    $joinedUser = $chatManager->getUserByRid($rid);
    $response = [        
        'user' => $joinedUser,        
        'join' => true,
    ];
    
    //send history to user
    $response = [        
        'user' => $joinedUser,        
        'history' => $chatManager->getHistory($chat->getUid(), $authUser->id, $limit),
        'unreadedMessages' => $chatManager->getUnreadedMessages($chat->getUid(), $authUser->id),
    ];

    $conn = $chatClients[$rid];
    $conn->send(Json::encode([
        'type' => 'history',         
        'chatID' => true,
        'data' => $response
    ]));
}

function startVideoCallRequest($rid, array $data)
{
    global $chatManager, $chatClients;    
    
    if (!empty($data['token'])) {
        $authUser = UserModel::findIdentityByAccessToken($data['token']);
        if (empty($authUser)) {
            $chatClients[$rid]->close();
            return;
        }
    } else {
        $chatClients[$rid]->close();
        return;
    }
    if (!abuseProtection($rid, $authUser->id)) {
        $conn = $chatClients[$rid];
        $conn->send(Json::encode(['type' => 'message', 'data' => ['user' => ['id' => $authUser->id], 'message' => 'You send too many messages', 'chatID' => true]]));
        $chatClients[$rid]->close();
        return;
    }
    $chat = $chatManager->getUserChat($rid);
    if (!$chat) {
        return;
    }
    $result = [];
    $result['user'] = $chatManager->getUserByRid($rid);
    $result['room'] = $data['room'];
    if (!empty($data['camera']) && $data['camera'] == "on") {
        $result['camera'] = "on";
    } else {
        $result['camera'] = "off";
    }
    if (!empty($data['restart']) && $data['restart'] == "yes") {
        $result['restart'] = "yes";
    } else {
        $result['restart'] = "no";
    }
/*    foreach ($chat->getUsers() as $user) { 
        if ($user->getRid() != $rid) {
            $conn = $chatClients[$user->getRid()];
            $conn->send(Json::encode(['type' => 'startVideoCall', 'data' => $result]));
        }                  
    }*/
    $sendedTo = [];
    foreach ($chatClients as $connection) {
        $requestUser = $chatManager->getUserByRid(getResourceId($connection));
        if (!empty($requestUser) && $requestUser->id == $data['user']['id'] && empty($sendedTo[$requestUser->id])) {
            $connection->send(Json::encode(['type' => 'startVideoCall', 'data' => $result]));
            $sendedTo[$requestUser->id] = true;
        }        
    }
}

function removeslashes($string)
{
    $string=implode("",explode("\\",$string));
    return stripslashes(trim($string));
}

function getResourceId($connection)
{
    return $connection->id;
}

function closeRequest($rid)
{
    date_default_timezone_set('UTC');
    //get user for closed connection
    echo "Try to close connection with $rid \r\n";
    global $chatManager, $chatClients;
    $requestUser = $chatManager->getUserByRid($rid);
    //var_dump($requestUser);
    $chat = $chatManager->getUserChat($rid);
    //echo 'close connection to current user';
    $response = array(
        'type' => 'close',
        'chatID' => true,
        'timestamp' => date('c',time()),
        'data' => ['user' => $requestUser]
    );    
    $conn = $chatClients[$rid];
    $conn->send(Json::encode($response));    
    //echo 'remove user from chat room';
    $chatManager->removeUserFromChat($rid);
    //echo 'send notification for other users in this chat';
    $users = $chat->getUsers();
    $response = array(
        'type' => 'logout',
        'chatID' => true,
        'timestamp' => date('c',time()),
        'data' => ['user' => $requestUser]
    );
    foreach ($users as $user) {
        $conn = $chatClients[$user->getRid()];
        $conn->send(Json::encode($response));
    }

    if (isset($requestUser->id)) {
        $authUser = UserModel::findOne(['id' => $requestUser->id]);
        try {
            $authUser->cam_online = 0;    
            $authUser->save();
        } catch (\Exception $e) {

        }   
        if ($authUser->user_type == UserModel::USER_MALE) {  
            $chatId = $chat->getUid();
            $subQuery = (new Query)
                ->select('user_type, id')
                ->from([UserModel::tableName()]);
            $chatLastMessage = Chat::find()->leftJoin(Action::tableName(), 'action.id = chat.action_id')
                                ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
                                ->where(['chat_id' => $chatId])
                                ->andWhere(['>=', 'chat.created_at', date("Y-m-d H:i:s", time()-5*60)])
                                ->orderBy(['chat.created_at' => SORT_DESC])->one();           
            if (!empty($chatLastMessage)) {
                updateChatDuration($chatLastMessage, $authUser, $chatClients[$rid], $chatId);
            }
        }
    }    
}

function getAnotherUserInChat($chat, $userID)
{    
    if (!empty($chat)) {
        $users = $chat->getUsers();
        foreach ($users as $user) {
            if ($user->id != $userID) {
                return $user;
            }
        }
    }
    return (object) ['id' => null];
}

function ping()
{
    date_default_timezone_set('UTC');
    global $chatClients, $chatManager, $messagesPerMinute, $ticks;
    $ticks +=15;
    if ($ticks % 60 == 0) {
        foreach ($messagesPerMinute as $key => $value) {
            unset($messagesPerMinute[$key]);
        }
    } else if ($ticks % 30 == 0) {
        
    }
    $response = [
        "type" => "ping"
    ];
    foreach ($chatClients as $connection) {        
        $connection->send(Json::encode($response));
    }
    try {
        foreach ($chatClients as $connection) {        
            $rid = getResourceId($connection);
            $requestUser = $chatManager->getUserByRid($rid);
            $chat = $chatManager->getUserChat($rid);
            if (empty($requestUser) || empty($chat)) {
                $connection->close();
                return;
            }
            $authUser = UserModel::findOne(['id' => $requestUser->id]);
            $chatId = $chat->getUid();
            $subQuery = (new Query)
                    ->select('user_type, id')
                    ->from([UserModel::tableName()]);
            $query = Chat::find()->select('chat.*, user_type, action_creator, action_receiver')
                                ->leftJoin(Action::tableName(), 'action.id = chat.action_id')
                                ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
                                ->where(['chat_id' => $chatId])
                                ->andWhere(['>=', 'chat.created_at', date("Y-m-d H:i:s", time()-5*60-15)]);
            $chatLastMessage = $query->orderBy(['chat.created_at' => SORT_DESC])->asArray()->one(); 
            $videoQuery = VideoChat::find()
                            ->leftJoin(Action::tableName(), 'action.id = video_chat.action_id');
            if ($authUser->user_type == UserModel::USER_MALE) {
               $videoQuery->where(['action_creator' => $authUser->id]);               
               $anotherUser = getAnotherUserInChat($chat, $authUser->id);
               $videoQuery->andWhere(['action_receiver' => $anotherUser->id]);
            } elseif ($authUser->user_type == UserModel::USER_FEMALE) {
               $videoQuery->where(['action_receiver' => $authUser->id]);
               $anotherUser = getAnotherUserInChat($chat, $authUser->id);
               $videoQuery->andWhere(['action_creator' => $anotherUser->id]);
            }                                                       
            $videoQuery->andWhere(['>=', 'video_chat.updated_at', date("Y-m-d H:i:s", time()-5*60-15)]);
            $chatVideoSession = $videoQuery->orderBy(['video_chat.updated_at' => SORT_DESC])->asArray()->one();

            if ($authUser->user_type == UserModel::USER_MALE) {                               
                if (!empty($chatLastMessage) || !empty($chatVideoSession)) {
                    updateChatDuration((object)$chatLastMessage, $authUser, $connection, $chatId);
                } elseif (empty($chatLastMessage) && empty($chatVideoSession) && date("Y-m-d H:i:s", time()-5*60) >= $requestUser->getLastChatActivity()) {
                    $connection->send(Json::encode(['type' => 'warning', 'data' => ['user' => ['id' => $authUser->id], 'message' => 'Chat was closed due user inactivity. Refresh this page to restart chat.', 'chatID' => true]]));
                    $connection->close();
                }
            }
            // man send message and didn't receive answer in 1 minute
            // var_dump($requestUser->getLastChatActivity());
            if ($authUser->user_type == UserModel::USER_FEMALE && !empty($chatLastMessage) && $chatLastMessage['user_type'] == UserModel::USER_MALE && (date("Y-m-d H:i:s", time()-1*76) <= $chatLastMessage['created_at']) && (date("Y-m-d H:i:s", time()-1*60) >= $chatLastMessage['created_at'])) {
                $missingChats                  = new MissingChat();
                $missingChats->chat_session_id = $chatLastMessage['chat_session_id'];
                $missingChats->action_id       = $chatLastMessage['action_id'];
                $missingChats->chat_creator     = $chatLastMessage['action_creator'];
                $missingChats->chat_receiver    = $chatLastMessage['action_receiver'];
                $missingChats->created_at = date("Y-m-d H:i:s", time());
                if (!$missingChats->save()) {
                    echo 'Can\'t be saved chat session. Errors: '. join(', ', $missingChats->getFirstErrors());
                    $connection->close();
                }
            }
            // close chat after 5 minute of inactivity
            //var_dump($chatLastMessage);

            if ( (empty($chatVideoSession) && !empty($chatLastMessage) && $authUser->cam_online == 0 && date("Y-m-d H:i:s", time()-5*60) >= $chatLastMessage['created_at']) || (empty($chatVideoSession) && $authUser->user_type == UserModel::USER_MALE && date("Y-m-d H:i:s", time()-5*60) >= $requestUser->getLastChatActivity())) {
                $videoIsOff = true;
                $users = $chat->getUsers();
                /*foreach ($users as $user) {
                    $chatUser = UserModel::findOne(['id' => $user->id]);
                    if ($chatUser->cam_online == 1 && $chatUser->user_type == UserModel::USER_FEMALE) {
                        foreach ($users as $userItem) {
                            if ($userItem->id != $chatUser->id) {
                                
                                if (!empty($chatVideoSession)) {
                                    $chatManager->storeMessage($user, $chat, $userItem->id, null, time());
                                    $videoIsOff = false;
                                }
                            }
                        }
                    }
                }*/
                if ($videoIsOff) {
                    echo 'Inactivity ' . "\r\n";
                    foreach ($users as $user) {
                        $conn = $chatClients[$user->getRid()];
                        $conn->send(Json::encode(['type' => 'warning', 'data' => ['user' => ['id' => $authUser->id], 'message' => 'Chat was closed due user inactivity. Refresh this page to restart chat.', 'chatID' => true]]));
                        $conn->close();
                    }
                }
            }
        }
    } catch (\Exception $e) {
        echo $e->getMessage();           
        echo $e->getTraceAsString();
    } catch (\Throwable $e) { 
        echo $e->getMessage();           
        echo $e->getTraceAsString();
        //return false;
    }  
}

function updateChatDuration($chatLastMessage, $authUser, $connection, $chatId)
{
    global $chatManager;
    $rid = getResourceId($connection);
    $chat = $chatManager->getUserChat($rid);
    date_default_timezone_set('UTC');
    $videoQuery = VideoChat::find()
                            ->leftJoin(Action::tableName(), 'action.id = video_chat.action_id');
    if ($authUser->user_type == UserModel::USER_MALE) {
       $videoQuery->where(['action_creator' => $authUser->id]);               
       $anotherUser = getAnotherUserInChat($chat, $authUser->id);
       $videoQuery->andWhere(['action_receiver' => $anotherUser->id]);
    } elseif ($authUser->user_type == UserModel::USER_FEMALE) {
       $videoQuery->where(['action_receiver' => $authUser->id]);
       $anotherUser = getAnotherUserInChat($chat, $authUser->id);
       $videoQuery->andWhere(['action_creator' => $anotherUser->id]);
    } 
    $videoQuery->andWhere(['>=', 'video_chat.updated_at', date("Y-m-d H:i:s", time()-5*60-15)]);
    $chatVideoSession = $videoQuery->orderBy(['video_chat.updated_at' => SORT_DESC])->asArray()->one();
    
    if (empty($chatLastMessage) && empty($chatVideoSession)) {
        $user = $chatManager->getUserByRid(getResourceId($connection));
        $connection->send(Json::encode(['type' => 'warning', 'data' => ['user' => ['id' => $user->id], 'message' => 'Chat was closed due user inactivity. Refresh this page to restart chat.', 'chatID' => true]]));
        $connection->close();
        return;
    } else {
        if (!empty($chatVideoSession)) {
            $query = Chat::find()                                                    
                        ->where(['chat_id' => $chatId]);
            $chatLastMessage = $query->orderBy(['chat.created_at' => SORT_DESC])->one();
            if (empty($chatLastMessage)) {
                return;
            }
        }
        $chatSession = ChatSession::findOne(['id' => $chatLastMessage->chat_session_id]);
        if (empty($chatSession) || $chatSession->active == 0) {
            return;
        }
        //$tz  = new \DateTimeZone(date_default_timezone_get());        
        // looking for the first action in current chat session
        $chatLastMessage = Chat::find()->select('chat.*')
                                ->where(['chat_session_id' => $chatSession->id])
                                ->orderBy(['chat.created_at' => SORT_ASC])->asArray()->one();
        $chatLastMessage = (object)$chatLastMessage;
        $sinceLastMessage = \DateTime::createFromFormat('Y-m-d H:i:s', $chatSession->updated_at/*, $tz*/)
                      ->diff(new \DateTime('now'/*, $tz*/));
        //var_dump($chatSession->updated_at);
        //var_dump($sinceLastMessage->i*60 + $sinceLastMessage->s);                  
        $chatSession->duration += $sinceLastMessage->i*60 + $sinceLastMessage->s;
        $chatSession->updated_at = date("Y-m-d H:i:s", time());
        if (!$chatSession->save()) {
            $connection->close();
            return;
        }
        $actionType = ActionType::findOne(['name' => 'chat']);
        if (empty($actionType)) {
            $actionType = new ActionType();
            $actionType->name = 'chat';
            if (!$actionType->save()) {
                echo 'Can\'t be saved action type. Errors: '. join(', ', $actionType->getFirstErrors());
            }
        }    
        $billingService = new BillingService($authUser->id);
        $paymentsService = new PaymentsService($authUser->id);
        $userBalance = $paymentsService->getBalance();
        $chatPricePerMinute = $billingService->getActionPrice($skinID = 1, $planID = 1, $actionType->id, $authUser->id);
        $chatPrice = ($chatSession->duration)*$chatPricePerMinute/60;
        if (!is_numeric($chatPricePerMinute)) {
            echo 'Can\'t get video price';
            $connection->close();
            return;
        }
        $currentChatPrice = ($sinceLastMessage->i*60 + $sinceLastMessage->s)*$chatPricePerMinute/60;    

        if ($userBalance < $currentChatPrice) {                    
            $connection->send(Json::encode(['type' => 'error', 'data' => ['user' => ['id' => $authUser->id], 'message' => 'You have not enough money to continue chat', 'chatID' => true]]));
            $connection->close();
            return;
        }
        
        $updateSuccessfull = $paymentsService->updateCreditsByActionId((int)$chatLastMessage->action_id, $chatPrice);        
        if (!$updateSuccessfull) {
            $paymentsService->addCredits((int)$chatLastMessage->action_id, $chatPrice);
        }
        $girl = Action::find()->select(['user_type', 'status', 'agency_id'])
                          ->leftJoin(UserModel::tableName(), 'user.id = action.action_receiver')
                          ->where(['user_type' => UserModel::USER_FEMALE, 'status' => UserModel::STATUS_ACTIVE, 'action.id' => $chatLastMessage->action_id])->asArray()->one();
        if (empty($girl)) {
            $girl = Action::find()->select(['user_type', 'status', 'agency_id'])
                                  ->leftJoin(UserModel::tableName(), 'user.id = action.action_creator')
                                  ->where(['user_type' => UserModel::USER_FEMALE, 'status' => UserModel::STATUS_ACTIVE, 'action.id' => $chatLastMessage->action_id])->asArray()->one();
        }
        if (!empty($girl)) {
            $girlAgency = UserModel::find()->where(['user_type' => UserModel::USER_AGENCY, 'agency_id'=> $girl['agency_id'], 'status' => UserModel::STATUS_ACTIVE])->one();
        }
        if (!empty($girlAgency)) {
            $agencyPricePerMinute = $billingService->getAgencyPrice($skinID = 1, $planID = 1, $actionType->id, $girlAgency->id);

            if (!empty($agencyPricePerMinute)) {
                $agencyRevard            = AgencyRevard::findOne(['action_id' => $chatLastMessage->action_id]);
                $increment = ($sinceLastMessage->i*60 + $sinceLastMessage->s)*$agencyPricePerMinute/60;
                if ($increment < 0.01) {
                    $increment = 0.01;
                }
                if (empty($agencyRevard)) {
                    $agencyRevard            = new AgencyRevard();
                    $agencyRevard->action_id = $chatLastMessage->action_id;
                    $agencyRevard->user_id   = $girlAgency['id'];                                    
                    $agencyRevard->amount    = $increment;
                } else {
                    $agencyRevard->amount    += $increment;
                }
                
                if (!$agencyRevard->save()) {
                    echo 'Can\'t be saved chat session. Errors: '. join(', ', $agencyRevard->getFirstErrors());
                    $connection->close();
                    return;
                }                
            }
        } else {
            echo 'Can\'t get agency';
            $connection->close();
            return;
        }                
    }    
}

$chatTimeoutWorker = new Worker();

$chatTimeoutWorker->name = "chatTimeoutWorker";

$chatTimeoutWorker->onWorkerStart = function($chatTimeoutWorker)
{
    $timerIntervalOneMinute = 5;
    Timer::add($timerIntervalOneMinute, 'checkForResponse');
};
    
function checkForResponse()
{
    //echo "get some data to support mysql connection\r\n";
    // get some data to support mysql connection
    $actionType = UserModel::find()->where(['id' => 1])->one();
}

function pongRequest($rid, array $data)
{
    if (!empty($data['token'])) {
        $authUser = UserModel::findIdentityByAccessToken($data['token']);
        if (empty($authUser)) {
            $chatClients[$rid]->close();
            return;
        }        
        if ($authUser->user_type == UserModel::USER_FEMALE) {            
            try {
                if (isset($data['video']) && $data['video'] === 1) {
                    $authUser->cam_online = 1;    
                } else {
                    $authUser->cam_online = 0;    
                }                
                $authUser->save();
            } catch (\Exception $e) {

            } 
        }
    } else {
        $chatClients[$rid]->close();
        return;
    }
    echo "Client with $rid send response to ping\r\n";
}

// Run worker
if(!defined('GLOBAL_START'))
{
    Worker::runAll();
}