<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use Workerman\Worker;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ServerController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex()
    {        

		// Create a Websocket server
		$ws_worker = new Worker("websocket://0.0.0.0:2347");

		// 4 processes
		$ws_worker->count = 1;

		// Emitted when new connection come
		$ws_worker->onConnect = function($connection)
		{
		    echo "New connection\n";
		 };

		// Emitted when data received
		$ws_worker->onMessage = function($connection, $data)
		{
		    // Send hello $data
		    $connection->send('hello ' . $data);
		};

		// Emitted when connection closed
		$ws_worker->onClose = function($connection)
		{
		    echo "Connection closed\n";
		};

		// Run worker
		Worker::runAll();
    }
}
