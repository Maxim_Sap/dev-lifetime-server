<?php

use yii\db\Migration;

/**
 * Handles the creation for table `missing_chat`.
 */
class m170506_131721_create_missing_chat_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%missing_chat}}', [
            'id' => $this->primaryKey(),                        
            'action_id'       => $this->integer()->notNull(),
            'chat_session_id' => $this->integer()->notNull(),
            'chat_creator' => $this->integer()->notNull(),
            'chat_receiver' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
        ], $tableOptions);

        $this->createIndex(
            'idx-missing_chat-action_id',
            'missing_chat',
            'action_id'
        );

        $this->addForeignKey(
            'fk-missing_chat-action_id',
            'missing_chat',
            'action_id',
            'action',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-missing_chat-chat_session_id',
            'missing_chat',
            'chat_session_id'
        );

        $this->addForeignKey(
            'fk-missing_chat-chat_session_id',
            'missing_chat',
            'chat_session_id',
            'chat_session',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-missing_chat-chat_creator',
            'missing_chat',
            'chat_creator'
        );

        $this->addForeignKey(
            'fk-missing_chat-chat_creator',
            'missing_chat',
            'chat_creator',
            'user',
            'id',
            'CASCADE'
        ); 

        $this->createIndex(
            'idx-missing_chat-chat_receiver',
            'missing_chat',
            'chat_receiver'
        );

        $this->addForeignKey(
            'fk-missing_chat-chat_receiver',
            'missing_chat',
            'chat_receiver',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-missing_chat-chat_receiver',
            'missing_chat'
        );

        $this->dropIndex(
            'idx-missing_chat-chat_receiver',
            'missing_chat'
        ); 

        $this->dropForeignKey(
            'fk-missing_chat-chat_creator',
            'missing_chat'
        );

        $this->dropIndex(
            'idx-missing_chat-chat_creator',
            'missing_chat'
        );

        $this->dropForeignKey(
            'fk-missing_chat-chat_session_id',
            'missing_chat'
        );

        $this->dropIndex(
            'idx-missing_chat-chat_session_id',
            'missing_chat'
        );        


        $this->dropForeignKey(
            'fk-missing_chat-action_id',
            'missing_chat'
        );

        $this->dropIndex(
            'idx-missing_chat-action_id',
            'missing_chat'
        );

        $this->dropTable('{{%missing_chat}}');
    }
}
