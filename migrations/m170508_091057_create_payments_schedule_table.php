<?php

use yii\db\Migration;

class m170508_091057_create_payments_schedule_table extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%payments_schedule}}', [
            'id'          => $this->primaryKey(),
            'agency_id'   => $this->integer()->notNull(),
            'period_date' => $this->date()->notNull(),
            'status'      => $this->integer(1)->notNull(),//1-current, 2-in_process, 3-closed
            'earnings'    => $this->double()->notNull(),
            'penalty'     => $this->double()->notNull(),
            'paid_value'  => $this->double()->notNull(),
            'transfer'    => $this->double()->notNull(),
            'comments'    => $this->string(),
            'payment_day' => 'timestamp NULL DEFAULT NULL ',
        ], $tableOptions);
    }

    public function down()
    {

        $this->dropTable('{{%payments_schedule}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
