<?php

use yii\db\Migration;

/**
 * Handles the creation for table `log_table`.
 */
class m170505_131700_create_log_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%log_table}}', [
            'id' => $this->primaryKey(),
            'user_id' =>  $this->integer()->notNull(),
            'user_type' => $this->integer()->notNUll(),
            'description'  => $this->string(),
            'ip' => $this->string(60),
            'created_at'  => $this->timestamp()->defaultExpression('now()'),
        ], $tableOptions);        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%log_table}}');
    }
}
