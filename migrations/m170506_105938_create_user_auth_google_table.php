<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_auth_google`.
 */
class m170506_105938_create_user_auth_google_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%user_auth_google}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->unique(),
            'google_id'    => $this->string(50)
        ], $tableOptions);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_auth_google-user_id',
            'user_auth_google',
            'user_id',
            true
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_auth_google-user_id',
            'user_auth_google',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-user_auth_google-user_id',
            'user_auth_google'
        );

        $this->dropIndex(
            'idx-user_auth_google-user_id',
            'user_auth_google'
        );

        $this->dropTable('{{%user_auth_google}}');
    }
}
