<?php

use yii\db\Migration;

/**
 * Handles the creation for table `message_table`.
 */
class m170504_080521_create_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%message}}', [
            'id' => $this->primaryKey(),
            'message_creator' => $this->integer(),
            'message_receiver' => $this->integer(),
            'title' => $this->string(),
            'description' => $this->text(),
            'reply_status' => $this->integer(1),
            'was_deleted_by_user' => $this->integer(1),
            'was_deleted_by_admin' => $this->integer(1),
            'created_at' => $this->timestamp(),
            'readed_at' => $this->timestamp()
        ], $tableOptions);

        $this->createIndex(
            'idx-message-message_creator',
            'message',
            'message_creator'
        );

         $this->addForeignKey(
            'fk-message-message_creator',
            'message',
            'message_creator',
            'user',
            'id',
            'CASCADE'
        ); 

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-message-message_creator',
            'message'
        );

        $this->dropIndex(
            'idx-message-message_creator',
            'message'
        );    

        $this->dropTable('{{%message}}');
    }
}
