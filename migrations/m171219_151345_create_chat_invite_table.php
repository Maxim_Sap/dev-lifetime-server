<?php

use yii\db\Migration;

/**
 * Handles the creation for table `chat_invite_table`.
 */
class m171219_151345_create_chat_invite_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%chat_invite}}', [
            'id' => $this->primaryKey(),
            'invite_sender' => $this->integer()->notNull(),
            'invite_receiver' => $this->integer()->notNull(),
            'message' => $this->text(),
            'show' => $this->smallInteger(4)->notNull()->defaultValue(1),
            'created_at' => $this->timestamp()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('chat_invite');
    }
}
