<?php

use yii\db\Migration;

/**
 * Handles the creation for table `chat_table`.
 */
class m170505_125650_create_chat_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%chat}}', [
            'id' => $this->primaryKey(),
            'action_id' => $this->integer()->notNull(),
            'chat_session_id' => $this->integer()->notNull(),
            'chat_id' => $this->string(60)->notNull(),
            'message' => $this->text(),
            'original_message' => $this->text(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('now()'),
            'readed_at' => $this->timestamp()->defaultValue(null),
            'edited' => $this->smallInteger(1)->defaultValue(0),
            'active' => $this->integer(1)->defaultValue(1)
        ], $tableOptions);

        $this->createIndex(
            'idx-chat-action_id',
            'chat',
            'action_id'
        );

         $this->addForeignKey(
            'fk-chat-action_id',
            'chat',
            'action_id',
            'action',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-chat-chat_session_id',
            'chat',
            'chat_session_id'
        );

        $this->addForeignKey(
            'fk-chat-chat_session_id',
            'chat',
            'chat_session_id',
            'chat_session',
            'id',
            'CASCADE'
        );         

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%chat}}');
    }
}
