<?php

use yii\db\Migration;

/**
 * Handles the creation for table `agency_revard_table`.
 */
class m171002_100353_create_rule_violation_by_agency_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%rule_violation_by_agency}}', [
            'id'            => $this->primaryKey(),
            'agency_id'     => $this->integer()->notNull(),                        
            'description'   => $this->text(), 
            'created_at'    => $this->timestamp()->notNull()->defaultExpression('now()')
        ], $tableOptions);

        $this->createIndex(
            'idx-rule_violation_by_agency-agency_id',
            'rule_violation_by_agency',
            'agency_id'
        );

        $this->addForeignKey(
            'fk-rule_violation_by_agency-agency_id',
            'rule_violation_by_agency',
            'agency_id',
            'agency',
            'id',
            'CASCADE'
        ); 

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-rule_violation_by_agency-agency_id',
            'rule_violation_by_agency'
        );

        $this->dropIndex(
            'idx-rule_violation_by_agency-agency_id',
            'rule_violation_by_agency'
        );   

        $this->dropTable('{{%rule_violation_by_agency}}');
    }
}
