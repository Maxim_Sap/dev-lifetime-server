<?php

use yii\db\Migration;

/**
 * Handles the creation for table `gift`.
 */
class m170426_144711_create_gift_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%gift}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'description' => $this->text(),
            'price' => $this->decimal(10, 2),
            'agency_price' => $this->decimal(10, 2),
            'image' => $this->string(),
            'type_id' => $this->integer()->notNull(),
            'status' => $this->integer(4)->notNull()->defaultValue(1),
            'created_at' => $this->timestamp()->defaultExpression('now()'),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%gift}}');
    }
}
