<?php

use yii\db\Migration;

class m170825_070605_referral_stats extends Migration
{
    public function up()
    {
        $tableOptions = null;
        $this->createTable('{{%referral_stats}}', [
            'id'            => $this->primaryKey(),
            'referral_link' => $this->string()->notNull(),
            'ip'            => $this->string()->notNull(),
            'type'          => $this->integer()->notNull(), // 1 - visit, 2 - sign-up
            'date_create'   => $this->timestamp()->defaultExpression('NOW()'),
        ], $tableOptions);
    }

    public function down()
    {

        $this->dropTable('{{%referral_stats}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170825_070605_referral_stats cannot be reverted.\n";

        return false;
    }
    */
}
