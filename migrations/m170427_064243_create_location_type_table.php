<?php

use yii\db\Migration;

/**
 * Handles the creation for table `location_type`.
 */
class m170427_064243_create_location_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%location_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%location_type}}');
    }
}
