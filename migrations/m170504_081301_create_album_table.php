<?php

use yii\db\Migration;

/**
 * Handles the creation for table `album_table`.
 */
class m170504_081301_create_album_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%album}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'title' => $this->string(),
            'description' => $this->string(),
            'main_photo_id' => $this->integer(),
            'public' => $this->integer(1),
            'created_at' => $this->timestamp(),
            'active' => $this->integer(1),
        ], $tableOptions);

        $this->createIndex(
            'idx-album-user_id',
            'album',
            'user_id'
        );

         $this->addForeignKey(
            'fk-album-user_id',
            'album',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-album-user_id',
            'album'
        );

        $this->dropIndex(
            'idx-album-user_id',
            'album'
        );

        $this->dropTable('{{%album}}');
    }
}
