<?php

use yii\db\Migration;

/**
 * Handles the creation for table `photo`.
 */
class m170504_081850_create_photo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%photo}}', [
            'id' => $this->primaryKey(),
            'album_id' => $this->integer(),
            'small_thumb' => $this->string(),
            'medium_thumb' => $this->string(),
            'original_image' => $this->string(),
            'watermark_small' => $this->string(),
            'watermark_medium' => $this->string(),
            'watermark_orign' => $this->string(),
            'title' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer(4),
            'premium' => $this->integer(1)->defaultValue(0),
            'approve_status' => $this->integer(4),
        ], $tableOptions);

        $this->createIndex(
            'idx-photo-album_id',
            'photo',
            'album_id'
        );

        $this->addForeignKey(
            'fk-photo-album_id',
            'photo',
            'album_id',
            'album',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-photo-album_id',
            'photo'
        );

        $this->dropIndex(
            'idx-photo-album_id',
            'photo'
        );

        $this->dropTable('{{%photo}}');
    }
}
