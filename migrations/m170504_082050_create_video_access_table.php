<?php

use yii\db\Migration;

/**
 * Handles the creation for table `video`.
 */
class m170504_082050_create_video_access_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%video_access}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'video_id' => $this->integer(),
            'started_at' => $this->timestamp()->notNull()->defaultExpression('now()'),
            'ended_at' => $this->timestamp()->notNull(),            
        ], $tableOptions);

        $this->createIndex(
            'idx-video_access-user_id',
            'video_access',
            'user_id'
        );

        $this->addForeignKey(
            'fk-video_access-user_id',
            'video_access',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx-video_access-video_id',
            'video_access',
            'video_id'
        );

        $this->addForeignKey(
            'fk-video_access-video_id',
            'video_access',
            'video_id',
            'video',
            'id',
            'CASCADE',
            'CASCADE'
        );         

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

    	$this->dropForeignKey(
            'fk-video_access-video_id',
            'video_access'
        );

        $this->dropIndex(
            'idx-video_access-video_id',
            'video_access'
        );    

        $this->dropForeignKey(
            'fk-video_access-user_id',
            'video_access'
        );

        $this->dropIndex(
            'idx-video_access-user_id',
            'video_access'
        );   

        $this->dropTable('{{%video_access}}');
    }
}
