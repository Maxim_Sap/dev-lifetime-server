<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_visiters`.
 */
class m170506_140013_create_user_visiters_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_visiters}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'visited_user_id' => $this->integer()->notNull(),
            'visit_datetime' => $this->timestamp()->notNull()->defaultExpression('now()')
        ], $tableOptions);

        $this->createIndex(
            'idx-user_visiters-user_id',
            'user_visiters',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_visiters-user_id',
            'user_visiters',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );     
    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-user_visiters-user_id',
            'user_visiters'
        );

        $this->dropIndex(
            'idx-user_visiters-user_id',
            'user_visiters'
        );

        $this->dropTable('{{%user_visiters}}');
    }
}
