<?php

use yii\db\Migration;

class m170831_071947_affiliates extends Migration
{
    public function up()
    {
        $tableOptions = null;
        $this->createTable('{{%affiliates}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string()->notNull(),
            'referral_link' => $this->string()->notNull(),
            'm_phone'       => $this->string(),
        ], $tableOptions);
    }

    public function down()
    {

        $this->dropTable('{{%affiliates}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_071947_affiliates cannot be reverted.\n";

        return false;
    }
    */
}
