<?php

use yii\db\Migration;

/**
 * Handles the creation for table `admin_permissions_table`.
 */
class m171115_150505_create_admin_permissions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('admin_permissions', [
            'id' => $this->primaryKey(),
            'permissions' => $this->text()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('admin_permissions');
    }
}
