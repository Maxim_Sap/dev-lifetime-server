<?php

use yii\db\Migration;

/**
 * Handles the creation for table `agency_revard_table`.
 */
class m170505_141253_create_agency_revard_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%agency_revard}}', [
            'id'            => $this->primaryKey(),
            'action_id'     => $this->integer()->notNull(),            
            'user_id'       => $this->integer()->notNull(),
            'amount'        => $this->decimal(10, 2)->notNull(), 
            'created_at'    => $this->timestamp()->notNull()->defaultExpression('now()')
        ], $tableOptions);

        $this->createIndex(
            'idx-agency_revard-user_id',
            'agency_revard',
            'user_id'
        );

        $this->addForeignKey(
            'fk-agency_revard-user_id',
            'agency_revard',
            'user_id',
            'user',
            'id',
            'CASCADE'
        ); 

        $this->createIndex(
            'idx-agency_revard-action_id',
            'agency_revard',
            'action_id'
        );

        $this->addForeignKey(
            'fk-agency_revard-action_id',
            'agency_revard',
            'action_id',
            'action',
            'id',
            'CASCADE'
        ); 

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-agency_revard-action_id',
            'agency_revard'
        );

        $this->dropIndex(
            'idx-agency_revard-action_id',
            'agency_revard'
        ); 

        $this->dropForeignKey(
            'fk-agency_revard-user_id',
            'agency_revard'
        );

        $this->dropIndex(
            'idx-agency_revard-user_id',
            'agency_revard'
        );         

        $this->dropTable('{{%agency_revard}}');
    }
}
