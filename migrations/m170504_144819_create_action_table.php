<?php

use yii\db\Migration;

/**
 * Handles the creation for table `action_table`.
 */
class m170504_144819_create_action_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%action}}', [
            'id' => $this->primaryKey(),
            'action_type_id' => $this->integer()->notNull(),
            'skin_id' => $this->integer()->notNull(),
            'action_creator' => $this->integer()->notNull(),
            'action_receiver' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('now()'),
            'updated_at' => $this->timestamp(),
        ], $tableOptions);

        // creates index for column `skin_id`
        $this->createIndex(
            'idx-action-skin_id',
            'action',
            'skin_id'
        );

        // add foreign key for table `skin`
        $this->addForeignKey(
            'fk-action-skin_id',
            'action',
            'skin_id',
            'skin',
            'id',
            'CASCADE'
        );

        // creates index for column `action_type_id`
        $this->createIndex(
            'idx-action-action_type_id',
            'action',
            'action_type_id'
        );

        // add foreign key for table `action_type`
        $this->addForeignKey(
            'fk-action-action_type',
            'action',
            'action_type_id',
            'action_type',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-action-action_creator',
            'action',
            'action_creator'
        );

        $this->addForeignKey(
            'fk-action-action_creator',
            'action',
            'action_creator',
            'user',
            'id',
            'CASCADE'
        ); 


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        // drops foreign key for table `action`
        $this->dropForeignKey(
            'fk-action-skin_id',
            'action'
        );

        // drops index for column `skin_id`
        $this->dropIndex(
            'idx-action-skin_id',
            'action'
        );

        $this->dropForeignKey(
            'fk-action-action_type',
            'action'
        );

        $this->dropIndex(
            'idx-action-action_type_id',
            'action'
        );

        $this->dropForeignKey(
            'fk-action-action_creator',
            'action'
        );

        $this->dropIndex(
            'idx-action-action_creator',
            'action'
        );        

        $this->dropTable('{{%action}}');
    }
}
