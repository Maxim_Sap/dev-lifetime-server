<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_profile`.
 */
class m170507_081008_create_user_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_profile}}', [
            'id'             => $this->primaryKey(),
            'first_name'     => $this->string(),
            'last_name'      => $this->string(),
            'phone'          => $this->string(50),
            'm_phone'          => $this->string(50),
            'payment_key'      => $this->string(250),
            'birthday'       => $this->date(),
            'sex'            => $this->smallInteger(3),  
            'religion'       => $this->smallInteger(4),          
            'marital'        => $this->integer(),
            'kids'           => $this->integer(),
            'height'         => $this->integer(),
            'weight'         => $this->integer(),
            'eyes'           => $this->integer(),
            'hair'           => $this->integer(),
            'smoking'        => $this->integer(),
            'alcohol'        => $this->integer(),  
            'ethnos'         => $this->integer(),          
            'physique'       => $this->integer(),          
            'address'        => $this->string()->defaultValue(null),
            'passport'       => $this->string()->defaultValue(null),
            'education'      => $this->integer()->defaultValue(null),
            'occupation'     => $this->string()->defaultValue(null),
            'english_level'  => $this->integer(),            
            'looking_age_from'    => $this->integer()->defaultValue(14),
            'looking_age_to'    => $this->integer()->defaultValue(70),
            'about_me'       => $this->text()->defaultValue(null),
            'hobbies'        => $this->text()->defaultValue(null),
            'my_ideal'       => $this->text()->defaultValue(null),
            'other_language' => $this->string()->defaultValue(null),
        ], $tableOptions);
        

        // add foreign key for table `user_profile`
        $this->addForeignKey(
            'fk-user-profile_id',
            'user_profile',
            'id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx-user_profile-eyes',
            'user_profile',
            'eyes'
        );

        $this->addForeignKey(
            'fk-user_profile-eyes',
            'user_profile',
            'eyes',
            'eyes_color',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx-user_profile-hair',
            'user_profile',
            'hair'
        );

        $this->addForeignKey(
            'fk-user_profile-hair',
            'user_profile',
            'hair',
            'hair_color',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx-user_profile-physique',
            'user_profile',
            'physique'
        );

        $this->addForeignKey(
            'fk-user_profile-physique',
            'user_profile',
            'physique',
            'physique',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-user_profile-physique',
            'user_profile'
        );

        $this->dropIndex(
            'idx-user_profile-physique',
            'user_profile'
        );
        $this->dropForeignKey(
            'fk-user_profile-eyes',
            'user_profile'
        );

        $this->dropIndex(
            'idx-user_profile-eyes',
            'user_profile'
        );
        $this->dropForeignKey(
            'fk-user_profile-hair',
            'user_profile'
        );

        $this->dropIndex(
            'idx-user_profile-hair',
            'user_profile'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user-profile_id',
            'user_profile'
        );
        
        $this->dropTable('{{%user_profile}}');
    }
}
