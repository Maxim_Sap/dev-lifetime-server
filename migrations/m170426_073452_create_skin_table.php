<?php

use yii\db\Migration;

/**
 * Handles the creation for table `skin`.
 */
class m170426_073452_create_skin_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%skin}}', [
            'id' => $this->primaryKey(),
            'code'    => $this->string()->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('now()'),
        ], $tableOptions);
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%skin}}');
    }
}
