<?php

use yii\db\Migration;

class m170507_214109_create_junction_action_credit_table extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%action_credit}}', [
            'action_id' => $this->integer()->notNull()->unique(),
            'credit_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'idx-action-credit_action_id',
            'action_credit',
            'action_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-action-credit_action_id',
            'action_credit',
            'action_id',
            'action',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-action-credit_credit_id',
            'action_credit',
            'credit_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-action-credit_credit_id',
            'action_credit',
            'credit_id',
            'credit',
            'id',
            'CASCADE'
        );


    }

    public function down()
    {

        $this->dropForeignKey(
            'fk-action-credit_credit_id',
            'action_credit'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-action-credit_credit_id',
            'action_credit'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-action-credit_action_id',
            'action_credit'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-action-credit_action_id',
            'action_credit'
        );
        
        $this->dropTable('{{%action_credit}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
