<?php

use yii\db\Migration;

class m170507_221120_foreigh_keys extends Migration
{
    public function up()
    {
        $this->createIndex(
            'idx-user-agency_id',
            'user',
            'agency_id'
        );

        $this->addForeignKey(
            'fk-user-agency_id',
            'user',
            'agency_id',
            'agency',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx-log_table-user_id',
            'log_table',
            'user_id'
        );

        $this->addForeignKey(
            'fk-log_table-user_id',
            'log_table',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx-log_table-user_type',
            'log_table',
            'user_type'
        );

        $this->addForeignKey(
            'fk-log_table-user_type',
            'log_table',
            'user_type',
            'user_type',
            'id',
            'CASCADE',
            'CASCADE'
        );    

        $this->createIndex(
            'idx-gift-type_id',
            'gift',
            'type_id'
        );

        $this->addForeignKey(
            'fk-gift-type_id',
            'gift',
            'type_id',
            'gift_type',
            'id',
            'CASCADE',
            'CASCADE'
        );    

        $this->createIndex(
            'idx-gift-status',
            'gift',
            'status'
        );

        $this->addForeignKey(
            'fk-gift-status',
            'gift',
            'status',
            'gift_status',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-gift-status',
            'gift'
        );
        $this->dropIndex(
            'idx-gift-status',
            'gift'
        );
        
        $this->dropForeignKey(
            'fk-gift-type_id',
            'gift'
        );
        $this->dropIndex(
            'idx-gift-type_id',
            'gift'
        );

        $this->dropForeignKey(
            'fk-user-agency_id',
            'user'
        );
        $this->dropIndex(
            'idx-user-agency_id',
            'user'
        );

        $this->dropForeignKey(
            'fk-log_table-user_id',
            'log_table'
        );
        $this->dropIndex(
            'idx-log_table-user_id',
            'log_table'
        );

        $this->dropForeignKey(
            'fk-log_table-user_type',
            'log_table'
        );
        $this->dropIndex(
            'idx-log_table-user_type',
            'log_table'
        );        

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
