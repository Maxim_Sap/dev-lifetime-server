<?php

use yii\db\Migration;

/**
 * Handles the creation for table `interpreter_table`.
 */
class m170503_113358_create_interpreter_revard_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%interpreter_revard}}', [
            'id' => $this->primaryKey(),                        
            'interpreter_type_id' => $this->integer(),
            'user_id' => $this->integer(),
            'deposit' => $this->integer(),
            'amount' => $this->decimal(10, 2),
            'fee' => $this->decimal(10, 2),
        ], $tableOptions);

        $this->createIndex(
            'idx-interpreter_revard-user_id',
            'interpreter_revard',
            'user_id'
        );

        $this->addForeignKey(
            'fk-interpreter_revard-user_id',
            'interpreter_revard',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx-interpreter_revard-interpreter_type_id',
            'interpreter_revard',
            'interpreter_type_id'
        );

        $this->addForeignKey(
            'fk-interpreter_revard-interpreter_type_id',
            'interpreter_revard',
            'interpreter_type_id',
            'interpreter_type',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-interpreter_revard-user_id',
            'interpreter_revard'
        );
        
        $this->dropIndex(
            'idx-interpreter_revard-user_id',
            'interpreter_revard'
        );

        $this->dropForeignKey(
            'fk-interpreter_revard-interpreter_type_id',
            'interpreter_revard'
        );

        $this->dropIndex(
            'idx-interpreter_revard-interpreter_type_id',
            'interpreter_revard'
        );

        $this->dropTable('{{%interpreter_revard}}');
    }
}
