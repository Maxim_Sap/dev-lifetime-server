<?php

use yii\db\Migration;

/**
 * Handles the creation for table `mailing_session_table`.
 */
class m171027_061224_create_mailing_session_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('mailing_session', [
            'id' => $this->bigPrimaryKey(),
            'sended_messages' => $this->integer(),
            'total_messages' => $this->integer(),
            'exclude_blacklist' => $this->smallInteger()->defaultValue(0),
            'paused' => $this->smallInteger()->defaultValue(0),
            'was_finished' => $this->smallInteger()->defaultValue(0),
            'started_at' => $this->timestamp(),
            'ended_at' => $this->timestamp(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('mailing_session');
    }
}
