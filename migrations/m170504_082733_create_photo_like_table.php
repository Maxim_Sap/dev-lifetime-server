<?php

use yii\db\Migration;

/**
 * Handles the creation for table `photo_like`.
 */
class m170504_082733_create_photo_like_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%photo_like}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'photo_id' => $this->integer(),
            'like' => $this->integer(1),
            'dislike' => $this->integer(1),
        ], $tableOptions);

        $this->createIndex(
            'idx-photo_like-user_id',
            'photo_like',
            'user_id'
        );

        $this->addForeignKey(
            'fk-photo_like-user_id',
            'photo_like',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx-photo_like-photo_id',
            'photo_like',
            'photo_id'
        );

        $this->addForeignKey(
            'fk-photo_like-photo_id',
            'photo_like',
            'photo_id',
            'photo',
            'id',
            'CASCADE',
            'CASCADE'
        );         

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-photo_like-photo_id',
            'photo_like'
        );

        $this->dropIndex(
            'idx-photo_like-photo_id',
            'photo_like'
        );    

        $this->dropForeignKey(
            'fk-photo_like-user_id',
            'photo_like'
        );

        $this->dropIndex(
            'idx-photo_like-user_id',
            'photo_like'
        );   

        $this->dropTable('{{%photo_like}}');
    }
}
