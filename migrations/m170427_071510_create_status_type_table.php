<?php

use yii\db\Migration;

/**
 * Handles the creation for table `status_type_table`.
 */
class m170427_071510_create_status_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%status_type}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description' => $this->string()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%status_type}}');
    }
}
