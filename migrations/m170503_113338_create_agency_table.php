<?php

use yii\db\Migration;

/**
 * Handles the creation for table `agency`.
 */
class m170503_113338_create_agency_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%agency}}', [
            'id'               => $this->primaryKey(),
            'name'             => $this->string(),
            'contact_person'   => $this->string(),
            'bank_detail'      => $this->string(),
            'passport_number'  => $this->string(30),
            'office_address'   => $this->string(150),
            'skype'            => $this->string(150),
            'director_address' => $this->string(150),
            'office_phone'     => $this->string(50),
            'card_number'      => $this->string(50),
            'card_user_name'   => $this->string(),
            'approve_status'   => $this->smallInteger(3),
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropTable('{{%agency}}');
    }
}
