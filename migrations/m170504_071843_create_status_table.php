<?php

use yii\db\Migration;

/**
 * Handles the creation for table `status_table`.
 */
class m170504_071843_create_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%status}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'status_type_id' => $this->integer(),
            'active' => $this->integer(1),
            'start_datetime' => $this->timestamp()->defaultExpression('now()'),
            'stop_datetime' => $this->timestamp()
        ], $tableOptions);

        $this->createIndex(
            'idx-status-user_id',
            'status',
            'user_id'
        );

        $this->addForeignKey(
            'fk-status-user_id',
            'status',
            'user_id',
            'user',
            'id',
            'CASCADE'
        ); 

        $this->createIndex(
            'idx-status-status_type_id',
            'status',
            'status_type_id'
        );

        $this->addForeignKey(
            'fk-status-status_type_id',
            'status',
            'status_type_id',
            'status_type',
            'id',
            'CASCADE'
        );         

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-status-user_id',
            'status'
        );
        
        $this->dropIndex(
            'idx-status-user_id',
            'status'
        );

        $this->dropForeignKey(
            'fk-status-status_type_id',
            'status'
        );
        
        $this->dropIndex(
            'idx-status-status_type_id',
            'status'
        );

        $this->dropTable('{{%status}}');
    }
}
