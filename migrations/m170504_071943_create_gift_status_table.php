<?php

use yii\db\Migration;

/**
 * Handles the creation for table `status_table`.
 */
class m170504_071943_create_gift_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%gift_status}}', [
            'id' => $this->primaryKey(),
            'description' => $this->string(),
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropTable('{{%gift_status}}');
    }
}
