<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_blacklist`.
 */
class m170506_095938_create_user_blacklist_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%user_blacklist}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'blacklist_user_id' => $this->integer()->notNull(),
        ], $tableOptions);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_blacklist_list-user_id',
            'user_blacklist',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_blacklist_list-user_id',
            'user_blacklist',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `blacklist_user_id`
        $this->createIndex(
            'idx-user_blacklist_list-blacklist_user_id',
            'user_blacklist',
            'blacklist_user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_blacklist_list-blacklist_user_id',
            'user_blacklist',
            'blacklist_user_id',
            'user',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-user_blacklist_list-blacklist_user_id',
            'user_blacklist'
        );

        $this->dropIndex(
            'idx-user_blacklist_list-blacklist_user_id',
            'user_blacklist'
        );

        $this->dropForeignKey(
            'fk-user_blacklist_list-user_id',
            'user_blacklist'
        );

        $this->dropIndex(
            'idx-user_blacklist_list-user_id',
            'user_blacklist'
        );

        $this->dropTable('{{%user_blacklist}}');
    }
}
