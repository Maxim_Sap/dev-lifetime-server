<?php

use yii\db\Migration;

/**
 * Handles the creation for table `mailing_template_table`.
 */
class m171025_094530_create_mailing_template_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('mailing_template', [
            'id' => $this->primaryKey(),
            'template_creator' => $this->integer()->notNull(),
            'agency_id' => $this->integer()->notNull(),
            'title' => $this->string(255),
            'body' => $this->text(),
            'created_for' => $this->smallInteger(2)->notNull()->defaultValue(0),
            'status' => $this->smallInteger(4)->notNull()->defaultValue(1),
            'created_at' => $this->timestamp()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('mailing_template');
    }
}
