<?php

use yii\db\Migration;

/**
 * Handles the creation for table `pricelist_table`.
 */
class m170503_095456_create_pricelist_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%pricelist}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'skin_id' => $this->integer(),
            'plan_id' => $this->integer(),
            'action_type_id' =>$this->integer(),
            'price' => $this->decimal(10, 2),
            'start_date' => $this->timestamp()->notNull()->defaultExpression('now()'),
            'stop_date' => $this->timestamp(),
            'important' => $this->integer(1)
        ], $tableOptions);

        $this->createIndex(
            'idx-pricelist-user_id',
            'pricelist',
            'user_id'
        );

        $this->addForeignKey(
            'fk-pricelist-user_id',
            'pricelist',
            'user_id',
            'user',
            'id',
            'CASCADE'
        ); 

        $this->createIndex(
            'idx-pricelist-plan_id',
            'pricelist',
            'plan_id'
        );

        $this->addForeignKey(
            'fk-pricelist-plan_id',
            'pricelist',
            'plan_id',
            'plan',
            'id',
            'CASCADE'
        );   

        $this->createIndex(
            'idx-pricelist-skin_id',
            'pricelist',
            'skin_id'
        );

        $this->addForeignKey(
            'fk-pricelist-skin_id',
            'pricelist',
            'skin_id',
            'skin',
            'id',
            'CASCADE'
        );   

        $this->createIndex(
            'idx-pricelist-action_type_id',
            'pricelist',
            'action_type_id'
        );

        $this->addForeignKey(
            'fk-pricelist-action_type_id',
            'pricelist',
            'action_type_id',
            'action_type',
            'id',
            'CASCADE'
        );              

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-pricelist-action_type_id',
            'pricelist'
        );
        
        $this->dropIndex(
            'idx-pricelist-action_type_id',
            'pricelist'
        );

        $this->dropForeignKey(
            'fk-pricelist-plan_id',
            'pricelist'
        );
        
        $this->dropIndex(
            'idx-pricelist-plan_id',
            'pricelist'
        );

        $this->dropForeignKey(
            'fk-pricelist-skin_id',
            'pricelist'
        );
        
        $this->dropIndex(
            'idx-pricelist-skin_id',
            'pricelist'
        );

        $this->dropForeignKey(
            'fk-pricelist-user_id',
            'pricelist'
        );
        
        $this->dropIndex(
            'idx-pricelist-user_id',
            'pricelist'
        );        

        $this->dropTable('{{%pricelist}}');
    }
}
