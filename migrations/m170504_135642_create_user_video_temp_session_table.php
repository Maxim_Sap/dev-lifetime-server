<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_video_temp_session_table`.
 */
class m170504_135642_create_user_video_temp_session_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%user_video_temp_session}}', [
            'id'                => $this->primaryKey(),
            'video_creator'     => $this->integer()->notNull(),
            'video_receiver'    => $this->integer()->notNull(),
            'created_at'        => $this->timestamp()->defaultExpression('NOW()'),
        ], $tableOptions);

        $this->createIndex(
            'idx-user_video_temp_session-video_creator',
            'user_video_temp_session',
            'video_creator'
        );

        $this->addForeignKey(
            'fk-user_video_temp_session-video_creator',
            'user_video_temp_session',
            'video_creator',
            'user',
            'id',
            'CASCADE'
        ); 

        $this->createIndex(
            'idx-user_video_temp_session-video_receiver',
            'user_video_temp_session',
            'video_receiver'
        );

        $this->addForeignKey(
            'fk-user_video_temp_session-video_receiver',
            'user_video_temp_session',
            'video_receiver',
            'user',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-user_video_temp_session-video_receiver',
            'user_video_temp_session'
        );

        $this->dropIndex(
            'idx-user_video_temp_session-video_receiver',
            'user_video_temp_session'
        );        

        $this->dropForeignKey(
            'fk-user_video_temp_session-video_creator',
            'user_video_temp_session'
        );

        $this->dropIndex(
            'idx-user_video_temp_session-video_creator',
            'user_video_temp_session'
        );


        $this->dropTable('{{%user_video_temp_session}}');
    }
}
