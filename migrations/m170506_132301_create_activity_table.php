<?php

use yii\db\Migration;

/**
 * Handles the creation for table `activity_table`.
 */
class m170506_132301_create_activity_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%activity}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11),
            'started_at' => $this->timestamp()->defaultExpression('now()'),
            'ended_at' => $this->timestamp(),
            'cam_duration' => $this->integer()
        ], $tableOptions);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-activity-user_id',
            'activity',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-activity-user_id',
            'activity',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-activity-user_id',
            'activity'
        );

        $this->dropIndex(
            'idx-activity-user_id',
            'activity'
        );

        $this->dropTable('{{%activity}}');
    }
}
