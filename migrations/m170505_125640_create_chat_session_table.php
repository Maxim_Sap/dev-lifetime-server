<?php

use yii\db\Migration;

/**
 * Handles the creation for table `chat_session_table`.
 */
class m170505_125640_create_chat_session_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%chat_session}}', [
            'id' => $this->primaryKey(),            
            'duration' => $this->integer(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('now()'),
            'updated_at' => $this->timestamp(),
            'active' => $this->integer(1)
        ], $tableOptions);


    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropTable('{{%chat_session}}');
    }
}
