<?php

use yii\db\Migration;

class m170706_143548_subscribe extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%subscribe}}', [
            'id'         => $this->primaryKey(),
            'skin_id'    => $this->integer(),
            'email'      => $this->string(),
            'created_at' => $this->timestamp(),
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropTable('{{%subscribe}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170706_143548_subscribe cannot be reverted.\n";

        return false;
    }
    */
}
