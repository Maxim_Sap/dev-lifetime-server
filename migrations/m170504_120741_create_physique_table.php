<?php

use yii\db\Migration;

/**
 * Handles the creation of table `physique_color`.
 */
class m170504_120741_create_physique_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%physique}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%physique}}');
    }
}
