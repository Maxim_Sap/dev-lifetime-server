<?php

use yii\db\Migration;

class m170703_122429_blog extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%blog}}', [
            'id'          => $this->primaryKey(),
            'title'       => $this->string(255)->notNull(),
            'description' => $this->text(),
            'image'       => $this->string(),
            'category'    => $this->string(),
            'on_main'     => $this->integer(1)->notNull()->defaultValue(0),
            'status'      => $this->integer(4)->notNull()->defaultValue(1),
            'sort_order'  => $this->integer(4)->notNull()->defaultValue(0),
            'created_at'  => $this->timestamp()->defaultExpression('now()'),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%blog}}');
    }
}
