<?php

use yii\db\Migration;
//use Yii;


/**
 * Handles the creation for table `user`.
 */
class m170503_065657_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user}}', [
            'id'                => $this->primaryKey(),            
            'email'             => $this->string(),
            'auth_key'          => $this->string()->notNull(),
            'password_hash'     => $this->string()->notNull(),
            'password_reset_token' => $this->string()->defaultValue(NULL),
            'skin_id'           => $this->integer()->notNull(),
            'user_type'         => $this->integer()->notNull(),
            'agency_id'         => $this->integer()->notNull(),            
            'referral_id'       => $this->integer()->defaultValue(NULL),
            'avatar_photo_id'   => $this->integer()->defaultValue(NULL),
            'access_token'      => $this->string(400)->notNull(),
            'token_end_date'    => $this->integer(11)->notNull(),
            'last_activity'     => $this->integer(11),
            'cam_online'        => $this->integer(1)->notNull()->defaultValue(0),
            'status'            => $this->smallInteger()->notNull()->defaultValue(10),            
            'owner_status'      => $this->smallInteger()->notNull()->defaultValue(10),
            'visible'           => $this->smallInteger()->notNull()->defaultValue(1),
            'fake'              => $this->smallInteger()->notNull()->defaultValue(0),
            'approve_status_id' => $this->integer(1)->notNull()->defaultValue(1),                        
            'created_at'        => $this->timestamp()->notNull()->defaultExpression('now()'),
        ], $tableOptions);

        // creates index for column `skin_id`
        $this->createIndex(
            'idx-user-skin_id',
            'user',
            'skin_id'
        );

        // add foreign key for table `skin`
        $this->addForeignKey(
            'fk-user-skin_id',
            'user',
            'skin_id',
            'skin',
            'id',
            'CASCADE'
        );

        // creates index for column `user_type`
        $this->createIndex(
            'idx-user-user_type',
            'user',
            'user_type'
        );

        // add foreign key for table `user_type`
        $this->addForeignKey(
            'fk-user-user_type',
            'user',
            'user_type',
            'user_type',
            'id',
            'CASCADE'
        );

    }
    

    /**
     * @inheritdoc
     */
    public function down()
    {

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user-skin_id',
            'user'
        );

        // drops index for column `skin_id`
        $this->dropIndex(
            'idx-user-skin_id',
            'user'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user-user_type',
            'user'
        );

        // drops index for column `user_type`
        $this->dropIndex(
            'idx-user-user_type',
            'user'
        );

        $this->dropTable('{{%user}}');
    }
}
