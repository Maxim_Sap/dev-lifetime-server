<?php

use yii\db\Migration;

class m170507_135253_create_pricelist_agency_table extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%pricelist_agency}}', [
            'id'   => $this->primaryKey(),
            'user_id'        => $this->integer(),
            'skin_id'        => $this->integer()->notNull(),
            'plan_id'        => $this->integer()->notNull(),
            'action_type_id' => $this->integer()->notNull(),
            'start_date'     => $this->timestamp()->notNull()->defaultExpression('now()'),
            'stop_date'      => 'timestamp NULL DEFAULT NULL ',
            'price'          => $this->decimal(10,2)->notNull(),
            'important'      => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex(
            'idx-pricelist_agency-user_id',
            'pricelist_agency',
            'user_id'
        );

        $this->addForeignKey(
            'fk-pricelist_agency-user_id',
            'pricelist_agency',
            'user_id',
            'user',
            'id',
            'CASCADE'
        ); 

        $this->createIndex(
            'idx-pricelist_agency-plan_id',
            'pricelist_agency',
            'plan_id'
        );

        $this->addForeignKey(
            'fk-pricelist_agency-plan_id',
            'pricelist_agency',
            'plan_id',
            'plan',
            'id',
            'CASCADE'
        );   

        $this->createIndex(
            'idx-pricelist_agency-skin_id',
            'pricelist_agency',
            'skin_id'
        );

        $this->addForeignKey(
            'fk-pricelist_agency-skin_id',
            'pricelist_agency',
            'skin_id',
            'skin',
            'id',
            'CASCADE'
        );   

        $this->createIndex(
            'idx-pricelist_agency-action_type_id',
            'pricelist_agency',
            'action_type_id'
        );

        $this->addForeignKey(
            'fk-pricelist_agency-action_type_id',
            'pricelist_agency',
            'action_type_id',
            'action_type',
            'id',
            'CASCADE'
        );              

    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-pricelist_agency-action_type_id',
            'pricelist_agency'
        );
        
        $this->dropIndex(
            'idx-pricelist_agency-action_type_id',
            'pricelist_agency'
        );

        $this->dropForeignKey(
            'fk-pricelist_agency-plan_id',
            'pricelist_agency'
        );
        
        $this->dropIndex(
            'idx-pricelist_agency-plan_id',
            'pricelist_agency'
        );

        $this->dropForeignKey(
            'fk-pricelist_agency-skin_id',
            'pricelist_agency'
        );
        
        $this->dropIndex(
            'idx-pricelist_agency-skin_id',
            'pricelist_agency'
        );

        $this->dropForeignKey(
            'fk-pricelist_agency-user_id',
            'pricelist_agency'
        );
        
        $this->dropIndex(
            'idx-pricelist_agency-user_id',
            'pricelist_agency'
        );        
        
        $this->dropTable('{{%pricelist_agency}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
