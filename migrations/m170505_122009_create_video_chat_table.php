<?php

use yii\db\Migration;

/**
 * Handles the creation for table `video_chat_table`.
 */
class m170505_122009_create_video_chat_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%video_chat}}', [
            'id' => $this->primaryKey(),
            'action_id' => $this->integer(),
            'video_chat_id' => $this->string(60),
            'duration' => $this->integer(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('now()'),
            'updated_at' => $this->timestamp(),
            'active' => $this->integer()
        ], $tableOptions);

        $this->createIndex(
            'idx-video_chat-action_id',
            'video_chat',
            'action_id'
        );

        $this->addForeignKey(
            'fk-video_chat-action_id',
            'video_chat',
            'action_id',
            'action',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-video_chat-action_id',
            'video_chat'
        );

        $this->dropIndex(
            'idx-video_chat-action_id',
            'video_chat'
        );
        
        $this->dropTable('{{%video_chat}}');
    }
}
