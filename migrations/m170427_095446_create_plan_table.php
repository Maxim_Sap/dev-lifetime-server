<?php

use yii\db\Migration;

/**
 * Handles the creation for table `plan`.
 */
class m170427_095446_create_plan_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%plan}}', [
            'id' => $this->primaryKey(),
            'skin_id' => $this->integer(),
            'name' => $this->string(),
            'price' => $this->decimal(10,2),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('now()')
        ], $tableOptions);

        // creates index for column `skin_id`
        $this->createIndex(
            'idx-plan-skin_id',
            'plan',
            'skin_id'
        );

        // add foreign key for table `skin`
        $this->addForeignKey(
            'fk-plan-skin_id',
            'plan',
            'skin_id',
            'skin',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-plan-skin_id',
            'plan'
        );

        $this->dropIndex(
            'idx-plan-skin_id',
            'plan'
        );

        $this->dropTable('{{%plan}}');
    }
}
