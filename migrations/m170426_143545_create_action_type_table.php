<?php

use yii\db\Migration;

/**
 * Handles the creation for table `action_type`.
 */
class m170426_143545_create_action_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%action_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'description' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('now()'),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%action_type}}');
    }
}
