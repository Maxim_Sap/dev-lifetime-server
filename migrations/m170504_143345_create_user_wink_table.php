<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_wink_table`.
 */
class m170504_143345_create_user_wink_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%user_wink}}', [
            'id' => $this->primaryKey(),
            'user_creator' => $this->integer()->notNull(),
            'user_receiver' => $this->integer()->notNull(),
            'created_at'    => $this->timestamp()->defaultExpression('now()'),
            'viewed_at'     => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex(
            'idx-user_wink-user_creator',
            'user_wink',
            'user_creator'
        );

        $this->addForeignKey(
            'fk-user_wink-user_creator',
            'user_wink',
            'user_creator',
            'user',
            'id',
            'CASCADE'
        ); 

        $this->createIndex(
            'idx-user_wink-user_receiver',
            'user_wink',
            'user_receiver'
        );

        $this->addForeignKey(
            'fk-user_wink-user_receiver',
            'user_wink',
            'user_receiver',
            'user',
            'id',
            'CASCADE'
        ); 
    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-user_wink-user_receiver',
            'user_wink'
        );

        $this->dropIndex(
            'idx-user_wink-user_receiver',
            'user_wink'
        );   

        $this->dropForeignKey(
            'fk-user_wink-user_creator',
            'user_wink'
        );

        $this->dropIndex(
            'idx-user_wink-user_creator',
            'user_wink'
        ); 
        

        $this->dropTable('{{%user_wink}}');
    }
}
