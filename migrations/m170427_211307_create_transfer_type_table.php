<?php

use yii\db\Migration;

class m170427_211307_create_transfer_type_table extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%transfer_type}}', [
            'id' => $this->primaryKey(),
            'code'          => $this->string()->notNull()->unique(),
            'title'         => $this->string()->notNull(),
        ], $tableOptions);

        $this->insert('transfer_type', ['code' => "stripe", 'title' => 'Add Funds By Stripe']);
        $this->insert('transfer_type', ['code' => "cash", 'title' => 'Add Funds By Cash']);

        
    }

    public function down()
    {
        
        $this->dropTable('{{%transfer_type}}');
    }


    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
