<?php

use yii\db\Migration;

class m170507_094452_create_pricelist_penalty_table extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%pricelist_penalty}}', [
            'id'   => $this->primaryKey(),
            'type_id' => $this->integer()->unique()->notNull(),//1 - letters, 2 - gifts
            'price'          => $this->double()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%pricelist_penalty}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
