<?php

use yii\db\Migration;

/**
 * Handles the creation for table `mailing_queue_table`.
 */
class m171027_061325_create_mailing_queue_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('mailing_queue', [
            'id' => $this->bigPrimaryKey(),
            'mailing_session_id' => $this->bigInteger()->notNull(),
            'created_for' => $this->smallInteger()->defaultValue(0),
            'template_id' => $this->integer()->notNull(),
            'sender' => $this->integer()->notNull(),
            'receiver' => $this->integer()->notNull(),
            'was_received' => $this->smallInteger()->defaultValue(0),
            'was_aborted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'received_at' => $this->timestamp()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('mailing_queue');
    }
}
