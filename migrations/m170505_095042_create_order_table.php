<?php

use yii\db\Migration;

/**
 * Handles the creation for table `order_table`.
 */
class m170505_095042_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'action_id' => $this->integer(),
            'gift_id' => $this->integer(),
            'quantity' => $this->integer(),
            'penalty_value' => $this->decimal(10, 2)->notNull()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'order_status_id' => $this->integer(4)
        ], $tableOptions);

        $this->createIndex(
            'idx-order-action_id',
            'order',
            'action_id'
        );

        $this->addForeignKey(
            'fk-order-action_id',
            'order',
            'action_id',
            'action',
            'id',
            'CASCADE'
        ); 

        $this->createIndex(
            'idx-order-gift_id',
            'order',
            'gift_id'
        );

        $this->addForeignKey(
            'fk-order-gift_id',
            'order',
            'gift_id',
            'gift',
            'id',
            'CASCADE'
        );       

        $this->createIndex(
            'idx-order-order_status_id',
            'order',
            'order_status_id'
        );

        $this->addForeignKey(
            'fk-order-order_status_id',
            'order',
            'order_status_id',
            'order_status',
            'id',
            'CASCADE'
        );          

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-order-order_status_id',
            'order'
        );
        
        $this->dropIndex(
            'idx-order-order_status_id',
            'order'
        );

        $this->dropForeignKey(
            'fk-order-gift_id',
            'order'
        );
        
        $this->dropIndex(
            'idx-order-gift_id',
            'order'
        );

        $this->dropForeignKey(
            'fk-order-action_id',
            'order'
        );
        
        $this->dropIndex(
            'idx-order-action_id',
            'order'
        );        

        $this->dropTable('{{%order}}');
    }
}
