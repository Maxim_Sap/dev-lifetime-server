<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_user`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m170607_092513_create_junction_user_and_interpreter_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_interpreter}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'interpreter_id' => $this->integer(),            
        ], $tableOptions);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_interpreter-user_id',
            'user_interpreter',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_interpreter-user_id',
            'user_interpreter',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_interpreter-interpreter_id',
            'user_interpreter',
            'interpreter_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_interpreter-interpreter_id',
            'user_interpreter',
            'interpreter_id',
            'user',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user_interpreter-user_id',
            'user_interpreter'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-user_interpreter-user_id',
            'user_interpreter'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user_interpreter-interpreter_id',
            'user_interpreter'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-user_interpreter-interpreter_id',
            'user_interpreter'
        );

        $this->dropTable('{{%user_interpreter}}');
    }
}
