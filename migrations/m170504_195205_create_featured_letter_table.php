<?php

use yii\db\Migration;

/**
 * Handles the creation for table `featured_letter_table`.
 */
class m170504_195205_create_featured_letter_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%featured_letter}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'letter_id' => $this->integer()->notNull()
        ], $tableOptions);

        $this->createIndex(
            'idx-featured_letter-user_id',
            'featured_letter',
            'user_id'
        );

        $this->addForeignKey(
            'fk-featured_letter-user_id',
            'featured_letter',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-featured_letter-letter_id',
            'featured_letter',
            'letter_id'
        );

        $this->addForeignKey(
            'fk-featured_letter-letter_id',
            'featured_letter',
            'letter_id',
            'letter',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-featured_letter-letter_id',
            'featured_letter'
        );

        $this->dropIndex(
            'idx-featured_letter-letter_id',
            'featured_letter'
        );

        $this->dropForeignKey(
            'fk-featured_letter-user_id',
            'featured_letter'
        );

        $this->dropIndex(
            'idx-featured_letter-user_id',
            'featured_letter'
        );        

        $this->dropTable('{{%featured_letter}}');
    }
}
