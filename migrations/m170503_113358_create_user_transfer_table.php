<?php

use yii\db\Migration;

/**
 * Handles the creation for table `interpreter_table`.
 */
class m170503_113358_create_user_transfer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%user_transfer}}', [
            'id' => $this->primaryKey(),                        
            'transfer_type_id' => $this->integer(),
            'user_id' => $this->integer(),
            'deposit' => $this->integer(),
            'amount' => $this->decimal(10, 2),
            'fee' => $this->decimal(10, 2),
        ], $tableOptions);

        $this->createIndex(
            'idx-user_transfer-user_id',
            'user_transfer',
            'user_id'
        );

        $this->addForeignKey(
            'fk-user_transfer-user_id',
            'user_transfer',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx-user_transfer-transfer_type_id',
            'user_transfer',
            'transfer_type_id'
        );

        $this->addForeignKey(
            'fk-user_transfer-transfer_type_id',
            'user_transfer',
            'transfer_type_id',
            'transfer_type',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-user_transfer-user_id',
            'user_transfer'
        );
        
        $this->dropIndex(
            'idx-user_transfer-user_id',
            'user_transfer'
        );

        $this->dropForeignKey(
            'fk-user_transfer-transfer_type_id',
            'user_transfer'
        );

        $this->dropIndex(
            'idx-user_transfer-transfer_type_id',
            'user_transfer'
        );

        $this->dropTable('{{%user_transfer}}');
    }
}
