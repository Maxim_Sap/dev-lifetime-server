<?php

use yii\db\Migration;

class m170508_055730_add_data extends Migration
{
    public function up()
    {        

        /* for skin table */
        $this->insert('{{%skin}}', ['code' => 'skin1', 'created_at' => date('Y-m-d H:i:s')]);
        /* for user_type table */
        $this->insert('{{%user_type}}', ['title' => "Male"]);
        $this->insert('{{%user_type}}', ['title' => "Female"]);
        $this->insert('{{%user_type}}', ['title' => "Agency"]);
        $this->insert('{{%user_type}}', ['title' => "Interpreter"]);
        $this->insert('{{%user_type}}', ['title' => "Affilate"]);
        $this->insert('{{%user_type}}', ['title' => "Superadmin"]);
        $this->insert('{{%user_type}}', ['title' => "Agency admin"]);
                
        /* for eyes color table */
        $this->insert('{{%eyes_color}}', ['name' => 'Brown', 'color' => '#8B4513']);
        $this->insert('{{%eyes_color}}', ['name' => 'Blue', 'color' => '#00BFFF']);
        $this->insert('{{%eyes_color}}', ['name' => 'Gray', 'color' => '#C0C0C0']);
        $this->insert('{{%eyes_color}}', ['name' => 'Green', 'color' => '#228B22']);
        $this->insert('{{%eyes_color}}', ['name' => 'Hazel', 'color' => '#776536']);
        $this->insert('{{%eyes_color}}', ['name' => 'Amber', 'color' => '#ffbf00']);
        /* for hair table */
        $this->insert('{{%hair_color}}', ['name' => 'Black', 'color' => '#090806']);
        $this->insert('{{%hair_color}}', ['name' => 'Blond', 'color' => '#E5C8A8']);
        $this->insert('{{%hair_color}}', ['name' => 'Brown', 'color' => '#6A4E42']);
        $this->insert('{{%hair_color}}', ['name' => 'Red', 'color' => '#B55239']);
        $this->insert('{{%hair_color}}', ['name' => 'White', 'color' => '#FFF5E1']);
        $this->insert('{{%hair_color}}', ['name' => 'Grey', 'color' => '#B7A69E']);
        /* for physique table*/
        $this->insert('{{%physique}}', ['name' => 'thin']);
        $this->insert('{{%physique}}', ['name' => 'athletic']);
        $this->insert('{{%physique}}', ['name' => 'average']);
        $this->insert('{{%physique}}', ['name' => 'a few extra pounds']);

        $this->insert('{{%action_type}}', ['name' => 'chat', 'description' =>'']);
        $this->insert('{{%action_type}}', ['name' => 'videochat', 'description' =>'']);
        $this->insert('{{%action_type}}', ['name' => 'send letter', 'description' =>'']);        
        $this->insert('{{%action_type}}', ['name' => 'send present', 'description' =>'']);        

        /* for agency table*/
         $this->insert('{{%agency}}', [
            'name' => 'Users without agency', 
            'contact_person' => 'None',
            'passport_number' => 'None',
            'office_address' => 'None',
            'office_phone' => 'None',
            'card_number' => 'None',
            'card_user_name' => 'None',
            'approve_status' => 4
        ]);       

        /* for user profile table */        
        
        $countries = [
            'Afghanistan',
            'Albania',
            'Algeria',
            'Andorra',
            'Angola',
            'Antigua and Barbuda',
            'Argentina',
            'Armenia',
            'Australia',
            'Austria',
            'Azerbaijan',
            'Bahamas',
            'Bahrain',
            'Bangladesh',
            'Barbados',
            'Belarus',
            'Belgium',
            'Belize',
            'Benin',
            'Bhutan',
            'Bolivia',
            'Bosnia and Herzegovina',
            'Botswana',
            'Brazil',
            'Brunei',
            'Bulgaria',
            'Burkina Faso',
            'Burundi',
            'Cabo Verde',
            'Cambodia',
            'Cameroon',
            'Canada',
            'Central African Republic (CAR)',
            'Chad',
            'Chile',
            'China',
            'Colombia',
            'Comoros',
            'Democratic Republic of the Congo',
            'Republic of the Congo',
            'Costa Rica',
            "Cote d'Ivoire",
            "Croatia",
            'Cuba',
            'Cyprus',
            'Czech Republic',
            'Denmark',
            'Djibouti',
            'Dominica',
            'Dominican Republic',
            'Ecuador',
            'Egypt',
            'El Salvador',
            'Equatorial Guinea',
            'Eritrea',
            'Estonia',
            'Ethiopia',
            'Fiji',
            'Finland',
            'France',
            'Gabon',
            'Gambia',
            'Georgia',
            'Germany',
            'Ghana',
            'Greece',
            'Grenada',
            'Guatemala',
            'Guinea',
            'Guinea-Bissau',
            'Guyana',
            'Haiti',
            'Honduras',
            'Hungary',
            'Iceland',
            'India',
            'Indonesia',
            'Iran',
            'Iraq',
            'Ireland',
            'Israel',
            'Italy',
            'Jamaica',
            'Japan',
            'Jordan',
            'Kazakhstan',
            'Kenya',
            'Kiribati',
            'Kosovo',
            'Kuwait',
            'Kyrgyzstan',
            'Laos',
            'Latvia',
            'Lebanon',
            'Lesotho',
            'Liberia',
            'Libya',
            'Liechtenstein',
            'Lithuania',
            'Luxembourg',
            'Macedonia (FYROM)',
            'Madagascar',
            'Malawi',
            'Malaysia',
            'Maldives',
            'Mali',
            'Malta',
            'Marshall Islands',
            'Mauritania',
            'Mauritius',
            'Mexico',
            'Micronesia',
            'Moldova',
            'Monaco',
            'Mongolia',
            'Montenegro',
            'Morocco',
            'Mozambique',
            'Myanmar (Burma)',
            'Namibia',
            'Nauru',
            'Nepal',
            'Netherlands',
            'New Zealand',
            'Nicaragua',
            'Niger',
            'Nigeria',
            'North Korea',
            'Norway',
            'Oman',
            'Pakistan',
            'Palau',
            'Palestine',
            'Panama',
            'Papua New Guinea',
            'Paraguay',
            'Peru',
            'Philippines',
            'Poland',
            'Portugal',
            'Qatar',
            'Romania',
            'Russia',
            'Rwanda',
            'Saint Kitts and Nevis',
            'Saint Lucia',
            'Saint Vincent and the Grenadines',
            'Samoa',
            'San Marino',
            'Sao Tome and Principe',
            'Saudi Arabia',
            'Senegal',
            'Serbia',
            'Seychelles',
            'Sierra Leone',
            'Singapore',
            'Slovakia',
            'Slovenia',
            'Solomon Islands',
            'Somalia',
            'South Africa',
            'South Korea',
            'South Sudan',
            'Spain',
            'Sri Lanka',
            'Sudan',
            'Suriname',
            'Swaziland',
            'Sweden',
            'Switzerland',
            'Syria',
            'Taiwan',
            'Tajikistan',
            'Tanzania',
            'Thailand',
            'Timor-Leste',
            'Togo',
            'Tonga',
            'Trinidad and Tobago',
            'Tunisia',
            'Turkey',
            'Turkmenistan',
            'Tuvalu',
            'Uganda',
            'Ukraine',
            'United Arab Emirates (UAE)',
            'United Kingdom (UK)',
            'United States of America (USA)',
            'Uruguay',
            'Uzbekistan',
            'Vanuatu',
            'Vatican City (Holy See)',
            'Venezuela',
            'Vietnam',
            'Yemen',
            'Zambia',
            'Zimbabwe'
        ];
        foreach ($countries as $country) {
            $this->insert('{{%country}}', [
                'name' => $country,
                'proxy' => 0
            ]);
        }

        $this->insert('{{%order_status}}', ['description' => "new"]);
        $this->insert('{{%order_status}}', ['description' => "wait cancel"]);
        $this->insert('{{%order_status}}', ['description' => "cancelled"]);
        $this->insert('{{%order_status}}', ['description' => "wait to approve"]);
        $this->insert('{{%order_status}}', ['description' => "approved"]);

    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
