<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_favorite_list`.
 */
class m170506_085938_create_user_favorite_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%user_favorite_list}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'favorite_user_id' => $this->integer()->notNull(),
        ], $tableOptions);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_favorite_list-user_id',
            'user_favorite_list',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_favorite_list-user_id',
            'user_favorite_list',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `favorite_user_id`
        $this->createIndex(
            'idx-user_favorite_list-favorite_user_id',
            'user_favorite_list',
            'favorite_user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_favorite_list-favorite_user_id',
            'user_favorite_list',
            'favorite_user_id',
            'user',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-user_favorite_list-favorite_user_id',
            'user_favorite_list'
        );

        $this->dropIndex(
            'idx-user_favorite_list-favorite_user_id',
            'user_favorite_list'
        );

        $this->dropForeignKey(
            'fk-user_favorite_list-user_id',
            'user_favorite_list'
        );

        $this->dropIndex(
            'idx-user_favorite_list-user_id',
            'user_favorite_list'
        );

        $this->dropTable('{{%user_favorite_list}}');
    }
}
