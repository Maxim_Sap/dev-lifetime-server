<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_profile`.
 */
class m170507_081018_create_credit_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%credit}}', [
            'id'        => $this->primaryKey(),
            'user_id' => $this->integer(),
            'amount'    => $this->decimal(10, 2),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('now()'),
        ], $tableOptions);
        
        $this->createIndex(
            'idx-credit-user_id',
            'credit',
            'user_id'
        );

        // add foreign key for table `user_profile`
        $this->addForeignKey(
            'fk-credit-user_id',
            'credit',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );       

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-credit-user_id',
            'credit'
        );

        $this->dropIndex(
            'idx-credit-user_id',
            'credit'
        );
        
        $this->dropTable('{{%credit}}');
    }
}
