<?php

use yii\db\Migration;

class m170507_221118_approve_status extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%approve_status}}', [
            'id'     => $this->primaryKey(),
            'description' => $this->string(250),
        ], $tableOptions);

        $this->insert('approve_status', ['description' => 'Not approved']);
        $this->insert('approve_status', ['description' => 'In progress']);
        $this->insert('approve_status', ['description' => 'Declined']);
        $this->insert('approve_status', ['description' => 'Approved']);
    }

    public function down()
    {
        $this->dropTable('{{%approve_status}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
