<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_video_setting_table`.
 */
class m170504_135456_create_user_video_setting_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_video_setting}}', [
            'id'                  => $this->primaryKey(),
            'video_creator'       => $this->integer()->notNull(),
            'video_receiver'      => $this->integer()->notNull(),
            'message'             => $this->string(),
            'session_description' => $this->text(),
            'created_at'          => $this->timestamp()->defaultExpression('NOW()'),
            'active'              => $this->integer()->defaultValue(1),
        ], $tableOptions);

        $this->createIndex(
            'idx-user_video_setting-video_creator',
            'user_video_setting',
            'video_creator'
        );

        $this->addForeignKey(
            'fk-user_video_setting-video_creator',
            'user_video_setting',
            'video_creator',
            'user',
            'id',
            'CASCADE'
        ); 

        $this->createIndex(
            'idx-user_video_setting-video_receiver',
            'user_video_setting',
            'video_receiver'
        );

        $this->addForeignKey(
            'fk-user_video_setting-video_receiver',
            'user_video_setting',
            'video_receiver',
            'user',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-user_video_setting-video_receiver',
            'user_video_setting'
        );

        $this->dropIndex(
            'idx-user_video_setting-video_receiver',
            'user_video_setting'
        );        

        $this->dropForeignKey(
            'fk-user_video_setting-video_creator',
            'user_video_setting'
        );

        $this->dropIndex(
            'idx-user_video_setting-video_creator',
            'user_video_setting'
        );

        $this->dropTable('{{%user_video_setting}}');
    }
}
