<?php

use yii\db\Migration;
use fzaninotto\Faker;
use app\models\Agency;
use app\models\User;

class m170508_055739_add_test_data extends Migration
{
    public function up()
    {
        $generator = \Faker\Factory::create();

        /* for interpreter_type table */
        $this->insert('{{%interpreter_type}}', ['type' => 'type1', 'description' => 'some desc']);
        $this->insert('{{%interpreter_type}}', ['type' => 'type2', 'description' => 'some desc222']);

        /* for user profile table */        
        $menList = [];
        $girlsList = [];
        for ($i=1; $i <= 50; $i++) { 

            $gender = $generator->numberBetween(1, 3);

            if ($gender == User::USER_AGENCY) {
                $this->insert('{{%agency}}', [
                    'name' => $generator->company, 
                    'contact_person' => $generator->name,
                    'passport_number' => $generator->ean8,
                    'office_address' => $generator->address,
                    'office_phone' => $generator->phoneNumber,
                    'card_number' => $generator->creditCardNumber,
                    'card_user_name' => $generator->name,
                    'approve_status' => 4
                ]);
                $agencyID = \Yii::$app->db->getLastInsertId();
            }
            $timeOffset = 1;
            if ($gender == User::USER_MALE) {
                $firstName = $generator->firstNameMale;
                $agencyID = 1;
                $menList[] = $i;
            } else if ($gender == User::USER_FEMALE) {
                $girlsList[] = $i;
                $firstName = $generator->firstNameFemale;
                $agencyCount = Agency::find()->count();
                if (!empty($agencyCount) && $agencyCount > 1) {
                    $agencyID = $generator->numberBetween(2, $agencyCount);
                } else {
                    $agencyID = 1;
                }
                $timeOffset = ($generator->numberBetween(0, 1)) ? 1 : -1;             
            } else {
                $firstName = $generator->firstName;
            }
            
            /* for user table */
            $this->insert('user', [
                'email' => $generator->email,
                'auth_key' => \Yii::$app->security->generateRandomString(40), 
                'password_hash' => '$2y$13$4p6ZRwNbVE6gi6q3tEaWJe3Cr8sGMV87jihZ4oU2.AlsxlhjPbVfS',
                'password_reset_token' => '', 
                'skin_id' => 1, 
                'user_type' => $gender,
                'agency_id' => $agencyID,
                'avatar_photo_id'  => $generator->numberBetween(1+20*($i-1), 20*$i),
                'access_token' => \Yii::$app->security->generateRandomString(40),
                'token_end_date' => time() + $generator->numberBetween(10000, 30000),
                'last_activity' => time() + $timeOffset * $generator->numberBetween(10000, 30000),
                'cam_online' => $generator->numberBetween(0, 1),                                
                'status' => User::STATUS_ACTIVE,
                'owner_status' => User::STATUS_ACTIVE,
                'visible' => ($generator->numberBetween(0, 1)) ? User::STATUS_VISIBLE : User::STATUS_INVISIBLE,                                
                'fake' => User::STATUS_FAKE_NO,
                'approve_status_id' => 4,
                'created_at' => date('Y-m-d H:i:s', time() - $generator->numberBetween(24*60*60*10, 24*60*60*40)),                        
            ]);
            $this->insert('{{%user_profile}}', [
                'id' => $i,
                'first_name' => $firstName,
                'last_name' => $generator->lastName,
                'phone' =>  $generator->phoneNumber,
                'birthday'       => $generator->dateTimeBetween($startDate = '-60 years', $endDate = '-18 years')->format('Y-m-d'),
                'sex'            => $gender,
                'religion'       => $generator->numberBetween(1, 4),
                'marital'        => $generator->numberBetween(1, 4),
                'kids'           => $generator->numberBetween(0, 5),
                'height'         => $generator->numberBetween(150, 240),
                'weight'         => $generator->numberBetween(50, 150),
                'eyes'           => $generator->numberBetween(1, 6),
                'hair'           => $generator->numberBetween(1, 6),
                'smoking'        => $generator->numberBetween(1, 3),
                'alcohol'        => $generator->numberBetween(1, 3),            
                'address'        => $generator->address(),
                'passport'       => $generator->ean8(),
                'education'      => $generator->numberBetween(1, 5),
                'ethnos'      => $generator->numberBetween(1, 5),
                'physique' => $generator->numberBetween(1, 4),
                'occupation'     => $generator->sentence(10, true),
                'english_level'  => $generator->numberBetween(1, 3),            
                'looking_age_from' => $generator->numberBetween(14, 40),
                'looking_age_to' => $generator->numberBetween(40, 70),
                'about_me'       => $generator->text(),
                'hobbies'        => $generator->text(),
                'my_ideal'       => $generator->text(),
                'other_language' => $generator->sentence(10, true),
            ]);

            $menPhotoName = [
                '1.jpg',
                '2.jpg',
                '3.jpg',
                '4.jpg',
                '5.jpg',
            ];

            $girlsPhotoName = [
                '0d00d4b6eb34611d3d4f12d8ba580e07.jpg',
                '0d7acc3a16a68e67ed945d255bbe22ea.jpg',
                '22b3c827707b9cca77d8bd6d066893e2.jpg',
                '9b07fe0c0ceeebb662331136a2b6ef61.jpg',
                '9bed1b69531dff746159714e7bf67489.jpg',
                'a400b4f90ddb58f429e9adb24715dd98.jpg',
                'c4188f76c39079072507296efc9f96bc.jpg',
                'd544277f2eb7277421cc7430fa64c5f4.jpg',
                'de15ff9b742e40d2b11fa9778c2e26e6.jpg',
                'f493008aec9959fb2851b5fcb2196690.jpg'
            ];

            /* for album table */
            $this->insert('{{%album}}', [
                'user_id' => $i,
                'title' => "Public Album",
                'description' => "Public Album",
                'main_photo_id' => $generator->numberBetween(1+20*($i-1), 20*$i),
                'public' => $generator->numberBetween(0, 1),
                'created_at' => date('Y-m-d H:i:s', time() - $generator->numberBetween(24*60*60*30, 24*60*60*90)),
                'active' => 1,
            ]);
            /* for photo table */

            for ($j=1; $j <= 20; $j++) { 
                if ($gender == User::USER_MALE) {
                    $imagePart = $menPhotoName[$generator->numberBetween(0, count($menPhotoName)-1)];
                    $smallThumb = 'uploads/thumb_63x63/' . $imagePart;
                    $mediumThumb = 'uploads/thumb_255x290/' . $imagePart;
                    $originalThumb = 'uploads/original/' . $imagePart;
                } else {
                    $imagePart = $girlsPhotoName[$generator->numberBetween(0, count($girlsPhotoName)-1)];
                    $smallThumb = 'uploads/thumb_63x63/' . $imagePart;
                    $mediumThumb = 'uploads/thumb_255x290/' . $imagePart;
                    $originalThumb = 'uploads/original/' . $imagePart;
                }
                $this->insert('{{%photo}}', [                
                    'album_id' => $i, 
                    'small_thumb' => $smallThumb, 
                    'medium_thumb' => $mediumThumb,
                    'original_image' => $originalThumb,
                    'title' => $generator->sentence(),
                    'description' => $generator->text(),
                    'status' => 1,
                    'approve_status' => 4
                ]);
            }
            /* information for table user_wink */ 
            if (count($girlsList) > 2 && count($menList) > 2) {
                $tempArray = [];
                for ($j=0; $j < 2; $j++) {
                    do {
                        if ($gender == 1) {
                            $user_creator = $girlsList[$generator->numberBetween(0, count($girlsList)-1)];
                            $user_receiver = $i;
                            if (!$user_creator) break;
                        } elseif ($gender == 2) {
                            $user_creator = $menList[$generator->numberBetween(0, count($menList)-1)];
                            $user_receiver = $i;
                            if (!$user_creator) break;
                        } else {
                            break;
                        }                    
                    } while (in_array($user_creator, $tempArray));
                    if (($gender == 1 || $gender == 2) && !in_array($user_creator, $tempArray)) {
                        $tempArray[] = $user_creator;                                
                        $this->insert('{{%user_wink}}', [
                            'user_creator' => $user_creator,
                            'user_receiver' => $user_receiver,
                            'created_at' => date('Y-m-d H:i:s', time()),
                            'viewed_at' => ($generator->numberBetween(0,1) == 1) ? date('Y-m-d H:i:s', time()+60) : null
                        ]);
                    }
                }
                /* table user_visiters */
                
                $tempArray = [];
                for ($j=0; $j < 2; $j++) { 
                    do {
                        if ($gender == 1) {
                            $user_id = $girlsList[$generator->numberBetween(0, count($girlsList)-1)];
                            $visited_user_id = $i;
                            if (!$user_id) break;
                        } elseif ($gender == 2) {
                            $user_id = $menList[$generator->numberBetween(0, count($menList)-1)];
                            $visited_user_id = $i;
                            if (!$user_id) break;
                        } else {
                            break;
                        }                     
                    } while (in_array($user_id, $tempArray));
                    if (($gender == 1 || $gender == 2) && !in_array($user_id, $tempArray)) {
                        $tempArray[] = $user_id;
                        $this->insert('{{%user_visiters}}', [
                            'user_id' => $user_id,
                            'visited_user_id' => $visited_user_id,                    
                            'visit_datetime' => date('Y-m-d H:i:s', time()+$generator->numberBetween(1000, 2000))
                        ]); 
                    }
                }
                /* table user_favorite_list */
                
                $tempArray = [];
                for ($j=0; $j < 2; $j++) { 
                    do {
                        if ($gender == 1) {
                            $user_id = $girlsList[$generator->numberBetween(0, count($girlsList)-1)];
                            $favorite_user_id = $i;
                            if (!$user_id) break;
                        } elseif ($gender == 2) {
                            $user_id = $menList[$generator->numberBetween(0, count($menList)-1)];
                            $favorite_user_id = $i;
                            if (!$user_id) break;
                        } else {
                            break;
                        }                      
                    } while (in_array($user_id, $tempArray));
                    if (($gender == 1 || $gender == 2) && !in_array($user_id, $tempArray)) {
                        $tempArray[] = $user_id;
                        $this->insert('{{%user_favorite_list}}', [
                            'user_id' => $user_id,
                            'favorite_user_id' => $favorite_user_id,                                        
                        ]); 
                    }
                }
                
                $tempArray = [];
                for ($j=0; $j < 2; $j++) {
                    do {
                        if ($gender == 1) {
                            $user_id = $girlsList[$generator->numberBetween(0, count($girlsList)-1)];
                            $blacklist_user_id = $i;
                            if (!$user_id) break;
                        } elseif ($gender == 2) {
                            $user_id = $menList[$generator->numberBetween(0, count($menList)-1)];
                            $blacklist_user_id = $i;
                            if (!$user_id) break;
                        } else {
                            break;
                        }                      
                    } while (in_array($user_id, $tempArray));
                    if (($gender == 1 || $gender == 2) && !in_array($user_id, $tempArray)) {
                        $tempArray[] = $user_id;
                        $this->insert('{{%user_blacklist}}', [
                            'user_id' => $user_id,
                            'blacklist_user_id' => $blacklist_user_id,                                        
                        ]);
                    } 
                }
            }
            

        }

        $this->insert('user', [
            'email' => 'super@admin.com',
            'auth_key' => \Yii::$app->security->generateRandomString(40), 
            'password_hash' => '$2y$13$8aptGrW9MfoeEy/7uIcG2Ok.Fu86iC.KCzmUqsgt/cC45WHa3J466',
            'password_reset_token' => '', 
            'skin_id' => 1, 
            'user_type' => 6,
            'agency_id' => 1,
            'avatar_photo_id'  => 0,
            'access_token' => '',
            'token_end_date' => time() + $generator->numberBetween(10000, 30000),
            'last_activity' => time() - $generator->numberBetween(10000, 30000),
            'cam_online' => $generator->numberBetween(0, 1),                                
            'status' => 10,
            'owner_status' => 10,
            'approve_status_id' => 4,
            'created_at' => date('Y-m-d H:i:s', time() - $generator->numberBetween(24*60*60*10, 24*60*60*40)),                        
        ]);    
        $superadminID = \Yii::$app->db->getLastInsertId();   

        $this->insert('{{%user_profile}}', [
            'id' => $superadminID,
            'first_name' => 'superadmin',
            'last_name' => 'superadmin',
            'phone' =>  null,
            'birthday'       => $generator->dateTimeThisCentury->format('Y-m-d'),
            'sex'            => $gender,
            'religion'       => $generator->numberBetween(1, 4),
            'marital'        => $generator->numberBetween(1, 4),
            'kids'           => $generator->numberBetween(0, 5),
            'height'         => $generator->numberBetween(150, 250),
            'weight'         => $generator->numberBetween(50, 200),
            'eyes'           => $generator->numberBetween(1, 6),
            'hair'           => $generator->numberBetween(1, 6),
            'smoking'        => $generator->numberBetween(1, 3),
            'alcohol'        => $generator->numberBetween(1, 3),            
            'ethnos'        => $generator->numberBetween(1, 4),  
            'physique'        => $generator->numberBetween(1, 4),
            'address'        => $generator->address(),
            'passport'       => $generator->ean8(),
            'education'      => $generator->numberBetween(0, 4),
            'occupation'     => $generator->sentence(10, true),   
            'english_level'  => $generator->numberBetween(0, 3),         
            'looking_age_from' => $generator->numberBetween(14, 40),
            'looking_age_to' => $generator->numberBetween(40, 70),
            'about_me'       => $generator->text(),
            'hobbies'        => $generator->text(),
            'my_ideal'       => $generator->text(),
        ]);


        $statusType = ['BASIC', 'BRONZE', 'SILVER', 'GOLD', 'PLATINUM'];
        foreach ($statusType as $type) {
            $this->insert('{{%status_type}}', [
                'title' => $type,
                'description' => 'Description for status ' . $type
            ]); 
        }
        $count = 0;
        $users = [];
        while ($count < 10) {
            $userID = $generator->numberBetween(1, 20);
            if (!in_array($userID, $users)) {
                $users[] = $userID;
            }
            $count = count($users);
        }
        foreach ($users as $id) {
            $this->insert('{{%status}}', [
                'user_id' => $id,
                'status_type_id' => $generator->numberBetween(1, 5),
                'active' => $generator->numberBetween(0, 1),
                'start_datetime' => date('Y-m-d H:i:s', time()),
                'stop_datetime' => date('Y-m-d H:i:s', time() + $generator->numberBetween(24*60*60, 24*60*60*30)),
            ]); 
        }
        
        $sitePages = ['about', 'instructions', 'rules'];
        foreach ($sitePages as $slug) {
            $this->insert('{{%site_page}}', [
                'slug' => $slug,
                'meta_title' => $generator->sentence(3),
                'meta_description' => $generator->sentence(4),
                'meta_keywords' => $generator->sentence(3),
                'description' => $generator->sentence(12),
            ]);
        }

        for ($i=1; $i <= 10; $i++) { 
            $this->insert('{{%city}}', [
                'name' => $generator->city,
                'proxy' => 0,
                'original' => 0
            ]);
        }
        $this->insert('{{%location_type}}', [
            'name' => 'some name 1'
        ]);
        $this->insert('{{%location_type}}', [
            'name' => 'some name 2'
        ]);

        for ($i=1; $i <= 50; $i++) { 
            $this->insert('{{%location}}', [
                'user_id' => $i,
                'location_type_id' => $generator->numberBetween(1, 2),
                'country_id' => $generator->numberBetween(1, 100),
                'city_id' => $generator->numberBetween(1, 9)
            ]);
        }

        $this->insert('{{%gift_status}}', ['description' => "new"]);
        $this->insert('{{%gift_status}}', ['description' => "wait cancel"]);
        $this->insert('{{%gift_status}}', ['description' => "cancelled"]);
        $this->insert('{{%gift_status}}', ['description' => "wait to approve"]);
        $this->insert('{{%gift_status}}', ['description' => "approved"]);

        $this->insert('{{%gift_type}}', ['title' => "accessory"]);
        $this->insert('{{%gift_type}}', ['title' => "cellphone"]);
        $this->insert('{{%gift_type}}', ['title' => "notebook"]);
        $this->insert('{{%gift_type}}', ['title' => "perfume"]);
        $this->insert('{{%gift_type}}', ['title' => "food"]);
        $this->insert('{{%gift_type}}', ['title' => "other"]);
        $this->insert('{{%gift_type}}', ['title' => "individual gifts"]);
        $this->insert('{{%gift_type}}', ['title' => "flowers"]);
        $this->insert('{{%gift_type}}', ['title' => "lingerie"]);

        for ($i=0; $i<10; $i++) {
            $this->insert('{{%gift}}', [
                'name' => $generator->sentence(3), 
                'description' => $generator->sentence(12), 
                'price' => $generator->randomFloat($nbMaxDecimals = 2, 100, 399), 
                'image' => '',
                'type_id' => $generator->numberBetween(1, 6),
                'status' => $generator->numberBetween(1, 5),
            ]);
        }

        $this->insert('{{%plan}}', ['skin_id' => 1, 'name' => 'plan_title', 'price' => 5]);

        $this->insert('{{%pricelist_agency}}', ['skin_id' => 1, 'plan_id' => 1, 'action_type_id' => 1, 'price' => 2, 'important' => 1]);
        $this->insert('{{%pricelist_agency}}', ['skin_id' => 1, 'plan_id' => 1, 'action_type_id' => 2, 'price' => 3, 'important' => 1]);
        $this->insert('{{%pricelist_agency}}', ['skin_id' => 1, 'plan_id' => 1, 'action_type_id' => 3, 'price' => 4, 'important' => 1]);
        $this->insert('{{%pricelist_agency}}', ['skin_id' => 1, 'plan_id' => 1, 'action_type_id' => 4, 'price' => 5, 'important' => 1]);
        
        $this->insert('{{%pricelist}}', ['skin_id' => 1, 'plan_id' => 1, 'action_type_id' => 1, 'price' => 5, 'important' => 1]);
        $this->insert('{{%pricelist}}', ['skin_id' => 1, 'plan_id' => 1, 'action_type_id' => 2, 'price' => 10, 'important' => 1]);
        $this->insert('{{%pricelist}}', ['skin_id' => 1, 'plan_id' => 1, 'action_type_id' => 3, 'price' => 10, 'important' => 1]);
        $this->insert('{{%pricelist}}', ['skin_id' => 1, 'plan_id' => 1, 'action_type_id' => 4, 'price' => 20, 'important' => 1]);

        $this->insert('{{%pricelist_penalty}}', ['type_id' => 1, 'price' => 0.5]);
        $this->insert('{{%pricelist_penalty}}', ['type_id' => 2, 'price' => 5]);

    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
