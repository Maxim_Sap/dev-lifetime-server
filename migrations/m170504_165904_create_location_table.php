<?php

use yii\db\Migration;

/**
 * Handles the creation for table `location`.
 */
class m170504_165904_create_location_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%location}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'location_type_id' => $this->integer(),
            'country_id' => $this->integer(),
            'city_id' => $this->integer()
        ], $tableOptions);

        $this->createIndex(
            'idx-location-user_id',
            'location',
            'user_id'
        );

        $this->addForeignKey(
            'fk-location-user_id',
            'location',
            'user_id',
            'user',
            'id',
            'CASCADE'
        ); 

        $this->createIndex(
            'idx-location-location_type_id',
            'location',
            'location_type_id'
        );

        $this->addForeignKey(
            'fk-location-location_type_id',
            'location',
            'location_type_id',
            'location_type',
            'id',
            'CASCADE'
        );  

        $this->createIndex(
            'idx-location-country_id',
            'location',
            'country_id'
        );

        $this->addForeignKey(
            'fk-location-country_id',
            'location',
            'country_id',
            'country',
            'id',
            'CASCADE'
        );                    

        $this->createIndex(
            'idx-location-city_id',
            'location',
            'city_id'
        );

        $this->addForeignKey(
            'fk-location-city_id',
            'location',
            'city_id',
            'city',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-location-city_id',
            'location'
        );

        $this->dropIndex(
            'idx-location-city_id',
            'location'
        );

        $this->dropForeignKey(
            'fk-location-country_id',
            'location'
        );

        $this->dropIndex(
            'idx-location-country_id',
            'location'
        );

        $this->dropForeignKey(
            'fk-location-location_type_id',
            'location'
        );

        $this->dropIndex(
            'idx-location-location_type_id',
            'location'
        );

        $this->dropForeignKey(
            'fk-location-user_id',
            'location'
        );

        $this->dropIndex(
            'idx-location-user_id',
            'location'
        );                

        $this->dropTable('{{%location}}');
    }
}
