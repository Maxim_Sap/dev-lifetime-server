<?php

use yii\db\Migration;

/**
 * Handles the creation for table `contact_list_table`.
 */
class m171220_070403_create_contact_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    	if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%contact_list}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'contact_id' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%contact_list}}');
    }
}
