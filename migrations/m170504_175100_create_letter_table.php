<?php

use yii\db\Migration;

/**
 * Handles the creation for table `letter_table`.
 */
class m170504_175100_create_letter_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%letter}}', [
            'id' => $this->primaryKey(),
            'action_id' => $this->integer(),
            'theme_id' => $this->integer(),
            'title' => $this->string(),
            'description' => $this->text(),
            'readed_at' => $this->timestamp(),
            'reply_status' => $this->integer(1),
            'was_deleted_by_man' => $this->integer(1),
            'was_deleted_by_girl' => $this->integer(1),
            'pay_to_agency' => $this->integer(1),
        ], $tableOptions);

        $this->createIndex(
            'idx-letter-action_id',
            'letter',
            'action_id'
        );

        $this->addForeignKey(
            'fk-letter-action_id',
            'letter',
            'action_id',
            'action',
            'id',
            'CASCADE'
        ); 

        $this->createIndex(
            'idx-letter-theme_id',
            'letter',
            'theme_id'
        );

        $this->addForeignKey(
            'fk-letter-theme_id',
            'letter',
            'theme_id',
            'letter_theme',
            'id',
            'CASCADE',
            'CASCADE'
        ); 

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-letter-theme_id',
            'letter'
        );
        
        $this->dropIndex(
            'idx-letter-theme_id',
            'letter'
        );

        $this->dropForeignKey(
            'fk-letter-action_id',
            'letter'
        );
        
        $this->dropIndex(
            'idx-letter-action_id',
            'letter'
        );

        $this->dropTable('{{%letter}}');
    }
}
