<?php

use yii\db\Migration;

/**
 * Handles the creation for table `photo`.
 */
class m170504_081950_create_photo_access_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%photo_access}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'photo_id' => $this->integer(),
            'started_at' => $this->timestamp()->notNull()->defaultExpression('now()'),
            'ended_at' => $this->timestamp()->notNull(),            
        ], $tableOptions);

        $this->createIndex(
            'idx-photo_access-user_id',
            'photo_access',
            'user_id'
        );

        $this->addForeignKey(
            'fk-photo_access-user_id',
            'photo_access',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx-photo_access-photo_id',
            'photo_access',
            'photo_id'
        );

        $this->addForeignKey(
            'fk-photo_access-photo_id',
            'photo_access',
            'photo_id',
            'photo',
            'id',
            'CASCADE',
            'CASCADE'
        );         

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

    	$this->dropForeignKey(
            'fk-photo_access-photo_id',
            'photo_access'
        );

        $this->dropIndex(
            'idx-photo_access-photo_id',
            'photo_access'
        );    

        $this->dropForeignKey(
            'fk-photo_access-user_id',
            'photo_access'
        );

        $this->dropIndex(
            'idx-photo_access-user_id',
            'photo_access'
        );   

        $this->dropTable('{{%photo_access}}');
    }
}
