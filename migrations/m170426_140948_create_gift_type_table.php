<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_type_table`.
 */
class m170426_140948_create_gift_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%gift_type}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50)->notNull()->unique(),
        ], $tableOptions);
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%gift_type}}');
    }
}
