<?php

use yii\db\Migration;

/**
 * Handles the creation for table `task_type`.
 */
class m170427_131742_create_task_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%task_type}}', [
            'id' => $this->primaryKey(),            
            'type' => $this->string(255),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%task_type}}');
    }
}
