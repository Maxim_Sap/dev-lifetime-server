<?php

use yii\db\Migration;

/**
 * Handles the creation for table `photo`.
 */
class m170504_081851_create_video_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%video}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'path'     => $this->string()->notNull(),
            'poster'      => $this->integer(),
            'description' => $this->text(),                    
            'status' => $this->smallInteger(4)->defaultValue(1),
            'public' => $this->smallInteger(1)->defaultValue(0),
            'premium' => $this->smallInteger(1)->defaultValue(0),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('now()'),
        ], $tableOptions);

        $this->createIndex(
            'idx-video-user_id',
            'video',
            'user_id'
        );

        $this->addForeignKey(
            'fk-video-user_id',
            'video',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx-video-poster',
            'video',
            'poster'
        );

        $this->addForeignKey(
            'fk-video-poster',
            'video',
            'poster',
            'photo',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-video-poster',
            'video'
        );

        $this->dropIndex(
            'idx-video-poster',
            'video'
        );

        $this->dropForeignKey(
            'fk-video-user_id',
            'video'
        );

        $this->dropIndex(
            'idx-video-user_id',
            'video'
        );

        $this->dropTable('{{%video}}');
    }
}
