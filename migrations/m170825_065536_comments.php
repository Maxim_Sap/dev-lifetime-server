<?php

use yii\db\Migration;

class m170825_065536_comments extends Migration
{
    public function up()
    {
        $tableOptions = null;
        $this->createTable('{{%comments}}', [
            'id'               => $this->primaryKey(),
            'resource_type_id' => $this->integer()->notNull(), // 1 - task, 2 - shop
            'resource_id'      => $this->integer()->notNull(),
            'user_id'          => $this->integer()->notNull(),
            'comments'         => $this->string(),
            'date_create'      => $this->timestamp()->defaultExpression('NOW()'),
            'date_read'      => $this->timestamp(),
        ], $tableOptions);
    }

    public function down()
    {

        $this->dropTable('{{%comments}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170825_065536_comments cannot be reverted.\n";

        return false;
    }
    */
}
