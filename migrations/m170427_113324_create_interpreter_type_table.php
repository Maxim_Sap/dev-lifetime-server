<?php

use yii\db\Migration;

/**
 * Handles the creation for table `interpreter_type`.
 */
class m170427_113324_create_interpreter_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%interpreter_type}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
            'description' => $this->string()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%interpreter_type}}');
    }
}
