<?php

use yii\db\Migration;

/**
 * Handles the creation for table `gift_status_table`.
 */
class m170426_144630_create_order_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%order_status}}', [
            'id' => $this->primaryKey(),
            'description' => $this->string()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%order_status}}');
    }
}
