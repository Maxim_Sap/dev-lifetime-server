<?php

use yii\db\Migration;

/**
 * Handles the creation for table `task_table`.
 */
class m170504_131751_create_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%task}}', [
            'id' => $this->primaryKey(),
            'task_creator' => $this->integer()->notNull(),
            'task_performer' => $this->integer()->notNull(),
            'description'   => $this->text(),
            'comment'       => $this->text(),
            'created_at'   => $this->timestamp()->defaultExpression('now()'),
            'approved_at'  => $this->timestamp()->defaultValue(null),
            'status_id'     => $this->integer()->notNull()->defaultValue(1),
        ], $tableOptions);

        $this->createIndex(
            'idx-task-task_creator',
            'task',
            'task_creator'
        );

        $this->addForeignKey(
            'fk-task-task_creator',
            'task',
            'task_creator',
            'user',
            'id',
            'CASCADE'
        ); 

        $this->createIndex(
            'idx-task-task_performer',
            'task',
            'task_performer'
        );

        $this->addForeignKey(
            'fk-task-task_performer',
            'task',
            'task_performer',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-task-status_id',
            'task',
            'status_id'
        );

        $this->addForeignKey(
            'fk-task-status_id',
            'task',
            'status_id',
            'task_type',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%task}}');
    }
}
