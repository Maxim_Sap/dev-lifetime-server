<?php

use yii\db\Migration;

/**
 * Handles the creation for table `interpreter_table`.
 */
class m170503_113356_create_interpreter_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {            
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%interpreter}}', [
            'id' => $this->primaryKey(),                        
            'interpreter_type_id' => $this->integer(),
            'deposit' => $this->integer(),
            'amount' => $this->decimal(10, 2),
            'fee' => $this->decimal(10, 2),
        ], $tableOptions);


        $this->addForeignKey(
            'fk-user-interpreter_id',
            'interpreter',
            'id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx-interpreter-interpreter_type_id',
            'interpreter',
            'interpreter_type_id'
        );

        $this->addForeignKey(
            'fk-interpreter-interpreter_type_id',
            'interpreter',
            'interpreter_type_id',
            'interpreter_type',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-user-interpreter_id',
            'interpreter'
        );

        $this->dropForeignKey(
            'fk-interpreter-interpreter_type_id',
            'interpreter'
        );

        $this->dropIndex(
            'idx-interpreter-interpreter_type_id',
            'interpreter'
        );

        $this->dropTable('{{%interpreter}}');
    }
}
