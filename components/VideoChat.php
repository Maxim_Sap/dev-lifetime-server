<?php

namespace app\components;

use Yii;
use app\components\lib\ActiveRecord;

/**
 * This is the model class for table "video_chat".
 *
 * @property integer $id
 * @property integer $action_id
 * @property string $video_chat_id
 * @property integer $duration
 * @property string $created_at
 * @property string $updated_at
 * @property integer $active
 *
 * @property Action $action
 */
class VideoChat extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video_chat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'duration', 'active'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['video_chat_id'], 'string', 'max' => 60],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => Action::className(), 'targetAttribute' => ['action_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_id' => 'Action ID',
            'video_chat_id' => 'Video Chat ID',
            'duration' => 'Duration',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id']);
    }
}
