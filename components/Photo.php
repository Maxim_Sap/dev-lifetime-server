<?php

namespace app\components;

use Yii;
use app\components\lib\ActiveRecord;
use app\models\Album;

/**
 * This is the model class for table "photo".
 *
 * @property integer $id
 * @property integer $album_id
 * @property string $small_thumb
 * @property string $medium_thumb
 * @property string $original_image
 * @property string $title
 * @property string $description
 * @property integer $status
 * @property integer $approve_status
 *
 * @property Album $album
 * @property PhotoLike[] $photoLikes
 */
class Photo extends ActiveRecord
{
    /**
     * @inheritdoc
     */

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    const STATUS_NOT_APPROVED = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_DECLINED = 3;
    const STATUS_APPROVED = 4;

    public static function tableName()
    {
        return 'photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['album_id', 'status', 'approve_status'], 'integer'],
            [['approve_status'], 'required'],
            [['small_thumb', 'medium_thumb', 'original_image', 'title', 'description'], 'string', 'max' => 255],
            [['album_id'], 'exist', 'skipOnError' => true, 'targetClass' => Album::className(), 'targetAttribute' => ['album_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'album_id' => 'Album ID',
            'small_thumb' => 'Small Thumb',
            'medium_thumb' => 'Medium Thumb',
            'original_image' => 'Original Image',
            'title' => 'Title',
            'description' => 'Description',
            'status' => 'Status',
            'approve_status' => 'Approve Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbum()
    {
        return $this->hasOne(Album::className(), ['id' => 'album_id']);
    }

}
