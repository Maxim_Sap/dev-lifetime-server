<?php
namespace app\components;

use Yii;
use app\collections\History;

/**
 * Class VideoManager
 * @package app\components
 */
class VideoManager
{
    /** @var app\components\User[] */
    private $users = [];
    /** @var string a namespace of class to get user instance */
    public $userClassName = '\app\components\UserModel';
    /**
     * Check if user exists in list
     * return resource id if user in current videochat - else null
     *
     * @access private
     * @param $id
     * @param $videoChatId
     * @return null|int
     */
    public function isUserExistsInChat($id, $videoChatId)
    {
        foreach ($this->users as $rid => $user) {
            $videoChat = $user->getVideoChat();
            if (!$videoChat) {
                continue;
            }
            if ($user->id == $id && $videoChat->getUid() == $videoChatId) {
                return $rid;
            }
        }
        return null;
    }
    /**
     * Add new user to manager
     *
     * @access public
     * @param integer $rid
     * @param mixed $id
     * @param array $props
     * @return void
     */
    public function addUser($rid, $id, array $props = [])
    {
        $user = new User($id, $this->userClassName, $props);
        $user->setRid($rid);
        $this->users[$rid] = $user;
    }
    /**
     * Return if exists user videochat room
     *
     * @access public
     * @param $rid
     * @return app\ChatRoom|null
     */
    public function getUserVideoChat($rid)
    {
        $user = $this->getUserByRid($rid);
        return $user ? $user->getVideoChat() : null;
    }
    /**
     * Find videochat room by id, if not exists create new videochat room
     * and assign to user by resource id
     *
     * @access public
     * @param $videoChatId
     * @param $rid
     * @return app\ChatRoom|null
     */
    public function findVideoChat($videoChatId, $rid)
    {
        $videoChat = null;
        $storedUser = $this->getUserByRid($rid);
        foreach ($this->users as $user) {
            $userChat = $user->getVideoChat();
            if (!$userChat) {
                continue;
            }            
            if ($userChat->getUid() == $videoChatId) {
                $videoChat = $userChat;
                echo 'User('.$user->id.') will be joined to: '.$videoChatId . "\r\n";
                break;
            }
        }
        if (!$videoChat) {
            echo 'New videochat room: '.$videoChatId.' for user: '.$storedUser->id . "\r\n";
            $videoChat = new VideoChatRoom();
            $videoChat->setUid($videoChatId);
        }
        $storedUser->setVideoChat($videoChat);
        return $videoChat;
    }
    /**
     * Get user by resource id
     *
     * @access public
     * @param $rid
     * @return User
     */
    public function getUserByRid($rid)
    {
        return !empty($this->users[$rid]) ? $this->users[$rid] : null;
    }
    /**
     * Find user by resource id and remove it from videochat
     *
     * @access public
     * @param $rid
     * @return void
     */
    public function removeUserFromVideoChat($rid)
    {
        $user = $this->getUserByRid($rid);
        if (!$user) {
            return;
        }
        $videoChat = $user->getVideoChat();
        if ($videoChat) {
            $videoChat->removeUser($user);
        }
        unset($this->users[$rid]);
    }
    /**
     * Store video params in database
     *
     * @access public
     * @param app\User $user
     * @param app\VideoChatRoom $videoChat
     * @param string $message
     */
    public function storeVideoParams($fromUser, $videoChatId, $toUser, $timestamp)
    {
        $params = [
            'video_chat_id' => $videoChatId,            
            'action_creator' => $fromUser,
            'action_receiver' => $toUser,
            'created_at' => date('Y-m-d H:i:s', $timestamp),
            'active' => 1
        ];
        AbstractStorage::factory()->storeVideoParams($params);
    }

}