<?php
namespace app\components;
use yii\db\Connection;

class Factory {
    static private $yii_db;
    static function getYiiDB(){
        if(empty(static::$yii_db)){
            $db_config = require __DIR__.'/../config/db.php';
            static::$yii_db =
                new Connection([
                    'dsn' => $db_config['dsn'],
                    'username' => $db_config['username'],
                    'password' => $db_config['password'],
                    'charset' => 'utf8',
                    'commandClass'=>"\\app\\components\\lib\\Command",
                ]);
        }
        return static::$yii_db;
    }
}