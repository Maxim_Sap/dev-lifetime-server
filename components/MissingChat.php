<?php

namespace app\components;

use Yii;
use app\components\lib\ActiveRecord;
/**
 * This is the model class for table "missing_chat".
 *
 * @property integer $id
 * @property integer $action_id
 * @property integer $chat_session_id
 * @property integer $chat_creator
 * @property integer $chat_receiver
 * @property string $created_at
 *
 * @property Action $action
 * @property User $chatCreator
 * @property User $chatReceiver
 * @property ChatSession $chatSession
 */
class MissingChat extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'missing_chat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'chat_session_id', 'chat_creator', 'chat_receiver'], 'required'],
            [['action_id', 'chat_session_id', 'chat_creator', 'chat_receiver'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_id' => 'Action ID',
            'chat_session_id' => 'Chat Session ID',
            'chat_creator' => 'Chat Creator',
            'chat_receiver' => 'Chat Receiver',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChatCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'chat_creator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChatReceiver()
    {
        return $this->hasOne(User::className(), ['id' => 'chat_receiver']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChatSession()
    {
        return $this->hasOne(ChatSession::className(), ['id' => 'chat_session_id']);
    }
}
