<?php
namespace app\components;
/**
 * Class ChatRoom
 * @package app\components
 */
class VideoChatRoom
{    
    /** @var string */
    private $uid;
    /** @var app\components\User[] */
    private $users = [];
    /**
     * Set videochat room unique id
     *
     * @access public
     * @param $uid
     * @return void
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }
    /**
     * Get videochat room unique id
     *
     * @access public
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }
    /**
     * Get videochat room user list
     *
     * @access public
     * @return app\User[]
     */
    public function getUsers()
    {
        return $this->users;
    }
    /**
     * Add user to videochat room
     *
     * @access public
     * @param app\User $user
     * @return void
     */
    public function addUser(User $user)
    {
        $this->users[$user->getId()] = $user;
    }
    /**
     * Remove user from videochat room
     *
     * @access public
     * @param app\User $user
     * @return void
     */
    public function removeUser(User $user)
    {
        unset($this->users[$user->getId()]);
    }
}