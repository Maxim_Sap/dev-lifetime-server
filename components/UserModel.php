<?php

namespace app\components;

use Yii;
use app\components\lib\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Query;
use yii\web\Request as WebRequest;
use Firebase\JWT\JWT;
use app\models\User;


/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $email
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $skin_id
 * @property integer $user_type
 * @property integer $avatar_photo_id
 * @property string $access_token
 * @property integer $token_end_date
 * @property integer $last_activity
 * @property integer $cam_online
 * @property integer $status
 * @property string $created_at
 *
 * @property Action[] $actions
 * @property Agency[] $agencies
 * @property AgencyRevard[] $agencyRevards
 * @property Album[] $albums
 * @property FeaturedLetter[] $featuredLetters
 * @property Interpreter[] $interpreters
 * @property Location[] $locations
 * @property LogTable[] $logTables
 * @property Message[] $messages
 * @property MissingChat[] $missingChats
 * @property MissingChat[] $missingChats0
 * @property PhotoLike[] $photoLikes
 * @property Pricelist[] $pricelists
 * @property Status[] $statuses
 * @property Task[] $tasks
 * @property Task[] $tasks0
 * @property UserProfile $profile 
 * @property UserType $userType
 * @property UserAuthFacebook $userAuthFacebook
 * @property UserAuthGoogle $userAuthGoogle
 * @property UserBlacklist[] $userBlacklists
 * @property UserBlacklist[] $userBlacklists0
 * @property UserFavoriteList[] $userFavoriteLists
 * @property UserFavoriteList[] $userFavoriteLists0
 * @property UserVideoSetting[] $userVideoSettings
 * @property UserVideoSetting[] $userVideoSettings0
 * @property UserVideoTempSession[] $userVideoTempSessions
 * @property UserVideoTempSession[] $userVideoTempSessions0
 * @property UserVisiters[] $userVisiters
 * @property UserWink[] $userWinks
 * @property UserWink[] $userWinks0
 */
class UserModel extends ActiveRecord implements IdentityInterface
{

    const STATUS_DELETED = 0;
    const STATUS_NO_ACTIVE = 3;
    const STATUS_NO_EMAIL_CONFIRM = 5;
    const STATUS_ACTIVE = 10;  
    const STATUS_VISIBLE = 10;
    const STATUS_INVISIBLE = 0;
    const STATUS_FAKE_YES = 10;
    const STATUS_FAKE_NO = 0;

    const USER_MALE = 1;
    const USER_FEMALE = 2;
    const USER_AGENCY = 3;
    const USER_INTERPRETER = 4;
    const USER_AFFILIATE = 5;
    const USER_SUPERADMIN = 6;
    const USER_ADMIN = 7;

    use \damirka\JWT\UserTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['skin_id', 'user_type', 'token_end_date', 'email'], 'required'],
            [['skin_id', 'user_type', 'avatar_photo_id', 'token_end_date', 'last_activity', 'cam_online', 'status', 'approve_status_id', 'agency_id', 'fake', 'visible'], 'integer'],
            [['created_at'], 'safe'],
            [['user_ids'], 'each', 'rule' => ['integer']],
            ['email', 'trim'],            
            ['email', 'email'],            
            [['email'], 'string', 'max' => 255],          
        ];
    }
    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'skin_id' => 'skin ID',            
            'user_type' => 'User Type',
            'avatar_photo_id' => 'Avatar Photo ID',
            'access_token' => 'Access Token',
            'token_end_date' => 'Token End Date',
            'last_activity' => 'Last Activity',
            'cam_online' => 'Cam Online',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    public function behaviors()
    {
        return [            
            [
            'class' => \voskobovich\linker\LinkerBehavior::className(),
                'relations' => [
                    'user_ids' => [
                        'users'
                    ]
                ],
            ],
        ];
    }

    public function fields()
    {
        $fieldsList = [
            // field name is the same as the attribute name
            'id',            
            'username' => function($model) {
                return $model->profile->first_name . ' ' . $model->profile->last_name;
            },
            'avatar' => function($model) {
                return (!empty($model->avatar->small_thumb)) ? $model->avatar->small_thumb : null;
            },            
            'cam_online',
            'user_type'
        ];
        $user = \Yii::$app->user->identity;
        if (!empty($user) && in_array($user->user_type, [User::USER_AGENCY, User::USER_AFFILIATE, User::USER_ADMIN, User::USER_SUPERADMIN])) {
            $fieldsList[] = 'agency_id';
            $fieldsList[] = 'access_token';
            $fieldsList[] = 'created_at';            
            $fieldsList[] = 'email';
            $fieldsList[] = 'fake';
            $fieldsList[] = 'last_activity';
            $fieldsList[] = 'owner_status';
            $fieldsList[] = 'skin_id';
            $fieldsList[] = 'status';            
            $fieldsList[] = 'token_end_date';            
            $fieldsList[] = 'visible';            
        }
        return $fieldsList;
    }

    protected static function getSecretKey()
    {
        $config = parse_ini_file(__DIR__ . '/../config/settings.ini', true);
        return $config["jwt_secret_key"];        
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }


    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => [self::STATUS_ACTIVE]]);
    }

    public function getProfile() 
    {
        return $this->hasOne(UserProfile::className(), ['id' => 'id']);
    }

    public static function findByJTI($id)
    {
        return static::findOne(['id' => $id]);
    }

    public function getJTI()
    {
        return $this->getId();
    }

    public function getAvatar()
    {
        return $this->hasOne(Photo::className(), ['id' => 'avatar_photo_id']);
    }

    public function getUsers()
    {
        return $this->hasMany(
            User::className(), 
            ['id' => 'user_id']
        )->viaTable(
            '{{%user_interpreter}}',
            ['interpreter_id' => 'id']
        );
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        $secret = static::getSecretKey();

        // Decode token and transform it into array.
        // Firebase\JWT\JWT throws exception if token can not be decoded
        try {
            $decoded = JWT::decode($token, $secret, [static::getAlgo()]);
        } catch (\Exception $e) {
            return false;
        }

        static::$decodedToken = (array) $decoded;

        // If there's no jti param - exception
        if (!isset(static::$decodedToken['jti'])) {
            return false;
        }
        // Token expired
        if (!isset(static::$decodedToken['expiresOn']) || (static::$decodedToken['expiresOn'] < time())) {
            return false;
        }


        // JTI is unique identifier of user.
        // For more details: https://tools.ietf.org/html/rfc7519#section-4.1.7
        $id = static::$decodedToken['jti'];

        return static::findByJTI($id);
    }

    public function getJWT($timeDelta = 0)
    {
        // Collect all the data
        $secret      = static::getSecretKey();
        $currentTime = time();
        $request     = Yii::$app->request;
        $hostInfo    = '';

        // There is also a \yii\console\Request that doesn't have this property
        if ($request instanceof WebRequest) {
            $hostInfo = $request->hostInfo;
        }

        if ($timeDelta == 0) {
            $timeDelta = Yii::$app->params['token_time'];
        }

        // Merge token with presets not to miss any params in custom
        // configuration
        $token = array_merge([
            'iss' => $hostInfo,
            'aud' => $hostInfo,
            'iat' => $currentTime,
            'nbf' => $currentTime
        ], static::getHeaderToken());

        // Set up id
        $token['jti'] = $this->getJTI();
        $token['expiresOn'] = $currentTime + $timeDelta;

        return JWT::encode($token, $secret, static::getAlgo());
    }

}
