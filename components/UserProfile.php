<?php

namespace app\components;

use Yii;
use app\components\lib\ActiveRecord;

/**
 * This is the model class for table "user_profile".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $birthday
 * @property integer $sex
 * @property integer $religion
 * @property integer $marital
 * @property integer $kids
 * @property integer $height
 * @property integer $weight
 * @property integer $eyes
 * @property integer $hair
 * @property integer $smoking
 * @property integer $alcohol
 * @property string $address
 * @property string $passport
 * @property integer $education
 * @property string $occupation
 * @property integer $english_level
 * @property integer $looking_age_from
 * @property integer $looking_age_to
 * @property string $about_me
 * @property string $hobbies
 * @property string $my_ideal
 * @property string $other_language
 *
 * @property User $id0
 */
class UserProfile extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['birthday'], 'safe'],
            [['sex', 'religion', 'marital', 'eyes', 'hair', 'smoking', 'ethnos', 'alcohol', 'education', 'english_level', 'looking_age_from', 'looking_age_to'], 'integer'],
            [['about_me', 'hobbies', 'my_ideal'], 'string'],
            [['first_name', 'last_name', 'address', 'passport', 'occupation', 'other_language'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 50],
            [['eyes', 'hair', 'education', 'marital', 'religion'], 'in', 'range' => [1, 2, 3, 4, 5, 6, 7, 8, 9], 'message' => 'This parameter can\'t take this value'],
            [['smoking', 'alcohol', 'english_level'], 'in', 'range' => [1,2,3,4], 'message' => 'This parameter can\'t take this value'],
            [['kids'], 'integer', 'min' => 0, 'max' => 5],
            [['height', 'weight'], 'integer', 'min' => 30, 'max' => 250],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone' => 'Phone',
            'birthday' => 'Birthday',
            'sex' => 'Sex',
            'religion' => 'Religion',
            'marital' => 'Marital',
            'kids' => 'Kids',
            'height' => 'Height',
            'weight' => 'Weight',
            'eyes' => 'Eyes',
            'hair' => 'Hair',
            'smoking' => 'Smoking',
            'alcohol' => 'Alcohol',
            'address' => 'Address',
            'passport' => 'Passport',
            'education' => 'Education',
            'occupation' => 'Occupation',
            'english_level' => 'English Level',
            'looking_age_from' => 'Looking Age From',
            'looking_age_to' => 'Looking Age To',
            'about_me' => 'About Me',
            'hobbies' => 'Hobbies',
            'my_ideal' => 'My Ideal',
            'other_language' => 'Other Language',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }


}
