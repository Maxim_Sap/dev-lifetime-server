<?php

namespace app\components;

use Yii;
use app\components\lib\ActiveRecord;

/**
 * This is the model class for table "action_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $created_at
 *
 * @property Action[] $actions
 * @property Pricelist[] $pricelists
 */
class ActionType extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'action_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['created_at'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActions()
    {
        return $this->hasMany(Action::className(), ['action_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPricelists()
    {
        return $this->hasMany(Pricelist::className(), ['action_type_id' => 'id']);
    }
}
