<?php

namespace app\components;

use Yii;
use app\components\lib\ActiveRecord;
/**
 * This is the model class for table "chat_session".
 *
 * @property integer $id
 * @property integer $duration
 * @property string $created_at
 * @property string $updated_at
 * @property integer $active
 *
 * @property Chat[] $chats
 * @property MissingChat[] $missingChats
 */
class ChatSession extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat_session';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['duration', 'active'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'duration' => 'Duration',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChats()
    {
        return $this->hasMany(Chat::className(), ['chat_session_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMissingChats()
    {
        return $this->hasMany(MissingChat::className(), ['chat_session_id' => 'id']);
    }
}
