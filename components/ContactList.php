<?php

namespace app\components;

use Yii;
use app\components\lib\ActiveRecord;
/**
 * This is the model class for table "contact_list".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $contact_id
 */
class ContactList extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'contact_id'], 'required'],
            [['user_id', 'contact_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'contact_id' => 'Contact ID',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getContact()
    {
        return $this->hasOne(User::className(), ['id' => 'contact_id']);
    }

}
