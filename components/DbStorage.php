<?php
namespace app\components;

use Yii;
use yii\db\Query;
use yii\db\Exception;
use yii\helpers\Html;
use app\components\UserModel;

use app\models\AgencyRevard;
use app\models\MissingChat;
use app\services\PaymentsService;
use app\services\BillingService;

/**
 * Class DbStorage
 *
 * This class is database storage implementation for chat messages storing
 * @package jones\wschat\components
 */
class DbStorage extends AbstractStorage
{
    /**
     * Get name of table
     *
     * @access public
     * @static
     * @return string
     */
    public static function tableName()
    {
        return 'chat';
    }

    /**
     * Get list of attributes
     *
     * @access public
     * @return array
     */
    public function attributes()
    {
        return [
            'id', 'chat_id', 'user_id', 'username', 'created_at', 'readed_at', 'message',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getHistory($chatId, $userId, $limit = 10)
    {
        try {
            $query = Chat::find()
                ->select(['chat.message', 'chat.created_at', 'chat.readed_at'])
                ->leftJoin(Action::tableName(), 'action.id = chat.action_id')
                ->leftJoin(UserProfile::tableName(), 'user_profile.id = action.action_creator')
                ->leftJoin(UserModel::tableName(), 'user.id = user_profile.id')
                ->leftJoin(Photo::tableName(), 'user.avatar_photo_id = photo.id')
                ->addSelect(['action.action_creator', 'first_name', 'photo.small_thumb'])
                ->where(['chat_id' => $chatId])
                ->andWhere(['not', ['message' => null]]);
            $query->orderBy(['created_at' => SORT_DESC]);
            if ($limit) {
                $query->limit($limit);
            }
            $results = $query->asArray()->all();
        } catch (\Exception $e) {
            echo $e->getMessage() . "\r\n";
            echo $e->getTraceAsString() . "\r\n";
            //return false;
        } catch (\Throwable $e) {
            echo $e->getMessage();
            echo $e->getTraceAsString();
            //return false;
        }
        $i         = 0;
        $newResult = [];
        foreach ($results as $result) {
            if (empty($result['first_name'])) {
                $newResult[$i]['username'] = 'Noname';
            } else {
                if ($userId == $result['action_creator']) {
                    $newResult[$i]['username'] = 'You';
                } else {
                    $newResult[$i]['username'] = $result['first_name'];
                }
            }
            $newResult[$i]['id']         = $result['action_creator'];
            $newResult[$i]['message']    = str_replace(array("\r\n", "\r", "\n"), "<br />", $result['message']);
            $newResult[$i]['avatar']     = (!empty($result['small_thumb'])) ? $result['small_thumb'] : null;
            $newResult[$i]['created_at'] = date('c', strtotime($result['created_at']));
            $newResult[$i]['readed_at']  = !empty($result['readed_at']) ? 1 : 0;
            $i++;
        }

        return $newResult;
    }

    public function removeslashes($string)
    {
        $string = implode("", explode("\\", $string));

        return stripslashes(trim($string));
    }

    public function getUnreadedMessages($chatId, $userId, $limit = 100)
    {
        try {
            $query = Chat::find()
                ->select(['chat.readed_at'])
                ->leftJoin(Action::tableName(), 'action.id = chat.action_id')
                ->where(['chat_id' => $chatId, 'chat.readed_at' => null])
                ->andWhere(['action_receiver' => $userId])
                ->andWhere(['not', ['message' => null]]);
            if ($limit) {
                $query->limit($limit);
            }
            $result = $query->count();
        } catch (\Exception $e) {
            echo $e->getMessage();

            return FALSE;
        } catch (\Throwable $e) {
            echo $e->getMessage();

            return FALSE;
        }

        return $result;
    }

    public function setAllMessageAsReaded($chatId, $userCreatorId, $userReceiverId)
    {
        date_default_timezone_set('UTC');
        try {
            $actionType = ActionType::findOne(['name' => 'chat']);
            if (empty($actionType)) {
                $actionType       = new ActionType();
                $actionType->name = 'chat';
                if (!$actionType->save()) {
                    throw new \Exception('Can\'t be saved action type. Errors: ' . join(', ', $actionType->getFirstErrors()));
                }
            }
            $actionIds = Action::find()->select('id')
                ->where([
                    'action_type_id'  => $actionType->id,
                    'action_creator'  => $userCreatorId,
                    'action_receiver' => $userReceiverId,
                ]);
            Chat::updateAll(['readed_at' => date("Y-m-d H:i:s", time())],
                ['chat_id' => $chatId, 'action_id' => $actionIds, 'readed_at' => null]);
        } catch (\Exception $e) {
            echo $e->getMessage();

            return FALSE;
        } catch (\Throwable $e) {
            echo $e->getMessage();

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @inheritdoc
     */
    public function storeMessage(array $params)
    {
        date_default_timezone_set('UTC');
        //$transaction = Yii::$app->db->beginTransaction();
        try {
            $actionType = ActionType::findOne(['name' => 'chat']);
            if (empty($actionType)) {
                $actionType       = new ActionType();
                $actionType->name = 'chat';
                if (!$actionType->save()) {
                    throw new \Exception('Can\'t be saved action type. Errors: ' . join(', ', $actionType->getFirstErrors()));
                }
            }
            $action                  = new Action();
            $action->action_type_id  = $actionType->id;
            $action->skin_id         = 1;
            $action->action_creator  = $params['action_creator'];
            $action->action_receiver = $params['action_receiver'];
            $action->created_at      = $params['created_at'];
            $action->updated_at      = $params['created_at'];

            if (!$action->save()) {
                throw new \Exception('Can\'t be saved action. Errors: ' . join(', ', $action->getFirstErrors()));
            }
            $authUser = UserModel::findOne(['id' => $params['action_creator'], 'status' => UserModel::STATUS_ACTIVE]);
            $subQuery = (new Query)
                ->select('user_type, cam_online, id')
                ->from([UserModel::tableName()]);

            $chatLastMessage = Chat::find()->select('chat.*, user_type, action_creator, action_receiver')
                                ->leftJoin(Action::tableName(), 'action.id = chat.action_id')
                                ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = action.action_creator')
                                ->where(['chat_id' => $params['chat_id']])
                                ->andWhere(['>=', 'chat.created_at', date("Y-m-d H:i:s", time()-5*60)])
                                ->orderBy(['chat.created_at' => SORT_ASC])->asArray()->one(); 
            if (empty($chatLastMessage)) {
                $chatSession = new ChatSession();
                $chatSession->created_at = $params['created_at'];                
                $chatSession->duration = 0;
                $chatSession->active = 0; // chat session inactive until man answer on girl message, or girl answer on man message
            } else {
                $chatSession = ChatSession::findOne(['id' => $chatLastMessage['chat_session_id']]);
                //$tz  = new \DateTimeZone(date_default_timezone_get());
                //echo $params['created_at'] ."\n";
                //echo $chat->created_at ."\n"; 
                // check if last chat message was written by girl and this message was written by man or last message was written by man and this message was written by girl
                if ((($chatLastMessage['user_type'] == UserModel::USER_FEMALE && $authUser->user_type == UserModel::USER_MALE) ||
                    ($chatLastMessage['user_type'] == UserModel::USER_MALE && $authUser->user_type == UserModel::USER_FEMALE) /*|| 
                    (($authUser->user_type == UserModel::USER_FEMALE && $authUser->cam_online == 1) || 
                     ($chatLastMessage['user_type'] == UserModel::USER_FEMALE && $chatLastMessage['cam_online'] == 1))*/) && 
                    $chatSession->active == 0) {
                    $chatSession->active = 1;
                    $sinceLastMessage = \DateTime::createFromFormat('Y-m-d H:i:s', $chatLastMessage['created_at']/*, $tz*/)
                     ->diff(\DateTime::createFromFormat('Y-m-d H:i:s', $params['created_at']/*, $tz*/)); 
                    // echo $sinceLastMessage->i*60 + $sinceLastMessage->s . "\n";
                    $chatSession->duration += $sinceLastMessage->i*60 + $sinceLastMessage->s;
                    if ($authUser->user_type == UserModel::USER_FEMALE) {
                        $billingService = new BillingService($chatLastMessage['action_creator']);
                        $paymentsService = new PaymentsService($chatLastMessage['action_creator']);
                    } else {
                        $billingService = new BillingService($authUser->id);
                        $paymentsService = new PaymentsService($authUser->id);                        
                    }

                    $chatPricePerMinute = $billingService->getActionPrice($skinID = 1, $planID = 1, $actionType->id, $authUser->id);
                    $chatPrice          = ($chatSession->duration) * $chatPricePerMinute / 60;
                    $updateSuccessfull  = $paymentsService->updateCreditsByActionId($chatLastMessage['action_id'], $chatPrice);
                    if (!$updateSuccessfull) {
                        $paymentsService->addCredits($chatLastMessage['action_id'], $chatPrice);
                    }
                    // send bonus to agency
                    if ($chatLastMessage['user_type'] == UserModel::USER_FEMALE) {
                        $girl       = UserModel::findOne(['id' => $chatLastMessage['action_creator']]);
                        $girlAgency = UserModel::find()->where(['user_type' => UserModel::USER_AGENCY, 'agency_id' => $girl->agency_id, 'status' => UserModel::STATUS_ACTIVE])->one();
                    } elseif ($authUser->user_type == UserModel::USER_FEMALE) {
                        $girlAgency = UserModel::find()->where(['user_type' => UserModel::USER_AGENCY, 'agency_id' => $authUser->agency_id, 'status' => UserModel::STATUS_ACTIVE])->one();
                    }
                    if (!empty($girlAgency)) {
                        $agencyPricePerMinute = $billingService->getAgencyPrice($skinID = 1, $planID = 1, $actionType->id, $girlAgency->id);

                        if (!empty($agencyPricePerMinute)) {
                            $agencyRevard            = new AgencyRevard();
                            $agencyRevard->action_id = $chatLastMessage['action_id'];
                            $agencyRevard->user_id   = $girlAgency->id;
                            $agencyRevard->amount    = ($sinceLastMessage->i * 60 + $sinceLastMessage->s) * $agencyPricePerMinute / 60;
                            if (!$agencyRevard->save()) {
                                throw new \Exception('Can\'t be saved chat session. Errors: ' . join(', ', $agencyRevard->getFirstErrors()));
                            }
                            //$letters->pay_to_agency = 1;
                        }
                    } else {
                        throw new \Exception('Can\'t get agency');
                    }
                    // girl answered on message after 1 minute
                    if ($chatLastMessage['user_type'] == UserModel::USER_MALE && $authUser->user_type == UserModel::USER_FEMALE && date("Y-m-d H:i:s", time() - 60) > $chatLastMessage['created_at']) {
                        $missingChats                  = new MissingChat();
                        $missingChats->chat_session_id = $chatSession->id;
                        $missingChats->action_id       = $chatLastMessage['action_id'];
                        $missingChats->chat_creator    = $chatLastMessage['action_creator'];
                        $missingChats->chat_receiver   = $authUser->id;
                        $missingChats->created_at      = date("Y-m-d H:i:s", time());
                        if (!$missingChats->save()) {
                            throw new \Exception('Can\'t be saved chat session. Errors: ' . join(', ', $missingChats->getFirstErrors()));
                        }
                    }
                }
            }
            $chatSession->updated_at = $params['created_at'];

            if (!$chatSession->save()) {
                throw new \Exception('Can\'t be saved chat session. Errors: ' . join(', ', $chatSession->getFirstErrors()));
            }
            $newChatMessage                   = new Chat();
            $newChatMessage->action_id        = $action->id;
            $newChatMessage->chat_session_id  = $chatSession->id;
            $newChatMessage->chat_id          = $params['chat_id'];
            $newChatMessage->message          = $params['message'];
            $newChatMessage->original_message = $params['message'];
            $newChatMessage->created_at       = $params['created_at'];
            $newChatMessage->edited           = 0;
            $newChatMessage->active           = 1;
            if (!$newChatMessage->save()) {
                throw new \Exception('Can\'t be saved chat message. Errors: ' . join(', ', $newChatMessage->getFirstErrors()));
            }

            //$transaction->commit();
        } catch (\Exception $e) {
            //$transaction->rollBack();
            echo $e->getMessage();
            echo $e->getTraceAsString();

            return FALSE;
        } catch (\Throwable $e) {
            //$transaction->rollBack();
            echo $e->getMessage();
            echo $e->getTraceAsString();

            return FALSE;
        }

        return TRUE;
    }

    public function storeVideoParams(array $params)
    {
        date_default_timezone_set('UTC');
        //$transaction = Yii::$app->db->beginTransaction();
        try {

            $actionType = ActionType::findOne(['name' => 'videochat']);
            if (empty($actionType)) {
                $actionType       = new ActionType();
                $actionType->name = 'videochat';
                if (!$actionType->save()) {
                    throw new \Exception('Can\'t be saved action type. Errors: ' . join(', ', $actionType->getFirstErrors()));
                }
            }

            $action = Action::find()->where([
                'action_type_id'  => $actionType->id,
                'action_creator'  => $params['action_creator'],
                'action_receiver' => $params['action_receiver'],
            ])
                ->andWhere(['>=', 'updated_at', date("Y-m-d H:i:s", time() - 5)])
                ->orderBy(['updated_at' => SORT_DESC])
                ->one();

            if (empty($action)) {
                $action                  = new Action();
                $action->action_type_id  = $actionType->id;
                $action->skin_id         = 1;
                $action->action_creator  = $params['action_creator'];
                $action->action_receiver = $params['action_receiver'];
                $action->created_at      = $params['created_at'];
            } else {
                $videoChat = VideoChat::find()->where(['action_id' => $action->id])
                ->andWhere(['>=', 'updated_at', date("Y-m-d H:i:s", time() - 5 * 60)])
                ->orderBy(['updated_at' => SORT_DESC])->one();
            }
            $action->updated_at = $params['created_at'];

            if (!$action->save()) {
                throw new \Exception('Can\'t be saved action. Errors: ' . join(', ', $action->getFirstErrors()));
            }
            
            if (empty($videoChat)) {
                $videoChat                = new VideoChat();
                $videoChat->action_id     = $action->id;
                $videoChat->video_chat_id = $params['video_chat_id'];
                $videoChat->created_at    = $params['created_at'];
                $videoChat->duration      = 0;
                $videoChat->active        = 1;
                $this->insertNewChatMessage($params['action_creator'], $params['action_receiver'], null, $params['created_at']);
            } else {
                $videoChat->video_chat_id = $params['video_chat_id'];
                /*                $tz  = new \DateTimeZone('Europe/Brussels');
                                echo $params['created_at'] ."\n";
                                echo $videoChat->created_at ."\n";
                                $sinceLastVideo = \DateTime::createFromFormat('Y-m-d H:i:s', $videoChat->created_at, $tz)
                                 ->diff(\DateTime::createFromFormat('Y-m-d H:i:s', $params['created_at'], $tz));
                                 echo $sinceLastMessage->i*60 + $sinceLastMessage->s . "\n";
                                $videoChat->duration += $sinceLastMessage->i*60 + $sinceLastMessage->s;*/
            }
            $videoChat->updated_at = $params['created_at'];

            if (!$videoChat->save()) {
                throw new \Exception('Can\'t be saved video chat. Errors: ' . join(', ', $videoChat->getFirstErrors()));
            }

            //$transaction->commit();
        } catch (\Exception $e) {
            //$transaction->rollBack();
            echo $e->getMessage();
            echo $e->getTraceAsString();

            return FALSE;
        } catch (\Throwable $e) {
            //$transaction->rollBack();
            echo $e->getMessage();
            echo $e->getTraceAsString();

            return FALSE;
        }

        return TRUE;
    }

    public function insertNewChatMessage($creator, $receiver, $message, $createdAt)
    {
        /******* update chat duration  ********/
        $chatId = Chat::find()
                        ->leftJoin(Action::tableName(), 'action.id = chat.action_id')
                        ->where(['or', 
                                ['action_creator' => $creator, 
                                'action_receiver' => $receiver], 
                                ['action_creator' => $receiver, 
                                'action_receiver' => $creator],
                                ])
                        ->orderBy(['chat.created_at' => SORT_ASC])->asArray()->one();

        $chatLastMessage = Chat::find()->select('chat.*, action_creator, action_receiver')
                        ->leftJoin(Action::tableName(), 'action.id = chat.action_id')
                        ->where(['chat_id' => $chatId['chat_id']])
                        ->andWhere(['>=', 'chat.created_at', date("Y-m-d H:i:s", time()-5*60)])
                        ->orderBy(['chat.created_at' => SORT_ASC])->asArray()->one(); 
        if (empty($chatLastMessage)) {
            $actionType = ActionType::findOne(['name' => 'chat']);
            if (empty($actionType)) {
                $actionType       = new ActionType();
                $actionType->name = 'chat';
                if (!$actionType->save()) {
                    throw new \Exception('Can\'t be saved action type. Errors: ' . join(', ', $actionType->getFirstErrors()));
                }
            }
            $action                  = new Action();
            $action->action_type_id  = $actionType->id;
            $action->skin_id         = 1;
            $action->action_creator  = $creator;
            $action->action_receiver = $receiver;
            $action->created_at      = $createdAt;
            $action->updated_at      = $createdAt;

            if (!$action->save()) {
                throw new \Exception('Can\'t be saved action. Errors: ' . join(', ', $action->getFirstErrors()));
            }
            $chatSession = new ChatSession();
            $chatSession->created_at = $createdAt;                
            $chatSession->duration = 0;
            $chatSession->active = 1;
            $chatSession->updated_at = $createdAt;

            if (!$chatSession->save()) {
                throw new \Exception('Can\'t be saved chat session. Errors: ' . join(', ', $chatSession->getFirstErrors()));
            }
            $newChatMessage                   = new Chat();
            $newChatMessage->action_id        = $action->id;
            $newChatMessage->chat_session_id  = $chatSession->id;
            $newChatMessage->chat_id          = $chatId['chat_id'];
            $newChatMessage->message          = $message;
            $newChatMessage->original_message = $message;
            $newChatMessage->created_at       = $createdAt;
            $newChatMessage->edited           = 0;
            $newChatMessage->active           = 1;
            if (!$newChatMessage->save()) {
                throw new \Exception('Can\'t be saved chat message. Errors: ' . join(', ', $newChatMessage->getFirstErrors()));
            }
        }
        /*****************************end chat duration**********************************/
    }

    public function getChatIdFromDb($userFrom, $userTo)
    {
        try {
            $actionType = ActionType::findOne(['name' => 'chat']);
            if (empty($actionType)) {
                $actionType       = new ActionType();
                $actionType->name = 'chat';
                if (!$actionType->save()) {
                    throw new \Exception('Can\'t be saved action type. Errors: ' . join(', ', $actionType->getFirstErrors()));
                }
            }
            $action = Action::find()
                ->where([
                    'action_type_id'  => $actionType->id,
                    'action_creator'  => $userFrom,
                    'action_receiver' => $userTo,
                ])
                ->orWhere([
                    'action_type_id'  => $actionType->id,
                    'action_creator'  => $userTo,
                    'action_receiver' => $userFrom,
                ])
                ->orderBy(['created_at' => SORT_ASC])
                ->one();
            if (!empty($action)) {
                $chat = $action->getChats()->orderBy(['created_at' => SORT_DESC])->one();
            }
            if (empty($chat)) {
                $chat_id = \Yii::$app->security->generateRandomString(40);
                $params  = [
                    'chat_id'         => $chat_id,
                    'action_creator'  => $userFrom,
                    'action_receiver' => $userTo,
                    'message'         => null,
                    'created_at'      => date('Y-m-d H:i:s', time()-3600),
                    'active'          => 1,
                ];
                $this->storeMessage($params);

                return $chat_id;
            } else {
                return $chat->chat_id;
            }
        } catch (\Exception $e) {
            echo $e->getMessage() . "\r\n";
            echo $e->getTraceAsString() . "\r\n";
        } catch (\Throwable $e) {
            echo $e->getMessage();
            echo $e->getTraceAsString();
        }
    }
}
