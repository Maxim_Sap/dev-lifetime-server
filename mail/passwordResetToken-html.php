<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\User */
Yii::$app->urlManager->setHostInfo(Yii::$app->params["clientUrl"]);
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['account/reset-password', 'token' => $user->password_reset_token]);

?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->profile->first_name) ?>,</p>

    <p>Follow the link below to reset your password:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>