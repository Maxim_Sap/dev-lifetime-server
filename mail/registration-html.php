<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\User */
Yii::$app->urlManager->setHostInfo(Yii::$app->params["clientUrl"]);
$registerLink = Yii::$app->urlManager->createAbsoluteUrl(['account/register', 'token' => $user->password_reset_token]);

?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->profile->first_name) ?>,</p>

    <p>Follow the link below to register your user account:</p>

    <p><?= Html::a(Html::encode($registerLink), $registerLink) ?></p>
</div>