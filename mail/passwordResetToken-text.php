<?php

/* @var $this yii\web\View */
/* @var $user app\models\User */
Yii::$app->urlManager->setHostInfo(Yii::$app->params["clientUrl"]);
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['account/reset-password', 'token' => $user->password_reset_token]);
?>
Hello <?= $user->profile->first_name ?>,

Follow the link below to reset your password:

<?= $resetLink ?>