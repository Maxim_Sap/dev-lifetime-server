<?php

/* @var $this yii\web\View */
/* @var $user app\models\User */

Yii::$app->urlManager->setHostInfo(Yii::$app->params["clientUrl"]);
$registerLink = Yii::$app->urlManager->createAbsoluteUrl(['account/register', 'token' => $user->password_reset_token]);
?>
Hello <?= $user->profile->first_name ?>,

Follow the link below to register your user account:

<?= $registerLink ?>