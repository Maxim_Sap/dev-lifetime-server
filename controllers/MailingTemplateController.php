<?php
namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\Response;

/* my models */
use app\models\LogTable;
use app\models\User;
use app\models\ApproveStatus;
use app\models\UserProfile;
use app\models\MailingTemplate;
use app\models\MailingQueue;
use app\models\MailingSession;



class MailingTemplateController extends ActiveController
{
    use CommonActionsTrait;
    public $modelClass = '';

    public function behaviors()
    {
        $authArray = ['get-chat-templates', 'get-message-templates', 'get-chat-template', 'get-message-template', 
                      'create-chat-template', 'update-chat-template', 'delete-chat-template', 
                      'create-message-template', 'update-message-template', 'delete-message-template',
                      'start-chat-mailing', 'start-message-mailing', 
                      'close-chat-mailing-session', 'close-message-mailing-session',
                      'get-chat-mailing-session', 'get-message-mailing-session'
                      ];
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => $authArray
        ];

        $access = $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['login'],
            'rules' => [
                [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => $authArray,
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $verbs = $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ];
        
        unset($behaviors['access']);
        unset($behaviors['authenticator']);
        unset($behaviors['verbs']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Allow-Origin'      => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],
        ];
        
        $behaviors['rateLimiter'] = [
            // Use class
            'class' => \highweb\ratelimiter\RateLimiter::className(),
            
            // The maximum number of allowed requests
            'rateLimit' => 100,
            
            // The time period for the rates to apply to
            'timePeriod' => 60,
            
            // Separate rate limiting for guests and authenticated users
            // Defaults to true
            // - false: use one set of rates, whether you are authenticated or not
            // - true: use separate ratesfor guests and authenticated users
            'separateRates' => false,
            
            // Whether to return HTTP headers containing the current rate limiting information
            'enableRateLimitHeaders' => false,
        ];
        
        $behaviors['authenticator'] = $auth;
        $behaviors['access'] = $access;
        $behaviors['verbs'] = $access;
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }
    
    private function getTemplates($type) 
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_FEMALE, User::USER_ADMIN, User::USER_AGENCY, User::USER_SUPERADMIN])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }

        $page = Yii::$app->request->get('page');
        $perPage = Yii::$app->request->get('per_page');

        if (isset($page) && !is_numeric($page)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Wrong page parameter'
            ];
        } else {
            $page = (int) $page;
        }

        if ($page < 0) {
            $page = 1;
        }

        if (isset($perPage) && !is_numeric($perPage)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Wrong per page parameter'
            ];
        } else {
            $perPage = (int) $perPage;
        }

        if (isset($perPage) && $perPage < 0) {
            $perPage = 10;
        }
        
        if ($user->user_type == User::USER_FEMALE) {            
            $query = MailingTemplate::find()
                        ->where([
                            'template_creator' => $user->id, 
                            'created_for' => $type, 
                            'agency_id' => $user->agency_id
                        ]);            
            if (!empty($perPage)) {
                $query->limit($perPage);
            }
            if (!empty($page)) {
                $query->offset(($page-1)*$perPage);
            }
            $templates = $query->all();
        } elseif ($user->user_type == User::USER_AGENCY || $user->user_type == User::USER_ADMIN) {            
            $subQuery = UserProfile::find()
                        ->select(['id', 'first_name', 'last_name']);                        

            $query = MailingTemplate::find()
                        ->select(['mailing_template.id', 'template_creator', 'title', 'body', 'status', 'created_at', 
                                  'user_profile.first_name', 'user_profile.last_name'])
                        ->where(['or', ['template_creator' => $user->getMyChidrensIds()], ['template_creator' => $user->id]])
                        ->andWhere([                            
                            'created_for' => $type, 
                            'agency_id' => $user->agency_id
                        ])
                        ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = template_creator');
            $count = $query->count();            
            if (!empty($perPage)) {
                $query->limit($perPage);
            }
            if (!empty($page)) {
                if ($count >= ($page-1)*$perPage) {
                    $query->offset(($page-1)*$perPage);
                } else {
                    return [
                        'success' => false,
                        'code' => 1,
                        'message' => 'Page not found'
                    ];
                }                
            }
            $templates = $query->asArray()->all();
        } elseif ($user->user_type == User::USER_SUPERADMIN) {
            $subQuery = UserProfile::find()
                        ->select(['id', 'first_name', 'last_name']);

            $query = MailingTemplate::find()
                        ->select(['mailing_template.id', 'template_creator', 'title', 'body', 'status', 'created_at', 
                                  'user_profile.first_name', 'user_profile.last_name'])
                        ->where([
                            'created_for' => $type
                        ])
                        ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = template_creator');

            $count = $query->count();
            if (!empty($perPage)) {
                $query->limit($perPage);
            }
            if (!empty($page)) {
                if ($count >= ($page-1)*$perPage) {
                    $query->offset(($page-1)*$perPage);
                } else {
                    return [
                        'success' => false,
                        'code' => 1,
                        'message' => 'Page not found'
                    ];
                }  
            }
            $templates = $query->asArray()->all();
        }

        $result = [
            'success' => true,
            'templates' => $templates
        ];

        if (in_array($user->user_type, [User::USER_ADMIN, User::USER_AGENCY, User::USER_SUPERADMIN])) {
            $result['count'] = $count;
        }

        return $result;
    }

    public function actionGetChatTemplates()
    {
        return $this->getTemplates(MailingTemplate::CREATED_FOR_CHAT);    
    }

    public function actionGetMessageTemplates()
    {
        return $this->getTemplates(MailingTemplate::CREATED_FOR_MESSAGE);
    }    
    
    private function getTemplate($templateID, $type)
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_FEMALE, User::USER_ADMIN, User::USER_AGENCY, User::USER_SUPERADMIN])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }

        if ($user->user_type == User::USER_FEMALE) {
            $template = MailingTemplate::find()
                        ->where([
                            'template_creator' => $user->id, 
                            'created_for' => $type, 
                            'agency_id' => $user->agency_id, 
                            'id' => $templateID
                        ])
                        ->one();
        } elseif ($user->user_type == User::USER_AGENCY || $user->user_type == User::USER_ADMIN) {            
            $subQuery = UserProfile::find()
                        ->select(['id', 'first_name', 'last_name']);  

            $template = MailingTemplate::find()
                        ->select(['mailing_template.id', 'template_creator', 'title', 'body', 'status', 'created_at',  
                                  'user_profile.first_name', 'user_profile.last_name'])
                        ->where(['or', ['template_creator' => $user->getMyChidrensIds()], ['template_creator' => $user->id]])
                        ->andWhere([                            
                            'created_for' => $type, 
                            'agency_id' => $user->agency_id,
                            'mailing_template.id' => $templateID
                        ])                        
                        ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = template_creator')
                        ->asArray()->one();
        } elseif ($user->user_type == User::USER_SUPERADMIN) {
            $subQuery = UserProfile::find()
                        ->select(['id', 'first_name', 'last_name']);  

            $template = MailingTemplate::find()
                        ->select(['mailing_template.id', 'template_creator', 'title', 'body', 'status', 'created_at', 
                                  'user_profile.first_name', 'user_profile.last_name'])
                        ->where([
                            'created_for' => $type, 
                            'mailing_template.id' => $templateID
                        ])
                        ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = template_creator')
                        ->asArray()->one();
        }
        if (empty($template)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'Template not found'
            ];
        } else {
            return [
                'success' => true,
                'template' => $template
            ];
        }
    }

    public function actionGetChatTemplate($templateID)
    {
        return $this->getTemplate($templateID, MailingTemplate::CREATED_FOR_CHAT);
    }

    public function actionGetMessageTemplate($templateID)
    {
        return $this->getTemplate($templateID, MailingTemplate::CREATED_FOR_MESSAGE);
    }

    private function createTemplate($type)
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_FEMALE, User::USER_ADMIN, User::USER_AGENCY, User::USER_SUPERADMIN])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }

        $model = new MailingTemplate();

        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '')) {
            $model->template_creator = $user->id;
            $model->agency_id = $user->agency_id;
            $model->created_for = $type;
            if ($user->user_type == User::USER_FEMALE) {
                $model->status = ApproveStatus::STATUS_IN_PROGRESS;
            } else {
                $model->status = ApproveStatus::STATUS_APPROVED;
            }
            if (!$model->validate()) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => join(', ', $actionType->getFirstErrors())
                ];
            } else {
                if (!$model->save()) {
                    return [
                        'success' => false, 
                        'code' => 1,
                        'message' => 'Error during template saving'
                    ];
                } else {
                    return [
                        'success' => true,
                        'id' => $model->id
                    ];
                }
            }
        } else {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }
    }

    public function actionCreateChatTemplate()
    {
        return $this->createTemplate(MailingTemplate::CREATED_FOR_CHAT);
    }

    public function actionCreateMessageTemplate()
    {
        return $this->createTemplate(MailingTemplate::CREATED_FOR_MESSAGE);
    }

    private function updateTemplate($templateID, $type)
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_ADMIN, User::USER_AGENCY, User::USER_SUPERADMIN])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }
        if ($user->user_type == User::USER_ADMIN || $user->user_type == User::USER_AGENCY) {
            $model = MailingTemplate::find()
                    ->where(['or', ['template_creator' => $user->getMyChidrensIds()], ['template_creator' => $user->id]])
                    ->andWhere([
                        'id' => $templateID, 
                        'agency_id' => $user->agency_id,                         
                        'created_for' => $type
                    ])->one();
        } elseif ($user->user_type == User::USER_SUPERADMIN) {
            $model = MailingTemplate::findOne([
                        'id' => $templateID,
                        'created_for' => $type
                    ]);
        }
        
        if (empty($model)) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => 'Template not found'
            ];
        }

        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '')) {                                    
            if (!$model->validate()) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => join(', ', $actionType->getFirstErrors())
                ];
            } else {
                if (!$model->save()) {
                    return [
                        'success' => false, 
                        'code' => 1,
                        'message' => 'Error during template saving'
                    ];
                } else {
                    return [
                        'success' => true,
                        'id' => $model->id
                    ];
                }
            }
        } else {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }
    }

    public function actionUpdateChatTemplate($templateID)
    {
        return $this->updateTemplate($templateID, MailingTemplate::CREATED_FOR_CHAT);
    }

    public function actionUpdateMessageTemplate($templateID)
    {
        return $this->updateTemplate($templateID, MailingTemplate::CREATED_FOR_MESSAGE);
    }

    private function deleteTemplate($templateID, $type)
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_FEMALE, User::USER_ADMIN, User::USER_AGENCY, User::USER_SUPERADMIN])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }

        if ($user->user_type == User::USER_ADMIN || $user->user_type == User::USER_AGENCY) {
            $model = MailingTemplate::find()
                    ->where(['or', ['template_creator' => $user->getMyChidrensIds()], ['template_creator' => $user->id]])
                    ->andWhere([
                        'id' => $templateID, 
                        'agency_id' => $user->agency_id,                         
                        'created_for' => $type
                    ])->one();
        } elseif ($user->user_type == User::USER_SUPERADMIN) {
            $model = MailingTemplate::findOne([
                        'id' => $templateID,
                        'created_for' => $type
                    ]);
        }

        if ($model->delete() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }

    public function actionDeleteChatTemplate($templateID)
    {
        return $this->deleteTemplate($templateID, MailingTemplate::CREATED_FOR_CHAT);
    }

    public function actionDeleteMessageTemplate($templateID)
    {
        return $this->deleteTemplate($templateID, MailingTemplate::CREATED_FOR_MESSAGE);
    }


}