<?php
namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\Response;

/* my models */
use app\models\LogTable;
use app\models\User;
use app\models\ApproveStatus;
use app\models\UserProfile;
use app\models\MailingTemplate;
use app\models\MailingQueue;
use app\models\MailingSession;

use app\models\Chat;
use app\models\ChatSession;
use app\models\Action;
use app\models\ActionType;
use app\services\LetterService;


class MailingQueueController extends ActiveController
{
    use CommonActionsTrait;
    public $modelClass = '';

    const NUMBER_OF_CHAT_MESSAGE_SEND = 5;
    const NUMBER_OF_MESSAGE_SEND = 5;

    public function behaviors()
    {
        $authArray = ['get-queues'];
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => $authArray
        ];

        $access = $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['login'],
            'rules' => [
                [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => $authArray,
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $verbs = $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ];
        
        unset($behaviors['access']);
        unset($behaviors['authenticator']);
        unset($behaviors['verbs']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Allow-Origin'      => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],
        ];
        
        $behaviors['rateLimiter'] = [
            // Use class
            'class' => \highweb\ratelimiter\RateLimiter::className(),
            
            // The maximum number of allowed requests
            'rateLimit' => 10,
            
            // The time period for the rates to apply to
            'timePeriod' => 60,
            
            // Separate rate limiting for guests and authenticated users
            // Defaults to true
            // - false: use one set of rates, whether you are authenticated or not
            // - true: use separate ratesfor guests and authenticated users
            'separateRates' => false,
            
            // Whether to return HTTP headers containing the current rate limiting information
            'enableRateLimitHeaders' => false,
        ];
        
        $behaviors['authenticator'] = $auth;
        $behaviors['access'] = $access;
        $behaviors['verbs'] = $access;
        $behaviors['authenticator']['except'] = ['options', 'send-chat-message', 'send-message'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    private function startMailing($type)
    {
        if ($type == MailingTemplate::CREATED_FOR_CHAT) {
            $limit = self::NUMBER_OF_CHAT_MESSAGE_SEND;
        } else {
            $limit = self::NUMBER_OF_MESSAGE_SEND;
        }

        // get messages that have to be send
        $messages = MailingQueue::find()
                    ->where([
                        'mailing_queue.created_for' => $type,
                        'was_received' => MailingQueue::WASNT_RECEIVED,
                        'was_aborted' => MailingQueue::WASNT_ABORTED,
                        'paused' => MailingSession::NOT_PAUSED,
                    ])
                    ->leftJoin(MailingSession::tableName(), 'mailing_session.id = mailing_queue.mailing_session_id')
                    ->limit($limit)
                    ->all();        

        foreach ($messages as $message) {
            // get template
            $template = MailingTemplate::find()
                    ->where([
                        'template_creator' => $message->sender, 
                        'created_for' => $type,  
                        'id' => $message->template_id
                    ])
                    ->one();
            if (empty($template)) {
                return [
                    'success' => false,
                    'code' => 1,
                    'message' => 'Template not found'
                ];  
            }                

            if ($template->status != ApproveStatus::STATUS_APPROVED) {
                return [
                    'success' => false,
                    'code' => 1,
                    'message' => 'Template not approved'
                ];
            }
            // transform template
            $transformedTemplate = $this->transformTemplate($template->title, $template->body, $message->receiver);

            if ($transformedTemplate['success'] == false) {
                return [
                    'success' => false,
                    'code' => 1,
                    'message' => $transformedTemplate['message']
                ];
            }                        

            // send messages

            if ($type == MailingTemplate::CREATED_FOR_CHAT) {
                $result = $this->insertChatMessage($message->sender, $message->receiver,
                                                   $transformedTemplate['body'], date('Y-m-d H:i:s'));                
                if ($result == false) {
                    return [
                        'success' => false,
                        'code' => 1,
                        'message' => 'Error during chat message send'
                    ];
                }
            } else {
                $result = $this->insertMessage($message->sender, $message->receiver, 
                                               $transformedTemplate['title'], $transformedTemplate['body']);
                if ($result['success'] == false) {
                    return $result;
                }
            }

            // update message queue
            // update message session
            $message->was_received = MailingQueue::WAS_RECEIVED;            
            $message->received_at = date('Y-m-d H:i:s');
            $mailingSession = $message->getMailingSession()->one();
            $mailingSession->sended_messages = $mailingSession->sended_messages + 1;
            if ($mailingSession->sended_messages == $mailingSession->total_messages) {
                $mailingSession->was_finished = MailingSession::WAS_FINISHED;
                $mailingSession->ended_at = date('Y-m-d H:i:s');
            }            
            if ($message->save()) {
                $mailingSession->save();
            } else {
                return [
                    'success' => false,
                    'message' => 'Error during save mailing session'
                ];
            }
        }
        return [
            'success' => true
        ];                        

    }

    public function actionSendMessage()
    {
        return $this->startMailing(MailingTemplate::CREATED_FOR_MESSAGE);
    }

    public function actionSendChatMessage()
    {
        return $this->startMailing(MailingTemplate::CREATED_FOR_CHAT);
    }

    private function transformTemplate($title, $body, $userId)
    {
        $search = ['%username%'];        
        $userProfile = UserProfile::find()
                      ->select(['first_name'])
                      ->where([
                            'user_type' => User::USER_MALE,
                            'status' => User::STATUS_ACTIVE,
                            'owner_status' => User::STATUS_ACTIVE,
                            'approve_status_id' => ApproveStatus::STATUS_APPROVED,
                            'user.id' => $userId
                      ])
                      ->leftJoin(User::tableName(), 'user.id = user_profile.id')
                      ->one();
        $firstName = $userProfile->first_name;              
        $replace = [$firstName];
        if (!empty($userProfile)) {
            return [
                'success' => true,
                'title' => str_replace($search, $replace, $title),
                'body' => str_replace($search, $replace, $body)
            ];
        } else {
            return [
                'success' => false,
                'message' => 'User not found'
            ];
        }
    }

    private function insertChatMessage($creator, $receiver, $message, $createdAt)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $chatId = Chat::find()
                            ->leftJoin(Action::tableName(), 'action.id = chat.action_id')
                            ->where(['or', 
                                    ['action_creator' => $creator, 
                                    'action_receiver' => $receiver], 
                                    ['action_creator' => $receiver, 
                                    'action_receiver' => $creator],
                                    ])
                            ->orderBy(['chat.created_at' => SORT_ASC])->asArray()->one();
            if (empty($chatId)) {
                $chatId = \Yii::$app->security->generateRandomString(40);
            } else {
                $chatId = $chatId['chat_id'];
            }

            $chatLastMessage = Chat::find()->select('chat.*, action_creator, action_receiver')
                            ->leftJoin(Action::tableName(), 'action.id = chat.action_id')
                            ->where(['chat_id' => $chatId])
                            ->andWhere(['>=', 'chat.created_at', date("Y-m-d H:i:s", time()-5*60)])
                            ->orderBy(['chat.created_at' => SORT_ASC])->asArray()->one(); 
            if (empty($chatLastMessage)) {
                $actionType = ActionType::findOne(['name' => 'chat']);
                if (empty($actionType)) {
                    $actionType       = new ActionType();
                    $actionType->name = 'chat';
                    if (!$actionType->save()) {
                        throw new \Exception('Can\'t be saved action type. Errors: ' . join(', ', $actionType->getFirstErrors()));
                    }
                }
                $action                  = new Action();
                $action->action_type_id  = $actionType->id;
                $action->skin_id         = 1;
                $action->action_creator  = $creator;
                $action->action_receiver = $receiver;
                $action->created_at      = $createdAt;
                $action->updated_at      = $createdAt;

                if (!$action->save()) {
                    throw new \Exception('Can\'t be saved action. Errors: ' . join(', ', $action->getFirstErrors()));
                }
                $chatSession = new ChatSession();
                $chatSession->created_at = $createdAt;                
                $chatSession->duration = 0;
                $chatSession->active = 0;
                $chatSession->updated_at = $createdAt;

                if (!$chatSession->save()) {
                    throw new \Exception('Can\'t be saved chat session. Errors: ' . join(', ', $chatSession->getFirstErrors()));
                }
                $newChatMessage                   = new Chat();
                $newChatMessage->action_id        = $action->id;
                $newChatMessage->chat_session_id  = $chatSession->id;
                $newChatMessage->chat_id          = $chatId;
                $newChatMessage->message          = $message;
                $newChatMessage->original_message = $message;
                $newChatMessage->created_at       = $createdAt;
                $newChatMessage->edited           = 0;
                $newChatMessage->active           = 1;
                if (!$newChatMessage->save()) {
                    throw new \Exception('Can\'t be saved chat message. Errors: ' . join(', ', $newChatMessage->getFirstErrors()));
                }
            }
            $transaction->commit();
        } catch (\Exception $e) {            
            //echo var_dump($e->getTrace()); die;
            $transaction->rollBack();
            return false;            
        } catch (\Throwable $e) {
            $transaction->rollBack();
            return false;
        }
        return true;
    }

    private function insertMessage($creator, $receiver, $title, $description)
    {
        $letterService = new LetterService($creator);

        return $letterService->send('send letter', 1, $receiver, $title, null, $description, null);
    }

    public function actionGetQueues($sessionID, $page = 1)
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_ADMIN, User::USER_AGENCY, User::USER_SUPERADMIN])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }

        $page = Yii::$app->request->get('page');
        $perPage = Yii::$app->request->get('per_page');

        if (isset($page) && !is_numeric($page)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Wrong page parameter'
            ];
        } else {
            $page = (int) $page;
        }

        if ($page < 0) {
            $page = 1;
        }

        if (isset($perPage) && !is_numeric($perPage)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Wrong per page parameter'
            ];
        } else {
            $perPage = (int) $perPage;
        }

        if (isset($perPage) && $perPage < 0) {
            $perPage = 10;
        }

        if ($user->user_type == User::USER_AGENCY || 
            $user->user_type == User::USER_ADMIN || 
            $user->user_type == User::USER_SUPERADMIN) {

            $subQuery = UserProfile::find()
                        ->select(['id', 'first_name', 'last_name']);

            $query = MailingQueue::find()
                        ->select(['receiver', 'was_received', 'was_aborted', 'created_at', 'received_at', 'user_profile.first_name', 'user_profile.last_name']);
            if ($user->user_type == User::USER_AGENCY || $user->user_type == User::USER_ADMIN) {
                $query = $query->where(['sender' => $user->getMyChidrensIds()]);    
            }            
            $query = $query->andWhere([                            
                        'mailing_session_id' => $sessionID,
                    ])                        
                    ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = receiver');     
            $count = $query->count();            
            if (!empty($perPage)) {
                $query->limit($perPage);
            }
            if (!empty($page)) {
                if ($count >= ($page-1)*$perPage) {
                    $query->offset(($page-1)*$perPage);
                } else {
                    return [
                        'success' => false,
                        'code' => 1,
                        'message' => 'Page not found'
                    ];
                }                
            }
            $queues = $query->asArray()->all();
        }

        $result = [
            'success' => true,
            'queues' => $queues
        ];

        if (in_array($user->user_type, [User::USER_ADMIN, User::USER_AGENCY, User::USER_SUPERADMIN])) {
            $result['count'] = $count;
        }
        return $result;
    }

}