<?php
namespace app\controllers;

use app\models\Blog;
use app\models\Comments;
use app\services\PhotoService;
use app\services\StatusService;
use Yii;
use yii\base\DynamicModel;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\helpers\HtmlPurifier;

/*my models*/
use app\models\ActionType;
use app\models\Agency;
use app\models\Album;
use app\models\AgencyBonus;
use app\models\ApproveStatus;
use app\models\ChatSession;
use app\models\LogTable;
use app\models\OtherAgencyPenalty;
use app\models\Photo;
use app\models\Affiliates;
use app\models\Message;
use app\models\ReferralStats;
use app\models\Pricelist;
use app\models\PaymentsSchedule;
use app\models\PricelistAgency;
use app\models\PricelistPenalty;
use app\models\SitePage;
use app\models\User;
use app\models\UserProfile;
use app\models\UserType;
use app\models\Video;
use app\models\forms\RegisterNewUserForm;
/*my services*/
use app\services\AlbumService;
use app\services\MessageService;
use app\services\BillingService;
use app\services\ChatService;
use app\services\LetterService;
use app\services\ShopService;
use app\services\Statuses;
use app\services\PaymentsService;
use app\services\NeedToApproveService;


class AdminController extends ActiveController
{
    public $modelClass = '';

    public function behaviors()
    {
        $authArray = [
            'get-total-finance-report', 'get-agency-finance-info', 'get-agencies-with-finance-info', 'register-new-user',
            'get-album-photo', 'get-log', 'set-user-invisible', 'get-agency', 'logout', 'get-user-types',
            'set-user-activity', 'set-custom-page-text', 'get-user-activity', 'delete-user',
            'delete-video', 'get-photos', 'get-need-to-approve-items', 'get-blog-post', 'add-post',
            'upload-post-image', 'blog-post-delete', 'get-tasks', 'add-task', 'delete-task', 'get-message',
            'mailing', 'get-pricelist', 'add-pricelist', 'get-pricelist-agency', 'add-agency-pricelist',
            'delete-pricelist-item', 'delete-agency-pricelist-item', 'pricelist-penalty', 'get-penalties', 'get-penalty', 'get-users-info-from-agency', 'get-agency-balance', 'get-wait-to-approve-items',
            'add-penalty', 'delete-penalty', 'get-user-finance-info','set-online-status', 'get-letters', 'set-user-approve-status',
            'get-new-message-count', 'get-user-letters', 'read-letter', 'send-letter','get-affiliates','get-affiliates-stats',
            'get-affiliates-finance', 'get-count-need-to-approve', 'get-need-to-approve-items-by-agency', 'get-need-to-approve-by-param', 'get-users-info-by-users-type', 'get-users-info-from-agency',
            'get-agency-payments-schedule', 'get-total-people-count','add-agency-payments','transfer-agency-payments',
            'get-agency-girls-with-payments', 'get-agency-notifications'
        ];
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only'  => $authArray,
        ];

        $access                         = $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only'  => ['login'],
            'rules' => [
                [
                    'actions' => ['login'],
                    'allow'   => TRUE,
                    'roles'   => ['?'],
                ],
                [
                    'actions' => $authArray,
                    'allow'   => TRUE,
                    'roles'   => ['@'],
                ],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class'   => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $verbs                          = $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ];

        unset($behaviors['access']);
        unset($behaviors['authenticator']);
        unset($behaviors['verbs']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors'  => [
                // restrict access to
                'Origin'                           => Yii::$app->params['origin'],
                'Access-Control-Allow-Origin'      => Yii::$app->params['origin'],
                'Access-Control-Request-Method'    => ['POST', 'GET', 'OPTIONS'],
                // Allow only POST and PUT methods
                'Access-Control-Request-Headers'   => ['*'],
                // Allow only headers 'X-Wsse'
                'Access-Control-Allow-Credentials' => TRUE,
                // Allow OPTIONS caching
                'Access-Control-Max-Age'           => 3600,
                // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
            ],
        ];

        $behaviors['authenticator']           = $auth;
        $behaviors['access']                  = $access;
        $behaviors['verbs']                   = $access;
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    protected function checkRights($user, $otherUser)
    {
        if (empty($user) || empty($otherUser)) {
            return [
                'success' => FALSE,
                'code'    => 220,
                'message' => 'Access denied',
            ];
        }
        if (empty($user->status) || empty($user->owner_status) ||
            $user->status == User::STATUS_NO_ACTIVE || $user->status == User::STATUS_DELETED ||
            $user->owner_status == User::STATUS_NO_ACTIVE || $user->owner_status == User::STATUS_DELETED
        ) {
            return [
                'success' => FALSE,
                'code'    => 221,
                'message' => 'User not active',
            ];
        }
        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_SUPERADMIN, User::USER_ADMIN])) {
            if ($otherUser->status == User::STATUS_NO_ACTIVE || $otherUser->status == User::STATUS_NO_EMAIL_CONFIRM || $otherUser->status == User::STATUS_DELETED) {
                return [
                    'success' => FALSE,
                    'code'    => 222,
                    'message' => 'User not active',
                ];
            }
        }

        if ($user->user_type == User::USER_INTERPRETER && (in_array($otherUser->id, $user->user_ids) || $otherUser->id == $user->id)) {
            return [
                'success' => TRUE,
            ];
        }

        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            // user without assosiated agency, users agency not match
            if ($user->agency_id == 1 || $user->agency_id != $otherUser->agency_id) {
                return [
                    'success' => FALSE,
                    'code'    => 223,
                    'message' => 'Access denied',
                ];
            }
        } elseif (in_array($user->user_type, [User::USER_SUPERADMIN]) || $user->id == $otherUser->id) {
            return [
                'success' => TRUE,
            ];
        } elseif (in_array($user->user_type, [User::USER_MALE, User::USER_FEMALE])) {
            return [
                'success' => FALSE,
                'code'    => 224,
                'message' => 'Access denied',
            ];
        } else {
            if (($user->user_type == $otherUser->user_type && $user->id != $otherUser->id) ||
                !in_array($user->user_type, [User::USER_MALE, User::USER_FEMALE]) ||
                !in_array($otherUser->user_type, [User::USER_MALE, User::USER_FEMALE])
            ) {
                return [
                    'success' => FALSE,
                    'code'    => 225,
                    'message' => 'Access denied',
                ];
            }
        }

        return [
            'success' => TRUE,
        ];
    }

    public function actionGetUserActivity()
    {
        $otherUserID = Yii::$app->request->post('otherUserID');
        $user        = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => $result2['message'],
            ];
        }

        if ($otherUserID == $user->id) {
            $otherUserID = null;
        }
        $searchParams = Yii::$app->request->post('search_params');

        $agencyUsersIds = [];
        if (empty($searchParams['agencyID'])) {
            $searchParams['agencyID'] = 1;
            $agency                   = Agency::find()->where(['id' => $searchParams['agencyID'], 'approve_status' => ApproveStatus::STATUS_APPROVED])->one();
        } else {
            if (in_array($user->user_type, [User::USER_ADMIN, User::USER_AGENCY])) {
                $searchParams['agencyID'] = $user->agency_id;
                $agency = Agency::find()->where(['id' => $user->agency_id, 'approve_status' => ApproveStatus::STATUS_APPROVED])->andWhere(['!=', 'id', 1])->one();
            } elseif ($user->user_type == User::USER_SUPERADMIN) {
                $agency = Agency::find()->where(['id' => $searchParams['agencyID'], 'approve_status' => ApproveStatus::STATUS_APPROVED])->andWhere(['!=', 'id', 1])->one();
            }            
        }
        
        if (!empty($agency)) {
            $agencyUsers = $agency->getUsers()->where(['status' => [User::STATUS_ACTIVE]])->all();
            foreach ($agencyUsers as $agencyUser) {
                $agencyUsersIds[] = $agencyUser->id;
            }
        }

        if ($user->user_type == User::USER_INTERPRETER) {
            $agencyUsersIds = $user->user_ids;
        }

        $limit  = Yii::$app->request->post('limit');
        $offset = Yii::$app->request->post('offset');
            
        if (empty($otherUserID) && empty($agencyUsersIds)) {
            $usersActivityInfo['activityArray'] = [];
            $usersActivityInfo['count']         = 0;
        } else {
            //return $agencyUsersIds;
            $usersActivityInfo = $user->getUserActivity($otherUserID, $agencyUsersIds, $searchParams, $offset, $limit);
            //return $usersActivityInfo;
        }

        return [
            'success'           => TRUE,
            'usersActivityInfo' => $usersActivityInfo['activityArray'],
            'count'             => $usersActivityInfo['count'],
        ];

    }

    public function actionGetAgencyPaymentsSchedule()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }   

        if (!in_array($user->user_type, [User::USER_ADMIN, User::USER_SUPERADMIN, User::USER_AGENCY])) {
            return [
                'success' => FALSE, 
                'code' => 22, 
                'message' => 'Access denied'
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        $postArray = Yii::$app->request->post();
        $agencyID = isset($postArray['agencyID']) && is_numeric($postArray['agencyID']) ? $postArray['agencyID'] : null;
        $orderID  = isset($postArray['orderID']) && is_numeric($postArray['orderID']) ? $postArray['orderID'] : FALSE;
        $dateFrom = isset($postArray['dateFrom']) && strtotime($postArray['dateFrom']) ? $postArray['dateFrom'] : null;
        $dateTo   = isset($postArray['dateTo']) && strtotime($postArray['dateTo']) ? $postArray['dateTo'] : null;
        if ($orderID) {
            $paymentsSheduleArray = PaymentsSchedule::getByOrderId($orderID);
        } else {
            $paymentsSheduleArray = PaymentsSchedule::getPeriod($agencyID, $dateFrom, $dateTo);
            if (empty($paymentsSheduleArray)) {
                return [
                    'success' => FALSE, 
                    'code' => 2, 
                    'message' => 'Order not found',                    
                ];
            }
        }

        return [
            'success' => TRUE, 
            'code' => 1, 
            'paymentsSheduleArray' => $paymentsSheduleArray
        ];
    }

    public function actionGetAgencyBalance()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }        
        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('finance_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('finance_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        $dateFrom = Yii::$app->request->post('dateFrom');
        $dateTo = Yii::$app->request->post('dateTo');
        $findUserID = Yii::$app->request->post('findUserID');

        $dateFrom = (isset($dateFrom) && strtotime($dateFrom)) ? $dateFrom : FALSE;
        $dateTo   = (isset($dateTo) && strtotime($dateTo)) ? $dateTo : FALSE;
        $findUserID = (isset($findUserID) && is_numeric($findUserID)) ? (int)$findUserID : null;

        $result = $user->checkOwn($findUserID);
        if (!$result) {
            return [
                'success' => FALSE, 
                'code' => 2, 
                'message' => 'Access denied'
            ];
        }

        $billingService = new BillingService($user->id);
        $agencyBalance = $billingService->getAgencyBalance($findUserID, $dateFrom, $dateTo);

        return [
            'success' => TRUE, 
            'balance' => $agencyBalance
        ];

    }

    public function actionGetAgencyGirlsWithPayments()
    {
        $user = Yii::$app->user->identity;

        if (empty($user) || !in_array($user->user_type, [User::USER_SUPERADMIN, User::USER_SITEADMIN, User::USER_AGENCY, User::USER_ADMIN])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        $searchParams    = Yii::$app->request->post('searchParams');
        if ($user->user_type == User::USER_ADMIN) {
            $userAgency = User::find()->where([
                                            'user_type' => User::USER_AGENCY, 
                                            'status' => User::STATUS_ACTIVE, 
                                            'agency_id' => $user->agency_id
                                        ])
                                      ->one();                                        
            $paymentsService = new PaymentsService($userAgency->id);
        } else {
            $paymentsService = new PaymentsService($user->id);
        }
        

        $agencyGirls = $paymentsService->getTotalFinanceInfoByAgency($searchParams);

        return [
            'success' => TRUE,
            'girlsList' => $agencyGirls['girlsList']
        ];

    }

    public function actionMailing()
    {
        $user = Yii::$app->user->identity;

        if (empty($user) || !in_array($user->user_type, [User::USER_SUPERADMIN, User::USER_SITEADMIN, User::USER_AGENCY, User::USER_ADMIN])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $userIDs    = Yii::$app->request->post('userIDs');
        $userTypeID = Yii::$app->request->post('userTypeID');

        if (isset($userTypeID) && is_numeric($userTypeID)) {
            if ($userTypeID == User::USER_MALE) {
                $maleUsers = User::find()->where(['user_type' => User::USER_MALE, 'status' => User::STATUS_ACTIVE, 'owner_status' => User::STATUS_ACTIVE])->asArray()->all();
                if (!empty($userIDs)) {
                    foreach ($maleUsers as $maleUser) {
                        if (in_array($maleUser['id'], $userIDs)) {
                            $ids[] = $maleUser['id'];
                        }
                    }
                } else {
                    foreach ($maleUsers as $maleUser) {
                        $ids[] = $maleUser['id'];
                    }
                }                
            } elseif ($userTypeID == User::USER_FEMALE) {
                $femaleUsers = User::find()->where(['user_type' => User::USER_FEMALE, 'status' => User::STATUS_ACTIVE, 'owner_status' => User::STATUS_ACTIVE])->all();

                if (!empty($userIDs)) {
                    foreach ($femaleUsers as $femaleUser) {
                        $agency      = $femaleUser->getAgency()->where(['approve_status' => 4])->one();
                        $currentUser = $agency->getUsers()->where(['user_type' => User::USER_AGENCY])->one();
                        if (!empty($currentUser) && in_array($currentUser->id, $userIDs)) {

                            $ids[] = $femaleUser->id;
                        }
                    }
                } else {
                    foreach ($femaleUsers as $femaleUser) {
                        $ids[] = $femaleUser->id;
                    }
                }
            }
            if (!empty($ids)) {
                $userIDs = $ids;
            }
        }

        if (empty($userIDs) || !is_array($userIDs)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        } else {
            $userIDs = array_filter($userIDs, 'is_numeric');
        }

        $description = Yii::$app->request->post('description');
        $caption     = Yii::$app->request->post('caption');

        if (!isset($description) || trim($description) == '' || !isset($caption) || trim($caption) == "") {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $messageService = new MessageService($user->id);

        foreach ($userIDs as $item) {
            $result = $messageService->sendMessage($item, trim($caption), trim($description));
            if ($result['success'] == FALSE) {
                return [
                    'success' => FALSE,
                    'message' => $result['message'],
                ];
            }
        }

        LogTable::addLog($user->id, $user->user_type, 'sent message to user ID: ' . implode(', ', $userIDs));

        return [
            'success' => TRUE,
            'message' => 'All messages was sent',
        ];

    }

    public function actionDeleteVideo()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        $otherUserID = Yii::$app->request->post('otherUserID');
        $otherUserID = (isset($otherUserID) && is_numeric($otherUserID)) ? (int)$otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => $result2['message'],
            ];
        }

        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN && $user->user_type != User::USER_SITEADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, " user with {$user->id} tries to delete video owned by user {$otherUser->id}");

                return [
                    'success' => FALSE,
                    'code'    => 1,
                    'message' => 'Access Denied',
                ];
            }
        }

        $videoID = Yii::$app->request->post('videoID');
        $videoID = (!empty($videoID) && filter_var($videoID, FILTER_VALIDATE_INT)) ? (int)$videoID : null;
        $result  = $otherUser->deleteVideo($videoID);

        LogTable::addLog($user->id, $user->user_type, ' delete video ID: ' . $videoID . " owner user ID: " . $otherUser->id);

        return $result;
    }

    public function actionSetUserApproveStatus()
    {
        $otherUserID = Yii::$app->request->post('otherUserID');

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        $otherUserID = (isset($otherUserID) && is_numeric($otherUserID)) ? (int)$otherUserID : $user->id;
        
        $otherUser = User::findOne(['id' => $otherUserID]);
        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => $result2['message'],
            ];
        }

        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN && $user->user_type != User::USER_SITEADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, " user with {$user->id} tries to set approve status");

                return [
                    'success' => FALSE,
                    'code'    => 1,
                    'message' => 'Access Denied',
                ];
            }
        }

        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_SUPERADMIN, User::USER_ADMIN])) {  //admin/agency/agency admin
            return [
                'success' => FALSE, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }
        
        $result = $user->setApproveStatus($otherUserID);

        if($result['success']){
            switch ($result['status']){
                case ApproveStatus::STATUS_NOT_APPROVED:
                    $status = 'not approved';
                    break;
                case ApproveStatus::STATUS_IN_PROGRESS:
                    $status = 'in progress';
                    break;
                case ApproveStatus::STATUS_DECLINED:
                    $status = 'decline';
                    break;
                case ApproveStatus::STATUS_APPROVED:
                    $status = 'approved';
                    break;
                
            }
            LogTable::addLog($user->id, $user->user_type, 'set user ID: '.$otherUserID.' approve status: '.$status);
        }

        return $result;

    }

    public function actionGetWaitToApproveItems()
    {
        
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }
        
        $user = User::find()->where([
                                'agency_id' => $user->agency_id, 
                                'user_type' => User::USER_AGENCY, 
                                'status' => User::STATUS_ACTIVE
                            ])->one();
        $myGirlsID = $user->getMyChidrensIds();
        
        if (empty($myGirlsID))
            return [
                'success' => TRUE, 
                'users'=>[],
                'videoArray'=>[],
                'photosArray'=>[],
                'del_photosArray'=>[]
            ];

        $user = User::find()
            ->select('user.id, user.last_activity, profile.first_name, profile.last_name, approve_status.description')
            ->leftJoin(UserProfile::tableName() . ' profile', 'profile.id = user.id')
            ->leftJoin(ApproveStatus::tableName(), 'approve_status.id = user.approve_status_id')
            ->where(['in','user.id',$myGirlsID])
            ->andWhere(['in','user.approve_status_id', [ApproveStatus::STATUS_NOT_APPROVED, ApproveStatus::STATUS_IN_PROGRESS, ApproveStatus::STATUS_DECLINED]]);
        $usersArray = $user->asArray()->all();

        $video = Video::find()
            ->select('video.id, profile.first_name, profile.last_name, profile.id, video.created_at')
            ->leftJoin(UserProfile::tableName() . ' profile', 'profile.id = video.user_id')
            ->where(['in','video.id',$myGirlsID])
            ->andWhere(['in','video.status', [ApproveStatus::STATUS_NOT_APPROVED, ApproveStatus::STATUS_IN_PROGRESS]]);
        $videoArray = $video->asArray()->all();

        $photo = Photo::find()
            ->select('photo.id, photo.created_at, photo.album_id, photo.small_thumb, profile.first_name, profile.last_name, profile.id as user_id')
            ->leftJoin(Album::tableName(), 'album.id = photo.album_id')
            ->leftJoin(UserProfile::tableName() . ' profile', 'profile.id = album.user_id')
            ->where(['in','album.user_id',$myGirlsID])
            ->andWhere(['in','photo.approve_status',[ApproveStatus::STATUS_NOT_APPROVED, ApproveStatus::STATUS_IN_PROGRESS, ApproveStatus::STATUS_DECLINED]]);
        $photoArray = $photo->asArray()->all();

        $deleledPhoto = Photo::find()
            ->select('photo.id, photo.created_at, photo.album_id, photo.small_thumb, profile.first_name, profile.last_name, profile.id as user_id')
            ->leftJoin(Album::tableName(), 'album.id = photo.album_id')
            ->leftJoin(UserProfile::tableName() . ' profile', 'profile.id = album.user_id')
            ->where(['in','album.user_id',$myGirlsID])
            ->andWhere(['photo.status'=>0]);
        $deleledPhotoArray = $deleledPhoto->asArray()->all();

        return [
            'success' => TRUE, 
            'users'=>$usersArray,
            'videoArray'=>$videoArray,
            'photosArray'=>$photoArray,
            'deletedPhotosArray'=>$deleledPhotoArray
        ];
    }

    public function actionGetCountNeedToApprove()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_SUPERADMIN, User::USER_ADMIN])) {
            return [
                'success' => FALSE, 
                'code' => 2, 
                'message' => 'Access denied'
            ];
        }

        $count = 0;
        $needToApproveService = new NeedToApproveService($user->id);
        if ($user->user_type == User::USER_SUPERADMIN) {            
            $count = $needToApproveService->getNeedToApproveAgencies()->count() + 
                     $needToApproveService->getNeedToApproveUsers()->count() +
                     $needToApproveService->getNeedToApproveVideo()->count() +
                     $needToApproveService->getNeedToApprovePhoto()->count() +
                     $needToApproveService->getNeedToApproveDeletedPhoto()->count();
        } else {
            $count += $needToApproveService->getNeedToApproveAgencies($user->agency_id)->count() +
                      $needToApproveService->getNeedToApproveUsers($user->agency_id)->count() +
                      $needToApproveService->getNeedToApproveVideo($user->agency_id)->count() +
                      $needToApproveService->getNeedToApprovePhoto($user->agency_id)->count() +
                      $needToApproveService->getNeedToApproveDeletedPhoto($user->agency_id)->count();
        }

/*        $new_tasks_comments = (new Comments())->notReadCommentsCount($ress->user_type_id, $p['user_id'], 1);
        $new_gifts_comments = (new Comments())->notReadCommentsCount($ress->user_type_id, $p['user_id'], 2);
        $new_system_message = ((new Message())->getNewMessageCountForAdmin($ress->user_type_id, $p['user_id']));*/

        return [
            'success'            => TRUE,
            'count'              => $count,
/*            'new_tasks_comments' => $new_tasks_comments,
            'new_gifts_comments' => $new_gifts_comments,
            'new_system_message' => $new_system_message,
*/      ];
    }    

    public function actionGetAgencyNotifications()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_SUPERADMIN, User::USER_ADMIN])) {
            return [
                'success' => FALSE, 
                'code' => 2, 
                'message' => 'Access denied'
            ];
        }

        $count = 0;
        $needToApproveService = new NeedToApproveService($user->id);
        if ($user->user_type == User::USER_SUPERADMIN) {            
            return [
                'success' => true,
                'unrepliedLetters' => $needToApproveService->getNeedToReplyLetters()->count(),
                'unrepliedMessages' => $needToApproveService->getNeedToReplyMessages()->count(),
                'unfinishedTasks' => $needToApproveService->getUndoneTasks()->count(),
                'unsendGifts' => $needToApproveService->getUndoneGifts()->count(),
                'notApprovedChatTemplates' => $needToApproveService->getNotApprovedChatTemplates()->count(),
                'notApprovedMessageTemplates' => $needToApproveService->getNotApprovedMessageTemplates()->count(),
            ];
        } else {
            return [
                'success' => true,
                'unrepliedLetters' => $needToApproveService->getNeedToReplyLetters($user->agency_id)->count(),
                'unrepliedMessages' => $needToApproveService->getNeedToReplyMessages($user->agency_id)->count(),
                'unfinishedTasks' => $needToApproveService->getUndoneTasks($user->agency_id)->count(),
                'unsendGifts' => $needToApproveService->getUndoneGifts($user->agency_id)->count(),
                'notApprovedChatTemplates' => $needToApproveService->getNotApprovedChatTemplates($user->agency_id)->count(),
                'notApprovedMessageTemplates' => $needToApproveService->getNotApprovedMessageTemplates($user->agency_id)->count(),
            ];
        }
    }

    public function actionGetNewMessageCount()
    {

    	$user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_INTERPRETER, User::USER_SITEADMIN, User::USER_SUPERADMIN, User::USER_ADMIN])) {
            return [
            	'success' => FALSE, 
            	'code' => 2, 
            	'message' => 'Access denied'
            ];
        }

        $skinID = Yii::$app->request->post('skinID');

        if (empty($skinID) || !is_numeric($skinID)) {
        	return [
            	'success' => FALSE, 
            	'code' => 1, 
            	'message' => 'Missing params'
            ];
        }

        $userIDs = Yii::$app->request->post('myUsersIDs');

        $userIDs = (!empty($userIDs) && is_array($userIDs)) ? array_filter($userIDs, 'is_numeric') : FALSE;

        if (!$userIDs) {
            return [
            	'success' => FALSE, 
            	'code' => 1, 
            	'message' => 'Missing params'
            ];
        }

        $result = [];
        foreach ($userIDs as $userID) {
            $chatService = new ChatService($userID);
            $letterService = new LetterService($userID);                        
            $result[$userID]['chat']    = $chatService->getNewMessageDetailCount();
            $result[$userID]['letters'] = $letterService->getNewLetters();
        }

        return [
        	'success' => TRUE, 
        	'code' => 1,
        	'totalNewMessageCount' => $result
        ];
    }

/*    public function actionGetNeedToApproveItems()
    {

        $user = Yii::$app->user->identity;

        if (empty($user) || $user->user_type != User::USER_SUPERADMIN) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $agency = Agency::find()
            ->select('user.id, agency.approve_status, user.last_activity, agency.name')
            ->leftJoin(User::tableName(), 'user.agency_id = agency.id')
            ->where(['user.user_type' => User::USER_AGENCY])
            ->andWhere(['in', 'agency.approve_status', [ApproveStatus::STATUS_NOT_APPROVED, ApproveStatus::STATUS_IN_PROGRESS]])->asArray()->all();

        $user = User::find()
            ->select('user.id, user.last_activity, user_profile.first_name, user_profile.last_name, approve_status.description')
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = user.id')
            ->leftJoin(ApproveStatus::tableName(), 'approve_status.id = user.approve_status_id')
            ->where(['in', 'approve_status_id', [ApproveStatus::STATUS_NOT_APPROVED, ApproveStatus::STATUS_IN_PROGRESS]])->asArray()->all();

        $video = Video::find()
            ->select('video.id, user_profile.first_name, user_profile.last_name, user_profile.id as user_id, video.created_at')
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = video.user_id')
            ->where(['video.status' => [ApproveStatus::STATUS_NOT_APPROVED, ApproveStatus::STATUS_IN_PROGRESS]])->asArray()->all();

        $photo = Photo::find()
            ->select('photo.id, album.created_at, photo.album_id, photo.small_thumb, user_profile.first_name, user_profile.last_name, user_profile.id as user_id')
            ->leftJoin(Album::tableName(), 'album.id = photo.album_id')
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = album.user_id')
            ->where(['photo.approve_status' => [ApproveStatus::STATUS_NOT_APPROVED, ApproveStatus::STATUS_IN_PROGRESS]])->asArray()->all();

        $deletedPhoto = Photo::find()
            ->select('photo.id, album.created_at, photo.album_id, photo.small_thumb, user_profile.first_name, user_profile.last_name, user_profile.id as user_id')
            ->leftJoin(Album::tableName(), 'album.id = photo.album_id')
            ->leftJoin(UserProfile::tableName(), 'user_profile.id = album.user_id')
            ->where(['photo.status' => Photo::STATUS_DELETED])->asArray()->all();

        return [
            'success'      => TRUE,
            'agency'       => $agency,
            'user'         => $user,
            'video'        => $video,
            'photo'        => $photo,
            'deletedPhoto' => $deletedPhoto,
        ];
    }*/

    public function actionGetNeedToApproveItemsByAgency()
    {
        
        $user = Yii::$app->user->identity;

        if (empty($user) || $user->user_type != User::USER_SUPERADMIN) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }        

        $needToApproveService = new NeedToApproveService($user->id);        

        return [
            'success'         => TRUE,
            'agencyList'     => $needToApproveService->getNeedToApproveAgencies()->asArray()->all(),
            'users'           => $needToApproveService->getNeedToApproveUsers()->asArray()->all(),
            'videoList'      => $needToApproveService->getNeedToApproveVideo()->asArray()->all(),
            'photosList'     => $needToApproveService->getNeedToApprovePhoto()->asArray()->all(),
            'deletedPhotosList' => $needToApproveService->getNeedToApproveDeletedPhoto()->asArray()->all(),
            'giftsList'      => $needToApproveService->getNeedToApproveGifts()->asArray()->all(),
            'tasksList'      => $needToApproveService->getNeedToApproveTasks()->asArray()->all(),
        ];
    }

    public function actionGetNeedToApproveByParam()
    {

        $user = Yii::$app->user->identity;

        if (empty($user) || $user->user_type != User::USER_SUPERADMIN) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }   
        $agencyID = Yii::$app->request->post('agencyID');
        $otherUserID = Yii::$app->request->post('otherUserID');

        $agencyID     = (isset($agencyID) && is_numeric($agencyID)) ? $agencyID : FALSE;
        $otherUserID = (isset($otherUserID) && is_numeric($otherUserID)) ? $otherUserID : FALSE;

        $needToApproveService = new NeedToApproveService($user->id);

        if ($agencyID) {
            return [
                'success'        => TRUE,                
                'users'          => $needToApproveService->getNeedToApproveUsers($agencyID)->asArray()->all(),
                'videoList'      => $needToApproveService->getNeedToApproveVideo($agencyID)->asArray()->all(),
                'photosList'     => $needToApproveService->getNeedToApprovePhoto($agencyID)->asArray()->all(),
                'deletedPhotosList' => $needToApproveService->getNeedToApproveDeletedPhoto($agencyID)->asArray()->all(),
                'giftsList'      => $needToApproveService->getNeedToApproveGifts($agencyID)->asArray()->all(),
                'tasksList'      => $needToApproveService->getNeedToApproveTasks($agencyID)->asArray()->all(),
            ];
        } elseif ($otherUserID) {
            return [
                'success'        => TRUE,
                'users'          => $needToApproveService->getNeedToApproveUserByUserID($otherUserID)->asArray()->all(),
                'videoList'      => $needToApproveService->getNeedToApproveVideoByUserID($otherUserID)->asArray()->all(),
                'photosList'     => $needToApproveService->getNeedToApprovePhotoByUserID($otherUserID)->asArray()->all(),
                'deletedPhotosList' => $needToApproveService->getNeedToApproveDeletedPhotoByUserID($otherUserID)->asArray()->all(),
            ];
        }            

    }

    public function actionGetUsersInfoFromAgency()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_SUPERADMIN, User::USER_ADMIN])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }
        if ($user->user_type == User::USER_SUPERADMIN) {
            $otherUserID = Yii::$app->request->post('otherUserID');
            $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;
        } elseif ($user->user_type == User::USER_AGENCY) {
            $otherUserID = $user->id;
        } elseif ($user->user_type == User::USER_ADMIN) {
            $adminUser = User::find()->where(['agency_id' => $user->agency_id, 'user_type' => User::USER_AGENCY])->one();
            $otherUserID = $adminUser->id;
        }

        $userType = Yii::$app->request->post('type');

        $userType = (isset($userType) && is_string($userType)) ? $userType : '';

        $otherUser = User::findOne(['id' => $otherUserID]);
/*        return [
            'success' => TRUE, 
            'code' => 1,             
            'usersArray' => $userType
        ];*/

        if ($otherUser->user_type == User::USER_AFFILIATE) {
            $usersInfoArray = $otherUser->getUsersInfoFromAgency('men', $otherUser->user_type, $otherUser->id);
        } else {
            if ($userType == 'agency') {
                $usersInfoArray = Agency::find()                
                ->select(['name as first_name', 'user.id'])
                ->leftJoin(User::tableName(), 'user.agency_id = agency.id')
                ->where(['user.id' => $otherUserID])
                ->asArray()
                ->all();
            } else {
                if (!in_array($userType, ['translators', 'men', 'admin', 'girls'])) {
                    $userType = 'girls';
                }
                $usersInfoArray = $otherUser->getUsersInfoFromAgency($userType, $otherUser->user_type, $otherUser->id);
            }            
        }
        return [
            'success' => TRUE, 
            'code' => 1,             
            'usersArray' => $usersInfoArray
        ];

    }

    public function actionGetUsersInfoByUsersType()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!in_array($user->user_type, [User::USER_SUPERADMIN, User::USER_AGENCY, User::USER_ADMIN, User::USER_SITEADMIN])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }

        $userList = [];
        if ($user->user_type == User::USER_SUPERADMIN || $user->user_type == User::USER_SITEADMIN) {
            $userAgencyID = null;
        } elseif ($user->user_type == User::USER_AGENCY) {
            $userAgencyID = $user->id;
        } elseif ($user->user_type == User::USER_ADMIN) {
            $adminUser = User::find()->where(['agency_id' => $user->agency_id, 'user_type' => User::USER_AGENCY])->one();
            $userAgencyID = $adminUser->id;
        }
        
        $skinID = Yii::$app->request->post('skinID');
        $userType = Yii::$app->request->post('userType');

        if (!isset($skinID) || !is_numeric($skinID) || !isset($userType) || !is_numeric($userType)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        if ($userType == User::USER_AGENCY || $userType == User::USER_FEMALE || 
            $userType == User::USER_ADMIN || $userType == User::USER_INTERPRETER) {
            $query = Agency::find()->select(['user.id', 'agency.name'])
                                      ->leftJoin(User::tableName(), 'user.agency_id = agency.id')  
                                      ->where(['agency.approve_status' => ApproveStatus::STATUS_APPROVED, 
                                               'user_type' => User::USER_AGENCY])
                                      ->andWhere(['!=', 'agency.id', 1]);
            if (!empty($userAgencyID)) {
                $query->andWhere(['user.id' => $userAgencyID]);
            }
            $userList = $query->asArray()->all();
        }
        if ($userType == User::USER_MALE) {
            $query = User::find()->addSelect(['user.id', 'first_name', 'last_name'])
                                    ->leftJoin(UserProfile::tableName(), 'user.id = user_profile.id')
                                    ->where([
                                        'user_type' => User::USER_MALE, 
                                        'status' => User::STATUS_ACTIVE, 
                                        'approve_status_id' => ApproveStatus::STATUS_APPROVED
                                        ]);
            if (!empty($userAgencyID)) {
                $query->andWhere(['agency_id' => $user->agency_id]);
            }            
            $userList = $query->asArray()->all();
        }
        if ($userType == User::USER_SITEADMIN) {
            $query = User::find()->addSelect(['user.id', 'first_name', 'last_name'])
                                    ->leftJoin(UserProfile::tableName(), 'user.id = user_profile.id')
                                    ->where([
                                        'user_type' => User::USER_SITEADMIN, 
                                        'status' => User::STATUS_ACTIVE, 
                                        'approve_status_id' => ApproveStatus::STATUS_APPROVED
                                        ]);
            if (!empty($userAgencyID)) {
                $query->andWhere(['agency_id' => $user->agency_id]);
            }            
            $userList = $query->asArray()->all();
        }

        return [
            'success' => TRUE, 
            'code' => 1, 
            'userList' => $userList
        ];
    }

    public function actionDeleteUser()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        $otherUserID = Yii::$app->request->post('otherUserID');
        $otherUserID = (isset($otherUserID) && is_numeric($otherUserID)) ? (int)$otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => $result2['message'],
            ];
        }

        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN && $user->user_type != User::USER_SITEADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to delete user {$otherUser->id}");

                return [
                    'success' => FALSE,
                    'code'    => 1,
                    'message' => 'Access Denied',
                ];
            }
        }

        $result = $otherUser->deleteUser();

        return $result;

        LogTable::addLog($user->id, $user->user_type, 'delete user ID:' . $otherUser->id);

        return [
            'success' => $result,
            'message' => 'user delete',
        ];

    }

    public function actionAddPricelist()
    {

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if ($user->user_type != User::USER_SUPERADMIN) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $skinID       = Yii::$app->request->post('skinID');
        $planID       = Yii::$app->request->post('planID');
        $actionTypeID = Yii::$app->request->post('actionTypeID');
        $price        = Yii::$app->request->post('price');
        $pricelistID  = Yii::$app->request->post('pricelistID');
        $otherUserID  = Yii::$app->request->post('otherUserID');
        $startDate    = (Yii::$app->request->post('startDate') && strtotime(Yii::$app->request->post('startDate'))) ? Yii::$app->request->post('startDate') : date("Y-m-d H:i:s",time());
        $stopDate     = (Yii::$app->request->post('stopDate') && strtotime(Yii::$app->request->post('stopDate'))) ? Yii::$app->request->post('stopDate') : null;
        if($stopDate != null && $startDate >= $stopDate){
            return [
                'success' => FALSE,
                'message' => 'Start date can\'t be greater than stop date',
            ];
        }

        if (isset($pricelistID) && is_numeric($pricelistID)) {
            $pricelist = Pricelist::findOne(['id' => $pricelistID]);
            if (empty($pricelist)) {
                return [
                    'success' => FALSE,
                    'code'    => 1,
                    'message' => 'Missing params',
                ];
            }
            $skinID = $pricelist->skin_id;
            $planID = $pricelist->plan_id;
            $actionTypeID = $pricelist->action_type_id;
        } else {
            $pricelist = new Pricelist();
        }

        if (!isset($skinID) || !isset($planID) || !isset($actionTypeID) || !isset($price)) {
            return [
                'success' => FALSE,
                'message' => 'Missing params',
            ];
        }



        $otherUserID = isset($otherUserID) && trim($otherUserID) != "" ? trim($otherUserID) : null;
        $pricelist->load([
            'Pricelist' => [
                'id'             => $pricelistID,
                'user_id'        => $otherUserID,
                'skin_id'        => $skinID,
                'plan_id'        => $planID,
                'action_type_id' => $actionTypeID,
                'price'          => $price,
                'start_date'     => $startDate,
                'stop_date'      => $stopDate,
                'important'      => (isset($pricelist->important)) ? $pricelist->important : 0,
            ],
        ]);
        if (!$pricelist->save()) {
            return [
                'success' => FALSE,
                'message' => 'Missing params',
            ];
        }

        $actionTypeList = ActionType::find()->asArray()->all();
        $allPricelist   = Pricelist::find()->where(['skin_id' => $skinID])->asArray()->all();

        LogTable::addLog($user->id, $user->user_type, 'add/update pricelist ID: ' . $pricelist->id);

        return [
            'success'        => TRUE,
            'actionTypeList' => $actionTypeList,
            'pricelist'      => $allPricelist,
        ];

    }

    public function actionAddAgencyPricelist()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $skinID       = Yii::$app->request->post('skinID');
        $planID       = Yii::$app->request->post('planID');
        $actionTypeID = Yii::$app->request->post('actionTypeID');
        $price        = Yii::$app->request->post('price');
        $startDate    = (Yii::$app->request->post('startDate') && strtotime(Yii::$app->request->post('startDate'))) ? Yii::$app->request->post('startDate') : date("Y-m-d H:i:s",time());
        $stopDate     = (Yii::$app->request->post('stopDate') && strtotime(Yii::$app->request->post('stopDate'))) ? Yii::$app->request->post('stopDate') : null;

        if($stopDate !== null && $startDate >= $stopDate){
            return [
                'success' => FALSE,
                'message' => 'Start date can\'t be greater than stop date1',
            ];
        }

        $pricelistID = Yii::$app->request->post('pricelistID');
        $otherUserID = Yii::$app->request->post('otherUserID');

        if (isset($pricelistID) && is_numeric($pricelistID)) {
            $pricelist = PricelistAgency::findOne(['id' => $pricelistID]);
            if (empty($pricelist)) {
                return [
                    'success' => FALSE,
                    'code'    => 1,
                    'message' => 'Missing params',
                ];
            }
            $skinID = $pricelist->skin_id;
            $planID = $pricelist->plan_id;
            $actionTypeID = $pricelist->action_type_id;
        } else {
            $pricelist = new PricelistAgency();
        }

        if (!isset($skinID) || !isset($planID) || !isset($actionTypeID) || !isset($price)) {
            return [
                'success' => FALSE,
                'message' => 'Missing params',
            ];
        }

        if ($user->user_type != User::USER_SUPERADMIN) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }



        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? (int)$otherUserID : null;
        $pricelist->load([
            'PricelistAgency' => [
                'id'             => $pricelistID,
                'user_id'        => $otherUserID,
                'skin_id'        => $skinID,
                'plan_id'        => $planID,
                'action_type_id' => $actionTypeID,
                'start_date'     => $startDate,
                'stop_date'      => $stopDate,
                'price'          => $price,
                'important'      => (isset($pricelist->important)) ? $pricelist->important : 0,
            ],
        ]);
        if (!$pricelist->save()) {
            return [
                'success' => FALSE,
                'message' => join(', ', $pricelist->getFirstErrors()),
            ];
        }

        $actionTypeList = ActionType::find()->asArray()->all();
        $allPricelist   = (new Query())
            ->from([PricelistAgency::tableName()])
            ->select('pricelist_agency.*, agency.name')
            ->leftJoin(User::tableName(), 'user.id = pricelist_agency.user_id')
            ->leftJoin(Agency::tableName(), 'agency.id = user.agency_id')
            ->where(['pricelist_agency.skin_id' => $skinID])
            ->all();

        LogTable::addLog($user->id, $user->user_type, 'add/update agency pricelist ID: ' . $pricelist->id);

        return [
            'success'        => TRUE,
            'actionTypeList' => $actionTypeList,
            'pricelist'      => $allPricelist,
        ];

    }

    public function actionGetPenalties()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }
        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_SITEADMIN, User::USER_ADMIN, User::USER_SUPERADMIN])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('finance_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('finance_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        $dateFrom = Yii::$app->request->post('dateFrom');
        $dateTo = Yii::$app->request->post('dateTo');
        $agencyID = Yii::$app->request->post('agencyID');

        $dateFrom = (isset($dateFrom) && strtotime($dateFrom)) ? $dateFrom : null;
        $dateTo   = (isset($dateTo) && strtotime($dateTo)) ? $dateTo . " 23:59:59" : null;
        $agencyID = (isset($agencyID) && is_numeric($agencyID)) ? (int)$agencyID : null;

        $result = $user->checkOwn($agencyID);
        if (!$result) {
            return [
                'success' => FALSE, 
                'code' => 2, 
                'message' => 'Access denied'
            ];
        }

        $shopService   = new ShopService($user->id);
        $letterService = new LetterService($user->id);

        $penaltyGifts = $shopService->getPenaltyShopItems($dateFrom, $dateTo, $agencyID);

        $penaltyLetters = $letterService->getExpired($dateFrom, $dateTo, $agencyID);

        $otherPenalty = $user->getOtherPenalty($dateFrom, $dateTo, $user->user_type, $agencyID);

        return [
            'success'        => TRUE,
            'penaltyGifts'   => $penaltyGifts,
            'penaltyLetters' => $penaltyLetters,
            'otherPenalty'   => $otherPenalty            
        ];
    }

    public function actionGetPenalty($penaltyID = null)
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_SITEADMIN, User::USER_ADMIN, User::USER_SUPERADMIN])) {
        	return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('finance_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('finance_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        $shopService   = new ShopService($user->id);
        $letterService = new LetterService($user->id);

        $dateFrom = Yii::$app->request->post('dateFrom');
        $dateTo = Yii::$app->request->post('dateTo');
        $agencyID = Yii::$app->request->post('agencyID');
        $penaltyID = Yii::$app->request->post('penaltyID');

        $dateFrom = (isset($dateFrom) && strtotime($dateFrom)) ? $dateFrom : null;
        $dateTo   = (isset($dateTo) && strtotime($dateTo)) ? $dateTo . " 23:59:59" : null;
        $agencyID = (isset($agencyID) && is_numeric($agencyID)) ? (int)$agencyID : null;

        $penaltyGifts = $shopService->getPenaltyShopItems($dateFrom, $dateTo, $agencyID);

        $penaltyLetters = $letterService->getExpired($dateFrom, $dateTo, $agencyID);

        $penaltyID    = isset($penaltyID) && is_numeric($penaltyID) ? (int)$penaltyID : null;

        if ($user->user_type == User::USER_ADMIN) {
        	$agencyUser = User::findOne(['id' => $agencyID]);
        	if ($agencyUser->agency_id == $user->agency_id) {
        		$user = $agencyUser;
        	} else {
        		return [
	                'success' => FALSE,
	                'code'    => 1,
	                'message' => 'Access Denied',
        		];
        	}
        }

        $otherPenalty = $user->getOtherPenalty($user->user_type, $penaltyID, $dateFrom, $dateTo, $agencyID);

        return [
            'success'        => TRUE,
            'penaltyGifts'   => $penaltyGifts,
            'penaltyLetters' => $penaltyLetters,
            'otherPenalty'   => $otherPenalty,
            //'1' => [$dateFrom, $dateTo, $agencyID, $penaltyID]
        ];
    }

    public function actionAddPenalty()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if ($user->user_type != User::USER_SUPERADMIN) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $otherUserID  = Yii::$app->request->post('otherUserID');
        $description  = Yii::$app->request->post('description');
        $penaltyID    = Yii::$app->request->post('penaltyID');
        $penaltyValue = Yii::$app->request->post('penaltyValue');

        if (!isset($otherUserID) || !is_numeric($otherUserID) || !isset($description) || trim($description) == "") {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $penaltyID = isset($penaltyID) && is_numeric($penaltyID) ? $penaltyID : null;
        if ($penaltyID != null) {
            $otherPenaltyModel = OtherAgencyPenalty::findOne(['id' => $penaltyID]);
            if (empty($otherPenaltyModel)) {
                $otherPenaltyModel = new OtherAgencyPenalty();
            }
        } else {
            $otherPenaltyModel = new OtherAgencyPenalty();
        }

        $otherPenaltyModel->user_id       = (int)$otherUserID;
        $otherPenaltyModel->penalty_value = (float)$penaltyValue;
        $otherPenaltyModel->description   = trim($description);

        if (!$otherPenaltyModel->save()) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => join(', ', $otherPenaltyModel->getFirstErrors()),
            ];
        }

        LogTable::addLog($user->id, $user->user_type, 'add penalty to user ID: ' . $otherUserID . ", value: " . $penaltyValue . ", description: " . trim($description));

        return ['success' => TRUE];

    }

    public function actionDeletePenalty()
    {

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }
        $penaltyID = Yii::$app->request->post('penaltyID');

        if (!isset($penaltyID) || !is_numeric($penaltyID)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        if ($user->user_type != User::USER_SUPERADMIN) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $penaltyID = isset($penaltyID) && is_numeric($penaltyID) ? $penaltyID : null;

        $otherPenalty = OtherAgencyPenalty::findOne(['id' => $penaltyID]);

        if (empty($otherPenalty)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Penalty not found',
            ];
        }

        $otherPenalty->delete();

        LogTable::addLog($user->id, $user->user_type, 'delete penalty ID: ' . $penaltyID);

        return [
            'success' => TRUE,
            'message' => 'Penalty was deleted',
        ];

    }


    public function actionGetPricelist()
    {

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if ($user->user_type != User::USER_SUPERADMIN) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $skinID = Yii::$app->request->post('skinID');
        if (!isset($skinID) || !is_numeric($skinID)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $actionTypeList = ActionType::find()->asArray()->all();
        $pricelist      = Pricelist::find()->where(['skin_id' => (int)$skinID])->asArray()->all();

        return [
            'success'        => TRUE,
            'actionTypeList' => $actionTypeList,
            'pricelist'      => $pricelist,
        ];

    }

    public function actionGetPricelistAgency()
    {

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $skinID = Yii::$app->request->post('skinID');
        if (!isset($skinID) || !is_numeric($skinID)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('finance_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('finance_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        $actionTypeList = ActionType::find()->asArray()->all();

        if ($user->user_type != User::USER_SUPERADMIN && $user->user_type != User::USER_SITEADMIN) {
            $agency = $user->getAgency()->where(['approve_status' => 4])->andWhere(['!=', 'id', 1])->one();
            if (!empty($agency)) {
                $user = User::find()->where(['agency_id' => $agency->id, 'user_type' => User::USER_AGENCY])->one();
            }
            if (empty($user))
                return [
                    'success' => FALSE,
                    'code'    => 1,
                    'message' => 'Agency not found',
                ];
            $billingService = new BillingService(); //2 - letter, 4 - video, 5 - chat, 6 - watch video
            $actionType     = ActionType::findOne(['name' => 'send letter']);
            if (!empty($actionType)) {
                $priceLetter = $billingService->getAgencyPrice($skinID, $planID = 1, $actionType->id, $user->id);
            }
            $actionType = ActionType::findOne(['name' => 'videochat']);
            if (!empty($actionType)) {
                $priceVideoChat = $billingService->getAgencyPrice($skinID, $planID = 1, $actionType->id, $user->id);
            }
            $actionType = ActionType::findOne(['name' => 'videochat']);
            if (!empty($actionType)) {
                $priceChat = $billingService->getAgencyPrice($skinID, $planID = 1, $actionType->id, $user->id);
            }
            $actionType = ActionType::findOne(['name' => 'watch video']);
            if (!empty($actionType)) {
                $priceWatchVideo = $billingService->getAgencyPrice($skinID, $planID = 1, $actionType->id, $user->id);
            }

            return [
                'success'         => TRUE,
                'priceLetter'     => $priceLetter,
                'priceVideoChat' => $priceVideoChat,
                'priceChat'       => $priceChat,
                'priceWatchVideo' => $priceWatchVideo,
            ];

        } else {
            $pricelist = (new Query())
                ->from([PricelistAgency::tableName()])
                ->select('pricelist_agency.*, agency.name')
                ->leftJoin(User::tableName(), 'user.id = pricelist_agency.user_id')
                ->leftJoin(Agency::tableName(), 'agency.id = user.agency_id')
                ->where(['pricelist_agency.skin_id' => $skinID])
                ->all();
        }

        return [
            'success'        => TRUE,
            'actionTypeList' => $actionTypeList,
            'pricelist'      => $pricelist,
        ];

    }


    public function actionDeleteAgencyPricelistItem()
    {

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if ($user->user_type != User::USER_SUPERADMIN) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }

        $skinID      = Yii::$app->request->post('skinID');
        $pricelistID = Yii::$app->request->post('pricelistID');

        $skinID      = isset($skinID) && is_numeric($skinID) ? (int)$skinID : null;
        $pricelistID = isset($pricelistID) && is_numeric($pricelistID) ? (int)$pricelistID : null;

        $pricelist = PricelistAgency::findOne(['id' => $pricelistID, 'skin_id' => $skinID]);
        if (empty($pricelist)) {
            return [
                'success' => FALSE,
                'message' => 'Not found pricelist id',
            ];
        }

        if ($pricelist->delete()) {
            LogTable::addLog($user->id, $user->user_type, 'delete agency pricelist item ID: ' . $pricelist->id);

            return [
                'success' => TRUE,
                'message' => 'pricelist was successfully deleted',
            ];
        } else {
            return [
                'success' => FALSE,
                'message' => 'pricelist delete error',
            ];
        }
    }

    public function actionDeletePricelistItem()
    {

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if ($user->user_type != User::USER_SUPERADMIN) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }

        $skinID      = Yii::$app->request->post('skinID');
        $pricelistID = Yii::$app->request->post('pricelistID');

        if (!isset($pricelistID) || !is_numeric($pricelistID) || !isset($skinID) || !is_numeric($skinID)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $pricelist = Pricelist::findOne(['id' => $pricelistID, 'skin_id' => $skinID]);
        if (empty($pricelist)) {
            return [
                'success' => FALSE,
                'message' => 'Not found pricelist id',
            ];
        }

        if ($pricelist->delete()) {
            LogTable::addLog($user->id, $user->user_type, 'delete pricelist item ID: ' . $pricelist->id);

            return [
                'success' => TRUE,
                'message' => 'pricelist item was successfully deleted',
            ];
        } else {
            return ['success' => FALSE, 'message' => 'pricelist delete error'];
        }
    }

    public function actionGetActionPrice()
    {
        $p = Yii::$app->request->post();

        if (empty($p)) {
            throw new BadRequestHttpException('post', 1);
        }

        if (!isset($p['user_id']) || !isset($p['skin_id']) || !isset($p['plan_id']) || !isset($p['action_type_id'])) {
            throw new BadRequestHttpException('Missing key params', 1);
        }

        $billing_model = new Billing();

        //return $billing_model->getActionPrice($p['skin_id'], $p['plan_id'], $p['action_type_id'], $p['user_id']);
        return $billing_model->getAgencyPrice($p['skin_id'], $p['plan_id'], $p['action_type_id'], $p['user_id']);
    }

    public function actionAddAgencyPayments()
    {        
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if ($user->user_type != User::USER_SUPERADMIN) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }
        $orderID = Yii::$app->request->post('orderID');
        $agencyID = Yii::$app->request->post('agencyID');
        $paidValue = Yii::$app->request->post('paidValue');
        $comments = Yii::$app->request->post('comments');

        $model = DynamicModel::validateData(compact('orderID', 'agencyID', 'paidValue', 'comments'), [
            [['comments'], 'string'],
            [['orderID', 'agencyID'], 'integer'],
            ['paidValue', 'number']
        ]);

        if ($model->hasErrors()) {
            // validation fails
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => join(', ', $model->getFirstErrors()),
            ];
        } else {            
            $result = PaymentsSchedule::addPayments($agencyID, $orderID, $paidValue, $comments);
            if($result['success']){
                LogTable::addLog($user->id, $user->user_type, 'Add agency payments. Order ID: ' . $orderID." Agency ID: ".$agencyID." Sum: ".$paidValue);
            }
            return $result;
        }        

    }

    public function actionTransferAgencyPayments()
    {

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if ($user->user_type != User::USER_SUPERADMIN) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }

        $orderID = Yii::$app->request->post('orderID');

        $model = DynamicModel::validateData(compact('orderID'), [            
            ['orderID', 'integer', 'min' => 1],
        ]);
        if ($model->hasErrors()) {
            // validation fails
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => join(', ', $model->getFirstErrors()),
            ];
        } else {
            // validation succeeds
            $result = PaymentsSchedule::transferPayments($orderID);
            if ($result['success']) {
                LogTable::addLog($user->id, $user->user_type, 'Transfer agency payments. Order ID: ' . $orderID);
            }
            return $result;
        }

    }

    public function actionCheckAgencyPaymentsSchedule()
    {

        $agencyList       = Agency::find()->asArray()->all();
        $updateCurrentMonth = [];
        if (!empty($agencyList)) {
            $billingService = new BillingService();
            $dateFrom     = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m'), 1, date('Y')));
            $dateTo       = date('Y-m-d H:i:s', time());

            foreach ($agencyList as $agency) {
                $user = User::find()->where(['agency_id' => $agency['id'], 'user_type' => User::USER_AGENCY, 'status' => User::STATUS_ACTIVE])->andWhere(['!=', 'agency_id', 1])->one();
                if (!empty($user)) {
                    // get other penalty for agency
                    $agencyBalance      = $billingService->getAgencyTotalBalance($user->id, $dateFrom, $dateTo);                    
                    $earnings            = (isset($agencyBalance['bonus'][0]['total_amount'])) ? $agencyBalance['bonus'][0]['total_amount'] : 0;
                    $penalty             = (isset($agencyBalance['agency_total_penalty'][0]['total_penalty_value'])) ? (float)$agencyBalance['agency_total_penalty'][0]['total_penalty_value'] : 0;
                    //get letter penalty for agency
                    $letterService = new LetterService($user->id);
                    $letterPenalty = $letterService->getExpired($dateFrom, $dateTo, $user->id);
                    
                    foreach ($letterPenalty['lettersArray'] as $letter) {                        
                        $penalty += $letter['penalty_value'];                        
                    }
                    // get gifts penalty for agency
                    $shopService   = new ShopService($user->id);
                    $penaltyGifts = $shopService->getPenaltyShopItems($dateFrom, $dateTo, $user->id);
                    
                    foreach ($penaltyGifts as $gift) {                        
                        $penalty += $gift['penalty_value'];                        
                    }
                    $updateCurrentMonth[] = PaymentsSchedule::updateCurrMonth($user->id, $earnings, $penalty);
                }
            }
        }

        return $updateCurrentMonth;

    }

    public function actionCheckPenalty()
    {
        $shopItemsArray = new ShopService();
        $result = $shopItemsArray->updatePenaltyShopItem();

        $lettersItemsArray = new LetterService();
        $result2  = $lettersItemsArray->updatePenaltyLettersItem();

        return [
            'success' => TRUE, 
            'code' => 1, 
            'message' => 'ok', 
            'gifts' => $result, 
            'letters' => $result2
        ];
    }


    public function actionRegisterNewUser()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $user = Yii::$app->user->identity;

        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $model = new RegisterNewUserForm();

        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '')) {
            $result = $model->signup();
            if ($result['success']) {
                LogTable::addLog($user->id, $user->user_type, 'Created new user with id ' . $result['user']->id);

                return [
                    'success' => TRUE,
                    'userID'  => $result['user']->id,
                ];
            } else {
                return [
                    'success' => FALSE,
                    'code'    => 'Validation errors',
                    'errors'  => $result['errors'],
                ];
            }
        } else {
            LogTable::addLog($user->id, $user->user_type, 'Error during register new user in admin');

            return [
                'success' => 'false',
                'code'    => 1,
                'message' => 'An error occurred while processing your request. If you think this is a bug, please contact us.',
            ];
        }
    }

    public function actionGetPhotos()
    {
        $otherUserID = Yii::$app->request->post('otherUserID');

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => $result2['message'],
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && ($otherUser->id != $user->id) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && ($otherUser->id != $user->id) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN && $user->user_type != User::USER_SITEADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to get photo from user {$otherUser->id}");
                throw new ForbiddenHttpException('Access Denied', 10003);
            }
        }

        $albumID = Yii::$app->request->post('albumID');

        $albumService = new AlbumService($otherUser->id);

        $result = $albumService->getPhotos($albumID, $status = 1);

        $albumInfo = $albumService->getAlbumInfo($albumID);

        return [
            'success'   => TRUE,
            'photos'    => $result,
            'albumInfo' => $albumInfo,
        ];
    }

    public function actionGetUserFinanceInfo()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_SITEADMIN, User::USER_SUPERADMIN, User::USER_ADMIN])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('finance_access') || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('finance_access') || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        $otherUserID = Yii::$app->request->post('otherUserID');
        $agencyID    = Yii::$app->request->post('agencyID');
        $agencyID    = isset($agencyID) && is_numeric($agencyID) ? $agencyID : null;

        if ($agencyID != null && $user->user_type == User::USER_SUPERADMIN) {
            $user = User::findOne(['id' => $agencyID, 'user_type' => User::USER_AGENCY]);
        } elseif ($agencyID != null && in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            $user = User::findOne(['agency_id' => $user->agency_id, 'user_type' => User::USER_AGENCY]);
        }

        $limit        = Yii::$app->request->post('limit');
        $offset       = Yii::$app->request->post('offset');
        $searchParams = Yii::$app->request->post('searchParams');

        $myChildrenIDs = null;
        if ($otherUserID == null) {
            $myChildrenIDs = $user->getMyChidrensIds();

            if (empty($myChildrenIDs)) {
                return [
                    'success'          => TRUE,
                    'usersFinanceInfo' => [
                        'letters'     => [
                            'count' => 0,
                        ],
                        'chats'       => [
                            'count' => 0,
                        ],
                        'video_chats' => [
                            'count' => 0,
                        ],
                        'gifts'       => [
                            'count' => 0,
                        ],
                        'premiumPhotos' => [
                            'count' => 0,
                        ],
                        'PremiumVideo' => [
                            'count' => 0,
                        ],
                    ],
                ];
            }
            $paymentsService  = new PaymentsService($user->id);
            $usersFinanceInfo = $paymentsService->getGroupFinanceInfo($myChildrenIDs, $searchParams, $limit, $offset);
        } else {
            $result = $user->checkOwn($otherUserID);
            if (!$result) {
                return [
                    'success' => FALSE,
                    'code'    => 1,
                    'message' => 'Missing params',
                ];
            }
            $paymentsService  = new PaymentsService($user->id);
            $usersFinanceInfo = $paymentsService->getUserFinanceInfo($otherUserID, $searchParams);
        }

        return [
            'success'          => TRUE,
            'usersFinanceInfo' => $usersFinanceInfo,
        ];

    }

    public function actionGetAlbumPhoto()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $albumName   = Yii::$app->request->post('albumName');
        $otherUserID = Yii::$app->request->post('otherUserID');

        $titles      = (!empty($albumName)) ? trim($albumName) : null;
        $otherUserID = (isset($otherUserID) && is_numeric($otherUserID)) ? (int)$otherUserID : $user->id;
        if (in_array($user->user_type, [User::USER_ADMIN]) && ($user->id != $otherUserID) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && ($user->id != $otherUserID) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        if (empty($titles)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }
        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => $result2['message'],
            ];
        }

        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN && $user->user_type != User::USER_SITEADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to get foto of user {$otherUser->id}");

                return [
                    'success' => FALSE,
                    'code'    => 1,
                    'message' => 'Access Denied',
                ];
            }
        }

        $album  = new AlbumService($otherUser->id);
        $result = $album->getPhotos($titles);

        return [
            'success' => TRUE,
            'photos'  => $result,
        ];
    }

    public function actionGetAgencyFinanceInfo()
    {
        $postArray = Yii::$app->request->post();
        $user      = Yii::$app->user->identity;
        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('finance_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('finance_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        if (empty($postArray['agencyID']) || !filter_var($postArray['agencyID'], FILTER_VALIDATE_INT)) {
            LogTable::addLog($user->id, $user->user_type, 'Error during get agency finance info. Check post array');

            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        if (isset($user) && $user->user_type != User::USER_SUPERADMIN) {
            if ($user->id != $postArray['agencyID']) {
                $agencyUser = User::findOne(['id' => $postArray['agencyID']]);
                if (!empty($agencyUser)) {
                    $userBelongsToAgency = $user->agency_id == $agencyUser->agency_id;
                }
                if (empty($userBelongsToAgency)) {
                    LogTable::addLog($user->id, $user->user_type, 'Error during get agency finance info. User not belong to agency');

                    return [
                        'success' => FALSE,
                        'code'    => 1,
                        'message' => 'Missing params',
                    ];
                }
            }
        }

        $agencyUser = User::findOne(['id' => $postArray['agencyID']]);
        $agency     = $agencyUser->getAgency()->where(['approve_status' => 4])->andWhere(['!=', 'id', 1])->one();
        if (!empty($agency)) {
            $agencyUsersIds = $agency->getUsersIDs('girls');
        } else {
            $agencyUsersIds = [];
        }

        $searchParams    = isset($postArray['searchParams']) ? $postArray['searchParams'] : null;
        $paymentsService = new PaymentsService($user->id);
        $agencyArray     = $paymentsService->getAgencyFinanceInfo($agencyUsersIds, $searchParams);

        return [
            'success'     => TRUE,
            'agencyArray' => $agencyArray,
        ];

    }

    public function actionGetAgency()
    {
        $status = Yii::$app->request->post('status');
        $user   = Yii::$app->user->identity;
        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_SUPERADMIN, User::USER_ADMIN, User::USER_SITEADMIN])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }

        $agencies = Agency::find()
            ->select('user.id as user_id, user.status, user.owner_status, user.approve_status_id, agency.id, 
                        agency.name, agency.contact_person, agency.approve_status, photo.small_thumb, photo.medium_thumb')
            ->leftJoin('user', 'user.agency_id = agency.id')
            ->leftJoin('photo', 'photo.id = user.avatar_photo_id')
            ->where(['user_type' => User::USER_AGENCY]);
        if ($status && is_numeric($status)) {
            $agencies->andWhere(['agency.approve_status' => $status]);
        }

        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            $agencies->andWhere(['agency.id' => $user->agency_id]);
        }

        $agencies->andWhere(['!=', 'agency.id', 1]);
        $agencyArray = $agencies->asArray()->all();
        $agencyCount = $agencies->count();

        return [
            'success'     => TRUE,
            'agencyArray' => $agencyArray,
            'count'       => $agencyCount,
        ];

    }

    public function actionGetAgenciesWithFinanceInfo(){
        $user   = Yii::$app->user->identity;
        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!in_array($user->user_type, [User::USER_SUPERADMIN])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }

        $postArray = Yii::$app->request->post();
        $searchParams    = isset($postArray['searchParams']) ? $postArray['searchParams'] : null;

        $agencies_mass = (new PaymentsService())->getAgenciesFinanceInfo($searchParams);
        //var_dump($agencies_mass); die;
        $temp = [];
        $i = 0;
        foreach ($agencies_mass['agencyList'] as $agency) {
            $temp[$i] = $agency;
            $agency = Agency::findOne(['id' => $agency['id']]);
            $temp[$i]['ruleViolations'] = $agency->violations;
            $i++;
        }
        $agencies_mass['agencyList'] = $temp;
        return [
            'success'     => TRUE,
            'agencyArray' => $agencies_mass,
        ];
    }

    public function actionGetAffiliates()
    {
        $user   = Yii::$app->user->identity;
        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!in_array($user->user_type, [User::USER_SUPERADMIN])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }

        $affiliates = User::find()
            ->select('user.id as user_id, user.status, user.approve_status_id, photo.small_thumb, photo.medium_thumb, affiliates.name')
            ->leftJoin('photo', 'photo.id = user.avatar_photo_id')
            ->leftJoin('affiliates','affiliates.id = user.id')
            ->where(['user_type' => User::USER_AFFILIATE]);

        $affiliatesArray = $affiliates->asArray()->all();
        $affiliatesCount = $affiliates->count();

        return [
            'success'     => TRUE,
            'affiliatesArray' => $affiliatesArray,
            'count'       => $affiliatesCount,
        ];

    }

    public function actionGetMessage()
    {

        $user = Yii::$app->user->identity;

        if (!$user) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        if (!in_array($user->user_type, [User::USER_SUPERADMIN, User::USER_SITEADMIN, User::USER_ADMIN, User::USER_AGENCY])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access denied',
            ];
        }

        $messageID    = Yii::$app->request->post('messageID');
        $limit        = Yii::$app->request->post('limit');
        $offset       = Yii::$app->request->post('offset');
        $read         = Yii::$app->request->post('read');
        $searchParams = Yii::$app->request->post('searchParams');

        $messageID = isset($messageID) && is_numeric($messageID) ? (int)$messageID : null;
        $limit     = isset($limit) && is_numeric($limit) ? (int)$limit : null;
        $offset    = isset($offset) && is_numeric($offset) ? $offset : null;
        $params    = isset($searchParams) ? $searchParams : null;

        $messageService = new MessageService($user->id);
        $messageArray   = $messageService->getMessageToAdmin($messageID, $limit, $offset, $params);

        if (!empty($messageID) && isset($read) && $read == 1) {
            $messageService->readMessage($messageID);
        }

        return $messageArray;
    }

    public function actionGetUserTypes()
    {

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_SUPERADMIN, User::USER_SITEADMIN, User::USER_ADMIN])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        $userTypes = UserType::find()->all();

        return [
            'success'   => TRUE,
            'userTypes' => $userTypes,
        ];

    }

    public function actionSetCustomPageText()
    {
        $token       = Yii::$app->request->post('token');
        $description = Html::encode(Yii::$app->request->post('description'));
        $pageName    = Yii::$app->request->post('pageName');

        if (!isset($token) || !isset($description) ||
            trim($description) == '' || !isset($pageName) ||
            trim($pageName) == ''
        ) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        if ($user->user_type != User::USER_SUPERADMIN) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $sitePage = SitePage::findOne(['slug' => $pageName]);

        if (empty($sitePage)) {
            $sitePage       = new SitePage();
            $sitePage->slug = $pageName;
        }

        $sitePage->description = $description;

        if (!$sitePage->save()) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Page not save',
            ];
        }

        LogTable::addLog($user->id, $user->user_type, 'update text on page : ' . $pageName);

        return [
            'success' => TRUE,
        ];
    }

    public function actionGetTotalPeopleCount()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }        

        $skinID = Yii::$app->request->post('skinID');
        if (!isset($skinID) || !is_numeric($skinID) || $skinID != 1) {
            return [
                'success' => FALSE, 
                'code' => 2, 
                'message' => 'Access denied'
            ];
        }

        if (!in_array($user->user_type, [User::USER_SUPERADMIN, User::USER_SITEADMIN])) {
            return [
                'success' => FALSE, 
                'code' => 2, 
                'message' => 'Access denied'
            ];
        }

        $menQuery = User::find()->where([
                            'user_type' => User::USER_MALE, 
                            'status' => User::STATUS_ACTIVE, 
                            'owner_status' => User::STATUS_ACTIVE, 
                            'approve_status_id' => ApproveStatus::STATUS_APPROVED
                        ]);
        $menTotal = $menQuery->count();
        $menOnline = $menQuery->andWhere(['>', 'last_activity', time()-30])->count();

        $womenQuery = User::find()->where([
                            'user_type' => User::USER_FEMALE, 
                            'status' => User::STATUS_ACTIVE, 
                            'owner_status' => User::STATUS_ACTIVE, 
                            'approve_status_id' => ApproveStatus::STATUS_APPROVED
                        ]);
        $womenTotal = $womenQuery->count();
        $womenOnline  = $womenQuery->andWhere(['>', 'last_activity', time()-30])->count();
        $agencyQuery = User::find()->where([
                            'user_type' => User::USER_AGENCY, 
                            'status' => User::STATUS_ACTIVE, 
                            'owner_status' => User::STATUS_ACTIVE, 
                            'approve_status_id' => ApproveStatus::STATUS_APPROVED
                        ]);
        $agencyTotal = $agencyQuery->count();
        $agencyOnline = $agencyQuery->andWhere(['>', 'last_activity', time()-30])->count();

        return [
            'success' => TRUE, 
            'code' => 1,         
            'men' => [
                'online' => $menOnline,
                'total' => $menTotal
            ], 
            'women' => [
                'online' => $womenOnline,
                'total' => $womenTotal,
            ],
            'agency' => [
                'online' => $agencyOnline,
                'total' => $agencyTotal
            ], 
        ];
    }

    public function actionSetUserActivity()
    {
        $otherUserID = Yii::$app->request->post('otherUserID');

        $deactivationText = Yii::$app->request->post('deactivationText');

        $deactivationText = (isset($deactivationText) && is_string($deactivationText)) ? HtmlPurifier::process($deactivationText) : '';

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $otherUserID = (isset($otherUserID) || is_numeric($otherUserID)) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => $result2['message'],
            ];
        }
        $otherUserProfile = UserProfile::findOne(['id' => $otherUser->id]);
        if (!$otherUserProfile) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found'
            ];
        }

        if ($otherUser->status == User::STATUS_ACTIVE && empty($deactivationText)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Please enter deactivation text',
            ];
        }       
        
        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN]) && $otherUserProfile->deactivation_by_agency == 1) {
            if ($otherUser->status == User::STATUS_NO_ACTIVE) {
                $userStatus = User::STATUS_ACTIVE;
            } elseif ($otherUser->status == User::STATUS_ACTIVE) {
                $userStatus = User::STATUS_NO_ACTIVE;
            }
            $otherUserProfile->deactivation_reason = $deactivationText;
            $otherUserProfile->deactivation_time = date('Y-m-d H:i:s');
            $otherUserProfile->deactivation_by_agency = 1;
            $otherUser->status = $userStatus;                        
            $messageReceivers = User::find()->where(['user_type' => User::USER_SUPERADMIN])->all();
            foreach ($messageReceivers as $admin) {
                $systemMessage = new Message();
                $systemMessage->message_creator = $user->id;
                $systemMessage->message_receiver = $admin->id;                
                if ($otherUser->status == User::STATUS_ACTIVE) {
                    $systemMessage->title = 'User profile was activated';
                    $systemMessage->description = 'User with id ' . $user->id . ' activate user profile with id ' . $otherUser->id;
                } elseif ($otherUser->status == User::STATUS_NO_ACTIVE) {
                    $systemMessage->title = 'User profile was deactivated';
                    $systemMessage->description = 'User profile was deactivated with deactivation reason : <br />' . $deactivationText;
                }
                $systemMessage->save();
            }
        } elseif (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN]) && $otherUserProfile->deactivation_by_agency == 0) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'You cannot activate this profile. Profile was deactivated by site admin.',
            ];
        } elseif ($user->user_type == User::USER_SUPERADMIN) {
            if ($otherUser->status == User::STATUS_NO_ACTIVE) {
                $userStatus = User::STATUS_ACTIVE;
                $otherUserProfile->deactivation_by_agency = 1;
            } elseif ($otherUser->status == User::STATUS_ACTIVE) {
                $userStatus = User::STATUS_NO_ACTIVE;
                $otherUserProfile->deactivation_by_agency = 0;
            }             
            $otherUserProfile->deactivation_reason = $deactivationText;
            $otherUserProfile->deactivation_time = date('Y-m-d H:i:s');            
            $otherUser->status = $userStatus;
        }

        if ($otherUser->user_type == User::USER_AGENCY) {
            $result = User::updateAll(['owner_status' => $userStatus], ['in', 'agency_id', [$otherUser->agency_id]]);
        }

        if ($otherUser->save() && $otherUserProfile->save()) {
            if ($otherUser->status == User::STATUS_NO_ACTIVE) {
                $statusString = 'no active';
            } elseif ($otherUser->visible == User::STATUS_ACTIVE) {
                $statusString = 'active';
            } else {
                $statusString = 'not defined';
            }
            LogTable::addLog($user->id, $user->user_type, 'set user ID: ' . $otherUser->id . ' activity status: ' . $statusString);
        } else {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Error during status change',
            ];
        }

        return [
            'success' => TRUE,
            'status'  => ($otherUser->status == User::STATUS_ACTIVE) ? 1 : 0,
        ];
    }

    public function actionSetUserInvisible()
    {
        $token       = Yii::$app->request->post('token');
        $otherUserID = Yii::$app->request->post('otherUserID');

        if (!isset($token)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $otherUserID = (isset($otherUserID) || is_numeric($otherUserID)) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => $result2['message'],
            ];
        }
        if ($otherUser->visible == User::STATUS_VISIBLE) {
            $otherUser->visible = User::STATUS_INVISIBLE;
        } else {
            $otherUser->visible = User::STATUS_VISIBLE;
        }

        if ($otherUser->save()) {
            if ($otherUser->visible == User::STATUS_INVISIBLE) {
                $statusString = 'invisible';
            } elseif ($otherUser->visible == User::STATUS_VISIBLE) {
                $statusString = 'active';
            } else {
                $statusString = 'not defined';
            }
            LogTable::addLog($user->id, $user->user_type, 'set user ID: ' . $otherUser->id . ' invisible status: ' . $statusString);
        } else {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Error during status change',
            ];
        }

        return [
            'success' => TRUE,
            'status'  => ($otherUser->visible == User::STATUS_INVISIBLE) ? 1 : 0,
        ];
    }

    public function actionReadLetter()
    {
        $letterID = Yii::$app->request->post('letterID');
        $otherUserID = Yii::$app->request->post('otherUserID');

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => 'User not found'
            ];
        }

        if (empty($letterID) || !is_numeric($letterID)) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }

        $otherUserID = (isset($otherUserID) && is_numeric($otherUserID)) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);
        
        if (!$result2['success']) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => $result2['message'],
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && ($user->id != $otherUserID) && (!isset($user->permissions) || $user->permissions->cantAccess('letter_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && ($user->id != $otherUserID) && (!isset($user->permissions) || $user->permissions->cantAccess('letter_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to read letter of user {$otherUser->id}");

                return [
                    'success' => FALSE,
                    'code'    => 1,
                    'message' => 'Access Denied',
                ];
            }
        }

        $letterService = new LetterService($otherUser->id);

        return $letterService->read((int)$letterID);
    }

    public function actionSendLetter()
    {        

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_SUPERADMIN, User::USER_SITEADMIN, User::USER_INTERPRETER])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }        
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('letter_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        $fromUserID = Yii::$app->request->post('fromUserID');
        $fromUserID = isset($fromUserID) && is_numeric($fromUserID) ? $fromUserID : null;
        $user = User::findOne(['id' => $fromUserID]);

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        $otherUserID = Yii::$app->request->post('toUserID');
        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success'] && $result2['code'] != 224) {
            return [
                'success' => false, 
                'code' => $result2['code'],
                'message' => $result2['message']
            ];
        }

        $skinID = Yii::$app->request->post('skinID');
        $title = Yii::$app->request->post('title');
        $description = Yii::$app->request->post('desc');
        $themeID = Yii::$app->request->post('themeID');
        $previousLetterID = Yii::$app->request->post('previousLetterID');

        $title              = isset($title) && trim($title) != "" ? HtmlPurifier::process(trim($title)) : null;
        $description        = isset($description) && trim($description) != "" ? HtmlPurifier::process(trim($description)) : null;
        $themeID            = isset($themeID) && is_numeric($themeID) ? (int)$themeID : null;
        $previousLetterID = isset($previousLetterID) && is_numeric($previousLetterID) ? (int)$previousLetterID : null;

        if (empty($description) || (empty($title) && empty($themeID))) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }
        
        if (!$user->user_type || !$otherUser->user_type || 
            ($user->user_type == $otherUser->user_type) || 
            !in_array($user->user_type, [User::USER_MALE, User::USER_FEMALE]) || 
            !in_array($otherUser->user_type, [User::USER_MALE, User::USER_FEMALE])) {
                return [
                    'success' => false, 
                    'message' => 'Missing params2'
                ];
        }

        if ($user->inBlackList($otherUser->id)) {
            return [
                'success' => false, 
                'message' => 'User add you in black list'
            ];
        }

        $letterService = new LetterService($user->id);

        return $letterService->send('send letter', $skinID, $otherUser->id, $title, $themeID, $description, $previousLetterID);
    }

    public function actionGetUserLetters()
    {
        $otherUserID = Yii::$app->request->post('otherUserID');

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $otherUserID = (isset($otherUserID) && is_numeric($otherUserID)) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);
        
        if (!$result2['success']) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => $result2['message'],
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && ($user->id != $otherUserID) && (!isset($user->permissions) || $user->permissions->cantAccess('letter_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && ($user->id != $otherUserID) && (!isset($user->permissions) || $user->permissions->cantAccess('letter_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        $myChildrenIDs = null;
        if($user->user_type == User::USER_INTERPRETER) {            
            $myChildrenIDs = $user->user_ids;
        } else {
            $myChildrenIDs = $otherUser->getMyChidrensIds('girls');
        }

        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN && $user->user_type != User::USER_SITEADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to get letters of user {$otherUser->id}");

                return [
                    'success' => FALSE,
                    'code'    => 1,
                    'message' => 'Access Denied',
                ];
            }
        }

        $postArray = Yii::$app->request->post();

        $themeID      = isset($postArray['theme_id']) ? $postArray['theme_id'] : null;
        $lettersID     = isset($postArray['letters_id']) ? $postArray['letters_id'] : null;
        $type          = isset($postArray['type']) ? $postArray['type'] : null;
        $limit         = isset($postArray['limit']) ? $postArray['limit'] : null;
        $offset        = isset($postArray['offset']) ? $postArray['offset'] : null;
        $searchParams = isset($postArray['searchParams']) ? $postArray['searchParams'] : null;

        $letterService = new LetterService($otherUser->id);

        $lettersArray = $letterService->get($lettersID, $themeID, $type, $limit, $offset, $searchParams);

        //для детального сообщения получаем предыдущее и следующее сообщение
        if (!empty($lettersID) && $lettersArray['success']) {
            $nextPrevLettersID                  = $letterService->getPrevNextLetersIds($lettersID);
            $lettersArray['next_prev_letters_ids'] = $nextPrevLettersID;
        }

        return $lettersArray;
    }

    public function actionGetLetters()
    {
        $otherUserID = Yii::$app->request->post('otherUserID');

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $otherUserID = (isset($otherUserID) && is_numeric($otherUserID)) ? $otherUserID : $user->id;
        if (in_array($user->user_type, [User::USER_ADMIN]) && ($user->id != $otherUserID) && (!isset($user->permissions) || $user->permissions->cantAccess('letter_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && ($user->id != $otherUserID) && (!isset($user->permissions) || $user->permissions->cantAccess('letter_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);
        
        if (!$result2['success']) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => $result2['message'],
            ];
        }

        $myChildrenIDs = null;
        if($user->user_type == User::USER_INTERPRETER) {            
            $myChildrenIDs = $user->user_ids;
        } else {
            $myChildrenIDs = $otherUser->getMyChidrensIds('girls');
        }       

        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to get letters of user {$otherUser->id}");

                return [
                    'success' => FALSE,
                    'code'    => 1,
                    'message' => 'Access Denied',
                ];
            }
        }
    
        $limit         = Yii::$app->request->post('limit');
        $offset        = Yii::$app->request->post('offset');
        $searchParams = Yii::$app->request->post('searchParams');        

        $letterService = new LetterService($otherUser->id);
        $lettersArray = $letterService->getToAdmin($searchParams, $limit, $offset, $myChildrenIDs);

        return $lettersArray;
    }

    public function actionGetLog()
    {
        $postArray = Yii::$app->request->post();
        if (!isset($postArray['token']) || trim($postArray['token']) == "") {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $user = Yii::$app->user->identity;

        if (empty($user) ||
            !in_array($user->user_type, [User::USER_AGENCY, User::USER_SUPERADMIN, User::USER_ADMIN])
        ) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $otherUserID    = isset($postArray['otherUserID']) && is_numeric($postArray['otherUserID']) ? $postArray['otherUserID'] : null;
        $limit       = isset($postArray['limit']) && is_numeric($postArray['limit']) ? (int)$postArray['limit'] : 10;
        $page       = isset($postArray['page']) && is_numeric($postArray['page']) ? (int)$postArray['page'] : 0;
        $searchParams = isset($postArray['searchParams']) && is_array($postArray['searchParams']) ? $postArray['searchParams'] : null;
        if (!empty($searchParams) && !empty($searchParams['userType'])) {
            if ($searchParams['userType'] == User::USER_SUPERADMIN && $user->user_type != User::USER_SUPERADMIN) {
                $searchParams['userType'] = User::USER_AGENCY;
            }            
        }
        if (!empty($searchParams) && isset($searchParams['dateTo']) && strtotime($searchParams['dateTo'])) {
            $searchParams['dateTo'] = $searchParams['dateTo'] . ' 23:59:59';
        } else {
            $searchParams['dateTo'] = date('Y-m-d', time()) . ' 23:59:59';
        }
        $agencyUsersIds = [];

        if (empty($page) || $page < 0) {
            $page = 0;
        }
        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            $agency = Agency::find()->where(['id' => $user->agency_id])->andWhere(['!=', 'id', 1])->one();
            if (!empty($agency)) {
                $usersInAgency = $agency->getUsers()->where(['status' => [User::STATUS_ACTIVE]])->all();
                foreach ($usersInAgency as $user) {
                    $agencyUsersIds[] = $user->id;
                }
                if (!empty($otherUserID) && !in_array($otherUserID, $agencyUsersIds)) {
                    return [
                        'success' => FALSE,
                        'code'    => 1,
                        'message' => 'Wrong user id',
                    ];
                }
            }
        }

        $log = LogTable::getLog($agencyUsersIds, $otherUserID, $searchParams, $page-1, $limit);
        $userTypes = UserType::find()->asArray()->all();

        $logArray = [];
        $i = 0;
        foreach ($log->getModels() as $row) {
            //$logArray[] = [];
            $logArray[$i]['id'] = $row['id'];
            $logArray[$i]['user_id'] = $row['user_id'];
            foreach ($userTypes as $userType) {
                if ($userType['id'] == $row['user_type']) {
                    $logArray[$i]['title'] = $userType['title'];
                }
            }
            $logArray[$i]['description'] = $row['description'];
            $logArray[$i]['created_at'] = $row['created_at'];            
            $i++;
        }

        return [
            'success'  => TRUE,
            'logArray' => $logArray,
            'count' => $log->getTotalCount()
        ];

    }

    public function actionGetTasks()
    {

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_SITEADMIN, User::USER_SUPERADMIN, User::USER_ADMIN])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access denied',
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('tasks_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('tasks_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        $limit        = Yii::$app->request->post('limit');
        $offset       = Yii::$app->request->post('offset');
        $taskID       = Yii::$app->request->post('taskID');
        $searchParams = Yii::$app->request->post('searchParams');


        $limit        = isset($limit) && is_numeric($limit) ? (int)$limit : null;
        $offset       = isset($offset) && is_numeric($offset) ? (int)$offset : null;
        $taskID       = isset($taskID) && is_numeric($taskID) ? (int)$taskID : null;
        $searchParams = isset($searchParams) ? $searchParams : null;

        $ownerID = $user->id;

        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            $agency = $user->getAgency()->where(['approve_status' => 4])->andWhere(['!=', 'id', 1])->one();
            if (!empty($agency)) {
                $user = User::find()->where(['agency_id' => $agency->id, 'user_type' => User::USER_AGENCY])->one();
            }
            if (empty($user))
                return [
                    'success' => FALSE,
                    'code'    => 1,
                    'message' => 'Agency not found',
                ];
        }

        $result = $user->getUserTasks($user->user_type, $searchParams, $limit, $offset, $taskID);

        if (!empty($taskID)) {
            $comments_read_model = (new Comments())->readCommentsByResourceId($user->user_type, $taskID);
        }

        return $result;

    }

    public function actionAddTask()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_SITEADMIN, User::USER_SUPERADMIN, User::USER_ADMIN])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access denied',
            ];
        }
        if (in_array($user->user_type, [User::USER_ADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('tasks_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('tasks_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        $agencyIDs   = Yii::$app->request->post('agencyIDs');
        $dateTo      = Yii::$app->request->post('dateTo');
        $description = Yii::$app->request->post('description');
        $comment     = Yii::$app->request->post('comment');
        $taskID      = Yii::$app->request->post('taskID');
        $status      = Yii::$app->request->post('status');

        $agencyIDs   = isset($agencyIDs) && is_array($agencyIDs) ? array_filter($agencyIDs, 'is_numeric') : FALSE;
        $dateTo      = isset($dateTo) && strtotime($dateTo) ? $dateTo : FALSE;
        $description = isset($description) && trim($description) != '' ? trim($description) : FALSE;
        $comment     = isset($comment) && trim($comment) != '' ? trim($comment) : FALSE;
        $taskID      = isset($taskID) && is_numeric($taskID) ? $taskID : FALSE;
        $status      = isset($status) && is_numeric($status) ? $status : FALSE;

        if (!isset($agencyIDs)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        //status 1,2 agency only
        if ($user->user_type == User::USER_AGENCY && !in_array((int)$status, [1, 2])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $result = $user->addTask($user->user_type, $agencyIDs, $dateTo, $description, $taskID, $status);

        if ($result['success']) {
            LogTable::addLog($user->id, $user->user_type, 'Add task to agency ID:' . implode(', ', $agencyIDs));

            if ($comment) {
                $fields         = [
                    'resource_type_id' => 1,
                    'resource_id'      => $result['task_id'],
                    'user_id'          => $user->id,
                    'comments'         => $comment,
                ];
                $comments_model = new Comments();
                if (!$comments_model->load(['Comments' => $fields]) || !$comments_model->save()) {
                    return [
                        'success' => FALSE,
                        'code' => 2,
                        'message' => 'error add comments'
                    ];
                }
            }
        }

        return $result;

    }

    public function actionDeleteTask()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $taskID = Yii::$app->request->post('taskID');

        if (!isset($taskID) || !is_numeric($taskID)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        if ($user->user_type != User::USER_SUPERADMIN && $user->user_type != User::USER_SITEADMIN) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access denied',
            ];
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('tasks_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        $result = $user->deleteTask($taskID);

        if ($result['success']) {
            LogTable::addLog($user->id, $user->user_type, 'Delete task ID: ' . $taskID);
        }

        return $result;

    }

    public function actionPricelistPenalty()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $skinID   = Yii::$app->request->post('skinID');
        $typeData = Yii::$app->request->post('typeData');

        if (empty($skinID) || !is_numeric($skinID)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        if (!empty($typeData) && !empty($skinID) && in_array($user->user_type, [User::USER_SUPERADMIN])) {
            // update pricelist
            foreach ($typeData as $type => $price) {
                if (is_numeric($type) && is_numeric($price['price'])) {
                    PricelistPenalty::updateAll(['price' => $price['price']], ['type_id' => $type]);
                }
            }
        }

        $penaltyPricelist = PricelistPenalty::find()->asArray()->all();

        return [
            'success'          => TRUE,
            'penaltyPricelist' => $penaltyPricelist,
        ];
    }

    public function actionGetTotalFinanceReport()
    {
        $postArray = Yii::$app->request->post();
        $user      = Yii::$app->user->identity;
        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if ($user->user_type != User::USER_SUPERADMIN && $user->user_type != User::USER_SITEADMIN) {
            LogTable::addLog($user->id, $user->user_type, "User with id {$user->id} tries to get protected information. Method getTotalFinanceInfo");

            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('finance_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        $searchParams = isset($postArray['searchParams']) ? $postArray['searchParams'] : null;

        $paymentsService = new PaymentsService($user->id);

        $agencyArray = $paymentsService->getTotalFinanceInfo($searchParams);

        if(isset($postArray['agencyOnly']) && $postArray['agencyOnly']){
            $totalFinanceAllPeriod  = null;
            $totalFinanceFromPeriod = null;
        }else{
            $totalFinanceAllPeriod  = $paymentsService->getTotalFinanceByPeriod();
            $totalFinanceFromPeriod = $paymentsService->getTotalFinanceByPeriod($searchParams);
        }



        return [
            'success'     => TRUE,
            'agencyArray' => $agencyArray,
            'totalFinanceAllPeriod' => $totalFinanceAllPeriod, 
            'totalFinanceFromPeriod' => $totalFinanceFromPeriod,
            'searchParams' => $searchParams
        ];
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        // check if the user can access $action and $model
        // throw ForbiddenHttpException if access should be denied
        $user = \Yii::$app->user->identity;
        if (in_array($user->user_type, [User::USER_MALE, User::USER_FEMALE, User::USER_INTERPRETER]))
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));

    }


    public function actionGetTotalFinanceReport2()
    {
        $postArray = Yii::$app->request->post();
        $user      = Yii::$app->user->identity;
        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (empty($postArray)) {
            LogTable::addLog($user->id, $user->user_type, 'Error during get total finance info. Check post array');

            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }        
        
        if ($user->user_type != User::USER_SUPERADMIN) {
            LogTable::addLog($user->id, $user->user_type, "User with id {$user->id} tries to get protected information. Method getTotalFinanceInfo");

            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }

        $searchParams = isset($postArray['search_params']) ? $postArray['search_params'] : null;

        //$user = new User($p['user_id']);

        $agencyArray = []; //$user->payments->getTotalFinanceInfo($search_params);

        return [
            'success'     => TRUE,
            'agencyArray' => $agencyArray,
        ];
    }

    public function actionGetBlogPost()
    {

        $postArray = Yii::$app->request->post();

        if (!isset($postArray['token']) || trim($postArray['token']) == "") {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $user = Yii::$app->user->identity;

        if (empty($user) || ($user->user_type != User::USER_SUPERADMIN && $user->user_type != User::USER_SITEADMIN)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('blog_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        $PostID = isset($postArray['PostID']) ? (int)$postArray['PostID'] : FALSE;

        if ($PostID) {
            $blog_post_list = Blog::getItem($PostID);
        } else {
            $blog_post_list = Blog::getList();
        }

        return [
            'success'   => TRUE,
            'code'      => 1,
            'post_list' => $blog_post_list,
        ];

    }


    public function actionAddPost()
    {
        $postArray = Yii::$app->request->post();
        if (!isset($postArray['token']) || trim($postArray['token']) == "") {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $user = Yii::$app->user->identity;

        if (empty($user) || ($user->user_type != User::USER_SUPERADMIN && $user->user_type != User::USER_SITEADMIN)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('blog_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        $post_title    = isset($postArray['post_title']) && !empty(trim($postArray['post_title'])) ? trim($postArray['post_title']) : FALSE;
        $post_desc     = isset($postArray['post_desc']) && !empty(trim($postArray['post_desc'])) ? trim($postArray['post_desc']) : FALSE;
        $post_category = isset($postArray['post_category']) && $postArray['post_category'] == 'post' ? 'post' : 'story';
        $post_image    = isset($postArray['post_image']) && !empty(trim($postArray['post_image'])) ? trim($postArray['post_image']) : FALSE;
        $status        = isset($postArray['status']) ? (int)$postArray['status'] : FALSE;
        $on_main       = isset($postArray['on_main']) ? (int)$postArray['on_main'] : FALSE;
        $sort_order    = isset($postArray['sort_order']) ? (int)$postArray['sort_order'] : FALSE;
        $post_id       = isset($postArray['post_id']) ? (int)$postArray['post_id'] : FALSE;

        if (!$post_title || !$post_desc || !$post_image) {
            return [
                'success' => FALSE,
                'code'    => 2,
                'message' => 'Missing params',
            ];
        }

        if ($post_id) {
            $result   = Blog::editPost($post_id, $post_title, $post_desc, $post_image, $post_category, $status, $on_main, $sort_order);
            $log_text = ' edit';
        } else {
            $log_text = ' add';
            $result   = Blog::addPost($post_title, $post_desc, $post_image, $post_category, $status, $on_main, $sort_order);
        }


        if ($result['success']) {
            LogTable::addLog($user->id, $user->user_type, $user->id . $log_text . ' post ID: ' . $result['post_id']);
        }

        return $result;
    }

    public function actionUploadPostImage()
    {
        $postArray = Yii::$app->request->post();
        if (!isset($postArray['token']) || trim($postArray['token']) == "") {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $user = Yii::$app->user->identity;

        if (empty($user) || ($user->user_type != User::USER_SUPERADMIN && $user->user_type != User::USER_SITEADMIN)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('blog_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        $photoService = new PhotoService($user->id);
        $result       = $photoService->uploadPostImg();

        return $result;
    }

    public function actionBlogPostDelete()
    {
        $postArray = Yii::$app->request->post();
        if (!isset($postArray['token']) || trim($postArray['token']) == "") {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $user = Yii::$app->user->identity;

        $post_id = isset($postArray['PostID']) ? (int)$postArray['PostID'] : FALSE;

        if (empty($user) || ($user->user_type != User::USER_SUPERADMIN && $user->user_type != User::USER_SITEADMIN) || !$post_id) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }
        if (in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('blog_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }        
        $result = Blog::deletePost($post_id);

        if ($result['success']) {
            LogTable::addLog($user->id, $user->user_type, $user->id . ' delete post ID: ' . $post_id);
        }

        return $result;
    }

    public function actionSetOnlineStatus()
    {
        $postArray = Yii::$app->request->post();

        $user = Yii::$app->user->identity;

        $userIDs = isset($postArray['userIDs']) ? $postArray['userIDs'] : FALSE;
        $status  = isset($postArray['status']) ? (int)$postArray['status'] : FALSE;
        $timeTo = (isset($postArray['timeTo']) && is_numeric($postArray['timeTo'])) ? (int)$postArray['timeTo'] : 60 * 60 * 24 * 7;

        if (empty($user) || !in_array($user->user_type, [User::USER_SUPERADMIN, User::USER_SITEADMIN, User::USER_INTERPRETER, User::USER_AGENCY, User::USER_ADMIN])
            || !$userIDs || !is_array($userIDs) || $status === FALSE
        ) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }
        if ($user->user_type == User::USER_ADMIN || $user->user_type == User::USER_INTERPRETER) {
            $userAgency = User::find()
                            ->where([
                                'user_type' => User::USER_AGENCY, 
                                'agency_id' => $user->agency_id,
                                'status' => User::STATUS_ACTIVE
                            ])
                            ->one();
            if (!empty($userAgency)) {
                $result = (new StatusService($userAgency->id))->setOnlineStatus($userIDs, $status, $userAgency->user_type, $userAgency->agency_id, $timeTo);
            } else {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'Missing params'
                ];
            }          
        } else {
            $result = (new StatusService($user->id))->setOnlineStatus($userIDs, $status, $user->user_type, $user->agency_id, $timeTo);
        }        

        if ($result['success']) {
            LogTable::addLog($user->id, $user->user_type, $user->id . ' set online status IDs: ' . implode(',', $userIDs));
        }

        return $result;
    }


    public function actionGetAffiliatesStats()
    {

        $postArray = Yii::$app->request->post();

        $user = Yii::$app->user->identity;

        $date_from = isset($postArray['date_from']) && strtotime($postArray['date_from']) ? $postArray['date_from'] : FALSE;
        $date_to = isset($postArray['date_to']) && strtotime($postArray['date_to']) ? $postArray['date_to'] : FALSE;
        $other_user_id = isset($postArray['other_user_id']) && is_numeric($postArray['other_user_id']) ? $postArray['other_user_id'] : FALSE;
        $limit = isset($postArray['limit']) ? $postArray['limit'] : null;
        $offset = isset($postArray['offset']) ? $postArray['offset'] : null;

        if (empty($user) || !in_array($user->user_type, [User::USER_SUPERADMIN, User::USER_AFFILIATE])
            || !$date_from || !$date_to) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $referral_stats = (new ReferralStats())->getStats($other_user_id, $date_from, $date_to, $limit, $offset);

        return $referral_stats;
    }

    public function actionGetAffiliatesFinance()
    {
        $postArray = Yii::$app->request->post();

        $user = Yii::$app->user->identity;

        $date_from = isset($postArray['date_from']) && strtotime($postArray['date_from']) ? $postArray['date_from'] : FALSE;
        $date_to = isset($postArray['date_to']) && strtotime($postArray['date_to']) ? $postArray['date_to'] : FALSE;
        $other_user_id = isset($postArray['other_user_id']) && is_numeric($postArray['other_user_id']) ? $postArray['other_user_id'] : FALSE;

        if (empty($user) || !in_array($user->user_type, [User::USER_SUPERADMIN, User::USER_AFFILIATE])
            || !$date_from || !$date_to) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        if(empty($other_user_id)){
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Please select user',
            ];
        }

        $other_user_model = User::findOne(['id'=>$other_user_id]);
        if(empty($other_user_model)){
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $my_children_ids = $other_user_model->getAffiliatesChildrenIds();
        if(empty($my_children_ids)){
            return ['success' => FALSE, 'code' => 2, 'message' => 'You don\'t have users'];
        }

        $search_params = [
            'date_from' =>$date_from,
            'date_to' =>$date_to,
            'my_children_ids' =>$my_children_ids,
        ];

        $paymentsService  = new PaymentsService($other_user_id);
        $ress = $paymentsService->getAffiliatesFinanceByPeriod($search_params);

        return ['success' => TRUE, 'code' => 1, 'mass' => $ress];

        //return json_encode($ress);
    }
}