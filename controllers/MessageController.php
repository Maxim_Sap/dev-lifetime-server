<?php
namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\Response;
use yii\helpers\HtmlPurifier;

/* my models */
use app\models\LogTable;
use app\models\User;
use app\models\UserProfile;
use app\models\Message;
use app\models\ApproveStatus;


/* my services */
use app\services\MessageService;


class MessageController extends ActiveController
{
    use CommonActionsTrait;
    public $modelClass = '';

    public function behaviors()
    {
        $authArray = ['get-messages', 'send-message', 'read-message', 'delete-message', 'get-messages-count'];
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => $authArray
        ];

        $access = $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['login'],
            'rules' => [
                [
                    'actions' => ['send-message-to-admin'],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => $authArray,
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $verbs = $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ];
        
        unset($behaviors['access']);
        unset($behaviors['authenticator']);
        unset($behaviors['verbs']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Allow-Origin'      => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],
        ];
        
        $behaviors['rateLimiter'] = [
            // Use class
            'class' => \highweb\ratelimiter\RateLimiter::className(),
            
            // The maximum number of allowed requests
            'rateLimit' => 100,
            
            // The time period for the rates to apply to
            'timePeriod' => 60,
            
            // Separate rate limiting for guests and authenticated users
            // Defaults to true
            // - false: use one set of rates, whether you are authenticated or not
            // - true: use separate ratesfor guests and authenticated users
            'separateRates' => false,
            
            // Whether to return HTTP headers containing the current rate limiting information
            'enableRateLimitHeaders' => false,
        ];
        
        $behaviors['authenticator'] = $auth;
        $behaviors['access'] = $access;
        $behaviors['verbs'] = $access;
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }
   
    public function actionGetMessages()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        $ownerID = $user->id;
        if (in_array($user->user_type, [User::USER_INTERPRETER, User::USER_ADMIN])) {
            $agency = $user->getAgency()
                            ->where(['!=', 'id', 1])
                            ->andWhere(['approve_status' => ApproveStatus::STATUS_APPROVED])
                            ->one();
            if ($agency) {
                $user = User::find()
                                ->select(['id'])
                                ->where(['agency_id' => $agency->id, 'user_type' => User::USER_AGENCY])
                                ->one();
            }            
            if (empty($user)) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'Agency not found'
                ];
            }
        }
        $messageID = Yii::$app->request->post('messageID');
        $limit = Yii::$app->request->post('limit');
        $offset = Yii::$app->request->post('offset');

        $messageID  = isset($messageID) ? $messageID : null;
        $limit      = isset($limit) ? $limit : null;
        $offset     = isset($offset) ? $offset : null;

        $messageService = new MessageService($user->id);
        $messageArray = $messageService->getMessage($messageID, $limit, $offset);

        //для детального сообщения получаем предыдущее и следующее сообщение
        if (!empty($messageID) && $messageArray['success']) {            
            $nextPrevMessageIDs                 = $messageService->getPrevNextMessageIds($messageID);
            $messageArray['next_prev_mess_ids'] = $nextPrevMessageIDs;
        }

        return $messageArray;

    }

    public function actionGetMessagesCount()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        $ownerID = $user->id;
        if (in_array($user->user_type, [User::USER_INTERPRETER, User::USER_ADMIN])) {
            $agency = $user->getAgency()
                            ->where(['!=', 'id', 1])
                            ->andWhere(['approve_status' => ApproveStatus::STATUS_APPROVED])
                            ->one();
            if ($agency) {
                $user = User::find()
                                ->select(['id'])
                                ->where(['agency_id' => $agency->id, 'user_type' => User::USER_AGENCY])
                                ->one();
            }            
            if (empty($user)) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'Agency not found'
                ];
            }
        }

        $messageService = new MessageService($user->id);
        $count = $messageService->getMessagesCount();

        return [
            'success' => true,
            'count' => (int)$count['count']
        ];
    }

    public function actionSendMessage()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        $fromUserID = $user->id;
        $desc = Yii::$app->request->post('desc');
        $title = Yii::$app->request->post('title');
        $previousLetterID = Yii::$app->request->post('previousLetterID');
        
        $desc                = isset($desc) ? HtmlPurifier::process(trim($desc)) : null;
        $title             = isset($title) && trim($title) != "" ? HtmlPurifier::process(trim($title)) : null;
        $previousLetterID = isset($previousLetterID) && is_numeric($previousLetterID) ? (int)$previousLetterID : null;

        if (empty($desc) || empty($previousLetterID) || empty($fromUserID) || empty($title)) {
            return [
                'success' => false, 
                'message' => 'Missing params6755'
            ];
        }

        $previousMessage = Message::findOne(['id' => $previousLetterID]);
        if (!empty($previousMessage) && $previousMessage->message_receiver == $user->id) {
            $toUserID = $previousMessage->message_creator;
        } else {
            return [
                'success' => false, 
                'message' => 'Missing params2'
            ];            
        }        
        
        $otherUser = User::findOne(['id' => $toUserID]);

        if (empty($user) || empty($otherUser) || 
           ($user->user_type != User::USER_SUPERADMIN && $otherUser->user_type != User::USER_SUPERADMIN) || 
           ($user->user_type == User::USER_SUPERADMIN && $otherUser->user_type == User::USER_SUPERADMIN)) {
            return [
                'success' => false, 
                'message' => 'Missing params3'
            ];
        }

        $messageService = new MessageService($user->id);
        return $messageService->sendMessage($toUserID, $title, $desc, $previousLetterID);
    }

    public function actionSendMessageToAdmin()
    {

        $siteAdmin = User::find()->where(['user_type' => User::USER_SUPERADMIN])->one();

        $fromUserID = Yii::$app->request->post('fromUserID');
        $toUserID = Yii::$app->request->post('toUserID');
        $desc = Yii::$app->request->post('desc');
        $caption = Yii::$app->request->post('caption');
        $previousLetterID = Yii::$app->request->post('previousLetterID');

        $fromUserID        = isset($fromUserID) && is_numeric($fromUserID) ? (int)$fromUserID : null;
        $toUserID          = isset($toUserID) && is_numeric($toUserID) ? (int)$toUserID : $siteAdmin->id;
        $desc                = isset($desc) ? HtmlPurifier::process(trim($desc)) : null;
        $caption             = isset($caption) && trim($caption) != "" ? HtmlPurifier::process(trim($caption)) : null;
        $previousLetterID = isset($previousLetterID) && is_numeric($previousLetterID) ? (int)$previousLetterID : null;

        if (empty($desc) || (empty($toUserID) && empty($fromUserID)) || (empty($caption))) {
            return [
                'success' => false, 
                'message' => 'missing params'
            ];
        }

        $user      = User::findOne(['id' => $fromUserID]);    
        $otherUser = User::findOne(['id' => $toUserID]);

        if (empty($otherUser) || (isset($user) && $user->user_type != User::USER_SUPERADMIN && $otherUser->user_type != User::USER_SUPERADMIN) || (isset($user) && $user_type == User::USER_SUPERADMIN && $other_user_type == User::USER_SUPERADMIN)) {
            return [
                'success' => false, 
                'message' => 'missing params'
            ];
        }

        $messageService = new MessageService($otherUser->id);

        return $messageService->sendMessage($toUserID, $caption, $desc, $previousLetterID);
    }    

    public function actionDeleteMessage()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        $messageID = Yii::$app->request->post('messageID');
        if (empty($messageID)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        $messageService = new MessageService($user->id);

        return $messageService->deleteMessage($messageID);
    }

    public function actionReadMessage()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        $messageID = Yii::$app->request->post('messageID');

        if (empty($messageID)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }
        if ($user->user_type == User::USER_ADMIN) {
            $userAgency = User::find()
                            ->where([
                                'user_type' => User::USER_AGENCY, 
                                'agency_id' => $user->agency_id,
                                'status' => User::STATUS_ACTIVE
                            ])
                            ->one();
            if (!empty($userAgency)) {
                $messageService = new MessageService($userAgency->id);
            } else {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'Missing params'
                ];
            }           
        } else {
            $messageService = new MessageService($user->id);
        }
        

        return $messageService->readMessage($messageID);
    }


}