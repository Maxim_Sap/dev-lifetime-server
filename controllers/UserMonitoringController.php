<?php
namespace app\controllers;

use app\models\Blog;
use Yii;
use Stripe\Stripe;
use Stripe\Charge;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/* my models */

use app\models\User;
use app\models\UserProfile;
use app\models\ApproveStatus;


use app\services\LetterService;
use app\services\MessageService;
use app\services\ChatService;



class UserMonitoringController extends ActiveController
{
    public $modelClass = '';

    public function behaviors()
    {
        $authArray = ['get-new-message-count', 'set-last-activity', 'users-online-count'];
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => $authArray
        ];

        $access = $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['login'],
            'rules' => [
                [
                    'actions' => ['login'],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => $authArray,
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $verbs = $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ];
        
        unset($behaviors['access']);
        unset($behaviors['authenticator']);
        unset($behaviors['verbs']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Allow-Origin'      => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],
        ];

/*        $behaviors['rateLimiter'] = [
            // Use class
            'class' => \highweb\ratelimiter\RateLimiter::className(),
            
            // The maximum number of allowed requests
            'rateLimit' => 2000000,
            
            // The time period for the rates to apply to
            'timePeriod' => 60,
            
            // Separate rate limiting for guests and authenticated users
            // Defaults to true
            // - false: use one set of rates, whether you are authenticated or not
            // - true: use separate ratesfor guests and authenticated users
            'separateRates' => false,
            
            // Whether to return HTTP headers containing the current rate limiting information
            'enableRateLimitHeaders' => false,
        ];*/
        
        $behaviors['authenticator'] = $auth;
        $behaviors['access'] = $access;
        $behaviors['verbs'] = $access;
        $behaviors['authenticator']['except'] = ['options', 'get-user-online-count', 'get-user-online-by-ids'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    public function actionGetNewMessageCount()
    {
        // letters and chat message
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_MALE, User::USER_FEMALE])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }      

        $letterService = new LetterService($user->id);
        $messageService = new MessageService($user->id);
        $chatService = new ChatService($user->id);
        $newLetters    = $letterService->getNewLetters();
        $newMessage    = $messageService->getNewMessage();
        $newChatMessage = $chatService->getNewMessageCount();

        $chatsUserName = [];
        $i = 0;
        foreach ($newChatMessage['users'] as $chatMessage) {
            $chatsUserName[$i]['id'] = $chatMessage['action_creator'];
            $tz = new \DateTimeZone('Europe/Brussels');               
            if (!empty($chatMessage['birthday'])) {
                $chatsUserName[$i]['age'] = \DateTime::createFromFormat('Y-m-d', $chatMessage['birthday'], $tz)
                ->diff(new \DateTime('now', $tz))
                ->y;
            } else {
                $chatsUserName[$i]['age'] = null;
            }                 
            $chatsUserName[$i]['first_name'] = $chatMessage['first_name'];
            $chatsUserName[$i]['message'] = $chatMessage['message'];
            $chatsUserName[$i]['avatar'] = $chatMessage['small_thumb'];
            $chatsUserName[$i]['count'] = $chatMessage['count'];
            $chatsUserName[$i]['online'] = ($chatMessage['last_activity'] >= time() - 30) ? 1 : 0;
            $i++;
        }

        return [
            'success' => true, 
            'lettersCount' => $newLetters['count'], 
            'lettersUserName' => $newLetters['users'], 
            'chatsCount' => $newChatMessage['count'], 
            'chatsUserName' => $chatsUserName, 
            'messageCount' => $newMessage['count'], 
            'messageUserName' => $newMessage['users']
        ];

    }

    public function actionGetUserOnlineByIds()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $postArray = Yii::$app->request->post();
        
        if (empty($postArray) || !isset($postArray['IDs']) || !is_array($postArray['IDs'])) {
            return [
                'success' => false,
                'code' => 1, 
                'message' => 'Missing params',
            ];
        }

        $onlineUsersArray = User::find()
            ->select(['id', 'cam_online'])
            ->where(['in', 'id', $postArray['IDs']])
            ->andWhere(['>=', 'user.last_activity', time() - 30])
            ->asArray()
            ->all();       

        return [
            'success' => true, 
            'site_online_users' => $onlineUsersArray
        ];
    }

    public function actionGetUserOnlineCount()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;               

        $onlineUsers = User::find()
            ->select(['id', 'cam_online'])            
            ->where(['>=', 'user.last_activity', time() - 30])
            ->andWhere(['user.visible' => User::STATUS_VISIBLE, 'user.status' => User::STATUS_ACTIVE, 'user.approve_status_id' => ApproveStatus::STATUS_APPROVED]);
        if (!empty(Yii::$app->user->identity->user_type) && Yii::$app->user->identity->user_type == User::USER_FEMALE) {
            $onlineUsers->andWhere(['user.user_type' => User::USER_MALE]);
        } else {
            $onlineUsers->andWhere(['user.user_type' => User::USER_FEMALE]);
        }
        $count = $onlineUsers            
            ->count();       

        return [
            'success' => true, 
            'count' => $count
        ];
    }   

    public function actionUsersOnlineCount()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;                       
        $onlineUsers = User::find()
            ->select(['id', 'cam_online'])            
            ->where(['>=', 'user.last_activity', time() - 30])
            ->andWhere(['user.visible' => User::STATUS_VISIBLE, 'user.status' => User::STATUS_ACTIVE, 'user.approve_status_id' => ApproveStatus::STATUS_APPROVED]);
        if (!empty(Yii::$app->user->identity->user_type) && Yii::$app->user->identity->user_type == User::USER_FEMALE) {
            $onlineUsers->andWhere(['user.user_type' => User::USER_MALE]);
        } else {
            $onlineUsers->andWhere(['user.user_type' => User::USER_FEMALE]);
        }
        $count = $onlineUsers            
            ->count();       

        return [
            'success' => true, 
            'count' => $count
        ];
    }     

    public function actionSetLastActivity()
    {
        
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }            
        
        $result = $user->setLastActivity();

        return $result;

    }

}