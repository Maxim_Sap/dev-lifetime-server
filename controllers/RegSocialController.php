<?php
namespace app\controllers;

use app\models\LogTable;
use app\models\ReferralStats;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\NotAcceptableHttpException;
use app\models\UserAuthFacebook;
use app\models\UserAuthGoogle;
use app\models\UserType;
use app\models\UserProfile;
use app\models\AgencyPersonal;
use app\models\Affiliates;
use app\models\User;
use app\models\Affs;
use app\models\Agency;
use app\models\Womans;
use yii\web\Controller;
use app\services\AlbumService;

class RegSocialController extends Controller
{

    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['X-Wsse'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],

            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        if ($action->id == 'facebook') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionFacebook()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $postArray = Yii::$app->request->post();

        if (empty($postArray) || !isset($postArray['user_token']) || $postArray['user_token'] == "" 
            || !isset($postArray['facebook_id']) || $postArray['facebook_id'] == "" 
            || !isset($postArray['soc_reg_user_type']) || !is_numeric($postArray['soc_reg_user_type'])) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'Missing params'
            ];
        }

        $userType = $postArray['soc_reg_user_type'];
        $referralLink = isset($postArray['referralLink']) && !empty($postArray['referralLink']) ? $postArray['referralLink'] : null;
        $userIp = isset($postArray['userIp']) && !empty($postArray['userIp']) ? $postArray['userIp'] : null;

        //if ($userType == User::USER_INTERPRETER) {
        if ($userType != User::USER_MALE) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access denied'
            ];
        }

        $result = UserAuthFacebook::findOne(['facebook_id' => $postArray['facebook_id']]);
        if (!empty($result)) {
            //autorisation
            
            $user = User::findOne(['id' => $result->user_id]);
            if (empty($user)) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'user not found'
                ];
            }
            
            $user->token_end_date = time() + Yii::$app->params['remember_token_time'];
            $user->last_activity  = time();
			$user->scenario = User::SCENARIO_REGISTER_BY_SOCIAL;
            $user->access_token = $user->getJWT();
            if (!$user->save()) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'user error save',
                    'errors' => $user->errors
                ];
            }

            LogTable::addLog($user->id,$userType,' login by facebook');

            return [
                'success' => true, 
                'code' => 2, 
                'userInfo'  => [ 
                    'userID' => $user->id, 
                    'token' => $user->access_token
                ]
            ];
            
        } else {
            //sign up
            $userTypes = UserType::findOne(['id' => $userType]);
            if (empty($userTypes)) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'user_types not found'
                ];
            }

            if (isset($postArray['user_email'])) {
                $userEmail = ($postArray['user_email'] != "undefined") ? $postArray['user_email'] : null;
            } else {
                $userEmail = null;
            }

            $referral_id = null;
            if(isset($referralLink) && !empty($referralLink)){
                $AffiliatesModel = Affiliates::findOne(['referral_link'=>$referralLink]);
                if(!empty($AffiliatesModel)){
                    ReferralStats::addVisitor(trim($referralLink),$userIp,$type = 2);
                    $referral_id = $AffiliatesModel->id;
                }
            }

            $password = Yii::$app->security->generateRandomString(40);
            $user              = new User();
            //$user->scenario = User::SCENARIO_REGISTER_BY_SOCIAL;
            $user->email       = $userEmail;
            $user->skin_id     = 1;
            $user->user_type   = $userType;
            $user->agency_id = 1;
            $user->referral_id = $referral_id;
            $user->avatar_photo_id = 0;  
            $user->status = User::STATUS_ACTIVE;          
            $user->token_end_date = time() + Yii::$app->params['remember_token_time'];
            $user->last_activity  = time();
            $user->cam_online = 0;
            $user->setPassword($password);
            $user->generateAuthKey();

            if ($userType == User::USER_MALE) {
                $user->approve_status_id = 4; // default male status = 4
            } else {
                $user->approve_status_id = 1; 
            }
                        
            if (!$user->save()) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => join(', ', $user->getFirstErrors()),
                ];
            }
            $user->access_token = $user->getJWT();

            if (!$user->save()) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => join(', ', $user->getFirstErrors())
                ];
            }

            //add personal info
            $userProfile = new UserProfile();  
            $userProfile->id = $user->id;          
            $nameArray = explode(' ', $postArray['user_name']);
            $userProfile->first_name = $nameArray[0];
            if (isset($nameArray[1]) && $nameArray[1] != ""){
                $userProfile->last_name = $nameArray[1];
            }
            
            if (!$userProfile->save()) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'User profile error save'
                ];
            }
            // create a public album for user
            $albumService = new AlbumService($user->id);
            $albumService->Create(1, 'Public Album', 'Public Album');

            $userFbModel           = new UserAuthFacebook();
            $userFbModel->user_id  = $user->id;
            $userFbModel->facebook_id = $postArray['facebook_id'];
            
            if (!$userFbModel->save()) {
                return [
                	'success' => false, 
                	'code' => 1, 
                	'message' => 'FB error save'
                	];
            }

            LogTable::addLog($user->id,$userType,'Registered by Facebook');

            return [
                'success' => true, 
                'code' => 1, 
                'userInfo'  => [ 
                    'userID' => $user->id, 
                    'token' => $user->access_token
                ]
            ];
        }
    }

    public function actionGoogle()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $postArray = Yii::$app->request->post();

        if (empty($postArray) || !isset($postArray['google_conf'])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Google Auth error'
            ];
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $postArray['google_conf']['url_token']);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($postArray['google_conf'])));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);

        $tokenInfo = json_decode($result, true);

        if (!isset($tokenInfo['access_token'])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Google token error'
            ];
        }

        $params = array(
            'client_id'     => $postArray['google_conf']['client_id'],
            'client_secret' => $postArray['google_conf']['client_secret'],
            'redirect_uri'  => $postArray['google_conf']['redirect_uri'],
            'grant_type'    => 'authorization_code',
            'code'          => $postArray['google_conf']['code'],
            'access_token'  => $tokenInfo['access_token'],
        );
        
        $userInfo = json_decode(file_get_contents($postArray['google_conf']['get_user_info_url'] . '?' . urldecode(http_build_query($params))), true);

        if (empty($userInfo)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User info empty'
            ];
        }

        if (!isset($userInfo['id'])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Google user_info error'
            ];
        }

        if (!isset($postArray['user_type']) || !is_numeric($postArray['user_type'])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'user_type'
            ];
        }

        $userType = $postArray['user_type'];

        $userTypeModel = UserType::findOne(['id' => $userType]);
        if (empty($userTypeModel)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'user type not found'
            ];
        }
        
        if ($userType != User::USER_MALE) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access denied'
            ];
        }

        $result = UserAuthGoogle::findOne(['google_id' => $userInfo['id']]);

        if (!empty($result)) {
            
            $user = User::findOne(['id' => $result->user_id]);
            if (empty($user)) {
                return [
                    'success' => false,
                    'code' => 1, 
                    'message' => 'user not found'
                ];
            }
            
            $user->token_end_date = time() + Yii::$app->params['remember_token_time'];
            $user->last_activity  = time();
            $user->scenario = User::SCENARIO_REGISTER_BY_SOCIAL;
            $user->access_token = $user->getJWT();

            if (!$user->save()) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'user error save'
                ];
            }

            $userInfo = [
                'userID' => $user->id,
                'token' => $user->access_token
            ];

            LogTable::addLog($user->id,User::USER_MALE,'login by google');

            return [
                'success' => true, 
                'code' => 2, 
                'userInfo' => $userInfo
            ];

        } else {
            $referralLink = isset($postArray['referralLink']) && !empty($postArray['referralLink']) ? $postArray['referralLink'] : null;
            $userIp = isset($postArray['userIp']) && !empty($postArray['userIp']) ? $postArray['userIp'] : null;
            $referral_id = null;
            if(isset($referralLink) && !empty($referralLink)){
                $AffiliatesModel = Affiliates::findOne(['referral_link'=>$referralLink]);
                if(!empty($AffiliatesModel)){
                    ReferralStats::addVisitor(trim($referralLink),$userIp,$type = 2);
                    $referral_id = $AffiliatesModel->id;
                }
            }

            $password = Yii::$app->security->generateRandomString(40);
            $user              = new User();
            //$user->scenario = User::SCENARIO_REGISTER_BY_SOCIAL;
            $user->email       = $userInfo['email'];
            $user->skin_id     = 1;
            $user->user_type   = $userType;
            $user->agency_id = 1;
            $user->referral_id = $referral_id;
            $user->avatar_photo_id = 0;
            $user->status = User::STATUS_ACTIVE;          
            $user->token_end_date = time() + Yii::$app->params['remember_token_time'];
            $user->last_activity  = time();
            $user->cam_online = 0;
            $user->setPassword($password);
            $user->generateAuthKey();

            if ($userType == User::USER_MALE) {
                $user->approve_status_id = 4; // default male status = 4
            } else {
                $user->approve_status_id = 1; 
            }
                        
            if (!$user->save()) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'User error save'
                ];
            }            
/*            

            if (!$user->save()) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'User error save'
                ];
            }*/
            
            //add personal info
            $userProfile = new UserProfile();
            $userProfile->id = $user->id;            
            $nameArray = explode(' ', $userInfo['name']);
            $userProfile->first_name = $nameArray[0];
            if (isset($nameArray[1]) && $nameArray[1] != ""){
                $userProfile->last_name = $nameArray[1];
            }
            
            if (!$userProfile->save()) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'user_info error save'
                ];
            }

            // create a public album for user
            $albumService = new AlbumService($user->id);
            $albumService->Create(1, 'Public Album', 'Public Album');

            $userAuthGoogle               = new UserAuthGoogle();
            $userAuthGoogle->user_id      = $user->id;
            $userAuthGoogle->google_id    = $userInfo['id'];

            if (!$userAuthGoogle->save()) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'UserAuthGoogle error save'
                ];
            }

            $userInfo = [
                'userID' => $user->id,
                'token' => $user->access_token
            ];

            LogTable::addLog($user->id,User::USER_MALE,"registered by google");

            return [
                'success' => true, 
                'code' => 2, 
                'userInfo' => $userInfo
            ];
            
        }

    }

    public function actionSetUserType()
    {
        $userType = Yii::$app->request->get('user_type');
        $session   = Yii::$app->session;
        $session->set('soc_reg_user_type', $userType);
    }

    public static function SwitchAndSaveUserType($user_type, $user_id)
    {
        switch ($user_type) {
            case 1:
                $man_model          = new Mans();
                $man_model->user_id = $user_id;
                if (!$man_model->save()) {
                    return json_encode(['success' => false, 'code' => 2, 'message' => 'Error save man data']);
                }
                //добавляем при старте определенную сумму
                $user = new User($user_id);
                $user->payments->AddFunds(1000000, 'cash');
                break;
            case 2:
                $womans_model            = new Womans();
                $womans_model->user_id   = $user_id;
                $womans_model->agency_id = 8;//test agency
                if (!$womans_model->save()) {
                    return json_encode(['success' => false, 'code' => 2, 'message' => 'Error save woman data']);
                }
                //создаем доп альбом
                $user = new User($user_id);
                $user->album->Create(true, 'document', $descr = 'document album');
                break;
            case 3:
                $agency_model          = new Agency();
                $agency_model->user_id = $user_id;
                if (!$agency_model->save()) {
                    return json_encode(['success' => false, 'code' => 2, 'message' => 'Error save agency data']);
                }
                $agency_pers_model          = new AgencyPersonal();
                $agency_pers_model->user_id = $user_id;
                if (!$agency_pers_model->save()) {
                    return json_encode(['success' => false, $agency_pers_model->errors]);
                }
                break;
            case 5:
                $affiliates_model          = new Affs();
                $affiliates_model->user_id = $user_id;
                if (!$affiliates_model->save()) {
                    return json_encode(['success' => false, 'code' => 2, 'message' => 'Error save affiliates data']);
                }
                break;
        }
    }

}
