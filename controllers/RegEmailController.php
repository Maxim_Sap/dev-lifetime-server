<?php
namespace app\controllers;


use app\models\Affiliates;
use app\models\ReferralStats;
use Yii;
use app\models\LogTable;
use yii\web\BadRequestHttpException;
use yii\base\InvalidParamException;
use app\models\User;
use yii\rest\ActiveController;
use app\models\forms\SignupForm;
use app\models\forms\SignupAdminForm;
use app\models\forms\ResetPasswordForm;
use app\models\forms\PasswordResetRequestForm;
use yii\filters\AccessControl;
use yii\helpers\Json;

class RegEmailController extends ActiveController
{
    public $modelClass = '';

    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['X-Wsse'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],
            ],
            'rateLimiter' => [
                // Use class
                'class' => \highweb\ratelimiter\RateLimiter::className(),
                'rateLimit' => 600,
                'timePeriod' => 60,
                'separateRates' => FALSE,
                'enableRateLimitHeaders' => FALSE,
            ]
        ];
    }

    public function actionChangePasswordRequest()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new PasswordResetRequestForm();        
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->validate()) {
            if ($model->sendEmail()) {
                return [
                    'success' => true,
                    'message' => 'Check your email for further instructions.'
                ];                
            } else {
                return [
                    'success' => false,
                    'message' => 'Sorry, we are unable to reset password for the provided email address.'
                ];                
            }
        } else {
            return [
                'success' => false,
                'code' => "Validation errors",
                'errors' => $model->errors
            ]; 
        }        
    }

    public function actionResetPasswordCheck()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $token = Yii::$app->request->post('token');
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];            
        }
    
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->resetPassword()) {
                return [
                    'success' => true
                ]; 
            } else {
                 return [
                    'success' => false,
                    'message' => 'Error occurred while processing your request'
                ];
            }            
        } else {
            $model->validate();
            return [
                'success' => false,
                'code' => "Validation errors",
                'errors' => $model->errors
            ]; 
        }
    }

    public function actionCheckEmail()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $email = Yii::$app->request->post('email');
        if (!isset($email)) {
            return [
                'success' => false, 
                'message' => 'missing params email'
            ];
        }
        $email = trim($email);

        $user = User::findOne(['email' => $email]);
        if (empty($user)) {
            return [
                'success' => true, 
                'message' => 'email ok'
            ];
        } else {
            return [
                'success' => false, 
                'message' => 'email busy'
            ];
        }

    }

    public function actionRegister()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = new SignupForm();
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '')) {
            $result = $model->signup();
            if ($result['success']) {
                if (!User::isPasswordResetTokenValid($result['user']->password_reset_token)) {
                    $result['user']->generatePasswordResetToken();
                    if (!$result['user']->save()) {
                        return [
                            'success' => false, 
                            'message' => 'Error occurred while registering your user account'
                        ];
                    }
                }
                $emailSendSuccessfully = Yii::$app
                    ->mailer
                    ->compose(
                        ['html' => 'registration-html', 'text' => 'registration-text'],
                        ['user' => $result['user']]
                    )
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
                    ->setTo($result['user']->email)
                    ->setSubject('Registration link for ' . Yii::$app->name)
                    ->send();
                if ($emailSendSuccessfully) {
                    return [
                        'success' => true, 
                        'message' => 'To complete your registration click on the link we sent to your email'
                    ];
                } else {    
                    return [
                        'success' => false, 
                        'message' => 'Error occurred while sending mail to your email'
                    ];
                }
            } else {
                $model->validate();
                return [
                    'success' => 'false',
                    'code' => 'Validation errors',
                    'errors' => $model->errors,
                ];
            }
        } else {
            //LogTable::addLog('Error during sign up');
            return [
                'success' => 'false',
                'code' => 1,
                'message' => 'An error occurred while processing your request. If you think this is a bug, please contact us.',
            ];
        }        

    }

    public function actionAdminRegister()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = new SignupAdminForm();
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '')) {
            $result = $model->signup();
            if ($result['success']) {
                    return [
                        'success' => true, 
                        'message' => 'Your registration information was successfully send to server. Wait until admin approve your account'
                    ];               
            } else {
                $model->validate();
                return [
                    'success' => 'false',
                    'code' => 'Validation errors',
                    'errors' => $model->errors
                ];
            }
        } else {
            //LogTable::addLog('Error during admin sign up');
            return [
                'success' => 'false',
                'code' => 1,
                'message' => 'An error occurred while processing your request. If you think this is a bug, please contact us.'
            ];
        }        
    }

    public function actionCheck()
    {
        $token = Yii::$app->request->post('token');
        $ip = Yii::$app->request->post('ip');
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        try {
            if (empty($token) || !is_string($token)) {
                throw new InvalidParamException('Password reset token cannot be blank.');
            }

            if (!$user = User::findByPasswordResetToken($token)) {
                throw new InvalidParamException('Wrong password reset token.');
            }  
        } catch (InvalidParamException $e) {
            return [
                'success' => false, 
                'message' => $e->getMessage()
            ];
        }
        $user->status = User::STATUS_ACTIVE;
        $user->access_token = $user->getJWT();
        $user->removePasswordResetToken();
        $user->last_activity = time();
        $user->token_end_date = time() + Yii::$app->params['token_time'];
        if ($user->user_type == User::USER_MALE) {
            $user->approve_status_id = 4;
        }

        if(!empty($user->referral_id)){
            $AffiliatesModel = Affiliates::findOne(['id'=>$user->referral_id]);
            if(!empty($AffiliatesModel)){
                $referral_link = $AffiliatesModel->referral_link;
                ReferralStats::addVisitor(trim($referral_link),$ip,$type = 2);
            }
        }

        
        if (!$user->save()) {
            return [
                'success' => false, 
                $user->errors
            ];
        } else {
            $result['userID'] = $user->id;
            $result['token']  = $user->access_token;

            return [
                'success' => true, 
                'message' => 'User successfully registered', 
                'userInfo' => $result
            ];
        }

    }

    public function actionCheckToken($params = null, $refresh = true)
    {
        $result = Yii::$app->runAction('user/check-token', [$params, $refresh]);

        return JSON::encode($result);
    }
}
