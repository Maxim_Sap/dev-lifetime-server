<?php
namespace app\controllers;

use app\models\Comments;
use Yii;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\Response;

/* my models */

use app\models\LogTable;
use app\models\Status;
use app\models\User;
use app\models\UserProfile;
use app\models\Gift;
use app\models\GiftSearch;
use app\models\Order;
use app\models\OrderStatus;
use app\models\AdminPermissions;


/* my services */
use app\services\ShopService;
use app\services\PhotoService;


class GiftController extends ActiveController
{
    use CommonActionsTrait;
    public $modelClass = '';

    public function behaviors()
    {
        $authArray = [
            'get-gifts', 'add-gift', 'delete-gift',
            'confirm-gift-payment', 'get-shop-items', 'set-shop-gift-status'
        ];
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only'  => $authArray,
        ];

        $access                         = $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only'  => ['login'],
            'rules' => [
                [
                    'actions' => [],
                    'allow'   => TRUE,
                    'roles'   => ['?'],
                ],
                [
                    'actions' => $authArray,
                    'allow'   => TRUE,
                    'roles'   => ['@'],
                ],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class'   => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $verbs                          = $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ];

        unset($behaviors['access']);
        unset($behaviors['authenticator']);
        unset($behaviors['verbs']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors'  => [
                // restrict access to
                'Origin'                           => Yii::$app->params['origin'],
                'Access-Control-Allow-Origin'      => Yii::$app->params['origin'],
                'Access-Control-Request-Method'    => ['POST', 'GET', 'OPTIONS'],
                // Allow only POST and PUT methods
                'Access-Control-Request-Headers'   => ['*'],
                // Allow only headers 'X-Wsse'
                'Access-Control-Allow-Credentials' => TRUE,
                // Allow OPTIONS caching
                'Access-Control-Max-Age'           => 3600,
                // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
            ],
        ];

        $behaviors['rateLimiter'] = [
            // Use class
            'class'                  => \highweb\ratelimiter\RateLimiter::className(),

            // The maximum number of allowed requests
            'rateLimit'              => 100,

            // The time period for the rates to apply to
            'timePeriod'             => 60,

            // Separate rate limiting for guests and authenticated users
            // Defaults to true
            // - false: use one set of rates, whether you are authenticated or not
            // - true: use separate ratesfor guests and authenticated users
            'separateRates'          => false,

            // Whether to return HTTP headers containing the current rate limiting information
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['authenticator']           = $auth;
        $behaviors['access']                  = $access;
        $behaviors['verbs']                   = $access;
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }



    public function actionGetGifts()
    {

        $this->checkAccess($this->id, null);       

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $otherUserID = Yii::$app->request->post('otherUserID');
        $otherUserID = (isset($otherUserID) && filter_var($otherUserID, FILTER_VALIDATE_INT)) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false,
                'code'    => 1,
                'message' => $result2['message'],
            ];
        }
        $postArray = Yii::$app->request->post();
        $params = [
            'status'       => isset($postArray['status']) && is_numeric($postArray['status']) ? (int)$postArray['status'] : null,
            'giftsIDs'     => isset($postArray['giftsIDs']) && is_array($postArray['giftsIDs']) && array_filter($postArray['giftsIDs'], 'is_numeric') ? array_map('intval', $postArray['giftsIDs']) : [],
            'limit'        => isset($postArray['limit']) && is_numeric($postArray['limit']) ? (int)$postArray['limit'] : null,
            'page'         => isset($postArray['page']) && is_numeric($postArray['page']) ? (int)$postArray['page'] : 0,
            'searchParams' => isset($postArray['searchParams']) && is_array($postArray['searchParams']) ? $postArray['searchParams'] : [],
        ];
       
        if (!empty($otherUserID) && $user->inBlackList($otherUserID)) {
            return [
                'success' => false,
                'code'    => 2,
                'message' => 'User add your id in black list',
            ];
        }

        $searchModel  = new GiftSearch();
        $dataProvider = $searchModel->search($params);

        return [
            'success'    => TRUE,
            'giftsArray' => $dataProvider->models,
            'giftsCount' => $dataProvider->getTotalCount(),
        ];
    }

    public function actionAddGift()
    {
        $this->checkAccess($this->id, null); 

        $skinID = Yii::$app->request->post('skinID');
        if (!isset($skinID) || !is_numeric($skinID)) {
            return [
                'success' => false,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }
        $giftID = Yii::$app->request->post('giftID');
        $giftID = (isset($giftID) && is_numeric($giftID)) ? (int)$giftID : null;
        $model  = Gift::findOne($giftID);
        if ($model !== null) {
            $gift = $model;
        } else {
            $gift = new Gift();
        }

        if (!$gift->load(Yii::$app->getRequest()->getBodyParams(), '') && !$gift->validate()) {
            return [
                'success' => false,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!empty($giftID)) {
            LogTable::addLog($user->id, $user->user_type, 'update gift ID: ' . $giftID . " title: " . $gift->name);
            $gift->status = 1;

            return [
                'success' => $gift->save(),
            ];
        }
        LogTable::addLog($user->id, $user->user_type, 'add gift, title: ' . $gift->name);
        $gift->status = 1;

        return [
            'success' => $gift->save(),
            'giftID'  => $gift->id,
        ];
    }

    public function actionDeleteGift()
    {
        $this->checkAccess($this->id, null); 

        $skinID = Yii::$app->request->post('skinID');
        $giftID = Yii::$app->request->post('giftID');
        if (!isset($skinID) || !is_numeric($skinID) || !isset($giftID) || !is_numeric($giftID)) {
            return [
                'success' => false,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        $gift = Gift::findOne(['id' => $giftID]);
        if ($gift) {
            $result = $gift->delete();
            if ($result) {
                LogTable::addLog($user->id, $user->user_type, 'dell gift ID: ' . $giftID);
            }
        }
        if (!empty($result)) {
            return [
                'success' => TRUE,
            ];
        } else {
            return [
                'success' => false,
            ];
        }

    }

    public function actionGetShopItems()
    {

        $this->checkAccess($this->id, null);

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }
        $skinID = Yii::$app->request->post('skinID');
        $dateFrom = Yii::$app->request->post('dateFrom');
        $dateTo = Yii::$app->request->post('dateTo');
        $dateParam = Yii::$app->request->post('date_param');
        if(!isset($dateParam)){
	        $dateFrom = isset($dateFrom) && strtotime($dateFrom) ? $dateFrom  : date('Y-m-d', time());
	        $dateTo = isset($dateTo) && strtotime($dateTo) ? $dateTo . ' 23:59:59' : date('Y-m-d', time()) . ' 23:59:59';
		}

        if (!isset($skinID) || !is_numeric($skinID)) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }
        $actionID = Yii::$app->request->post('actionID');
        $actionID = (isset($actionID) && is_numeric($actionID)) ? $actionID : null;

        if (!empty($actionID)) {
            $comments_read_model = (new Comments())->readCommentsByResourceId($user->user_type, $actionID);
        }

        $shopService  = new ShopService($user->id);
        $myShopItems = $shopService->getMyShopItems($actionID, $dateFrom, $dateTo);
        if (in_array($user->user_type, [User::USER_SUPERADMIN, User::USER_AGENCY, User::USER_ADMIN])) {
            return $myShopItems;
        } else {
            unset($myShopItems['orderStatusArray']);
            $i=0;
            $tempArray = [];
            foreach ($myShopItems['items'] as $item) {
                $tempArray[$i]['action_id'] = $item['action_id'];                               
                $tempArray[$i]['quantity'] = $item['quantity'];                               
                $tempArray[$i]['comment'] = $item['comment'];                               
                $tempArray[$i]['created_at'] = $item['created_at'];                               
                $tempArray[$i]['order_status_id'] = $item['order_status_id'];                               
                $tempArray[$i]['status_title'] = $item['status_title'];                               
                $tempArray[$i]['gift_name'] = $item['gift_name'];                               
                $tempArray[$i]['gift_img'] = $item['gift_img'];                               
                $tempArray[$i]['gift_price'] = $item['gift_price'];                               
                $tempArray[$i]['user_to_name'] = $item['user_to_name'];                               
                $tempArray[$i]['user_to_last_name'] = $item['user_to_last_name'];                               
                $tempArray[$i]['user_to_id'] = $item['user_to_id'];                 
                $tempArray[$i]['user_from_name'] = $item['user_from_name'];                               
                $tempArray[$i]['user_from_last_name'] = $item['user_from_last_name'];                               
                $tempArray[$i]['user_from_id'] = $item['user_from_id'];                                               
                $i++;
            }
            $myShopItems['items'] = $tempArray;
            
            return $myShopItems;
        }
        
    }

    public function actionSetShopGiftStatus()
    {
        $this->checkAccess($this->id, null);
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }
        $skinID = Yii::$app->request->post('skinID');
        $shopItemID = Yii::$app->request->post('shopItemID');
        $status = Yii::$app->request->post('status');

        if (!isset($skinID) || !is_numeric($skinID)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        if (!isset($shopItemID) || !is_numeric($shopItemID) || !isset($status) || !is_numeric($status)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        if($user->user_type != User::USER_SUPERADMIN && !in_array($status, [OrderStatus::STATUS_NEW, OrderStatus::STATUS_WAIT_CANCEL, OrderStatus::STATUS_WAIT_APPROVE])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access denied'
            ];
        }

        $comment = Yii::$app->request->post('comment');
        $comment = isset($comment) && trim($comment) != '' ? $comment : null;
        $shopService = new ShopService($user->id);
        $result = $shopService->setShopGiftStatus($shopItemID, $status, $comment);

        $discuss = Yii::$app->request->post('discuss');
        $discuss = isset($discuss) && trim($discuss) != '' ? $discuss : false;

        if ($result['success'] && $discuss) {
            $fields         = [
                'resource_type_id' => 2,
                'resource_id'      => $shopItemID,
                'user_id'          => $user->id,
                'comments'         => $discuss,
            ];
            $comments_model = new Comments();
            if (!$comments_model->load(['Comments' => $fields]) || !$comments_model->save()) {
                return [
                    'success' => FALSE,
                    'code' => 2,
                    'message' => 'error add comments'
                ];
            }
        }

        LogTable::addLog($user->id, $user->user_type, 'set gift ID: ' . $shopItemID . ' in shop status: '.$status);

        return $result;
    }

    public function actionConfirmGiftPayment()
    {        
        $this->checkAccess($this->id, null);     

        $skinID    = Yii::$app->request->post('skinID');
        $userTo    = Yii::$app->request->post('userTo');
        $giftArray = Yii::$app->request->post('giftArray');
        if (!isset($skinID) || !is_numeric($skinID) || !isset($userTo) || !is_numeric($userTo) || !isset($giftArray) || !is_array($giftArray)) {
            return [
                'success' => false,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $user = Yii::$app->user->identity;

        if (empty($user) || $user->user_type != User::USER_MALE) {
            return [
                'success' => false,
                'code'    => 1,
                'message' => 'Missing params',
                'error' => $user,
            ];
        }


        $other_user_model = User::findOne(['id'=>$userTo]);
        if(empty($other_user_model) || $other_user_model->user_type != User::USER_FEMALE){
            return [
                'success' => false,
                'code'    => 1,
                'message' => 'Missing params',
                'error' => $user,
            ];
        }

        $order_result = (new Order())->addOrder($skinID, $user->id, $userTo, $giftArray);

        return $order_result;

    }


    public function checkAccess($action, $model = null, $params = [])
    {
        // check if the user can access $action and $model
        // throw ForbiddenHttpException if access should be denied
        $user = \Yii::$app->user->identity;
        if ($action === 'add-gift' || $action === 'update' || $action === 'delete-gift' || $action === 'get-shop-items') {
            if (in_array($user->user_type, [User::USER_MALE, User::FEMALE, User::USER_INTERPRETER]))
                throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if ($action === 'set-shop-gift-status') {
            if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN, User::USER_SUPERADMIN])) {
                throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
            }
        }
        if (in_array($user->user_type, [User::USER_ADMIN, User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('gift_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
    }

}