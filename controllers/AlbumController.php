<?php
namespace app\controllers;

use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\Response;

/* my models */
use app\models\Albums;
use app\models\LogTable;
use app\models\Photo;
use app\models\User;
use app\models\UserProfile;

/* my services */
use app\services\AlbumService;
use app\services\PhotoService;


class AlbumController extends ActiveController
{
    use CommonActionsTrait;
    public $modelClass = '';

    public function behaviors()
    {
        $authArray = ['edit-album', 'set-album-cover', 'get-albums-list-info'];
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => $authArray
        ];

        $access = $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['login'],
            'rules' => [
                [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => $authArray,
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $verbs = $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ];
        
        unset($behaviors['access']);
        unset($behaviors['authenticator']);
        unset($behaviors['verbs']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Allow-Origin'      => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],
        ];

        $behaviors['rateLimiter'] = [
            // Use class
            'class' => \highweb\ratelimiter\RateLimiter::className(),
            
            // The maximum number of allowed requests
            'rateLimit' => 100,
            
            // The time period for the rates to apply to
            'timePeriod' => 60,
            
            // Separate rate limiting for guests and authenticated users
            // Defaults to true
            // - false: use one set of rates, whether you are authenticated or not
            // - true: use separate ratesfor guests and authenticated users
            'separateRates' => false,
            
            // Whether to return HTTP headers containing the current rate limiting information
            'enableRateLimitHeaders' => false,
        ];
        
        $behaviors['authenticator'] = $auth;
        $behaviors['access'] = $access;
        $behaviors['verbs'] = $access;
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }
   
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    public function beforeAction($action)
    {                   
        if (parent::beforeAction($action)) {
            $user = \Yii::$app->user->identity;
            if (!Yii::$app->user->isGuest && in_array($user->user_type, [User::USER_ADMIN, User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('user_access'))) {
                throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
            }
            return true;
        } else {
            return false;
        }
    }

    public function actionCreateAlbum()
    {
        $title = Yii::$app->request->post('title');

        if (!isset($title) || trim($title) == "") {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        $otherUserID = Yii::$app->request->post('otherUserID');
        
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'User not found'
            ];
        }

        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }

        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to create album for user {$otherUser->id}");
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'Access Denied'
                ];
            }
        }
        
        $public = Yii::$app->request->post('public');
        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN, User::USER_SUPERADMIN])) {
            if (isset($public) && filter_var($public, FILTER_VALIDATE_INT)) {
                $public = $public;
            } else {
                $public = null;
            }            
        } else {
            $public = 1;
        }
        $description = Yii::$app->request->post('description');
        $description  = (isset($description) && $description != '') ? trim($description) : 'standart album';

        $albumService = new AlbumService($otherUser->id);
        $result = $albumService->Create($public, trim($title), $description);
        if (!$result['success']) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => $ress['message']
            ];
        }

        LogTable::addLog($user->id, $user->user_type, 'create/update photo album: ' . $title . ' user ID: ' . $otherUser->id);

        return [
            'success' => true, 
            'message' => 'album_id: ' . $result['album_id']
        ];
    }

    public function actionGetAlbumsListInfo()
    {        

        $otherUserID = Yii::$app->request->post('otherUserID');
        
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'User not found'
            ];
        }

        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }

        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to get albums from user {$otherUser->id}");
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'Access Denied'
                ];
            }
        }

        $public = Yii::$app->request->post('public');
        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN, User::USER_SUPERADMIN])) {
            if (isset($public) && filter_var($public, FILTER_VALIDATE_INT)) {
                $public = $public;
            } else {
                $public = null;
            }            
        } else {
            $public = 1;
        }
        
        $albumService = new AlbumService($otherUser->id);
        $albums = $albumService->getAlbumsListInfo($public);

        return [
            'success' => true, 
            'albums' => $albums
        ];
    }

    public function actionUploadPhotoToAlbum()
    {
        $token = Yii::$app->request->post('token');
        $albumTitle = Yii::$app->request->post('albumTitle');

        if (empty($albumTitle) || !isset($token)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        $result = json_decode(JSON::encode($this->actionCheckToken(['token' => $token])));

        if (!$result->success) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        $user = User::findIdentityByAccessToken($token);

        $currentUserType = $user->user_type;

        $title = Yii::$app->request->post('albumTitle');                
        $photoTitle = Yii::$app->request->post('photo_title'); 
        $photoDesc  = Yii::$app->request->post('photo_desc');
        $otherUserID = Yii::$app->request->post('otherUserID');

        $photoTitle = (!empty($photoTitle)) ? $photoTitle : '';
        $photoDesc = (!empty($photoDesc)) ? $photoDesc : '';

        $userID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;
        $otherUser = User::findOne(['id' => $userID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => $result2['message']
            ];
        }
        
        if ($userID != $user->id && $user->user_type != User::USER_SUPERADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to upload foto to user {$otherUser->id}");
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'Access Denied'
                ];
            }
        }        

        // load photo in temp directory
        if ($user->user_type == User::USER_SUPERADMIN && $title == 'temp') {
            LogTable::addLog($user->id, $user->user_type, $user->id . 'upload photo to temp folder, owner user ID: '. $otherUser->id);
            $photoService = new PhotoService($otherUser->id);
            return $photoService->uploadGiftImg();
        }

        LogTable::addLog($user->id, $user->user_type, $user->id . 'upload photo to album: '. $title .', owner user ID: '.$otherUser->id);

        $albumService = new AlbumService($otherUser->id);

        return $albumService->Get($title)->uploadPhoto($photoTitle, $photoDesc);
    } 

    public function actionEditAlbum()
    {
        $albumID = Yii::$app->request->post('albumID');
        $public = Yii::$app->request->post('public');
        $active = Yii::$app->request->post('active');
        $title = Yii::$app->request->post('title');
        $description = Yii::$app->request->post('description');
        
        if (!isset($albumID) || !isset($public) || !isset($title) || !isset($description) ||
            !filter_var($albumID, FILTER_VALIDATE_INT) || !is_numeric($public) || !is_numeric($active) ||
            trim($title) == '' || trim($description) == '') {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'Missing params' . print_r(!isset($public), true)
            ];
        }

        $otherUserID = Yii::$app->request->post('otherUserID');
        
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'User not found'
            ];
        }

        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }

        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to create album for user {$otherUser->id}");
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'Access Denied'
                ];
            }
        }

        $albumService = new AlbumService($otherUser->id);
        $result = $albumService->edit($albumID, $public, $title, $description, $active);

        if ($result['success']) {
            LogTable::addLog($user->id, $user->user_type, 'User with id ' . $user->id . ' update photo album ID: ' . $albumID . ', owner user ID: ' . $otherUser->id);
        }

        return $result;
    }

    public function actionDeleteAlbum()
    {
        $title = Yii::$app->request->post('title');
        if (empty($p)) {
            return json_encode(['success' => false, 'code' => 1, 'message' => 'Missing params']);
        }
        if (empty($p['user_id']) || empty($p['title'])) {
            throw new BadRequestHttpException('missing params', 1);
        }
        $title = trim($p['title']);
        if ($title == 'default') {
            return ['success' => false, 'message' => 'Access is denied'];
        }

        $user = new User($p['user_id']);

        $result = $user->album->delete($title);

        return $result;
    }

    public function actionSetAlbumCover()
    {
        
        $otherUserID = Yii::$app->request->post('otherUserID');
        
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'User not found'
            ];
        }

        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }

        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to set album cover for album {$albumID} for user {$otherUser->id}");
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'Access Denied'
                ];
            }
        }

        $albumID = Yii::$app->request->post('albumID');
        $photoID = Yii::$app->request->post('photoID');

        if (!isset($photoID) || !filter_var($photoID, FILTER_VALIDATE_INT) || !isset($albumID) || !filter_var($albumID, FILTER_VALIDATE_INT)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }        

        $albumService = new AlbumService($otherUser->id);
        $result = $albumService->setAlbumCover($photoID, $albumID);

        return $result;
    }


}