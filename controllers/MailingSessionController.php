<?php
namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\Response;

/* my models */
use app\models\LogTable;
use app\models\User;
use app\models\ApproveStatus;
use app\models\UserProfile;
use app\models\MailingTemplate;
use app\models\MailingQueue;
use app\models\MailingSession;



class MailingSessionController extends ActiveController
{
    use CommonActionsTrait;
    public $modelClass = '';

    public function behaviors()
    {
        $authArray = ['start-chat-mailing', 'start-message-mailing', 
                      'close-chat-mailing-session', 'close-message-mailing-session',
                      'get-chat-mailing-session', 'get-message-mailing-session',
                      'change-chat-mailing-status', 'change-message-mailing-status',
                      'get-chat-sessions', 'get-message-sessions', 'get-sessions', 'get-session'
                      ];
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => $authArray
        ];

        $access = $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['login'],
            'rules' => [
                [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => $authArray,
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $verbs = $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ];
        
        unset($behaviors['access']);
        unset($behaviors['authenticator']);
        unset($behaviors['verbs']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Allow-Origin'      => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],
        ];
        
        $behaviors['rateLimiter'] = [
            // Use class
            'class' => \highweb\ratelimiter\RateLimiter::className(),
            
            // The maximum number of allowed requests
            'rateLimit' => 100,
            
            // The time period for the rates to apply to
            'timePeriod' => 60,
            
            // Separate rate limiting for guests and authenticated users
            // Defaults to true
            // - false: use one set of rates, whether you are authenticated or not
            // - true: use separate ratesfor guests and authenticated users
            'separateRates' => false,
            
            // Whether to return HTTP headers containing the current rate limiting information
            'enableRateLimitHeaders' => false,
        ];
        
        $behaviors['authenticator'] = $auth;
        $behaviors['access'] = $access;
        $behaviors['verbs'] = $access;
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    private function startMailing($type)
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_FEMALE])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }

        $templateID = Yii::$app->request->post('templateID');
        $excludeBlacklist = Yii::$app->request->post('blacklist');

        if (!isset($templateID) || !is_numeric($templateID)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'Missing params'
            ];
        } else {
            $templateID = (int) $templateID;
        }
        if (!isset($excludeBlacklist)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'Missing params'
            ];
        } else {
            $excludeBlacklist = (boolean) $excludeBlacklist;
        }

        // get template
        $template = MailingTemplate::find()
                    ->where([
                        'template_creator' => $user->id, 
                        'created_for' => $type, 
                        'agency_id' => $user->agency_id, 
                        'id' => $templateID
                    ])
                    ->one();
        if (empty($template)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'Template not found'
            ];  
        }                

        if ($template->status != ApproveStatus::STATUS_APPROVED) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'Template not approved'
            ];
        }

        // get last mailing session for this girl and check if it was finished
        $lastMailingSession = MailingSession::find()
                              ->where([
                                    'creator' => $user->id,
                                    'was_finished' => MailingSession::WASNT_FINISHED,
                                    'created_for' => $type
                              ])        
                              ->orderBy(['started_at' => SORT_DESC])                      
                              ->asArray()
                              ->one();
        if (!empty($lastMailingSession)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'You have unfinished mailing. You can stop mailing session pressing "Stop" button'
            ];
        }                      
        // get all online men
        $onlineMen = User::find()
                    ->select('id')
                    ->where([
                        'user_type' => User::USER_MALE,
                        'status' => User::STATUS_ACTIVE,
                        'owner_status' => User::STATUS_ACTIVE,
                        'visible' => User::STATUS_VISIBLE,
                        'approve_status_id' => ApproveStatus::STATUS_APPROVED,                        
                    ])
                    ->andWhere(['>=', 'last_activity', time() - 30])
                    ->all();

        // saving mailing session        
        $mailingSession = new MailingSession();
        $mailingSession->creator = $user->id;
        $mailingSession->template_id = $templateID;
        $mailingSession->created_for = $type;
        $mailingSession->sended_messages = 0;
        $mailingSession->dublicated_messages = 0;
        $mailingSession->total_messages = count($onlineMen);
        $mailingSession->exclude_blacklist = ($excludeBlacklist) ? 1 : 0;
        $mailingSession->paused = MailingSession::NOT_PAUSED;
        $mailingSession->was_finished = MailingSession::WASNT_FINISHED;
        $mailingSession->started_at = date('Y-m-d H:i:s');
        $mailingSession->ended_at = null;
        $dublicatedMessages = 0;
        $totalMessages = 0;

        if (!$mailingSession->save()) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'Error during start mailing session'
            ];
        }

        $templateWasAlreadySend = MailingQueue::find()
                            ->select('id, receiver')
                            ->where([
                                'created_for' => $type,
                                'template_id' => $templateID,
                                'sender' => $user->id,
                                'was_aborted' => MailingQueue::WASNT_ABORTED
                            ])
                            ->asArray()->all();
        $templateWasAlreadySend = ArrayHelper::map($templateWasAlreadySend, 'id', 'receiver');                    
        $rows = [];                           
        foreach ($onlineMen as $man) {
            if (!in_array($man->id, $templateWasAlreadySend)) {                
                if ($excludeBlacklist) {
                    if (empty($user->getUserBlacklists0()->where(['blacklist_user_id' => $man->id])->one())) {
                        $rows[] = [
                            'id' => null,
                            'mailing_session_id' => $mailingSession->id,
                            'created_for' => $type,
                            'template_id' => $templateID,
                            'sender' => $user->id,
                            'receiver' => $man->id,
                            'was_received' => MailingQueue::WASNT_RECEIVED,
                            'was_aborted' => MailingQueue::WASNT_ABORTED,
                            'created_at' => date('Y-m-d H:i:s'),
                            'received_at' => null
                        ];                        
                        $totalMessages++;
                    }                    
                } else {
                    $rows[] = [
                        'id' => null,
                        'mailing_session_id' => $mailingSession->id,
                        'created_for' => $type,
                        'template_id' => $templateID,
                        'sender' => $user->id,
                        'receiver' => $man->id,
                        'was_received' => MailingQueue::WASNT_RECEIVED,
                        'was_aborted' => MailingQueue::WASNT_ABORTED,
                        'created_at' => date('Y-m-d H:i:s'),
                        'received_at' => null
                    ];
                    $totalMessages++;
                }                
            } else {
                $dublicatedMessages++;
            }                                   
        }

        $mailingQueue = new MailingQueue();
        Yii::$app->db->createCommand()->batchInsert(MailingQueue::tableName(), $mailingQueue->attributes(), $rows)->execute();

        $mailingSession->total_messages = $totalMessages;
        $mailingSession->dublicated_messages = $dublicatedMessages;
        if (!$mailingSession->save()) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'Error during start mailing session'
            ];
        }

        return [
            'success' => true,
            'id' => $mailingSession->id,
        ];

    }

    public function actionStartChatMailing()
    {
        return $this->startMailing(MailingTemplate::CREATED_FOR_CHAT);
    }

    public function actionStartMessageMailing()
    {
        return $this->startMailing(MailingTemplate::CREATED_FOR_MESSAGE);
    }

    private function closeMailingSession($type)
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_FEMALE])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }

        $mailingSessionId = Yii::$app->request->post('session_id');
        if (empty($mailingSessionId) || !is_numeric($mailingSessionId)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'Wrong session id'
            ];
        } else {
            $mailingSessionId = (int) $mailingSessionId;
        }       

        $lastMailingSession = MailingSession::find()
                              ->where([
                                    'id' => $mailingSessionId,
                                    'creator' => $user->id,
                                    'was_finished' => MailingSession::WASNT_FINISHED,
                                    'created_for' => $type
                              ])
                              ->orderBy(['started_at' => SORT_DESC])                              
                              ->one();
        if (!empty($lastMailingSession)) {
            MailingQueue::updateAll([
                                     'was_aborted' => MailingQueue::WAS_ABORTED
                                    ], 
                                    [
                                     'mailing_session_id' => $lastMailingSession->id, 
                                     'was_received' => MailingQueue::WASNT_RECEIVED,
                                     'created_for' => $type
                                    ]);
            $lastMailingSession->was_finished = MailingSession::WAS_FINISHED;
            $lastMailingSession->ended_at = date('Y-m-d H:i:s');

            if (!$lastMailingSession->save()) {
                return [
                    'success' => false,
                    'code' => 1,
                    'message' => 'Error during close mailing session'
                ];
            }
            return [
                'success' => true,
                'message' => 'Mailing session was successully stopped'
            ];
        } else {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'You have not unfinished mailing sessions.'
            ];
        }                      
    }

    public function actionCloseChatMailingSession()
    {
        return $this->closeMailingSession(MailingTemplate::CREATED_FOR_CHAT);
    }

    public function actionCloseMessageMailingSession()
    {
        return $this->closeMailingSession(MailingTemplate::CREATED_FOR_MESSAGE);
    }

    private function getMailingSession($type, $sessionID = null)
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_FEMALE, User::USER_ADMIN, User::USER_AGENCY, User::USER_SUPERADMIN])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }
        if ($user->user_type == User::USER_FEMALE) {
            $lastMailingSession = MailingSession::find()
                              ->where([
                                    'creator' => $user->id,
                                    'was_finished' => MailingSession::WASNT_FINISHED,
                                    'created_for' => $type
                              ])
                              ->orderBy(['started_at' => SORT_DESC])
                              ->limit(1)
                              ->asArray()
                              ->one();
            if (!empty($lastMailingSession)) {
                return [
                    'success' => true,
                    'id' => $lastMailingSession['id'],
                    'paused' => (int) $lastMailingSession['paused'],
                    'started_at' => date('c', strtotime($lastMailingSession['started_at'])),
                    'sended_messages' => (int) $lastMailingSession['sended_messages'],
                    'total_messages' => (int) $lastMailingSession['total_messages']
                ];
            } else {
                return [
                    'success' => false,
                    'code' => 1,
                    'message' => 'You have not active mailing sessions'
                ];
            }
        } elseif ($user->user_type == User::USER_AGENCY || $user->user_type == User::USER_ADMIN || $user->user_type == User::USER_SUPERADMIN) {
            $subQuery = UserProfile::find()
                        ->select(['id', 'first_name', 'last_name']);

            $subQuery2 = MailingTemplate::find()                                                
                        ->select(['id', 'title', 'body']);

            $query = MailingSession::find()
                        ->select(['mailing_session.id', 'creator', 'title', 'body', 'sended_messages', 'dublicated_messages', 'total_messages', 'exclude_blacklist', 'paused', 'was_finished', 'started_at', 'ended_at', 'user_profile.first_name', 'user_profile.last_name']);
            if ($user->user_type == User::USER_AGENCY || $user->user_type == User::USER_ADMIN) {
                $query = $query->andWhere(['creator' => $user->getMyChidrensIds()]);
            }
            $query = $query->andWhere([                            
                                'mailing_session.created_for' => $type,
                            ]);
            if (!empty($sessionID)) {
                $query = $query->andWhere(['mailing_session.id' => $sessionID]); 
            }                       
            $query      ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = creator')
                        ->leftJoin(['template' => $subQuery2], 'template_id = template.id');
            $session = $query->asArray()->limit(1)->one();
            if (!empty($session)) {
                return [
                    'success' => true,
                    'session' => $session                    
                ];
            } else {
                return [
                    'success' => false,
                    'code' => 1,
                    'message' => 'Session with such ID doesnot exist'
                ];
            }
        }        
    }

    public function actionGetChatMailingSession()
    {
        return $this->getMailingSession(MailingTemplate::CREATED_FOR_CHAT);
    }

    public function actionGetMessageMailingSession()
    {
        return $this->getMailingSession(MailingTemplate::CREATED_FOR_MESSAGE);
    }

    private function changeMailingStatus($type)
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_FEMALE])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }

        $mailingSessionId = Yii::$app->request->post('session_id');
        if (empty($mailingSessionId) || !is_numeric($mailingSessionId)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'Wrong session id'
            ];
        } else {
            $mailingSessionId = (int) $mailingSessionId;
        }

        $lastMailingSession = MailingSession::find()
                              ->where([
                                    'id' => $mailingSessionId,
                                    'creator' => $user->id,
                                    'was_finished' => MailingSession::WASNT_FINISHED,
                                    'created_for' => $type
                              ])
                              ->orderBy(['started_at' => SORT_DESC])                              
                              ->one();
        if (!empty($lastMailingSession)) {
            if ($lastMailingSession->paused == MailingSession::PAUSED) {
                $lastMailingSession->paused = MailingSession::NOT_PAUSED;
            } else {
                $lastMailingSession->paused = MailingSession::PAUSED;
            }                     

            if (!$lastMailingSession->save()) {
                return [
                    'success' => false,
                    'code' => 1,
                    'message' => 'Error during change mailing session'
                ];
            }
            return [
                'success' => true,
                'message' => 'Mailing session status was successully changed'
            ];
        } else {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'You have not unfinished mailing sessions.'
            ];
        }                              
    }

    public function actionChangeChatMailingStatus()
    {
        return $this->changeMailingStatus(MailingTemplate::CREATED_FOR_CHAT);
    }

    public function actionChangeMessageMailingStatus()
    {
        return $this->changeMailingStatus(MailingTemplate::CREATED_FOR_MESSAGE);
    }

    private function getMailingSessions($type)
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_ADMIN, User::USER_AGENCY, User::USER_SUPERADMIN])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }

        $page = Yii::$app->request->get('page');
        $perPage = Yii::$app->request->get('per_page');
        $dateFrom = Yii::$app->request->get('date_from');
        $dateTo = Yii::$app->request->get('date_to');

        if (isset($page) && !is_numeric($page)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Wrong page parameter'
            ];
        } else {
            $page = (int) $page;
        }

        if ($page < 0) {
            $page = 1;
        }

        if (isset($perPage) && !is_numeric($perPage)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Wrong per page parameter'
            ];
        } else {
            $perPage = (int) $perPage;
        }

        if (isset($perPage) && $perPage < 0) {
            $perPage = 10;
        }

        if (isset($dateFrom) && !strtotime($dateFrom)) {
            $dateFrom = date("Y-m-d", 0);
        } elseif (isset($dateFrom) && strtotime($dateFrom)) {
            $dateFrom = date("Y-m-d", strtotime($dateFrom));
        }

        if (isset($dateTo) && !strtotime($dateTo)) {
            $dateTo = date("Y-m-d") . ' 23:59:59';
        } elseif (isset($dateTo) && strtotime($dateTo)) {
            $dateTo = date("Y-m-d", strtotime($dateTo)) . ' 23:59:59';
        }
        
        if ($user->user_type == User::USER_AGENCY || $user->user_type == User::USER_ADMIN) {            
            $subQuery = UserProfile::find()
                        ->select(['id', 'first_name', 'last_name']);

            $subQuery2 = MailingTemplate::find()                                                
                        ->select(['id', 'title', 'body']);

            $query = MailingSession::find()
                        ->select(['mailing_session.id', 'creator', 'title', 'body', 'sended_messages', 'total_messages', 'paused', 'exclude_blacklist', 'was_finished', 'started_at', 'ended_at', 'user_profile.first_name', 'user_profile.last_name'])
                        ->where(['creator' => $user->getMyChidrensIds()])
                        ->andWhere([                            
                            'mailing_session.created_for' => $type,
                        ])                        
                        ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = creator')
                        ->leftJoin(['template' => $subQuery2], 'template_id = template.id');
            if (!empty($dateFrom) && !empty($dateTo)) {
                if ($dateTo >= $dateFrom) {
                    $query->andWhere(['>=', 'mailing_session.started_at', $dateFrom])
                      ->andWhere(['<=', 'mailing_session.started_at', $dateTo]);
                } else {
                    return [
                        'success' => false,
                        'code' => 1,
                        'message' => 'End date have to be greater or equal start date'
                    ];
                }                
            }                        
            $count = $query->count();            
            if (!empty($perPage)) {
                $query->limit($perPage);
            }
            if (!empty($page)) {
                if ($count >= ($page-1)*$perPage) {
                    $query->offset(($page-1)*$perPage);
                } else {
                    return [
                        'success' => false,
                        'code' => 1,
                        'message' => 'Page not found'
                    ];
                }                
            }
            $query = $query->orderBy(['started_at' => SORT_DESC]);
            $sessions = $query->asArray()->all();
        } elseif ($user->user_type == User::USER_SUPERADMIN) {
            $subQuery = UserProfile::find()
                        ->select(['id', 'first_name', 'last_name']);

            $subQuery2 = MailingTemplate::find()                                                
                        ->select(['id', 'title', 'body']);

            $query = MailingSession::find()
                        ->select(['mailing_session.id', 'creator', 'title', 'body', 'sended_messages', 'total_messages', 'paused', 'exclude_blacklist', 'was_finished', 'started_at', 'ended_at', 'user_profile.first_name', 'user_profile.last_name'])
                        ->andWhere([                            
                            'mailing_session.created_for' => $type, 
                        ])
                        ->leftJoin(MailingQueue::tableName(), 'mailing_session_id = mailing_session.id')
                        ->leftJoin(['user_profile' => $subQuery], 'user_profile.id = creator')
                        ->leftJoin(['template' => $subQuery2], 'template_id = id');
            if (!empty($dateFrom) && !empty($dateTo)) {
                if ($dateTo >= $dateFrom) {
                    $query->andWhere(['>=', 'mailing_session.started_at', $dateFrom])
                      ->andWhere(['<=', 'mailing_session.started_at', $dateTo]);
                } else {
                    return [
                        'success' => false,
                        'code' => 1,
                        'message' => 'End date have to be greater or equal start date'
                    ];
                }                
            }    
            $count = $query->count();
            if (!empty($perPage)) {
                $query->limit($perPage);
            }
            if (!empty($page)) {
                if ($count >= ($page-1)*$perPage) {
                    $query->offset(($page-1)*$perPage);
                } else {
                    return [
                        'success' => false,
                        'code' => 1,
                        'message' => 'Page not found'
                    ];
                }  
            }
            $query = $query->orderBy(['started_at' => SORT_DESC]);
            $sessions = $query->asArray()->all();
        }

        $result = [
            'success' => true,
            'sessions' => $sessions
        ];

        if (in_array($user->user_type, [User::USER_ADMIN, User::USER_AGENCY, User::USER_SUPERADMIN])) {
            $result['count'] = $count;
        }

        return $result;
    }

    public function actionGetChatSessions()
    {
        return $this->getMailingSessions(MailingTemplate::CREATED_FOR_CHAT);
    }

    public function actionGetMessageSessions()
    {
        return $this->getMailingSessions(MailingTemplate::CREATED_FOR_MESSAGE);
    }

    public function actionGetSessions()
    {
        return $this->getMailingSessions([MailingTemplate::CREATED_FOR_CHAT, MailingTemplate::CREATED_FOR_MESSAGE]);
    }

    public function actionGetSession($sessionID)
    {
        return $this->getMailingSession([MailingTemplate::CREATED_FOR_CHAT, MailingTemplate::CREATED_FOR_MESSAGE], $sessionID);
    }
}