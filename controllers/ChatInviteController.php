<?php
namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\Response;

/* my models */
use app\models\LogTable;
use app\models\User;
use app\models\ApproveStatus;
use app\models\UserProfile;

use app\models\Chat;
use app\models\ChatInvite;
use app\models\Photo;



class ChatInviteController extends ActiveController
{
    use CommonActionsTrait;
    public $modelClass = '';

    const CHAT_INVITES_SHOWN_BY_DEFAULT = 5;

    public function behaviors()
    {
        $authArray = ['get-invites', 'hide-invite'];
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => $authArray
        ];

        $access = $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['login'],
            'rules' => [
                [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => $authArray,
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $verbs = $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ];
        
        unset($behaviors['access']);
        unset($behaviors['authenticator']);
        unset($behaviors['verbs']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Allow-Origin'      => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],
        ];
        
        $behaviors['rateLimiter'] = [
            // Use class
            'class' => \highweb\ratelimiter\RateLimiter::className(),
            
            // The maximum number of allowed requests
            'rateLimit' => 30,
            
            // The time period for the rates to apply to
            'timePeriod' => 60,
            
            // Separate rate limiting for guests and authenticated users
            // Defaults to true
            // - false: use one set of rates, whether you are authenticated or not
            // - true: use separate ratesfor guests and authenticated users
            'separateRates' => false,
            
            // Whether to return HTTP headers containing the current rate limiting information
            'enableRateLimitHeaders' => false,
        ];
        
        $behaviors['authenticator'] = $auth;
        $behaviors['access'] = $access;
        $behaviors['verbs'] = $access;
        $behaviors['authenticator']['except'] = ['options', 'send-chat-message', 'send-message'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }


    public function actionGetInvites($page = 1)
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_MALE])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }

        //$page = Yii::$app->request->get('page');
        $perPageFromGET = Yii::$app->request->get('per_page');

        if (!empty($perPageFromGET)) {
            $perPage = $perPageFromGET;
        } else {
            $perPage = self::CHAT_INVITES_SHOWN_BY_DEFAULT;
        }

/*        if (isset($page) && !is_numeric($page)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Wrong page parameter'
            ];
        } else {
            $page = (int) $page;
        }

        if ($page < 0) {
            $page = 1;
        }*/

        if (isset($perPage) && !is_numeric($perPage)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Wrong per page parameter'
            ];
        } else {
            $perPage = (int) $perPage;
        }

        if (isset($perPage) && ($perPage < 0 || $perPage > 20)) {
            $perPage = self::CHAT_INVITES_SHOWN_BY_DEFAULT;
        }

        if ($user->user_type == User::USER_MALE) {
            $query = (new \yii\db\Query())
                                        ->select(['user.id',
                                              'chat_invite.id as invite_id',
                                              'small_thumb', 
                                              'medium_thumb', 
                                              'first_name',
                                              'message',
                                              'last_activity', 
                                              'birthday'
                                              ])
                                        ->from(ChatInvite::tableName())
                                        ->leftJoin(User::tableName(), 'user.id = chat_invite.invite_sender')
                                        ->leftJoin(UserProfile::tableName(), 'user.id = user_profile.id')
                                        ->leftJoin(Photo::tableName(), 'photo.id = user.avatar_photo_id');            

            // show only chat invites that was created in last 5 minutes
            $query->andWhere(['show' => ChatInvite::SHOW_INVITE]);         
            $query->andWhere(['>=', 'chat_invite.created_at', date('Y-m-d H:i:s', time()-5*60)]);            
            if (!empty($perPage)) {
                $query->limit($perPage);
            }
            
            $query->orderBy(['chat_invite.created_at' => SORT_ASC]);

            $chatInvites = $query->all();
            
            $simplifiedChatInvites = [];
            $i = 0;
            foreach ($chatInvites as $chatInvite) {
                $simplifiedChatInvites[$i]['id'] = $chatInvite['id'];
                $simplifiedChatInvites[$i]['invite_id'] = $chatInvite['invite_id'];
                $tz = new \DateTimeZone('Europe/Brussels');               
                if (!empty($chatInvite['birthday'])) {
                    $simplifiedChatInvites[$i]['age'] = \DateTime::createFromFormat('Y-m-d', $chatInvite['birthday'], $tz)
                    ->diff(new \DateTime('now', $tz))
                    ->y;
                } else {
                    $simplifiedChatInvites[$i]['age'] = null;
                }                 
                $simplifiedChatInvites[$i]['first_name'] = $chatInvite['first_name'];
                $simplifiedChatInvites[$i]['message'] = $chatInvite['message'];
                $simplifiedChatInvites[$i]['small_thumb'] = $chatInvite['small_thumb'];
                $simplifiedChatInvites[$i]['medium_thumb'] = $chatInvite['medium_thumb'];
                $simplifiedChatInvites[$i]['online'] = ($chatInvite['last_activity'] >= time() - 30) ? 1 : 0;
                $i++;
            }
        }

        $result = [
            'success' => true,
            'chatInvites' => $simplifiedChatInvites
        ];

        if (in_array($user->user_type, [User::USER_ADMIN, User::USER_AGENCY, User::USER_SUPERADMIN])) {
            $result['count'] = $count;
        }
        return $result;
    }

    public function actionHideInvite()
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_MALE])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }

        $inviteID = Yii::$app->request->post('inviteID');   

        if ($user->user_type == User::USER_MALE) {
            $chatInvite = ChatInvite::find()->where(['id' => $inviteID, 
                                                     'invite_receiver' => $user->id, 
                                                     'show' => ChatInvite::SHOW_INVITE])->one();
            if (empty($chatInvite)) {
                return [
                    'success' => false,
                    'code' => 1,
                    'message' => 'Invite not found'
                ];
            } else {
                $chatInvite->show = ChatInvite::HIDE_INVITE;
                if (!$chatInvite->update(false)) {
                    return [
                        'success' => false,
                        'code' => 1,
                        'message' => 'Error during update'
                    ];
                } else {
                    return [
                        'success' => true
                    ];
                }
            }
        }
    }
}