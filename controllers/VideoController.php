<?php
namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\Response;

/* my models */
use app\models\LogTable;
use app\models\Video;
use app\models\ActionType;
use app\models\Action;
use app\models\AgencyRevard;
use app\models\User;
use app\models\UserProfile;
use app\models\Photo;
use app\models\Status;
use app\models\VideoAccess;

/* my services */
use app\services\VideoService;
use app\services\BillingService;
use app\services\PaymentsService;

class VideoController extends ActiveController
{
    use CommonActionsTrait;
    public $modelClass = '';

    public function behaviors()
    {
        $authArray = ['get-videos', 'update-access'];
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => $authArray
        ];

        $access = $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['login'],
            'rules' => [
                [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => $authArray,
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $verbs = $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ];
        
        unset($behaviors['access']);
        unset($behaviors['authenticator']);
        unset($behaviors['verbs']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Allow-Origin'      => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],
        ];
        
        $behaviors['rateLimiter'] = [
            // Use class
            'class' => \highweb\ratelimiter\RateLimiter::className(),
            
            // The maximum number of allowed requests
            'rateLimit' => 100,
            
            // The time period for the rates to apply to
            'timePeriod' => 60,
            
            // Separate rate limiting for guests and authenticated users
            // Defaults to true
            // - false: use one set of rates, whether you are authenticated or not
            // - true: use separate ratesfor guests and authenticated users
            'separateRates' => false,
            
            // Whether to return HTTP headers containing the current rate limiting information
            'enableRateLimitHeaders' => false,
        ];
        
        $behaviors['authenticator'] = $auth;
        $behaviors['access'] = $access;
        $behaviors['verbs'] = $access;
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }
    
    public function beforeAction($action)
    {    
        if (parent::beforeAction($action)) {               
            $user = \Yii::$app->user->identity;
            if (!Yii::$app->user->isGuest && in_array($user->user_type, [User::USER_ADMIN, User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('user_access'))) {
                throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
            }
            return true;
        } else {
            return false;
        }
    }

    public function actionGetVideos()
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        $otherUserID = Yii::$app->request->post('otherUserID');
        $otherUserID = (isset($otherUserID) && filter_var($otherUserID, FILTER_VALIDATE_INT)) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);        

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }
        
        $videoList = $otherUser->getVideos()->addSelect(['video.id', 'path', 'video.description','video.status', 'video.public','video.premium',  'photo.medium_thumb', 'original_image', 'watermark_small', 'watermark_medium', 'watermark_orign'])->leftJoin(Photo::tableName(), 'video.poster = photo.id')->asArray()->all();        
        
        $videoAccess = VideoAccess::find()->where(['user_id' => $user->id])->asArray()->all();        
        $canWatch = ArrayHelper::map($videoAccess, 'id', 'video_id');

        $date = date("Y-m-d H:i:s", time());
        $userStatusArray = $user->getStatuses()->joinWith('statusType')->where(["<=", 'start_datetime', $date])->andWhere(['active' => Status::STATUS_ACTIVE])->andWhere([">=", 'stop_datetime', $date])->all();        
        $userStatus      = isset($userStatusArray[0]["statusType"]->title) ? $userStatusArray[0]["statusType"]->title : "BASIC";

        $videos = [];
        if (!empty($videoList)) {
            $i = 0;
            foreach ($videoList as $video) {
                if ($video['status'] == 4 && $video['public'] == 1) {
                    $videos[$i]['id'] = $video['id'];
                    $videos[$i]['path'] = $video['path'];
                    $videos[$i]['medium_thumb'] = $video['medium_thumb'];
                    $videos[$i]['original_image'] = $video['original_image'];
                    if ($video['premium'] == 1 && $userStatus != "PREMIUM" && !in_array($video['id'], $canWatch)) {
                        $videos[$i]['path'] = '/img/premium_big.jpg'/*$video['watermark_orign']*/;
                        $videos[$i]['medium_thumb'] = '/img/premium_medium.jpg'/*$video['watermark_medium']*/;
                        $videos[$i]['original_image'] = '/img/premium_big.jpg'/*$video['watermark_orign']*/;
                    }                    
                }
                $i++;
            }
        }

        return [
            'success' => true, 
            'videos' => $videos, 
        ];
    }

    public function actionUpdateAccess()
    {
        $videoID = Yii::$app->request->post('videoID');

        if (!isset($videoID) || !is_numeric($videoID)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            throw new UnauthorizedHttpException();            
        }

        if ($user->user_type == User::USER_MALE) { 
            $paymentsService = new PaymentsService($user->id);
            $userBalance = $paymentsService->getBalance();
            $actionType = ActionType::findOne(['name' => 'watch video']);
            if (empty($actionType)) {
                return [
                    'success' => FALSE, 
                    'code' => 2, 
                    'message' => 'Watch user video price error'
                ];
            }

            $billingService     = new BillingService($user->id);
            $watchVideoPrice = $billingService->getActionPrice($skinID = 1, $planID = 1, $actionType->id, $user->id);

            if (!is_numeric($watchVideoPrice)) {
                return [
                    'success' => FALSE, 
                    'code' => 3, 
                    'message' => 'Watch user video price error'
                ];
            }

            if ($userBalance < $watchVideoPrice) {
                return [
                    'success' => FALSE, 
                    'code' => 2, 
                    'message' => 'Not enough money'
                ];
            }
            $video = Video::findOne(['id' => $videoID]);
            if (!empty($video)) {                
                //add new action
                $action                 = new Action();
                $action->action_type_id = $actionType->id;
                $action->skin_id        = 1;
                $action->action_creator = $user->id;
                $action->action_receiver  = $video->user_id;
            } else {
                return [
                    'success' => FALSE, 
                    'code' => 2, 
                    'message' => 'Wrong video id'
                ];                
            }
            
            if (!$action->save()) {
                return [
                    'success' => FALSE, 
                    'code' => 2, 
                    'message' => 'Action not save'
                ];
            }

            //с мужиков взымаем плату
            $paymentsService->addCredits($action->id, $watchVideoPrice);

            //добавляем премию агенству
            $girl = User::findOne(['id' => $video->user_id]);
            if (!empty($girl)) {
                //даем бонус агенству
                $agency      = User::find()->where([
                                                    'user_type' => User::USER_AGENCY, 
                                                    'agency_id' => $girl->agency_id,
                                                    'status' => User::STATUS_ACTIVE
                                                ])->one();
                $agencyWatchVideoPrice = $billingService->getAgencyPrice($skinID = 1, $planID = 1, $actionType->id, $agency->id);
                if (!empty($agencyWatchVideoPrice)) {
                    $agencyRevard            = new AgencyRevard();
                    $agencyRevard->action_id = $action->id;
                    $agencyRevard->user_id   = $agency->id;
                    $agencyRevard->amount    = $agencyWatchVideoPrice;
                    if (!$agencyRevard->save()) {
                        return [
                            'success' => FALSE, 
                            'code' => 2, 
                            'message' => 'Agency bonus not save'
                        ];
                    }
                } else {
                    return [
                        'success' => FALSE, 
                        'code' => 4, 
                        'message' => 'Watch user video price error'
                    ];
                }
            }
            $videoService = new VideoService($user->id);
            $result = $videoService->updateAccess($videoID);
        }
        
        if (!empty($result)) {
            return [
                'success' => true
            ];
        }

        return [
            'success' => false,
            'code' => 1,
            'message' => 'Some error happend'
        ];
    }

}