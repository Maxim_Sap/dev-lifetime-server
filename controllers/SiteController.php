<?php

namespace app\controllers;

use app\models\Blog;
use app\models\Subscribe;
use yii\rest\ActiveController;
use app\models\SitePage;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\UploadForm;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use app\models\BlogSearch;
use app\models\User;
use app\models\Chat;

class SiteController extends ActiveController
{
    public $modelClass = '';

    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['X-Wsse'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],

            ],
        ];
    }

    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $this->view->registerJsFile('/js/custom.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        // тестируем работу приложения, добавлять в продакшен не нужно
/*        $chat = new Chat();
        $chat->action_id = 736;
        $chat->chat_session_id = 27;
        $chat->chat_id = '2vtx_kU02J7F7rMom40BOnXZsltsm48LVfpwJZPU';
        $chat->message = 'test';
        $chat->original_message = 'test';
        $chat->created_at = date('Y-m-d H:i:s');
        var_dump($chat->validate());
        var_dump($chat->save());
        var_dump($chat->errors);*/

        return null;
    }

    public function actionPayments($user_id)
    {
        $stripe_conf = Yii::$app->params['stripe'];
        $this->view->registerCssFile('/css/bootstrap-formhelpers-min.css');
        $this->view->registerJsFile('https://js.stripe.com/v2/');
        $this->view->registerJsFile('/js/payment-stripe.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/bootstrap-formhelpers-min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/bootstrapValidator-min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

        return $this->render('payments', ['user_id' => $user_id, 'stripe_conf' => $stripe_conf]);
    }

    public function actionContact()
    {
        return null;
    }

    /*
    public function actionGetStaticPagesOnMain()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $AboutPage = SitePage::findOne(['slug' => 'about']);
        if (empty($AboutPage)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'Page not found'
            ];
        }

        $HistoryPage = SitePage::findOne(['slug' => 'history']);
        if (empty($AboutPage)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'Page not found'
            ];
        }

        $sitePage['about'] = $AboutPage->attributes;
        $sitePage['history'] = $HistoryPage->attributes;
        return [
            'success' => true,
            'pageContent' => $sitePage
        ];
    }*/

    public function actionGetInfoPages()
    {
        $user = \Yii::$app->user->identity;
        if (!Yii::$app->user->isGuest && in_array($user->user_type, [User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('blog_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        $skinID = Yii::$app->request->post('skinID');
        $pageName = Yii::$app->request->post('pageName');
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (!isset($skinID) || !filter_var($skinID, FILTER_VALIDATE_INT) || !isset($pageName) || trim($pageName) == '') {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        $sitePage = SitePage::findOne(['slug' => $pageName]);
        if (empty($sitePage)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Page not found'
            ];
        }
        $sitePage = $sitePage->attributes;
        return [
            'success' => true, 
            'pageContent' => $sitePage
        ];
    }

    public function actionGetPostAndAboutOnMainPage()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $posts = Blog::getOnHomapage();
        $AboutPage = SitePage::findOne(['slug' => 'about']);
        $AboutPage = $AboutPage->attributes;

        return [
            'posts' => $posts,
            'about_page' => $AboutPage
        ];
    }

    public function actionGetFooterData()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $posts = Blog::getFooterPost();
        $HistoryPage = SitePage::findOne(['slug' => 'history']);
        $HistoryPage = $HistoryPage->attributes;

        return [
            'posts' => $posts,
            'history_page' => $HistoryPage,
        ];
    }

    public function actionAddToSubscribe()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $email = Yii::$app->request->post('email');
        $skinID = Yii::$app->request->post('skinID');
        $model = new Subscribe();
        $result = $model->addUser($email,$skinID);
        
        return $result;
    }

    public function actionGetSubscribe()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = Subscribe::find()->orderBy(['ID'=>SORT_DESC])->asArray()->all();

        return $result;

    }

    public function actionGetPosts()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $postId = Yii::$app->request->post('postId');
        $params = Yii::$app->request->post('params');
        if($postId){
            $posts = Blog::getItem((int)$postId);
        }else{
            $postArray = Yii::$app->request->post();
            if (!isset($postArray['page']) || !is_numeric($postArray['page'])) {
                $parameters['page'] = 0;
            } else {
                $parameters['page'] = (int)$postArray['page']-1;
            }
            if (!empty($postArray['params']['category'])) {
                $parameters['category'] = $postArray['params']['category'];
            }
            if (!empty($postArray['search'])) {
                $parameters['search'] = $postArray['search'];
            } else {
                $parameters['search'] = '';
            }
            if (isset($postArray['limit']) && is_numeric($postArray['limit'])) {
                $parameters['limit'] = $postArray['limit'];
            } else {
                $parameters['limit'] = 3;
            }
            
            try {
                $searchModel  = new BlogSearch();
                $dataProvider = $searchModel->search($parameters);                
                return [
                    'success'    => true,
                    'postList' => $dataProvider->models,
                    'count' => $dataProvider->getTotalCount(),
                ];
            } catch(Exception $e) {
                return [
                    'success' => false,
                    'code' => 1,
                    'message' => $e->getMessage()
                ];                
            }
            
            $posts = Blog::getList($params);
        }

        return $posts;
    }


}
