<?php

namespace app\controllers;


use app\models\Blog;

use app\models\ReferralStats;
use Yii;

use Stripe\Stripe;

use Stripe\Charge;

use yii\db\Expression;

use yii\db\Query;

use yii\helpers\Html;

use yii\filters\AccessControl;

use yii\filters\ContentNegotiator;

use yii\filters\VerbFilter;

use yii\filters\auth\HttpBearerAuth;

use yii\helpers\Json;

use yii\rest\ActiveController;

use yii\web\BadRequestHttpException;

use yii\web\ForbiddenHttpException;

use yii\web\Response;


/* my models */

use app\models\Activity;
use app\models\Chat;
use app\models\Action;
use app\models\Agency;

use app\models\Albums;
use app\models\ActionType;

use app\models\ApproveStatus;

use app\models\ChatSession;

use app\models\City;

use app\models\Country;

use app\models\Location as LocationsModel;

use app\models\LogTable;

use app\models\Photo;

use app\models\Status;

use app\models\User;

use app\models\UserProfile;
use app\models\AdminPermissions;
use app\models\UserType;
use app\models\UserInterpreter;
use app\models\UserBlacklist;

use app\models\UserWink;

use app\models\UserFavoriteList;

use app\models\UserVisiters;

use app\models\Video;


/* my services */

use app\services\Billing;

use app\services\Message;

use app\services\ChatService;

use app\services\Shop;

use app\services\StatusService;

use app\services\LetterService;

use app\services\Locations;

use app\services\AlbumService;

use app\services\PhotoService;

use app\services\PaymentsService;
use app\services\BillingService;


/*my forms*/

use app\models\forms\LoginForm;

use app\models\forms\ChangePasswordForm;


class UserController extends ActiveController

{

    public $modelClass = 'app\models\Users';


    public function behaviors()

    {

        $authArray = [
            'get-user-info', 'edit-info', 'logout', 'change-password',

            'get-girls-by-params', 'get-other-user-info', 'get-users-for-admin', 'get-user-video',

            'upload-video', 'upload-and-set-avatar', 'upload-and-set-poster', 'update-video-description',

            'get-albums-list-info', 'create-album', 'set-favorite', 'get-girls-to-chat-page',

            'get-users-for-chat', 'get-user-in-chat', 'set-user-to-black-list',

            'send-wink', 'get-user-balance', 'get-pricelist','add-user-funds','get-payment-key'
        ];

        $behaviors = parent::behaviors();


        $auth = $behaviors['authenticator'] = [

            'class' => HttpBearerAuth::className(),

            'only' => $authArray,

        ];


        $access = $behaviors['access'] = [

            'class' => AccessControl::className(),

            'only' => ['login'],

            'rules' => [

                [

                    'actions' => ['login'],

                    'allow' => TRUE,

                    'roles' => ['?'],

                ],

                [

                    'actions' => $authArray,

                    'allow' => TRUE,

                    'roles' => ['@'],

                ],

            ],

        ];

        $behaviors['contentNegotiator'] = [

            'class' => ContentNegotiator::className(),

            'formats' => [

                'application/json' => Response::FORMAT_JSON,

            ],

        ];

        $verbs = $behaviors['verbs'] = [

            'class' => VerbFilter::className(),

            'actions' => [

                'logout' => ['post'],

            ],

        ];


        unset($behaviors['access']);

        unset($behaviors['authenticator']);

        unset($behaviors['verbs']);


        $behaviors['corsFilter'] = [

            'class' => \yii\filters\Cors::className(),

            'cors' => [

                // restrict access to

                'Origin' => Yii::$app->params['origin'],

                'Access-Control-Allow-Origin' => Yii::$app->params['origin'],

                'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS'],

                // Allow only POST and PUT methods

                'Access-Control-Request-Headers' => ['*'],

                // Allow only headers 'X-Wsse'

                'Access-Control-Allow-Credentials' => TRUE,

                // Allow OPTIONS caching

                'Access-Control-Max-Age' => 3600,

                // Allow the X-Pagination-Current-Page header to be exposed to the browser.

                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],

            ],

        ];


        $behaviors['rateLimiter'] = [

            // Use class

            'class' => \highweb\ratelimiter\RateLimiter::className(),


            // The maximum number of allowed requests

            'rateLimit' => 600,


            // The time period for the rates to apply to

            'timePeriod' => 60,


            // Separate rate limiting for guests and authenticated users

            // Defaults to true

            // - false: use one set of rates, whether you are authenticated or not

            // - true: use separate ratesfor guests and authenticated users

            'separateRates' => FALSE,


            // Whether to return HTTP headers containing the current rate limiting information

            'enableRateLimitHeaders' => FALSE,

        ];


        $behaviors['authenticator'] = $auth;

        $behaviors['access'] = $access;

        $behaviors['verbs'] = $access;

        $behaviors['authenticator']['except'] = [
            'options', 'login',

            'get-girls-on-main-page', 'get-users',

            'get-user-locations','add-referral-visitor'

        ];


        return $behaviors;

    }


    public function actions()

    {

        $actions = parent::actions();


        unset($actions['index']);

        unset($actions['view']);

        unset($actions['create']);

        unset($actions['update']);

        unset($actions['delete']);


        return $actions;

    }


    public function actionLogin()

    {

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        Yii::$app->user->logout();

        $model = new LoginForm();


        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {

            if (Yii::$app->user->identity->status == User::STATUS_NO_ACTIVE) {

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Your profile is not active',

                ];

            } elseif (Yii::$app->user->identity->owner_status == User::STATUS_NO_ACTIVE) {

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Your agency is not active',

                ];

            } elseif (Yii::$app->user->identity->approve_status_id != ApproveStatus::STATUS_APPROVED) {

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Your profile was not approved',

                ];

            } else {

                $user = Yii::$app->user->identity;

                if (!$user) {

                    LogTable::addLog($user->id, $user->user_type, 'User object is empty');

                    return [

                        'success' => FALSE,

                        'code' => 1,

                        'message' => 'Error occurred while processing your request',

                    ];

                }
                if ($model->remember == 1) {
                	$user->access_token = $user->getJWT(Yii::$app->params['remember_token_time']);
                	$user->token_end_date = time() + Yii::$app->params['remember_token_time'];
                } else {
                	$user->access_token = $user->getJWT();
                	$user->token_end_date = time() + Yii::$app->params['token_time'];
                }                                

                $user->last_activity = time();

                if (!$user->save()) {

                    LogTable::addLog($user->id, $user->user_type, "Error during saving user with id {$user->id} in database");

                    return [

                        'success' => FALSE,

                        'code' => 1,

                        'message' => 'Error occurred while processing your request',

                    ];

                }


                $activity = new Activity();

                $activity->user_id = $user->id;

                $activity->started_at = date('Y-m-d H:i:s', time());

                $activity->ended_at = date('Y-m-d H:i:s', time());


                if (!$activity->save()) {

                    LogTable::addLog($user->id, $user->user_type, "Error during saving user activity for user with id {$user->id} in database");

                    return [

                        'success' => FALSE,

                        'code' => 1,

                        'message' => 'Error occurred while processing your request',

                    ];

                }


                $result = [];

                $result['userID'] = $user->id;

                $result['token'] = $user->access_token;
                if (!in_array($user->user_type,[User::USER_AGENCY,User::USER_AFFILIATE])) {
                    $result['userName'] = $user->profile->first_name . ' ' . $user->profile->last_name;
                } elseif($user->user_type == User::USER_AFFILIATE){
                    $affiliatesModel = $user->getAffiliates()->one();
                    $result['userName'] = $affiliatesModel->name;
                }else {
                    $result['userName'] = 'Not defined';
                }


                $result['avatar'] = (!empty($user->avatar->small_thumb)) ? $user->avatar->small_thumb : null;

                if (in_array($user->user_type, [
                    User::USER_AGENCY, User::USER_INTERPRETER,
                    User::USER_SUPERADMIN, User::USER_ADMIN,User::USER_AFFILIATE
                ])) {

                    $result['userType'] = $user->user_type;

                }


                if (in_array($user->user_type, [User::USER_FEMALE, User::USER_AGENCY, User::USER_INTERPRETER, User::USER_ADMIN])) {

                    $agency = $user->getAgency()->where(['approve_status' => ApproveStatus::STATUS_APPROVED])->one();

                    if (!empty($agency)) {
                        $agencyOwner         = $agency->getUsers()->where(['user_type' => User::USER_AGENCY, 'status' => User::STATUS_ACTIVE])->one();
                        $result['agency_id'] = $agencyOwner->id;
                        if ($user->user_type == User::USER_AGENCY) {
                            $result['userName'] = $agency->name;
                        }

                    } else {

                        $result['agency_id'] = 1;

                    }


                }
                if (in_array($user->user_type, [User::USER_ADMIN, User::USER_SITEADMIN])) {
                    $result['permissions'] = $this->getPermissions($user->permissions, $user->user_type);
                }

                LogTable::addLog($user->id, $user->user_type, "Successfull login for user with id {$user->id}");

                return [

                    'success' => TRUE,

                    'message' => 'Login is ok',

                    'userInfo' => $result,

                ];

            }

        } else {

            $model->validate();

            return [

                'success' => FALSE,

                'code' => 'Validation errors',

                'errors' => $model->errors,

            ];

        }

    }


    /**
     * Logout action.
     *
     * @return string
     */

    public function actionLogout()

    {


        $token = Yii::$app->request->post('token');


        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        LogTable::addLog($user->id, $user->user_type, "Successfull logout for user with id " . $user->id);
        $user->cam_online = 0;
        $user->save();

        Yii::$app->user->logout();


        return ['success' => TRUE];

    }


    public function actionCheckToken($params = null, $refresh = TRUE)

    {


        if ($params == null) {

            $token = Yii::$app->request->post('token');

        } else {

            $token = $params["token"];

        }


        if (!isset($token) || trim($token) == "") {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params',

            ];

        }


        $user = User::findIdentityByAccessToken($token);


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Token not found',

            ];

        }


        $rememberMeEndDate = $user->token_end_date + Yii::$app->params['remember_token_time'];


        if ($user->token_end_date < time() && $user->token_end_date < $rememberMeEndDate) {

            //cookie remember token


            $user->token_end_date = time() + Yii::$app->params['token_time'];

            if ($refresh && !$user->save()) {

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Error occurred while processing your request',

                ];

            }

        } else if ($user->token_end_date < time()) {

            return [

                'success' => FALSE,

                'code' => 2,

                'message' => 'Token time expired. Please login again.',

            ];

        }


        return [

            'success' => TRUE,

            'userType' => $user->user_type,

        ];

    }


    public function actionUploadVideo()

    {

        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        $otherUserID = Yii::$app->request->post('otherUserID');

        $otherUserID = (isset($otherUserID) && filter_var($otherUserID, FILTER_VALIDATE_INT)) ? $otherUserID : $user->id;


        $otherUser = User::findOne(['id' => $otherUserID]);


        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        $photoService = new PhotoService($otherUser->id);

        return $photoService->uploadVideo();


    }


    public function actionUploadAndSetAvatar()

    {


        $otherUserID = Yii::$app->request->post('otherUserID');


        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? (int)$otherUserID : $user->id;


        $otherUser = User::findOne(['id' => $otherUserID]);


        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        $params = Yii::$app->request->post('params');

        $params = isset($params) && $params == 'onlySet' ? TRUE : FALSE;


        if (empty($otherUser)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        if ($params) {

            $photoID = Yii::$app->request->post('photoID');

            $photoID = isset($photoID) && is_numeric($photoID) ? (int)$photoID : FALSE;

            if (!$photoID) {

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Photo not found',

                ];

            }

            $photoService = new PhotoService($otherUser->id);

            $result = $photoService->getPhotoById($photoID, TRUE);

            if (empty($result)) {

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Photo not found',

                ];

            }

            if ($result['approve_status'] != Photo::STATUS_APPROVED) {

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Photo not approved and can\'t be set as avatar',

                ];

            }

            $imageID = $result['id'];

        } else {

            $albumService = new AlbumService($otherUser->id);

            $result = $albumService->Get('default')->uploadPhoto('avatar', 'avatar photo');

            if (!$result['success']) {

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => $result['message'],

                ];

            }

            $imageID = $result['photo_id'];

        }


        $smallThumb = $result['small_thumb'];

        $mediumThumb = $result['medium_thumb'];


        $otherUser->avatar_photo_id = $imageID;


        if ($user->user_type == User::USER_FEMALE) { // if girl change her avatar, change her status to check new avatar

            $user->approve_status_id = 2;

        }


        $otherUser->scenario = User::SCENARIO_REGISTER_BY_SOCIAL;

        if (!$otherUser->save()) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User avatar error save',

            ];

        }


        LogTable::addLog($user->id, $user->user_type, $user->id . ' upload avatar, owner user ID: ' . $otherUserID);


        return [

            'success' => TRUE,

            'message' => 'Avatar change',

            'avatar' => [

                'small_thumb' => $smallThumb,

                'medium_thumb' => $mediumThumb,
                'original_image' => $result['original_image'],

            ],

        ];

    }


    public function actionGetUserBalance()

    {

        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        $paymentsService = new PaymentsService($user->id);


        return [

            'success' => TRUE,

            'balance' => $paymentsService->getBalance(),

        ];

    }


    public function actionUploadAndSetPoster()

    {


        $otherUserID = Yii::$app->request->post('otherUserID');


        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;


        $otherUser = User::findOne(['id' => $otherUserID]);


        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }

        if (in_array($user->user_type, [User::USER_ADMIN, User::USER_SITEADMIN]) && ($otherUserID != $user->id) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN) {

            // check if agency have this user                        

            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();

            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {

                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to update video poster for user {$otherUser->id}");

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Access Denied',

                ];

            }

        }


        $params = Yii::$app->request->post('params');

        $params = isset($params) && $params == 'only_set' ? TRUE : FALSE;


        $videoID = Yii::$app->request->post('videoID');


        if (empty($videoID) || !filter_var($videoID, FILTER_VALIDATE_INT)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Video not found',

            ];

        }


        if ($params) {

            $photoID = Yii::$app->request->post('photoID');

            $photoID = isset($photoID) && is_numeric($photoID) ? $photoID : FALSE;

            if (!$photoID) {

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Photo not found',

                ];

            }

            $photoService = new PhotoService($otherUser->id);

            $result = $photoService->getPhotoById($photoID, TRUE);

            if (empty($result)) {

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Photo not found',

                ];

            }

        } else {

            $albumService = new AlbumService($otherUser->id);

            $result = $albumService->Get('video poster')->uploadPhoto('poster', 'video poster photo');

            if (!$result['success']) {

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => $result['message'],

                ];

            }

        }


        $imageID = $result['photo_id'];

        $smallThumb = $result['small_thumb'];

        $mediumThumb = $result['medium_thumb'];


        $video = Video::findOne(['id' => $videoID]);


        if (empty($video)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Video not found',

            ];

        }


        if (!empty($video->poster)) {

            $oldPosterPhoto = Photo::findOne(['id' => $video->poster]);

            @unlink(dirname(__FILE__) . '/../web/' . $oldPosterPhoto->small_thumb);

            @unlink(dirname(__FILE__) . '/../web/' . $oldPosterPhoto->medium_thumb);

            @unlink(dirname(__FILE__) . '/../web/' . $oldPosterPhoto->original_image);

            @unlink(dirname(__FILE__) . '/../web/' . $oldPosterPhoto->watermark_small);

            @unlink(dirname(__FILE__) . '/../web/' . $oldPosterPhoto->watermark_medium);

            @unlink(dirname(__FILE__) . '/../web/' . $oldPosterPhoto->watermark_orign);

        }

        $video->poster = $imageID;

        if ($video->premium == 1 && !empty($video->poster)) {

            $photoService = new PhotoService($otherUser->id);

            $photo = $video->posterImage;

            $watermarkFilePath = dirname(__FILE__) . '/../web/img/premium.png';

            $photoService->addWatermark($photo, $watermarkFilePath);

        }


        if (!$video->save()) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Video poster error save',

            ];

        }


        if (!empty($oldPosterPhoto)) {

            $oldPosterPhoto->delete();

        }


        LogTable::addLog($user->id, $user->user_type, 'User with id ' . $user->id . ' upload poster, owner user ID: ' . $otherUserID);


        return [

            'success' => TRUE,

            'message' => 'Video poster change',

            'avatar' => [

                'small_thumb' => $smallThumb,

                'medium_thumb' => $mediumThumb,

            ],

        ];

    }

    public function actionGetPricelist($skinID, $planID, $type)
    {

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];
        }

        if (!isset($skinID) || !is_numeric($skinID) || !isset($planID) || !is_numeric($planID) || !isset($type)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Missing params',
            ];
        }

        $actionType = ActionType::findOne(['name' => str_replace('-', ' ', $type)]);
        if (empty($actionType)) {
            return [
                'success' => FALSE,
                'code'    => 2,
                'message' => 'Wrong type',
            ];
        }

        $billingService = new BillingService($user->id);
        $price          = $billingService->getActionPrice($skinID, $planID, $actionType->id, $user->id);

        return [
            'success' => TRUE,
            'price'   => $price,
        ];

    }

    public function actionGetGirlsToChatPage()

    {


        $skinID = Yii::$app->request->post('skinID');

        $userType = Yii::$app->request->post('userType');


        if (!isset($skinID) || !is_numeric($skinID) || !isset($userType) || !is_numeric($userType)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params',

            ];

        }


        $otherUserID = Yii::$app->request->post('otherUserID');


        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : null;


        if ($user->id == $otherUserID) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'You can\'t chatting with yourself',

            ];

        }


        if (empty($otherUserID)) {

            $otherUserID = $user->id;

        }


        $otherUser = User::findOne(['id' => $otherUserID]);


        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        if (time() - $otherUser->last_activity >= 30) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'This user is offline',

            ];

        }


        $limit = Yii::$app->request->post('limit');

        $params = [

            'limit' => (isset($limit)) ? $limit : null,

            'skinID' => $skinID,

            'userID' => $user->id,

            'userType' => $userType,

            'otherUserID' => $otherUserID,

            'visibility' => 'all',

            'order' => 'user.last_activity DESC',

        ];


        $params['type'] = 'last_chat';//and online users


        $userLastChat = json_decode(JSON::encode($this->actionGetUsersForChat($params)));


        //$params['type'] = 'active_chat';

        unset($params['visibility']);

        $params['type'] = 'last_activity';


        $userActiveChat = json_decode(JSON::encode($this->actionGetUsersForChat($params)));


        $params['type'] = 'winks';


        $userWinks = json_decode(JSON::encode($this->actionGetUsers($params)));


        $params['type'] = 'favorite';


        $userFavorite = json_decode(JSON::encode($this->actionGetUsers($params)));


        /*$billing_model = new Billing();



        $chat_minute_price = $billing_model->getActionPrice($p['skin_id'], $plan_id = 1, $action_type_id = 5, $p['user_id']);



        $chat_video_price_value = $billing_model->getActionPrice($p['skin_id'], $plan_id = 1, $action_type_id = 4, $p['user_id']);

*/

        return [

            'success' => TRUE,

            'userLastChat' => $userLastChat->girls,

            'userLastChatCount' => $userLastChat->count,

            'userActiveChat' => $userActiveChat->girls,

            'userActiveChatCount' => $userActiveChat->count,

            'userWinks' => $userWinks->girls,

            'userFavorite' => $userFavorite->girls,

        ];


    }


    public function actionGetUserInChat($otherUserID)

    {

        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }

        if ($user->id == $otherUserID) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'You can\'t chatting with yourself',

            ];

        }


        $otherUser = json_decode(JSON::encode($this->actionGetOtherUserInfo($otherUserID)));


        if (!empty($otherUser->userData->personalInfo->birthday)) {

            $tz = new \DateTimeZone('Europe/Brussels');

            $age = \DateTime::createFromFormat('Y-m-d', $otherUser->userData->personalInfo->birthday, $tz)
                ->diff(new \DateTime('now', $tz))
                ->y;

        } else {

            $age = null;

        }


        if ($otherUser->success == FALSE) {

            return [

                'success' => $otherUser->success,

                'code' => $otherUser->code,

                'message' => $otherUser->message,

            ];

        } else {

            return [

                'success' => TRUE,

                'user' => [

                    'first_name' => $otherUser->userData->personalInfo->first_name,

                    'birthday' => $otherUser->userData->personalInfo->birthday,

                    'age' => $age,

                    'id' => $otherUser->userData->personalInfo->id,
                    'height' => $otherUser->userData->personalInfo->height,
                    'weight' => $otherUser->userData->personalInfo->weight,
                    'online' => ($otherUser->userData->last_activity >= time() - 30) ? 1 : 0,

/*                    'country' => $otherUser->userData->location,

                    'city' => $otherUser->userData->city,

                    'marital' => $otherUser->userData->personalInfo->marital,*/

                    'cam_online' => $otherUser->userData->cam_online,

                    'small_avatar' => $otherUser->userData->avatar->small,

                    'medium_avatar' => $otherUser->userData->avatar->normal,

                    'inFavourite' => $otherUser->userData->in_favorite,

                ],

            ];

        }

    }


    public function actionCreateAlbum()

    {

        $title = Yii::$app->request->post('title');


        if (!isset($title) || trim($title) == "") {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params',

            ];

        }


        $otherUserID = Yii::$app->request->post('otherUserID');


        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;


        $otherUser = User::findOne(['id' => $otherUserID]);


        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN) {

            // check if agency have this user                        

            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();

            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {

                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to create album for user {$otherUser->id}");

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Access Denied',

                ];

            }

        }


        $public = Yii::$app->request->post('public');

        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN, User::USER_SUPERADMIN])) {

            if (isset($public) && filter_var($public, FILTER_VALIDATE_INT)) {

                $public = $public;

            } else {

                $public = null;

            }

        } else {

            $public = 1;

        }

        $description = Yii::$app->request->post('description');

        $description = (isset($description) && $description != '') ? trim($description) : 'standart album';


        $albumService = new AlbumService($otherUser->id);

        $result = $albumService->Create($public, trim($title), $description);

        if (!$result['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result['message'],

            ];

        }


        LogTable::addLog($user->id, $user->user_type, 'create/update photo album: ' . $title . ' user ID: ' . $otherUser->id);


        return [

            'success' => TRUE,

            'message' => 'album_id: ' . $result['album_id'],

        ];

    }


    public function actionGetAlbumsListInfo()

    {


        $otherUserID = Yii::$app->request->post('otherUserID');


        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;


        $otherUser = User::findOne(['id' => $otherUserID]);


        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN) {

            // check if agency have this user                        

            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();

            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {

                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to get albums from user {$otherUser->id}");

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Access Denied',

                ];

            }

        }


        $public = Yii::$app->request->post('public');

        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN, User::USER_SUPERADMIN])) {

            if (isset($public) && filter_var($public, FILTER_VALIDATE_INT)) {

                $public = $public;

            } else {

                $public = null;

            }

        } else {

            $public = 1;

        }


        $albumService = new AlbumService($otherUser->id);

        $albums = $albumService->getAlbumsListInfo($public);


        return [

            'success' => TRUE,

            'albums' => $albums,

        ];

    }


    public function actionGetUserVideo()

    {

        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        $otherUserID = Yii::$app->request->post('otherUserID');

        $otherUserID = (isset($otherUserID) && filter_var($otherUserID, FILTER_VALIDATE_INT)) ? $otherUserID : $user->id;


        $otherUser = User::findOne(['id' => $otherUserID]);


        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN) {

            // check if agency have this user                        

            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();

            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {

                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to get photo from user {$otherUser->id}");

                throw new ForbiddenHttpException('Access Denied', 10003);

            }

        }


        $videoList = $otherUser->getVideos()->addSelect(['video.id', 'path', 'video.description', 'video.status', 'video.public', 'video.premium', 'photo.medium_thumb', 'original_image'])->leftJoin(Photo::tableName(), 'video.poster = photo.id')->asArray()->all();

        $userInfo = $otherUser->getProfile()
            ->select(['id', 'first_name', 'last_name'])
            ->asArray()->one();


        return [

            'success' => TRUE,

            'videoList' => $videoList,

            'userInfo' => $userInfo,

        ];

    }


    public function actionLocation()

    {

        $p = Yii::$app->request->post();


        if (empty($p)) {

            return json_encode(['success' => FALSE, 'code' => 1, 'message' => 'missing params']);

        }


        if (!isset($p['user_id'], $p['country'], $p['type'])) {

            return json_encode(['success' => FALSE, 'code' => 1, 'message' => 'missing params']);

        }


        $location = ['country' => $p['country'], 'type' => $p['type']];


        if (isset($p['city'])) {

            $location['city'] = $p['city'];

        }


        if (isset($p['start'])) {

            $location['start'] = $p['start'];

        }


        if (isset($p['end'])) {

            $location['end'] = $p['end'];

        }


        return json_encode(Locations::Set($p['user_id'], $location));


    }


    public function actionSetUserToBlackList()

    {

        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 2,

                'message' => 'Token error',

            ];

        }


        $otherUserID = Yii::$app->request->post('otherUserID');


        if (!isset($otherUserID) || (!is_numeric($otherUserID) && !is_array($otherUserID))) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params',

            ];

        }

        if (is_numeric($otherUserID) && $otherUserID == $user->id) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'You can\'t add yourself to blacklist',

            ];

        }

        if (is_numeric($otherUserID)) {

            $otherUser = User::findOne(['id' => (int)$otherUserID]);


            $result2 = $this->actionCheckRights($user, $otherUser);


            if (!$result2['success']) {

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => $result2['message'],

                ];

            }

        }


        return $user->setToBlackList($otherUserID);

    }


    public function actionGetCitiesByLitera()

    {

        $p = Yii::$app->request->post();


        if (empty($p) || !isset($p['city']) || trim($p['city']) == '') {

            throw new BadRequestHttpException('Error post', 1);

        }


        if (strlen($p['city']) < 3) {

            throw new BadRequestHttpException('city should have more than 3 characters', 1);

        }


        return Locations::GetCitiesByLitera($p['city']);

    }


    public function actionCheckRights($user, $otherUser)

    {

        if (empty($user) || empty($otherUser)) {

            return [

                'success' => FALSE,

                'code' => 2,

                'message' => 'Access denied',

            ];

        }

        if (empty($user->status) || empty($user->owner_status) ||

            $user->status == User::STATUS_NO_ACTIVE || $user->status == User::STATUS_DELETED ||

            $user->owner_status == User::STATUS_NO_ACTIVE || $user->owner_status == User::STATUS_DELETED
        ) {

            return [

                'success' => FALSE,

                'code' => 2,

                'message' => 'User not active',

            ];

        }

        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_SUPERADMIN, User::USER_ADMIN])) {

            if ($otherUser->status == User::STATUS_NO_ACTIVE || $otherUser->status == User::STATUS_DELETED ||

                ($user->id != $otherUser->id && $otherUser->visible == User::STATUS_INVISIBLE) ||

                $otherUser->owner_status == User::STATUS_NO_ACTIVE || $otherUser->owner_status == User::STATUS_DELETED

            ) {

                return [

                    'success' => FALSE,

                    'code' => 2,

                    'message' => 'User not active',

                ];

            }

        }
        if ($user->user_type == User::USER_INTERPRETER && (in_array($otherUser->id, $user->user_ids) || $otherUser->id == $user->id)) {
            return [
                'success' => TRUE,
            ];
        }

        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN])) {

            // user without assosiated agency, users agency not match

            if ($user->agency_id == 1 || $user->agency_id != $otherUser->agency_id) {

                return [

                    'success' => FALSE,

                    'code' => 2,

                    'message' => 'Access denied',

                ];

            }

        } elseif (in_array($user->user_type, [User::USER_SUPERADMIN,User::USER_AFFILIATE]) || $user->id == $otherUser->id) {

            return [

                'success' => TRUE,

            ];

        } else {

            if (($user->user_type == $otherUser->user_type && $user->id != $otherUser->id) ||

                !in_array($user->user_type, [User::USER_MALE, User::USER_FEMALE]) ||

                !in_array($otherUser->user_type, [User::USER_MALE, User::USER_FEMALE])
            ) {

                return [

                    'success' => FALSE,

                    'code' => 2,

                    'message' => 'Access denied',

                ];

            }

        }

        return [

            'success' => TRUE,

        ];

    }


    public function actionSetPhotoStatus()

    {

        $otherUserID = Yii::$app->request->post('otherUserID');

        $token = Yii::$app->request->post('token');

        $photoID = Yii::$app->request->post('photoID');

        $status = Yii::$app->request->post('status');


        if (!isset($token) || trim($token) == "" ||

            !isset($photoID) || trim($photoID) == "" ||

            !isset($status) || !is_numeric($status)
        ) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params',

            ];

        }


        $result = json_decode(JSON::encode($this->actionCheckToken(['token' => $token])));


        if (!$result->success) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params',

            ];

        }

        $user = User::findIdentityByAccessToken($token);


        $otherUserID = (isset($otherUserID) && filter_var($otherUserID, FILTER_VALIDATE_INT)) ? $otherUserID : $user->id;


        $otherUser = User::findOne(['id' => $otherUserID]);


        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        $photoService = new PhotoService($otherUserID);


        $status = ($status == 0) ? Photo::STATUS_DELETED : Photo::STATUS_ACTIVE;


        $result = $photoService->setStatus($photoID, $status);


        if ($result['success']) {

            LogTable::addLog($user->id, $user->user_type, $user->id . ' set photo status ' . $status . ', to photo ID: ' . $photoID);

        }


        return $result;

    }


    public function actionEditInfo()

    {

        $postArray = Yii::$app->request->post();


        if (empty($postArray) || !isset($postArray['token']) || trim($postArray['token']) == "") {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params',

            ];

        }


        $result = json_decode(JSON::encode($this->actionCheckToken(['token' => $postArray['token']])));


        if (!$result->success) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params',

            ];

        }


        $user = User::findIdentityByAccessToken($postArray['token']);

        $currentUserType = $user->user_type;


        $otherUserID = isset($postArray['otherUserID']) && is_numeric($postArray['otherUserID']) ? $postArray['otherUserID'] : $user->id;


        $otherUser = User::findOne(['id' => $otherUserID]);
        if (in_array($user->user_type, [User::USER_ADMIN, User::USER_SITEADMIN]) && ($otherUserID != $user->id) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }

        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        if (empty($postArray['email'])) {

            return [

                'success' => FALSE,

                'message' => 'email can`t be empty',

            ];

        }


        if (!in_array($otherUser->user_type, [User::USER_AGENCY,User::USER_AFFILIATE])) {

            $otherUserProfile = UserProfile::findOne(['id' => $otherUserID]);


            if (empty($otherUserProfile)) {

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'User not found',

                ];

            }
            if (!empty($postArray['birthday']) && (time() < strtotime('+18 years', strtotime($postArray['birthday'])))) {
               return [
                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Check your birthday. You have to be at least 18 years old.',
               ];
            }

            $fields = [

                'user_id' => $otherUserID,

                'first_name' => Html::encode($postArray['first_name']),

                'last_name' => (!empty($postArray['last_name'])) ? Html::encode($postArray['last_name']) : '',

                'phone'   => (!empty($postArray['phone'])) ? Html::encode($postArray['phone']) : '',
                'm_phone' => (!empty($postArray['m_phone'])) ? Html::encode($postArray['m_phone']) : '',

                'birthday' => (!empty($postArray['birthday']) && strtotime($postArray['birthday'])) ? date("Y-m-d", strtotime($postArray['birthday'])) : null,

                'sex' => (!empty($postArray['sex'])) ? $postArray['sex'] : User::USER_FEMALE,

                'religion' => $postArray['religion'],

                'marital' => $postArray['marital'],

                'kids' => $postArray['kids'],

                'height' => $postArray['height'],

                'weight' => $postArray['weight'],

                'eyes' => $postArray['eyes'],
                'physique' => $postArray['physique'],

                'hair' => $postArray['hair'],

                'smoking' => $postArray['smoking'],

                'alcohol' => $postArray['alcohol'],

                'ethnos' => (!empty($postArray['ethnos'])) ? $postArray['ethnos'] : 1,

                'passport' => (!empty($postArray['passport'])) ? Html::encode($postArray['passport']) : null,

                'education' => $postArray['education'],

                'occupation' => (!empty($postArray['occupation'])) ? Html::encode($postArray['occupation']) : null,

                'english_level' => $postArray['english_level'],

                'looking_age_from' => $postArray['looking_age_from'],

                'looking_age_to' => $postArray['looking_age_to'],

                'about_me' => Html::encode($postArray['about_me']),

                'hobbies' => Html::encode($postArray['hobbies']),

                'my_ideal' => Html::encode($postArray['my_ideal']),

                'address' => Html::encode($postArray['address']),

                'other_language' => Html::encode($postArray['other_language']),

            ];

            $otherUserProfile->load(['UserProfile' => $fields]);


            if (!$otherUserProfile->save()) {

                $otherUserProfile->validate();

                return [

                    'success' => FALSE,

                    'code' => 'Validation errors',

                    'errors' => $otherUserProfile->errors,

                ];

            }

        }


        if (empty($otherUser)) {

            return [

                'success' => FALSE,

                'message' => 'user not found',

            ];

        }


        $otherUser->email = $postArray['email'];


        if (in_array($currentUserType, [User::USER_AGENCY, User::USER_SUPERADMIN]) && $otherUser->user_type == User::USER_AGENCY) {

            // if agency change profile, set agency_status to "in progress"

            $agency = $otherUser->getAgency()->where(['!=', 'id', 1])->one();

            if (!empty($agency)) {

                $agency->name = Html::encode($postArray['agency_name']);

                $agency->contact_person = Html::encode($postArray['contact_person']);
                $agency->director_address = Html::encode($postArray['director_address']);
                $agency->skype = Html::encode($postArray['skype']);
                $agency->bank_detail = Html::encode($postArray['bank_detail']);

                //$agency->passport_number = $postArray['passport_number'];

                $agency->office_address = Html::encode($postArray['office_address']);

                $agency->office_phone = Html::encode($postArray['office_phone']);
                $agency->m_phone      = Html::encode($postArray['m_phone']);

                //$agency->card_number = $postArray['card_number'];

                //$agency->card_user_name = $postArray['card_user_name'];

                //$agency_model->card_date_end  = $card_date_end;

                $agency->approve_status = ($currentUserType == User::USER_SUPERADMIN) ? Html::encode($postArray['agency_approve_status']) : 2; //default in progress

                $otherUser->owner_status = User::STATUS_WAIT_FOR_APPROVE;

                if (!$agency->save() || !$otherUser->save()) {

                    return [

                        'success' => FALSE,

                        'message' => 'error save agency person',

                    ];

                }

            } else {

                return [

                    'success' => FALSE,

                    'message' => 'Wrong agency',

                ];

            }

        } elseif ($user->user_type == User::USER_FEMALE) { // if girls change profile than change her status to "in progress"

            $otherUser->approve_status_id = 2;

        }

        //for affiliates
        if (in_array($currentUserType, [User::USER_AFFILIATE, User::USER_SUPERADMIN]) && $otherUser->user_type == User::USER_AFFILIATE) {
            $affiliates = $otherUser->getAffiliates()->one();

            if (!empty($affiliates)) {
                $affiliates->name = Html::encode($postArray['first_name']);
                $affiliates->m_phone = Html::encode($postArray['m_phone']);
                if (!$affiliates->save()) {
                    return [
                        'success' => FALSE,
                        'message' => 'error save agency person',
                    ];
                }
            }
        }

        //for admin permissions
        if (in_array($currentUserType, [User::USER_AGENCY, User::USER_SUPERADMIN]) && $otherUser->user_type == User::USER_ADMIN) {
            $newPermissions = Yii::$app->params['admin_permissions'];            
            if (!empty($postArray['permissions']) && json_decode($postArray['permissions'])) {
                $modifiedPermissions = [];
                $permissions = json_decode($postArray['permissions']);
                foreach ($permissions as $access => $permission) {
                    foreach ($newPermissions as $newAccess => $newPermission) {
                        if ($access == $newAccess) {                            
                            $modifiedPermissions[$newAccess] = ($permission == AdminPermissions::ALLOW) ? AdminPermissions::ALLOW : AdminPermissions::DENY;
                        }
                    }
                }
            } else {
                $modifiedPermissions = [];
                foreach ($newPermissions as $newAccess => $newPermission) {
                    $modifiedPermissions[$newAccess] = ($newPermission['value']== AdminPermissions::ALLOW) ? AdminPermissions::ALLOW : AdminPermissions::DENY;
                }
            }
            if (!empty($otherUser->permissions)) {
                $adminPermissions = $otherUser->permissions;
                $adminPermissions->permissions = json_encode($modifiedPermissions);
            } else {
                $adminPermissions = new AdminPermissions();
                $adminPermissions->id = $otherUser->id;
                $adminPermissions->permissions = json_encode($modifiedPermissions);
            }
            
            if (!$adminPermissions->save()) {
                return [

                    'success' => FALSE,

                    'message' => 'Error during admin save',

                ];
            }
        }

        //for siteadmin permissions
        if (in_array($currentUserType, [User::USER_SUPERADMIN]) && $otherUser->user_type == User::USER_SITEADMIN) {
            $newPermissions = Yii::$app->params['siteadmin_permissions'];            
            if (!empty($postArray['permissions']) && json_decode($postArray['permissions'])) {
                $modifiedPermissions = [];
                $permissions = json_decode($postArray['permissions']);
                foreach ($permissions as $access => $permission) {
                    foreach ($newPermissions as $newAccess => $newPermission) {
                        if ($access == $newAccess) {                            
                            $modifiedPermissions[$newAccess] = ($permission == AdminPermissions::ALLOW) ? AdminPermissions::ALLOW : AdminPermissions::DENY;
                        }
                    }
                }
            } else {
                $modifiedPermissions = [];
                foreach ($newPermissions as $newAccess => $newPermission) {
                    $modifiedPermissions[$newAccess] = ($newPermission['value']== AdminPermissions::ALLOW) ? AdminPermissions::ALLOW : AdminPermissions::DENY;
                }
            }
            if (!empty($otherUser->permissions)) {
                $adminPermissions = $otherUser->permissions;
                $adminPermissions->permissions = json_encode($modifiedPermissions);
            } else {
                $adminPermissions = new AdminPermissions();
                $adminPermissions->id = $otherUser->id;
                $adminPermissions->permissions = json_encode($modifiedPermissions);
            }
            
            if (!$adminPermissions->save()) {
                return [

                    'success' => FALSE,

                    'message' => 'Error during admin save',

                ];
            }
        }

        if ($currentUserType == User::USER_SUPERADMIN) {

            $otherUser->approve_status_id = $postArray['approve_status'];

        }

        if (in_array($currentUserType, [User::USER_AGENCY, User::USER_SUPERADMIN, User::USER_ADMIN])) {

            // if agency or admin then change status

            if (!($currentUserType == User::USER_AGENCY && $user->id == $otherUser->id)) {

                $otherUser->status = $postArray['user_active_status'];

            }

            if ($otherUser->user_type == User::USER_AGENCY && $currentUserType != User::USER_AGENCY) {

                $otherUser->owner_status = $postArray['user_active_status'];

            }

            if ($otherUser->user_type == User::USER_AGENCY && in_array($currentUserType, [User::USER_AGENCY, User::USER_SUPERADMIN])) {

                if (!empty($agency)) {

                    $userIDs = $agency->getUsersIDs();

                    if (!empty($userIDs)) {

                        $result = User::updateAll(['owner_status' => $postArray['user_active_status']], ['in', 'id', $userIDs]);

                    }

                }

            }

        }

        if (in_array($currentUserType, [User::USER_AGENCY, User::USER_SUPERADMIN, User::USER_ADMIN]) && $otherUser->user_type == User::USER_INTERPRETER && !empty($postArray['girls_ids']) && is_array($postArray['girls_ids'])) {//если меняем переводчику профиль проверяем наличие связей с девочками
            $dbConnection = Yii::$app->db;
            $transaction  = $dbConnection->beginTransaction();
            try {
                $dbConnection->createCommand()
                    ->delete(
                        UserInterpreter::tableName(),
                        ['interpreter_id' => $otherUser->id]
                    )
                    ->execute();
                $rows = [];
                foreach ($postArray['girls_ids'] as $id) {
                    $rows[] = [$id, $otherUser->id];
                }
                $dbConnection->createCommand()
                    ->batchInsert(UserInterpreter::tableName(), ['user_id', 'interpreter_id'], $rows)->execute();
                $transaction->commit();

            } catch (Exception $ex) {
                $transaction->rollBack();
                throw $ex;
            }
            //$otherUser->user_ids = $postArray['girls_ids'];            

        }


        if ($currentUserType == User::USER_FEMALE && $user->id == $otherUser->id) {

            //if girl change profile than change her status to not active

            $otherUser->status = User::STATUS_NO_ACTIVE;

        }

        if (!$otherUser->save()) {

            $otherUser->validate();

            return [

                'success' => FALSE,

                'errors' => $otherUser->errors,

                'message' => 'error save user',

            ];

        }


        if (!empty($postArray['country'])) {

            $location = $otherUser->getLocation()->one();

            if (!empty($location)) {

                $location->country_id = (int)$postArray['country'];

            } else {

                $location = new LocationsModel();

                $location->user_id = $otherUser->id;

                $location->location_type_id = 1;

                $location->country_id = (int)$postArray['country'];

            }

            $cityName = (!empty($postArray['city'])) ? $postArray['city'] : "";

            $city = City::find()->where(['name' => $cityName])->one();

            if (!empty($city)) {

                $city->name = $cityName;

            } else {

                $city = new City();

                $city->name = $cityName;

                $city->proxy = 0;

                $city->original = 0;

                if (!$city->save()) {

                    return [

                        'success' => FALSE,

                        'code' => 1,

                        'message' => 'Error during city save',

                    ];

                }

            }


            $location->city_id = $city->id;


            if (!$location->save()) {

                return [

                    'success' => FALSE,

                    'message' => 'Error during location save',

                ];

            }

        }


        LogTable::addLog($user->id, $user->user_type, 'User ' . $user->id . ' update profile user ID: ' . $otherUser->id);


        return [

            'success' => TRUE,

            'message' => 'Profile updated',

        ];

    }


    public function actionGetUserWinks()

    {

        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        $params = [

            'type' => 'winks',

            'userType' => $user->user_type,

            'userID' => $user->id,

            'skinID' => 1,

            'limit' => 20,

        ];

        $girlsOnline = json_decode($this->actionGetUsers($params));


        return $girlsOnline;


    }


    public function actionSendWink()

    {

        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }

        $otherUserID = Yii::$app->request->post('otherUserID');

        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;


        $otherUser = User::findOne(['id' => $otherUserID]);


        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        //условие на ошибку отрабатывает если пользователей нету, одинаковые пол, только мальчик и девочка

        if (!$user->user_type || !$otherUser->user_type || $user->user_type == $otherUser->user_type || !in_array($user->user_type, [User::USER_MALE, User::USER_FEMALE]) || !in_array($otherUser->user_type, [User::USER_MALE, User::USER_FEMALE])) {

            return [

                'success' => FALSE,

                'code' => 2,

                'message' => 'Access denied',

            ];

        }


        if ($user->inBlackList($otherUser->id)) {

            return [

                'success' => FALSE,

                'code' => 2,

                'message' => 'User add your id in black list',

            ];

        }


        $result = $user->updateWinks($otherUser->id);


        return $result;

    }


    public function actionReadWinks()

    {

        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }

        $otherUserID = Yii::$app->request->post('otherUserID');

        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;


        $otherUser = User::findOne(['id' => $otherUserID]);


        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        $result = $user->readWinks($otherUser->id);


        return $result;


    }


    public function actionUpdateVideoDescription()

    {

        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }

        $otherUserID = Yii::$app->request->post('otherUserID');

        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;


        $otherUser = User::findOne(['id' => $otherUserID]);
        if (in_array($user->user_type, [User::USER_ADMIN, User::USER_SITEADMIN]) && ($otherUserID != $user->id) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }

        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN) {

            // check if agency have this user                        

            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();

            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {

                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to update video description for user {$otherUser->id}");

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Access Denied',

                ];

            }

        }


        $videoDescription = Yii::$app->request->post('videoDescription');

        if ($videoDescription != null) {

            $videoDescription = Html::encode($videoDescription);

        }

        $videoStatus = Yii::$app->request->post('status');

        $publicVideo = Yii::$app->request->post('public');

        $premiumVideo = Yii::$app->request->post('premium');

        $videoID = Yii::$app->request->post('videoID');


        if (empty($videoID) || !filter_var($videoID, FILTER_VALIDATE_INT)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Video not found',

            ];

        }


        $videoStatus = isset($videoStatus) && filter_var($videoStatus, FILTER_VALIDATE_INT) ? $videoStatus : ApproveStatus::STATUS_NOT_APPROVED;

        $publicVideo = isset($publicVideo) && filter_var($publicVideo, FILTER_VALIDATE_INT) ? $publicVideo : 0;

        $premiumVideo = isset($premiumVideo) && filter_var($premiumVideo, FILTER_VALIDATE_INT) ? $premiumVideo : 0;


        $result = $otherUser->updateVideoDescription((int)$videoID, $videoDescription, $videoStatus, $publicVideo, $premiumVideo);


        return $result;

    }


    public function actionGetOtherUserInfo($otherUserID = "")

    {

        if (empty($otherUserID)) {

            $otherUserID = Yii::$app->request->post('otherUserID');

        }


        if (!isset($otherUserID) || !filter_var($otherUserID, FILTER_VALIDATE_INT)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params',

            ];

        }


        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 2,

                'message' => 'Token error',

            ];

        }

        if (in_array($user->user_type, [User::USER_ADMIN, User::USER_SITEADMIN]) && ($otherUserID != $user->id) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }
        $otherUser = User::findOne(['id' => $otherUserID]);


        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        if ($user->inBlackList($otherUserID)) {

            return [

                'success' => FALSE,

                'code' => 2,

                'message' => 'User add your id in black list',

            ];

        }


        //get avatar
        $userAvatar = Photo::findOne(['id' => $otherUser->avatar_photo_id]);

        if (in_array($otherUser['user_type'], [User::USER_AGENCY])) {
            $userData['agencyRuleViolations'] = $otherUser->agency->violations;
            $userData['country'] = (!empty($otherUser->location) && !empty($otherUser->location->country)) ? $otherUser->location->country->name : 0;

            if (!empty($otherUser->location)) {

                $city = City::findOne(['id' => $otherUser->location->city_id]);

            }

            $userData['city'] = (!empty($city->name)) ? $city->name : '';
        }
        if (in_array($otherUser['user_type'], [User::USER_AGENCY, User::USER_ADMIN])) {

            $userAgency = $otherUser->getAgency()->asArray()->one();

            if (!empty($userAgency)) {

                $userData['avatar']['small']  = ($userAvatar) ? $userAvatar['small_thumb'] : null;
                $userData['avatar']['normal'] = ($userAvatar) ? $userAvatar['medium_thumb'] : null;
                $userData['agency_info']      = $userAgency;
                $userData['user_type']        = $otherUser['user_type'];
                $userData['userID']           = $otherUserID;
                $userData['email']            = $otherUser['email'];
                $userData['approve_status']        = $otherUser['approve_status_id'];
                if ($otherUser['user_type'] == User::USER_ADMIN) {
                    if (!empty($otherUser->profile)) {
                        $userData['first_name'] = $otherUser->profile->first_name;
                        $userData['last_name'] = $otherUser->profile->last_name;
                        $userData['sex'] = $otherUser->profile->sex;
                    }
                    $userData['permissions'] = $this->getPermissions($otherUser->permissions, $otherUser['user_type']);
                }
                return [

                    'success' => TRUE,

                    'userData' => $userData,

                ];

            }

        }elseif($otherUser['user_type'] == User::USER_AFFILIATE){
            $userAffiliates = $otherUser->getAffiliates()->asArray()->one();
            if (!empty($userAffiliates)) {
                $userData['avatar']['small']  = ($userAvatar) ? $userAvatar['small_thumb'] : null;
                $userData['avatar']['normal'] = ($userAvatar) ? $userAvatar['medium_thumb'] : null;
                $userData['affiliates_info']      = $userAffiliates;
                $userData['user_type']        = $otherUser['user_type'];
                $userData['approve_status']        = $otherUser['approve_status_id'];
                $userData['userID']           = $otherUserID;
                $userData['email']            = $otherUser['email'];

                return [
                    'success' => TRUE,
                    'userData' => $userData,
                ];
            }
        }        
        if ($user->user_type == User::USER_SUPERADMIN && $otherUser['user_type'] == User::USER_SITEADMIN) {
            $userData['avatar']['small']  = ($userAvatar) ? $userAvatar['small_thumb'] : null;
            $userData['avatar']['normal'] = ($userAvatar) ? $userAvatar['medium_thumb'] : null;
            $userData['user_type']        = $otherUser['user_type'];
            $userData['userID']           = $otherUserID;
            $userData['email']            = $otherUser['email'];
            $userData['approve_status']        = $otherUser['approve_status_id'];
            if (!empty($otherUser->profile)) {
                $userData['first_name'] = $otherUser->profile->first_name;
                $userData['last_name'] = $otherUser->profile->last_name;
                $userData['sex'] = $otherUser->profile->sex;
            }
            $userData['permissions'] = $this->getPermissions($otherUser->permissions, $otherUser['user_type']);
            return [
                    'success' => TRUE,
                    'userData' => $userData,
            ];
        }

        $userProfile = UserProfile::findOne(['id' => $otherUserID]);


        if (empty($userProfile)) {

            return [

                'success' => FALSE,

                'code' => 2,

                'message' => 'User not found',

            ];

        }

        //get avatar

        $userInFavorite = UserFavoriteList::findOne(['user_id' => $user->id, 'favorite_user_id' => $otherUser->id]);

        $userInFavoriteStatus = (!empty($userInFavorite)) ? 1 : null;

        $userInBlacklist = UserBlacklist::findOne(['user_id' => $user->id, 'blacklist_user_id' => $otherUser->id]);
        $userBlacklistStatus = (!empty($userInBlacklist)) ? 1 : null;


        $userData = [];

        $userData['personalInfo']['eyes'] = (!empty($userProfile->eyes)) ? $userProfile->eyesColor->name : null;

        $userData['personalInfo']['hair'] = (!empty($userProfile->hair)) ? $userProfile->hairColor->name : null;

        $userData['personalInfo']['birthday'] = $userProfile->birthday;

        $userData['personalInfo']['height'] = $userProfile->height;

        $userData['personalInfo']['weight'] = $userProfile->weight;

        $userData['personalInfo']['marital'] = $userProfile->marital;

        $userData['personalInfo']['children'] = $userProfile->kids;

        $userData['personalInfo']['smoking'] = $userProfile->smoking;

        $userData['personalInfo']['english_level'] = $userProfile->english_level;

        $userData['personalInfo']['about_me'] = $userProfile->about_me;

        $userData['personalInfo']['my_ideal'] = $userProfile->my_ideal;

        $userData['personalInfo']['first_name'] = $userProfile->first_name;

        $userData['personalInfo']['religion'] = $userProfile->religion;

        $userData['personalInfo']['last_name'] = $userProfile->last_name;

        $userData['personalInfo']['id'] = $userProfile->id;


        $userData['avatar']['small'] = ($userAvatar) ? $userAvatar['small_thumb'] : null;

        $userData['avatar']['normal'] = ($userAvatar) ? $userAvatar['medium_thumb'] : null;

        $userData['location'] = (!empty($otherUser->location)) ? $otherUser->location->country->name : 'Not defined';

        $userData['created_at'] = $otherUser->created_at;

        if (!empty($otherUser->location)) {

            $city = City::findOne(['id' => $otherUser->location->city_id]);

        }

        $userData['city']          = (!empty($city->name)) ? $city->name : 'Not defined';
        $albumService              = new AlbumService($otherUser->id);
        $userData['albums']        = $albumService->getAlbumsListInfo(1);
        $userData['video_albums']  = $albumService->getUserVideoList(1);
        $userData['last_activity'] = $otherUser->last_activity;
        $userData['cam_online']    = $otherUser->cam_online;
        $userData['in_favorite']   = $userInFavoriteStatus;
        $userData['in_blacklist']  = $userBlacklistStatus;
        $userData['user_type']     = $otherUser['user_type'];


        if ($user->user_type == User::USER_AGENCY || $user->user_type == User::USER_ADMIN || $user->user_type == User::USER_SUPERADMIN) {

            $userData['email'] = $otherUser->email;

            $userData['personalInfo']['hobbies'] = $userProfile->hobbies;

            $userData['personalInfo']['phone']   = $userProfile->phone;
            $userData['personalInfo']['m_phone'] = $userProfile->m_phone;
            

            $userData['personalInfo']['education'] = $userProfile->education;
            $userData['personalInfo']['deactivation_reason'] = $userProfile->deactivation_reason;
            $userData['personalInfo']['deactivation_time'] = $userProfile->deactivation_time;

        }


        if (in_array($user->user_type, [User::USER_MALE, User::USER_FEMALE])) {

            $user->setVisitor($otherUser->id);

        }


        $params = [

            'limit' => 12,

            'userType' => ($user->user_type == User::USER_MALE) ? User::USER_FEMALE : User::USER_MALE,

            'skinID' => 1,

        ];


        $params['type'] = 'single';

        $params['statusParams'] = 'online';


        $singleOnlineGirls = json_decode(JSON::encode($this->actionGetUsers($params)));


        return [

            'success' => TRUE,

            'userData' => $userData,

            'singleOnlineGirls' => $singleOnlineGirls,

        ];

    }


    public function actionGetGirlsByParams($parameters = null)

    {


        if ($parameters == null) {

            $parameters = Yii::$app->request->post();

            if (isset($parameters['orderBy'])) {

                if ($parameters['orderBy'] == 'created_at') {

                    $parameters['order'] = "user.created_at";

                } elseif ($parameters['orderBy'] == 'last_activity') {

                    $parameters['order'] = "user.last_activity";

                }

                if (isset($parameters['order']) && isset($parameters['direction']) && $parameters['direction'] == 'DESC') {

                    $parameters['order'] = $parameters['order'] . ' DESC';

                } else {

                    $parameters['order'] = $parameters['order'] . ' ASC';

                }

                unset($parameters['orderBy']);

                unset($parameters['direction']);

            }

        }


        if (empty($parameters) || !isset($parameters['skinID']) || !filter_var($parameters['skinID'], FILTER_VALIDATE_INT)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params',

            ];

        }


        $userType = (isset($parameters['userType']) && filter_var($parameters['userType'], FILTER_VALIDATE_INT))

            ? $parameters['userType'] : User::USER_FEMALE;

        $userID = (isset($parameters['userID']) && filter_var($parameters['userID'], FILTER_VALIDATE_INT))

            ? $parameters['userID'] : null;

        $agencyID = (isset($parameters['agencyID']) && filter_var($parameters['agencyID'], FILTER_VALIDATE_INT))

            ? $parameters['agencyID'] : null;

        $limit = (isset($parameters['limit']) && filter_var($parameters['limit'], FILTER_VALIDATE_INT))

            ? $parameters['limit'] : null;

        $offset = (isset($parameters['offset']) && filter_var($parameters['offset'], FILTER_VALIDATE_INT))

            ? $parameters['offset'] : null;

        $order = (isset($parameters['order'])) ? $parameters['order'] : null;

        $type = (isset($parameters['type'])) ? $parameters['type'] : null;

        $params = (isset($parameters['params'])) ? $parameters['params'] : null;

        $statusParams = (isset($parameters['statusParams']) && $parameters['statusParams'] != '') ? $parameters['statusParams'] : null;

        $status = (isset($parameters['status']) && filter_var($parameters['status'], FILTER_VALIDATE_INT)) ? $parameters['status'] : User::STATUS_ACTIVE;

        $ownerActive = (isset($parameters['owner_active'])) ? $parameters['owner_active'] : User::STATUS_ACTIVE;

        $visibility = (isset($parameters['visibility'])) ? $parameters['visibility'] : 'visible';

        $notRemoveStatus = (isset($parameters['notRemoveStatusParam'])) ? $parameters['notRemoveStatusParam'] : TRUE;

        $statusArray = [

            'all_count' => 0,

            'active_count' => 0,

            'no_active_count' => 0,

            'invisible' => 0,

            'new' => 0,

            'approve_inprogress' => 0,

            'approve_decline' => 0,

            'online' => 0,

        ];

        //return $parameters;
        $query = new Query();

        $userModel = $query->addSelect(['user.id', 'user.status', 'user.approve_status_id', 'user.last_activity', 'user.created_at', 'user.cam_online', 'user.fake', 'user.visible', 'user_profile.*', 'photo.small_thumb', 'photo.medium_thumb'])
            ->from(User::tableName())
            ->leftJoin(UserProfile::tableName(), 'user.id = user_profile.id')
            ->leftJoin(Photo::tableName(), 'user.avatar_photo_id = photo.id')
            ->where(['user.user_type' => $userType]);

        if ($status && $notRemoveStatus) {

            $userModel->andWhere(['user.status' => $status, 'user.approve_status_id' => 4]);

        }

        if ($ownerActive) {

            $userModel->andWhere(['user.owner_status' => $ownerActive]);

        }


        if ($type !== null) {

            switch ($type) {

                case 'single':

                    $userModel->andWhere(['user_profile.marital' => 1]);

                    break;

                case 'new':

                    $userModel->andWhere(['>=', 'user.created_at', date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 30)]);

                    break;

                case 'last_activity':

                    // less than 30 seconds ago
                    if (isset(Yii::$app->user->identity->id)) {
                        $subQuery = Chat::find()
                                    ->select('COUNT(action.action_creator) AS unreaded_messages')
                                    ->addSelect(['action.action_creator']) 
                                    ->leftJoin(Action::tableName(), 'action.id = chat.action_id')                
                                    ->where(['chat.readed_at' => null])                                    
                                    ->andWhere(['action_receiver' => Yii::$app->user->identity->id])                       
                                    ->groupBy('action.action_creator');
                        $userModel->leftJoin(['messages' => $subQuery], 'user.id = messages.action_creator')->orderBy('unreaded_messages DESC'); 
                    }                    
                    $userModel->andWhere(['>=', 'user.last_activity', time() - 30]);

                    break;

                case 'top':

                    $topUsers = ($userType == User::USER_MALE) ? Yii::$app->params['men_top'] : Yii::$app->params['girls_top'];

                    $userModel->andWhere(['in', 'user.id', $topUsers]);

                    break;

                case 'guest':

                    if (empty($userID))

                        break;

                    $guestIDs = User::getGuest($userID);

                    if (empty($guestIDs))

                        return [

                            'success' => TRUE,

                            'girls' => null,

                            'count' => 0,

                            'statusArray' => $statusArray,

                        ];

                    $userModel->leftJoin(UserVisiters::tableName(), 'user.id = user_visiters.user_id')
                        ->andWhere(['user_visiters.visited_user_id' => $userID]);

                    $order = 'user_visiters.visit_datetime DESC';

                    break;


                case 'recent':

                    if (empty($userID))

                        break;

                    $recentIDs = User::getRecent($userID);

                    if (empty($recentIDs))

                        return [

                            'success' => TRUE,

                            'girls' => null,

                            'count' => 0,

                            'statusArray' => $statusArray,

                        ];

                    $userModel->leftJoin(UserVisiters::tableName(), 'user.id = user_visiters.visited_user_id')
                        ->andWhere(['user_visiters.user_id' => $userID]);

                    $order = 'user_visiters.visit_datetime DESC';

                    break;


                case 'favorite':

                    if (empty($userID))

                        break;

                    $favoriteIDs = User::getFavorite($userID);

                    if (empty($favoriteIDs))

                        return [

                            'success' => TRUE,

                            'girls' => null,

                            'count' => 0,

                            'statusArray' => $statusArray,

                        ];

                    $userModel->andWhere(['in', 'user.id', $favoriteIDs]);

                    break;


                case 'admires':

                    if (empty($userID))

                        break;

                    $admiresIDs = User::getAdmires($userID);

                    if (empty($admiresIDs))

                        return JSON::encode(['success' => TRUE, 'girls' => null, 'count' => 0, 'statusArray' => $statusArray]);

                    $userModel->andWhere(['in', 'user.id', $admiresIDs]);

                    break;


                case 'matches':

                    if (empty($userID))

                        break;

                    $matchesIDs = User::getMatches($userID);

                    if (empty($matchesIDs))

                        return JSON::encode(['success' => TRUE, 'girls' => null, 'count' => 0, 'statusArray' => $statusArray]);

                    $userModel->andWhere(['in', 'user.id', $matchesIDs]);

                    break;


                case 'birthday': // на ближайший месяц

                    $userModel->andWhere('MONTH(user_profile.birthday) = MONTH(CURRENT_DATE) 

                        AND DAYOFMONTH(user_profile.birthday) > DAYOFMONTH(CURRENT_DATE) 

                        OR MONTH(user_profile.birthday) = IF(MONTH(CURRENT_DATE)+1 = 13,1,MONTH(CURRENT_DATE)+1) 

                        AND DAYOFMONTH(user_profile.birthday) < DAYOFMONTH(CURRENT_DATE)');

                    break;

                case 'winks':

                    if (empty($userID))

                        break;

                    $userModel->leftJoin(UserWink::tableName(), 'user.id = user_wink.user_creator')
                        ->andWhere(['user_wink.user_receiver' => $userID])
                        ->addSelect(['user_wink.viewed_at']);

                    $order = 'user_wink.created_at DESC';

                    break;

                case 'last_chat':

                    if (empty($userID))

                        break;

                    $subQuery = (new Query)
                        ->from([ChatSession::tableName()]);

                    if ($userType == User::USER_FEMALE) {

                        $subQuery->select('action_creator as other_user_id, max(chat_session.updated_at) as maxtime')
                            ->join('LEFT JOIN', 'chat', 'chat.chat_session_id = chat_session.id')
                            ->join('LEFT JOIN', 'action', 'action.id = chat.action_id')
                            ->join('LEFT JOIN', 'action_type', 'action.action_type_id = action_type.id')
                            ->where(['action_receiver' => $userID])
                            ->andWhere(['action_type.name' => 'chat']);

                        $subQuery1 = (new Query)
                            ->from([ChatSession::tableName()])
                            ->select('action_receiver as other_user_id, max(chat_session.updated_at) as maxtime')
                            ->join('LEFT JOIN', 'chat', 'chat.chat_session_id = chat_session.id')
                            ->join('LEFT JOIN', 'action', 'action.id = chat.action_id')
                            ->join('LEFT JOIN', 'action_type', 'action.action_type_id = action_type.id')
                            ->where(['action_creator' => $userID])
                            ->andWhere(['action_type.name' => 'chat']);

                    } else {

                        $subQuery->select('action_creator as other_user_id, max(chat_session.updated_at) as maxtime')
                            ->join('LEFT JOIN', 'chat', 'chat.chat_session_id = chat_session.id')
                            ->join('LEFT JOIN', 'action', 'action.id = chat.action_id')
                            ->join('LEFT JOIN', 'action_type', 'action.action_type_id = action_type.id')
                            ->where(['action_receiver' => $userID])
                            ->andWhere(['action_type.name' => 'chat']);

                        $subQuery1 = (new Query)
                            ->from([ChatSession::tableName()])
                            ->select('action_receiver as other_user_id, max(chat_session.updated_at) as maxtime')
                            ->join('LEFT JOIN', 'chat', 'chat.chat_session_id = chat_session.id')
                            ->join('LEFT JOIN', 'action', 'action.id = chat.action_id')
                            ->join('LEFT JOIN', 'action_type', 'action.action_type_id = action_type.id')
                            ->where(['action_creator' => $userID])
                            ->andWhere(['action_type.name' => 'chat']);

                    }

                    $subQuery->orderBy('maxtime DESC')
                        ->groupBy('other_user_id');

                    $subQuery->count();

                    $subQuery1->orderBy('maxtime DESC')
                        ->groupBy('other_user_id');

                    $subQuery1->count();

                    if ($subQuery1->count() > $subQuery->count()) {

                        $subQuery = $subQuery1;

                    }

                    $userModel->innerJoin(['chat_session_user' => $subQuery], 'chat_session_user.other_user_id = user.id');
                    if (isset(Yii::$app->user->identity->id)) {
                        $subQuery = Chat::find()
                                    ->select('COUNT(action.action_creator) AS unreaded_messages')
                                    ->addSelect(['action.action_creator']) 
                                    ->leftJoin(Action::tableName(), 'action.id = chat.action_id')                
                                    ->where(['chat.readed_at' => null])                                    
                                    ->andWhere(['action_receiver' => Yii::$app->user->identity->id])                       
                                    ->groupBy('action.action_creator');
                        $userModel->leftJoin(['messages' => $subQuery], 'user.id = messages.action_creator')->orderBy('unreaded_messages DESC'); 
                    }

                    $order = 'maxtime DESC';

                    break;


                case 'active_chat':

                    if (empty($userID))

                        break;

                    //$users_active_chat_ids = (new User($user_id))->chats->getActiveChatUsersIds();

                    $subQuery = (new Query)
                        ->from([ChatSession::tableName()]);

                    if ($userType == User::USER_FEMALE) {

                        $subQuery
                            ->select('user_to as other_user_id, max(date_update) as maxtime')
                            ->where(['user_from' => $userID]);

                    } elseif ($userType == User::USER_MALE) {

                        $subQuery
                            ->select('user_from as other_user_id, max(date_update) as maxtime')
                            ->where(['user_to' => $userID]);

                    }

                    $dateFormat = time() - 30 * 60; // 30 minutes ago

                    $subQuery
                        ->andWhere(['status' => User::STATUS_ACTIVE])
                        ->addSelect('date_update')
                        ->groupBy('other_user_id')
                        ->andWhere(['>=', 'date_update', date("Y-m-d H:i:s", $dateFormat)]);

                    $userModel->leftJoin(['chat_session_user' => $subQuery], 'chat_session_user.other_user_id = users.user_id');

                    $userModel->andWhere('chat_session_user.other_user_id IS NOT NULL');

                    $userModel->addSelect('chat_session_user.date_update as session_other_user_id');

                    $order = 'maxtime DESC';

                    break;


                case 'blacklist':

                    if (empty($userID))

                        break;


                    $userModel0 = User::findOne(['id' => $userID]);

                    $blackListIDs = $userModel0->getUserBlacklists0()->addSelect('blacklist_user_id')->asArray()->all();

                    $ids = [];
                    foreach ($blackListIDs as $id) {
                        $ids[] = $id['blacklist_user_id'];
                    }

                    if (empty($blackListIDs)) {
                        return JSON::encode([

                            'success' => TRUE,

                            'girls' => null,

                            'count' => 0,

                            'statusArray' => $statusArray,

                        ]);
                    }
                    
                    $userModel->andWhere(['in', 'user.id', $ids]);

                    break;

                case 'from_agency':

                    if (empty($userID || empty($agencyID)))

                        break;

                    $agency = Agency::findOne(['id' => $agencyID]);


                    if (empty($agency)) {

                        break;

                    }
                    if (Yii::$app->user->identity->user_type == User::USER_INTERPRETER) {
                        $agencyUserIDs = Yii::$app->user->identity->user_ids;
                    } else {
                        $agencyUserIDs = $agency->getUsersIDs($params, [User::STATUS_NO_ACTIVE, User::STATUS_ACTIVE]);
                    }

                    //return $agencyUserIDs;
                    if (empty($agencyUserIDs))

                        return ['success' => TRUE, 'girls' => [], 'count' => 0, 'statusArray' => $statusArray];

                    $userModel->andWhere(['in', 'user.id', $agencyUserIDs]);

                    if (isset($statusParams['cam_online']) && $statusParams['cam_online'] == 1) {
                        $userModel
                            ->andWhere(['>=', 'user.last_activity', time() - 30])
                            ->andWhere(['user.cam_online' => 1])
                            ->andWhere(['user.status' => User::STATUS_ACTIVE, 'user.approve_status_id' => 4]);
                    }

                    //выбор количества по статусам

                    $all_count = User::find()->where(['in', 'user.id', $agencyUserIDs])->count();

                    $active_count = User::find()->where(['in', 'user.id', $agencyUserIDs])->andWhere(['user.status' => User::STATUS_ACTIVE, 'user.approve_status_id' => 4])->count();

                    $no_active_count = User::find()->where(['in', 'user.id', $agencyUserIDs])->andWhere(['user.status' => User::STATUS_NO_ACTIVE])->count();

                    $invisible = User::find()->where(['in', 'user.id', $agencyUserIDs])->andWhere(['user.status' => User::STATUS_ACTIVE, 'user.visible' => User::STATUS_INVISIBLE])->count();

                    $new = User::find()->where(['in', 'user.id', $agencyUserIDs])->andWhere(['user.approve_status_id' => 1])->count();

                    $approve_inprogress = User::find()->where(['in', 'user.id', $agencyUserIDs])->andWhere(['user.approve_status_id' => 2])->count();

                    $approve_decline = User::find()->where(['in', 'user.id', $agencyUserIDs])->andWhere(['user.approve_status_id' => 3])->count();

                    $online_count = User::find()->where(['in', 'user.id', $agencyUserIDs])->andWhere(['>=', 'user.last_activity', time() - 30])->count();


                    $statusArray = [

                        'all_count' => $all_count,

                        'active_count' => $active_count,

                        'no_active_count' => $no_active_count,

                        'new' => $new,

                        'invisible' => $invisible,

                        'approve_inprogress' => $approve_inprogress,

                        'approve_decline' => $approve_decline,

                        'online' => $online_count,

                    ];

                    break;

                case 'from_admin':

                    if (empty($userID))

                        break;

                    //для мужиков

                    $allCount = User::find()->where(['user.user_type' => $userType])->count();

                    $activeCount = User::find()->where(['user.user_type' => $userType])
                        ->andWhere(['user.status' => User::STATUS_ACTIVE, 'user.approve_status_id' => 4])->count();

                    $noActiveCount = User::find()->where(['user.user_type' => $userType])
                        ->andWhere(['user.status' => [User::STATUS_NO_ACTIVE]])->count();

                    $new = User::find()->where(['user.user_type' => $userType])
                        ->andWhere(['user.approve_status_id' => 1])->count();

                    $invisible = User::find()->where(['user.user_type' => $userType, 'user.status' => User::STATUS_ACTIVE])->andWhere(['user.visible' => User::STATUS_INVISIBLE])->count();

                    $approveInprogress = User::find()->where(['user.user_type' => $userType])
                        ->andWhere(['user.approve_status_id' => 2])->count();

                    $approveDecline = User::find()->where(['user.user_type' => $userType])
                        ->andWhere(['user.approve_status_id' => 3])->count();

                    $onlineCount = User::find()->where(['user.user_type' => $userType])
                        ->andWhere(['>=', 'user.last_activity', time() - 30])->count();


                    $statusArray = [

                        'all_count' => $allCount,

                        'active_count' => $activeCount,

                        'no_active_count' => $noActiveCount,

                        'new' => $new,

                        'invisible' => $invisible,

                        'approve_inprogress' => $approveInprogress,

                        'approve_decline' => $approveDecline,

                        'online' => $onlineCount,

                    ];

                    break;

                case 'fake-men':

                    $userModel->andWhere(['user.fake' => [User::STATUS_FAKE_YES], 'user.approve_status_id' => 4]);

                    $allCount = User::find()->where(['user.user_type' => User::USER_MALE, 'user.status' => User::STATUS_ACTIVE, 'user.fake' => [User::STATUS_FAKE_YES, User::STATUS_FAKE_NO]])->count();

                    $activeCount = User::find()->where(['user.user_type' => User::USER_MALE, 'user.fake' => User::STATUS_FAKE_YES])->andWhere(['user.status' => User::STATUS_ACTIVE, 'user.approve_status_id' => 4])->count();

                    $noActiveCount = User::find()->where(['user.user_type' => User::USER_MALE, 'user.status' => User::STATUS_NO_ACTIVE])->andWhere(['user.fake' => [User::STATUS_FAKE_YES]])->count();

                    $new = User::find()->where(['user.user_type' => User::USER_MALE, 'user.fake' => [User::STATUS_FAKE_YES, User::STATUS_FAKE_NO]])
                        ->andWhere(['user.approve_status_id' => 1])->count();

                    $invisible = User::find()->where(['user.user_type' => User::USER_MALE])
                        ->andWhere(['user.visible' => User::STATUS_INVISIBLE, 'user.fake' => User::STATUS_FAKE_YES])->count();

                    $approveInprogress = User::find()->where(['user.user_type' => User::USER_MALE, 'user.fake' => [User::STATUS_FAKE_YES, User::STATUS_FAKE_NO]])
                        ->andWhere(['user.approve_status_id' => 2])->count();

                    $approveDecline = User::find()->where(['user.user_type' => User::USER_MALE, 'user.fake' => [User::STATUS_FAKE_YES, User::STATUS_FAKE_NO]])
                        ->andWhere(['user.approve_status_id' => 3])->count();

                    $onlineCount = User::find()->where(['user.user_type' => User::USER_MALE, 'user.fake' => [User::STATUS_FAKE_YES, User::STATUS_FAKE_NO]])
                        ->andWhere(['>=', 'user.last_activity', time() - 30])->count();


                    $statusArray = [

                        'all_count' => $allCount,

                        'active_count' => $activeCount,

                        'no_active_count' => $noActiveCount,

                        'new' => $new,

                        'invisible' => $invisible,

                        'approve_inprogress' => $approveInprogress,

                        'approve_decline' => $approveDecline,

                        'online' => $onlineCount,

                    ];

                    break;

            }

        }


        if ($statusParams !== null) {

            switch ($statusParams) {

                case 'active':

                    $userModel->andWhere(['user.status' => [User::STATUS_ACTIVE], 'user.approve_status_id' => 4]);

                    break;

                case 'no_active_count':

                    $userModel->andWhere(['user.status' => [User::STATUS_NO_ACTIVE]]);

                    break;

                case 'invisible':

                    $userModel->andWhere(['user.visible' => User::STATUS_INVISIBLE]);

                    break;

                case 'new':

                    $userModel->andWhere(['user.approve_status_id' => 1]);

                    break;

                case 'approve_inprogress':

                    $userModel->andWhere(['user.approve_status_id' => 2]);

                    break;

                case 'approve_decline':

                    $userModel->andWhere(['user.approve_status_id' => 3]);

                    break;

                case 'online':

                    $userModel->andWhere(['>=', 'user.last_activity', time() - 30]);

                    break;

            }

        }


        if ($params !== null) {

            if (isset($params['id']) && is_numeric($params['id'])) {

                $userModel->andFilterWhere(['user.id' => $params['id']]);

            }

            if (isset($params['name']) && !is_numeric($params['name'])) {

                $userModel->andFilterWhere(['like', 'user_profile.first_name', $params['name']], FALSE);

            }


            if (isset($params['city_id']) && is_numeric($params['city_id'])) {

                $userModel->leftJoin(LocationsModel::tableName(), 'user.id = location.user_id')
                    ->andFilterWhere(['location.city_id' => $params['city_id']]);

            }

            if (isset($params['country_id']) && is_array($params['country_id']) && array_filter($params['country_id'], 'is_numeric')) {

                $userModel->leftJoin(LocationsModel::tableName(), 'user.id = location.user_id')
                    ->andFilterWhere(['location.country_id' => $params['country_id']]);

            }

            if (isset($params['age_from']) && is_numeric($params['age_from']) && $params['age_from'] > 18) {

                $age_from = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y") - $params['age_from']));

                $userModel->andFilterWhere(['<=', 'DATE(user_profile.birthday)', $age_from]);

            }


            if (isset($params['age_to']) && is_numeric($params['age_to']) && $params['age_to'] < 60) {

                $age_to = date("Y-m-d", mktime(0, 0, 0, 1, 1, date("Y") - $params['age_to']));

                $userModel->andFilterWhere(['>=', 'DATE(user_profile.birthday)', $age_to]);

            }


            if (isset($params['heightFrom']) && is_numeric($params['heightFrom']) && $params['heightFrom'] > 140) {

                $userModel->andFilterWhere(['>=', 'user_profile.height', $params['heightFrom']]);

            }


            if (isset($params['heightTo']) && is_numeric($params['heightTo']) && $params['heightTo'] < 240) {

                $userModel->andFilterWhere(['<=', 'user_profile.height', $params['heightTo']]);

            }


            if (isset($params['physique']) && is_array($params['physique']) && array_filter($params['physique'], 'is_numeric')) {

                $userModel->andFilterWhere(['user_profile.physique' => $params['physique']]);

            }


            if (isset($params['new']) && $params['new'] == 1) {

                $userModel->andFilterWhere(['>=', 'user.created_at', date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 30)]);

            }


            if (isset($params['girls_online']) && $params['girls_online'] == 1) {

                $userModel->andFilterWhere(['>=', 'user.last_activity', time() - 30]);

            }

            if (isset($params['cam_online']) && $params['cam_online'] == 1) {

                $userModel->andWhere(['>=', 'user.last_activity', time() - 30])
                    ->andFilterWhere(['user.cam_online' => 1]);

            }

            if (isset($params['hair']) && is_array($params['hair'])) {

                $userModel->andFilterWhere(['in', 'user_profile.hair', $params['hair']]);

            }

            if (isset($params['eyes']) && is_array($params['eyes'])) {

                $userModel->andFilterWhere(['in', 'user_profile.eyes', $params['eyes']]);

            }


            if (isset($params['weight']) && is_array($params['weight'])) {

                $conditions = [

                    '1' => ['<=', 'user_profile.weight', 50],

                    '2' => ['between', 'user_profile.weight', 50, 60],

                    '3' => ['between', 'user_profile.weight', 60, 70],

                    '4' => ['between', 'user_profile.weight', 70, 80],

                    '5' => ['>=', 'user_profile.weight', 80],

                ];

                $weight_array = ['OR'];

                foreach ($params['weight'] as $key) {

                    if (isset($conditions[$key])) {

                        $weight_array[] = $conditions[$key];

                    }

                }

                $userModel->andWhere($weight_array);

            }


            if (isset($params['height']) && is_array($params['height'])) {

                $conditions = [

                    '1' => ['<=', 'user_profile.height', 150],

                    '2' => ['between', 'user_profile.height', 150, 165],

                    '3' => ['between', 'user_profile.height', 165, 175],

                    '4' => ['between', 'user_profile.height', 175, 185],

                    '5' => ['>=', 'user_profile.height', 185],

                ];


                $height_array = ['OR'];

                foreach ($params['height'] as $key) {

                    if (isset($conditions[$key])) {

                        $height_array[] = $conditions[$key];

                    }

                }

                $userModel->andWhere($height_array);

            }

            if (isset($params['married']) && is_array($params['married'])) {

                $userModel->andWhere(['in', 'user_profile.marital', $params['married']]);

            }

            if (isset($params['children']) && is_numeric($params['children'])) {

                if ($params['children'] == 3) {

                    $userModel->andWhere(['>=', 'user_profile.kids', $params['children']]);

                } elseif ($params['children'] == 0) {

                    $userModel->andWhere(['user_profile.kids' => 0]);

                } else {

                    $userModel->andWhere(['user_profile.kids' => $params['children']]);

                }


            }

            if (isset($params['smoke']) && is_numeric($params['smoke'])) {

                $userModel->andWhere(['user_profile.smoking' => $params['smoke']]);

            }

            if (isset($params['drink']) && is_numeric($params['drink'])) {

                $userModel->andWhere(['user_profile.alcohol' => $params['drink']]);

            }

        }

        //добавление состояния favorite

        if (!empty($userID)) {

            $subQuery2 = (new Query)
                ->addSelect(['user_favorite_list.favorite_user_id', 'in_favorite' => new Expression(1)])
                ->from([UserFavoriteList::tableName()])
                ->where(['user_favorite_list.user_id' => $userID])
                ->limit(100000);

            $userModel->leftJoin(['user_favorite_list' => $subQuery2], 'user_favorite_list.favorite_user_id = user.id');

            $userModel->addSelect(['user_favorite_list.in_favorite']);

            $subQuery2 = (new Query)
                ->addSelect(['user_blacklist.blacklist_user_id', 'in_blacklist' => new Expression(1)])
                ->from([UserBlacklist::tableName()])
                ->where(['user_blacklist.user_id' => $userID])
                ->limit(100000);

            $userModel->leftJoin(['user_blacklist' => $subQuery2], 'user_blacklist.blacklist_user_id = user.id');

            $userModel->addSelect(['user_blacklist.in_blacklist']);

        }


        if ($visibility == "visible" && $status) {

            $userModel->andWhere(['user.visible' => User::STATUS_VISIBLE]);

        } elseif ($visibility == "invisible" && $status) {

            $userModel->andWhere(['user.visible' => User::STATUS_INVISIBLE]);

        }


        $count = $userModel->count();


        if ($order !== null) {

            $userModel->addOrderBy($order);

        }

        if ($limit !== null) {

            $userModel->limit($limit);

        }

        if ($offset !== null && filter_var($offset, FILTER_VALIDATE_INT)) {

            $userModel->offset($offset);

        }

        $userModel->addOrderBy('user.id DESC');

        $result = $userModel->all();


        return [

            'success' => TRUE,

            'girls' => $result,

            'count' => $count,

            'statusArray' => $statusArray,

        ];


    }


    public function actionChangePassword()

    {

        $otherUserID = Yii::$app->request->post('otherUserID');


        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        $otherUserID = (isset($otherUserID) && filter_var($otherUserID, FILTER_VALIDATE_INT)) ? $otherUserID : $user->id;
        if (in_array($user->user_type, [User::USER_ADMIN, User::USER_SITEADMIN]) && ($otherUserID != $user->id) && (!isset($user->permissions) || $user->permissions->cantAccess('users_access'))) {
            throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
        }

        $otherUser = User::findOne(['id' => $otherUserID]);


        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        $model = new ChangePasswordForm();

        $fields = [

            'oldPassword' => Yii::$app->request->post('oldPassword'),

            'newPassword' => Yii::$app->request->post('newPassword'),

            'confirmPassword' => Yii::$app->request->post('confirmPassword'),

            'otherUserID' => $otherUserID,

        ];


        if ($model->load(['ChangePasswordForm' => $fields]) && $model->validate() && $model->saveNewPassword()) {

            return [

                'success' => TRUE,

                'message' => 'User password was changed',

            ];

        } else {

            return [

                'success' => FALSE,

                'code' => 'Validation errors',

                'errors' => $model->errors,

            ];

        }

    }


    public function actionGetGirlsOnMainPage()

    {


        $postArray = Yii::$app->request->post();


        if (empty($postArray) || !isset($postArray['skinID']) || !filter_var($postArray['skinID'], FILTER_VALIDATE_INT)

            || !isset($postArray['userType']) || !filter_var($postArray['userType'], FILTER_VALIDATE_INT)
        ) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params',

            ];

        }


        $params = [

            'limit' => (isset($postArray['limit'])) ? $postArray['limit'] : null,

            'userID' => (isset($postArray['userID'])) ? $postArray['userID'] : null,

            'userType' => $postArray['userType'],

            'skinID' => $postArray['skinID'],

        ];


        //$params['type'] = 'new'; не актуально если новые не добавляются

        $params['order'] = 'user.created_at DESC';


        $girlsNew = json_decode(JSON::encode($this->actionGetUsers($params)));


        unset($params['order']);


        $params['type'] = 'top';

        $girlsTop = json_decode(JSON::encode($this->actionGetUsers($params)));


        $params['type'] = 'last_activity';

        $girlsOnline = json_decode(JSON::encode($this->actionGetUsers($params)));


        unset($params['type']);

        $params['order'] = 'user.last_activity DESC';

        $params['limit'] = 7;

        $girlsAll = json_decode(JSON::encode($this->actionGetUsers($params)));


        $result['online'] = $girlsOnline->girls;

        $result['top'] = $girlsTop->girls;

        $result['new'] = $girlsNew->girls;

        $result['all'] = $girlsAll->girls;


        return [

            'success' => TRUE,

            'girls' => $result,

        ];


    }


    public function actionGetUsers($parameters = null)
    {

        $result = json_decode(JSON::encode($this->actionGetGirlsByParams($parameters)));
        //        return ['success' => true, '1' => $result];
        if (is_string($result)) {
            $result = json_decode($result);
        }

        if (!empty($result->girls)) {

            $girls = [];

            foreach ($result->girls as $girl) {

                $simplifiedUser['id'] = $girl->id;

                $simplifiedUser['first_name'] = $girl->first_name;

                $simplifiedUser['cam_online'] = $girl->cam_online;

                $simplifiedUser['birthday'] = $girl->birthday;

                $simplifiedUser['small_thumb'] = $girl->small_thumb;

                $simplifiedUser['medium_thumb'] = $girl->medium_thumb;

                $simplifiedUser['new'] = (time() - strtotime($girl->created_at) < 60 * 60 * 24 * 30) ? 1 : 0;

                $simplifiedUser['online'] = (time() - $girl->last_activity <= 30) ? 1 : 0;

                $simplifiedUser['inFavorite'] = !empty($girl->in_favorite) ? 1 : null;

                $simplifiedUser['inBlackList'] = !empty($girl->in_blacklist) ? 1 : null;

                if (!empty($girl->birthday)) {

                    $tz = new \DateTimeZone('Europe/Brussels');

                    $simplifiedUser['age'] = \DateTime::createFromFormat('Y-m-d', $girl->birthday, $tz)
                        ->diff(new \DateTime('now', $tz))
                        ->y;

                } else {

                    $simplifiedUser['age'] = null;

                }

                $user = Yii::$app->user->identity;

                if (!empty($user)) {

                    $chatService = new ChatService($user->id);

                    $simplifiedUser['unreadedMessages'] = $chatService->getUnreadedMessages($girl->id);

                }

                $girls[] = (object)$simplifiedUser;

            }

        } else {

            $girls = [];

        }

        return [

            'success' => $result->success,

            'girls' => $girls,

            'count' => $result->count,

            'statusArray' => [],

        ];

    }


    public function actionGetUsersForChat($parameters = null)

    {


        $otherUserID = Yii::$app->request->post('otherUserID');


        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        if (empty($otherUserID) && isset($parameters['otherUserID'])) {

            $otherUserID = $parameters['otherUserID'];

        }

        if (empty($otherUserID)) {

            $otherUserID = $user->id;

        }


        if (!isset($otherUserID) || !is_numeric($otherUserID)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params1',

            ];

        }


        $otherUser = User::findOne(['id' => $otherUserID]);


        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        // check for errors

        if ($user->id != $otherUser->id) {

            if (empty($user->user_type) || empty($otherUser->user_type) ||

                $user->user_type == $otherUser->user_type ||

                !in_array($user->user_type, [User::USER_MALE, User::USER_FEMALE]) ||

                !in_array($otherUser->user_type, [User::USER_MALE, User::USER_FEMALE])
            ) {

                return [

                    'success' => FALSE,

                    'code' => 2,

                    'message' => 'Access denied',

                ];

            }

        }


        if ($user->inBlackList($otherUser->id)) {

            return [

                'success' => FALSE,

                'code' => 2,

                'message' => 'User add you to blacklist',

            ];

        }

        if ($parameters == null) {

            $users = json_decode(JSON::encode($this->actionGetUsers()));

        } else {

            $users = json_decode(JSON::encode($this->actionGetUsers($parameters)));

        }

        return [

            'success' => $users->success,

            'girls' => $users->girls,

            'count' => $users->count,

            'statusArray' => $users->statusArray,

        ];


    }


    public function actionGetUsersForAdmin($parameters = null)
    {


        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_INTERPRETER, User::USER_SUPERADMIN, User::USER_ADMIN])) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Access Denied',

            ];

        }


        $result = json_decode(JSON::encode($this->actionGetGirlsByParams($parameters)));
        

        if (!empty($result->girls)) {

            $girls = [];

            foreach ($result->girls as $girl) {

                $simplifiedUser['id'] = $girl->id;

                $simplifiedUser['first_name'] = $girl->first_name;

                $simplifiedUser['last_name'] = $girl->last_name;

                $simplifiedUser['cam_online'] = $girl->cam_online;

                $simplifiedUser['birthday'] = $girl->birthday;

                $simplifiedUser['small_thumb'] = $girl->small_thumb;

                $simplifiedUser['medium_thumb'] = $girl->medium_thumb;

                $simplifiedUser['approve_status_id'] = $girl->approve_status_id;

                $simplifiedUser['visible'] = $girl->visible;

                $simplifiedUser['last_activity'] = $girl->last_activity;

                $simplifiedUser['fake'] = $girl->fake;

                $simplifiedUser['status'] = $girl->status;


                $simplifiedUser['new'] = (time() - strtotime($girl->created_at) < 60 * 60 * 24 * 30) ? 1 : 0;

                $simplifiedUser['online'] = (time() - $girl->last_activity <= 30) ? 1 : 0;

                if (!empty($girl->birthday)) {

                    $tz = new \DateTimeZone('Europe/Brussels');

                    $simplifiedUser['age'] = \DateTime::createFromFormat('Y-m-d', $girl->birthday, $tz)
                        ->diff(new \DateTime('now', $tz))
                        ->y;

                } else {

                    $simplifiedUser['age'] = null;

                }


                $girls[] = (object)$simplifiedUser;

            }

        } else {

            $girls = [];

        }

        return [

            'success' => $result->success,

            'girls' => $girls,

            'count' => $result->count,

            'statusArray' => $result->statusArray,

        ];

    }


    public function actionGetUsersOnAccountPage()

    {

        $skinID = Yii::$app->request->post('skin_id');

        $userType = Yii::$app->request->post('user_type');

        $limit = Yii::$app->request->post('limit');

        $userID = Yii::$app->request->post('userID');

        $token = Yii::$app->request->post('token');


        if (!isset($skinID) || !filter_var($skinID, FILTER_VALIDATE_INT)

            || !isset($userType) || !filter_var($userType, FILTER_VALIDATE_INT)

            || !isset($token)
        ) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params',

            ];

        }


        $response = json_decode(JSON::encode($this->actionCheckToken(['token' => $token])));


        if (!$response->success) {

            return [

                'success' => FALSE,

                'code' => 2,

                'message' => 'Token time leave',

            ];

        }


        $params = [

            'limit' => (isset($limit)) ? $limit : null,

            'skinID' => $skinID,

            'userType' => $userType,

            'userID' => $userID,

        ];


        $params['type'] = 'new';

        $params['order'] = 'user.created_at DESC';

        $params['visibility'] = 'visible';

        $girlsNew = json_decode(JSON::encode($this->actionGetUsers($params)));


        unset($params['order']);


        //guest

        $params['type'] = 'guest';

        $girlsGuest = json_decode(JSON::encode($this->actionGetUsers($params)));


        //birthday

        $params['type'] = 'birthday';

        $girlsBirthday = json_decode(JSON::encode($this->actionGetUsers($params)));


        //recent

        $params['type'] = 'recent';

        $girlsRecent = json_decode(JSON::encode($this->actionGetUsers($params)));


        //favorite

        $params['type'] = 'favorite';

        $girlsFavorite = json_decode(JSON::encode($this->actionGetUsers($params)));


        $result['newGirls'] = $girlsNew->girls;

        $result['guestGirls'] = $girlsGuest->girls;

        $result['birthdayGirls'] = $girlsBirthday->girls;

        $result['recentGirls'] = $girlsRecent->girls;

        $result['favoriteGirls'] = $girlsFavorite->girls;


        return [

            'success' => TRUE,

            'girls' => $result,

        ];


    }


    public function actionGetUserLocations()

    {

        $skinID = Yii::$app->request->post('skin_id');

        $searchUserType = Yii::$app->request->post('searchUserType');


        if (!isset($skinID) || !filter_var($skinID, FILTER_VALIDATE_INT)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params',

            ];

        }


        $params = [

            'skinID' => $skinID,

            'userType' => (isset($searchUserType) && filter_var($searchUserType, FILTER_VALIDATE_INT)) ? $searchUserType : User::USER_FEMALE,

        ];


        $userID = Yii::$app->request->post('user_id');

        $userID = isset($userID) && !filter_var($userID, FILTER_VALIDATE_INT) ? $userID : FALSE;


        if ($userID) {

            $girlIDs = array($userID);

        } else {

            $girls = json_decode(JSON::encode($this->actionGetGirlsByParams($params)));

            if (!empty($girls->girls)) {

                foreach ($girls->girls as $girl) {

                    $girlIDs[] = $girl->id;

                }

            }

        }


        $result = Locations::getLocationByIds($girlIDs);


        return $result;


    }


    public function actionSetFavorite()

    {

        $otherUserID = Yii::$app->request->post('otherUserID');


        $user = Yii::$app->user->identity;


        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        if (!isset($otherUserID) || !is_numeric($otherUserID)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'Missing params',

            ];

        }


        $otherUser = User::findOne(['id' => $otherUserID]);


        $result2 = $this->actionCheckRights($user, $otherUser);


        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }


        // check for errors

        if (empty($user->user_type) || empty($otherUser->user_type) ||

            $user->user_type == $otherUser->user_type ||

            !in_array($user->user_type, [User::USER_MALE, User::USER_FEMALE]) ||

            !in_array($otherUser->user_type, [User::USER_MALE, User::USER_FEMALE])
        ) {

            return [

                'success' => FALSE,

                'code' => 2,

                'message' => 'Access denied',

            ];

        }


        if ($user->inBlackList($otherUser->id)) {

            return [

                'success' => FALSE,

                'code' => 2,

                'message' => 'User add you to blacklist',

            ];

        }


        $result = $user->setToFavorite($otherUser->id);


        return $result;

    }


    public function actionGetUserInfo()

    {


        $token = Yii::$app->request->post('token');

        $otherUserID = Yii::$app->request->post('otherUserID');

        $user = Yii::$app->user->identity;

        if (empty($user)) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        if (isset($otherUserID) && filter_var($otherUserID, FILTER_VALIDATE_INT)) {

            $otherUser = User::findOne(['id' => $otherUserID]);

        } else {

            $otherUser = $user;

        }

        if (empty($otherUser)) {

            LogTable::addLog($user->id, $user->user_type, 'Error during get user info. User not found');

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => 'User not found',

            ];

        }


        $result2 = $this->actionCheckRights($user, $otherUser);

        if (!$result2['success']) {

            return [

                'success' => FALSE,

                'code' => 1,

                'message' => $result2['message'],

            ];

        }

        $userData = [];

        if (!in_array($otherUser->user_type, [User::USER_AGENCY,User::USER_AFFILIATE])) {

            $profile = $otherUser->profile;


            if (empty($profile)) {

                LogTable::addLog($user->id, $user->user_type, 'Error during get user info. User profile not found');

                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'User not found',

                ];

            }

            $userData['personalInfo'] = $profile;

        }


        //get avatar

        $userAvatar = $otherUser->avatar;


        //get balanse

        $paymentsService = new PaymentsService($otherUser->id);

        $userBalance = $paymentsService->getBalance();


        //get statuses

        $date = date("Y-m-d H:i:s", time());


        $userStatusArray = $otherUser->getStatuses()->joinWith('statusType')->where(["<=", 'start_datetime', $date])->andWhere(['active' => Status::STATUS_ACTIVE])->andWhere([">=", 'stop_datetime', $date])->all();

        $userStatus = isset($userStatusArray[0]["statusType"]->title) ? $userStatusArray[0]["statusType"]->title : "BRONZE";


        $userData['userID'] = $otherUser->id;

        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_INTERPRETER, User::USER_ADMIN, User::USER_SUPERADMIN])) {
            $otherUser->token_end_date = time() + Yii::$app->params['token_time'];
            $otherUser->access_token = $otherUser->getJWT();
            if ($otherUser->save()) {
                $userData['token'] = $otherUser->access_token;
            } else {
                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => 'Can`t get access token',

                ];
            }
            

        } else {
            if ($user->token_end_date > time() + Yii::$app->params['token_time']) {
                $user->access_token = $user->getJWT(Yii::$app->params['remember_token_time']);
            } elseif ($user->token_end_date > time()) {
                $user->access_token = $user->getJWT();
            } else {
                $user->access_token = $user->getJWT(-30);
            }           
            if (empty($user->email)) {
                $user->scenario = User::SCENARIO_REGISTER_BY_SOCIAL;
            }            
            if ($user->save()) {
                $userData['token'] = $user->access_token;
            } else {                
                return [

                    'success' => FALSE,

                    'code' => 1,

                    'message' => join(', ', $user->getFirstErrors()),

                ];
            }
        }


        $userData['avatarPhotoID'] = $otherUser->avatar_photo_id;

        $userData['avatar']['small'] = ($userAvatar) ? $userAvatar['small_thumb'] : null;

        $userData['avatar']['normal'] = ($userAvatar) ? $userAvatar['medium_thumb'] : null;

        $userData['avatar']['origin'] = ($userAvatar) ? $userAvatar['original_image'] : null;

        $userData['userType'] = $otherUser->user_type;

        $userData['email'] = $otherUser->email;

        $userData['cam_online'] = $otherUser->cam_online;

        if ($user->id == $otherUser->id || in_array($user->user_type, [User::USER_AGENCY, User::USER_INTERPRETER, User::USER_ADMIN, User::USER_SUPERADMIN])) {

            $userData['balance'] = round($userBalance, 2);

        }

        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_INTERPRETER, User::USER_ADMIN, User::USER_SUPERADMIN, User::USER_AFFILIATE])) {

            $userData['approve_status'] = $otherUser->approve_status_id;

            $userData['active_status'] = $otherUser->status;

            $userData['status'] = $userStatus;
            $userData['deactivation_reason'] = isset($otherUser->profile->deactivation_reason) ? $otherUser->profile->deactivation_reason : '';
            $userData['deactivation_time'] = isset($otherUser->profile->deactivation_time) ? $otherUser->profile->deactivation_time : '';

        }

        if (in_array($user->user_type, [User::USER_MALE])) {
            $userData['status'] = $userStatus;
            $userData['created_at'] = $otherUser->created_at;
        }

        $userAgency = $otherUser->getAgency()->where(['approve_status' => ApproveStatus::STATUS_APPROVED])->andWhere(['!=', 'id', 1])->asArray()->one();

        if (!empty($userAgency))

            $userData['agencyID'] = $userAgency['id'];


        $userData['location'] = (!empty($otherUser->location)) ? $otherUser->location : 0;

        if (!empty($otherUser->location)) {

            $city = City::findOne(['id' => $otherUser->location->city_id]);

        }

        $userData['city'] = (!empty($city->name)) ? $city->name : '';


        $countryList = Country::find()->all();

        $approve_statuses = ApproveStatus::find()->asArray()->all();

        //for agency

        if (in_array($userData['userType'], [User::USER_AGENCY, User::USER_ADMIN])) {

            $userAgency = $otherUser->getAgency()->asArray()->one();

            $agencyList = Agency::find()->addSelect('id', 'name', 'contact_person')->where(['!=', 'id', 1])->andWhere(['approve_status' => ApproveStatus::STATUS_APPROVED])->asArray()->all();

            if (!empty($userAgency)) {

                $userData['agency_info'] = $userAgency;

            }
            
            $userData['agencyRuleViolations'] = $otherUser->agency->violations;
            if ($user->user_type == User::USER_AGENCY && $otherUser->user_type == User::USER_ADMIN) {
                $userData['permissions'] = $this->getPermissions($otherUser->permissions, $otherUser['user_type']);
            }            

        }

        //for affiliates

        if ($userData['userType'] == User::USER_AFFILIATE) {

            $userAffiliates = $otherUser->getAffiliates()->asArray()->one();

            if (!empty($userAffiliates)) {

                $userData['affiliates_info'] = $userAffiliates;

            }

        }

        $girlsArray = null;


        if ($otherUser->user_type == User::USER_INTERPRETER && $userData['agencyID']) {

            $userAgency = $otherUser->getAgency()->where(['!=', 'id', 1])->one();

            if (!empty($userAgency)) {

                $girlsIDs = $userAgency->getUsersIDs('girls');

            }

            if (is_array($girlsIDs)) {

                $girlsArray = UserProfile::find()
                    ->select(['id', 'first_name', 'last_name'])
                    ->where(['in', 'id', $girlsIDs])->asArray()->all();

            }


            if (!empty($girlsArray)) {

                $userData['girlsIDs'] = $otherUser->user_ids;

            }

        }


        return [

            'success' => TRUE,

            'userData' => $userData,

            'countryList' => $countryList,

            'approve_statuses' => $approve_statuses,

            'girlsArray' => $girlsArray,

            'agencyList' => (!empty($agencyList)) ? $agencyList : [],

        ];

    }


    public function actionGetCountryList()

    {

        $countryList = Locations::getCountry();


        return [

            'success' => TRUE,

            'countryList' => $countryList,

        ];

    }

    public function actionAddReferralVisitor()
    {
        $postArray = Yii::$app->request->post();

        if (empty($postArray) || !isset($postArray['referralLink']) || !isset($postArray['ip'])
        || trim($postArray['referralLink']) == '' || trim($postArray['ip']) == '' ) {
            return [
                'success' => FALSE,
                'code' => 1,
                'message' => 'Missing params',
            ];
        }

        $result = ReferralStats::addVisitor(trim($postArray['referralLink']),trim($postArray['ip']),$type = 1);

        return $result;

    }

    public function actionAddUserFunds()

    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];

        }
        $postArray = Yii::$app->request->post();
        if (empty($postArray) || !isset($postArray['FromUserId']) || !isset($postArray['totalAmount'])
            || !isset($postArray['orderNum']) || !is_numeric($postArray['FromUserId'])
            || !isset($postArray['random_key']) ) {
            return [
                'success' => FALSE,
                'code' => 1,
                'message' => 'Missing params',
            ];
        }

        $paymentsModel = new PaymentsService($user->id);
        $ressult = $paymentsModel->addFunds($postArray['totalAmount'],$transType='MasterCard',$postArray['random_key']);

        return $ressult;

    }

    public function actionGetPaymentKey()

    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'User not found',
            ];

        }

        $paymentsModel = new PaymentsService($user->id);
        $ressult = $paymentsModel->getRandomKey();

        return $ressult;

    }
    
    public function actionRestartChatServer()
    {
        $output = shell_exec('php /var/www/api.lifetime.dating/commands/server.php restart -d');
        return $output;
    }

    private function getPermissions($userPermissions, $userType)
    {
        if (!empty($userPermissions) && json_decode($userPermissions->permissions)) {
            return json_decode($userPermissions->permissions);
        } else {
            if ($userType == self::USER_SITEADMIN) {
                $newPermissions = Yii::$app->params['siteadmin_permissions'];
            } else {
                $newPermissions = Yii::$app->params['admin_permissions'];
            }            
            $modifiedPermissions = [];
            foreach ($newPermissions as $newAccess => $newPermission) {
                $modifiedPermissions[$newAccess] = $newPermission['value'];
            }
            return $modifiedPermissions;
        }
    }

}