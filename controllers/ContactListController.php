<?php
namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\Response;

/* my models */
use app\models\LogTable;
use app\models\User;
use app\models\ApproveStatus;
use app\models\UserProfile;

use app\models\Chat;
use app\models\ChatSession;
use app\models\Action;
use app\models\ContactList;


class ContactListController extends ActiveController
{
    use CommonActionsTrait;
    public $modelClass = '';

    const NUMBER_OF_CONTACTS_DISPLAYED_BY_DEFAULT = 5;

    public function behaviors()
    {
        $authArray = ['get-contacts', 'delete-contact', 'delete-all-contacts'];
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => $authArray
        ];

        $access = $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['login'],
            'rules' => [
                [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => $authArray,
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $verbs = $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ];
        
        unset($behaviors['access']);
        unset($behaviors['authenticator']);
        unset($behaviors['verbs']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Allow-Origin'      => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],
        ];
        
        $behaviors['rateLimiter'] = [
            // Use class
            'class' => \highweb\ratelimiter\RateLimiter::className(),
            
            // The maximum number of allowed requests
            'rateLimit' => 10,
            
            // The time period for the rates to apply to
            'timePeriod' => 60,
            
            // Separate rate limiting for guests and authenticated users
            // Defaults to true
            // - false: use one set of rates, whether you are authenticated or not
            // - true: use separate ratesfor guests and authenticated users
            'separateRates' => false,
            
            // Whether to return HTTP headers containing the current rate limiting information
            'enableRateLimitHeaders' => false,
        ];
        
        $behaviors['authenticator'] = $auth;
        $behaviors['access'] = $access;
        $behaviors['verbs'] = $access;
        $behaviors['authenticator']['except'] = ['options', 'send-chat-message', 'send-message'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    public function actionDeleteContact($contactID)
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_FEMALE, User::USER_MALE])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }

        if ($user->user_type == User::USER_FEMALE || $user->user_type == User::USER_MALE) {
            $model = ContactList::deleteAll(['user_id' => $user->id, 'contact_id' => $contactID]);

            $updateChatTable = Chat::updateAll(['readed_at' => date('Y-m-d H:i:s')], ['readed_at' => NULL, 'action_id' => ArrayHelper::map(Action::find()->select('id')->where(['action_creator' => $contactID, 'action_receiver' => $user->id])->asArray()->all(), 'id', 'id')]);                    
        }

        if ($model === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }

    public function actionDeleteAllContacts()
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_FEMALE, User::USER_MALE])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }

        if ($user->user_type == User::USER_FEMALE || $user->user_type == User::USER_MALE) {
            $model = ContactList::deleteAll(['user_id' => $user->id]);
        }

        if ($model === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }

    public function actionGetContacts($page = 1, $sort='-last_activity,+first_name')
    {
        $user = Yii::$app->user->identity; 

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if (!in_array($user->user_type, [User::USER_MALE, User::USER_FEMALE])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access Denied'
            ];            
        }

        $pageFromGET = Yii::$app->request->get('page');
        $perPageFromGET = Yii::$app->request->get('per_page');
        $sortFromGET = Yii::$app->request->get('sort');

        if (!empty($pageFromGET)) {
            $page = $pageFromGET;
        }
        if (!empty($perPageFromGET)) {
            $perPage = $perPageFromGET;
        } else {
            $perPage = self::NUMBER_OF_CONTACTS_DISPLAYED_BY_DEFAULT;
        }       
        if (!empty($sortFromGET)) {
            $sort = $sortFromGET;
        }

        if (isset($page) && !is_numeric($page)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Wrong page parameter'
            ];
        } else {
            $page = (int) $page;
        }

        if ($page < 0) {
            $page = 1;
        }

        if (isset($perPage) && !is_numeric($perPage)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Wrong per page parameter'
            ];
        } else {
            $perPage = (int) $perPage;
        }

        if (isset($perPage) && $perPage <= 0) {
            $perPage = self::NUMBER_OF_CONTACTS_DISPLAYED_BY_DEFAULT;
        }

        if (isset($sort) && !is_string($sort)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Wrong sort parameter'
            ];
        } else {
            $sortArray = [];
            $sortArray['unreaded_messages'] = SORT_DESC;
            try {
                $tempArray = explode(',', $sort);                
                foreach ($tempArray as $element) {
                    if (is_string($element) && ($element[0] === '+' || $element[0] === ' ')) {
                        $sortArray[substr($element, 1)] = SORT_ASC;                    
                    } elseif (is_string($element) && $element[0] === '-') {
                        $sortArray[substr($element, 1)] = SORT_DESC;
                    } else {
                        throw new \Exception();
                    }
                }
            } catch (\Exception $e) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'Wrong sort parameter'
                ];
            }            
        }

        if ($user->user_type == User::USER_MALE || 
            $user->user_type == User::USER_FEMALE) {

            $subQuery = Chat::find()
                ->select('COUNT(action.action_creator) AS unreaded_messages')
                ->addSelect(['action.action_creator']) 
                ->leftJoin(Action::tableName(), 'action.id = chat.action_id')                
                ->where(['chat.readed_at' => null])  
                ->andWhere(['not', ['message' => null]])                                  
                ->andWhere(['action_receiver' => $user->id])                       
                ->groupBy('action.action_creator');            

            $query = ContactList::find()
                        ->select(['unreaded_messages', 'user_profile.first_name', 'user_profile.id'])
                        ->leftJoin(User::tableName(), 'user.id = contact_id')
                        ->leftJoin(UserProfile::tableName(), 'user_profile.id = user.id')
                        ->leftJoin(['messages' => $subQuery], 'contact_id = messages.action_creator')
                        ->where(['user_id' => $user->id]);
                        
                 
            $count = $query->count(); 

            if (!empty($perPage)) {
                $query->limit($perPage);
            }            
            if (!empty($page)) {                
                if ($count >= ($page-1)*$perPage) {
                    $query->offset(($page-1)*$perPage);
                    $query->orderBy($sortArray);                    
                } else {
                    return [
                        'success' => false,
                        'code' => 1,
                        'message' => 'Page not found'
                    ];
                }                
            }
            $contact_list = $query->asArray()->all();
        }

        $result = [
            'success' => true,
            'contact_list' => $contact_list
        ];

        if (in_array($user->user_type, [User::USER_MALE, User::USER_FEMALE])) {
            $result['count'] = $count;
        }
        return $result;
    }

}