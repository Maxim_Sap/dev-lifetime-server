<?php
namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\Response;
use yii\helpers\HtmlPurifier;

/* my models */
use app\models\LogTable;
use app\models\User;
use app\models\Letter;


/* my services */
use app\services\LetterService;



class LetterController extends ActiveController
{
    const NUMBER_OF_LETTERS_ON_HISTORY_PAGE = 5;
    use CommonActionsTrait;
    public $modelClass = '';

    public function behaviors()
    {
        $authArray = ['get-letters', 'send-letter', 'get-history', 'read-letter', 'get-letters-count',
                      'delete-letter', 'restore-letter', 'set-featured-letter', 'send-letter-from-admin'];
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => $authArray
        ];

        $access = $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['login'],
            'rules' => [
                [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => $authArray,
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $verbs = $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ];
        
        unset($behaviors['access']);
        unset($behaviors['authenticator']);
        unset($behaviors['verbs']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Allow-Origin'      => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],
        ];
        
        $behaviors['rateLimiter'] = [
            // Use class
            'class' => \highweb\ratelimiter\RateLimiter::className(),
            
            // The maximum number of allowed requests
            'rateLimit' => 100,
            
            // The time period for the rates to apply to
            'timePeriod' => 60,
            
            // Separate rate limiting for guests and authenticated users
            // Defaults to true
            // - false: use one set of rates, whether you are authenticated or not
            // - true: use separate ratesfor guests and authenticated users
            'separateRates' => false,
            
            // Whether to return HTTP headers containing the current rate limiting information
            'enableRateLimitHeaders' => false,
        ];
        
        $behaviors['authenticator'] = $auth;
        $behaviors['access'] = $access;
        $behaviors['verbs'] = $access;
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    public function beforeAction($action)
    {                   
        if (parent::beforeAction($action)) {
            $user = Yii::$app->user->identity;            
            if (!Yii::$app->user->isGuest && in_array($user->user_type, [User::USER_ADMIN, User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('letter_access'))) {
                throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
            }
            return true;
        } else {
            return false;
        }                
    }
   
    public function actionGetLetters()
    {

        $user = Yii::$app->user->identity;        

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        $themeID = Yii::$app->request->post('themeID');
        $letterID = Yii::$app->request->post('letterID');
        $type = Yii::$app->request->post('type');
        $limit = Yii::$app->request->post('limit');
        $offset = Yii::$app->request->post('offset');
        $searchParams = Yii::$app->request->post('searchParams');

        $themeID       = (isset($themeID) && is_numeric($themeID)) ? (int)$themeID : null;
        $letterID      = (isset($letterID) && is_numeric($letterID)) ? (int)$letterID : null;
        $type          = (isset($type)) ? $type : null;
        $limit         = isset($limit) ? $limit : null;
        $offset        = isset($offset) ? $offset : null;
        $searchParams = isset($searchParams) ? $searchParams : null;

        $letterService = new LetterService($user->id);

        $lettersArray = $letterService->get($letterID, $themeID, $type, $limit, $offset, $searchParams);
        
        //для детального сообщения получаем предыдущее и следующее сообщение
        if (!empty($letterID) && $lettersArray['success']) {
            $nextPrevLetterIDs                  = $this->getPrevNextLettersIds($user->id, $letterID);
            $lettersArray['next_prev_letters_ids'] = $nextPrevLetterIDs;
        }

        return $lettersArray;
    }

    public function getPrevNextLettersIds($userID, $letterID)
    {
        if (!isset($userID) || !is_numeric($userID) || !isset($letterID) || !is_numeric($letterID)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        $letterService = new LetterService($userID);

        return $letterService->getPrevNextLettersIds($letterID);

    }

    public function actionSendLetterFromAdmin()
    {
        $user = Yii::$app->user->identity;

        if (empty($user) || !in_array($user->user_type, [User::USER_SUPERADMIN, User::USER_AGENCY, User::USER_ADMIN])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }
        $userSenderID = Yii::$app->request->post('fromUserID');
        $userSenderID = isset($userSenderID) && is_numeric($userSenderID) ? (int)$userSenderID : null;
        $userReceiverID = Yii::$app->request->post('toUserID');
        $userReceiverID = isset($userReceiverID) && is_numeric($userReceiverID) ? (int)$userReceiverID : null;

        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            $myIDs = $user->getMyChidrensIds('girls');

            if (!in_array($userSenderID, $myIDs)) {
                return [
                    'success' => false, 
                    'code' => 1,
                    'message' => 'Wrong fromUserID'
                ];
            }
        }

        $userSender = User::findOne(['id' => $userSenderID]);
        $userReceiver = User::findOne(['id' => $userReceiverID]);

        $result2 = $this->checkRights($userSender, $userReceiver);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }

        $skinID = Yii::$app->request->post('skinID');
        $title = Yii::$app->request->post('title');
        $description = Yii::$app->request->post('desc');
        $themeID = Yii::$app->request->post('themeID');
        $previousLetterID = Yii::$app->request->post('previousLetterID');

        $title              = isset($title) && trim($title) != "" ? HtmlPurifier::process(trim($title)) : null;
        $description        = isset($description) && trim($description) != "" ? HtmlPurifier::process(trim($description)) : null;
        $themeID            = isset($themeID) && is_numeric($themeID) ? (int)$themeID : null;
        $previousLetterID = isset($previousLetterID) && is_numeric($previousLetterID) ? (int)$previousLetterID : null;

        if (empty($description) || (empty($title) && empty($themeID))) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }
        
        if (!$userSender->user_type || !$userReceiver->user_type || 
            ($userSender->user_type == $userReceiver->user_type) || 
            !in_array($userSender->user_type, [User::USER_MALE, User::USER_FEMALE]) || 
            !in_array($userReceiver->user_type, [User::USER_MALE, User::USER_FEMALE])) {
                return [
                    'success' => false, 
                    'message' => 'Missing params'
                ];
        }

        if ($userSender->inBlackList($userReceiver->id)) {
            return [
                'success' => false, 
                'message' => 'User add you in black list'
            ];
        }

        $letterService = new LetterService($userSender->id);

        return $letterService->send('send letter', $skinID, $userReceiver->id, $title, $themeID, $description, $previousLetterID);

    }

    public function actionSendLetter()
    {        

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        $otherUserID = Yii::$app->request->post('toUserID');
        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }

        $skinID = Yii::$app->request->post('skinID');
        $title = Yii::$app->request->post('title');
        $description = Yii::$app->request->post('desc');
        $themeID = Yii::$app->request->post('themeID');
        $previousLetterID = Yii::$app->request->post('previousLetterID');

        $title              = isset($title) && trim($title) != "" ? HtmlPurifier::process(trim($title)) : null;
        $description        = isset($description) && trim($description) != "" ? HtmlPurifier::process(trim($description)) : null;
        $themeID            = isset($themeID) && is_numeric($themeID) ? (int)$themeID : null;
        $previousLetterID = isset($previousLetterID) && is_numeric($previousLetterID) ? (int)$previousLetterID : null;

        if (empty($description) || (empty($title) && empty($themeID))) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }
        
        if (!$user->user_type || !$otherUser->user_type || 
            ($user->user_type == $otherUser->user_type) || 
            !in_array($user->user_type, [User::USER_MALE, User::USER_FEMALE]) || 
            !in_array($otherUser->user_type, [User::USER_MALE, User::USER_FEMALE])) {
                return [
                    'success' => false, 
                    'message' => 'Missing params'
                ];
        }

        if ($user->inBlackList($otherUser->id)) {
            return [
                'success' => false, 
                'message' => 'User add you in black list'
            ];
        }

        $letterService = new LetterService($user->id);

        return $letterService->send('send letter', $skinID, $otherUser->id, $title, $themeID, $description, $previousLetterID);
    }

    public function actionGetHistory($letterID)
    {        

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        $otherUserID = Yii::$app->request->post('otherUserID');
        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }

        $page = Yii::$app->request->post('page');
        if (empty($page) || $page < 0) {
            $page = 1;
        }
        $offset = ($page - 1) * self::NUMBER_OF_LETTERS_ON_HISTORY_PAGE;

        $myIDs = $otherUser->getMyChidrensIds('girls');
        
        $letterService = new LetterService($otherUser->id);

        $lettersArray = $letterService->getHistoryById($letterID, $myIDs, self::NUMBER_OF_LETTERS_ON_HISTORY_PAGE, $offset);

        if(!$lettersArray['success']){
            return [
                'success' => false, 
                'code' => 1, 
                'message' => $lettersArray['message']
            ];
        }

        return [
            'success' => true, 
            'lettersArray' => $lettersArray['letters'],
            'lettersCount'=>$lettersArray['count']
        ];
    }

    public function actionGetLettersCount()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        $otherUserID = Yii::$app->request->post('otherUserID');
        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;

        $themeID = Yii::$app->request->post('themeID');
        $letterID = Yii::$app->request->post('letterID');
        $type = Yii::$app->request->post('type');
        $limit = Yii::$app->request->post('limit');
        $offset = Yii::$app->request->post('offset');
        $searchParams = Yii::$app->request->post('searchParams');

        $themeID       = (isset($themeID) && is_numeric($themeID)) ? (int)$themeID : null;
        $letterID      = (isset($letterID) && is_numeric($letterID)) ? (int)$letterID : null;
        $type          = (isset($type)) ? $type : null;
        $limit         = isset($limit) ? $limit : null;
        $offset        = isset($offset) ? $offset : null;
        $searchParams = isset($searchParams) ? $searchParams : null;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }        

        $letterService = new LetterService($otherUser->id);
        $count = $letterService->getLettersCount($letterID, $themeID, $type, $limit, $offset, $searchParams);

        return [
            'success' => true, 
            'count' => (int)$count
        ];
    }

    public function actionGetNewLettersCount()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        $otherUserID = Yii::$app->request->post('otherUserID');
        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }        

        $letterService = new LetterService($otherUser->id);
        $newLetters = $letterService->getNewLetters();

        return [
            'success' => true, 
            'count' => $newLetters['count']
        ];
    }

    public function actionReadLetter()
    {
        $letterID = Yii::$app->request->post('letterID');

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => 'User not found'
            ];
        }

        if (empty($letterID) || !is_numeric($letterID)) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }

        $letterService = new LetterService($user->id);

        return $letterService->read((int)$letterID);
    }

    public function actionDeleteLetter()
    {
        $user = Yii::$app->user->identity;
        if (empty($user)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'User not found'
            ];
        }

        $letterID = Yii::$app->request->post('letterID');
        if (empty($letterID)) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }
        if (in_array($user->user_type, [User::USER_FEMALE, User::USER_MALE])) {
            $letterService = new LetterService($user->id);
        } elseif ($user->user_type == User::USER_AGENCY) {
            $otherUserID = Yii::$app->request->post('otherUserID');
            $myIDs = $user->getMyChidrensIds('girls');
            if (!in_array($otherUserID, $myIDs)) {
                return [
                    'success' => false, 
                    'code' => 1,
                    'message' => 'Wrong user ID'
                ];
            }
            $letterService = new LetterService($otherUserID);
        } else {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }

        return $letterService->setLetterStatus($letterID, Letter::WAS_DELETED);
    }

    public function actionRestoreLetter()
    {
        $user = Yii::$app->user->identity;
        if (empty($user)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'User not found'
            ];
        }

        $letterID = Yii::$app->request->post('letterID');
        if (empty($letterID)) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }

        $letterService = new LetterService($user->id);

        return $letterService->setLetterStatus($letterID, Letter::WASNT_DELETED);
    }

    public function actionSetFeaturedLetter()
    {
        $user = Yii::$app->user->identity;
        if (empty($user)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'User not found'
            ];
        }
        $letterID = Yii::$app->request->post('letterID');

        if (!isset($letterID)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }
        
        $letterService = new LetterService($user->id);

        return $letterService->setFeatured($letterID);
    }

}