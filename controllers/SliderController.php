<?php
namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\Response;

/* my models */
use app\models\LogTable;
use app\models\User;
use app\models\Album;


/* my services */
use app\services\LetterService;
use app\services\AlbumService;
use app\services\PhotoService;


class SliderController extends ActiveController
{
    use CommonActionsTrait;
    public $modelClass = '';

    public function behaviors()
    {
        $authArray = ['get-sliders', 'edit-slider', 'create-slider', 'delete-slider', 'get-slider'];
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => $authArray
        ];

        $access = $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['login'],
            'rules' => [
                [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => $authArray,
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $verbs = $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ];
        
        unset($behaviors['access']);
        unset($behaviors['authenticator']);
        unset($behaviors['verbs']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Allow-Origin'      => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],
        ];
        
        $behaviors['rateLimiter'] = [
            // Use class
            'class' => \highweb\ratelimiter\RateLimiter::className(),
            
            // The maximum number of allowed requests
            'rateLimit' => 100,
            
            // The time period for the rates to apply to
            'timePeriod' => 60,
            
            // Separate rate limiting for guests and authenticated users
            // Defaults to true
            // - false: use one set of rates, whether you are authenticated or not
            // - true: use separate ratesfor guests and authenticated users
            'separateRates' => false,
            
            // Whether to return HTTP headers containing the current rate limiting information
            'enableRateLimitHeaders' => false,
        ];
        
        $behaviors['authenticator'] = $auth;
        $behaviors['access'] = $access;
        $behaviors['verbs'] = $access;
        $behaviors['authenticator']['except'] = ['options', 'get-sliders-on-front-page'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }
   
    public function actionGetSliders()
    {
        $this->checkAccess($this->id, null);

        $user = Yii::$app->user->identity;        

        $albumID = 'slider';

        $album = Album::findOne(['active' => 1, 'title' => $albumID]);

        if (empty($album)) {
            $albumService = new AlbumService($user->id);
            $album = $albumService->Create(0, $albumID, 'slider album');
            if (!$result['success']) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => $result['message']
                ];
            }
            $album = ['id' => $album->album_id, 'title' => 'slider'];
            $result = [];
        } else {
            $albumService = new AlbumService($album->user_id);
            $sliders = $albumService->getPhotos($albumID, $status = 1);
            $i = 0;
            $result = [];
            foreach ($sliders as $slider) {
                $result[$i]['title'] = $slider['title'];
                $result[$i]['id'] = $slider['id'];
                $result[$i]['status'] = $slider['status'];
                $result[$i]['approve_status'] = $slider['approve_status'];
                $result[$i]['small_thumb'] = $slider['small_thumb'];
                $result[$i]['medium_thumb'] = $slider['medium_thumb'];
                $i++;
            }
        }

        $album = (object)$album;

        $response = [
            'success' => true,
            'sliders' => $result,            
        ];
        if ($user->user_type == User::USER_SUPERADMIN) {
            $response['album'] = [
                'id' => $album->id,
                'title' => $album->title
            ];
        }
        return $response;
    }

    public function actionGetSlidersOnFrontPage()
    {

        $albumID = 'slider';

        $album = Album::findOne(['active' => 1, 'title' => $albumID]);

        if (empty($album)) {
            return [
                'success' => true,
                'sliders' => [], 
            ];
        }
        $albumService = new AlbumService($album->user_id);
        $sliders = $albumService->getPhotos($albumID, $status = 1);

        $i = 0;
        $result = [];
        foreach ($sliders as $slider) {
            $result[$i]['title'] = $slider['title'];
            $result[$i]['original_image'] = $slider['original_image'];
            $i++;
        }

        $response = [
            'success' => true,
            'sliders' => $result,            
        ];
        
        return $response;
    }

/*    public function actionGetSlider($sliderID)
    {        

        $this->checkAccess($this->id, null);

        $user = Yii::$app->user->identity;

        $photoService = new PhotoService($user->id);
        $slider = $photoService->getPhotoById($sliderID);

        return $slider;
    }

    public function actionCreateSlider()
    {        

        $this->checkAccess($this->id, null);

        $user = Yii::$app->user->identity;

        $sliderTitle = Yii::$app->request->post('title');
        $sliderDescription = Yii::$app->request->post('description');
        
        $photoService = new PhotoService($user->id);
        $slider = $photoService->uploadSlider($sliderTitle, $sliderDescription);

        if(!$slider['success']){
            return [
                'success' => false, 
                'code' => 1, 
                'message' => $slider['message']
            ];
        }

        return [
            'success' => true, 
            'message' => $slider['message'],
        ];
    }

    public function actionEditSlider($sliderID)
    {
        $this->checkAccess($this->id, null);

        $user = Yii::$app->user->identity;

        $sliderTitle = Yii::$app->request->post('title');

        $photoService = new PhotoService($user->id);
        $slider = $photoService->updatePhotoTitleById($sliderID, $sliderTitle, 0);

        return [
            'success' => true, 
            'count' => $slider['message']
        ];
    }

    public function actionDeleteSlider($sliderID)
    {
        $this->checkAccess($this->id, null);

        $user = Yii::$app->user->identity;

        if (empty($sliderID)) {
            return [
                'success' => false, 
                'message' => 'Missing params'
            ];
        }

        $photoService = new PhotoService($user->id);

        return $photoService->deletePhotoById($sliderID);
    }*/

    public function checkAccess($action, $model = null, $params = [])
    {
        // check if the user can access $action and $model
        // throw ForbiddenHttpException if access should be denied        

        $user = Yii::$app->user->identity;        

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        /*if (!in_array($user->user_type, [User::USER_SUPERADMIN])) {
            return [
                'success' => FALSE,
                'code'    => 1,
                'message' => 'Access Denied',
            ];
        }*/
    }

}