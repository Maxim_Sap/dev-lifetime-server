<?php
namespace app\controllers;

use app\models\User;

trait CommonActionsTrait
{
    protected function checkRights($user, $otherUser)
    {
        if (empty($user) || empty($otherUser)) {
            return [
                'success' => false, 
                'code' => 2, 
                'message' => 'Access denied'
            ];
        }
        if (empty($user->status) || empty($user->owner_status) || 
            $user->status == User::STATUS_NO_ACTIVE || $user->status == User::STATUS_DELETED ||            
            $user->owner_status == User::STATUS_NO_ACTIVE || $user->owner_status == User::STATUS_DELETED) {
            return [
                'success' => false, 
                'code' => 2, 
                'message' => 'User not active'
            ];
        }
        if (!in_array($user->user_type, [User::USER_AGENCY, User::USER_SUPERADMIN, User::USER_ADMIN])) {
            if ($otherUser->status == User::STATUS_NO_ACTIVE || $otherUser->status == User::STATUS_DELETED ||            
                $otherUser->owner_status == User::STATUS_NO_ACTIVE || $otherUser->owner_status == User::STATUS_DELETED
                ) {
                return [
                    'success' => false, 
                    'code' => 2, 
                    'message' => 'User not active'
                ];
            }
        }
        
        if ($user->user_type == User::USER_INTERPRETER && (in_array($otherUser->id, $user->user_ids) || $otherUser->id == $user->id)) {
            return [
                'success' => TRUE,
            ];
        }
        
        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            // user without assosiated agency, users agency not match
            if ($user->agency_id == 1 || $user->agency_id != $otherUser->agency_id) {
                return [
                    'success' => false, 
                    'code' => 2, 
                    'message' => 'Access denied'
                ];
            }
        } elseif (in_array($user->user_type, [User::USER_SUPERADMIN]) || $user->id == $otherUser->id) {
            return [
                'success' => true
            ]; 
        } else {            
            if (($user->user_type == $otherUser->user_type && $user->id != $otherUser->id) || 
               !in_array($user->user_type, [User::USER_MALE, User::USER_FEMALE]) || 
               !in_array($otherUser->user_type, [User::USER_MALE, User::USER_FEMALE])) {
                return [
                    'success' => false, 
                    'code' => 2, 
                    'message' => 'Access denied'
                ];
            }
        }
        return [
            'success' => true
        ];        
    }
}