<?php
namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\Response;

/* my models */
use app\models\Albums;
use app\models\ActionType;
use app\models\Action;
use app\models\Agency;
use app\models\AgencyRevard;
use app\models\LogTable;
use app\models\Photo;
use app\models\PhotoLike;
use app\models\PhotoAccess;
use app\models\Status;
use app\models\User;
use app\models\UserProfile;


/* my services */
use app\services\AlbumService;
use app\services\PhotoService;
use app\services\BillingService;
use app\services\PaymentsService;


class PhotoController extends ActiveController
{
    use CommonActionsTrait;
    public $modelClass = '';

    public function behaviors()
    {
        $authArray = ['get-one-photo', 'update-photo-info', 'delete-photo', 'crop-photo', 'get-photos', 'update-access',
                      'upload-image-from-chat', 'update-photo-title', 'set-photo-status', 'upload-photo-to-album', 'like', 'dislike', 'like-all', 'get-girls-that-liked-photo'];
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => $authArray
        ];

        $access = $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['login'],
            'rules' => [
                [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => $authArray,
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $verbs = $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ];
        
        unset($behaviors['access']);
        unset($behaviors['authenticator']);
        unset($behaviors['verbs']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to
                    'Origin'                           => Yii::$app->params['origin'],
                    'Access-Control-Allow-Origin'      => Yii::$app->params['origin'],
                    'Access-Control-Request-Method'    => ['POST', 'GET', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers'   => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age'           => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
                ],
        ];
        
        $behaviors['rateLimiter'] = [
            // Use class
            'class' => \highweb\ratelimiter\RateLimiter::className(),
            
            // The maximum number of allowed requests
            'rateLimit' => 100,
            
            // The time period for the rates to apply to
            'timePeriod' => 60,
            
            // Separate rate limiting for guests and authenticated users
            // Defaults to true
            // - false: use one set of rates, whether you are authenticated or not
            // - true: use separate ratesfor guests and authenticated users
            'separateRates' => false,
            
            // Whether to return HTTP headers containing the current rate limiting information
            'enableRateLimitHeaders' => false,
        ];
        
        $behaviors['authenticator'] = $auth;
        $behaviors['access'] = $access;
        $behaviors['verbs'] = $access;
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    public function beforeAction($action)
    {                   
        if (parent::beforeAction($action)) {
            $user = \Yii::$app->user->identity;
            if (!Yii::$app->user->isGuest && in_array($user->user_type, [User::USER_ADMIN, User::USER_SITEADMIN]) && (!isset($user->permissions) || $user->permissions->cantAccess('user_access'))) {
                throw new \yii\web\ForbiddenHttpException(sprintf('Access denied.', $action));
            }
            return true;
        } else {
            return false;
        }        
    }
   
    public function actionGetOnePhoto()
    {
        $otherUserID = Yii::$app->request->post('otherUserID');
        
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            throw new UnauthorizedHttpException();
        }

        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }

        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to get photo from user {$otherUser->id}");
                throw new ForbiddenHttpException('Access Denied', 10003);
            }
        }

        $photoID = Yii::$app->request->post('photoID');

        if (isset($photoID) && is_numeric($photoID)) {
            $photoID = (int) $photoID;
        } else {
            return [
                'success' => false, 
                'code' => 1,
                'message' => 'Photo id can\'t be empty'
            ];
        }

        $photoService = new PhotoService($otherUser->id);
        $result = $photoService->getUserInfoByPhotoId($photoID);

        $photoAccess = PhotoAccess::find()->where(['user_id' => $user->id])->asArray()->all();        
        $canWatch = ArrayHelper::map($photoAccess, 'id', 'photo_id');
        
        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN, User::USER_SUPERADMIN])) {
            $simplifiedResult = $result;
        } else if ($user->id != $otherUser->id) {
            $simplifiedResult['id'] = $result['id'];
            $simplifiedResult['album_id'] = $result['album_id'];
            $simplifiedResult['small_thumb'] = $result['small_thumb'];
            $simplifiedResult['medium_thumb'] = $result['medium_thumb'];
            $simplifiedResult['original_image'] = $result['original_image'];            
            if ($result['premium'] == 1 && $userStatus != "PREMIUM" && !in_array($result['id'], $canWatch)) {
                $simplifiedResult['small_thumb'] = '/img/premium_small.png'/*$photo['watermark_small']*/;
                $simplifiedResult['medium_thumb'] = '/img/premium_medium.jpg'/*$photo['watermark_medium']*/;
                $simplifiedResult['original_image'] = '/img/premium_big.jpg'/*$photo['watermark_orign']*/;
            }
        } else if ($user->id == $otherUser->id) {
            $simplifiedResult['id'] = $result['id'];
            $simplifiedResult['album_id'] = $result['album_id'];
            $simplifiedResult['small_thumb'] = $result['small_thumb'];
            $simplifiedResult['medium_thumb'] = $result['medium_thumb'];
            $simplifiedResult['original_image'] = $result['original_image']; 
            $simplifiedResult['title'] = $result['title'];
            $simplifiedResult['description'] = $result['description'];
            $simplifiedResult['approve_status'] = $result['approve_status'];
            $simplifiedResult['first_name'] = $result['first_name'];
            $simplifiedResult['last_name'] = $result['last_name'];
            $simplifiedResult['user_id'] = $result['user_id'];                    
        }

        return [
            'success' => true, 
            'photo' => $simplifiedResult
        ];
    }

    public function actionUpdatePhotoInfo()
    {
        
        $otherUserID = Yii::$app->request->post('otherUserID');
        
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            throw new UnauthorizedHttpException();            
        }

        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }

        if ($otherUserID != $user->id && $user->user_type != User::USER_SUPERADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to update photo information from user {$otherUser->id}");
                throw new ForbiddenHttpException('Access Denied', 10003);
            }
        }

        $photoID = Yii::$app->request->post('photoID');
        if (!isset($photoID) || !filter_var($photoID, FILTER_VALIDATE_INT)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        $title = Yii::$app->request->post('title');
        $premium = Yii::$app->request->post('premium');
        $approveStatus = Yii::$app->request->post('approveStatus');

        $title = (isset($title) && trim($title) != '') ? trim($title) : "";
        $premium = (isset($premium) && is_numeric($premium)) ? (int)$premium : 1;            
        $approveStatus = (isset($approveStatus) && is_numeric($approveStatus)) ? $approveStatus : null;        

        if (in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            //for agency
            $photoService = new PhotoService($otherUser->id);
            $result = $photoService->updatePhotoTitleById($photoID, $title, $premium);
            LogTable::addLog($user->id, $user->user_type, 'Update Info Photo ID:'. $photoID. ", title: ". $title.", owner user ID: ".$otherUser->id);
        } elseif ($user->user_type == User::USER_SUPERADMIN) {
            //for admin
            $userDeleteStatus = Yii::$app->request->post('userDeleteStatus');
            $userDeleteStatus = (isset($userDeleteStatus) && $userDeleteStatus == 0) ? 0 : 1;
            $photoService = new PhotoService($otherUser->id);
            $result = $photoService->updatePhotoInfo($photoID, $title, $premium, $approveStatus, $userDeleteStatus);
            LogTable::addLog($user->id, $user->user_type, 'Update Info Photo ID:'. $photoID.", title: ".$title.", approve status:".$approveStatus.", owner user ID: ".$otherUser->id);
        } else {
            throw new ForbiddenHttpException('Access Denied', 10004);
        }

        return $result;
    }

    public function actionUpdatePhotoTitle()
    {

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => FALSE, 
                'code' => 1, 
                'message' => 'User not found'
            ];           
        }        

        $photoTitle = Yii::$app->request->post('photoTitle');

        $photoTitle = (isset($photoTitle) && trim($photoTitle) != '') ? trim($photoTitle) : "";        

        $photoID = Yii::$app->request->post('photoID');
        $photoID = (isset($photoID) && is_numeric($photoID)) ? (int) $photoID : null;
        
        $photo = Photo::findOne(['id' => $photoID]);
        // check if user is owner of the photo
        
        $ownerPhoto = $photo->album->user_id == $user->id;
        
        if (!$ownerPhoto && !in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }
        if ($photo->album->user_id != $user->id && in_array($user->user_type, [User::USER_AGENCY, User::USER_ADMIN])) {
            // check if agency have this user            
            $userInAgency = $photo->album->user->agency_id == $user->agency_id;
            if (!$userInAgency) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'Missing params'
                ];
            }
        }

        $user = User::findOne(['id' => $photo->album->user_id]);
        $photoService = new PhotoService($user->id);

        return $photoService->updatePhotoTitleById($photoID, $photoTitle, $photo->premium);
    }

    public function actionGetPhotos()
    {
        $otherUserID = Yii::$app->request->post('otherUserID');
        
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'User not found'
            ];
        }

        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;

        $otherUser = User::findOne(['id' => $otherUserID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }
        $albumID = Yii::$app->request->post('albumID');
        if (isset($albumID) && is_numeric($albumID)) {
            $albumID = (int) Yii::$app->request->post('albumID');
        } elseif ($albumID == 'letters') {
            $albumID = 'letters';
        } else {
            $albumID = 'Public Album';   
        }        


        $albumService = new AlbumService($otherUser->id);

        $result = $albumService->getPhotos($albumID, $status = 1);
        
        $date = date("Y-m-d H:i:s", time());

        $photoAccess = PhotoAccess::find()->where(['user_id' => $user->id])->asArray()->all();        
        $canWatch = ArrayHelper::map($photoAccess, 'id', 'photo_id');

        $userStatusArray = $user->getStatuses()->joinWith('statusType')->where(["<=", 'start_datetime', $date])->andWhere(['active' => Status::STATUS_ACTIVE])->andWhere([">=", 'stop_datetime', $date])->all();        
        $userStatus      = isset($userStatusArray[0]["statusType"]->title) ? $userStatusArray[0]["statusType"]->title : "BASIC";

        $photos = [];
        
        if (!empty($result)) {
            $i = 0;
            foreach ($result as $photo) {
                if ($photo['status'] == 1 && $photo['approve_status'] == 4 && $user->id != $otherUser->id) {                         
                    $photos[$i]['id'] = $photo['id'];
                    $photos[$i]['small_thumb'] = $photo['small_thumb'];
                    $photos[$i]['medium_thumb'] = $photo['medium_thumb'];
                    $photos[$i]['original_image'] = $photo['original_image'];
                    $photos[$i]['likes'] = (int)$photo['likes'];
                    $photos[$i]['dislikes'] = (int)$photo['dislikes'];
                    if ($photo['premium'] == 1 && $userStatus != "PREMIUM" && !in_array($photo['id'], $canWatch)) {
                        $photos[$i]['small_thumb'] = '/img/premium_small.png'/*$photo['watermark_small']*/;
                        $photos[$i]['medium_thumb'] = '/img/premium_medium.jpg'/*$photo['watermark_medium']*/;
                        $photos[$i]['original_image'] = '/img/premium_big.jpg'/*$photo['watermark_orign']*/;
                    }
                }
                if ($photo['status'] == 1 && $user->id == $otherUser->id) {
                    $photos[$i]['id'] = $photo['id'];
                    $photos[$i]['small_thumb'] = $photo['small_thumb'];
                    $photos[$i]['medium_thumb'] = $photo['medium_thumb'];
                    $photos[$i]['original_image'] = $photo['original_image'];
                    $photos[$i]['likes'] = (int)$photo['likes'];
                    $photos[$i]['dislikes'] = (int)$photo['dislikes'];
                    $photos[$i]['title'] = $photo['title'];
                    $photos[$i]['approve_status'] = $photo['approve_status'];
                }
                $i++;
            }
        }

        return [
            'success' => true, 
            'photos' => $photos, 
        ];
    }

    public function actionGetGirlsThatLikedPhoto()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            throw new UnauthorizedHttpException();            
        }

        $albumService = new AlbumService($user->id);
        $result = $albumService->getGirlsThatLikedMansPhoto($user->id);

        if ($result['success']) {
            $girls = $result['girls'];
            $i = 0;
            foreach ($girls as $girl) {
                $girls[$i]['online'] = ($girl['last_activity'] >= time() - 30) ? 'Online' : 'Offline';
                unset($girls[$i]['last_activity']);
                $i++;
            }
        } else {
            $girls = [];
        }
        
        return [
            'success' => true,
            'girls' => $girls
        ];
    }

    public function actionSetPhotoStatus()
    {        
        $photoID = Yii::$app->request->post('photoID');
        $status = Yii::$app->request->post('status');

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false,
                'code' => 1,
                'message' => 'User not found'
            ];
        }

        if (!isset($photoID) || !is_numeric($photoID) || !isset($status) || !is_numeric($status)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }                        
        $photo = Photo::findOne(['id' => $photoID]);
        // check if user is owner of the photo
        
        $ownerPhoto = $photo->album->user_id == $user->id;
        if ($ownerPhoto) {
            $photoService = new PhotoService($user->id);
            $result = $photoService->setStatus($photoID, $status);
            if ($result['success']) {
                LogTable::addLog($user->id, $user->user_type, ' set photo status '. $status. ', to photo ID: ' . $photoID);
            }

            return $result;
        } else {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Failed to remove the object for unknown reason.'
            ];
        }

    }    

    public function actionUploadPhotoToAlbum()
    {        
        $albumTitle = Yii::$app->request->post('albumTitle');

        if (empty($albumTitle)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        $currentUserType = $user->user_type;

        $title = Yii::$app->request->post('albumTitle');                
        $photoTitle = Yii::$app->request->post('photo_title'); 
        $photoDesc  = Yii::$app->request->post('photo_desc');
        $otherUserID = Yii::$app->request->post('otherUserID');

        $photoTitle = (!empty($photoTitle)) ? $photoTitle : '';
        $photoDesc = (!empty($photoDesc)) ? $photoDesc : '';

        $userID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : $user->id;
        $otherUser = User::findOne(['id' => $userID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => $result2['message']
            ];
        }
        
        if ($userID != $user->id && $user->user_type != User::USER_SUPERADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to upload foto to user {$otherUser->id}");
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'Access Denied'
                ];
            }
        }        

        // load photo in temp directory
        if ($user->user_type == User::USER_SUPERADMIN && $title == 'temp') {
            LogTable::addLog($user->id, $user->user_type, $user->id . 'upload photo to temp folder, owner user ID: '. $otherUser->id);
            $photoService = new PhotoService($otherUser->id);
            return $photoService->uploadGiftImg();
        }

        LogTable::addLog($user->id, $user->user_type, $user->id . 'upload photo to album: '. $title .', owner user ID: '.$otherUser->id);

        $albumService = new AlbumService($otherUser->id);

        return $albumService->Get($title)->uploadPhoto($photoTitle, $photoDesc);
    } 

    public function actionUpdateAccess()
    {
        $photoID = Yii::$app->request->post('photoID');

        if (!isset($photoID) || !is_numeric($photoID)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            throw new UnauthorizedHttpException();            
        }

        if ($user->user_type == User::USER_MALE) { 
            $paymentsService = new PaymentsService($user->id);
            $userBalance = $paymentsService->getBalance();
            $actionType = ActionType::findOne(['name' => 'watch photo']);
            if (empty($actionType)) {
                return [
                    'success' => FALSE, 
                    'code' => 2, 
                    'message' => 'Watch user photo price error'
                ];
            }

            $billingService     = new BillingService($user->id);
            $watchPhotoPrice = $billingService->getActionPrice($skinID = 1, $planID = 1, $actionType->id, $user->id);

            if (!is_numeric($watchPhotoPrice)) {
                return [
                    'success' => FALSE, 
                    'code' => 2, 
                    'message' => 'Watch user photo price error'
                ];
            }

            if ($userBalance < $watchPhotoPrice) {
                return [
                    'success' => FALSE, 
                    'code' => 2, 
                    'message' => 'Not enough money'
                ];
            }
            $photo = Photo::findOne(['id' => $photoID]);
            if (!empty($photo)) {                
                //add new action
                $action                 = new Action();
                $action->action_type_id = $actionType->id;
                $action->skin_id        = 1;
                $action->action_creator = $user->id;
                $action->action_receiver  = $photo->album->user_id;
            } else {
                return [
                    'success' => FALSE, 
                    'code' => 2, 
                    'message' => 'Wrong photo id'
                ];                
            }
            
            if (!$action->save()) {
                return [
                    'success' => FALSE, 
                    'code' => 2, 
                    'message' => 'Action not save'
                ];
            }

            //с мужиков взымаем плату
            $paymentsService->addCredits($action->id, $watchPhotoPrice);

            //добавляем премию агенству
            $girl = User::findOne(['id' => $photo->album->user_id]);
            if (!empty($girl)) {
                //даем бонус агенству
                $agency      = User::find()->where([
                                                    'user_type' => User::USER_AGENCY, 
                                                    'agency_id' => $girl->agency_id,
                                                    'status' => User::STATUS_ACTIVE
                                                ])->one();
                $agencyWatchPhotoPrice = $billingService->getAgencyPrice($skinID = 1, $planID = 1, $actionType->id, $agency->id);
                if (!empty($agencyWatchPhotoPrice)) {
                    $agencyBonus            = new AgencyRevard();
                    $agencyBonus->action_id = $action->id;
                    $agencyBonus->user_id   = $agency->id;
                    $agencyBonus->amount    = $agencyWatchPhotoPrice;
                    if (!$agencyBonus->save()) {
                        return [
                            'success' => FALSE, 
                            'code' => 2, 
                            'message' => 'Agency bonus not save'
                        ];
                    }
                } else {
                    return [
                        'success' => FALSE, 
                        'code' => 2, 
                        'message' => 'Watch user photo price error'
                    ];
                }
            }
            $photoService = new PhotoService($user->id);
            $result = $photoService->updateAccess($photoID);
        }
        
        if (!empty($result)) {
            return [
                'success' => true
            ];
        }

        return [
            'success' => false,
            'code' => 1,
            'message' => 'Some error happend'
        ];
    }

    public function actionDeletePhoto()
    {
        $photoID = Yii::$app->request->post('photoID');
        if (!isset($photoID) || !is_numeric($photoID)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            throw new UnauthorizedHttpException();            
        }

        // define photo owner
        $photoService = new PhotoService();
        $photoOwnerID = $photoService->getOwnerId($photoID);

        if (empty($photoOwnerID)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        $otherUser = User::findOne(['id' => $photoOwnerID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }

        if ($photoOwnerID != $user->id && $user->user_type != User::USER_SUPERADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to delete photo from user {$otherUser->id}");
                throw new ForbiddenHttpException('Access Denied', 10003);
            }
        }

        $photoService->setUser($otherUser->id);
        $result = $photoService->deletePhotoById($photoID);

        if ($result['success']) {
            LogTable::addLog($user->id, $user->user_type, 'User with id ' . $user->id . ' delete photo ID: '. $photoID . ", owner user ID: ". $otherUser->id);
        }

        return [
            'success' => true, 
            'userID' => $otherUser->id
        ];
    }

    public function actionUploadImageFromChat()
    {
        $otherUserID = Yii::$app->request->post('otherUserID');        

        if (!isset($otherUserID) || !is_numeric($otherUserID)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }
        
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }                

        $otherUserID = isset($otherUserID) && is_numeric($otherUserID) ? $otherUserID : null;

        $otherUser = User::findOne(['id' => $otherUserID]);

        if (empty($otherUser)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'User not found'
            ];
        }

        if ($user->user_type == User::USER_INTERPRETER) {
            $asUser = Yii::$app->request->post('asUser');
            if (empty($asUser) || !is_numeric($asUser) || !in_array($asUser, $user->user_ids)) {
                return [
                    'success' => false, 
                    'code' => 1, 
                    'message' => 'User not found'
                ];                
            } else {
                $user = User::findOne(['id' => (int)$asUser]);
            }
        }

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }
       
        $photoService = new PhotoService($user->id);
        $result = $photoService->uploadChatPhoto($otherUser->id);

        if (!$result['success']) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => $result['message']
            ];
        }
       
        $mediumThumb = $result['medium_thumb'];
       
        return [
            'success' => true,                                     
            'medium_thumb' => $mediumThumb        
        ];
    }

    public function actionLikeAll()
    {
        $user = Yii::$app->user->identity;

        if (empty($user)) {
            throw new UnauthorizedHttpException();            
        }

        $albumService = new AlbumService($user->id);
        $likes = $albumService->getAllLikes();        
        return [
            'success' => true,
            'likes' => (int) $likes
        ];
    }

    public function actionLike()
    {
        return $this->likeOrDislike('like');
    }

    public function actionDislike()
    {
        return $this->likeOrDislike('dislike');
    }

    private function likeOrDislike($type)
    {
        $photoID = Yii::$app->request->post('photoID');
        if (!isset($photoID) || !is_numeric($photoID)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Wrong photo id'
            ];
        }

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            throw new UnauthorizedHttpException();            
        }

        // define photo owner
        $photoService = new PhotoService();
        $photoOwnerID = $photoService->getOwnerId($photoID);

        if (empty($photoOwnerID)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Access denied'
            ];
        }

        $otherUser = User::findOne(['id' => $photoOwnerID]);

        if ($user->id == $otherUser->id) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'You can\'t like your own photo'
            ];
        }

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            return [
                'success' => false, 
                'code' => 1,
                'message' => $result2['message']
            ];
        }

        if ($this->changePhotoLikesOrDislikes($type, $photoID, $user->id)) {

            $photoService = new PhotoService($otherUser->id);
            $result = $photoService->getUserInfoByPhotoId($photoID);

            return [
                'success' => true,
                'likes' => $result['likes'],
                'dislikes' => $result['dislikes']
            ];
        } else {
            return [
                'success' => false, 
                'code' => 1,
                'message' => "Error during $type photo"
            ];
        }
        
    }

    private function changePhotoLikesOrDislikes($type, $photoID, $userID)
    {
        $photoLike = PhotoLike::find()->where(['photo_id' => $photoID, 'user_id' => $userID])->one();
        if (empty($photoLike)) {
            $photoLike = new PhotoLike();
            $photoLike->user_id = $userID;
            $photoLike->photo_id = $photoID;
        }
        if ($type == 'dislike') {
            if (empty($photoLike->dislike)) {
                $photoLike->likes = 0;
                $photoLike->dislikes = 1;
            } else {
                return true;
            }
        } else {
            if (empty($photoLike->like)) {
                $photoLike->likes = 1;
                $photoLike->dislikes = 0;
            } else {
                return true;
            }
        }
        return $photoLike->save();
    }

    public function actionCropPhoto()
    {
        $photoID = Yii::$app->request->post('photoID');
        if (!isset($photoID) || !is_numeric($photoID)) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        $imageCoordinates = Yii::$app->request->post('imageCoordinates');

        if (!isset($imageCoordinates) || !is_array($imageCoordinates) || !array_filter($imageCoordinates, 'is_numeric') || count($imageCoordinates) != 4) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        $imageSize = Yii::$app->request->post('imageSize');
        if (!isset($imageSize) || !is_array($imageSize) || !array_filter($imageSize, 'is_numeric') || count($imageSize) != 2) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        $user = Yii::$app->user->identity;

        if (empty($user)) {
            throw new UnauthorizedHttpException();            
        }

        // define photo owner
        $photoService = new PhotoService();
        $photoOwnerID = $photoService->getOwnerId($photoID);

        if (empty($photoOwnerID)) {
            return [
                'success' => false, 
                'code' => 1, 
                'message' => 'Missing params'
            ];
        }

        $otherUser = User::findOne(['id' => $photoOwnerID]);

        $result2 = $this->checkRights($user, $otherUser);

        if (!$result2['success']) {
            throw new ForbiddenHttpException('Access Denied', 10002);
        }

        if ($photoOwnerID != $user->id && $user->user_type != User::USER_SUPERADMIN) {
            // check if agency have this user                        
            $userAgency = $user->getAgency()->where(['!=', 'id', 1])->one();
            if (empty($userAgency) || $otherUser->agency_id != $userAgency->id) {
                LogTable::addLog($user->id, $user->user_type, "user with {$user->id} tries to crop photo from user {$otherUser->id}");
                throw new ForbiddenHttpException('Access Denied', 10003);
            }
        }

        $photoService->setUser($otherUser->id);
        $result = $photoService->cropImage($photoID, $imageCoordinates, $imageSize);

        return $result;
    }

}